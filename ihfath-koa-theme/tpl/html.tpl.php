<?php
// Kingdom of Arabic template
?>
<!DOCTYPE html>
<html lang="<?= $language->language ?>" dir="<?= $language->dir ?>">
  <head>
    <meta charset="utf-8">
    <?= $head ?>
    <title><?= $head_title ?></title>

    <!-- KOA CSS -->
    <?= $styles ?>
  </head>
  <body class="<?= $classes ?>" <?= $attributes ?>>
    <?= $page_top ?>
    <?= $page ?>
    <?= $page_bottom ?>

    <!-- TODO: Back to top -->
    <?= preg_replace('/jQuery\.extend/', '
      window.jQuery || (window.jQuery = { extend: function() {} });
      window.Drupal || (window.Drupal = {});
      jQuery.extend
    ', $scripts) ?>
  </body>
</html>
