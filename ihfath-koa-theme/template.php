<?php

// For autocmplete: kingdomarabic_

function kingdomarabic_theme() {
  $theme = array(
    "koa_custom_menu_item" => array(
      "template"       => "tpl/menu-item",
      "render element" => "item",
    ),
  );

  return $theme;
}

// Override menu for main page
function kingdomarabic_block_view_alter(&$data, $block) {
  // Change menu style to KOA theme
  if (is_koa_page()
      && $block->region === "navigation"
      && $block->module === "menu") {
    // Remove extra Drupal cruft
    unset($data["content"]["#contextual_links"]);
    $data["subject"] = "";

    // Reset theme
    foreach ($data["content"] as $id => $item) {
      $data["content"][$id]["#theme"] = "koa_custom_menu_item";
    }

    // Remove theme wrappers
    $data["content"]["#theme_wrappers"] = array();
  }
}
