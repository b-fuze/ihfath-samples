// @ts-check
window.addEventListener("DOMContentLoaded", function() {
  const hero = document.querySelector(".content-hero .background");
  const heroBottom = scrollY + hero.getBoundingClientRect().bottom;

  let counter = 0;
  window.addEventListener("scroll", () => {
    if (scrollY < heroBottom) {
      const cur = ++counter;
      requestAnimationFrame(function() {
        if (cur === counter) {
          hero.style.transform = "translate3d(0, " + (70 * (scrollY / heroBottom)) + "%, 0)";
        }
      });
    }
  });
});
