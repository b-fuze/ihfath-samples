window.addEventListener("DOMContentLoaded", function() {
  const levels = Array.from(document.querySelectorAll(".levels"));

  for (const levelList of levels) {
    levelList.addEventListener("click", function(e) {
      /* @type {HTMLElement} */
      const target = e.target;

      if (target.classList.contains("level-title")) {
        const level = target.parentNode;
        level.classList.toggle("expanded");
      }
    });

    // Prevent mouse selection
    levelList.addEventListener("mousedown", function(e) {
      /* @type {HTMLElement} */
      const target = e.target;

      if (target.classList.contains("level-title")) {
        e.preventDefault();
      }
    });
  }
});
