<?php

// Rewrite of Ihfathmsg's core utils

namespace Ihfathmsg;

abstract class Category {
  const Basic = 0;
  const Report = 1;
  const LessonNote = 2;
  const WeeklySchedule = 3;
  const LessonAttachment = 4;
  const PublicFile = 5;
}

const CATEGORY = array(
  "basic",                     // Admin-Professor, Admin-Student
  "report",                    // Reporting function
  "lesson-note",               // Lesson notes (when a participant rates at or below NOTE_THRESHOLD)
  "weekly-schedule",           // Prof weekly schedule lesson type
  "lesson-attachment",         // Lesson attaachments
  "public-file",               // Public file
);

const CATEGORY_CAMEL = array(
  "basic",
  "report",
  "lessonNote",
  "weeklySchedule",
  "lessonAttachment",
  "publicFile",
);

const CATEGORY_INDEX = array(
  0,
  1,
  2,
  3,
  4,
  5,
);

const IHFATH_ROLES = array(
  1,
  2,
  3,
);

// Cetegory meta details
const CATEGORY_META = array(
  array(),
  array(),
  array(),
  array(),
  array(),
  array(),
);

// Category specific options
const NOTE_THRESHOLD = 2;

class Utils {
  // Legend:
  //     -1: Required
  //   NULL: Optional, default empty
  //      *: Optional, default value
  static function copyArrayFields($reference, $input, $method_name = "Ihfathmsg\\Utils::copyArrayFields", $input_name = "\$msg") {
    $out_array = array();

    foreach ($reference as $field => $value) {
      if ($value === -1) {
        if (isset($input[$field]) && $input[$field] !== NULL) {
          $out_array[$field] = $input[$field];
        } else {
          throw new \Error("{$method_name}: {$input_name} is missing required field \"{$field}\"");
        }
      } else if ($value === NULL) {
        if (isset($input[$field]) && $input[$field] !== NULL) {
          $out_array[$field] = $input[$field];
        }
      } else {
        if (isset($input[$field]) && $input[$field] !== NULL) {
          $out_array[$field] = $input[$field];
        } else {
          $out_array[$field] = $value;
        }
      }
    }

    return $out_array;
  }
}

// Crud
class Prepare {
  const ATTACH_DIR = "public://ihfathusercontent/";
  const ATTACH_DIR_TMP = "public://ihfathusercontent_tmp/";

  static function validAttachments($max_count = 6) {
    global $user;
    $attachments = array();

    // Check that directories exist
    // TODO: Extract this to `enable`/`install` hooks
    $file_dir     = self::ATTACH_DIR;
    $file_dir_tmp = self::ATTACH_DIR_TMP;

    if (!file_prepare_directory($file_dir)) {
      drupal_mkdir($file_dir);
    }

    if (!file_prepare_directory($file_dir_tmp)) {
      drupal_mkdir($file_dir_tmp);
    }

    $uploaded_files = 0;
    $base_path      = base_path();

    // Loop over files
    $max_loop = $max_count + 1;

    for ($i=1; $i<$max_loop; $i++) {
      $file = file_save_upload("file" . $i, array(
        "file_validate_extensions" => array()
      ), self::ATTACH_DIR_TMP);

      if ($file) {
        $uploaded_files++;
        $file_hash = substr(sha1(time() . ""), 0, 8) . "-" . sha1(time() . "-" . $user->name . "-" . $file->filename);

        // Keep file
        $file->status = 1;
        file_save($file);

        // Add
        $attachments[] = (object) array(
          "fields" => array(
            "mid"       => 0,
            "suid"      => $user->uid,
            "file_name" => $file->filename,
            "file_size" => $file->filesize,
            "file_hash" => "",
          ),
          "hash_base" => $file_hash,
          "file" => $file,
        );
      }
    }

    return $attachments;
  }

  static function confirmAttachments($attachments, $mid) {
    foreach ($attachments as $attach) {
      $attach->fields["mid"] = $mid;
      $aid = db_insert("ihfath_msg_attach")
               ->fields($attach->fields)
               ->execute();

      // Complete hash
      $file_hash = $aid . $attach->hash_base;

      // Move file to new dir for no collisions
      file_move($attach->file, self::ATTACH_DIR . $file_hash);

      // Update database
      db_update("ihfath_msg_attach")
        ->fields(array("file_hash" => $file_hash))
        ->condition("aid", $aid)
        ->execute();
    }
  }
}

class Compose {
  // Legend:
  //     -1: Required
  //   NULL: Optional, default empty
  //      *: Optional, default value
  const MSG_FIELDS = array(
    // "userrole" => 0,
    "suid" => NULL,
    "ruid" => NULL,
    "category" => -1,
    "subject" => "",
    "body" => -1,
    "timestamp" => -1,
  );

  const ADDIN_FIELDS = array(
    "mid" => 0,
    "category" => -1,
    "baseint" => 0,
    "secondint" => 0,
    "txtval" => "",
  );

  const ATTACHMENT_FIELDS = array(
    "mid" => -1,
    "suid" => -1,
    "file_hash" => -1,
    "file_name" => -1,
    "file_size" => 0,
  );

  /**
   * Build messages and push them to the db
   * @param  array $msg         Array of the messages to be sent
   * @param  array $attachments Array of attachments for each message
   * @return array              Array of message IDs
   */
  static function basic($messages, $attachments = array(), $addins = array()) {
    $users = array();
    $msg_ids = array();
    $msg_attachments = array();

    foreach ($messages as $msg) {
      $msg_fields = Utils::copyArrayFields(self::MSG_FIELDS, $msg, "Ihfathmsg\\Compose::basic", "\$msg");
      $user = NULL;

      if (!isset($users[$msg_fields["suid"]])) {
        $user = $users[$msg_fields["suid"]] = user_load($msg_fields["suid"]);
      } else {
        $user = $users[$msg_fields["suid"]];
      }

      // FIXME: A NULL userrole probably indicates a bug
      $role = ihfathcore_get_role($user);
      $msg_fields["userrole"] = isset($role) ? $role : 0;

      $msg_query = \db_insert("ihfath_msg");
      $msg_query->fields($msg_fields);
      $msg_ids[] = $msg_query->execute();
    }

    // Add attachments and addins
    if (count($messages) === count($attachments)
        || count($messages) === count($addins)) {
      $index = 0;

      foreach ($messages as $msg) {
        $msg_id = $msg_ids[$index];

        $msg_attach = isset($attachments[$index]) ? $attachments[$index] : NULL;
        $msg_addins = isset($addins[$index]) ? $addins[$index] : NULL;

        // Populate attachments
        // FIXME: This ONLY works for a single message
        if ($msg_attach) {
          Prepare::confirmAttachments($msg_attach, $msg_id);
          $msg_attachments[$index] = count($msg_attach);
        }

        // Populate addins
        if ($msg_addins) {
          $addin_values = array();

          foreach ($msg_addins as $addin) {
            $addin_sanitized = Utils::copyArrayFields(self::ADDIN_FIELDS, $addin, "Ihfathmsg\\Compose::basic", "\$addin");
            $addin_sanitized["mid"] = $msg_id;
            $addin_values[] = $addin_sanitized;
          }

          \db_insert("ihfath_msg_addin")
            ->fields(array_keys(self::ADDIN_FIELDS), $addin_values)
            ->execute();
        }

        $index++;
      }
    }

    $msg_aggregate = array();

    $index = 0;
    foreach ($msg_ids as $mid) {
      $msg_aggregate[$mid] = (object) array(
        "mid" => $mid,
        "attachments" => isset($msg_attachments[$index]) ? $msg_attachments[$index] : 0,
      );

      $index++;
    }

    return $msg_aggregate;
  }

  static function simple($suid, $ruid, $subject, $body, $category = 0, $attachments = array(), $addins = array()) {
    $msg = self::basic(array(
      array(
        "suid" => $suid,
        "ruid" => $ruid,
        "subject" => $subject,
        "body" => $body,
        "category" => $category,
        "timestamp" => REQUEST_TIME,
      ),
    ), array(
      $attachments,
    ), array(
      $addins,
    ));

    return count($msg) ? array_values($msg)[0] : NULL;
  }

  static function report($lid, $suid, $body, $aspect, $attachments = NULL) {
    return self::simple($suid, NULL, NULL, $body, Category::Report, $attachments, array(
      array(
        "category" => Category::Report,
        "baseint" => $aspect,
        "secondint" => $lid,
      )
    ));
  }

  static function lessonNote($suid, $body, $lesson) { /* TODO: Implement */ }

  static function weeklySchedule($suid, $ruid, $body, $schedule) { /* TODO: Implement */ }

  static function lessonAttachment($lid, $suid, $attachments) {
    return self::simple($suid, NULL, "", "", Category::LessonAttachment, $attachments, array(
      array(
        "category" => Category::LessonAttachment,
        "baseint" => $lid,
        "secondint" => REQUEST_TIME,
      )
    ));
  }

  static function publicFile($suid, $attachments, $baseint = 0, $secondint = 0, $name = "", $desc = "") {
    return self::simple($suid, NULL, $name, $desc, Category::PublicFile, $attachments, array(
      array(
        "category" => Category::PublicFile,
        "baseint" => $baseint,
        "secondint" => $secondint,
      )
    ));
  }
}

class Get {
  const VALID_ADDIN_COLUMN = array(
    "baseint" => 1,
    "secondint" => 1,
  );

  static function exists($mid) {
    $msg_query = db_select("ihfath_msg", "im");
    $msg_query->fields("im", array("mid"));
    $msg_query->condition("im.mid", $mid);

    $msg = $msg_query->fetchAssoc();
    return !!$msg;
  }

  // Getters
  static function singleFrugal($mid) {
    $msg = Loader::basic($mid, FALSE, FALSE, NULL, NULL, NULL, NULL, FALSE, FALSE, FALSE, FALSE, FALSE, TRUE);

    return count($msg) ? $msg[0] : NULL;
  }

  static function single($mid, $set_viewed = FALSE) {
    $msg = Loader::basic($mid, TRUE, TRUE, NULL, NULL, NULL, NULL, FALSE, FALSE, $set_viewed);
    $msg_formatted = NULL;

    if (count($msg)) {
      $msg = array_values($msg)[0];

      if ($msg->msg->category && isset(CATEGORY[$msg->msg->category])) {
        // Use a specific loader
        $method = CATEGORY_CAMEL[$msg->msg->category];
        $msg_formatted = Loader::$method($msg);
      } else {
        $msg_formatted = $msg;
      }
    }

    return $msg_formatted;
  }

  static function bulk($viewed = NULL, $category = NULL, $role = NULL, $uid = NULL, $sent = FALSE, $pager = FALSE, $set_viewed = FALSE) {
    $msg_array = Loader::basic(NULL, TRUE, TRUE, $category, $role, $viewed, $uid, $sent, $pager, $set_viewed);
    $msg_formatted_array = array();

    foreach ($msg_array as $msg) {
      if ($msg->msg->category && isset(CATEGORY[$msg->msg->category])) {
        // Use a specific loader
        $method = CATEGORY_CAMEL[$msg->msg->category];
        $msg_formatted_array[] = Loader::$method($msg);
      } else {
        $msg_formatted_array[] = $msg;
      }
    }

    return $msg_formatted_array;
  }

  static function bulkAddin($category, $baseint = NULL, $secondint = NULL, $order_by = NULL, $viewed = NULL, $attachments = TRUE) {
    if ($order_by !== NULL && !isset(self::VALID_ADDIN_COLUMN[$order_by])) {
      throw new ErrorException("Ihfathmsg\\Get::bulkAddin: \$category can only be \"baseint\", \"secondint\", or NULL");
    }

    $msg_array = Loader::basic(NULL, TRUE, $attachments, $category, NULL, $viewed, NULL, NULL, FALSE, FALSE, FALSE, FALSE, FALSE, TRUE, $baseint, $secondint, $order_by);
    $msg_formatted_array = array();

    foreach ($msg_array as $msg) {
      if ($msg->msg->category && isset(CATEGORY[$msg->msg->category])) {
        // Use a specific loader
        $method = CATEGORY_CAMEL[$msg->msg->category];
        $msg_formatted_array[] = Loader::$method($msg);
      } else {
        $msg_formatted_array[] = $msg;
      }
    }

    return $msg_formatted_array;
  }

  static function count($viewed = NULL, $category = NULL, $role = NULL, $uid = NULL, $sent = FALSE, $group_users = FALSE) {
    return Loader::basic(NULL, FALSE, FALSE, $category, $role, $viewed, $uid, $sent, FALSE, FALSE, TRUE, $group_users);
  }

  static function singleByAttachment($file_hash, $set_viewed = FALSE) {
    $file_query = db_select("ihfath_msg_attach", "ima");
    $file_query->fields("ima", array("file_hash", "mid"))
               ->condition("ima.file_hash", $file_hash, "=");

    $attachment = $file_query->execute()->fetchObject();

    if ($attachment) {
      return self::single($attachment->mid, $set_viewed);
    } else {
      return NULL;
    }
  }

  static function attachment($file_id, $file_name) {
    $role = ihfathcore_get_role();

    // Prevent anonymous users from access to the files
    // TODO: Check if this constraint should be removed
    if (!$role) {
      drupal_not_found();
      exit;
    }

    // Get attachment metadata from db
    $file_query = db_select("ihfath_msg_attach", "ima");
    $file_query->fields("ima", array("file_hash", "file_name"))
               ->condition("ima.file_hash", $file_id, "=");

    $file_data = $file_query->execute()->fetchAssoc();

    if ($file_data === FALSE) {
      drupal_not_found();
      exit;
    }

    $file_data = (object) $file_data;

    if ($file_name !== $file_data->file_name) {
      // Exit when the right filename isn't provided
      drupal_not_found();
      exit;
    }

    file_transfer(Prepare::ATTACH_DIR . $file_data->file_hash, array(
      "Content-Type"        => "application/octet-stream",
      "Content-Disposition" => "attachment;",
    ));

    // Done
    exit;
  }
}

class Loader {
  static $pager_limit = 15;

  // TODO: Generalize this method
  static private function getIhfathRoleQuery($role) {
    static $role_id_map = array();
    $role_id = isset($role_id_map[$role]) ? $role_id_map[$role] : NULL;

    $role_name_map = array(
      1 => "administrator",
      2 => "professor",
      3 => "student",
    );

    if (!is_numeric($role) || !isset($role_name_map)) {
      throw new \ErrorException("Ihfathmsg\\Loader::getIhfathRoleQuery: \$role \"{$role}\" is invalid");
    }

    if (!isset($role_id)) {
      $role_id_query = db_select("role", "r");
      $role_id_query->fields("r", array("rid"));
      $role_id_query->condition("name", $role_name_map[$role], "=");

      $role_id = $role_id_query->execute()->fetchField();
      $role_id_map[$role] = $role_id;
    }

    return $role_id;
  }

  static function basic(
    $mid,
    $addins = TRUE,
    $attachments = TRUE,
    $category = CATEGORY_INDEX,
    $role = IHFATH_ROLES,
    $viewed = NULL,
    $uid = NULL,
    $sent = FALSE,
    $pager = FALSE,
    $set_viewed = FALSE,
    $count = FALSE,
    $group_users = FALSE,
    $frugal = FALSE,
    $addin_based = NULL,
    $addin_baseint = NULL,
    $addin_secondint = NULL,
    $addin_orderby = NULL
  ) {
    $ok = FALSE;
    $msg_array = array();
    $addin_array = array();
    $fields = array();
    $group_by_user = $sent ? "suid" : "ruid";

    if ($pager) {
      $query = \db_select("ihfath_msg", "im")->extend("PagerDefault");
      $query->limit(self::$pager_limit);
    } else {
      $query = \db_select("ihfath_msg", "im");
    }
    $query->orderBy("timestamp", "DESC");
    $result_query = $query;

    if ($mid) {
      $query->condition("im.mid", $mid);
    } else if (!$addin_based) {
      if ($viewed === NULL) {
        $query->condition("im.viewed", array(0, 1), "IN");
      } else {
        $query->condition("im.viewed", $viewed, "=");
        $fields[] = "viewed";
      }

      if ($category === NULL) {
        $query->condition("im.category", CATEGORY_INDEX);
      } else {
        $query->condition("im.category", $category);
        $fields[] = "category";
      }

      if ($role === NULL) {
        $query->condition("im.userrole", IHFATH_ROLES);
      } else {
        $query->condition("im.userrole", $role);
        $fields[] = "userrole";
      }

      if ($uid) {
        if ($sent) {
          $query->condition("im.suid", $uid);
          $fields[] = "suid";
        } else {
          $query->condition("im.ruid", $uid);
          $fields[] = "ruid";
        }
      } else {
        if ($sent) {
          $fields[] = "suid";
        } else {
          $fields[] = "ruid";
        }
      }
    } else {
      $addin_constraint_query = \db_select("ihfath_msg_addin", "ima");
      $addin_constraint_query->fields("ima", array("mid", "category", "baseint", "secondint"));

      if ($category === NULL) {
        $addin_constraint_query->condition("ima.category", CATEGORY_INDEX);
      } else {
        $addin_constraint_query->condition("ima.category", $category);
      }

      if (isset($addin_baseint)) {
        $addin_constraint_query->condition("ima.baseint", $addin_baseint);

        if (isset($addin_secondint)) {
          $addin_constraint_query->condition("ima.secondint", $addin_secondint);
        }
      }

      $result_query->rightJoin($addin_constraint_query, "ima", "ima.mid = im.mid");
    }

    if ($count) {
      $query->addExpression("COUNT(*)", "msg_count");
      $query->fields("im", $fields);

      if ($group_users) {
        $query->addExpression("MAX(timestamp)", "timestamp");
        $query->groupBy("{$group_by_user}");

        $user_query = db_select("users", "u");
        $user_query->fields("u", array("uid"));

        $user_query->leftJoin($query, "im", "u.uid = im.{$group_by_user}");
        $user_query->addField("im", "msg_count", "msg_count");
        $user_query->addField("im", "timestamp", "timestamp");

        if ($role) {
          // FIXME: Make this work for any number of roles
          $role_id = self::getIhfathRoleQuery(is_array($role) ? $role[0] : $role);
          $user_query->innerJoin("users_roles", "ur", "ur.uid = u.uid AND ur.rid = :roleid", array(":roleid" => (int) $role_id));
        }

        $user_query->orderBy("timestamp", "DESC");
        $result_query = $user_query;
      }
    } else {
      $query->fields("im");
    }

    $result = $result_query->execute();

    if ($count) {
      // Return with the msg count
      // FIXME: Provide a way to return a count based on a message addin query
      if ($group_users) {
        return $result->fetchAllAssoc("uid");
      } else {
        return $result->fetchAssoc()["msg_count"];
      }
    }

    while ($msg = $result->fetchAssoc()) {
      $msg = (object) $msg;
      $msg_array[$msg->mid] = (object) array(
        "msg"         => $msg,
        "body"        => $msg->body,
        "addins"      => array(),
        "attachments" => array(),
      );
    }

    if ($frugal) {
      $msg_result = array();

      foreach ($msg_array as $msg) {
        $msg_result[] = $msg->msg;
      }

      return $msg_result;
    }

    if (count($msg_array)) {
      $msg_id_array = array_keys($msg_array);

      // Get addins
      if ($addins) {
        $addin_query = \db_select("ihfath_msg_addin", "ima");
        $addin_query->fields("ima");

        $addin_query->condition("ima.mid", $msg_id_array, "IN");
        $addin_result = $addin_query->execute();

        while ($addin = $addin_result->fetchAssoc()) {
          $addin = (object) $addin;
          $msg_array[$addin->mid]->addins[] = $addin;
        }
      }

      // Get attachments
      if ($attachments) {
        $attach_query = \db_select("ihfath_msg_attach", "ima");
        $attach_query->fields("ima");

        $attach_query->condition("ima.mid", $msg_id_array, "IN");
        $attach_result = $attach_query->execute();

        while ($attach = $attach_result->fetchAssoc()) {
          $attach = (object) $attach;
          $msg_array[$attach->mid]->attachments[] = $attach;
        }
      }

      if ($set_viewed) {
        $viewed_query = \db_update("ihfath_msg");
        $viewed_query->fields(array(
          "viewed" => 1,
        ));

        $viewed_mid_array = array_keys($msg_array);
        $viewed_query->condition("mid", $msg_id_array, "IN");
        $viewed_query->execute();
      }
    }

    return $msg_array;
  }

  static function report($msg) {
    $msg = (object) array(
      "msg"         => $msg->msg,
      "body"        => $msg->body,
      "reason_text" => Misc::getReportReasons($msg->msg->userrole)[$msg->addins[0]->baseint][1],
      "reason_id"   => $msg->addins[0]->baseint,
      "lesson_id"   => $msg->addins[0]->secondint,
      "attachments" => $msg->attachments,
    );

    return $msg;
  }

  static function lessonNote($msg) {

  }

  static function weeklySchedule($msg) {

  }

  static function lessonAttachment($msg) {
    return (object) array(
      "msg"         => $msg->msg,
      "sender"      => user_load($msg->msg->suid),
      "attachments" => $msg->attachments,
      "lesson_id"   => $msg->addins[0]->baseint,
      "timestamp"   => $msg->addins[0]->secondint,
    );
  }

  static function publicFile($msg) {
    return (object) array(
      "name"        => $msg->msg->subject,
      "description" => $msg->body,
      "sender"      => user_load($msg->msg->suid),
      "file"        => $msg->attachments[0],
      "baseint"     => $msg->addins[0]->baseint,
      "secondint"   => $msg->addins[0]->secondint,
    );
  }
}

// Misc
class Misc {
  const BASE_REPORT_REASONS = array(
    // Professor report reasons
    2 => array(
      array(1, "Student is late"),
      array(1, "Student is absent"),
      array(1, "Student isn't paying attention"),
      array(1, "Internet connection lost"),
      array(1, "Student is occupied with his phone"),
      array(1, "Student doesn't do his homework"),
      array(1, "Whiteboard doesn't work for the student"),
      array(1, "Student doesn't understand the lesson well"),
      array(1, "Too much noise"),
      array(1, "Student can't hear the teacher well"),
      array(1, "Student's voice connection has issues"),
      array(1, "Student's level isn't improving as expected"),
      array(1, "Student understands things too slowly"),
      array(1, "Other"),
    ),

    // Student report reasons
    3 => array(
      array(1, "Teacher is late"),
      array(1, "Teacher is absent"),
      array(1, "Internet connection issues (I want to continue later)"),
      array(1, "Internet connection lost (I want to continue later)"),
      array(1, "Teacher isn't paying attention"),
      array(1, "Teacher is occupied with his phone"),
      array(1, "Teacher abandons the class"),
      array(1, "Teacher gives me no regard"),
      array(1, "Other"),
    ),
  );

  static function getReportReasons($role = NULL) {
    $reasons = variable_get("ihfathmsg_report_reasons", self::BASE_REPORT_REASONS);

    return $role ? $reasons[$role] : $reasons[2];
  }

  static function setReportReasons() {
    // TODO: Implement with variable_set
  }
}
