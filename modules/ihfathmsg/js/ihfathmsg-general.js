window.addEventListener("DOMContentLoaded", function() {
  var isDev;
  
  switch (location.host) {
    case "b-fusefactory.org":
    case "b-fusewarehouse.org": {
      isDev = true;
      break;
    }
    
    default:
      isDev = false;
  }
  
  // Setup colors
  lces.themify.colorize.compile();
  // lces.themify.colorize(52, 47, 60);
  lces.themify.colorize(169, 191, 4);
  lces.init();
  
  // Message update notifications
  function ihfathmsgUpdates() {
    var notifiSound = jSh.c("audio", {
      prop: {
        autoplay: false,
        preload: "auto",
        volume: 1,
        src: IHFATHMSG_BASEPATH + "assets/alert.mp3"
      }
    });
    
    var notifi;
    var notifiButtons = {};
    var lastUpdatedTimestamp = 0;
    
    new IhfathMasterSlaveInstance({
      field: "msg",
      
      updateData(data) {
        console.log(data.data, this.master);
        
        var isMaster = this.master;
        var json     = data.data;
        
        if (!isMaster && IHFATHMSG_TIMESTAMP < json.timestamp) {
          IHFATHMSG_TIMESTAMP = json.timestamp;
        }
        
        if (IHFATHMSG_TIMESTAMP > json.timestamp || lastUpdatedTimestamp === IHFATHMSG_TIMESTAMP) {
          return false;
        }
        
        lastUpdatedTimestamp = IHFATHMSG_TIMESTAMP;
        
        // Check if any new messages
        if (!json.count.length)
          return false;
        
        // Play sound notification
        notifiSound.currentTime = 0;
        notifiSound.play();
        
        if (json.staff) {
          var msgCount = json.count;
          var msgStr   = msgCount.length + " new messages: ";
          var basePath = (isDev ? "/drupal" : "") + "/ihfath/messages/";
          
          for (var i=0; i<msgCount.length; i++) {
            var mid = msgCount[i].mid;
            
            msgStr += `[ ${basePath}${mid}][b]${mid}[/button][/anchor] `;
          }
          
          notifi         = lces.new("notification", msgStr, null, "BL");
          notifi.visible = true;
          
          // Messagebox
          var msgBox = jSh("#ihfathmsg-new-link");
          
          if (msgBox) {
            var newCount = parseInt(msgBox.getAttribute("data-msg-count") || 0) + json.count.length;
            
            msgBox.classList.add("ihfathmsg-staff-count-pulse");
            msgBox.setAttribute("data-msg-count", newCount);
          }
        } else {
          var msgCount = json.count;
          var allCount = msgCount.length === 1 ? parseInt(msgCount[0].msgCount) : msgCount.reduce((a, b) => parseInt(a.msgCount || a) + parseInt(b.msgCount));
          var basePath = (isDev ? "/drupal" : "") + "/ihfath/messages/";
          
          // for (var i=0; i<msgCount.length; i++) {
          //   var suid = msgCount[i].suid;
          //   var name = msgCount[i].name;
          //   var mcnt = msgCount[i].msgCount;
          //
          //   msgStr += `[ ${basePath}${suid}/view/][b]${name} (${mcnt})[/button][/anchor] `;
          // }
          
          // Notify user with a link
          if (!notifi) {
            var msgStr     = "{#allCount} new message(s) [b][/break]";
            notifi         = lces.new("notification", msgStr, null, "BL");
            notifi.visible = true;
            
            notifi.allCount = 0;
          }
          
          notifi.allCount += allCount;
          
          // Add staff buttons
          for (var i=0; i<msgCount.length; i++) {
            var profAcc  = msgCount[i];
            var profSuid = profAcc.suid;
            var profBtn;
            
            if (!(profBtn = notifiButtons[profSuid])) {
              var profCnt = jSh.c("span", {
                text: profAcc.msgCount
              });
              
              var profAnch = jSh.c("a", {
                prop: {
                  href: `${basePath}${profAcc.suid}/view/`
                },
                
                child: [
                  jSh.c("button", {
                    child: [
                      jSh.t(profAcc.name + " ("),
                      profCnt,
                      jSh.t(")")
                    ]
                  })
                ]
              });
              
              function IhfathTeacherButton() {
                lces.types.component.call(this);
                
                this.anchor = null;
                this.span   = null;
                this.setState("count", 0);
                
                this.addStateListener("count", (count) => {
                  if (this.span)
                    this.span.textContent = count;
                });
              }
              
              jSh.inherit(IhfathTeacherButton, lces.types.component);
              
              profBtn = notifiButtons[profSuid] = jSh.extendObj(new IhfathTeacherButton(), {
                anchor: profAnch,
                span: profCnt,
                count: parseInt(profAcc.msgCount)
              });
              
              notifi.appendChild(profAnch);
            } else {
              profBtn.count += parseInt(profAcc.msgCount);
            }
          }
          
          // Update staffview
          var staffView = jSh("#ihfathmsg-admin-staff-cards");
          
          if (staffView) {
            for (var i=0; i<msgCount.length; i++) {
              var count = msgCount[i];
              var ctDiv = jSh(".ihfathmsg-staff-count-" + count.suid)[0];
              
              if (ctDiv) {
                ctDiv.classList.remove("ihfathmsg-staff-count-nil");
                ctDiv.classList.add("ihfathmsg-staff-count-pulse");
                
                var newCount = parseInt(ctDiv.getAttribute("data-staff-count")) + parseInt(count.msgCount);
                
                ctDiv.setAttribute("data-staff-count", newCount);
                ctDiv.getChild(0).textContent = newCount;
              }
            }
          }
        }
      },
      
      poll() {
        var that = this;
        
        // Check for new messages AJAX
        var req = new lcRequest({
          uri: (isDev ? "/drupal" : "") + "/ihfath/ajax/messages/latest/" + IHFATHMSG_TIMESTAMP,
          success() {
            var json = jSh.parseJSON(this.responseText);
            
            if (!json.error && json.timestamp) {
              IHFATHMSG_TIMESTAMP = json.timestamp;
            
              that.setCurStateValue({
                timestamp: IHFATHMSG_TIMESTAMP,
                data: json
              });
            }
          }
        });
        
        req.send();
      }
    });
  }
  
  ihfathmsgUpdates();
  
  // Expandable writing form
  var expandableForms = jSh(".ihfathmsg-form-expandable");
  
  if (expandableForms[0]) {
    var expFormsArray = [];
    
    function ExpandableForm(form) {
      var that = this;
      lces.types.component.call(this);
      
      this.setState("expanded", false);
      this.setState("normalHeight", 0);
      that.stateChanging = false;
      this.form = form;
      expFormsArray.push(this);
      var first = true;
      
      // Listeners
      this.addStateListener("expanded", function(expanded) {
        if (expanded) {
          if (first) {
            var formStyle = form.getAttribute("style");
            
            form.style.transition = "unset";
            form.classList.remove("ihfathmsg-form-collapsed");
            
            setTimeout(function() {
              that.recalc(true, false);
              
              form.classList.add("ihfathmsg-form-collapsed");
              form.setAttribute("style", "height: " + form.style.height);
              
              setTimeout(function() {
                form.classList.remove("ihfathmsg-form-collapsed");
                
                setTimeout(function() {
                  that.stateChanging = true;
                  that.normalHeight  = "auto";
                  that.stateChanging = false;
                  
                  first = false;
                }, 360);
              }, 5);
            }, 1);
            
            return;
          } else {
            form.classList.remove("ihfathmsg-form-collapsed");
          }
          
          setTimeout(function() {
            that.stateChanging = true;
            that.normalHeight  = "auto";
            that.stateChanging = false;
          }, 360);
        } else {
          that.recalc(true, first);
          
          setTimeout(function() {
            form.classList.add("ihfathmsg-form-collapsed");
          }, 1);
        }
        
        first = false;
      });
      
      this.addStateListener("normalHeight", function(height) {
        form.css({
          height: height + (typeof height === "number" ? "px" : "")
        });
      });
      
      form.addEventListener("click", function(e) {
        if (!that.expanded && !e.target.classList.contains("ihfathmsg-form-collapse-btn-visual")) {
          that.expanded = true;
        }
      });
      
      form.addEventListener("mousedown", function(e) {
        if (!that.expanded)
          e.preventDefault();
      });
      
      // Collapse button
      var collapseBtn = form.jSh(".ihfathmsg-form-collapse-btn")[0];
      
      collapseBtn.addEventListener("click", function() {
        that.expanded = false;
      });
      
      collapseBtn.addEventListener("mousedown", function(e) {
        e.preventDefault();
      });
      
      // Initial setup
      this.recalc();
      setTimeout(function() {
        that.expanded = form.getAttribute("data-ihm-expanded") === "true";
      }, 2);
      
      window.expForm = this;
    }
    
    jSh.inherit(ExpandableForm, lces.types.component);
    jSh.extendObj(ExpandableForm.prototype, {
      recalc(expanded, first) {
        if (this.stateChanging && !expanded) {
          var expanded  = this.expanded;
          this.expanded = true;
        }
        
        this.normalHeight = "auto";
        
        var func = () => {
          var cRect  = this.form.getBoundingClientRect();
          var height = cRect.bottom - cRect.top;
          
          this.normalHeight = height;
          
          if (this.stateChanging && !expanded) {
            this.expanded = expanded;
          }
        };
        
        if (!expanded || first)
          setTimeout(func, 2);
        else
          return func();
      }
    });
    
    for (var i=0; i<expandableForms.length; i++) {
      new ExpandableForm(expandableForms[i]);
    }
    
    var expFormWinTimeout = null;
    window.addEventListener("resize", function() {
      clearTimeout(expFormWinTimeout);
      
      expFormWinTimeout = setTimeout(function() {
        expFormsArray.forEach(function(form) {
          form.recalc();
        });
      }, 150);
    });
  }
  
  IhfathmsgRenewFileInputs();
});

function IhfathmsgRenewFileInputs() {
  // Configurable file inputs
  var fileInputs = jSh(".ihfathmsg-form-files")[0];
  
  if (fileInputs) {
    function FileInputManager(containers) {
      lces.types.component.call(this);
      
      this.files = [];
    }
    
    jSh.inherit(FileInputManager, lces.types.component);
    
    jSh.extendObj(FileInputManager.prototype, {
      add() {
        var files = this.files;
        
        for (var i=0; i<files.length; i++) {
          if (!files[i].visible) {
            files[i].visible = true;
            break;
          }
        }
        
        this.reindex();
      },
      
      remove(file) {
        file.visible = false;
        
        this.reindex();
      },
      
      reindex() {
        var files    = this.files;
        var visFiles = [];
        var invFiles = [];
        var newFiles;
        
        for (var i=0; i<files.length; i++) {
          var file = files[i];
          
          if (file.visible)
            visFiles.push(file);
          else
            invFiles.push(file);
        }
        
        for (var i=0; i<visFiles.length; i++) {
          visFiles[i].addable = i === visFiles.length - 1 && i !== 4;
        }
        
        newFiles = visFiles.concat(invFiles);
        
        for (var i=0; i<newFiles.length; i++) {
          var file = newFiles[i];
          
          file.num = i + 1;
          file.removable = true;
        }
        
        this.files = newFiles;
        
        if (visFiles.length === 1) {
          visFiles[0].removable = false;
        }
      }
    });
    
    function IhfathFileInput(num, parent, manager, visible) {
      var that = this;
      lces.types.component.call(this);
      
      // Set initial num state
      this.setState("num", num);
      this.setState("visible", true);
      this.setState("addable", true);
      this.setState("removable", true);
      
      this.manager = manager;
      manager.files.push(this);
      
      // Make DOM elements
      var input = jSh.c("input", {
        sel: "#edit-file" + num + ".form-file",
        prop: {
          name: "files[file" + num + "]",
          size: 60,
          type: "file"
        }
      });
      
      var btnAdd = jSh.c("div", {
        sel: ".ihfathmsg-file-attach-btn.ihfathmsg-add-file",
        text: "+",
        prop: {
          title: "Add a file"
        },
        events: {
          click() {
            that.manager.add();
          },
          
          mousedown(e) {
            e.preventDefault();
          }
        }
      });
      
      var btnRemove = jSh.c("div", {
        sel: ".ihfathmsg-file-attach-btn.ihfathmsg-remove-file.ihfathmsg-btn-first",
        text: "×",
        prop: {
          title: "Remove file"
        },
        events: {
          click() {
            that.manager.remove(that);
          },
          
          mousedown(e) {
            e.preventDefault();
          }
        }
      });
      
      var cont = jSh.d({
        sel: ".form-item.form-type-file.form-item-files-file" + num,
        child: [input, btnRemove, btnAdd]
      });
      
      this.cont = cont;
      parent.appendChild(cont);
      var lcfile = lces.new("fileinput", input);
      
      lcfile.element.css({
        display: "inline-block"
      });
      
      // Listeners
      this.addStateListener("num", function(num) {
        var oldNum = this.oldStateStatus;
        
        input.id   = "edit-file" + num;
        input.name = "files[file" + num + "]";
        
        cont.classList.remove("form-item-files-file" + oldNum);
        cont.classList.add("form-item-files-file" + num);
      });
      
      this.addStateListener("visible", function(visible) {
        if (visible) {
          cont.classList.remove("ihfathmsg-file-input-hidden");
        } else {
          cont.classList.add("ihfathmsg-file-input-hidden");
          
          // Reset
          lcfile.reset();
          
          // Push to the back if not already at the back, conditional to prevent waste of DOM mutations
          if (that.num !== 5) {
            parent.removeChild(cont);
            parent.appendChild(cont);
            manager.files.push(manager.files.splice(that.num - 1, 1)[0]);
          }
        }
      });
      
      this.addStateListener("addable", function(addable) {
        if (addable) {
          btnAdd.css({
            display: "inline-block"
          });
        } else {
          btnAdd.css({
            display: "none"
          });
        }
        
        if (that.removable) {
          btnAdd.classList.remove("ihfathmsg-btn-first");
        } else {
          btnAdd.classList.add("ihfathmsg-btn-first");
        }
      });
      
      this.addStateListener("removable", function(removable) {
        if (removable) {
          btnRemove.css({
            display: "inline-block"
          });
        } else {
          btnRemove.css({
            display: "none"
          });
        }
        
        if (that.addable && removable) {
          btnAdd.classList.remove("ihfathmsg-btn-first");
        } else {
          btnAdd.classList.add("ihfathmsg-btn-first");
        }
      });
      
      this.visible = visible;
      this.addable = visible;
    }
    
    jSh.inherit(IhfathFileInput, lces.types.component);
    
    // Remove all fileinputs
    fileInputs.removeChild(jSh.toArr(fileInputs.childNodes));
    var inputManager = new FileInputManager();
    
    for (var i=1; i<6; i++) {
      var file = new IhfathFileInput(i, fileInputs, inputManager, i === 1);
    }
    
    // Remove removable button
    inputManager.reindex();
  }
}
