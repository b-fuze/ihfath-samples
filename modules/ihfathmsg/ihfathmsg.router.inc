<?php

// Ihfath Messaging module code
//
// Prefix (For autocomplete): ihfathmsg_ _ihfathmsg_

// TODO: Consolidate this and the one from the Ihfath Lesson module
function _ihfathmsg_is_concerned_party($mid = NULL) {
  global $user;
  $uid       = $user->uid;
  $user_role = ihfathcore_get_role();
  
  if (!isset($user_role)) {
    // No appropriate roles found
    return $user_role;
  }
  
  // Check if message is directed from/to this person
  if (isset($mid)) {
    if (!is_numeric($mid)) {
      // Invalid mid, error
      return NULL;
    }
    
    $msg = Ihfathmsg\Get::singleFrugal($mid);
    
    // Check message exists
    if (!$msg) {
      // Message doesn't exist anyways
      return NULL;
    }
    
    // Admin role is always a concerned party
    if ($user_role === 1) {
      return $user_role;
    }
    
    // Check if matches sender or receiver field
    if ($msg->suid === $uid || $msg->ruid === $uid) {
      // Valid party
      return $user_role;
    } else {
      // Invalid, unconcerned party
      return NULL;
    }
  }
  
  return $user_role;
}

function _ihfathmsg_list_messages($role, $uid = NULL, $sent = FALSE) {
  switch ($role) {
    case 1:
      if ($sent) {
        // FIXME: This is the WRONG function and/or the WRONG
        // way to pull this off, ples fix
        return ihfathmsg_staff_view(TRUE, $role);
      } else {
        return ihfathmsg_admin_view_staffmember($uid);
      }
      break;
    case 2:
    case 3:
      if ($sent) {
        return ihfathmsg_staff_view(TRUE, $role);
      } else {
        return ihfathmsg_staff_view(FALSE, $role);
      }
      break;
  }
}

function _ihfathmsg_view_message($mid, $role, $sent = FALSE) {
  switch ($role) {
    case 1:
      if ($sent) {
        return ihfathmsg_admin_view_staffmember_msg($mid, TRUE, $role);
      } else {
        return ihfathmsg_admin_view_staffmember_msg($mid, FALSE, $role);
      }
      break;
    case 2:
    case 3:
      if ($sent) {
        return ihfathmsg_staff_view_msg($mid, TRUE, $role);
      } else {
        return ihfathmsg_staff_view_msg($mid, FALSE, $role);
      }
      break;
  }
}

/**
 * ROUTERS
 */

function _ihfathmsg_main_router() {
  $role = _ihfathmsg_is_concerned_party();
  
  if (isset($role)) {
    switch ($role) {
      case 1:
        return ihfathmsg_admin_main_view(2);
        break;
      case 2:
      case 3:
        return ihfathmsg_staff_view(FALSE, $role);
        break;
    }
  } else {
    drupal_access_denied();
  }
}

function _ihfathmsg_msg_router($path_first = NULL, $path_second = NULL) {
  if (!isset($path_first) && !isset($path_second)) {
    // Just a base path
    return _ihfathmsg_main_router();
  }
  
  $path = array($path_first, $path_second);
  
  // Check if it's a faulty path (see Possible variations)
  $path_count = count($path);
  
  if ($path_count > 3 || $path_count === 0 || isset($path[2]) && $path[2] ||
      !is_numeric($path[0]) && !($path[0] === "sent" || $path[0] === "students") ||
      isset($path[1]) && !is_numeric($path[1])) {
    drupal_not_found();
    return;
  }
  
  if (isset($path[1]) && $path[1]) {
    // Use second portion
    $mid_uid = $second = (int) $path[1];
  } else {
    // No second portion
    $mid_uid = $path[0];
    $second  = NULL;
  }
  
  $user_role = ihfathcore_get_role();
  $mid  = isset($second) ? $second : ($user_role == 1 ? NULL : $mid_uid);
  $mid  = is_numeric($mid)? (int) $mid : NULL; // NULL === "/sent" page
  $role = _ihfathmsg_is_concerned_party($mid);
  
  if (!$role) {
    drupal_not_found();
    return;
  }
  
  $sent = FALSE;
  if ($path[0] === "sent") {
    $sent = TRUE;
  }
  
  // Respond according to role
  switch ($role) {
    case 1:
      if ($sent) {
        if ($mid_uid === "sent") {
          // List sent messages
          return _ihfathmsg_list_messages($role, NULL, TRUE);
        } else {
          // View a sent message
          return _ihfathmsg_view_message($mid_uid, $role, TRUE);
        }
      } else {
        if (isset($second)) {
          // View a single message by a teacher/student
          return _ihfathmsg_view_message($mid_uid, $role, FALSE);
        } else {
          if ($mid_uid === "students") {
            return ihfathmsg_admin_main_view(3 /* 3 = students, role */);
          } else {
            // View list of messages by a teacher/student
            return _ihfathmsg_list_messages($role, $mid_uid, FALSE);
          }
        }
      }
      break;
    case 2:
    case 3:
      if ($sent) {
        if ($mid_uid === "sent") {
          // List sent messages
          return _ihfathmsg_list_messages($role, NULL, TRUE);
        } else {
          if ($mid_uid === "students") {
            // Not a valid URL for a student
            drupal_not_found();
            return;
          }
          
          // View a sent message
          return _ihfathmsg_view_message($mid_uid, $role, TRUE);
        }
      } else {
        if (isset($second)) {
          // This isn't a valid message page
          drupal_not_found();
          return;
        } else {
          // View message
          return _ihfathmsg_view_message($mid_uid, $role, FALSE);
        }
      }
      break;
  }
}
