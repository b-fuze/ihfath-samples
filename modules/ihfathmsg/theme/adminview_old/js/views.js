var Teachers = require("views/teachers");
var Students = require("views/students");

function Views(manager) {
  Teachers(manager);
  Students(manager);
}

module.exports = Views;
