// Get deps
var Views = require("views");
var Poll = require("poll");
var StateManager = require("misc/state");
var Controls = require("misc/controls");

function IhfathAdminView() {
  var manager = StateManager();
  
  // Make poll
  Poll();
  
  // Make controls
  Controls(manager);
  
  // Make views
  var views = Views(manager);
  
  // Tell the state manager about the viewss
  manager.views = views;
}

window.IhfathAdminView = IhfathAdminView;
