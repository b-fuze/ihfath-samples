<?php
global $base_url;

if (isset($form["#form"])) {
  $include_form = &$form["#form"];
} else {
  $include_form = NULL;
}

$role           = $form["#role"];
$path_component = isset($form["#path"]) ? $form["#path"] : "";
$sent           = $form["#sent"];
$messages       = ihfathmsg_get_message_list($form["#uid"], $sent, $form["#role"]);
$no_messages    = t($role === 1 ? "There are no messages from this user" : "You have no messages");
$base_path      = $base_url . "/ihfath/messages" . ($sent ? "/sent" : "") . $path_component;

?>

<!-- Professor/Student Nav -->
<?php
$navblock = NULL;
$title    = drupal_set_title();

switch ($role) {
  case 1:
    $navblock = module_invoke("mod_background_page", "block_view", "mod_block_background");
    break;
  case 2:
    $navblock = module_invoke("ihfathui", "block_view", "ihfathblk_prof_nav");
    break;
  case 3:
    $navblock = module_invoke("ihfathui", "block_view", "ihfathblk_studentnav");
    break;
}

if (isset($navblock)) {
  echo render($navblock);
  
  // Preserve title
  drupal_set_title($title);
} ?>

<!-- Message list -->
<div class="section ihfath-small-section">
  <div class="container ihfath-wrap">
    <?php
    if ($include_form) {
      $include_form_render = array(
        "#theme" => "ihfathmsg_write_message"
      ) + $include_form;
      
      echo drupal_render($include_form_render); ?>
      
      <hr>
      
      <?php
      if ($role !== 1) { ?>
        
        <div class="ihfathmsg-list-tabs">
          <i>
            <a href="<?php echo $base_url . "/ihfath/messages" ?>" <?php echo $sent ? "" : 'class="main-bg"'; ?>><?php echo t("Received"); ?></a>
            <a href="<?php echo $base_url . "/ihfath/messages/sent" ?>" <?php echo $sent ? 'class="main-bg"' : ""; ?>><?php echo t("Sent"); ?></a>
          </i>
        </div>
        
    <?php
      }
    } ?>
    
    <table>
      <tbody>
        <tr>
          <th>
            <?php echo t("Subject"); ?>
          </th>
          <th class="ihfathmsg-date-col">
            <?php echo t("Date"); ?>
          </th>
        </tr>
        
        <?php
        $msg_count = 0;
        
        foreach ($messages as $msg) {
          $msg = $msg->msg;
          $msg_count++; ?>
          
          <tr>
            <td>
              <a href="<?php echo $base_path . "/" . $msg->mid ?>">
                
                <?php
                if ($msg->category == Ihfathmsg\Category::Report) { ?>
                  
                  <div class="ihfath-msg-label main-bg ihfath-report">
                    <?php echo t("Report") ?>
                  </div>
                  
                <?php
                }
                
                if ($msg->subject) {
                  echo $msg->subject;
                } else {
                  echo check_plain(implode(" ", array_slice(preg_split('/\s+/', $msg->body), 0, 5)) . "...");
                }
                
                if (!$sent && !$msg->viewed) {
                  echo ' <span class="ihfath-messages-unread">' . t("new") . '</span>';
                } ?>
                
              </a>
            </td>
            <td>
              <?php
              $date = explode(" ", date("j M Y", (int) $msg->timestamp));
              echo t("@date @abbrv-month @year", array(
                "@date"        => $date[0],
                "@abbrv-month" => t($date[1]),
                "@year"        => $date[2]
              ), array(
                "context" => "Ihfath Message List"
              )); ?>
            </td>
          </tr>
          
        <?php
        }
        
        if (!$msg_count) { ?>
          
          <tr>
            <td colspan="2" class="ihfathmsg-no-messages"><?php echo $no_messages; ?></td>
          </tr>
          
        <?php
        } ?>
      </tbody>
    </table>
    
    <!-- Pager -->
    <div class="ihfath-message-pager">
      <?php echo theme("pager"); ?>
    </div>
  </div>
</div>

<style media="screen">
  .ihfathmsg-list-tabs {
    text-align: center;
    margin: 20px 0px;
  }
  
  .container.ihfath-wrap .ihfathmsg-list-tabs i {
    position: relative;
    z-index: 10;
    display: inline-block;
    vertical-align: middle;
    overflow: hidden;
    padding: 0px 0px;
    width: auto;
    
    background: #F2F2F2;
    border-radius: 45px;
  }
  
  .ihfathmsg-list-tabs i a {
    display: inline-block;
    vertical-align: top;
    padding: 0px 15px;
    height: 45px;
    width: 160px;
    
    font-style: normal;
    text-align: center;
    line-height: 45px;
  }
  
  .ihfathmsg-list-tabs i a.main-bg {
    font-weight: bold;
  }
  
  .ihfathmsg-no-messages {
    padding: 20px;
    
    text-align: center;
    font-size: 1.25em;
    font-weight: bold;
  }
  
  .ihfath-messages-unread {
    font-weight: bold;
    color: red;
  }
  
  .ihfathmsg-date-col {
    width: 20%;
  }
  
  .ihfath-small-section {
    padding: 50px 0px;
  }
  
  .ihfath-msg-label {
    display: inline-block;
    padding: 0px 5px;
    height: 18px;
    vertical-align: baseline;
    margin-right: 5px;
    
    text-transform: uppercase;
    color: #fff;
    line-height: 18px;
    font-size: 12px;
    border-radius: 2px;
  }
</style>
