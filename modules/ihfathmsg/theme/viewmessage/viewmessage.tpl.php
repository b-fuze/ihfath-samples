<?php

global $user;
global $base_url;
$sent         = $form["#sent"];
$role         = $form["#role"];
$go_back      = isset($form["#go_back"]) ? $form["#go_back"] : NULL;
$message_full = Ihfathmsg\Get::single($form["#mid"], !$sent);
$message      = $message_full->msg;
$attachments  = $message_full->attachments;
$sent_user    = user_load($message->suid);

// Set $sent for admins
if ($role === 1 && ((int) $message->userrole) === 1) {
  $sent = TRUE;
}

?>
<!-- Professor/Student Nav -->
<?php
$navblock = NULL;
$title    = drupal_set_title();

switch ($role) {
  case 1:
    $navblock = module_invoke("mod_background_page", "block_view", "mod_block_background");
    break;
  case 2:
    $navblock = module_invoke("ihfathui", "block_view", "ihfathblk_prof_nav");
    break;
  case 3:
    $navblock = module_invoke("ihfathui", "block_view", "ihfathblk_studentnav");
    break;
}

if (isset($navblock)) {
  echo render($navblock);
  
  // Preserve title
  drupal_set_title($title);
} ?>

<!-- Message view -->
<div class="section ihfath-small-section">
  <div class="container ihfath-wrap">
    <div class="ihfathmsg-messages">
      <div class="ihfathmsg-msg-container" data-im-mid="<?php echo $message->mid; ?>">
        <?php
        if ($message->category == Ihfathmsg\Category::Report) { ?>
          
          <div class="ihfath-msg-header-label">
            <?php echo t("Report") ?>
          </div>
          <div class="ihfath-msg-reason main-color">
            <?php echo t($message_full->reason_text) ?>
            <a
              href="<?php echo "{$base_url}/ihfath/lesson/{$message_full->lesson_id}" ?>"
              target="_blank"
              title="<?php echo t("Go to lesson's page") ?>"
              style="vertical-align: middle; color: #555;">
              <i class="fa fa-external-link-square"></i>
            </a>
          </div>
          
        <?php
        } else { ?>
          
          <h2 class="ihfathmsg-msg-subject main-color">
            <?php echo $message->subject ? htmlspecialchars($message->subject) : "<i>" . t("No subject") . "</i>"; ?>
          </h2>
          
        <?php
        }
        
        $expanded_time = $message->category == Ihfathmsg\Category::Report ? "ihfath-time-expand" : ""
        ?>
        <h2 class="ihfathmsg-msg-time <?php echo $expanded_time ?>">
          <?php echo date("j M Y ", (int) $message->timestamp) . t("at") . date(" g:i a", (int) $message->timestamp); ?>
          <span class="main-color"><?php echo t("from @user", array("@user" => $sent_user->name)) ?></span>
        </h2>
        <p class="ihfathmsg-msg-body"><?php echo ihfathmsg_format_msg($message->body); ?></p>
        
        <?php
        if ($message_full->attachments) {
          $attach_base_path = $base_url . "/ihfath/attachment/"; ?>
          
          <div class="ihfathmsg-msg-file-attachments">
            
            <?php
            foreach ($attachments as $attach_obj) { ?>
              
              <a href="<?php echo $attach_base_path . $attach_obj->file_hash . "/" . $attach_obj->file_name; ?>" class="ihfathmsg-msg-file-anchor">
                <span class="ihfathmsg-msg-file-name">
                  <?php echo htmlspecialchars($attach_obj->file_name); ?>
                </span>
                <span class="ihfathmsg-msg-file-size">
                  <?php echo format_size($attach_obj->file_size); ?>
                </span>
              </a>
              
            <?php
            } ?>
          </div>
          
        <?php
        } ?>
      </div>
    </div>
    
    <hr>
    
    <?php
    // Include response form
    if (!$sent) { ?>
      
      <div class="ihfath-response-form">
        <div class="uppercase">
          <?php echo t("Write a response"); ?>
        </div>
        
        <?php
        $include_form_render = array(
          "#theme"      => "ihfathmsg_write_message",
          "#uid"        => $user->uid,
          "#expandable" => FALSE,
          "#func"       => $role === 1 ? "ihfathmsg_admin_write_msg_form" : "ihfathmsg_staff_write_msg_form"
        );
        
        if (isset($go_back)) {
          $include_form_render["#go_back"] = $go_back;
        }
        
        echo drupal_render($include_form_render); ?>
        
      </div>
      
    <?php
  } else if (isset($go_back)) { ?>
    
    <a href="<?php echo $go_back; ?>" class="btn btn-lg btn-grey shape form-submit new-angle"><?php
      echo str_replace(array(
          "%left-arrow%",
          "%right-arrow%"
        ), array(
          '<i class="fa fa-long-arrow-left"></i>',
          '<i class="fa fa-long-arrow-right"></i>'
        ), t("%left-arrow% Go back"));
    ?></a>
    
  <?php
  } ?>
  </div>
</div>

<style>
  .ihfathmsg-msg-body {
    font-size: 1.3em !important;
  }
  
  .ihfath-response-form .uppercase {
    text-align: center;
    font-size: 18px;
    opacity: 0.35;
  }
  
  .ihfath-response-form {
    padding: 25px;
    background: #F7F7F7;
  }
</style>
