<?php

$uid        = isset($form["#uid"]) ? $form["#uid"] : NULL;
$mrid       = isset($form["#mrid"]) ? $form["#mrid"] : NULL;
$expandable = isset($form["#expandable"]) ? $form["#expandable"] : FALSE;
$go_back    = isset($form["#go_back"]) ? $form["#go_back"] : NULL;
$msg_form   = drupal_get_form($form["#func"], $uid, $mrid);

if ($expandable) {
  $expandable_class = "ihfathmsg-form-expandable ihfathmsg-form-collapsed";
  $expandable_attr  = 'data-ihm-expanded="false"';
} else {
  $expandable_class = "";
  $expandable_attr  = "";
}

if ($go_back) {
  $msg_form["msg_form"]["buttons"]["go_back"]["#path"] = $go_back;
} else {
  unset($msg_form["msg_form"]["buttons"]["go_back"]);
}

?>

<div class="ihfathmsg-message-form <?php echo $expandable_class; ?>" <?php echo $expandable_attr ?>>
  <?php if ($expandable) { ?>
    
    <div class="ihfathmsg-expand-text">
      <?php echo str_replace(array(
        "%pencil%"
      ), array(
        '<i class="fa fa-pencil"></i>'
      ), t("%pencil% Write a new message")); ?>
    </div>
    
    <div class="ihfathmsg-form-collapse-btn ihfathmsg-form-collapse-btn-visual" title="<?php echo t("Collapse form"); ?>">
      <i class="fa fa-caret-up ihfathmsg-form-collapse-btn-visual"></i>
    </div>
    
  <?php
  } ?>
  
  <?php echo render($msg_form); ?>
</div>

<style media="screen">
  .ihfathmsg-form-msg-body {
    border-bottom-left-radius: 0px;
  }
  
  /* Fix LCES file */
  .form-item div.lces-file {
    box-sizing: content-box;
    vertical-align: top;
  }
  
  .ihfathmsg-message-form {
    display: inline-block;
    width: 100%;
    padding-bottom: 15px;
  }
  
  div.form-item {
    padding-bottom: 20px;
  }
  
  div.form-item label {
    font-size: 15px;
  }
  
  .ihfathmsg-expand-text {
    pointer-events: none;
  }
  
  .ihfath-write-message-buttons {
    display: inline-block;
    vertical-align: bottom;
    float: right;
  }
  
  #edit-files {
    display: inline-block;
    vertical-align: bottom;
    float: left;
  }
</style>
