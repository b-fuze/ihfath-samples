<?php

// Ihfath Messaging ui code
//
// Prefix (For autocomplete): ihfathmsg_ _ihfathmsg_

function _ihfathmsg_user_unread() {
  global $user;
  $count_query = db_select("ihfath_msg", "im");

  $count_query->addExpression("COUNT(*)", "msgCount");
  $count_query->condition("im.ruid", $user->uid, "=");
  $count_query->condition("im.viewed", 0, "=");

  return $count_query->execute()->fetchField();
}

// ===========================================
//           CONFIG VIEW FUNCTIONS
// ===========================================

function ihfathmsg_config($form, &$form_state) {
  $form = array();

  $form["ihfathmsg_from_email_addr"] = array(
    "#type"          => "textfield",
    "#title"         => t("\"From:\" field in the automatic emails"),
    "#default_value" => variable_get("ihfathmsg_from_email_addr", "noreply@ihfath.com")
  );

  $form["ihfathmsg_main_email"] = array(
    "#type"          => "textarea",
    "#title"         => t("Forward to emails"),
    "#default_value" => variable_get("ihfathmsg_main_email", ""),
    "#description"   => t("List of emails to forward messages from the professors, separated by newlines."),
    "#cols"          => 1,
    "#rows"          => 5
  );

  return system_settings_form($form);
}

function ihfathmsg_config_validate($form, &$form_state) {
  $from_default = "noreply@ihfath.com";
  $from_email   = trim($form_state["values"]["ihfathmsg_from_email_addr"]);

  if (!valid_email_address($from_email)) {
    $form_state["values"]["ihfathmsg_from_email_addr"] = $from_default;
    variable_set("ihfathmsg_from_email_addr", $from_default);

    drupal_set_message(t("\":email\" is an invalid email address. Reverted to default \"" . $from_default . "\"", array(":email" => $from_email)), "error");
  }

  // Validate email list
  $emails           = explode("\n", $form_state["values"]["ihfathmsg_main_email"]);
  $preserved_emails = array();

  // Check each entered email as valid
  foreach ($emails as $email) {
    if (valid_email_address($cur_email = trim($email))) {
      $preserved_emails[] = $cur_email;
    }
  }

  // Save valid emails
  $preserved_emails_str = implode("\n", $preserved_emails);

  variable_set("ihfathmsg_main_email", $preserved_emails_str);
  $form_state["values"]["ihfathmsg_main_email"] = $preserved_emails_str;

  // Notify user
  drupal_set_message(t(count($preserved_emails) . " email(s) have been saved."), "status");
}

// ===========================================
//          GENERIC VIEW FUNCTIONS
// ===========================================

function ihfathmsg_upload_msg_attachments_form() {
  $form = array(
    // Message form elements
    "msg_form" => array(
      "files" => array(
        "#type" => "container",
        "#attributes" => array(
          "class" => array("ihfathmsg-form-files")
        ),

        "file1" => array(
          "#type" => "file"
        ),

        "file2" => array(
          "#type" => "file"
        ),

        "file3" => array(
          "#type" => "file"
        ),

        "file4" => array(
          "#type" => "file"
        ),

        "file5" => array(
          "#type" => "file"
        ),
      ),

      "buttons" => array(
        "#type"       => "container",
        "#attributes" => array(
          "class" => array("ihfath-write-message-buttons")
        ),

        "submit" => array(
          "#type"  => "submit",
          "#value" => t("Upload")
        ),
      ),
    ),
  );

  return $form;
}

function ihfathmsg_write_msg_form() {
  $form = array(
    // Message form elements
    "msg_form" => array(
      "subject" => array(
        "#type"       => "textfield",
        "#title"      => t("Subject"),
        "#attributes" => array(
          "dir"         => array("auto"),
          "placeholder" => t("Message subject...")
        )
      ),

      "body" => array(
        "#type"       => "textarea",
        "#title"      => t("Message"),
        "#required"   => TRUE,
        "#attributes" => array(
          "class"       => array("ihfathmsg-form-msg-body"),
          "dir"         => array("auto"),
          "placeholder" => t("Message body...")
        )
      ),

      "files" => array(
        "#type" => "container",
        "#attributes" => array(
          "class" => array("ihfathmsg-form-files")
        ),

        "file1" => array(
          "#type" => "file"
        ),

        "file2" => array(
          "#type" => "file"
        ),

        "file3" => array(
          "#type" => "file"
        ),

        "file4" => array(
          "#type" => "file"
        ),

        "file5" => array(
          "#type" => "file"
        ),
      ),

      "buttons" => array(
        "#type"       => "container",
        "#attributes" => array(
          "class" => array("ihfath-write-message-buttons")
        ),

        "submit" => array(
          "#type"  => "submit",
          "#value" => t("Submit")
        ),

        "go_back" => array(
          "#theme" => "link",
          "#text"  => str_replace(array(
              "%left-arrow%",
              "%right-arrow%"
            ), array(
              '<i class="fa fa-long-arrow-left"></i>',
              '<i class="fa fa-long-arrow-right"></i>'
            ), t("%left-arrow% Go back")
          ),
          "#path"    => "",
          "#options" => array(
            "attributes" => array(
              "class" => array("btn", "btn-lg", "btn-grey", "shape", "form-submit", "new-angle")
            ),

            "html" => TRUE
          )
        )
      )
    )
  );

  return $form;
}

function ihfathmsg_get_message_list($user_uid, $sent = NULL, $role) {
  $categories = array(
    Ihfathmsg\Category::Basic,
    Ihfathmsg\Category::Report,
    Ihfathmsg\Category::LessonNote,
  );

  if ($role === 1) {
    // Display messages to the admin
    $messages = Ihfathmsg\Get::bulk(NULL, $categories, NULL, $user_uid, !$sent, TRUE);
  } else {
    // Display messages to a normal user
    $messages = Ihfathmsg\Get::bulk(NULL, $categories, NULL, $user_uid, $sent, TRUE);
  }

  return $messages;
}

// TODO: This runs for each request, find some way to cache it
function ihfathmsg_format_msg($msg) {
  preg_match_all('/([^a-z\d]|^)((?:http|https):\/\/(?:[a-z\d\-]+\.)+[a-z\d\-]+(?:\/(?:[^\s\?]|\?(?!(?:\s|$)))*)?)(\??)/'.'i', $msg, $matches, PREG_OFFSET_CAPTURE);

  $all_matches = $matches;
  $matches     = $matches[0];
  $new_str     = "";
  $test_len    = strlen($msg);
  $last_index  = $test_len - 1;

  for ($i=count($matches)-1; $i>=0; $i--) {
    $cur_char      = $all_matches[1][$i][0];
    $cur_endchar   = $all_matches[3][$i][0];
    $cur_link      = htmlspecialchars($all_matches[2][$i][0]);
    $cur_str       = $matches[$i][0];
    $cur_index     = $matches[$i][1];
    $cur_end_index = strlen($cur_str) + $cur_index;
    $after_text    = "";

    if ($cur_end_index !== $test_len) {
      $after_text = htmlspecialchars(mb_substr($msg, $cur_end_index, $last_index - $cur_end_index, "UTF-8"));
    }

    $new_str    = htmlspecialchars($cur_char) . '<a href="' . $cur_link . '">' . $cur_link . '</a>' . $cur_endchar . $after_text . $new_str;
    $last_index = $cur_index;
  }

  if ($last_index !== 0) {
    $after_text = htmlspecialchars(mb_substr($msg, 0, $last_index + 1, "UTF-8"));
    $new_str    = $after_text . $new_str;
  }

  // Add <p>'s and <br>'s
  $new_str = preg_replace('/(?:\n|^)([^\n]+)(?:\n|$)/', '<p class="ihfathmsg-msg-body">$1</p>', $new_str);
  $new_str = preg_replace('/\n/', '<br>', $new_str);

  return $new_str;
}

function ihfathmsg_get_message_attachments($mid) {
  $attach_query = db_select("ihfath_msg_attach", "ima");
  $attach_query->fields("ima", array("aid", "file_name", "file_size"))
               ->condition("ima.mid", $mid, "=");
  return $attach_query->execute()->fetchAllAssoc("aid");
}

// ===========================================
//            ADMIN VIEW FUNCTIONS
// ===========================================

function ihfathmsg_admin_get_users($role) {
  $user_result = Ihfathmsg\Get::count(FALSE, array(
    Ihfathmsg\Category::Basic,
    Ihfathmsg\Category::Report,
    Ihfathmsg\Category::LessonNote,
    // Ihfathmsg\Category::WeeklySchedule, // TODO: Should this be checked here? (besides the fact it's only for profs)
  ), $role, NULL, TRUE, TRUE);
  $users = user_load_multiple(array_keys($user_result));

  foreach ($user_result as $user) {
    $users[$user->uid]->msg_count = $user->msg_count ? $user->msg_count : 0;
  }

  return $users;
}

function ihfathmsg_admin_main_view($list_role) {
  return array(
    "#theme"     => "ihfathmsg_admin_view",
    "#list_role" => $list_role
  );
}
