// Cellidi (CELLI DIsplay, i after Cell is for stylistic purposes)
//
// Cellidi is library for displaying interactive tables (read: calendars, weekly
// schedules, etc) via an abstract interface that at its core does all the heavy
// lifting managing the state of your CellDisplay
//
// CellDisplay
//
// CellDisplay is the overarching main component of Cellidi, an umbrella term
// for the whole lower-level framework

// headerFunc implementation
//
// position: position of cell, (top/bottom header: left to right, right/left header: top to bottom) (zero indexed)
//
// virtualPosition: Actual data position (zero indexed, not-including display adjustments offsets)
//
// orientation: Orientation of the header cell, "top", "bottom", "right", or "left"
//
// return: Main header cell DOM
function headerFunc(position, virtualPosition, orientation, cellDisplay) {
  // Implementation...
}

var options = {
  // The element in which to place the new CellDisplay
  element: jSh("#calendar"),
  
  // (optional) The element on which to add focus events
  focus: null,
  
  // Width and height (in cells) of the CellDisplay
  dimensions: [
    x,
    y
  ],
  
  // (optional) Virtual offset, e.g. to make another arbitrary day the first day of the
  // week, to make hours start from some arbitrary hour, etc
  offsetX: 0,
  offsetY: 0,
  
  // (optional) Whether user can select any cells to mutate them
  // Requires: onCellSelectChange
  canSelect: false,
  
  // (optional) The max selectable cells at a time, 0: Infinity, anything else for a set limit
  maxSelect: 0,
  
  // (optional) Whether CellDisplay will record the history of its state, which
  // the user can shift through via Ctrl + Z to Undo, with (Shift) prepended to
  // the combination to go forward (Redo) in history (if any)
  history: true,
  
  // (optional) The maximum history ("future" and past history) recordable at
  // one time
  maxHistory: 60,
  
  // (optional) The headers around the edges of the cell wrapper, e.g. weekdays
  // for a weekly schedule, each orientation is optional
  headers: {
    top: headerFunc, // See: `headerFunc` implementation
    bottom: headerFunc,
    right: headerFunc,
    left: headerFunc
  },
  
  // (optional) the CellDisplay's model, each property converted here into LCES
  // states, can be listened to with a following example model like so:
  //
  //   cellDisplayInstance.model.addStateListener("stateName", function(newValue) { ... });
  //   cellDisplayInstance.model.stateName = "newValue";
  //
  //   model: {
  //     stateName: 10
  //   }
  //
  // Individual cell listener functions (on*) can access it through the `this`
  // context like so:
  //
  //   this.model.addStateListener("stateName", function(newValue) { ... });
  //   this.model.stateName = "newValue";
  model: {
    
  },
  
  // (optional)
  // onCellSelectChange: Triggered on any cell(s) when their selection state changes
  //
  // selected: Array of new Cell Model selected states
  // cellModel: Array of affected Cell Models See: `cellModel` example
  //
  // return: Final result of the `selected` array, i.e. you can change the
  // `selected` state, e.g. to reject selection
  onCellSelectChange: function(selected, cellModel) {
    
  },
  
  // (optional)
  // onCellStateChange: Triggered on any cell(s) when their state changes
  //
  // state: Array of new Cell Model states
  // cellModel: Array of affected Cell Models See: `cellModel` example
  //
  // return: Final result of the `state` array, i.e. you can change the
  // state, e.g. to reject/alter some state change
  onCellStateChange: function(state, cellModel) {
    
  },
  
  // newCell: Callback to generate the DOM, and optionally a data bank
  // for a new cell.
  //
  // The data bank is simply accessible via `cell._data` and contains static
  // properties that can be used to store specific metadata for said cell
  //
  // A dead cell is one that offers no contribution to the actual dataset, it
  // can't be selected, changed, etc
  //
  // return: Object containing cell DOM, and data bank (optional), in the
  // following example format:
  //   {
  //     dom: jSh.d(".some-cell"),
  //
  //     // optional
  //     data: {
  //       randomData: "foo",
  //       someThing: jSh.d(".bar")
  //     },
  //
  //     // optional
  //     deadCell: false
  //   }
  newCell: function(x, y, virtualX, virtualY, cellDisplay) {
    
  },
  
  // (optional)
  // newModal: Callback to create the modal UI for the user to mutate cells
  //
  // `this` context is of the CellDisplay's instance, which contains the
  // following methods:
  //
  //   this.select(selArr)
  //
  //   Description: forcefully select _or_ deselect the specified cells, `selArr`
  //   should be an array of arrays, the inner arrays representing the new state
  //   of a cell, `x` being real `x` (not virtual, same for `y`) and `sel` is
  //   the new selection state like so:
  //
  //     [
  //       [x, y, sel],
  //       [x, y, sel],
  //       ...
  //     ]
  //
  //   this.change(cellArr)
  //
  //   Description: forcefully change the states of specified cells, `cellArr`
  //   should be an array of arrays, the inner arrays representing the new state
  //   of a cell, `x` being real `x` (not virtual, same for `y`) and `state` is
  //   the new cell state like so:
  //
  //     [
  //       [x, y, state],
  //       [x, y, state],
  //       ...
  //     ]
  //
  // return: DOM of modal
  //
  // *** OLD ***
  // return: object holding the DOM and event listeners for the modal like so:
  //
  //   {
  //     dom: modalDOM,
  //     onShowModal: function(cellModel) { ... },
  //     onHideModal: function(curCellModel) { ... }
  //   }
  newModal() {
    // Example implementation
    return {
      dom: jSh.d(".my-modal"),
      
      onShowModal: function(cell) {},
      onHideModal: function(curCell) {}
    };
  }
};

var calender = new CellDisplay(options, data);

// Events
calender.on("activeinput", function(e) {
  // e = {
  //   dom: DOM of target,
  //   cell: cellModel | null,
  //   header: boolean,
  //   event: DOMEvent
  // }
  
  // Implementation...
});

calender.on("blurinput", function(e) {
  // e = {
  //   event: DOMEvent
  // }
  
  // Implementation...
});

calender.on("newstate", function() {
  // Implementation...
});

// If `canSelect` option flag is true
calender.on("newselection", function(e) {
  // e = {
  //   selection: selectionTable
  // }
  
  // Implementation...
});

calender.addStateListener("hoveredCell", function(cell) {
  // Implementation...
});

calender.addStateListener("hoveredHeaderCell", function(cell) {
  // Implementation...
});

// If `canSelect` option flag is true
calender.addStateListener("selectedCell", function(cell) {
  // cell: cellModel | null
  
  // Implementation...
});
