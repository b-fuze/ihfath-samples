(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
// Construct the CellDisplay

var CellDisplay = require("./core/celldisplay");
var domWrapper = require("./core/dom-wrapper");
var domCells = require("./core/dom-cells");
var addHistory = require("./misc/history").addHistory;
var addSelection = require("./misc/selection");
var addClipboard = require("./misc/clipboard");

function construct(userOptions, data) {
  var cd      = new CellDisplay(userOptions);
  var options = cd.options;
  
  // Attempt to set data, will most likely error on
  // improperly formed data
  if (Array.isArray(data)) {
    cd._state       = cd.genStateTable(data, options.emptyState);
    cd._presetState = true;
  }
  
  // Create wrapper element
  cd.wrapper = domWrapper(cd);
  
  // Create cells
  domCells(cd);
  
  // Add history feature
  if (options.history) {
    addHistory(cd);
  }
  
  // Add selection feature
  if (options.canSelect) {
    addSelection(cd);
  }
  
  // Add clipboard feature
  if (options.clipboard) {
    addClipboard(cd);
  }
  
  // Append wrapper to main element
  cd.options.element.appendChild(cd.wrapper);
  
  return cd;
}

module.exports = construct;

},{"./core/celldisplay":2,"./core/dom-cells":3,"./core/dom-wrapper":4,"./misc/clipboard":7,"./misc/history":8,"./misc/selection":9}],2:[function(require,module,exports){
var domCells = require("./dom-cells");
var domWrapper = require("./dom-wrapper");

// CellDisplay class file

var displayCount = 0;

// CellDisplay constructor
function CellDisplay(options) {
  // Get LCES component
  lces.types.component.call(this);
  var that = this;
  
  // Set the CellDisplay's Cellidi id
  jSh.constProp(this, "cid", ++displayCount);
  
  // Extend base options
  var baseOptions = jSh.extendObj({}, this.options);
  this.options = jSh.extendObj(baseOptions, options);
  
  // Visual (non-virtual) Cell Mapping
  this._cellMap = [];
  
  // Virtual (non-visual) cell mapping
  this._cellVirtualMap = [];
  
  // Linked cells (for cellspans)
  this._linkedCells = [];
  
  // Wrapper Residue (for cleanup while rerendering)
  this._wrapperResidue = [];
  
  // Header map
  this.headers = {
    top: null,
    bottom: null,
    right: null,
    left: null,
    corner: null,
    
    // DOM element wrappers for top and bottom headers
    domTop: null,
    domBottom: null,
  };
  
  // Current state (overall data state)
  this._state       = this.genStateTable(this.options.emptyState || null);
  this._presetState = false;
  
  // Add model states
  if (options.model) {
    var model       = options.model;
    var modelStates = Object.getOwnPropertyNames(model);
    
    for (var i=0; i<modelStates.length; i++) {
      var modelStateName = modelStates[i];
      
      this.setState(modelStateName, model[modelStateName]);
    }
  }
  
  // Add events
  
  // 'newstate' event
  //
  // This event is triggered after a new state has completely propogated through
  // all the necessary protocols and is evidently different from the previous
  // state, a user can use this to maybe store the new state in some place, etc
  this.addEvent("newstate");
  
  // 'render' event
  //
  // This event is normally triggered by user code to indicate if and when the
  // CellDisplay should trigger a rerender (normally to update some visual
  // element's positions, or something of similar nature)
  this.addEvent("render");
  
  // 'activeinput' event
  this.addEvent("activeinput");
  
  // 'blurinput' event
  this.addEvent("blurinput");
  
  // 'rebuild' event
  //
  // This is triggered when `cb.rebuild(newStateTable)` is called and the cells
  // are reconstructed
  this.addEvent("rebuild");
  
  // Rebuild callback, called with the data to map cell-by-cell
  this.rebuildCb = null;
  
  // Add states
  
  // Auto adjusted state (for the user)
  this.setState("state", this._state);
  this.addStateListener("state", function(newState) {
    var offsetX = that.options.offsetX;
    var offsetY = that.options.offsetY;
    
    var newTable = that._shiftStateTable(newState, offsetX, offsetY);
    
    // Apply changes
    that.change(newTable);
  });
  
  // Change `state`'s get function
  var tmpStateTable = null;
  
  this.states.state.get = function() {
    // If tmp table is stale update it
    if (!tmpStateTable) {
      tmpStateTable = that._shiftStateTable(that._state, that.options.offsetX * -1, that.options.offsetY * -1);
    }
    
    return tmpStateTable;
  };
  
  // Update the `state` LCES state when `newstate` event is fired
  this.on("newstate", function() {
    tmpStateTable = null;
  });
  
  // Current hovered cell (model)
  this.setState("hoveredCell", null);
  
  // Current hovered header cell
  this.setState("hoveredHeaderCell", null);
  
  // Current (main) selected cell model
  this.setState("selectedCell", null);
}

jSh.inherit(CellDisplay, lces.types.component);

CellDisplay.prototype.rebuild = function(newStateTable) {
  if (!this.options.emptyState) {
    return false;
  }
  
  var oldWrapper = this.wrapper;
  
  // Cleanup
  this._wrapperCleanUp();
  
  // Set new state
  if (Array.isArray(newStateTable)) {
    this._state       = this.genStateTable(newStateTable, this.options.emptyState);
    this._presetState = true;
  } else {
    this._presetState = false;
  }
  
  // Rebuild wrapper
  this.wrapper = domWrapper(this, true);
  
  // Rebuild cells
  domCells(this);
  
  // Remove the old wrapper and add the new one
  this.options.element.removeChild(oldWrapper);
  this.options.element.appendChild(this.wrapper);
  
  this.triggerEvent("rebuild", {});
};

CellDisplay.prototype._wrapperCleanUp = function() {
  var residue = this._wrapperResidue;
  
  for (var i=0; i<residue.length; i++) {
    var curResidue = residue[i];
    
    switch (curResidue[0]) {
      case "dom":
        curResidue[1].removeEventListener(curResidue[2], curResidue[3]);
        break;
    }
  }
  
  this._wrapperResidue = [];
};

/**
 * _residueDom
 *
 * Internal mechanism to keep track of event listener functions that should
 * be removed on a rerender
 */
CellDisplay.prototype._residueDom = function(element, event, callback) {
  this._wrapperResidue.push([
    "dom", element, event, callback
  ]);
  
  element.addEventListener(event, callback);
};

CellDisplay.prototype._transformCoords = function(x, y, direction) {
  var dimensions = this.options.dimensions;
  var newX       = x + (this.options.offsetX * direction);
  var newY       = y + (this.options.offsetY * direction);
  
  var width  = dimensions[0];
  var height = dimensions[1];
  
  if (newX >= width) {
    newX = newX % width;
  } else if (newX < 0) {
    newX += width;
  }
  
  if (newY >= height) {
    newY = newY % height;
  } else if (newY < 0) {
    newY += height;
  }
  
  return [
    newX,
    newY
  ];
};

CellDisplay.prototype.fromVirtualCoords = function(x, y) {
  return this._transformCoords(x, y, -1);
};

CellDisplay.prototype.toVirtualCoords = function(x, y) {
  return this._transformCoords(x, y, 1);
};

CellDisplay.prototype._shiftStateTable = function(table, offsetX, offsetY) {
  var dimensions = this.options.dimensions;
  var width      = dimensions[0];
  var height     = dimensions[1];
  var newTable   = [];
  var column;
  
  for (var x=0; x<width; x++) {
    var newX = x + offsetX;
    
    if (newX >= width) {
      newX = newX % width;
    } else if (newX < 0) {
      newX += width;
    }
    
    if (offsetY === 0) {
      newTable.push(table[newX].slice());
    } else if (offsetY < 0) {
      column = table[newX];
      
      newTable.push(column.slice(height + offsetY).concat(column.slice(0, height + offsetY)));
    } else {
      column = table[newX];
      
      newTable.push(column.slice(offsetY).concat(column.slice(0, offsetY)));
    }
  }
  
  return newTable;
};

CellDisplay.prototype.getCellModelFromCoords = function(x, y) {
  var coords = [
    x, y
  ];
  
  return this._cellMap[coords[0]][coords[1]] || null;
};

CellDisplay.prototype.getCellModelFromDOM = function(dom) {
  var coords;
  
  // Return null if not a valid cell DOM
  if (!(coords = dom.cellidiCoords)) {
    return null;
  }
  
  return this._cellMap[coords[0]][coords[1]] || null;
};

CellDisplay.prototype.getCellState = function(cellModel) {
  var coords = cellModel.coords;
  
  return this._state[coords[0]][coords[1]];
};

CellDisplay.prototype.genStateTable = function(newValue, copyCallback) {
  // Just copy a current table
  if (Array.isArray(newValue)) {
    var copy = [];
    
    if (typeof copyCallback === "function") {
      for (var j=0; j<newValue.length; j++) {
        var curCol = newValue[j];
        var newCol = [];
        
        for (var k=0; k<curCol.length; k++) {
          newCol.push(copyCallback(true, curCol[k]));
        }
        
        copy.push(newCol);
      }
      
    } else {
      for (var i=0; i<newValue.length; i++) {
        copy.push(newValue[i].slice());
      }
    }
    
    return copy;
  }
  
  // Generate a generic table
  var dimensions = this.options.dimensions;
  var width      = dimensions[0];
  var height     = dimensions[1];
  var callback   = null;
  var initialState;
  
  if (typeof newValue === "function") {
    callback = newValue;
  } else {
    // Use newValue if defined
    if (newValue === undefined) {
      initialState = 0;
    } else {
      initialState = newValue;
    }
  }
  
  var table = [];
  
  for (var x=0; x<width; x++) {
    var column = [];
    
    for (var y=0; y<height; y++) {
      if (callback) {
        // Use value returned from callback instead of initialState
        column.push(callback(x, y));
      } else {
        column.push(initialState);
      }
    }
    
    table.push(column);
  }
  
  return table;
};

// Static method
CellDisplay.genStateTable = function(newValue, width, height, copyCallback) {
  return CellDisplay.prototype.genStateTable.call({
    options: {
      dimensions: [width, height]
    }
  }, newValue, copyCallback);
};

CellDisplay.prototype.diffStateTables = function(table1, table2, tableRefs, diffFunc) {
  var dimensions = this.options.dimensions;
  var width      = dimensions[0];
  var height     = dimensions[1];
  
  var diffCoordArray = [];
  var diffRefArray   = [];
  
  if (tableRefs) {
    for (var i=0; i<tableRefs.length; i++) {
      diffRefArray.push([]);
    }
  }
  
  for (var x=0; x<width; x++) {
    for (var y=0; y<height; y++) {
      var cell1  = table1[x][y];
      var cell2  = table2[x][y];
      var append = false;
      
      if (diffFunc) {
        append = diffFunc(cell1, cell2);
      } else {
        if (cell1 !== cell2) {
          append = true;
        }
      }
      
      if (append) {
        diffCoordArray.push([x, y]);
        
        if (tableRefs) {
          for (var i=0; i<tableRefs.length; i++) {
            diffRefArray[i].push(tableRefs[i][x][y]);
          }
        }
      }
    }
  }
  
  var returnObj = {
    coords: diffCoordArray
  };
  
  if (tableRefs) {
    returnObj.ref = diffRefArray;
  }
  
  // Return the arrays
  return returnObj;
};

CellDisplay.prototype.mapEachCell = CellDisplay.mapEachCell =
function mapEachCell(table, callback, tableRefs) {
  var width;
  var height;
  
  if (this !== CellDisplay) {
    var dimensions = this.options.dimensions;
    
    width  = dimensions[0];
    height = dimensions[1];
  } else {
    width  = table.length;
    height = table[0].length;
  }
  
  var newTable = [];
  
  for (var x=0; x<width; x++) {
    var column = [];
    
    for (var y=0; y<height; y++) {
      if (tableRefs) {
        var currentRefs = [];
        
        for (var i=0; i<tableRefs.length; i++) {
          currentRefs.push(tableRefs[i][x][y]);
        }
        
        column.push(callback(table[x][y], x, y, currentRefs));
      } else {
        // Call without table refs
        column.push(callback(table[x][y], x, y));
      }
    }
    
    newTable.push(column);
  }
  
  return newTable;
};

CellDisplay.prototype.change = function(newStateTable, noDiff) {
  var dimensions = this.options.dimensions;
  var width      = dimensions[0];
  var height     = dimensions[1];
  var newTable   = [];
  var cells      = this._cellMap;
  
  var tmpCoordArray = [];
  var tmpStateArray = [];
  var tmpCellArray  = [];
  
  if (noDiff) {
    // Render the whole thing
    for (var x=0; x<width; x++) {
      for (var y=0; y<height; y++) {
        tmpCoordArray.push([x, y]);
        tmpStateArray.push(newStateTable[x][y]);
        tmpCellArray.push(cells[x][y]);
      }
    }
  } else {
    // Diff out the changed parts to avoid unnecessary rendering
    var diffState = this.diffStateTables(newStateTable, this._state, [newStateTable, this._cellMap], this.options.diffStates);
    
    tmpCoordArray = diffState.coords;
    tmpStateArray = diffState.ref[0];
    tmpCellArray  = diffState.ref[1];
  }
  
  // Now render by calling user callback
  var cbDiff = this.options.onCellStateChange(tmpStateArray, tmpCellArray);
  
  // Apply diff if any
  if (cbDiff) {
    for (var i=0; i<cbDiff.length; i++) {
      var coord = tmpCoordArray[i];
      newStateTable[coord[0]][coord[1]] = cbDiff[i];
    }
  }
  
  // Set new state
  this._state = this.genStateTable(newStateTable, this.options.emptyState);
  
  // Trigger newstate event
  this.triggerEvent("newstate", {
    state: this.genStateTable(newStateTable, this.options.emptyState)
  });
  
  // Return stateTable
  return newStateTable;
};

CellDisplay.prototype.changeSubset = function(coords, newState, noDiff) {
  var newStateTable = this.genStateTable(this._state, this.options.emptyState);
  var oldState      = this._state;
  var callback;
  
  if (typeof newState === "function") {
    callback = newState;
  }
  
  // Apply changes
  for (var i=0; i<coords.length; i++) {
    var coord = coords[i];
    
    if (callback) {
      newStateTable[coord[0]][coord[1]] = callback(coord[0], coord[1], oldState[coord[0]][coord[1]]);
    } else {
      newStateTable[coord[0]][coord[1]] = newState;
    }
  }
  
  // Render complete table with changes
  this.change(newStateTable, noDiff);
  
  // Return table
  return newStateTable;
};

CellDisplay.prototype.changeSelection = function(newState) {
  if (this.options.canSelect) {
    var emptySelection = this.genStateTable(false);
    var diff           = this.diffStateTables(this.selection, emptySelection, [this._cellMap]);
    
    return this.changeSubset(diff.coords, newState);
  } else {
    return null;
  }
};

// Base options to be extended by user options
CellDisplay.prototype.options = {
  element: null,
  focus: null,
  name: null,
  
  dimensions: [0, 0],
  
  offsetX: 0,
  offsetY: 0,
  
  canSelect: false,
  maxSelect: 0,
  
  history: false,
  maxHistory: 60,
  
  clipboard: false,
  
  baseCellHeight: 40, // TODO: Use this throughout the codebase
  setCellWidth: true,
  
  headers: {},
  
  model: {},
  
  onCellSelectChange: null,
  onCellStateChange: null,
  newCell: null,
  diffStates: null,
  emptyState: null
};

// Set exports
module.exports = CellDisplay;

},{"./dom-cells":3,"./dom-wrapper":4}],3:[function(require,module,exports){
// Construct all the cells for the CellDisplay

function buildCornerHeader(x, y, cd, setWidth) {
  var headers = cd.options.headers;
  var corner  = (headers.corner ? headers.corner(y, x, cd) : jSh.d(null, ih("&nbsp;")));
  
  if (!(corner instanceof HTMLElement)) {
    throw new TypeError("Cellidi CellDisplay: corner header cell callback must return a valid DOM element");
  }
  
  corner.classList.add("cellidi-header-cell");
  corner.classList.add("cellidi-header-cell-horizontal");
  corner.classList.add("cellidi-corner-cell-" + y + "-" + x);
  
  if (setWidth) {
    corner.classList.add(setWidth);
  }
  
  corner.cellidiHeaderCell = true;
  corner.cellidiCoords = [
    -1, -1
  ];
  
  corner.cellidiVirtualCoords = [
    -1, -1
  ];
  
  corner.cellidiCorner = [x, y];
  
  return corner;
}

function buildVerticalHeader(cd, orientation, build, width, setWidth) {
  var headerWrap = jSh.d(".cellidi-" + orientation + "-header");
  var headerMap  = [];
  var frag       = jSh.docFrag();
  
  var headers = cd.options.headers;
  
  if (headers.left) {
    frag.appendChild(buildCornerHeader("left", "top", cd, setWidth));
  }
  
  for (var i=0; i<width; i++) {
    var virtual = cd.toVirtualCoords(i, 0);
    var curCell = build(i, virtual[0], orientation, cd);
    
    if (!(curCell instanceof HTMLElement)) {
      throw new TypeError("Cellidi CellDisplay: " + orientation + " header cell callback must return a valid DOM element");
    }
    
    // Add classname and meta properties
    curCell.classList.add("cellidi-header-cell");
    curCell.classList.add("cellidi-header-cell-horizontal");
    
    if (setWidth) {
      curCell.classList.add(setWidth);
    }
    
    curCell.cellidiHeaderCell = true;
    curCell.cellidiCoords = [
      i, 0
    ];
    
    curCell.cellidiVirtualCoords = [
      virtual[0], 0
    ];
    
    frag.appendChild(curCell);
    headerMap.push(curCell);
  }
  
  if (headers.right) {
    frag.appendChild(buildCornerHeader("right", "top", cd));
  }
  
  // Append all header cells to headerWrap
  headerWrap.appendChild(frag);
  
  return {
    wrapper: headerWrap,
    map: headerMap
  };
}

function buildHorizontalHeader(cd, pos, orientation, build, setWidth) {
  var virtual = cd.toVirtualCoords(0, pos);
  var header  = build(pos, virtual[1], "left", cd);
  
  if (!(header instanceof HTMLElement)) {
    throw new TypeError("Cellidi CellDisplay: " + orientation + " header cell callback must return a valid DOM element");
  }
  
  // Add classname and meta properties
  header.classList.add("cellidi-header-cell");
  header.classList.add("cellidi-header-cell-vertical");
  header.classList.add("cellidi-header-cell-" + orientation);
  
  if (setWidth) {
    header.classList.add(setWidth);
  }
  
  header.cellidiHeaderCell = true;
  header.cellidiCoords = [
    0, pos
  ];
  
  header.cellidiVirtualCoords = [
    0, virtual[1]
  ];
  
  return header;
}

function domCells(cd) {
  var options     = cd.options;
  var wrapper     = cd.wrapper;
  var wrapperFrag = jSh.docFrag();
  var headers     = options.headers || {};
  var width       = options.dimensions[0];
  var height      = options.dimensions[1];
  
  var top    = headers.top;
  var bottom = headers.bottom;
  var right  = headers.right;
  var left   = headers.left;
  
  var state    = cd._presetState ? cd._state : null;
  var setWidth = options.setCellWidth ? "cellidi-d" + cd.cid + "-cell" : "";
  
  // Get row callback
  var onRow = options.newRow;
  
  // Build top header
  if (top) {
    var topHeader = buildVerticalHeader(cd, "top", top, width, setWidth);
    
    // Append to main cell wrapper
    wrapperFrag.appendChild(cd.headers.domTop = topHeader.wrapper);
    cd.headers.top = topHeader.map;
  }
  
  // Make left/right cell mappings
  if (left) cd.headers.left = [];
  if (right) cd.headers.right = [];
  
  // Cell mapping
  var cellMap        = cd._cellMap = [];
  var cellVirtualMap = cd._cellVirtualMap = [];
  
  // Linked cells (for cellspan)
  var linkedCells    = cd._linkedCells = cd.genStateTable(false);
  var linkedCellList = cd._linkedCellsList = [];
  
  for (var i=0; i<width; i++) {
    cellMap.push([]);
    cellVirtualMap.push([]);
  }
  
  // Row fragment
  var rowWrapper = jSh.d(".cellidi-cell-row-wrapper");
  
  // Build out each cell
  for (var y=0; y<height; y++) {
    var row         = jSh.d(".cellidi-cell-row");
    var tmpRowArray = [];
    
    // Add left header
    if (left) {
      var leftHeader = buildHorizontalHeader(cd, y, "left", left, setWidth);
      
      row.appendChild(leftHeader);
      cd.headers.left.push(leftHeader);
    }
    
    for (var x=0; x<width; x++) {
      if (!linkedCells[x][y]) {
        var virtual = cd.toVirtualCoords(x, y);
        var newCell = state
                      ? options.newCell(x, y, virtual[0], virtual[1], state[virtual[0]][virtual[1]], cd)
                      : options.newCell(x, y, virtual[0], virtual[1], undefined, cd);
        var newCellDOM = newCell.dom;
        
        // Check that the cell is a proper DOM element
        if (!(newCellDOM instanceof HTMLElement)) {
          throw new TypeError("Cellidi CellDisplay: Cell (" + x + ", " + y + ") callback must return a valid DOM element via its object (.dom property)");
        }
        
        // Add classname
        newCellDOM.classList.add("cellidi-cell");
        newCellDOM.cellidiCoords = [
          x, y
        ];
        
        if (setWidth) {
          newCellDOM.classList.add(setWidth);
        }
        
        // Add dead-cell class
        if (newCell.deadCell) {
          newCellDOM.classList.add("cellidi-dead-cell");
        }
        
        newCellDOM.cellidiActiveCell = !newCell.deadCell;
        
        // Add coords to cell model
        newCell.coords = [
          x, y
        ];
        
        newCell.virtualCoords = [
          virtual[0], virtual[1]
        ];
        
        // Append cell
        row.appendChild(newCellDOM);
        cellMap[x][y] = newCell;
        cellVirtualMap[virtual[0]][virtual[1]] = newCell;
        
        // Check for - and apply if found - cellspan
        var cellSpanY = Math.round(newCell.spanY);
        var cellSpanX = Math.round(newCell.spanX);
        
        if (cellSpanY > 1) {
          for (var j=1; j<cellSpanY; j++) {
            var newY = y + j;
            
            cellMap[x][newY] = newCell;
            
            var spanVirtualY = cd.toVirtualCoords(x, newY);
            cellVirtualMap[spanVirtualY[0]][spanVirtualY[1]] = newCell;
            
            // Mark next cells as "full"
            linkedCells[x][newY] = newCell;
            linkedCellList.push([x, newY]);
          }
          
          // Apply height
          newCellDOM.style.height = (100 * cellSpanY) + "%";
          newCellDOM.style.zIndex = 100; // To raise above other cells under it
        }
        
        // TODO: DRY this please
        if (cellSpanX > 1) {
          for (var k=1; k<cellSpanX; k++) {
            var newX = x + k;
            
            cellMap[newX][y] = newCell;
            
            var spanVirtualX = cd.toVirtualCoords(newX, y);
            cellVirtualMap[spanVirtualX[0]][spanVirtualX[1]] = newCell;
            
            // Mark next cells as "full"
            linkedCells[newX][y] = newCell;
            linkedCellList.push([newX, y]);
          }
          
          // Apply height
          newCellDOM.style.height = (100 * cellSpanX) + "%";
          newCellDOM.style.zIndex = 100; // To raise above other cells under it
        }
        
        // Append to tmparray
        tmpRowArray.push(newCell);
      } else {
        row.appendChild(jSh.d(".cellidi-cellspan-fill.cellidi-cell.cellidi-dead-cell" + (setWidth ? "." + setWidth : ""))); // Add empty cell to fill in space
        tmpRowArray.push(linkedCells[x][y]);
      }
    }
    
    // Add right header
    if (right) {
      var rightHeader = buildHorizontalHeader(cd, y, "right", right, setWidth);
      
      row.appendChild(rightHeader);
      cd.headers.right.push(rightHeader);
    }
    
    rowWrapper.appendChild(row);
    
    // Process row
    if (onRow) {
      onRow(row, tmpRowArray, y, virtual[1], cd);
    }
  }
  
  // Append rows
  wrapperFrag.appendChild(rowWrapper);
  
  // Build bottom header
  if (bottom) {
    var bottomHeader = buildVerticalHeader(cd, "bottom", bottom, width);
    
    // Append to main cell wrapper
    wrapperFrag.appendChild(cd.headers.domBottom = bottomHeader.wrapper);
    cd.headers.bottom = bottomHeader.map;
  }
  
  wrapper.appendChild(wrapperFrag);
}

module.exports = domCells;

},{}],4:[function(require,module,exports){
// CellDisplay main wrapper

function domWrapper(cd, rebuild) {
  var options = cd.options;
  var wrapper = jSh.d(".cellidi-cd-wrapper");
  var focus   = cd.options.focus || wrapper;
  var oldClickTarget;
  var oldHoverTarget;
  
  // Add cells width styling
  if (options.setCellWidth) {
    // Account for any left or right headers
    var additionOffset = Number(!!options.headers.left) + Number(!!options.headers.right);
    
    wrapper.appendChild(jSh.c("style", {
      prop: {
        type: "text/css"
      },
      
      child: [
        jSh.t(".cellidi-d" + cd.cid + "-cell {\n"
            + "  width: " + (100 / (options.dimensions[0] + additionOffset)) + "%\n"
            + "}")
      ]
    }));
  }
  
  // Add "mouseup" listener
  cd._residueDom(wrapper, "mouseup", function(e) {
    if (oldClickTarget) {
      var target    = e.target;
      var curTarget = target;
      var cell      = null;
      
      // Try to see if this is cell
      while (curTarget !== wrapper) {
        if (curTarget.cellidiCoords && curTarget.cellidiActiveCell) {
          cell = curTarget;
          
          break;
        }
        
        curTarget = curTarget.parentNode;
      }
      
      // Set selectedCell
      if (cell === oldClickTarget) {
        cd.selectedCell = cell;
        
        oldClickTarget = null;
      }
    }
  });
  
  // Add "mousedown" listener
  cd._residueDom(wrapper, "mousedown", function(e) {
    var target    = e.target;
    var curTarget = target;
    var cell      = null;
    
    (cd.options.focus || cd.wrapper).focus();
    
    // Try to see if this is a cell
    while (curTarget !== wrapper) {
      if (curTarget.cellidiCoords && curTarget.cellidiActiveCell) {
        cell = curTarget;
        
        break;
      }
      
      curTarget = curTarget.parentNode;
    }
    
    if (cell) {
      var header = cell.cellidiHeaderCell;
      
      if (!header) {
        cell = cd.getCellModelFromDOM(cell);
        oldClickTarget = cell;
      } else {
        oldClickTarget = null;
      }
      
      cd.triggerEvent("activeinput", {
        dom: target,
        cell: cell,
        header: header,
        event: e
      });
    } else {
      oldClickTarget = null;
      
      cd.triggerEvent("activeinput", {
        dom: target,
        cell: null,
        header: false,
        event: e
      });
    }
    
    function clearEvents(e) {
      window.removeEventListener("mouseup", clearEvents);
      window.removeEventListener("blur", clearEvents);
      
      cd.triggerEvent("blurinput", {
        event: e
      });
    }
    
    window.addEventListener("mouseup", clearEvents);
    window.addEventListener("blur", clearEvents);
  });
  
  // Add "mousemove" listener
  cd._residueDom(wrapper, "mousemove", function(e) {
    var target = e.target;
    
    if (target === oldHoverTarget) {
      // This element has been processed already
      return;
    }
    
    // To prevent unnecessary processing
    oldHoverTarget = target;
    
    if (target.cellidiCoords) {
      if (target.cellidiActiveCell) {
        cd.hoveredCell = cd.getCellModelFromDOM(target);
      } else if (target.cellidiHeaderCell) {
        cd.hoveredHeaderCell = target;
      }
    }
  });
  
  // Add "tabindex" to wrapper if it's the focus
  if (focus === wrapper) {
    wrapper.setAttribute("tabindex", 0);
  }
  
  // Prevent Chrome from jumping around everytime when the cellwrapper is focused
  if (!rebuild || focus === wrapper) {
    focus.addEventListener("focus", function(e) {
      e.preventDefault();
    });
  }
  
  return wrapper;
}

module.exports = domWrapper;

},{}],5:[function(require,module,exports){
// Validation mechanics for Cellidi

function isFunction(props, obj, optional) {
  if (optional) {
    for (var i=0; i<props.length; i++) {
      var optValue = obj[props[i]];
      
      if (optValue !== undefined && typeof optValue !== "function") {
        return new TypeError("Cellidi CellDisplay: options." + props[i] + " should be a function");
      }
    }
  } else {
    for (var j=0; j<props.length; j++) {
      var value = obj[props[j]];
      
      if (typeof value !== "function") {
        return new TypeError("Cellidi CellDisplay: options." + props[j] + " should exist and be a function");
      }
    }
  }
  
  return null; // Success
}

function isBoolean(value) {
  return typeof value === "boolean";
}

function validateOptions(options) {
  var error;
  
  if (jSh.type(options) !== "object") {
    return new TypeError("Cellidi CellDisplay: Options parameter must be an object");
  }
  
  // Check element TODO: Check if shouldn't compare against HTMLElement
  if (!(options.element instanceof HTMLElement)) {
    return new TypeError("Cellidi CellDisplay: options.element must be a proper DOM element");
  }
  
  // Check focus TODO: Check if shouldn't compare against HTMLElement
  if (options.focus && !(options.focus instanceof HTMLElement)) {
    return new TypeError("Cellidi CellDisplay: options.focus must be a proper DOM element");
  }
  
  // Check unique name
  if (options.name !== undefined && !(typeof options.name === "string" && options.name)) {
    return new TypeError("Cellidi CellDisplay: options.name must be a non-empty string");
  }
  
  // Check dimensions
  if (!Array.isArray(options.dimensions) || options.dimensions.length !== 2) {
    return new TypeError("Cellidi CellDisplay: options.dimensions must be an array with two numbers");
  }
  
  // Check individual dimensions
  options.dimensions.every(function(dim, i) {
    if (jSh.numOp(dim, null) === null || dim < 1) {
      error = new TypeError("Cellidi CellDisplay: options.dimensions[" + i + "] must be valid integer above 0");
      return false;
    }
    
    return true;
  });
  
  // Check width styling
  if (options.setCellWidth && !isBoolean(options.setCellWidth)) {
    return new TypeError("Cellidi CellDisplay: options.setCellWidth must be a boolean");
  }
  
  // Check base cell height
  if (options.baseCellHeight !== undefined && jSh.numOp(options.baseCellHeight, null) === null) {
    return new TypeError("Cellidi CellDisplay: options.baseCellHeight must be valid number");
  }
  
  // Return on error
  if (error) return error;
  
  // Check offset
  if (options.offsetX !== undefined && jSh.numOp(options.offsetX, null) === null) {
    return new TypeError("Cellidi CellDisplay: options.offsetX must be valid number");
  }
  
  if (options.offsetY !== undefined && jSh.numOp(options.offsetY, null) === null) {
    return new TypeError("Cellidi CellDisplay: options.offsetY must be valid number");
  }
  
  // Check selection
  if (options.canSelect) {
    if (!isBoolean(options.canSelect)) {
      return new TypeError("Cellidi CellDisplay: options.canSelect must be a boolean");
    }
    
    if (typeof options.onCellSelectChange !== "function") {
      return new TypeError("Cellidi CellDisplay: options.canSelect requires the function options.onCellSelectChange");
    }
    
    if (options.maxSelect !== undefined) {
      if (typeof options.maxSelect !== "number" || isNaN(options.maxSelect)) {
        return new TypeError("Cellidi CellDisplay: options.maxSelect must be a valid number");
      } else if (options.maxSelect < 0) {
        // Normalize `options.maxSelect`
        options.maxSelect = 0;
      }
    }
  }
  
  // Check history
  if (options.history) {
    if (!isBoolean(options.history)) {
      return new TypeError("Cellidi CellDisplay: options.history must be a boolean");
    }
    
    if (options.maxHistory !== undefined && (typeof options.maxHistory !== "number" || isNaN(options.maxHistory) || options.maxHistory < 1)) {
      return new TypeError("Cellidi CellDisplay: options.maxHistory must be a valid number greater than 0");
    }
  }
  
  if (options.clipboard !== undefined && !isBoolean(options.clipboard)) {
    return new TypeError("Cellidi CellDisplay: options.history must be a boolean");
  }
  
  // Check model
  if (options.model && jSh.type(options.model) !== "object") {
    return new TypeError("Cellidi CellDisplay: options.model must be an object");
  }
  
  // Check individual (required) function options
  error = isFunction([
    "newCell"
  ], options);
  
  // Return on error
  if (error) return error;
  
  // Check individual (optional) function options
  error = isFunction([
    "onCellSelectChange",
    "onCellStateChange",
    "newRow"
  ], options, true);
  
  // Return on error
  if (error) return error;
  
  // Check for complex state functions
  if (options.diffStates) {
    error = isFunction([
      "diffStates",
      "emptyState"
    ], options);
    
    // Return on error
    if (error) return error;
  }
  
  // Check headers
  if (options.headers) {
    // Error if headers isn't supplied properly
    if (jSh.type(options.headers) !== "object") {
      return new TypeError("Cellidi CellDisplay: options.headers must be an object");
    }
    
    // Check each header function
    error = isFunction([
      "top",
      "bottom",
      "right",
      "left",
      "corner"
    ], options.headers, true);
    
    // Return on error
    if (error) return error;
  }
  
  return true; // Passed tests
}

module.exports = validateOptions;

},{}],6:[function(require,module,exports){
// Include all required files

var construct = require("./construct");
var CellDisplay = require("./core/celldisplay");
var checkValidOptions = require("./core/options");

function Cellidi(options, data) {
  // Validate options
  var validOptions = checkValidOptions(options);
  
  // Check if options are invalid
  if (validOptions !== true) {
    // Fail
    throw validOptions;
  }
  
  // Construct the CellDisplay
  var cd = construct(options, data);
  
  return cd;
}

// Make CellDisplay's static functions accessible
Cellidi.CellDisplay = CellDisplay;

window.Cellidi = Cellidi;

},{"./construct":1,"./core/celldisplay":2,"./core/options":5}],7:[function(require,module,exports){
// CellDisplay clipboard feature

function addClipboard(cd) {
  var options = cd.options;
  var focus   = options.focus || cd.wrapper;
  var mime    = "application/x-cellidi";
  
  focus.addEventListener("copy", function(e) {
    if (e.clipboardData) {
      e.clipboardData.setData(mime, serializeState(cd));
      
      e.preventDefault();
      e.stopPropagation();
    }
  });
  
  focus.addEventListener("paste", function(e) {
    if (e.clipboardData) {
      var state = e.clipboardData.getData(mime);
      
      if (state) {
        var frame = jSh.parseJSON(state);
        
        if (frame && !frame.error && validSource(cd, frame)) {
          cd.state = frame.state;
        }
      }
    }
  });
}

function serializeState(cd) {
  var options = cd.options;
  var frame   = {
    name: options.name,
    dimensions: options.dimensions,
    state: cd.state,
  };
  
  return JSON.stringify(frame);
}

function validSource(cd, foreign) {
  var options    = cd.options;
  var dimensions = options.dimensions;
  var valid      = false;
  
  if (
    options.name === foreign.name
    && foreign.dimensions
    && dimensions[0] === foreign.dimensions[0]
    && dimensions[1] === foreign.dimensions[1]
  ) {
    valid = true;
  }
  
  return valid;
}

module.exports = addClipboard;

},{}],8:[function(require,module,exports){
// CellDisplay history feature

function addHistory(cd) {
  var options     = cd.options;
  var focus       = options.focus || cd.wrapper;
  var diffFunc    = options.diffStates || undefined;
  var emptyFunc   = options.emptyState || undefined;
  var linkedCells = cd._linkedCellsList.length && cd._linkedCellsList;
  var stack       = cd._historyStack = {
    buffer: null,
    history: [],
    future: [],
  };
  
  function historyShift(backward) {
    // Get history
    var newState = stateHistory(cd, null, backward, emptyFunc);
    
    if (linkedCells) {
      // Clear any linked cells (cells in the occupied space of cellspans)
      var emptyState = (emptyFunc || null) && emptyFunc();
      
      for (var i=0; i<linkedCells.length; i++) {
        var linkedCoord = linkedCells[i];
        
        cd._state[linkedCoord[0]][linkedCoord[1]] = emptyState;
        newState[linkedCoord[0]][linkedCoord[1]] = emptyState;
      }
    }
    
    // Diff states
    var diffHistory = cd.diffStateTables(newState, cd._state, [newState, cd._cellMap], diffFunc);
    
    // Call state callback
    cd.options.onCellStateChange(diffHistory.ref[0], diffHistory.ref[1]);
    
    // Set new state
    cd._state = newState;
    
    // Trigger newstate event
    cd.triggerEvent("newstate", {
      state: cd.genStateTable(newState, emptyFunc),
      _historyChange: true // Internal history flag, to determine if should log new history or not
    });
  }
  
  focus.addEventListener("keydown", function(e) {
    if (e.ctrlKey && e.keyCode === 90 /* Z Key */) {
      historyShift(e.shiftKey);
      
      e.preventDefault();
    }
  });
  
  cd.on("newstate", function(e) {
    if (!e._historyChange) {
      var state = e.state;
      
      stateHistory(cd, state, null, emptyFunc);
    } else {
      cd.triggerEvent("historystack", {
        previous: stack.history.length,
        future: stack.future.length,
      });
    }
  });
  
  // Reset when cellDisplay is rebuilt
  cd.on("rebuild", function() {
    linkedCells = cd._linkedCellsList.length && cd._linkedCellsList;
    stack       = cd._historyStack = {
      buffer: null,
      history: [],
      future: []
    };
  });
  
  // Add history events
  cd.addEvent("historystack");
  cd.shiftHistory = function(backward) {
    historyShift(!!backward);
  };
}

function stateHistory(cd, current, redo, emptyFunc) {
  var stack   = cd._historyStack;
  var history = stack.history;
  var future  = stack.future;
  
  if (current || redo) {
    // If redo flag is on, pop out latest redo from stack, and return it
    if (redo) {
      // Get latest stack from the future
      var lastRedo = future.pop();
      
      // Only push buffer to history if there's actually something in the future
      if (stack.buffer && lastRedo) {
        history.push(stack.buffer);
        
        // Push redo onto buffer
        stack.buffer = lastRedo;
      }
      
      // Return redo
      return lastRedo || stack.buffer || cd.genStateTable(cd._state, emptyFunc);
    }
    
    // Clear redos if not empty
    if (future.length > 0) {
      future = cd._historyStack.future = [];
    }
    
    // Copy the old history into a new array
    var currentCopy = cd.genStateTable(current, emptyFunc);
    
    if (stack.buffer) {
      // Push old history to history stack
      history.push(stack.buffer);
      
      // Truncate stack if too large
      if (history.length > cd.options.maxHistory) {
        history.shift();
      }
    }
    
    // Add current to buffer
    stack.buffer = currentCopy;
    
    return currentCopy;
  } else {
    // Pop the older history out
    var recentHistory = history.pop();
    
    // Push buffer to the future
    if (stack.buffer && recentHistory) {
      future.push(stack.buffer);
      
      // Save redo to buffer
      stack.buffer = recentHistory;
    }
    
    return recentHistory || stack.buffer || cd.genStateTable(cd._state, emptyFunc);
  }
}

module.exports = {
  addHistory: addHistory,
  stateHistory: stateHistory
};

},{}],9:[function(require,module,exports){
// Cellidi CellDisplay selection functionality

function addSelection(cd) {
  var oldSelection   = cd.genStateTable(false);
  var selection      = cd.genStateTable(false);
  var options        = cd.options;
  var focus          = options.focus || cd.wrapper;
  var emptyFunc      = options.emptyState || undefined;
  var linkedCells    = cd._linkedCellsList.length && cd._linkedCellsList;
  var selectedCell   = null;
  var selectingState = true;
  
  // Add selecting state to CellDisplay
  cd.setState("selecting", false);
  
  // Expose `selection` to other components
  cd.setState("selection", cd.genStateTable(false));
  
  // Add newselection event to CellDisplay
  cd.addEvent("newselection");
  
  // Update selection
  cd.on("newselection", function() {
    cd.states.selection.stateStatus = cd.genStateTable(selection);
  });
  
  // CellDisplay listeners
  
  // Receive new selection other component
  cd.addStateListener("selection", function(newTable) {
    // Render new selection
    selection = renderSelDiff(cd, newTable, selection, linkedCells, emptyFunc);
    
    // Trigger event
    cd.triggerEvent("newselection", {
      selection: cd.genStateTable(selection)
    });
    
    // Replace older selection table
    oldSelection = selection;
  });
  
  // Shortcut key select all (Ctrl + A) (TODO: Have to rework this to work with `rebuild` event)
  focus.addEventListener("keydown", function(e) {
    if (e.ctrlKey && e.keyCode === 65 /* A Key */) {
      var fullSelection = cd.genStateTable(true);
      
      selection = renderSelDiff(cd, fullSelection, selection, linkedCells, emptyFunc);
      cd.triggerEvent("newselection", {
        selection: cd.genStateTable(selection)
      });
      
      e.preventDefault();
    }
  });
  
  // Mouse selection
  cd.on("activeinput", function(e) {
    var cell = e.cell;
    
    if (cell && !e.header) {
      cd.selecting = true;
      
      if (cell === selectedCell) {
        cd.selectedCell = selectedCell = null;
      } else {
        cd.selectedCell = selectedCell = cell;
      }
      
      var tmpTable;
      var newTable;
      
      if (e.event.shiftKey) {
        // Just change the current cells
        tmpTable = cd.genStateTable(selection);
        var cellCoords = selectedCell.coords;
        
        selectingState = !tmpTable[cellCoords[0]][cellCoords[1]];
        
        if (e.event.ctrlKey) {
          var x      = cellCoords[0];
          var height = cd.options.dimensions[1];
          
          for (var y=0; y<height; y++) {
            tmpTable[x][y] = selectingState;
          }
        } else {
          tmpTable[cellCoords[0]][cellCoords[1]] = selectingState;
        }
        
        newTable = renderSelDiff(cd, tmpTable, selection, linkedCells, emptyFunc);
      } else {
        // Make all cells unselected then select this one
        tmpTable = cd.genStateTable(false);
        var coords = cell.coords;
        
        if (e.event.ctrlKey) {
          var curX      = coords[0];
          var curHeight = cd.options.dimensions[1];
          
          for (var y=0; y<curHeight; y++) {
            tmpTable[curX][y] = true;
          }
        } else {
          tmpTable[coords[0]][coords[1]] = true;
        }
        
        newTable = renderSelDiff(cd, tmpTable, selection, linkedCells, emptyFunc);
      }
      
      oldSelection = newTable;
      selection    = newTable;
      cd.triggerEvent("newselection", {
        selection: cd.genStateTable(newTable)
      });
    }
  });
  
  cd.on("blurinput", function() {
    cd.selecting = false;
  });
  
  cd.addStateListener("hoveredCell", function(cell) {
    if (cd.selecting && selectedCell) {
      if (cell === selectedCell) {
        // Just go back to the old selection table with the current cell (un)selected
        selection = renderSelDiff(cd, oldSelection, selection, linkedCells, emptyFunc);
      } else {
        //
        var newTable      = cd.genStateTable(oldSelection);
        var cellCoords    = cell.coords;
        var selCellCoords = selectedCell.coords;
        
        // Select a partial column if both cells are on the same column
        if (cellCoords[0] === selCellCoords[0]) {
          var startCoords = Math.min(cellCoords[1], selCellCoords[1]);
          var endCoords   = Math.max(cellCoords[1], selCellCoords[1]) + 1;
          
          var xCoord = cellCoords[0];
          
          for (var y=startCoords; y<endCoords; y++) {
            newTable[xCoord][y] = selectingState;
          }
        } else {
          newTable[cellCoords[0]][cellCoords[1]] = selectingState;
        }
        
        // Render (un)selected cells
        selection = renderSelDiff(cd, newTable, selection, linkedCells, emptyFunc);
        
        // Trigger newselection event
        cd.triggerEvent("newselection", {
          selection: cd.genStateTable(selection)
        });
      }
    }
  });
  
  // Keep track of selection count
  cd.on("newselection", function(e) {
    // TODO: Implement this...
  });
  
  // Reset when cellDisplay is rebuilt
  cd.on("rebuild", function(e) {
    linkedCells    = cd._linkedCellsList.length && cd._linkedCellsList;
    selectedCell   = null;
    selectingState = true;
    
    // FIXME: Finish this...
  });
}

function renderSelDiff(cd, newSelTable, oldSelTable, linkedCells, emptyFunc) {
  if (linkedCells) {
    // Clear any linked cells (cells in the space of cellspans)
    var emptyState = false;
    
    for (var i=0; i<linkedCells.length; i++) {
      var linkedCoord = linkedCells[i];
      
      oldSelTable[linkedCoord[0]][linkedCoord[1]] = emptyState;
      newSelTable[linkedCoord[0]][linkedCoord[1]] = emptyState;
    }
  }
  
  var diff    = cd.diffStateTables(oldSelTable, newSelTable, [newSelTable, cd._cellMap]);
  var diffMap = diff.coords;
  
  // Call selector callback
  var cbDiff   = cd.options.onCellSelectChange(diff.ref[0], diff.ref[1]);
  var newTable = cd.genStateTable(newSelTable);
  
  if (cbDiff) {
    for (var i=0; i<cbDiff.length; i++) {
      var coord = diffMap[i];
      newTable[coord[0]][coord[1]] = cbDiff[i];
    }
  }
  
  return newTable;
}

module.exports = addSelection;

},{}]},{},[6]);
