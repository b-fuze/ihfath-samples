// CellDisplay main wrapper

function domWrapper(cd, rebuild) {
  var options = cd.options;
  var wrapper = jSh.d(".cellidi-cd-wrapper");
  var focus   = cd.options.focus || wrapper;
  var oldClickTarget;
  var oldHoverTarget;
  
  // Add cells width styling
  if (options.setCellWidth) {
    // Account for any left or right headers
    var additionOffset = Number(!!options.headers.left) + Number(!!options.headers.right);
    
    wrapper.appendChild(jSh.c("style", {
      prop: {
        type: "text/css"
      },
      
      child: [
        jSh.t(".cellidi-d" + cd.cid + "-cell {\n"
            + "  width: " + (100 / (options.dimensions[0] + additionOffset)) + "%\n"
            + "}")
      ]
    }));
  }
  
  // Add "mouseup" listener
  cd._residueDom(wrapper, "mouseup", function(e) {
    if (oldClickTarget) {
      var target    = e.target;
      var curTarget = target;
      var cell      = null;
      
      // Try to see if this is cell
      while (curTarget !== wrapper) {
        if (curTarget.cellidiCoords && curTarget.cellidiActiveCell) {
          cell = curTarget;
          
          break;
        }
        
        curTarget = curTarget.parentNode;
      }
      
      // Set selectedCell
      if (cell === oldClickTarget) {
        cd.selectedCell = cell;
        
        oldClickTarget = null;
      }
    }
  });
  
  // Add "mousedown" listener
  cd._residueDom(wrapper, "mousedown", function(e) {
    var target    = e.target;
    var curTarget = target;
    var cell      = null;
    
    (cd.options.focus || cd.wrapper).focus();
    
    // Try to see if this is a cell
    while (curTarget !== wrapper) {
      if (curTarget.cellidiCoords && curTarget.cellidiActiveCell) {
        cell = curTarget;
        
        break;
      }
      
      curTarget = curTarget.parentNode;
    }
    
    if (cell) {
      var header = cell.cellidiHeaderCell;
      
      if (!header) {
        cell = cd.getCellModelFromDOM(cell);
        oldClickTarget = cell;
      } else {
        oldClickTarget = null;
      }
      
      cd.triggerEvent("activeinput", {
        dom: target,
        cell: cell,
        header: header,
        event: e
      });
    } else {
      oldClickTarget = null;
      
      cd.triggerEvent("activeinput", {
        dom: target,
        cell: null,
        header: false,
        event: e
      });
    }
    
    function clearEvents(e) {
      window.removeEventListener("mouseup", clearEvents);
      window.removeEventListener("blur", clearEvents);
      
      cd.triggerEvent("blurinput", {
        event: e
      });
    }
    
    window.addEventListener("mouseup", clearEvents);
    window.addEventListener("blur", clearEvents);
  });
  
  // Add "mousemove" listener
  cd._residueDom(wrapper, "mousemove", function(e) {
    var target = e.target;
    
    if (target === oldHoverTarget) {
      // This element has been processed already
      return;
    }
    
    // To prevent unnecessary processing
    oldHoverTarget = target;
    
    if (target.cellidiCoords) {
      if (target.cellidiActiveCell) {
        cd.hoveredCell = cd.getCellModelFromDOM(target);
      } else if (target.cellidiHeaderCell) {
        cd.hoveredHeaderCell = target;
      }
    }
  });
  
  // Add "tabindex" to wrapper if it's the focus
  if (focus === wrapper) {
    wrapper.setAttribute("tabindex", 0);
  }
  
  // Prevent Chrome from jumping around everytime when the cellwrapper is focused
  if (!rebuild || focus === wrapper) {
    focus.addEventListener("focus", function(e) {
      e.preventDefault();
    });
  }
  
  return wrapper;
}

module.exports = domWrapper;
