// Validation mechanics for Cellidi

function isFunction(props, obj, optional) {
  if (optional) {
    for (var i=0; i<props.length; i++) {
      var optValue = obj[props[i]];
      
      if (optValue !== undefined && typeof optValue !== "function") {
        return new TypeError("Cellidi CellDisplay: options." + props[i] + " should be a function");
      }
    }
  } else {
    for (var j=0; j<props.length; j++) {
      var value = obj[props[j]];
      
      if (typeof value !== "function") {
        return new TypeError("Cellidi CellDisplay: options." + props[j] + " should exist and be a function");
      }
    }
  }
  
  return null; // Success
}

function isBoolean(value) {
  return typeof value === "boolean";
}

function validateOptions(options) {
  var error;
  
  if (jSh.type(options) !== "object") {
    return new TypeError("Cellidi CellDisplay: Options parameter must be an object");
  }
  
  // Check element TODO: Check if shouldn't compare against HTMLElement
  if (!(options.element instanceof HTMLElement)) {
    return new TypeError("Cellidi CellDisplay: options.element must be a proper DOM element");
  }
  
  // Check focus TODO: Check if shouldn't compare against HTMLElement
  if (options.focus && !(options.focus instanceof HTMLElement)) {
    return new TypeError("Cellidi CellDisplay: options.focus must be a proper DOM element");
  }
  
  // Check unique name
  if (options.name !== undefined && !(typeof options.name === "string" && options.name)) {
    return new TypeError("Cellidi CellDisplay: options.name must be a non-empty string");
  }
  
  // Check dimensions
  if (!Array.isArray(options.dimensions) || options.dimensions.length !== 2) {
    return new TypeError("Cellidi CellDisplay: options.dimensions must be an array with two numbers");
  }
  
  // Check individual dimensions
  options.dimensions.every(function(dim, i) {
    if (jSh.numOp(dim, null) === null || dim < 1) {
      error = new TypeError("Cellidi CellDisplay: options.dimensions[" + i + "] must be valid integer above 0");
      return false;
    }
    
    return true;
  });
  
  // Check width styling
  if (options.setCellWidth && !isBoolean(options.setCellWidth)) {
    return new TypeError("Cellidi CellDisplay: options.setCellWidth must be a boolean");
  }
  
  // Check base cell height
  if (options.baseCellHeight !== undefined && jSh.numOp(options.baseCellHeight, null) === null) {
    return new TypeError("Cellidi CellDisplay: options.baseCellHeight must be valid number");
  }
  
  // Return on error
  if (error) return error;
  
  // Check offset
  if (options.offsetX !== undefined && jSh.numOp(options.offsetX, null) === null) {
    return new TypeError("Cellidi CellDisplay: options.offsetX must be valid number");
  }
  
  if (options.offsetY !== undefined && jSh.numOp(options.offsetY, null) === null) {
    return new TypeError("Cellidi CellDisplay: options.offsetY must be valid number");
  }
  
  // Check selection
  if (options.canSelect) {
    if (!isBoolean(options.canSelect)) {
      return new TypeError("Cellidi CellDisplay: options.canSelect must be a boolean");
    }
    
    if (typeof options.onCellSelectChange !== "function") {
      return new TypeError("Cellidi CellDisplay: options.canSelect requires the function options.onCellSelectChange");
    }
    
    if (options.maxSelect !== undefined) {
      if (typeof options.maxSelect !== "number" || isNaN(options.maxSelect)) {
        return new TypeError("Cellidi CellDisplay: options.maxSelect must be a valid number");
      } else if (options.maxSelect < 0) {
        // Normalize `options.maxSelect`
        options.maxSelect = 0;
      }
    }
  }
  
  // Check history
  if (options.history) {
    if (!isBoolean(options.history)) {
      return new TypeError("Cellidi CellDisplay: options.history must be a boolean");
    }
    
    if (options.maxHistory !== undefined && (typeof options.maxHistory !== "number" || isNaN(options.maxHistory) || options.maxHistory < 1)) {
      return new TypeError("Cellidi CellDisplay: options.maxHistory must be a valid number greater than 0");
    }
  }
  
  if (options.clipboard !== undefined && !isBoolean(options.clipboard)) {
    return new TypeError("Cellidi CellDisplay: options.history must be a boolean");
  }
  
  // Check model
  if (options.model && jSh.type(options.model) !== "object") {
    return new TypeError("Cellidi CellDisplay: options.model must be an object");
  }
  
  // Check individual (required) function options
  error = isFunction([
    "newCell"
  ], options);
  
  // Return on error
  if (error) return error;
  
  // Check individual (optional) function options
  error = isFunction([
    "onCellSelectChange",
    "onCellStateChange",
    "newRow"
  ], options, true);
  
  // Return on error
  if (error) return error;
  
  // Check for complex state functions
  if (options.diffStates) {
    error = isFunction([
      "diffStates",
      "emptyState"
    ], options);
    
    // Return on error
    if (error) return error;
  }
  
  // Check headers
  if (options.headers) {
    // Error if headers isn't supplied properly
    if (jSh.type(options.headers) !== "object") {
      return new TypeError("Cellidi CellDisplay: options.headers must be an object");
    }
    
    // Check each header function
    error = isFunction([
      "top",
      "bottom",
      "right",
      "left",
      "corner"
    ], options.headers, true);
    
    // Return on error
    if (error) return error;
  }
  
  return true; // Passed tests
}

module.exports = validateOptions;
