// Construct all the cells for the CellDisplay

function buildCornerHeader(x, y, cd, setWidth) {
  var headers = cd.options.headers;
  var corner  = (headers.corner ? headers.corner(y, x, cd) : jSh.d(null, ih("&nbsp;")));
  
  if (!(corner instanceof HTMLElement)) {
    throw new TypeError("Cellidi CellDisplay: corner header cell callback must return a valid DOM element");
  }
  
  corner.classList.add("cellidi-header-cell");
  corner.classList.add("cellidi-header-cell-horizontal");
  corner.classList.add("cellidi-corner-cell-" + y + "-" + x);
  
  if (setWidth) {
    corner.classList.add(setWidth);
  }
  
  corner.cellidiHeaderCell = true;
  corner.cellidiCoords = [
    -1, -1
  ];
  
  corner.cellidiVirtualCoords = [
    -1, -1
  ];
  
  corner.cellidiCorner = [x, y];
  
  return corner;
}

function buildVerticalHeader(cd, orientation, build, width, setWidth) {
  var headerWrap = jSh.d(".cellidi-" + orientation + "-header");
  var headerMap  = [];
  var frag       = jSh.docFrag();
  
  var headers = cd.options.headers;
  
  if (headers.left) {
    frag.appendChild(buildCornerHeader("left", "top", cd, setWidth));
  }
  
  for (var i=0; i<width; i++) {
    var virtual = cd.toVirtualCoords(i, 0);
    var curCell = build(i, virtual[0], orientation, cd);
    
    if (!(curCell instanceof HTMLElement)) {
      throw new TypeError("Cellidi CellDisplay: " + orientation + " header cell callback must return a valid DOM element");
    }
    
    // Add classname and meta properties
    curCell.classList.add("cellidi-header-cell");
    curCell.classList.add("cellidi-header-cell-horizontal");
    
    if (setWidth) {
      curCell.classList.add(setWidth);
    }
    
    curCell.cellidiHeaderCell = true;
    curCell.cellidiCoords = [
      i, 0
    ];
    
    curCell.cellidiVirtualCoords = [
      virtual[0], 0
    ];
    
    frag.appendChild(curCell);
    headerMap.push(curCell);
  }
  
  if (headers.right) {
    frag.appendChild(buildCornerHeader("right", "top", cd));
  }
  
  // Append all header cells to headerWrap
  headerWrap.appendChild(frag);
  
  return {
    wrapper: headerWrap,
    map: headerMap
  };
}

function buildHorizontalHeader(cd, pos, orientation, build, setWidth) {
  var virtual = cd.toVirtualCoords(0, pos);
  var header  = build(pos, virtual[1], "left", cd);
  
  if (!(header instanceof HTMLElement)) {
    throw new TypeError("Cellidi CellDisplay: " + orientation + " header cell callback must return a valid DOM element");
  }
  
  // Add classname and meta properties
  header.classList.add("cellidi-header-cell");
  header.classList.add("cellidi-header-cell-vertical");
  header.classList.add("cellidi-header-cell-" + orientation);
  
  if (setWidth) {
    header.classList.add(setWidth);
  }
  
  header.cellidiHeaderCell = true;
  header.cellidiCoords = [
    0, pos
  ];
  
  header.cellidiVirtualCoords = [
    0, virtual[1]
  ];
  
  return header;
}

function domCells(cd) {
  var options     = cd.options;
  var wrapper     = cd.wrapper;
  var wrapperFrag = jSh.docFrag();
  var headers     = options.headers || {};
  var width       = options.dimensions[0];
  var height      = options.dimensions[1];
  
  var top    = headers.top;
  var bottom = headers.bottom;
  var right  = headers.right;
  var left   = headers.left;
  
  var state    = cd._presetState ? cd._state : null;
  var setWidth = options.setCellWidth ? "cellidi-d" + cd.cid + "-cell" : "";
  
  // Get row callback
  var onRow = options.newRow;
  
  // Build top header
  if (top) {
    var topHeader = buildVerticalHeader(cd, "top", top, width, setWidth);
    
    // Append to main cell wrapper
    wrapperFrag.appendChild(cd.headers.domTop = topHeader.wrapper);
    cd.headers.top = topHeader.map;
  }
  
  // Make left/right cell mappings
  if (left) cd.headers.left = [];
  if (right) cd.headers.right = [];
  
  // Cell mapping
  var cellMap        = cd._cellMap = [];
  var cellVirtualMap = cd._cellVirtualMap = [];
  
  // Linked cells (for cellspan)
  var linkedCells    = cd._linkedCells = cd.genStateTable(false);
  var linkedCellList = cd._linkedCellsList = [];
  
  for (var i=0; i<width; i++) {
    cellMap.push([]);
    cellVirtualMap.push([]);
  }
  
  // Row fragment
  var rowWrapper = jSh.d(".cellidi-cell-row-wrapper");
  
  // Build out each cell
  for (var y=0; y<height; y++) {
    var row         = jSh.d(".cellidi-cell-row");
    var tmpRowArray = [];
    
    // Add left header
    if (left) {
      var leftHeader = buildHorizontalHeader(cd, y, "left", left, setWidth);
      
      row.appendChild(leftHeader);
      cd.headers.left.push(leftHeader);
    }
    
    for (var x=0; x<width; x++) {
      if (!linkedCells[x][y]) {
        var virtual = cd.toVirtualCoords(x, y);
        var newCell = state
                      ? options.newCell(x, y, virtual[0], virtual[1], state[virtual[0]][virtual[1]], cd)
                      : options.newCell(x, y, virtual[0], virtual[1], undefined, cd);
        var newCellDOM = newCell.dom;
        
        // Check that the cell is a proper DOM element
        if (!(newCellDOM instanceof HTMLElement)) {
          throw new TypeError("Cellidi CellDisplay: Cell (" + x + ", " + y + ") callback must return a valid DOM element via its object (.dom property)");
        }
        
        // Add classname
        newCellDOM.classList.add("cellidi-cell");
        newCellDOM.cellidiCoords = [
          x, y
        ];
        
        if (setWidth) {
          newCellDOM.classList.add(setWidth);
        }
        
        // Add dead-cell class
        if (newCell.deadCell) {
          newCellDOM.classList.add("cellidi-dead-cell");
        }
        
        newCellDOM.cellidiActiveCell = !newCell.deadCell;
        
        // Add coords to cell model
        newCell.coords = [
          x, y
        ];
        
        newCell.virtualCoords = [
          virtual[0], virtual[1]
        ];
        
        // Append cell
        row.appendChild(newCellDOM);
        cellMap[x][y] = newCell;
        cellVirtualMap[virtual[0]][virtual[1]] = newCell;
        
        // Check for - and apply if found - cellspan
        var cellSpanY = Math.round(newCell.spanY);
        var cellSpanX = Math.round(newCell.spanX);
        
        if (cellSpanY > 1) {
          for (var j=1; j<cellSpanY; j++) {
            var newY = y + j;
            
            cellMap[x][newY] = newCell;
            
            var spanVirtualY = cd.toVirtualCoords(x, newY);
            cellVirtualMap[spanVirtualY[0]][spanVirtualY[1]] = newCell;
            
            // Mark next cells as "full"
            linkedCells[x][newY] = newCell;
            linkedCellList.push([x, newY]);
          }
          
          // Apply height
          newCellDOM.style.height = (100 * cellSpanY) + "%";
          newCellDOM.style.zIndex = 100; // To raise above other cells under it
        }
        
        // TODO: DRY this please
        if (cellSpanX > 1) {
          for (var k=1; k<cellSpanX; k++) {
            var newX = x + k;
            
            cellMap[newX][y] = newCell;
            
            var spanVirtualX = cd.toVirtualCoords(newX, y);
            cellVirtualMap[spanVirtualX[0]][spanVirtualX[1]] = newCell;
            
            // Mark next cells as "full"
            linkedCells[newX][y] = newCell;
            linkedCellList.push([newX, y]);
          }
          
          // Apply height
          newCellDOM.style.height = (100 * cellSpanX) + "%";
          newCellDOM.style.zIndex = 100; // To raise above other cells under it
        }
        
        // Append to tmparray
        tmpRowArray.push(newCell);
      } else {
        row.appendChild(jSh.d(".cellidi-cellspan-fill.cellidi-cell.cellidi-dead-cell" + (setWidth ? "." + setWidth : ""))); // Add empty cell to fill in space
        tmpRowArray.push(linkedCells[x][y]);
      }
    }
    
    // Add right header
    if (right) {
      var rightHeader = buildHorizontalHeader(cd, y, "right", right, setWidth);
      
      row.appendChild(rightHeader);
      cd.headers.right.push(rightHeader);
    }
    
    rowWrapper.appendChild(row);
    
    // Process row
    if (onRow) {
      onRow(row, tmpRowArray, y, virtual[1], cd);
    }
  }
  
  // Append rows
  wrapperFrag.appendChild(rowWrapper);
  
  // Build bottom header
  if (bottom) {
    var bottomHeader = buildVerticalHeader(cd, "bottom", bottom, width);
    
    // Append to main cell wrapper
    wrapperFrag.appendChild(cd.headers.domBottom = bottomHeader.wrapper);
    cd.headers.bottom = bottomHeader.map;
  }
  
  wrapper.appendChild(wrapperFrag);
}

module.exports = domCells;
