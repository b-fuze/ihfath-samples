// Include all required files

var construct = require("./construct");
var CellDisplay = require("./core/celldisplay");
var checkValidOptions = require("./core/options");

function Cellidi(options, data) {
  // Validate options
  var validOptions = checkValidOptions(options);
  
  // Check if options are invalid
  if (validOptions !== true) {
    // Fail
    throw validOptions;
  }
  
  // Construct the CellDisplay
  var cd = construct(options, data);
  
  return cd;
}

// Make CellDisplay's static functions accessible
Cellidi.CellDisplay = CellDisplay;

window.Cellidi = Cellidi;
