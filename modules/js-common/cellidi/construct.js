// Construct the CellDisplay

var CellDisplay = require("./core/celldisplay");
var domWrapper = require("./core/dom-wrapper");
var domCells = require("./core/dom-cells");
var addHistory = require("./misc/history").addHistory;
var addSelection = require("./misc/selection");
var addClipboard = require("./misc/clipboard");

function construct(userOptions, data) {
  var cd      = new CellDisplay(userOptions);
  var options = cd.options;
  
  // Attempt to set data, will most likely error on
  // improperly formed data
  if (Array.isArray(data)) {
    cd._state       = cd.genStateTable(data, options.emptyState);
    cd._presetState = true;
  }
  
  // Create wrapper element
  cd.wrapper = domWrapper(cd);
  
  // Create cells
  domCells(cd);
  
  // Add history feature
  if (options.history) {
    addHistory(cd);
  }
  
  // Add selection feature
  if (options.canSelect) {
    addSelection(cd);
  }
  
  // Add clipboard feature
  if (options.clipboard) {
    addClipboard(cd);
  }
  
  // Append wrapper to main element
  cd.options.element.appendChild(cd.wrapper);
  
  return cd;
}

module.exports = construct;
