// CellDisplay history feature

function addHistory(cd) {
  var options     = cd.options;
  var focus       = options.focus || cd.wrapper;
  var diffFunc    = options.diffStates || undefined;
  var emptyFunc   = options.emptyState || undefined;
  var linkedCells = cd._linkedCellsList.length && cd._linkedCellsList;
  var stack       = cd._historyStack = {
    buffer: null,
    history: [],
    future: [],
  };
  
  function historyShift(backward) {
    // Get history
    var newState = stateHistory(cd, null, backward, emptyFunc);
    
    if (linkedCells) {
      // Clear any linked cells (cells in the occupied space of cellspans)
      var emptyState = (emptyFunc || null) && emptyFunc();
      
      for (var i=0; i<linkedCells.length; i++) {
        var linkedCoord = linkedCells[i];
        
        cd._state[linkedCoord[0]][linkedCoord[1]] = emptyState;
        newState[linkedCoord[0]][linkedCoord[1]] = emptyState;
      }
    }
    
    // Diff states
    var diffHistory = cd.diffStateTables(newState, cd._state, [newState, cd._cellMap], diffFunc);
    
    // Call state callback
    cd.options.onCellStateChange(diffHistory.ref[0], diffHistory.ref[1]);
    
    // Set new state
    cd._state = newState;
    
    // Trigger newstate event
    cd.triggerEvent("newstate", {
      state: cd.genStateTable(newState, emptyFunc),
      _historyChange: true // Internal history flag, to determine if should log new history or not
    });
  }
  
  focus.addEventListener("keydown", function(e) {
    if (e.ctrlKey && e.keyCode === 90 /* Z Key */) {
      historyShift(e.shiftKey);
      
      e.preventDefault();
    }
  });
  
  cd.on("newstate", function(e) {
    if (!e._historyChange) {
      var state = e.state;
      
      stateHistory(cd, state, null, emptyFunc);
    } else {
      cd.triggerEvent("historystack", {
        previous: stack.history.length,
        future: stack.future.length,
      });
    }
  });
  
  // Reset when cellDisplay is rebuilt
  cd.on("rebuild", function() {
    linkedCells = cd._linkedCellsList.length && cd._linkedCellsList;
    stack       = cd._historyStack = {
      buffer: null,
      history: [],
      future: []
    };
  });
  
  // Add history events
  cd.addEvent("historystack");
  cd.shiftHistory = function(backward) {
    historyShift(!!backward);
  };
}

function stateHistory(cd, current, redo, emptyFunc) {
  var stack   = cd._historyStack;
  var history = stack.history;
  var future  = stack.future;
  
  if (current || redo) {
    // If redo flag is on, pop out latest redo from stack, and return it
    if (redo) {
      // Get latest stack from the future
      var lastRedo = future.pop();
      
      // Only push buffer to history if there's actually something in the future
      if (stack.buffer && lastRedo) {
        history.push(stack.buffer);
        
        // Push redo onto buffer
        stack.buffer = lastRedo;
      }
      
      // Return redo
      return lastRedo || stack.buffer || cd.genStateTable(cd._state, emptyFunc);
    }
    
    // Clear redos if not empty
    if (future.length > 0) {
      future = cd._historyStack.future = [];
    }
    
    // Copy the old history into a new array
    var currentCopy = cd.genStateTable(current, emptyFunc);
    
    if (stack.buffer) {
      // Push old history to history stack
      history.push(stack.buffer);
      
      // Truncate stack if too large
      if (history.length > cd.options.maxHistory) {
        history.shift();
      }
    }
    
    // Add current to buffer
    stack.buffer = currentCopy;
    
    return currentCopy;
  } else {
    // Pop the older history out
    var recentHistory = history.pop();
    
    // Push buffer to the future
    if (stack.buffer && recentHistory) {
      future.push(stack.buffer);
      
      // Save redo to buffer
      stack.buffer = recentHistory;
    }
    
    return recentHistory || stack.buffer || cd.genStateTable(cd._state, emptyFunc);
  }
}

module.exports = {
  addHistory: addHistory,
  stateHistory: stateHistory
};
