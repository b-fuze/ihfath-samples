// CellDisplay clipboard feature

function addClipboard(cd) {
  var options = cd.options;
  var focus   = options.focus || cd.wrapper;
  var mime    = "application/x-cellidi";
  
  focus.addEventListener("copy", function(e) {
    if (e.clipboardData) {
      e.clipboardData.setData(mime, serializeState(cd));
      
      e.preventDefault();
      e.stopPropagation();
    }
  });
  
  focus.addEventListener("paste", function(e) {
    if (e.clipboardData) {
      var state = e.clipboardData.getData(mime);
      
      if (state) {
        var frame = jSh.parseJSON(state);
        
        if (frame && !frame.error && validSource(cd, frame)) {
          cd.state = frame.state;
        }
      }
    }
  });
}

function serializeState(cd) {
  var options = cd.options;
  var frame   = {
    name: options.name,
    dimensions: options.dimensions,
    state: cd.state,
  };
  
  return JSON.stringify(frame);
}

function validSource(cd, foreign) {
  var options    = cd.options;
  var dimensions = options.dimensions;
  var valid      = false;
  
  if (
    options.name === foreign.name
    && foreign.dimensions
    && dimensions[0] === foreign.dimensions[0]
    && dimensions[1] === foreign.dimensions[1]
  ) {
    valid = true;
  }
  
  return valid;
}

module.exports = addClipboard;
