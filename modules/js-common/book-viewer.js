(function () {
	'use strict';

	function noop() {}

	function assign(tar, src) {
		for (var k in src) tar[k] = src[k];
		return tar;
	}

	function addLoc(element, file, line, column, char) {
		element.__svelte_meta = {
			loc: { file, line, column, char }
		};
	}

	function append(target, node) {
		target.appendChild(node);
	}

	function insert(target, node, anchor) {
		target.insertBefore(node, anchor);
	}

	function detachNode(node) {
		node.parentNode.removeChild(node);
	}

	function destroyEach(iterations, detach) {
		for (var i = 0; i < iterations.length; i += 1) {
			if (iterations[i]) iterations[i].d(detach);
		}
	}

	function createElement(name) {
		return document.createElement(name);
	}

	function createSvgElement(name) {
		return document.createElementNS('http://www.w3.org/2000/svg', name);
	}

	function createText(data) {
		return document.createTextNode(data);
	}

	function addListener(node, event, handler, options) {
		node.addEventListener(event, handler, options);
	}

	function removeListener(node, event, handler, options) {
		node.removeEventListener(event, handler, options);
	}

	function setAttribute(node, attribute, value) {
		if (value == null) node.removeAttribute(attribute);
		else node.setAttribute(attribute, value);
	}

	function setData(text, data) {
		text.data = '' + data;
	}

	function toggleClass(element, name, toggle) {
		element.classList.toggle(name, !!toggle);
	}

	function blankObject() {
		return Object.create(null);
	}

	function destroy(detach) {
		this.destroy = noop;
		this.fire('destroy');
		this.set = noop;

		this._fragment.d(detach !== false);
		this._fragment = null;
		this._state = {};
	}

	function destroyDev(detach) {
		destroy.call(this, detach);
		this.destroy = function() {
			console.warn('Component was already destroyed');
		};
	}

	function _differs(a, b) {
		return a != a ? b == b : a !== b || ((a && typeof a === 'object') || typeof a === 'function');
	}

	function fire(eventName, data) {
		var handlers =
			eventName in this._handlers && this._handlers[eventName].slice();
		if (!handlers) return;

		for (var i = 0; i < handlers.length; i += 1) {
			var handler = handlers[i];

			if (!handler.__calling) {
				try {
					handler.__calling = true;
					handler.call(this, data);
				} finally {
					handler.__calling = false;
				}
			}
		}
	}

	function flush(component) {
		component._lock = true;
		callAll(component._beforecreate);
		callAll(component._oncreate);
		callAll(component._aftercreate);
		component._lock = false;
	}

	function get() {
		return this._state;
	}

	function init(component, options) {
		component._handlers = blankObject();
		component._slots = blankObject();
		component._bind = options._bind;
		component._staged = {};

		component.options = options;
		component.root = options.root || component;
		component.store = options.store || component.root.store;

		if (!options.root) {
			component._beforecreate = [];
			component._oncreate = [];
			component._aftercreate = [];
		}
	}

	function on(eventName, handler) {
		var handlers = this._handlers[eventName] || (this._handlers[eventName] = []);
		handlers.push(handler);

		return {
			cancel: function() {
				var index = handlers.indexOf(handler);
				if (~index) handlers.splice(index, 1);
			}
		};
	}

	function set(newState) {
		this._set(assign({}, newState));
		if (this.root._lock) return;
		flush(this.root);
	}

	function _set(newState) {
		var oldState = this._state,
			changed = {},
			dirty = false;

		newState = assign(this._staged, newState);
		this._staged = {};

		for (var key in newState) {
			if (this._differs(newState[key], oldState[key])) changed[key] = dirty = true;
		}
		if (!dirty) return;

		this._state = assign(assign({}, oldState), newState);
		this._recompute(changed, this._state);
		if (this._bind) this._bind(changed, this._state);

		if (this._fragment) {
			this.fire("state", { changed: changed, current: this._state, previous: oldState });
			this._fragment.p(changed, this._state);
			this.fire("update", { changed: changed, current: this._state, previous: oldState });
		}
	}

	function _stage(newState) {
		assign(this._staged, newState);
	}

	function setDev(newState) {
		if (typeof newState !== 'object') {
			throw new Error(
				this._debugName + '.set was called without an object of data key-values to update.'
			);
		}

		this._checkReadOnly(newState);
		set.call(this, newState);
	}

	function callAll(fns) {
		while (fns && fns.length) fns.shift()();
	}

	function _mount(target, anchor) {
		this._fragment[this._fragment.i ? 'i' : 'm'](target, anchor || null);
	}

	var protoDev = {
		destroy: destroyDev,
		get,
		fire,
		on,
		set: setDev,
		_recompute: noop,
		_set,
		_stage,
		_mount,
		_differs
	};

	/* src/app-viewer/App.html generated by Svelte v2.15.3 */

	function data() {
	  return {
	    pageBase: "",
	    currentPage: 1, // 1-index based
	    currentPageHash: "",
	    userInputPage: "", // Number

	    overlay: false,
	    useOverlay: true,
	    showClose: false,

	    zoom: 1,
	    maxZoom: 4,
	    moving: false,
	    zoomOffsetX: 0,
	    zoomOffsetY: 0,

	    dropdownOpen: false,
	    headerHeight: 0,
	    bookList: [
	      // {
	      //   id: "",
	      //   title: "",
	      // },
	    ],
	    book: {
	      id: "",
	      title: "",
	      previewPage: "",
	      pages: 0,
	      direction: 0, // 0: ltr, 1: rtl
	      // pageData: [],
	      _lastPage: 1,
	    },
	  }
	}
	var methods = {
	  toggleOverlay() {
	    const { overlay } = this.get();
	    this.set({
	      overlay: !overlay,
	    });
	  },

	  filterUserPage() {
	    const { book, currentPage, userInputPage } = this.get();
	    const { pages } = book;

	    let parsedPage = parseInt(userInputPage, 10);

	    if (isNaN(parsedPage)) {
	      return this.set({
	        userInputPage: currentPage,
	      });
	    }

	    if (parsedPage > pages) {
	      parsedPage = pages;
	    }

	    if (parsedPage < 1) {
	      parsedPage = 1;
	    }

	    this.setPage(parsedPage);
	  },

	  zoom(amount) {
	    const { zoom, maxZoom, zoomOffsetX, zoomOffsetY } = this.get();
	    const pageRect = this.refs.page.getBoundingClientRect();
	    const width = pageRect.right - pageRect.left;
	    const height = pageRect.bottom - pageRect.top;

	    let newZoom = amount === null ? 1 : Math.min(Math.max(zoom + amount, 1), maxZoom);
	    amount = amount === null ? 0 : amount;

	    const maxX = (newZoom * width) - width;
	    const maxY = (newZoom * height) - height;

	    // Center the new zoom
	    let newX = Math.max(Math.min(zoomOffsetX, maxX) + (((maxX / 2) / zoom * 2) * amount), 0);
	    let newY = Math.max(Math.min(zoomOffsetY, maxY) + (((maxY / 2) / zoom * 2) * amount), 0);

	    if (newZoom !== zoom) {
	      this.set({
	        zoom: newZoom,
	        zoomOffsetX: newX,
	        zoomOffsetY: newY,
	      });
	    }
	  },

	  move(e) {
	    e.preventDefault();
	    const that = this;

	    const { zoom, zoomOffsetX, zoomOffsetY } = this.get();
	    const pageRect = this.refs.page.getBoundingClientRect();
	    const width = pageRect.right - pageRect.left;
	    const height = pageRect.bottom - pageRect.top;
	    const baseX = e.clientX;
	    const baseY = e.clientY;

	    const minX = 0;
	    const minY = 0;
	    const maxX = (zoom * width) - width;
	    const maxY = (zoom * height) - height;

	    console.log(width, height, minX, minX, maxX, maxY, zoom);
	    console.log(zoomOffsetX, zoomOffsetY);

	    const move = (e) => {
	      const xDiff = baseX - e.clientX;
	      const yDiff = baseY - e.clientY;

	      this.set({
	        zoomOffsetX: Math.min(Math.max(zoomOffsetX + xDiff, minX), maxX),
	        zoomOffsetY: Math.min(Math.max(zoomOffsetY + yDiff, minY), maxY),
	      });
	    };

	    window.addEventListener("mousemove", move);
	    window.addEventListener("mouseup", function clear() {
	      window.removeEventListener("mousemove", move);
	      window.removeEventListener("mouseup", clear);

	      that.set({
	        moving: false,
	      });
	      that.fire("moving", {
	        moving: false,
	      });
	    });

	    this.set({
	      moving: true,
	    });
	    this.fire("moving", {
	      moving: true,
	    });
	  },

	  setPage(pageNumInput) {
	    const { book } = this.get();
	    // const pageNum = pageData[pageNumInput] ? pageNumInput : 1;
	    // const page = pageData[pageNum] || pageData[0];

	    // Request page
	    // if (page) {
	      this.fire("pagerequest", {
	        page: pageNumInput,
	      });
	    // }
	  },

	  next() {
	    const { book: { pages }, currentPage } = this.get();
	    const newPage = Math.min(currentPage + 1, pages);

	    if (newPage !== currentPage) {
	      this.setPage(newPage);
	    }
	  },

	  prev() {
	    const { currentPage } = this.get();
	    const newPage = Math.max(currentPage - 1, 1);

	    if (newPage !== currentPage) {
	      this.setPage(newPage);
	    }
	  },

	  requestBook(bookId) {
	    this.toggleDropdown(false);
	    this.fire("bookrequest", {
	      id: bookId,
	    });
	  },

	  propagateClose() {
	    const { useOverlay } = this.get();

	    if (useOverlay) {
	      this.set({
	        overlay: false,
	      });
	    }

	    this.fire("close");
	  },

	  toggleDropdown(open) {
	    if (!this._ddBlurCb) {
	      const blur = (e) => {
	        let target = e.target;

	        while (target !== document.body) {
	          if (target === this.refs.booklist) {
	            return;
	          }

	          target = target.parentNode;
	        }

	        this.set({
	          dropdownOpen: false,
	        });
	        window.removeEventListener("click", blur);
	        this._ddBlurCb = null;
	      };

	      this._ddBlurCb = blur;
	      window.addEventListener("click", blur);
	    }

	    this.set({
	      dropdownOpen: open,
	    });
	  },
	};

	const file = "src/app-viewer/App.html";

	function click_handler(event) {
		const { component, ctx } = this._svelte;

		component.requestBook(ctx.bookEntry.id);
	}

	function get_each_context(ctx, list, i) {
		const child_ctx = Object.create(ctx);
		child_ctx.bookEntry = list[i];
		return child_ctx;
	}

	function create_main_fragment(component, ctx) {
		var text0, div12, div1, div0, div0_style_value, text1, div3, span0, text2_value = ctx.book.title, text2, text3, div2, text4, div3_style_value, text5, div7, div4, svg0, path0, text6, div5, svg1, path1, text7, div6, svg2, path2, text8, div10, div8, i0, text9, div9, i1, text10, div11, input, input_updating = false, text11, span1, text12_value = " / " + ctx.book.pages, text12;

		var if_block0 = (ctx.useOverlay) && create_if_block_2(component, ctx);

		function mousedown_handler(event) {
			component.move(event);
		}

		var if_block1 = (ctx.bookList.length) && create_if_block_1(component, ctx);

		var if_block2 = (ctx.showClose) && create_if_block(component, ctx);

		function click_handler_1(event) {
			component.zoom(0.5);
		}

		function click_handler_2(event) {
			component.zoom(-1);
		}

		function click_handler_3(event) {
			component.zoom(null);
		}

		function click_handler_4(event) {
			component.prev();
		}

		function click_handler_5(event) {
			component.next();
		}

		function input_input_handler() {
			input_updating = true;
			component.set({ userInputPage: input.value });
			input_updating = false;
		}

		function change_handler(event) {
			component.filterUserPage();
		}

		return {
			c: function create() {
				if (if_block0) if_block0.c();
				text0 = createText("\n");
				div12 = createElement("div");
				div1 = createElement("div");
				div0 = createElement("div");
				text1 = createText("\n  ");
				div3 = createElement("div");
				span0 = createElement("span");
				text2 = createText(text2_value);
				text3 = createText("\n    ");
				div2 = createElement("div");
				if (if_block1) if_block1.c();
				text4 = createText("\n\n      ");
				if (if_block2) if_block2.c();
				text5 = createText("\n  \n  ");
				div7 = createElement("div");
				div4 = createElement("div");
				svg0 = createSvgElement("svg");
				path0 = createSvgElement("path");
				text6 = createText("\n    ");
				div5 = createElement("div");
				svg1 = createSvgElement("svg");
				path1 = createSvgElement("path");
				text7 = createText("\n    ");
				div6 = createElement("div");
				svg2 = createSvgElement("svg");
				path2 = createSvgElement("path");
				text8 = createText("\n  ");
				div10 = createElement("div");
				div8 = createElement("div");
				i0 = createElement("i");
				text9 = createText("\n    ");
				div9 = createElement("div");
				i1 = createElement("i");
				text10 = createText("\n  ");
				div11 = createElement("div");
				input = createElement("input");
				text11 = createText("\n    ");
				span1 = createElement("span");
				text12 = createText(text12_value);
				div0.className = "ihfbv-page-render svelte-27jo0k";
				div0.style.cssText = div0_style_value = (ctx.currentPageHash ? `background-image: url('${ctx.pageBase}/${ctx.currentPageHash}');` : "")
	             + (`transform: translate(${ -ctx.zoomOffsetX }px, ${ -ctx.zoomOffsetY }px) scale(${ ctx.zoom }, ${ ctx.zoom });`);
				addLoc(div0, file, 18, 4, 440);
				addListener(div1, "mousedown", mousedown_handler);
				div1.className = "ihfbv-page svelte-27jo0k";
				toggleClass(div1, "ihfbv-is-zoomed", ctx.zoom > 1);
				toggleClass(div1, "ihfbv-is-moving", ctx.moving);
				addLoc(div1, file, 12, 2, 291);
				span0.className = "ihfbv-title-caption svelte-27jo0k";
				addLoc(span0, file, 26, 4, 817);
				div2.className = "ihfbv-right svelte-27jo0k";
				addLoc(div2, file, 27, 4, 875);
				div3.className = "ihfbv-title ihfbv-fade svelte-27jo0k";
				div3.style.cssText = div3_style_value = "height: " + (ctx.headerHeight ? ctx.headerHeight : 60) + "px;";
				addLoc(div3, file, 23, 2, 704);
				setAttribute(path0, "d", "M13 10h-3v3h-2v-3h-3v-2h3v-3h2v3h3v2zm8.172 14l-7.387-7.387c-1.388.874-3.024 1.387-4.785 1.387-4.971 0-9-4.029-9-9s4.029-9 9-9 9 4.029 9 9c0 1.761-.514 3.398-1.387 4.785l7.387 7.387-2.828 2.828zm-12.172-8c3.859 0 7-3.14 7-7s-3.141-7-7-7-7 3.14-7 7 3.141 7 7 7z");
				setAttribute(path0, "class", "svelte-27jo0k");
				addLoc(path0, file, 69, 8, 1994);
				setAttribute(svg0, "xmlns", "http://www.w3.org/2000/svg");
				setAttribute(svg0, "viewBox", "0 0 24 24");
				setAttribute(svg0, "class", "svelte-27jo0k");
				addLoc(svg0, file, 66, 6, 1909);
				addListener(div4, "click", click_handler_1);
				div4.className = "ihfbv-zoom-in svelte-27jo0k";
				addLoc(div4, file, 63, 4, 1842);
				setAttribute(path1, "d", "M13 10h-8v-2h8v2zm8.172 14l-7.387-7.387c-1.388.874-3.024 1.387-4.785 1.387-4.971 0-9-4.029-9-9s4.029-9 9-9 9 4.029 9 9c0 1.761-.514 3.398-1.387 4.785l7.387 7.387-2.828 2.828zm-12.172-8c3.859 0 7-3.14 7-7s-3.141-7-7-7-7 3.14-7 7 3.141 7 7 7z");
				setAttribute(path1, "class", "svelte-27jo0k");
				addLoc(path1, file, 78, 8, 2447);
				setAttribute(svg1, "xmlns", "http://www.w3.org/2000/svg");
				setAttribute(svg1, "viewBox", "0 0 24 24");
				setAttribute(svg1, "class", "svelte-27jo0k");
				addLoc(svg1, file, 75, 6, 2362);
				addListener(div5, "click", click_handler_2);
				div5.className = "ihfbv-zoom-out svelte-27jo0k";
				addLoc(div5, file, 72, 4, 2295);
				setAttribute(path2, "d", "M13 8h-8v-2h8v2zm0 4h-8v-2h8v2zm8.172 12l-7.387-7.387c-1.388.874-3.024 1.387-4.785 1.387-4.971 0-9-4.029-9-9s4.029-9 9-9 9 4.029 9 9c0 1.761-.514 3.398-1.387 4.785l7.387 7.387-2.828 2.828zm-12.172-8c3.859 0 7-3.14 7-7s-3.141-7-7-7-7 3.14-7 7 3.141 7 7 7z");
				setAttribute(path2, "class", "svelte-27jo0k");
				addLoc(path2, file, 87, 8, 2882);
				setAttribute(svg2, "xmlns", "http://www.w3.org/2000/svg");
				setAttribute(svg2, "viewBox", "0 0 24 24");
				setAttribute(svg2, "class", "svelte-27jo0k");
				addLoc(svg2, file, 84, 6, 2797);
				addListener(div6, "click", click_handler_3);
				div6.className = "ihfbv-zoom-out svelte-27jo0k";
				addLoc(div6, file, 81, 4, 2728);
				div7.className = "ihfbv-zoom ihfbv-fade svelte-27jo0k";
				addLoc(div7, file, 62, 2, 1802);
				i0.className = "fa fa-angle-left svelte-27jo0k";
				addLoc(i0, file, 97, 6, 3345);
				addListener(div8, "click", click_handler_4);
				div8.className = "ihfbv-prev svelte-27jo0k";
				addLoc(div8, file, 94, 4, 3284);
				i1.className = "fa fa-angle-right svelte-27jo0k";
				addLoc(i1, file, 102, 6, 3454);
				addListener(div9, "click", click_handler_5);
				div9.className = "ihfbv-next svelte-27jo0k";
				addLoc(div9, file, 99, 4, 3393);
				div10.className = "ihfbv-controls ihfbv-fade svelte-27jo0k";
				toggleClass(div10, "ihfbv-inverted-direction", ctx.book.direction);
				addLoc(div10, file, 91, 2, 3184);
				addListener(input, "input", input_input_handler);
				addListener(input, "change", change_handler);
				setAttribute(input, "type", "text");
				input.className = "ihfbv-pagenum-input svelte-27jo0k";
				addLoc(input, file, 106, 4, 3553);
				span1.className = "svelte-27jo0k";
				addLoc(span1, file, 111, 4, 3683);
				div11.className = "ihfbv-pagenum ihfbv-fade svelte-27jo0k";
				addLoc(div11, file, 105, 2, 3510);
				div12.className = "ihfbv-wrap svelte-27jo0k";
				toggleClass(div12, "ihfbv-embed", !ctx.useOverlay);
				toggleClass(div12, "ihfbv-active", ctx.useOverlay ? ctx.overlay : true);
				toggleClass(div12, "ihfbv-is-moving", ctx.moving);
				addLoc(div12, file, 7, 0, 144);
			},

			m: function mount(target, anchor) {
				if (if_block0) if_block0.m(target, anchor);
				insert(target, text0, anchor);
				insert(target, div12, anchor);
				append(div12, div1);
				append(div1, div0);
				component.refs.page = div1;
				append(div12, text1);
				append(div12, div3);
				append(div3, span0);
				append(span0, text2);
				append(div3, text3);
				append(div3, div2);
				if (if_block1) if_block1.m(div2, null);
				append(div2, text4);
				if (if_block2) if_block2.m(div2, null);
				append(div12, text5);
				append(div12, div7);
				append(div7, div4);
				append(div4, svg0);
				append(svg0, path0);
				append(div7, text6);
				append(div7, div5);
				append(div5, svg1);
				append(svg1, path1);
				append(div7, text7);
				append(div7, div6);
				append(div6, svg2);
				append(svg2, path2);
				append(div12, text8);
				append(div12, div10);
				append(div10, div8);
				append(div8, i0);
				append(div10, text9);
				append(div10, div9);
				append(div9, i1);
				append(div12, text10);
				append(div12, div11);
				append(div11, input);

				input.value = ctx.userInputPage;

				append(div11, text11);
				append(div11, span1);
				append(span1, text12);
			},

			p: function update(changed, ctx) {
				if (ctx.useOverlay) {
					if (if_block0) {
						if_block0.p(changed, ctx);
					} else {
						if_block0 = create_if_block_2(component, ctx);
						if_block0.c();
						if_block0.m(text0.parentNode, text0);
					}
				} else if (if_block0) {
					if_block0.d(1);
					if_block0 = null;
				}

				if ((changed.currentPageHash || changed.pageBase || changed.zoomOffsetX || changed.zoomOffsetY || changed.zoom) && div0_style_value !== (div0_style_value = (ctx.currentPageHash ? `background-image: url('${ctx.pageBase}/${ctx.currentPageHash}');` : "")
	             + (`transform: translate(${ -ctx.zoomOffsetX }px, ${ -ctx.zoomOffsetY }px) scale(${ ctx.zoom }, ${ ctx.zoom });`))) {
					div0.style.cssText = div0_style_value;
				}

				if (changed.zoom) {
					toggleClass(div1, "ihfbv-is-zoomed", ctx.zoom > 1);
				}

				if (changed.moving) {
					toggleClass(div1, "ihfbv-is-moving", ctx.moving);
				}

				if ((changed.book) && text2_value !== (text2_value = ctx.book.title)) {
					setData(text2, text2_value);
				}

				if (ctx.bookList.length) {
					if (if_block1) {
						if_block1.p(changed, ctx);
					} else {
						if_block1 = create_if_block_1(component, ctx);
						if_block1.c();
						if_block1.m(div2, text4);
					}
				} else if (if_block1) {
					if_block1.d(1);
					if_block1 = null;
				}

				if (ctx.showClose) {
					if (!if_block2) {
						if_block2 = create_if_block(component, ctx);
						if_block2.c();
						if_block2.m(div2, null);
					}
				} else if (if_block2) {
					if_block2.d(1);
					if_block2 = null;
				}

				if ((changed.headerHeight) && div3_style_value !== (div3_style_value = "height: " + (ctx.headerHeight ? ctx.headerHeight : 60) + "px;")) {
					div3.style.cssText = div3_style_value;
				}

				if (changed.book) {
					toggleClass(div10, "ihfbv-inverted-direction", ctx.book.direction);
				}

				if (!input_updating && changed.userInputPage) input.value = ctx.userInputPage;
				if ((changed.book) && text12_value !== (text12_value = " / " + ctx.book.pages)) {
					setData(text12, text12_value);
				}

				if (changed.useOverlay) {
					toggleClass(div12, "ihfbv-embed", !ctx.useOverlay);
				}

				if ((changed.useOverlay || changed.overlay)) {
					toggleClass(div12, "ihfbv-active", ctx.useOverlay ? ctx.overlay : true);
				}

				if (changed.moving) {
					toggleClass(div12, "ihfbv-is-moving", ctx.moving);
				}
			},

			d: function destroy$$1(detach) {
				if (if_block0) if_block0.d(detach);
				if (detach) {
					detachNode(text0);
					detachNode(div12);
				}

				removeListener(div1, "mousedown", mousedown_handler);
				if (component.refs.page === div1) component.refs.page = null;
				if (if_block1) if_block1.d();
				if (if_block2) if_block2.d();
				removeListener(div4, "click", click_handler_1);
				removeListener(div5, "click", click_handler_2);
				removeListener(div6, "click", click_handler_3);
				removeListener(div8, "click", click_handler_4);
				removeListener(div9, "click", click_handler_5);
				removeListener(input, "input", input_input_handler);
				removeListener(input, "change", change_handler);
			}
		};
	}

	// (1:0) {#if useOverlay}
	function create_if_block_2(component, ctx) {
		var div;

		function click_handler(event) {
			component.toggleOverlay();
		}

		return {
			c: function create() {
				div = createElement("div");
				addListener(div, "click", click_handler);
				div.className = "ihfbv-overlay svelte-27jo0k";
				toggleClass(div, "ihfbv-active", ctx.useOverlay && ctx.overlay);
				addLoc(div, file, 1, 2, 19);
			},

			m: function mount(target, anchor) {
				insert(target, div, anchor);
			},

			p: function update(changed, ctx) {
				if ((changed.useOverlay || changed.overlay)) {
					toggleClass(div, "ihfbv-active", ctx.useOverlay && ctx.overlay);
				}
			},

			d: function destroy$$1(detach) {
				if (detach) {
					detachNode(div);
				}

				removeListener(div, "click", click_handler);
			}
		};
	}

	// (29:6) {#if bookList.length}
	function create_if_block_1(component, ctx) {
		var div2, div0, span, text0_value = ctx.book.title, text0, text1, i, text2, div1;

		function click_handler(event) {
			component.toggleDropdown(true);
		}

		var each_value = ctx.bookList;

		var each_blocks = [];

		for (var i_1 = 0; i_1 < each_value.length; i_1 += 1) {
			each_blocks[i_1] = create_each_block(component, get_each_context(ctx, each_value, i_1));
		}

		return {
			c: function create() {
				div2 = createElement("div");
				div0 = createElement("div");
				span = createElement("span");
				text0 = createText(text0_value);
				text1 = createText("\n            ");
				i = createElement("i");
				text2 = createText("\n          ");
				div1 = createElement("div");

				for (var i_1 = 0; i_1 < each_blocks.length; i_1 += 1) {
					each_blocks[i_1].c();
				}
				span.className = "svelte-27jo0k";
				addLoc(span, file, 35, 12, 1126);
				i.className = "fa fa-angle-down svelte-27jo0k";
				addLoc(i, file, 36, 12, 1164);
				addListener(div0, "click", click_handler);
				div0.className = "ihfbv-dropdown-trigger svelte-27jo0k";
				addLoc(div0, file, 32, 10, 1021);
				div1.className = "ihfbv-dropdown svelte-27jo0k";
				toggleClass(div1, "ihfbv-active", ctx.dropdownOpen);
				addLoc(div1, file, 38, 10, 1224);
				div2.className = "ihfbv-input ihfbv-booklist svelte-27jo0k";
				addLoc(div2, file, 29, 8, 937);
			},

			m: function mount(target, anchor) {
				insert(target, div2, anchor);
				append(div2, div0);
				append(div0, span);
				append(span, text0);
				append(div0, text1);
				append(div0, i);
				append(div2, text2);
				append(div2, div1);

				for (var i_1 = 0; i_1 < each_blocks.length; i_1 += 1) {
					each_blocks[i_1].m(div1, null);
				}

				component.refs.booklist = div2;
			},

			p: function update(changed, ctx) {
				if ((changed.book) && text0_value !== (text0_value = ctx.book.title)) {
					setData(text0, text0_value);
				}

				if (changed.bookList) {
					each_value = ctx.bookList;

					for (var i_1 = 0; i_1 < each_value.length; i_1 += 1) {
						const child_ctx = get_each_context(ctx, each_value, i_1);

						if (each_blocks[i_1]) {
							each_blocks[i_1].p(changed, child_ctx);
						} else {
							each_blocks[i_1] = create_each_block(component, child_ctx);
							each_blocks[i_1].c();
							each_blocks[i_1].m(div1, null);
						}
					}

					for (; i_1 < each_blocks.length; i_1 += 1) {
						each_blocks[i_1].d(1);
					}
					each_blocks.length = each_value.length;
				}

				if (changed.dropdownOpen) {
					toggleClass(div1, "ihfbv-active", ctx.dropdownOpen);
				}
			},

			d: function destroy$$1(detach) {
				if (detach) {
					detachNode(div2);
				}

				removeListener(div0, "click", click_handler);

				destroyEach(each_blocks, detach);

				if (component.refs.booklist === div2) component.refs.booklist = null;
			}
		};
	}

	// (42:12) {#each bookList as bookEntry}
	function create_each_block(component, ctx) {
		var div, text_value = ctx.bookEntry.title, text;

		return {
			c: function create() {
				div = createElement("div");
				text = createText(text_value);
				div._svelte = { component, ctx };

				addListener(div, "click", click_handler);
				div.className = "ihfbv-option svelte-27jo0k";
				addLoc(div, file, 42, 14, 1367);
			},

			m: function mount(target, anchor) {
				insert(target, div, anchor);
				append(div, text);
			},

			p: function update(changed, _ctx) {
				ctx = _ctx;
				if ((changed.bookList) && text_value !== (text_value = ctx.bookEntry.title)) {
					setData(text, text_value);
				}

				div._svelte.ctx = ctx;
			},

			d: function destroy$$1(detach) {
				if (detach) {
					detachNode(div);
				}

				removeListener(div, "click", click_handler);
			}
		};
	}

	// (53:6) {#if showClose}
	function create_if_block(component, ctx) {
		var div, i;

		function click_handler_1(event) {
			component.propagateClose();
		}

		return {
			c: function create() {
				div = createElement("div");
				i = createElement("i");
				i.className = "fa fa-times svelte-27jo0k";
				addLoc(i, file, 56, 10, 1709);
				addListener(div, "click", click_handler_1);
				div.className = "ihfbv-input ihfbv-close svelte-27jo0k";
				addLoc(div, file, 53, 8, 1613);
			},

			m: function mount(target, anchor) {
				insert(target, div, anchor);
				append(div, i);
			},

			d: function destroy$$1(detach) {
				if (detach) {
					detachNode(div);
				}

				removeListener(div, "click", click_handler_1);
			}
		};
	}

	function App(options) {
		this._debugName = '<App>';
		if (!options || (!options.target && !options.root)) {
			throw new Error("'target' is a required option");
		}

		init(this, options);
		this.refs = {};
		this._state = assign(data(), options.data);
		if (!('useOverlay' in this._state)) console.warn("<App> was created without expected data property 'useOverlay'");
		if (!('overlay' in this._state)) console.warn("<App> was created without expected data property 'overlay'");
		if (!('moving' in this._state)) console.warn("<App> was created without expected data property 'moving'");
		if (!('zoom' in this._state)) console.warn("<App> was created without expected data property 'zoom'");
		if (!('currentPageHash' in this._state)) console.warn("<App> was created without expected data property 'currentPageHash'");
		if (!('pageBase' in this._state)) console.warn("<App> was created without expected data property 'pageBase'");
		if (!('zoomOffsetX' in this._state)) console.warn("<App> was created without expected data property 'zoomOffsetX'");
		if (!('zoomOffsetY' in this._state)) console.warn("<App> was created without expected data property 'zoomOffsetY'");
		if (!('headerHeight' in this._state)) console.warn("<App> was created without expected data property 'headerHeight'");
		if (!('book' in this._state)) console.warn("<App> was created without expected data property 'book'");
		if (!('bookList' in this._state)) console.warn("<App> was created without expected data property 'bookList'");
		if (!('dropdownOpen' in this._state)) console.warn("<App> was created without expected data property 'dropdownOpen'");
		if (!('showClose' in this._state)) console.warn("<App> was created without expected data property 'showClose'");
		if (!('userInputPage' in this._state)) console.warn("<App> was created without expected data property 'userInputPage'");
		this._intro = true;

		this._fragment = create_main_fragment(this, this._state);

		if (options.target) {
			if (options.hydrate) throw new Error("options.hydrate only works if the component was compiled with the `hydratable: true` option");
			this._fragment.c();
			this._mount(options.target, options.anchor);
		}
	}

	assign(App.prototype, protoDev);
	assign(App.prototype, methods);

	App.prototype._checkReadOnly = function _checkReadOnly(newState) {
	};

	// @ts-check

	function main(env) {
	  const locale = env.locale;
	  const pageCache = {
	    // "BOOK_ID": {
	    //   "PAGE_NUM": "PAGE_ID",
	    // },
	  };

	  const app = new App({
	    target: env.root,
	    data: {
	      pageBase: env.pageBase,
	      overlay: false,
	      useOverlay: "useOverlay" in env ? env.useOverlay : true,
	      headerHeight: "headerHeight" in env ? env.headerHeight : 60,
	      showClose: "showClose" in env ? env.showClose : false,
	    },
	  });

	  return {
	    open(book, page = 1) {
	      if (!pageCache[book.id]) {
	        pageCache[book.id] = {};
	      }

	      // Add preview page (first page) to cache
	      pageCache[book.id][1] = book.previewPage;

	      const newState = {
	        overlay: true,
	        book,
	      };

	      if (page === 1) {
	        Object.assign(newState, {
	          currentPage: page,
	          userInputPage: page,
	          currentPageHash: book.previewPage,
	        });
	      } else {
	        // Request page after this tick
	        setTimeout(() => {
	          app.fire("pagerequest", {
	            page,
	          });
	        }, 0);
	      }

	      app.set(newState);
	    },

	    openPage(page, hash) {
	      const { book } = app.get();
	      pageCache[book.id][page] = hash;

	      app.set({
	        currentPage: page,
	        userInputPage: page,
	        currentPageHash: hash,
	      });
	    },

	    setBooklist(bookList) {
	      console.log("BOOKLIST", bookList);
	      app.set({
	        bookList,
	      });
	    },

	    onrequest(cb) {
	      app.on("bookrequest", cb);
	    },

	    onpage(cb) {
	      app.on("pagerequest", function({ page }) {
	        const { book } = app.get();
	        const cache = pageCache[book.id];

	        if (cache[page]) {
	          book._lastPage = page;

	          app.set({
	            currentPage: page,
	            userInputPage: page,
	            currentPageHash: cache[page],
	            book,
	          });
	        } else {
	          cb(page, book.id);
	        }
	      });
	    },

	    onclose(cb) {
	      app.on("close", cb);
	    },

	    onmove(cb) {
	      app.on("moving", cb);
	    },

	    page(number) {
	      app.setPage(number);
	    },

	    toggle() {
	      const { overlay } = app.get();

	      app.set({
	        overlay: !overlay,
	      });
	    },

	    show() {
	      app.set({
	        overlay: true,
	      });
	    },

	    hide() {
	      app.set({
	        overlay: false,
	      });
	    }
	  };
	}

	window.ihfathBookViewer = main;

}());
//# sourceMappingURL=book-viewer.js.map
