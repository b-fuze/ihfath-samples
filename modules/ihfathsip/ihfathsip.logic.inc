<?php

// Ihfath SIP code
//
// Prefix (For autocomplete): ihfathsip_

namespace IhfathSip;

class Base {
  static $_cache = array(
    "users"    => array(),
    "orgs"     => array(),
    "orgs_ref" => array(),
  );
  
  static function checkValidOrg($oid, $exists = TRUE) {
    if (!is_numeric($oid)) {
      return NULL;
    }
    
    if (!$exists) {
      return (int) $oid;
    }
    
    // Check caches
    if (isset(self::$_cache["orgs_ref"][$oid]) || isset(self::$_cache["orgs"][$oid])) {
      return (int) $oid;
    }
    
    // Check database
    $query = db_select("ihfath_sip_org", "iso");
    $query->condition("iso.oid", $oid, "=");
    $query->addExpression("COUNT(*)", "count");
    
    $count = (int) $query->execute()->fetchField();
    
    if ($count) {
      self::$_cache["orgs_ref"][$oid] = TRUE;
      return (int) $oid;
    } else {
      return NULL;
    }
  }
  
  // Loading methods
  static function loadUser($uid) {
    if (!is_numeric($uid)) {
      die("\$uid should be a number");
    }
    
    // Check cache
    if (isset(self::$_cache["users"][$uid])) {
      return self::$_cache["users"][$uid];
    } else {
      $query = db_select("ihfath_sip_user", "isu");
      $query->fields("isu");
      $query->condition("isu.uid", $uid, "=");
      
      $user = (object) $query->execute()->fetchAssoc();
      
      // Put in cache
      self::$_cache["users"][$uid] = $user;
      return $user;
    }
  }
  
  static function loadOrg($oid) {
    if (!is_numeric($oid)) {
      die("\$oid should be a number");
    }
    
    // Check cache
    if (isset(self::$_cache["orgs"][$oid])) {
      return self::$_cache["orgs"][$oid];
    } else {
      $query = db_select("ihfath_sip_org", "iso");
      $query->fields("iso");
      $query->condition("iso.oid", $oid, "=");
      
      $org = (object) $query->execute()->fetchAssoc();
      
      // Put in cache
      self::$_cache["orgs"][$oid] = $org;
      return $org;
    }
  }
  
  static function loadOrgs() {
    $query = db_select("ihfath_sip_org", "iso");
    $query->fields("iso");
    
    return $query->execute()->fetchAllAssoc("oid");
  }
  
  // Org CRUD
  static function registerOrg($values) {
    // Check values
    $errors = self::_checkOrgRegisterValues($values, array(
      "new_org_id" => array("int", "You must provide a valid organization ID"),
      "new_org_name" => array("str", "You must provide an organization name"),
      "new_org_domain" => array("domain", "You must provide a valid OnSIP organization domain"),
      "new_org_username" => array("machinename", "You must provide a valid OnSIP organization admin username"),
      "new_org_pass" => array("str", "You must provide an OnSIP organization admin password"),
    ));
    
    if (count($errors) === 0) {
      // No errors, register organization
      $query = db_insert("ihfath_sip_org");
      $query->fields(array(
        "name" => $values["new_org_name"],
        "domain" => $values["new_org_domain"],
        "org_id" => $values["new_org_id"],
        "username" => $values["new_org_username"],
        "password" => $values["new_org_pass"],
      ));
      
      $new_id = $query->execute();
      
      if (is_numeric($new_id)) {
        drupal_set_message(t("OnSIP organization @org registered successfully", array("@org" => $values["new_org_name"])), "status");
        
        // Make default org
        if ($values["new_org_default"]) {
          self::setDefaultOrg($new_id);
        }
      } else {
        drupal_set_message(t("OnSIP organization @org couldn't be created, something went wrong", array("@org" => $values["new_org_name"])), "error");
      }
    }
  }
  
  static function _checkOrgRegisterValues($values, $name_error_map) {
    $errors = array();
    
    foreach ($name_error_map as $field => $criterion) {
      $value = $values[$field];
      
      switch ($criterion[0]) {
        case "int":
          if (!is_numeric($value)) {
            $errors[] = array($field, t($criterion[1]));
          }
          break;
        case "str":
          if (!is_string($value) || !$value) {
            $errors[] = array($field, t($criterion[1]));
          }
          break;
        case "domain":
          $value = trim($value);
          
          if (!preg_match('/^[a-z\d\-]+\.onsip\.com$/i', $value)) {
            $errors[] = array($field, t($criterion[1]));
          }
          break;
        case "machinename":
          $value = trim($value);
          
          if (!preg_match('/^[a-z\d\-]+$/i', $value)) {
            $errors[] = array($field, t($criterion[1]));
          }
          break;
      }
    }
    
    foreach ($errors as $error) {
      form_set_error("new_org][" . $error[0], t($error[1]));
    }
    
    return $errors;
  }
  
  static function updateOrg($values) {
    // Implementation...
  }
  
  static function setDefaultOrg($oid) {
    // $oid Already checked in self::orgAction
    $org = self::loadOrg($oid);
    
    variable_set("ihfathsip_default_org", $oid);
    drupal_set_message(t("@org set as default organization", array("@org" => $org->name)), "status");
  }
  
  static function getDefaultOrg() {
    return variable_get("ihfathsip_default_org");
  }
  
  static function deleteOrg($oid) {
    // $oid Already checked in self::orgAction
    $org = self::loadOrg($oid);
    
    $query = db_delete("ihfath_sip_org");
    $query->condition("oid", $oid, "=");
    
    $outcome = $query->execute();
    
    if ($outcome) {
      drupal_set_message(t("OnSIP organization @org deleted successfully", array("@org" => $org->name)), "status");
    } else {
      drupal_set_message(t("OnSIP organization @org wasn't deleted successfully", array("@org" => $org->name)), "error");
    }
  }
  
  static function orgAction($action, $oid) {
    $oid = self::checkValidOrg($oid);
    
    if (isset($oid)) {
      switch ($action) {
        case "default":
          self::setDefaultOrg($oid);
          break;
        case "delete":
          self::deleteOrg($oid);
          break;
        default:
          drupal_not_found();
      }
    } else {
      drupal_not_found();
    }
  }
  
  static function getOrgUserCount($oid) {
    $oid = self::checkValidOrg($oid);
    
    if (isset($oid)) {
      $query = db_select("ihfath_sip_user", "isu");
      $query->condition("isu.oid", $oid, "=");
      $query->addExpression("COUNT(*)", "count");
      
      return (int) $query->execute()->fetchField();
    } else {
      return NULL;
    }
  }
  
  // User stuff
  static function addUser($account) {
    // Make a corresponding SIP account for the current user
    $def_org = self::getDefaultOrg();
    $org     = self::loadOrg($def_org);
    $sess    = new Session($def_org);
    
    $name_encoded = "user" . $account->uid;
    $new_username = $name_encoded . "_" . substr(sha1(random_int(0, 1000000)), 0, 10);
    $new_authname = $name_encoded . "_" . substr(sha1(random_int(0, 1000000)), 0, 4). "_" . substr(sha1(random_int(0, 1000000)), 0, 8);
    $new_password = substr(sha1(random_int(0, 1000000)), 0, 15);
    
    $user_sip_data = API::request($sess, array(
      "Action"          => "UserAdd",
      "OrganizationId"  => $org->org_id,
      "Domain"          => $org->domain,
      "Username"        => $new_username,
      "AuthUsername"    => $new_authname,
      "Password"        => $new_password,
      "PasswordConfirm" => $new_password,
      "Name"            => $name_encoded,
      "Email"           => $account->mail,
    ));
    
    if (isset($user_sip_data->Response->Result->UserAdd->User->UserId)) {
      // We've successfully created another user
      $onsip_user_data = $user_sip_data->Response->Result->UserAdd->User;
      
      // Register user in Ihfath's SIP database
      $query = db_insert("ihfath_sip_user");
      $query->fields(array(
        "uid"           => $account->uid,
        "oid"           => $org->oid,
        "sip_uid"       => $onsip_user_data->UserId,
        "sip_auth_user" => $new_authname,
        "sip_name"      => $new_username,
        "sip_pass"      => $onsip_user_data->Password,
        "onsip_pass"    => $new_password,
      ));
      
      // Push to db
      $query->execute();
    } else {
      die("Failed to create OnSIP account");
    }
  }
}
