<?php

// Ihfath SIP code
//
// Prefix (For autocomplete): ihfathsip_

namespace IhfathSip;

class API {
  const onSIPUrl = "https://api.onsip.com/api";
  
  static function request($session, $post_data) {
    $ch = \curl_init();
    
    // If there's a session, add the session
    if (is_object($session)) {
      $post_data["SessionId"] = $session->getKey();
    }
    
    // Require JSON output
    $post_data["Output"] = "json";
    
    // Serialize into formdata string
    $raw_post_data = array();
    
    foreach ($post_data as $key => $value) {
      $raw_post_data[] = urlencode($key) . "=" . urlencode($value);
    }
    
    // Set cURL options
    \curl_setopt($ch, CURLOPT_URL, self::onSIPUrl);
    \curl_setopt($ch, CURLOPT_POST, count($post_data));
    \curl_setopt($ch, CURLOPT_POSTFIELDS, implode("&", $raw_post_data));
    \curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
    
    $result = \curl_exec($ch);
    \curl_close($ch);
    
    // Return result data
    return json_decode($result);
  }
}

class Session {
  const maxLifetime = 60; // Max session lifetime in seconds (5 minutes)
  static $_cache = array(
    "org_sessions" => array()
  );
  
  private $key = "";
  private $org = NULL;
  
  function __construct($oid) {
    // Set org
    $this->org = Base::loadOrg($oid);
    $sess      = NULL;
    
    // Check cache
    if (isset(self::$_cache["org_sessions"][$oid])) {
      $sess = self::$_cache["org_sessions"][$oid];
    }
    
    // Check if is contained in the database
    if ($this->org->onsip_session) {
      $sess = self::$_cache["org_sessions"][$oid] = array(
        "key"       => $this->org->onsip_session,
        "timestamp" => (int) $this->org->onsip_session_time
      );
    }
    
    // Check if session expired or if there's no session
    if (!$sess || time() - $sess["timestamp"] > self::maxLifetime) {
      // Gotta create a new one
      $this->getNewSession();
    } else {
      $this->key = $sess["key"];
    }
  }
  
  private function getNewSession() {
    $response = API::request(NULL, array(
      "Action" => "SessionCreate",
      "Username" => $this->org->username . "@" . $this->org->domain,
      "Password" => $this->org->password
    ));
    
    $context = $response->Response->Context;
    
    if ($context->Action->IsCompleted === "true" && $context->Session->IsEstablished === "true") {
      // Successfully acquired SessionId
      $this->key = $context->Session->SessionId;
      $time = time();
      
      // Add to cache
      self::$_cache["org_sessions"][$this->org->oid] = array(
        "key"       => $this->key,
        "timestamp" => $time
      );
      
      // Push to database
      $query = db_update("ihfath_sip_org");
      $query->condition("oid", $this->org->oid, "=");
      $query->fields(array(
        "onsip_session"      => $this->key,
        "onsip_session_time" => $time
      ));
    
      $query->execute();
    }
  }
  
  public function getKey() {
    return $this->key;
  }
}
