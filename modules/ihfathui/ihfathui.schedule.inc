<?php

// Schedule manipulation code
//
// Prefix (for autocomplete): ihfathui_ _ihfathui_

/**
 *  Returns timezone offset in seconds
 */
function _ihfathui_get_timezone_offset($tz) {
  $dtz = new DateTimeZone($tz);
  $time_in_tz = new DateTime("now", $dtz);
  return $dtz->getOffset($time_in_tz);
}

function _ihfathui_get_weektime_shift($weektimes, $shift) {
  $new_weektimes = array();

  foreach ($weektimes as $weektime) {
    $new_weektime = IhfathCoreWeektime::shiftWeektime($weektime, 0, $shift);
    $new_weektimes[] = $new_weektime;
  }

  return $new_weektimes;
}

function _ihfathui_prof_week_schedule_timezone_shift($schedule_tbl, $tz_shift) {
  $new_schedule = array();

  $cur_time = $tz_shift;
  $cur_time_old = $cur_time;
  $time_max = 24 * 7;

  for ($x=0; $x<7; $x++) {
    for ($y=0; $y<24; $y++) {
      if ($y === 0) {
        $new_schedule[$x] = array();
      }

      $cur_col  = &$new_schedule[$x];
      $tmp_time = $cur_time;

      // Wrap around to the end
      if ($tmp_time < 0) {
        $tmp_time += $time_max;
      }

      // Calculate the new coords
      $new_x = floor($tmp_time / 24);
      $new_y = $tmp_time - (floor($tmp_time / 24) * 24);

      if ($new_x > 6) {
        // var_dump($new_y);
        // $diff_x = $new_x - 6;
        $new_x -= 7;
      }

      $cur_col[] = $schedule_tbl[$new_x][$new_y];

      // Go to the next "hour"
      $cur_time++;
    }
  }

  return $new_schedule;
}

/**
 *
 * $week_offset: How many weeks to offset forward
 * $back_week_offset: (FIXME) (seconds) How many weeks back should the function go
 */
function _ihfathui_schedule_timezone_shift($uid, $week_offset = 0, $back_week_offset = 0, $timezone_shift = NULL, $base_time = NULL) {
  $user         = user_load($uid);
  $role         = ihfathcore_get_role($user);
  $offset       = ($timezone_shift === NULL ? _ihfathui_get_timezone_offset(drupal_get_user_timezone()) : $timezone_shift);
  $offset_hour  = $offset / 60 / 60; // seconds to hours
  $schedule     = $role === 2 /* prof */ ? unserialize(ihfathlsn_get_weekly_schedule($uid)) : ihfathlsn_gen_empty_schedule();
  $schedule_tbl = $schedule["table"];

  $new_schedule = _ihfathui_prof_week_schedule_timezone_shift($schedule_tbl, $offset_hour);

  // Add lessons with offset weektimes
  $time  = $base_time ? $base_time : time();
  $start = gmmktime(0, 0, 0, gmdate("n", $time), gmdate("j", $time) - gmdate("N", $time) + 1 + ($week_offset * 7), gmdate("Y", $time));
  $end   = gmmktime(0, 0, 0, gmdate("n", $time), gmdate("j", $time) - gmdate("N", $time) + 8 + ($week_offset * 7), gmdate("Y", $time));

  $lessons = ihfathlsn_get_user_lessons($uid, $role === 2, NULL, array(
    "start" => ($start - $offset) - $back_week_offset,
    "end"   => $end - $offset,
  ), function(&$lesson_query) {
    $studUsers = db_select("users", "us")->fields("us", array("uid", "name"));
    $profUsers = db_select("users", "up")->fields("up", array("uid", "name"));

    // JOIN queries
    $lesson_query->leftJoin($studUsers, "us", "il.suid = us.uid");
    $lesson_query->addField("us", "name", "student_name");
    $lesson_query->leftJoin($profUsers, "up", "il.puid = up.uid");
    $lesson_query->addField("up", "name", "prof_name");
  });

  // Make new weektimes
  $weektime_map = array();
  $weektimes    = array();

  foreach ($lessons as $lesson) {
    $weektime = (int) $lesson->weektime;

    if (!isset($weektime_map[$weektime])) {
      $weektime_map[$weektime] = count($weektimes);
      $weektimes[]             = $weektime;
    }
  }

  $new_weektimes = _ihfathui_get_weektime_shift($weektimes, $timezone_shift / 60 / 60);

  foreach ($lessons as $lesson) {
    $weektime = $new_weektimes[$weektime_map[(int) $lesson->weektime]];
    $weektime_parsed = IhfathCoreWeektime::parseWeektime($weektime);

    $weekday  = $weektime_parsed[0];
    $weekhour = $weektime_parsed[1];

    $hour = $new_schedule[$weekday - 1][$weekhour];

    if (is_array($hour)) {
      $prev_hour_lesson = $hour["lesson"];
      $hour = $hour["hour"];

      if ((int) $prev_hour_lesson->paid_duration < (int) $lesson->paid_duration) {
        // Replace the current lesson with this one
        // because it has a longer paid duration
        $new_schedule[$weekday - 1][$weekhour] = array(
          "hour"   => $hour,
          "lesson" => $lesson
        );
      }
    } else {
      $new_schedule[$weekday - 1][$weekhour] = array(
        "hour"   => $hour,
        "lesson" => $lesson
      );
    }
  }

  return array(
    "bounds" => $schedule["bounds"],
    "table"  => $new_schedule
  );
}
