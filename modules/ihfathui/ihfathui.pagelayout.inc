<?php

class IhfathUIPageLayout {
  static function getItem() {

  }

  static function getPage($page_id, $minimal = FALSE) {
    $query = db_select("ihfath_ui_pagelayout", "iupl");

    if ($minimal) {
      $query->fields("iupl", array(
        "item_id",
        "page_id",
        "weight",
      ));
    } else {
      $query->fields("iupl");
    }

    $query->orderBy("iupl.weight", "ASC");
    $results = $query->execute()->fetchAllAssoc("item_id");

    if (!$minimal) {
      foreach ($results as $item) {
        $item->content = json_decode($item->content);
      }
    }

    return $results;
  }

  static function savePage($id, $items) {
    $page_prev = self::getPage($id, TRUE);

    // Save items
    $saved_item_map = array();
    $removed_items = array();
    $saved_ids = array();

    foreach ($items as $item) {
      $new_fields = (array) $item;
      $new_fields["content"] = json_encode($item->content);
      unset($new_fields["item_id"]);

      if ((int) $item->item_id) {
        // Update existing item
        db_update("ihfath_ui_pagelayout")
          ->fields($new_fields)
          ->condition("item_id", $item->item_id)
          ->execute();

        $saved_ids[] = $item->item_id;
      } else {
        // Insert new item
        $new_id = db_insert("ihfath_ui_pagelayout")
                    ->fields($new_fields)
                    ->execute();

        $saved_ids[] = $new_id;
      }
    }

    $ids_map = array_flip($saved_ids);

    // Delete any discarded items
    foreach ($page_prev as $item) {
      if (!isset($ids_map[$item->item_id])) {
        db_delete("ihfath_ui_pagelayout")
          ->condition("item_id", $item->item_id)
          ->execute();
      }
    }

    return $saved_ids;
  }
}
