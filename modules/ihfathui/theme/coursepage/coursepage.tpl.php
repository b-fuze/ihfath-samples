<?php
global $language;
$lang = $language->language;
$file_base = url("sites/default/files/ihfathusercontent");

// Exposed:
//   - $course     The current course to display

// Get field value
$fv = function($field, $markup = TRUE) use($lang) {
  $use_lang = $lang;

  // global
  if (!isset($field[$lang])) {
    $use_lang = LANGUAGE_NONE;
  }

  $value = $field[$use_lang][0]["value"];
  if ($markup) {
    return check_markup($value, "ihfathui_markdown");
  } else {
    return $value;
  }
};

$features = array();

if (isset($course->field_ihfathlsn_c_cpfeat)) {
  $features = json_decode($fv($course->field_ihfathlsn_c_cpfeat, FALSE));

  if (!$features) {
    $features = array();
  }
}
?>

<div class="hero" style="background-image: url('<?= $file_base . '/' . $fv($course->field_ihfathlsn_c_cpimage, FALSE) ?>')">
  <div class="constrain">
    <h1><?= $course->data["current"]->description ?></h1>
  </div>
  <a href="<?= url("pricing/" . $course->name) ?>" class="button shadow register"><?= t("Register") ?></a>
</div>
<div class="course-body">
  <div class="details">
    <h2 class="title"><?= $fv($course->field_ihfathlsn_c_cptitle) ?></h2>
    <?= $fv($course->field_ihfathlsn_c_cpdesc) ?>

    <?php
    if ($features) { ?>

      <hr>
      <div class="features">
        <?php
        $feature_icons = array(
          '<svg xmlns="http://www.w3.org/2000/svg" fill-rule="evenodd" clip-rule="evenodd" viewBox="0 0 24 24">
             <path d="M22 24h-17c-1.657 0-3-1.343-3-3v-18c0-1.657 1.343-3 3-3h17v24zm-2-4h-14.505c-1.375 0-1.375 2 0 2h14.505v-2zm-3-15h-10v3h10v-3z"/>
           </svg>',
          '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
             <path d="M7 24h-6v-6h6v6zm8-9h-6v9h6v-9zm8-4h-6v13h6v-13zm0-11l-6 1.221 1.716 1.708-6.85 6.733-3.001-3.002-7.841 7.797 1.41 1.418 6.427-6.39 2.991 2.993 8.28-8.137 1.667 1.66 1.201-6.001z"/>
           </svg>',
          '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
             <path d="M12 0c-6.627 0-12 5.373-12 12s5.373 12 12 12 12-5.373 12-12-5.373-12-12-12zm6 14h-7v-8h2v6h5v2z"/>
           </svg>',
        );
        $feature_index = 0;
        foreach ($features as $feature) { ?>

          <div class="feature">
            <div class="icon">
              <?= $feature_icons[$feature_index % count($feature_icons)] ?>
            </div>
            <h3 class="title">
              <?= check_markup($feature->title, "ihfathui_markdown") ?>
            </h3>
            <div class="description">
              <?= check_markup($feature->description, "ihfathui_markdown") ?>
            </div>
          </div>

        <?php
        $feature_index++;
        } ?>
      </div>
      <hr>

    <?php
    } ?>

    <h2 class="title"><?= t("Description of our study program") ?></h2>

    <div class="levels">
      <?php
      foreach ($course->levels as $level) {
      ?>

        <div class="level">
          <h3 class="level-title"><?= $fv($level->field_ihfathlsn_c_lvl_title) ?></h3>
          <section><?= $fv($level->field_ihfathlsn_c_lvl_desc) ?></section>
        </div>

      <?php
      } ?>
    </div>

    <!-- Student testimonials -->
    <div class="testimonials">
      <h3 class="title"><span><?= t("Student Testimonials") ?></span></h3>
      <div class="text">
        <div class="testimonial">
          <div class="icon">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
              <path d="M20 12.875v5.068c0 2.754-5.789 4.057-9 4.057-3.052 0-9-1.392-9-4.057v-6.294l9 4.863 9-3.637zm-8.083-10.875l-12.917 5.75 12 6.5 11-4.417v7.167h2v-8.25l-12.083-6.75zm13.083 20h-4c.578-1 1-2.5 1-4h2c0 1.516.391 2.859 1 4z"/>
            </svg>
          </div>
          <div class="name">Fatima</div>
          <div class="country">France</div>
          <div class="content">
            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
            eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut
            enim ad minim veniam, quis nostrud
          </div>
          <footer>
            <div class="date">10/10/2010</div>
            <div class="rating">
              <div class="star"></div>
              <div class="star"></div>
              <div class="star"></div>
              <div class="star"></div>
              <div class="star -gray"></div>
            </div>
          </footer>
        </div>
      </div>
    </div>

    <?php
    if (count($course->faq)) {
    ?>

      <div class="faq-title">
        <h2 class="title"><?= t("Questions and Answers") ?></h2>
      </div>

      <div class="levels">
        <?php
        foreach ($course->faq as $faq) {
        ?>

          <div class="level">
            <h3 class="level-title"><?= $fv($faq->field_ihfathlsn_c_faq_title) ?></h3>
            <section><?= $fv($faq->field_ihfathlsn_c_faq_title) ?></section>
          </div>

        <?php
        } ?>
      </div>

    <?php
    } ?>
  </div>
</div>
