<?php
$ihfath = $form["#ihfath"];

$lesson    = $ihfath["lesson"];
$student   = $ihfath["student"];
$professor = $ihfath["professor"];
$lesson_expired_form = $ihfath["lesson_expired_form"];

$user_map = array(
  "professor" => $professor,
  "student" => $student,
);

$attachments = $ihfath["attachments"];
$file_form = $ihfath["file_form"];

// Get user role and its opposing role (if any)
$user_role = "";
$opposing_role = "";

$roles = array(
  NULL,
  "administrator",
  "professor",
  "student",
);

$role = ihfathcore_get_role();
$user_role = $roles[$role];

if ($role !== 1 /* admin */) {
  $opposing_role = $roles[$role === 2 /* prof */ ? 3  /* student */ : 2];
}

function jst(...$args) {
  return json_encode(...$args);
}

function jse(...$args) {
  return json_encode(...$args);
}

global $user;
global $base_url;
?>
<div class="section">
  <div class="container ihfath-wrap">
    <div class="row">
      <div class="heading side-head">
        <div class="head-6">
          <h4 class="" style="float: right;">
            <?php
            // "Startday Startdate / Duration Hours"
            echo str_replace(array(
              "[hilight%]",
              "[%hilight]"
              ), array(
                '<span class="main-color">',
                '</span>'
              ),
              t("@start_day [hilight%]@start_date[%hilight] / [hilight%]@paid_duration[%hilight] Hours", array(
                "@start_day"     => t(date("l", $lesson->start_date)),
                "@start_date"    => date("d", $lesson->start_date),
                "@paid_duration" => $lesson->paid_duration / 60,
              ))
            ); ?>
          </h4>
          <h4 class="uppercase">
            <?php
            // "Type Program Lesson"
            echo str_replace(array(
                "[hilight%]",
                "[%hilight]",
              ), array(
                '<span class="main-color">',
                '</span>',
              ),
              t("[hilight%]@program[%hilight] Lesson", array(
                "@type"    => $lesson->type_data->data["current"]->name,
                "@program" => $lesson->program_data->data["current"]->name,
              ))
            ); ?>
          </h4>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-md-6">
        <div class="heading side-head">
          <div class="head-8 ihfath-close-heading">
            <h5 class="uppercase"><?= t("Professor"); ?></h5>
          </div>
        </div>
        <div class="ihfath-close-heading-content">
          <h2 class="main-color"><?= $professor->name; ?></h2>
        </div>
      </div>

      <div class="col-md-6">
        <div class="heading side-head">
          <div class="head-8 ihfath-close-heading">
            <h5 class="uppercase"><?= t("Student"); ?></h5>
          </div>
        </div>
        <div class="ihfath-close-heading-content">
          <?php

          if ($user->uid === $lesson->puid || $user->uid === $lesson->suid) {
            if ($user_role === "professor") {
              $user_report = $student->name;
            } else {
              $user_report = $professor->name;
            }

            ?>

            <a href="<?= $base_url ?>/koa/report/<?= $lesson->lid ?>" class="cws-button btn btn-md btn-round btn-juicy_pink with-icon" style="margin: 0px; float: right;">
              <i class="fa fa-exclamation-circle"></i> <?= t("Report issues") ?>
            </a>

          <?php
          } ?>

          <h2 class="main-color"><?= $student->name ?></h2>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-md-6">
        <div class="heading side-head">
          <div class="head-8 ihfath-close-heading">
            <h5 class="uppercase"><?= t("Start time") ?></h5>
          </div>
        </div>
        <div class="ihfath-close-heading-content">
          <h2><span class="main-color"><?= date("G", $lesson->start_date) ?></span>:<span class="main-color"><?= date("i", $lesson->start_date) ?></span></h2>
        </div>
      </div>
      <div class="col-md-6">
        <div class="heading side-head">
          <div class="head-8 ihfath-close-heading">
            <h5 class="uppercase"><?= t("End time") ?></h5>
          </div>
        </div>
        <div class="ihfath-close-heading-content">
          <h2><span class="main-color"><?= date("G", $lesson->start_date + ($lesson->paid_duration * 60)) ?></span>:<span class="main-color"><?= date("i", $lesson->start_date + ($lesson->paid_duration * 60)) ?></span></h2>
        </div>
      </div>
    </div>

    <!-- Notes -->
    <?php

    $note_identifier = $user_role === "professor" ? "notes_professor" : "notes_student";
    $lesson_notes    = $lesson->$note_identifier;

    $rating_identifier  = $user_role === "professor" ? "rating_professor" : "rating_student";
    $user_lesson_rating = (int) $lesson->$rating_identifier;
    $lesson_rating      = $user_lesson_rating === 0 ? 5 : $user_lesson_rating / 2;

    ?>
    <div class="ihfath-lesson-notes <?= !$user_lesson_rating ? "-" : "" ?>ihfath-ln-submitted" style="margin: 0px -15px;">
      <hr>
      <div class="ihfath-ln-textarea">
        <textarea name="name" rows="4" placeholder="<?php echo t("Notes"); ?>" id="ihfath-ln-note-body-input" <?php echo $user_lesson_rating ? 'disabled="true"' : ""; ?>><?php echo $lesson_notes ? check_plain($lesson_notes) : ""; ?></textarea>
      </div>
      <div class="ihfath-ln-rating">
        <div class="ihfath-ln-rating-stars">
          <div class="ihfath-ln-rating-stars-input">
            <div class="uppercase">
              <?= !$user_lesson_rating ? t("Rate this lesson") : t("Your rating") ?>
            </div>
            <?php

            for ($i=0; $i<5; $i++) { ?>

              <i class="fa fa-star ihfath-ln-rating-input-star<?php echo $i + 1 <= $lesson_rating ? " main-color" : "" ; ?>"></i>

            <?php
            } ?>
          </div>
        </div>
        <div class="ihfath-ln-rating-submit">
          <?= t("Submit"); ?>
        </div>
        <?php

        if ($user_lesson_rating === 0) { ?>

          <!-- Note JS -->
          <script type="text/javascript">
            window.addEventListener("DOMContentLoaded", function() {
              // Initiate lesson note controller
              var controller = IhfathLessonNoteController({
                wrap: jSh(".ihfath-lesson-notes")[0],
                input: jSh("#ihfath-ln-note-body-input"),
                submit: jSh(".ihfath-ln-rating-submit")[0],
                stars: jSh(".ihfath-ln-rating-input-star"),
                minNoteChar: 3,
                baseURL: <?= jse($base_url) ?>,
                lid: <?= jse($lesson->lid) ?>
              });
            });
          </script>

        <?php
        }

        ?>
      </div>
    </div>

    <!-- Whiteboard -->
    <div class="row ihf-whiteboard-wrap-shell">
      <div class="ihf-whiteboard-wrap col-md-12">
        <!-- Whiteboard Toolbar -->
        <div class="heading side-head">
          <div class="head-themed ihfath-whiteboard-head">
            <h4 class="uppercase head-title">
              <!-- Logo -->
              <svg xmlns="http://www.w3.org/2000/svg" width="100" viewBox="0 0 195.55 78.05">
                <path d="M40.36 32.21c21.68 2.36 25 20.98 17.76 31.61 10.77-8.2 5.33-29.83-17.76-31.6zm-3 1.44c-1.04 0-2.11.08-3.23.21-6.68.8-12.94 4.9-15.86 11.3-4.13 9.07-1.16 17.96 5.4 24.4 7.56 7.4 18.45 9.27 27.16 4.67L56 66.75c-11.27 10.55-26.42 7.32-32.9-.08-4.87-5.55-5-12.33-3.28-17.35 2.2-6.41 7.82-10.5 15.26-11.69 5.9-.94 12.37.67 16.57 4.3a13.86 13.86 0 0 1 4.78 12.73c-1.03 6.4-4.71 8.77-8.91 10.51-5.57 2.32-9.95 2.29-12.34 1-3.66-1.97-3.98-5.82-2.17-8.25 2.05-2.75 5.4-3.75 7.82-4.24 2.3-.47 4.67-.58 4.67-.58l1.53-5.1s-2.8.24-6.07 1.18a22.1 22.1 0 0 0-6.23 2.7c-1.38.67-2.12.17-2.53-.43-.8-1.16-.31-2.5.96-4.07a9.13 9.13 0 0 1 6.32-2.82c1.98.03 3.16.6 2.98 2.1l.83-.17c.83-3.6-1.3-5.42-3.08-5.6-4-.44-6.82 1.32-8.8 3.7-1.43 1.7-1.71 4.67-1.33 6.12a5.68 5.68 0 0 0 2.34 3.18c-.3.34-.58.7-.83 1.09-2.24 3.4-1.88 8.17 1.12 11.49 3 3.32 9.82 4.66 15.38 2.27 5.32-2.28 8.34-7.07 9.35-12.48a19.26 19.26 0 0 0-6.13-17.54 20.05 20.05 0 0 0-13.96-5.07zm.26 5.4zm-4.86 1.31c-2.67 1-4.75 2.3-6.34 3.8a19.71 19.71 0 0 1 6.34-3.8zm-6.34 3.8c-7.27 6.62-6.6 17.4.73 22.74-4.69-3.54-8.38-15.47-.73-22.73z"/>
                <path d="M132.84 16.43c-1.3 0-2.43.3-3.4.9a5.7 5.7 0 0 0-2.22 2.55 9.39 9.39 0 0 0-.77 3.94v1.04c.02 1.48.3 2.76.83 3.86a5.83 5.83 0 0 0 2.3 2.5c1 .58 2.16.87 3.48.87 1.25 0 2.4-.19 3.43-.56a5.49 5.49 0 0 0 2.4-1.58v-6.08h-6v2.31h2.86v2.63c-.5.5-1.35.76-2.53.76-1.17 0-2.06-.41-2.66-1.23-.61-.82-.91-2.02-.91-3.62v-.97c0-1.59.29-2.78.84-3.58.56-.8 1.37-1.2 2.44-1.2.84 0 1.5.2 1.97.6.48.4.79 1.05.93 1.92h3.06a5.5 5.5 0 0 0-1.81-3.76c-1.02-.86-2.44-1.3-4.24-1.3zm28.56 0a6.04 6.04 0 0 0-5.68 3.55 9.16 9.16 0 0 0-.8 3.96v.75c0 1.46.28 2.76.82 3.9a6.05 6.05 0 0 0 5.68 3.5 5.98 5.98 0 0 0 5.67-3.54 9.2 9.2 0 0 0 .8-3.94v-.68c0-1.5-.27-2.81-.81-3.95a5.96 5.96 0 0 0-2.3-2.63 6.25 6.25 0 0 0-3.38-.92zm-69.56.21v15.24h3.14v-4.36l1.63-1.75 3.91 6.1h3.74l-5.57-8.46 5.41-6.77h-3.86l-3.88 5.01-1.38 1.9v-6.9zm13.8 0v15.24h3.14V16.64zm6.04 0v15.24h3.14V21.85l6.11 10.03h3.14V16.64h-3.13V26.7l-6.12-10.05zm29.76 0v15.24h4.72c1.33 0 2.53-.32 3.59-.92a6.22 6.22 0 0 0 2.46-2.57c.58-1.1.88-2.36.88-3.77v-.7a7.9 7.9 0 0 0-.9-3.8 6.26 6.26 0 0 0-2.47-2.57 7.06 7.06 0 0 0-3.6-.9zm28.75 0v15.24h3.14V27.7l-.31-7.18 4.1 11.35h2.16l4.1-11.36-.3 7.2v4.16h3.14V16.64h-4.12l-3.9 11.05-3.9-11.05zm-8.79 2.39a2.8 2.8 0 0 1 2.45 1.24c.57.83.86 2.05.86 3.64v.75c0 1.58-.3 2.79-.86 3.62a2.77 2.77 0 0 1-2.43 1.23c-1.07 0-1.9-.43-2.46-1.28a6.5 6.5 0 0 1-.86-3.62v-.75c0-1.58.3-2.78.87-3.6a2.8 2.8 0 0 1 2.43-1.23zm-16.82.15h1.55c1.23 0 2.17.4 2.81 1.2.64.8.96 1.98.96 3.52v.81a5.44 5.44 0 0 1-.99 3.44c-.65.8-1.59 1.2-2.81 1.2h-1.52zM97.72 36.82c-1.17 0-2.2.29-3.1.87-.9.57-1.6 1.39-2.1 2.46a8.77 8.77 0 0 0-.73 3.69v1.57c0 2.16.55 3.88 1.64 5.16a5.42 5.42 0 0 0 4.31 1.9c1.18 0 2.22-.29 3.12-.86.9-.57 1.59-1.39 2.07-2.45.49-1.06.73-2.29.73-3.69v-1.82a8.62 8.62 0 0 0-.76-3.59 5.56 5.56 0 0 0-5.18-3.24zm91 0c-1.16 0-2.19.28-3.08.85a5.45 5.45 0 0 0-2.04 2.42 8.63 8.63 0 0 0-.71 3.6v1.98c0 1.33.24 2.52.72 3.56a5.54 5.54 0 0 0 2 2.4 5.4 5.4 0 0 0 3.01.84c1.62 0 2.91-.42 3.88-1.28a5.45 5.45 0 0 0 1.73-3.68h-1.3a4.66 4.66 0 0 1-1.26 2.91c-.69.64-1.7.96-3.05.96a3.87 3.87 0 0 1-3.24-1.57 6.78 6.78 0 0 1-1.2-4.2v-1.87c0-1.82.41-3.24 1.23-4.28a4.01 4.01 0 0 1 3.3-1.54c2.5 0 3.9 1.29 4.22 3.88h1.3a5.51 5.51 0 0 0-1.65-3.65 5.4 5.4 0 0 0-3.87-1.33zm-81.8.2v15.24h1.3v-7.11h7.1v-1.1h-7.1v-5.93h8.15v-1.1zm21.45 0l-5.77 15.24h1.35l1.57-4.27h6.92l1.58 4.27h1.34l-5.77-15.23zm9.2 0v15.24h1.3v-6.34h4.43l3.68 6.34h1.38v-.13l-3.81-6.48c.96-.29 1.73-.8 2.3-1.55.57-.75.86-1.6.86-2.58 0-1.42-.46-2.52-1.37-3.31a5.64 5.64 0 0 0-3.81-1.18zm17.8 0l-5.77 15.24h1.35l1.57-4.27h6.92l1.58 4.27h1.34l-5.77-15.23zm9.24 0v15.24h5.1c1.54 0 2.75-.38 3.61-1.13a3.97 3.97 0 0 0 1.31-3.16c0-.88-.27-1.67-.82-2.34a3.64 3.64 0 0 0-2.13-1.31 3.38 3.38 0 0 0 2.4-3.3c0-1.32-.42-2.3-1.26-2.98-.84-.68-2.08-1.01-3.71-1.01zm13.42 0v15.24h1.29V37.03zm-80.3.93c1.43 0 2.57.52 3.4 1.56.83 1.04 1.25 2.47 1.25 4.3v1.62c0 1.86-.41 3.32-1.23 4.36a4.1 4.1 0 0 1-3.4 1.56 4.13 4.13 0 0 1-3.42-1.58 6.76 6.76 0 0 1-1.26-4.3v-1.59c0-1.87.42-3.32 1.26-4.36a4.13 4.13 0 0 1 3.4-1.57zm41.15.17h3.63c1.23 0 2.19.3 2.87.9.69.59 1.03 1.42 1.03 2.5 0 .97-.33 1.77-.98 2.38-.66.61-1.53.92-2.61.92h-3.94zm27.01 0h3.22c1.23 0 2.15.24 2.76.7.61.47.92 1.18.92 2.16 0 .93-.3 1.64-.9 2.13-.58.48-1.4.73-2.48.75h-3.52zm-36.91.48l3.05 8.3h-6.1zm27 0l3.05 8.3h-6.11zm9.91 6.34h3.97c1.09 0 1.94.27 2.55.82.62.55.93 1.3.93 2.23 0 1-.32 1.78-.97 2.35-.64.55-1.51.83-2.62.83h-3.86zM97.3 56.85c-1.08 0-2.06.18-2.93.54-.86.37-1.53.87-2 1.52a3.67 3.67 0 0 0-.69 2.2c0 1.58.87 2.84 2.6 3.78.63.34 1.5.69 2.58 1.04 1.09.35 1.84.69 2.26 1 .42.32.63.78.63 1.37 0 .54-.2.97-.63 1.27-.42.3-1 .45-1.75.45-2 0-3-.84-3-2.52H91.2a4.54 4.54 0 0 0 2.96 4.37c.97.42 2.03.63 3.2.63 1.7 0 3.04-.37 4.03-1.12 1-.75 1.5-1.78 1.5-3.1 0-1.19-.4-2.18-1.22-2.97-.82-.8-2.12-1.47-3.9-2-.98-.3-1.71-.61-2.22-.94-.5-.34-.74-.75-.74-1.25 0-.53.21-.96.64-1.28.43-.32 1.02-.49 1.79-.49.8 0 1.41.2 1.85.59.44.38.66.92.66 1.62h3.14c0-.91-.24-1.73-.71-2.45a4.56 4.56 0 0 0-1.98-1.66 6.8 6.8 0 0 0-2.9-.6zm13.6 0a5.78 5.78 0 0 0-5.56 3.46 9.13 9.13 0 0 0-.77 3.88v.93c0 2.3.56 4.1 1.68 5.42a5.76 5.76 0 0 0 4.61 1.96c1.79 0 3.23-.47 4.31-1.4a5.38 5.38 0 0 0 1.82-3.88h-3.13c-.07.93-.35 1.62-.83 2.06-.48.44-1.2.66-2.17.66-1.07 0-1.86-.38-2.36-1.14-.5-.76-.75-1.96-.75-3.61v-1.15c.02-1.58.28-2.74.8-3.49s1.3-1.13 2.35-1.13c.97 0 1.68.23 2.15.67.47.44.75 1.16.83 2.14H117a5.8 5.8 0 0 0-1.87-3.97 6.21 6.21 0 0 0-4.25-1.4zm29.34 0a6.04 6.04 0 0 0-5.68 3.55 9.16 9.16 0 0 0-.81 3.95v.76c0 1.46.28 2.76.82 3.89a6.05 6.05 0 0 0 5.69 3.5 5.98 5.98 0 0 0 5.67-3.54 9.2 9.2 0 0 0 .8-3.94v-.68c0-1.49-.28-2.8-.82-3.94a5.96 5.96 0 0 0-2.3-2.63 6.25 6.25 0 0 0-3.37-.92zm14.81 0a6.04 6.04 0 0 0-5.68 3.55 9.16 9.16 0 0 0-.8 3.95v.76c0 1.46.28 2.76.82 3.89a6.05 6.05 0 0 0 5.68 3.5 5.98 5.98 0 0 0 5.67-3.54 9.2 9.2 0 0 0 .8-3.94v-.68c0-1.49-.27-2.8-.82-3.94a5.95 5.95 0 0 0-2.29-2.63 6.25 6.25 0 0 0-3.38-.92zm-36 .21V72.3h3.15v-6.53h6.12v6.53h3.14V57.06h-3.14v6.17h-6.12v-6.17zm44.8 0V72.3h9.8v-2.53h-6.67V57.06zm-23.61 2.39a2.8 2.8 0 0 1 2.44 1.24c.58.83.86 2.05.86 3.64v.75c0 1.58-.3 2.79-.86 3.62a2.77 2.77 0 0 1-2.42 1.23c-1.08 0-1.9-.43-2.47-1.29a6.5 6.5 0 0 1-.86-3.62v-.74c0-1.58.3-2.78.87-3.6a2.8 2.8 0 0 1 2.44-1.23zm14.81 0a2.8 2.8 0 0 1 2.45 1.24c.57.83.86 2.05.86 3.64v.75c0 1.58-.3 2.79-.86 3.62a2.77 2.77 0 0 1-2.43 1.23c-1.07 0-1.9-.43-2.47-1.29a6.5 6.5 0 0 1-.85-3.62v-.74c0-1.58.3-2.78.86-3.6a2.8 2.8 0 0 1 2.44-1.23z"/>
                <path d="M39.98 1.32a3.02 3.02 0 1 0 0 6.05 3.02 3.02 0 0 0 0-6.05zm-.53 7.55c-2.48 10.01-9.9 12.65-15.92 7.4l5.28 15.41h.02v-.03s.57-3.74 4.14-4.2c.34-.05.67-.06 1-.06a9.35 9.35 0 0 1 5.44 2.2c-.02-.13-1.23-5.36-5.64-5.75 4.9-1.78 5.6-11.06 5.68-14.06zm1.06 0v20.75c.05-.05 2.44-2.72 5.57-2.8.32 0 .66.02 1 .08 2.57.41 3.87 4.08 4.09 4.75l5.27-15.38c-6.01 5.25-13.44 2.61-15.93-7.4zM22.1 11.68a1.65 1.65 0 1 0 0 3.31 1.65 1.65 0 0 0 0-3.3zm35.8 0a1.65 1.65 0 1 0 0 3.31 1.65 1.65 0 0 0 0-3.3zM22.5 16.56c-1.63 5.6-12.66 13.23-18.65 5.48l12.96 18.48c2.52-3.7 6.48-6.59 11-8.45zm34.95 0l-5.32 15.5c4.53 1.87 8.48 4.75 11 8.46l12.97-18.48c-6 7.75-17.02.12-18.65-5.48zM2.98 18.9a1.65 1.65 0 1 0 .84 3.08l.04.06-.04-.07a1.65 1.65 0 0 0-.84-3.07zm74.01 0a1.65 1.65 0 0 0-.84 3.07l-.04.07.04-.06A1.65 1.65 0 1 0 77 18.9zm-29.56 2.96c.92.03 1.74.7 1.27 2.24-1.16-.8-2.45-.52-3.2-.24-.33-1.26.88-2.04 1.93-2z"/>
              </svg>
              <span><?= t("Whiteboard") ?></span>
            </h4>
            <div class="ihfath-ldc-wbheader-clock"></div>

            <!-- Call Student/Teacher -->
            <?php
            if ($user_role !== "administrator"
                && isset($professor->field_ihfath_call_link[LANGUAGE_NONE])) {
              if ($user_role === "professor") {
                $cur_acc = $professor;
                $opp_acc = $student;
              } else {
                $cur_acc = $student;
                $opp_acc = $professor;
              } ?>

              <div class="ihfath-call-user">
                <a href="<?= $professor->field_ihfath_call_link[LANGUAGE_NONE][0]["value"] ?>" target="_blank" id="ihfath-init-call" class="main-bg">
                  <b class="fa fa-phone"></b><?= t("Call " . ucfirst($opposing_role)) ?>
                </a>
              </div>

            <?php
            } ?>

            <div class="ihf-align-right">
              <h4
                class="uppercase ihfwb-action -ihf-show-books"
                title="<?= t("Open books") ?>">
                <span>
                  <?= t("Books") ?>
                </span>
                <i class="fa fa-book"></i>
              </h4>
              <h4
                class="uppercase ihfwb-action -ihf-wb-fullscreen"
                title="<?= t("Make whiteboard fullscreen") ?>">
                <span>
                  <?= t("Fullscreen") ?>
                </span>
                <i class="fa fa-expand"></i>
              </h4>
            </div>
          </div>
        </div>

        <!-- Whiteboard Iframe -->
        <div class="ihfath-whiteboard">
          <div class="ihfath-whiteboard-embed">
            <iframe
                    id="ihfath-whiteboard-iframe"
                    src="<?= isset($student->field_ihfath_whiteboard[LANGUAGE_NONE]) ? $student->field_ihfath_whiteboard[LANGUAGE_NONE][0]["value"] : '' ?>"
                    width="100%"
                    height="100%">
            </iframe>
          </div>
          <div class="ihfath-whiteboard-loading">
            <div class="ihfath-loading-text uppercase">
              <?= t("Whiteboard Loading...") ?>
            </div>
          </div>
        </div>
      </div>
    </div> <!-- /Whiteboard -->

    <div class="ihf-book-viewer"></div>
    <script type="text/javascript">
      window.addEventListener("load", function() {
        IhfathWhiteboardBooks({
          mainNav: document.querySelector("header.top-head"),
          wboard: document.querySelector(".ihf-whiteboard-wrap"),
          wboardFrame: document.querySelector("#ihfath-whiteboard-iframe"),
          btnBooks: document.querySelector(".ihfwb-action.-ihf-show-books"),
          btnFullscreen: document.querySelector(".ihfwb-action.-ihf-wb-fullscreen"),
          viewerRoot: document.querySelector(".ihf-book-viewer"),
          pageBase: <?= jse(url("sites/default/files/ihfathbooks")) ?>,
          bookApi: <?= jse(url("ihfath/ajax/general/loadBook")) ?>,
          bookPageApi: <?= jse(url("ihfath/ajax/general/loadBookPage")) ?>,
          bookList: <?= jse($lesson->status ? IhfathCourse::getLevelBooks($lesson->status) : array()) ?>,
          locale: {
            fullscreen: <?= jst("Fullscreen") ?>,
            minimize: <?= jst("Minimize") ?>,
          },
        });
      });
    </script>

    <!-- Files/Attachments -->
    <div class="row" style="margin-top: 30px;">
      <div class="col-md-12" style="padding: 0px;">
        <div class="heading side-head">
          <div class="head-6">
            <h4 class="uppercase">
              <?= t("Files") ?>
            </h4>
          </div>
        </div>

        <div class="ihfath-file-items">
          <div class="ihfathmsg-msg-file-attachments">
            <?php
            $attach_base_path = $base_url . "/ihfath/attachment/";

            foreach ($attachments as $attach_group) {
              foreach ($attach_group->attachments as $attach_obj) { ?>

                <a href="<?= $attach_base_path . $attach_obj->file_hash . "/" . $attach_obj->file_name; ?>" title="<?= htmlspecialchars($attach_obj->file_name) ?>" class="ihfathmsg-msg-file-anchor">
                  <span class="ihfathmsg-msg-file-name">
                    <?= htmlspecialchars($attach_obj->file_name) ?>
                  </span>
                  <span class="ihfathmsg-msg-file-size">
                    <?= format_size($attach_obj->file_size) ?>
                  </span>
                </a>

              <?php
              }
            } ?>
          </div>

          <div class="ihfath-file-upload-form">
            <?php
            // Change submit button's classes
            $file_form["msg_form"]["buttons"]["submit"]["#attributes"]["class"] = array(
              "BTN_REPLACE_PATTERN"
            );

            $form_html = preg_replace("/\"[^\"]*BTN_REPLACE_PATTERN[^\"]*\"/i", '"btn main-bg btn-md form-submit btn-round"', drupal_render($file_form));
            echo $form_html; ?>
          </div>
        </div>

        <script type="text/javascript">
          // File upload form JS
          window.addEventListener("load", function() {
            const form = jSh("#ihfathlsn-ajax-lesson-attachment-form");

            IhfathAttachments({
              attachBasePath: <?= jse($attach_base_path) ?>,
              attachWrap: jSh(".ihfathmsg-msg-file-attachments")[0],
              form,
              formSubmit: jSh("#edit-submit"),
              formWrap: jSh(form.parentNode),

              baseUrl: <?= jse($base_url) ?>,
              lessonId: <?= jse($lesson->lid) ?>,

              localeUploading: <?= jst("Uploading...") ?>,
            });
          });
        </script>
      </div>
    </div>
  </div>
</div>

<?php
// Only start beacon if this lesson hasn't passed yet
$lesson_end = $lesson->start_date + ($lesson->paid_duration * 60);
if ($role !== 1) {
 ?>

  <div class="ihfath-expire-lesson">
    <!-- ... -->
  </div>

  <!-- Clock/Beacon -->
  <script type="text/javascript">
    window.addEventListener("load", function() {
      let headerPlaceholder = document.querySelector(".ihfath-ldc-header-clock");

      // Place clock in header for prof
      if (!headerPlaceholder) {
        const div = jSh.d(".ihfath-ldc-header-prof");
        const logo = document.querySelector(".container .logo");

        logo.parentNode.appendChild(div);
        logo.parentNode.classList.add("ihfath-ldc-clock-wrap");
        headerPlaceholder = div;
      }

      IhfathClockSync({
        apiUrl: <?= jse(url("ihfath/ajax/general/lessonBeacon")) ?>,
        cancelUrl: <?= jse(url("koa/cancel/" . $lesson->lid)) ?>,
        lessonId: <?= jse($lesson->lid) ?>,
        startDate: <?= jse($lesson->start_date * 1000) ?>,
        endDate: <?= jse($lesson_end * 1000) ?>,
        duration: <?= jse($lesson->paid_duration * 60 * 1000) ?>,
        role: <?= jse($role) ?>,
        cancelled: <?= jse((bool) ((int) $lesson->cancelled)) ?>,
        dom: [
          headerPlaceholder,
          document.querySelector(".ihfath-ldc-wbheader-clock"),
        ],

        lessonExpireWrap: document.querySelector(".ihfath-expire-lesson"),
        lessonExpireUrls: {
          reschedule: <?= jse(url("switchlesson/{$lesson->lid}")) ?>,
          append: <?= jse(url("appendlesson/{$lesson->lid}")) ?>,
          back: <?= jse(url("<front>")) ?>,
        },
        lessonExpireForm: {
          formBuildId: <?= jse($lesson_expired_form["form_build_id"]) ?>,
          formToken: <?= jse($lesson_expired_form["form_token"]) ?>,
          formId: <?= jse($lesson_expired_form["form_id"]) ?>,
        },

        locale: {
          inLesson: <?= jst("In Lesson") ?>,
          beforeLesson: <?= jst("Until Lesson") ?>,
          prof: <?= jst("Professor") ?>,
          student: <?= jst("Student") ?>,
          online: <?= jst("Online") ?>,
          offline: <?= jst("Offline") ?>,
          cancelled: <?= jst("Cancelled") ?>,
          actionCancel: <?= jst("Cancel") ?>,
          actionCancelDesc: <?= jst("Cancel this class") ?>,
          finished: <?= jst("Finished") ?>,

          lessonExpireMessageProfessor: <?= jst("Class is cancelled, you were too late.") ?>,
          lessonExpireBackHomeBtn: <?= jst("Back home") ?>,

          lessonExpireMessageStudent: <?= jst("Sorry your teacher missed you") ?>,
          lessonExpireRescheduleBtn: <?= jst("Reschedule lesson") ?>,
          lessonExpireAppendBtn: <?= jst("Append lesson") ?>,
        },
      });
    });
  </script>
<?php
}
?>
