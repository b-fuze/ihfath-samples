// @ts-check
import "../css/whiteboard-books.scss";

export function WhiteboardBooks(args) {
  const {
    mainNav,
    wboard,
    wboardFrame,
    viewerRoot,
    btnBooks,
    btnFullscreen,
    locale,
  } = args;
  const btnBooksIcon = btnBooks.querySelector(".fa");
  const btnFullscreenIcon = btnFullscreen.querySelector(".fa");
  const btnFullscreenMsg = btnFullscreen.querySelector("span");

  let wboardHeight = 0;
  let isFullscreen = false;
  let isBookvisible = false;

  // Create viewer
  const viewer = ihfathBookViewer({
    root: args.viewerRoot,
    pageBase: args.pageBase,
    useOverlay: false,
    headerHeight: 90,
    showClose: true,
  });

  viewer.onclose(() => {
    setBooksvisible(false);
  });

  viewer.onmove(({ moving }) => {
    if (moving) {
      wboardFrame.style.pointerEvents = "none";
    } else {
      wboardFrame.style.pointerEvents = "all";
    }
  });

  viewer.setBooklist(args.bookList);

  const books = {

  };
  function loadBook({ id }) {
    if (books[id]) {
      viewer.open(books[id]);
    } else {
      fetch(args.bookApi, {
        method: "POST",
        body: JSON.stringify({
          id,
        }),
      }).then(async (response) => {
        if (response.ok) {
          const data = jSh.parseJSON(await response.text());

          if (!data.error) {
            viewer.open(books[id] = data.data.book);
          }
        }
      });
    }
  }

  viewer.onrequest(loadBook);

  viewer.onpage((pageNum, bookId) => {
    fetch(args.bookPageApi, {
      method: "POST",
      body: JSON.stringify({
        id: bookId,
        page: pageNum
      }),
    }).then(async (response) => {
      if (response.ok) {
        const data = jSh.parseJSON(await response.text());

        if (!data.error) {
          viewer.openPage(pageNum, data.data.hash);
        }
      }
    });
  });
  // Load first book

  if (args.bookList.length) {
    loadBook({
      id: args.bookList[0].id,
    });
  }

  function styles(elm, styles) {
    // @ts-ignore
    Object.assign(elm.style, styles);
  };

  function classes(elm, classes, remove = false) {
    const names = classes.split(/\.+/g);

    if (remove) {
      for (const name of names) {
        if (name) {
          elm.classList.remove(name);
        }
      }
    } else {
      for (const name of names) {
        if (name) {
          elm.classList.add(name);
        }
      }
    }
  }

  function animateFullscreen(toggle, book = false, isFull = false) {
    const dur = 260;
    const rect = (toggle ? wboard : wboard.parentNode).getBoundingClientRect();

    const fullyFullStyles = {
      left: "0px",
      right: "0px",
      top: "0px",
      bottom: "0px",
    };
    const fullStyles = {
      left: "0px",
      right: book ? "33.333%" : "0px",
      top: "0px",
      bottom: "0px",
    };
    const smallStyles = {
      left: rect.left + "px",
      right: (innerWidth - rect.right) + "px",
      top: rect.top + "px",
      bottom: (innerHeight - rect.bottom) + "px",
    };

    console.log(smallStyles);

    // Start fixed
    if (toggle) {
      classes(wboard, ".ihf-whiteboard-fullscreen");
      styles(wboard, {
        width: "auto",
      });
    }

    // Get into start position
    styles(wboard, toggle && !isFull
                     ? smallStyles
                     : (isFull
                        ? smallStyles
                        : fullStyles));

    // Start animation
    requestAnimationFrame(() => {
      classes(wboard, ".ihf-whiteboard-animated");
      styles(wboard, toggle ? fullStyles : smallStyles);

      // End animation
      setTimeout(() => {
        classes(wboard, ".ihf-whiteboard-animated", true);

        // Unfix
        if (!toggle) {
          classes(wboard, ".ihf-whiteboard-fullscreen", true);
          styles(wboard, fullStyles);
          styles(wboard, {
            width: "100%",
          });
        }
      }, dur);
    });
  }

  function animateBook(toggle) {
    const dur = 260;
    const openStyle = {
      right: "0",
    };
    const closedStyle = {
      right: "-33.333%",
    };

    // Starter position
    styles(viewerRoot, toggle ? closedStyle : openStyle);

    if (toggle) {
      classes(viewerRoot, ".ihf-visible");
    }

    requestAnimationFrame(() => {
      classes(viewerRoot, ".ihf-book-animated");
      styles(viewerRoot, toggle ? openStyle : closedStyle);

      setTimeout(() => {
        classes(viewerRoot, ".ihf-book-animated", true);

        if (!toggle) {
          classes(viewerRoot, ".ihf-visible", true);
        }
      }, dur);
    });
  }

  function setFullscreen(toggle, books = null) {
    if (books === null) {
      books = isBookvisible;
    }

    if (toggle) {
      btnFullscreenIcon.className = "fa fa-compress";
      btnFullscreenMsg.textContent = locale.minimize;

      // Fill up empty space
      const rect = wboard.getBoundingClientRect();
      wboardHeight = rect.bottom - rect.top
      wboard.parentNode.style.height = wboardHeight + "px";

      // Hide header/navigation
      mainNav.style.display = "none";

      // hide scrollbar
      document.body.style.overflow = "hidden";

      // Start animation
      animateFullscreen(toggle, books, isFullscreen);
    } else {
      setBooksvisible(false, true);
      btnFullscreenIcon.className = "fa fa-expand";
      btnFullscreenMsg.textContent = locale.fullscreen;

      // Show header/navigation
      mainNav.style.display = "block";

      // Show scrollbar
      document.body.style.overflow = "visible";

      // Start animation
      animateFullscreen(toggle, books);
    }

    isFullscreen = toggle;
  }

  function setBooksvisible(toggle, fullScreenToggled = false) {
    if (toggle) {
      setFullscreen(true, true);
      animateBook(toggle);
    } else {
      if (!fullScreenToggled) {
        setFullscreen(true, false);
      }

      animateBook(toggle);
    }

    isBookvisible = toggle;
  }

  btnBooks.addEventListener("click", function() {
    setBooksvisible(!isBookvisible);
  });

  btnFullscreen.addEventListener("click", function() {
    setFullscreen(!isFullscreen);
  });
}
