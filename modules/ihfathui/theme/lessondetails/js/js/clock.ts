import jSh from "jshorts";
import LessonExpired from "./components/lesson-expired/LessonExpired.html";
import { Store } from "svelte/store";
import { AppStore } from "./components/defaults";

// Constants
let tickDelay = 600;
let beaconDelay = 1000 * 30; // 30 seconds
let apiUrl: string;
let locale: ILocale;

const enum Role {
  Professor = 2,
  Student = 3,
}

interface ILocale {
  inLesson: string;
  beforeLesson: string;
  prof: string;
  student: string;
  online: string;
  offline: string;
  cancelled: string;
  actionCancel: string;
  actionCancelDesc: string;
  finished: string;
};

interface IArgs {
  apiUrl: string;
  cancelUrl: string;
  locale: ILocale;
  lessonId: number;
  startDate: number;
  endDate: number;
  duration: number; // Hours
  cancelled: boolean;
  role: Role;
  dom: HTMLElement[];

  lessonExpireWrap: HTMLElement;
  lessonExpireUrls: {
    back: string;
    reschedule: string;
    append: string;
  };
  lessonExpireForm: {
    formBuildId: string;
    formToken: string;
    formId: string;
  };
}

interface IBeaconResponse {
  error: boolean;
  cause?: string;
  data: {
    cancelled: boolean;
    cancellable: boolean;
    finished: boolean;
    partyOnline: boolean;
    success: boolean;
  };
}

class ClockDom {
  private state: {
    time: number;
    startTime: number;
    endTime: number;
    duration: number;
    cancelled: boolean;
    cancellable: boolean;
    partyOnline: boolean;
    finished: boolean;
    role: Role;
  } = {
    time: 0,
    startTime: 0,
    endTime: 0,
    duration: 0,
    cancelled: false,
    cancellable: false,
    partyOnline: false,
    finished: false,
    role: null,
  };
  private IPartialState: {
    [K in keyof ClockDom["state"]]?: ClockDom["state"][K];
  };
  private _update: () => void;
  private root: Node;

  constructor(
    private anchor: HTMLElement,
    private opposingRole: string,
  ) {
    this.root = anchor.parentNode;
    this.build();
  }

  private build() {
    // Ihfath Lesson Details Clock UI
    const time = jSh.d(".ihfath-ldc-elapsed-time.ihfath-ldc-label");
    const timeInLesson = jSh.d(".ihfath-ldc-elapsed-time-label.ihfath-ldc-value", locale.inLesson);
    const timeCol = jSh.d(".ihfath-ldc-col.ihfath-ldc-col-time", null, [
      time,
      timeInLesson,
    ]);

    const opposingName = jSh.d(".ihfath-ldc-opposing-name.ihfath-ldc-label", this.opposingRole);
    const onlineStatus = jSh.d(".ihfath-ldc-opposing-online.ihfath-ldc-value", locale.online);
    const opposingCol = jSh.d(".ihfath-ldc-col.ihfath-ldc-col-opposing", null, [
      opposingName,
      onlineStatus,
    ]);

    const cancelCol = jSh.c("a", {
      sel: ".ihfath-ldc-col.ihfath-ldc-col-cancel",
      child: [
        jSh.c("i", ".fa.fa-times"),
        jSh.c("span", null, locale.actionCancel),
      ],
      prop: {
        title: locale.actionCancelDesc,
      },
    });

    const finishedLabel = jSh.d(".ihfath-ldc-finished", locale.cancelled);

    // UI root node
    const main = jSh.d({
      sel: ".ihfath-ldc-clock",
      child: [
        timeCol,
        opposingCol,
        cancelCol,
        finishedLabel,
      ],
    });

    // Display in document
    this.root.insertBefore(main, this.anchor);
    this.root.removeChild(this.anchor);

    // Set update callback
    this._update = () => {
      const state = this.state;

      // FIXME: Intersecting states should be consolidated
      if (state.cancelled || state.cancellable || state.time > state.duration) {
        if (cancelled || state.cancellable) {
          main.classList.add("ihfath-ldc-state-cancelled");
        } else {
          finishedLabel.textContent = locale.finished;
        }

        if (!main.classList.contains("ihfath-ldc-state-finished")) {
          main.classList.add("ihfath-ldc-state-finished");
          main.classList.remove("ihfath-ldc-state-cancellable");
        }
      } else {
        main.classList.remove("ihfath-ldc-state-finished");

        if (state.partyOnline) {
          onlineStatus.textContent = locale.online;
        } else {
          onlineStatus.textContent = locale.offline;
        }

        if (state.role === Role.Student && state.cancellable) {
          main.classList.add("ihfath-ldc-state-cancellable");
        } else {
          main.classList.remove("ihfath-ldc-state-cancellable");
        }

        const relativeTime = state.time;
        const absRelativeTime = Math.abs(state.time);

        if (relativeTime < 0) {
          timeInLesson.textContent = locale.beforeLesson;
        } else if (state.time < state.duration) {
          timeInLesson.textContent = locale.inLesson;
        } else {
          // Lesson's over
          time.textContent = "N/A";
          timeInLesson.textContent = locale.finished;
          return;
        }

        const hourMs = 1000 * 60 * 60;
        const minMs = 1000 * 60;
        const secMs = 1000;
        const hours = Math.floor(absRelativeTime / hourMs);
        const mins = Math.floor((absRelativeTime - (hourMs * hours)) / minMs);
        const secs = Math.floor((absRelativeTime - (hourMs * hours) - (minMs * mins)) / secMs);

        const padd = (v: number, len: number = 2) => ("" + v).padStart(len, "0");

        time.textContent = `${ padd(hours) }:${ padd(mins) }:${ padd(secs) }`;
      }
    };
    this._update();
  }

  set(state: ClockDom["IPartialState"]) {
    this.state = Object.assign(this.state, state);
    this._update();
  }
}

// State
let cancelled = false;
let partyOnline = false;
let cancellable = false;
let cancelIntent = false;
let lastBeacon = 0;

let lessonId = 0;
let duration = 0; // Hours
let startTime = 0;
let time = 0;
let endTime = 0;

let expiredLesson: LessonExpired = null;
let domInstances: ClockDom[] = [];
let timeout: NodeJS.Timeout = null;

function clockSync({
  apiUrl: apiUrlArg,
  locale: localeArg,
  lessonId: lessonIdArg,
  startDate: startDateArg,
  endDate: endDateArg,
  duration: durationArg,
  cancelled: cancelledArg,
  role: roleArg,
  dom: domArg,
  lessonExpireWrap,
  lessonExpireUrls,
  lessonExpireForm,
} : IArgs) {
  apiUrl = apiUrlArg;
  locale = localeArg;
  startTime = startDateArg;
  endTime = endDateArg;
  lessonId = lessonIdArg;
  duration = durationArg;
  cancelled = cancelledArg;

  timeout = setInterval(tick, tickDelay);
  const roles: {
    [role: number]: string;
  } = {
    2: locale.prof,
    3: locale.student,
  };

  // Create DOM
  for (const domRoot of domArg) {
    const clockDom = new ClockDom(domRoot, roles[5 - roleArg]);
    clockDom.set({
      endTime: endDateArg,
      role: roleArg,
    });

    domInstances.push(clockDom);
  }

  // Create store
  const elStore: AppStore = <any> new Store({
    locale,
  });

  // Create LessonExpire
  expiredLesson = new LessonExpired({
    store: elStore,
    target: lessonExpireWrap,
    data: {
      active: false,
      role: roleArg,
      backUrl: lessonExpireUrls.back,
      rescheduleUrl: lessonExpireUrls.reschedule,
      appendUrl: lessonExpireUrls.append,

      ...lessonExpireForm,
    },
  });

  // Call first tick
  tick();
}

function tick() {
  if (cancelled) {
    for (const dom of domInstances) {
      dom.set({
        cancelled,
      });
    }

    expiredLesson.set({
      active: true,
    });

    // Finish
    return clearInterval(timeout);
  }

  expiredLesson.set({
    active: cancellable,
  });

  // Update time
  const curTime = new Date().getTime();
  time = curTime - startTime;

  for (const dom of domInstances) {
    dom.set({
      time,
      partyOnline,
      cancellable,
      duration,
    });
  }

  if (curTime > startTime && curTime < endTime && (curTime - lastBeacon) > beaconDelay) {
    lastBeacon = curTime;
    message(lessonId);
  }
}

function message(lessonId: number) {
  fetch(apiUrl, {
    method: "POST",
    body: JSON.stringify({
      lessonId,
      cancel: cancelIntent,
    }),
  }).then(response => {
    if (response.ok) {
      response.text().then(data => {
        const parsed: IBeaconResponse = JSON.parse(data);

        if (parsed.error) {
          console.error("Failed to send beacon status code: ", parsed.cause);
        } else {
          // Response is ok
          const res = parsed.data;

          if (res.cancelled) {
            cancelled = true;
          } else {
            partyOnline = res.partyOnline;
            cancellable = res.cancellable;
          }
        }
      });
    } else {
      console.error("Failed to send beacon status code: ", response.status);
    }
  });
}

export {
  clockSync,
};
