// @ts-check
export function LessonNote(options) {
  var ctrl = lces.new();
  const {
    submit,
    input,
    wrap,
    stars,
  } = options;

  // Setup ctrl
  {
    ctrl.setState("rating", 5);
    ctrl.setState("note", "");
    ctrl.setState("enabled", false);
    ctrl.setState("submitted", false);

    // Add events
    ctrl.addStateListener("enabled", function(enabled) {
      if (enabled) {
        submit.classList.add("main-bg");
      } else {
        submit.classList.remove("main-bg");
      }
    });

    input.addEventListener("input", function() {
      ctrl.enabled = enabled(this);
    });

    submit.addEventListener("click", function() {
      if (ctrl.enabled) {
        input.disabled = true;
        ctrl.submitted = true;
        wrap.classList.add("ihfath-ln-submitted");

        (new lcRequest({
          uri: options.baseURL + "/ihfath/ajax/general/lessonUpdate",
          method: "POST",
          formData: JSON.stringify({
            lid: options.lid,
            rating: ctrl.rating,
            note_body: input.value.trim()
          }),
          success: function() {
            console.log("UPDATE NOTES", this.responseText);
          }
        })).send();
      }
    });

    function enabled(input) {
      return input.value.trim().length >= options.minNoteChar;
    }

    // Check if enabled
    ctrl.enabled = enabled(input);
  }

  // Star logic
  var rating = 0;
  var visual = 5;

  stars.forEach(function(star, i) {
    var starValue = i + 1;

    star.addEventListener("mousedown", function() {
      if (ctrl.submitted) {
        return false;
      }

      rating = starValue;

      renderStars(rating);
    });

    star.addEventListener("mouseover", function() {
      if (rating !== 0) {
        return false;
      }

      visual = starValue;
      renderStars(starValue);
    });
  });

  ctrl.addStateListener("submitted", function(submitted) {
    if (submitted) {
      rating = visual;
      renderStars(rating);
    }
  });

  function renderStars(rating) {
    // Update controller
    ctrl.rating = rating--;

    // Render update to stars
    stars.forEach(function(star, i_s) {
      if (i_s > rating) {
        star.classList.remove("main-color");
      } else {
        star.classList.add("main-color");
      }
    });
  }

  return ctrl;
}
