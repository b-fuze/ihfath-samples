// @ts-check
export function Attachments(args) {
  const {
    attachBasePath,
    attachWrap,
    form,
    formSubmit,
    formWrap,
    baseUrl,
    lessonId,
    localeUploading,
  } = args;

  form.addEventListener("submit", function submitCallback(e) {
    e.preventDefault();

    var data = new FormData(form);

    formSubmit.value = localeUploading;
    formSubmit.disabled = true;

    // Request
    var req = new lcRequest({
      method: "POST",
      uri: window.location.pathname,
      formData: data,
      success: function() {
        var output = jSh.parseJSON(this.responseText);

        if (!output.error) {
          var newAttachments = attachments(output.attachments);

          attachWrap.innerHTML = "";
          attachWrap.appendChild(newAttachments);

          formWrap.innerHTML = output.form;
          IhfathmsgRenewFileInputs();

          form = formWrap.jSh(0);
          form.addEventListener("submit", submitCallback);

          // Fix submit button's classnames
          formSubmit = jSh("#edit-submit");
          formSubmit.className = "btn main-bg btn-md form-submit btn-round";
        }
      }
    });

    req.send();
  });

  function attachments(attachGroups) {
    var itemsMapped = [];

    attachGroups.forEach(function(group) {
      group.attachments.forEach(function(item) {
        itemsMapped.push(jSh.c("a", {
          sel: ".ihfathmsg-msg-file-anchor",
          prop: {
            title: item.file_name,
            href: attachBasePath + item.file_hash + "/" + item.file_name,
          },
          child: [
            jSh.c("span", ".ihfathmsg-msg-file-name", item.file_name),
            jSh.c("span", ".ihfathmsg-msg-file-size", item.file_size),
          ],
        }));
      });
    });

    return itemsMapped;
  }
}
