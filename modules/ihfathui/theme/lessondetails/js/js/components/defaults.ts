import { Store } from "./base"

export type AppStore = Store<{
  locale: {
    lessonExpireMessageProfessor: string;
    lessonExpireBackHomeBtn: string;

    lessonExpireMessageStudent: string;
    lessonExpireRescheduleBtn: string;
    lessonExpireAppendBtn: string;
  };
}>;
