import Base from "../base";
import { AppStore } from "../defaults";

declare class LessonExpired extends Base<{
  active: boolean;
  role: 1 | 2 | 3;
  backUrl: string;
  rescheduleUrl: string;
  appendUrl: string;

  actionUrl: string;
  formBuildId: string;
  formToken: string;
  formId: string;
}, {}, AppStore> { };

export default LessonExpired;

export function submit(evt: Event) {
  const form = <HTMLFormElement> evt.target;



}

export function setActionUrl(this: LessonExpired, evt: Event) {
  const { rescheduleUrl, appendUrl } = this.get();
  const btn = <HTMLButtonElement> evt.target;
  let actionUrl = "";

  if (btn.name === "reschedule") {
    actionUrl = rescheduleUrl;
  } else {
    actionUrl = appendUrl;
  }

  this.set({
    actionUrl,
  });
}
