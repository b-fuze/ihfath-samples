<?php
global $language;
$lang = $language->language;

// Exposed variable:
//   - $course        Course with prices
//   - $products      Hour products

// Remap to an ID map
$products_ = array();

foreach ($products as $product) {
  $products_[$product->product_id] = $product;
}

$products = $products_;

// Get field value
$fv = function($field, $markup = TRUE, $column = "value") use($lang) {
  $use_lang = $lang;

  // global
  if (!isset($field[$lang])) {
    $use_lang = LANGUAGE_NONE;
  }

  if (!isset($field[$use_lang])) {
    return "";
  }

  $value = $field[$use_lang][0][$column];
  if ($markup) {
    return check_markup($value, "ihfathui_markdown");
  } else {
    return $value;
  }
};

$featured_price = $fv($course->field_ihfathlsn_c_f_price, 0);
$p_ids = json_decode($fv($course->field_ihfathlsn_c_prices, 0));

?>

<section class="content-hero">
  <div class="constrain">
    <h2 class="header">
      <?= t("Pricing for ") . $course->data["current"]->name ?>
    </h2>

    <div class="prices">
      <?php

      foreach ($p_ids as $product_id) {

        if (isset($products[$product_id])) {
          $product = $products[$product_id];
          ?>

          <div class="price-card <?= ($featured_price == $product->product_id ? 'featured' : '') ?>">
            <h2 class="title"><?= ($fv($product->field_ihfath_lesson_hours, 0)) . t(" hours") ?></h2>
            <div class="price">€<?= ($fv($product->commerce_price, 0, "amount") / 100) ?></div>
            <div class="details">
              <div class="detail">Premier lesson</div>
              <div class="detail">Top class</div>
            </div>
            <a href="<?= url("koa/checkout/{$course->name}/{$product->product_id}") ?>" class="button shadow">
              <?= t("Purchase") ?>
            </a>
          </div>

          <?php
        }
      } ?>
    </div>

    <a
      href="<?= url("course/" . $course->name) ?>"
      class="button shadow back-to-course">
      <?= t("← Back to course page") ?>
    </a>
  </div>
</section>
