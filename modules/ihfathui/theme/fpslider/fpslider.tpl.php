<?php
// KOA Frontpage slider and login
global $user;

// Exposed:
//   - $login_form     The login form
function jst(...$args) {
  return json_encode(t(...$args));
}

function jse(...$args) {
  return json_encode(...$args);
}

?>
<section class="content content-hero">
  <div class="background"></div>
  <div class="constrain">
    <div class="koa-welcome">
      <!-- <img src="<?= url('sites/all/themes/kingdomarabic/assets/graphics/logo.png') ?>" alt="<?= t('Kingdom of Arabic School Logo') ?>" class="logo"> -->
      <h1 class="company">
        <?= t('Kingdom of Arabic School') ?>
      </h1>
      <div class="description">
        <?= t("
        Une nouvelle approche dans l'apprentissage de la langue Arabe et du Coran avec des professeurs en vidéo conférence.
        ") /* TODO: Replace with messaging API */ ?>
      </div>
    </div>

    <?php
    if (!($user->uid > 0)) { ?>

      <form id="ihfath-login" method="POST" action="" class="login">
        <h2 class="header"><?= t("Login") ?></h2>
        <div class="login-error button">
          <span class="icon">&#215;</span>
          <span class="message"></span>
        </div>
        <input class="input darker" type="text" name="username" placeholder="<?= t("Username") ?>">
        <input class="input darker" type="password" name="password" placeholder="<?= t("Password") ?>">
        <button type="submit" class="submit-btn button"><?= t("Login") ?></button>
      </form>
      <script language="javascript">
        // AJAX login procedures
        window.addEventListener("DOMContentLoaded", function() {
          const apiUri = <?= jse(url("ihfath/ajax/login")) ?>;
          const form = document.getElementById("ihfath-login");
          const message = form.querySelector(".login-error");
          const messageBody = message.querySelector(".message");
          const submitBtn = form.querySelector(".submit-btn");
          const loginForm = <?= jse($login_form) ?>;

          const messages = {
            wrong: <?= jst("Wrong credentials") ?>,
            required: <?= jst("Both fields required") ?>,
          };

          function showMessage(msg = "wrong") {
            message.style.display = "flex";
            messageBody.textContent = messages[msg];
          }

          let pendingForm = false;
          form.addEventListener("submit", function(e) {
            e.preventDefault();

            const username = form.username.value.trim();
            const password = form.password.value;

            if (!pendingForm) {
              // Don't let the user submit empty fields
              if (!(username && password)) {
                return showMessage("required");
              }

              pendingForm = true;

              form.username.disabled = true;
              form.password.disabled = true;
              submitBtn.classList.add("is-pending");

              login(loginForm, form.username.value, form.password.value)
                .then(successfulLogin => {
                  form.username.disabled = false;
                  form.password.disabled = false;
                  submitBtn.classList.remove("is-pending");

                  if (successfulLogin) {
                    document.location = ".";
                  } else {
                    showMessage("wrong");
                  }

                  // Not a pending form anymore
                  pendingForm = false;
                });
            }
          });

          async function login(formFields, user, pass) {
            const data = new FormData();

            for (const [key, value] of Object.entries(formFields)) {
              data.set(key, value);
            }

            // Add credentials
            data.set("name", user);
            data.set("pass", pass);

            const response = await fetch(apiUri, {
              method: "POST",
              body: data,
            });

            if (response.ok) {
              const result = JSON.parse(await response.text());

              // TODO: Maybe use the UID for something later
              return !result.error;
            }

            // FIXME: Consider other potential errors here
          }
        });
      </script>

    <?php
    } ?>
  </div>
</section>
