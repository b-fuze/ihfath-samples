<?php

// Variables provided:
// $state = "thumb" | "canvas" | "public";
// $data = {region1: "Foo", region2: "Bar", region3: NULL, ...};
// $settings = {foo: "bar", ...};
// $style_policy = 0 | 1 | 2; // 0: No styles- only content, 1: Only styles- no content, 2: Both styles and content
// $theme_name = "...theme name...";
// $theme_meta = [ ... metadata ... ];
// $file_base = url("sites/default/files/ihfathusercontent");

// Include theme-required PHP:

// Variables provided:
// $state = "thumb" | "canvas" | "public";
// $data = {region1: "Foo", region2: "Bar", region3: NULL, ...};
// $settings = {foo: "bar", ...};
// $style_policy = 0 | 1 | 2; // 0: No styles- only content, 1: Only styles- no content, 2: Both styles and content
$regions = [];
$theme_name = basename(__FILE__, ".tpl.php");
$theme_meta = ihfathui_fp_course_theme_list()[$theme_name];
$file_base = url("sites/default/files/ihfathusercontent");

$region_visible = function($region_id) use ($data) {
  return isset($data[$region_id]) && (is_string($data[$region_id])
                                  ? trim($data[$region_id])
                                  : TRUE);
};

$region_control = function($id, $setting = FALSE) use ($state) {
  if ($state === "canvas") {
    return 'data-theme-editor-region' . ($setting ? '-setting' : '') . '="' . $id . '"';
  }

  return "";
};

$region = function($region_id, $dummy_text) use ($state, $data, $region_visible) {
  $print = $dummy_text;

  switch ($state) {
    case "thumb":
      // Thumbnail, just print
      return $print;
    case "canvas":
      if ($region_visible($region_id)) {
        $print = $data[$region_id];
      }

      return $print;
    case "public":
      if ($region_visible($region_id)) {
        return check_markup($data[$region_id], "ihfathui_markdown");
      }

      return "";
  }
};

$region_raw = function($region_id, $dummy_text) use ($state, $data, $region_visible) {
  $print = $dummy_text;

  switch ($state) {
    case "thumb":
      // Thumbnail, just print
      return $print;
    case "canvas":
      if ($region_visible($region_id)) {
        $print = $data[$region_id];
      }

      return $print;
    case "public":
      if ($region_visible($region_id)) {
        return $data[$region_id];
      }

      return "";
  }
};

$setting = function($setting, $default = "") use ($state, $settings, $data, $theme_meta) {
  if ($state === "public") {
    if (isset($settings->{$setting})) {
      return $settings->{$setting};
    }
  }

  return $default;
};

$link = function($sett, $default) use ($state, $setting, $settings) {
  if ($state === "canvas") {
    return "javascript:0[0]";
  } else {
    return $setting($sett, $default);
  }
};

$userlist = function($user_list) {
  $uids = array_filter(explode(",", $user_list), function($item) {
    return is_numeric($item);
  });

  if (count($uids)) {
    return user_load_multiple($uids);
  }

  return array();
};
;

// Theme functions:
//  - $region_control(string $id, bool $setting)           // Get a region element's attribute for use by the theme editor
//  - $region(string $id, string $dummy_text)              // Get a region's (markdown rendered) value
//  - $region_raw(string $id, string $dummy_text)          // Get a region's (raw) value
//  - $setting(string $setting_key, mixed $default_value)  // Get a setting's value
//  - $link(string $setting_key, mixed $default_value)     // Get a link/url, defaulting to `javascript:void 0` in a `canvas` state

if ($style_policy === 0 || $style_policy === 2) { ?>

  <section
    class="koa-content theme5-bullet-points"
    <?= $setting('course_name') ? 'id="' . $setting('course_name') . '"' : '' ?>
    style="background-image: url('<?= $file_base . '/' . $setting('background', 'css/graphics/arabic-book-covers-2.png') ?>">
    <div class="constrain">
      <ul class="bullet-points">
        <li>
          <div class="point-text">
            <h3 class="title" <?= $region_control('point-1-title') ?>>
              <?= $region('point-1-title', 'Title') ?>
            </h3>
            <div class="text-body" <?= $region_control('point-1') ?>>
              <?= $region('point-1', '
              Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
              eiusmod tempor incididunt ut labore et dolore magna aliqua.
              ') ?>
            </div>
          </div>
          <div class="point-image">
            <img src="<?= $file_base . '/' . $region_raw('point-1-image', 'css/graphics/arabic-book-covers-2.png') ?>" alt="" <?= $region_control('point-1-image') ?>>
          </div>
        </li>
        <li>
          <div class="point-text">
            <h3 class="title" <?= $region_control('point-2-title') ?>>
              <?= $region('point-2-title', 'Title') ?>
            </h3>
            <div class="text-body" <?= $region_control('point-2') ?>>
              <?= $region('point-2', '
              Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
              eiusmod tempor incididunt ut labore et dolore magna aliqua.
              ') ?>
            </div>
          </div>
          <div class="point-image">
            <img src="<?= $file_base . '/' . $region_raw('point-2-image', 'css/graphics/arabic-book-covers-2.png') ?>" alt="" <?= $region_control('point-2-image') ?>>
          </div>
        </li>
        <li>
          <div class="point-text">
            <h3 class="title" <?= $region_control('point-3-title') ?>>
              <?= $region('point-3-title', 'Title') ?>
            </h3>
            <div class="text-body" <?= $region_control('point-3') ?>>
              <?= $region('point-3', '
              Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
              eiusmod tempor incididunt ut labore et dolore magna aliqua.
              ') ?>
            </div>
          </div>
          <div class="point-image">
            <img src="<?= $file_base . '/' . $region_raw('point-3-image', 'css/graphics/arabic-book-covers-2.png') ?>" alt="" <?= $region_control('point-3-image') ?>>
          </div>
        </li>
      </ul>
    </div>
  </section>

<?php
}

if ($style_policy === 1 || $style_policy === 2) { ?>

  <style>
  .theme5-bullet-points {
  display: block;
  background-color: #253A53;
  background-size: cover;
  background-position: center;
}

.theme5-bullet-points .constrain {
  display: flex;
  flex-flow: row nowrap;
  justify-content: center;
  padding: 6rem 0;
  max-width: 1024px;
  margin: 0 auto;
}

.theme5-bullet-points .bullet-points {
  list-style: none;
  color: #fff;
  padding: 0;
}

.theme5-bullet-points .bullet-points li {
  position: relative;
  display: flex;
  flex-flow: row nowrap;
  max-width: 850px;
}

.theme5-bullet-points .bullet-points li::before {
  content: "";
  position: absolute;
  top: 0;
  bottom: 0;
  left: 50%;
  width: 4px;
  background: #00FF00;
  transform: translate(-50%, 0);
}

.theme5-bullet-points .bullet-points li::after {
  content: "";
  position: absolute;
  top: 50%;
  left: 50%;
  width: 35px;
  height: 35px;
  box-sizing: border-box;
  border-radius: 100%;
  background: #253A53;
  border: 4px solid #00FF00;
  transform: translate(-50%, -50%);
}

.theme5-bullet-points .bullet-points li:first-child::before {
  top: 50%;
}

.theme5-bullet-points .bullet-points li:last-child::before {
  bottom: 50%;
}

.theme5-bullet-points .bullet-points li:not(:first-child) {
  padding-top: 4rem;
}

.theme5-bullet-points .bullet-points li .point-text {
  width: 50%;
  padding-right: 4rem;
  box-sizing: border-box;
  font-size: 1.2rem;
  line-height: 1.8rem;
}

.theme5-bullet-points .bullet-points li .point-image {
  position: relative;
  padding-left: 4.5rem;
  width: 50%;
  box-sizing: border-box;
}

.theme5-bullet-points .bullet-points li .point-image img {
  max-width: 100%;
}

  </style>

<?php
} ?>
