<?php

// Variables provided:
// $state = "thumb" | "canvas" | "public";
// $data = {region1: "Foo", region2: "Bar", region3: NULL, ...};
// $settings = {foo: "bar", ...};
// $style_policy = 0 | 1 | 2; // 0: No styles- only content, 1: Only styles- no content, 2: Both styles and content
// $theme_name = "...theme name...";
// $theme_meta = [ ... metadata ... ];
// $file_base = url("sites/default/files/ihfathusercontent");

// Include theme-required PHP:
$THEME_HEAD;

// Theme functions:
//  - $region_control(string $id, bool $setting)           // Get a region element's attribute for use by the theme editor
//  - $region(string $id, string $dummy_text)              // Get a region's (markdown rendered) value
//  - $region_raw(string $id, string $dummy_text)          // Get a region's (raw) value
//  - $setting(string $setting_key, mixed $default_value)  // Get a setting's value
//  - $link(string $setting_key, mixed $default_value)     // Get a link/url, defaulting to `javascript:void 0` in a `canvas` state

if ($style_policy === 0 || $style_policy === 2) { ?>

  <section
    class="koa-content theme5-bullet-points"
    <?= $setting('course_name') ? 'id="' . $setting('course_name') . '"' : '' ?>
    style="background-image: url('<?= $file_base . '/' . $setting('background', 'css/graphics/arabic-book-covers-2.png') ?>">
    <div class="constrain">
      <ul class="bullet-points">
        <li>
          <div class="point-text">
            <h3 class="title" <?= $region_control('point-1-title') ?>>
              <?= $region('point-1-title', 'Title') ?>
            </h3>
            <div class="text-body" <?= $region_control('point-1') ?>>
              <?= $region('point-1', '
              Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
              eiusmod tempor incididunt ut labore et dolore magna aliqua.
              ') ?>
            </div>
          </div>
          <div class="point-image">
            <img src="<?= $file_base . '/' . $region_raw('point-1-image', 'css/graphics/arabic-book-covers-2.png') ?>" alt="" <?= $region_control('point-1-image') ?>>
          </div>
        </li>
        <li>
          <div class="point-text">
            <h3 class="title" <?= $region_control('point-2-title') ?>>
              <?= $region('point-2-title', 'Title') ?>
            </h3>
            <div class="text-body" <?= $region_control('point-2') ?>>
              <?= $region('point-2', '
              Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
              eiusmod tempor incididunt ut labore et dolore magna aliqua.
              ') ?>
            </div>
          </div>
          <div class="point-image">
            <img src="<?= $file_base . '/' . $region_raw('point-2-image', 'css/graphics/arabic-book-covers-2.png') ?>" alt="" <?= $region_control('point-2-image') ?>>
          </div>
        </li>
        <li>
          <div class="point-text">
            <h3 class="title" <?= $region_control('point-3-title') ?>>
              <?= $region('point-3-title', 'Title') ?>
            </h3>
            <div class="text-body" <?= $region_control('point-3') ?>>
              <?= $region('point-3', '
              Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
              eiusmod tempor incididunt ut labore et dolore magna aliqua.
              ') ?>
            </div>
          </div>
          <div class="point-image">
            <img src="<?= $file_base . '/' . $region_raw('point-3-image', 'css/graphics/arabic-book-covers-2.png') ?>" alt="" <?= $region_control('point-3-image') ?>>
          </div>
        </li>
      </ul>
    </div>
  </section>

<?php
}

if ($style_policy === 1 || $style_policy === 2) { ?>

  <style>
  THEME_CSS{}
  </style>

<?php
} ?>
