<?php

// Variables provided:
// $state = "thumb" | "canvas" | "public";
// $data = {region1: "Foo", region2: "Bar", region3: NULL, ...};
// $settings = {foo: "bar", ...};
// $style_policy = 0 | 1 | 2; // 0: No styles- only content, 1: Only styles- no content, 2: Both styles and content
// $theme_name = "...theme name...";
// $theme_meta = [ ... metadata ... ];
// $file_base = url("sites/default/files/ihfathusercontent");

// Include theme-required PHP:
$THEME_HEAD;

// Theme functions:
//  - $region_control(string $id, bool $setting)           // Get a region element's attribute for use by the theme editor
//  - $region(string $id, string $dummy_text)              // Get a region's (markdown rendered) value
//  - $setting(string $setting_key, mixed $default_value)  // Get a setting's value
//  - $link(string $setting_key, mixed $default_value)     // Get a link/url, defaulting to `javascript:void 0` in a `canvas` state

$template_instance = uniqid();
$getfield = function($entity, $field, $default = "") {
  return $entity->{$field}[LANGUAGE_NONE][0]["value"] ?? $default;
};

// Remap user list to simpler values
$users = array_values(array_map(function($user) use($getfield) {
  $name = trim($getfield($user, "field_ihfath_firstname") . " " . $getfield($user, "field_ihfath_lastname"));

  return (object) array(
    "uid" => $user->uid,
    "picture" => $user->picture ? file_create_url($user->picture->uri) : "",
    "name" => $name ? $name : $user->name,
    "cv" => $getfield($user, "field_ihfath_cv"),
    "video" => $getfield($user, "field_ihfath_videodemo"),
  );
}, $userlist($setting('teachers', ''))));

// Render theme
if ($style_policy === 0 || $style_policy === 2) { ?>

  <section
    <?= $setting('course_name') ? 'id="' . $setting('course_name') . '"' : '' ?>
    class="koa-content theme6-user-list">
    <div class="constrain">
      <h2 class="title" <?= $region_control('title') ?>>
        <?= $region('title', 'Section Title') ?>
      </h2>
      <div class="teachers">
        <?php
        foreach ($users as $user) { ?>

          <div class="teacher">
            <div
              class="image"
              style="background-image: url('<?= $user->picture ?>')"></div>
            <h3 class="name"><?= $user->name ?></h3>
            <button
              class="open-cv">
              <span>Open CV</span>
              <svg
                xmlns="http://www.w3.org/2000/svg"
                viewBox="0 0 24 24">
                <path d="M16 16c0 1.104-.896 2-2 2h-12c-1.104 0-2-.896-2-2v-8c0-1.104.896-2 2-2h12c1.104 0 2 .896 2 2v8zm8-10l-6 4.223v3.554l6 4.223v-12z"/>
              </svg>
            </button>
            <div class="hidden">
              <div
                class="cv"
                data-video="<?= check_plain($user->video) ?>"
                data-name="<?= check_plain($user->name) ?>">
                <?= check_markup($user->cv, "ihfathui_markdown") ?>
              </div>
            </div>
          </div>

        <?php
        } ?>
      </div>
    </div>
  </section>

<?php
}

if ($style_policy === 1 || $style_policy === 2) { ?>

  <style>
  THEME_CSS{}
  </style>

  <div class="teacher-cv-overlay">
    <div class="popup">
      <div class="video">
        <iframe
          src=""
          frameborder="0"
          allowfullscreen></iframe>
      </div>
      <div class="details">
        <h2 class="name"></h2>
        <div class="info"></div>
      </div>
      <div class="close">
        <span><?= t("Close") ?></span>
        <svg
          xmlns="http://www.w3.org/2000/svg"
          viewBox="0 0 24 24">
          <path d="M24 20.188l-8.315-8.209 8.2-8.282-3.697-3.697-8.212 8.318-8.31-8.203-3.666 3.666 8.321 8.24-8.206 8.313 3.666 3.666 8.237-8.318 8.285 8.203z"/>
        </svg>
      </div>
    </div>
  </div>

  <script>
    window.addEventListener("DOMContentLoaded", function() {
      const overlay = document.querySelector(".teacher-cv-overlay");
      const video = overlay.querySelector("iframe");
      const name = overlay.querySelector(".name");
      const info = overlay.querySelector(".info");
      const triggerButtons = document.querySelectorAll(".theme6-user-list .open-cv");

      function openOverlay(infoNodes) {
        const profName = infoNodes.dataset.name;
        const profVideo = (infoNodes.dataset.video + "").match(/v=([^&]+)(?:\s+|&|$)/);

        video.src = "https://www.youtube.com/embed/" + ((profVideo && profVideo[1]) || "NiGbPT8w8mY");
        name.textContent = profName;
        info.innerHTML = "";
        info.appendChild(infoNodes);

        overlay.classList.add("visible");
      }

      function closeOverlay() {
        overlay.classList.remove("visible");
        video.src = "data:text/html;<html></html>";
      }

      // Close overlay when clicking on dark shade
      overlay.addEventListener("click", function(e) {
        if (e.target === this) {
          closeOverlay();
        }
      });

      // Open overlay when clicking a button
      for (let i=0; i<triggerButtons.length; i++) {
        const button = triggerButtons[i];
        const infoNodes = button.nextElementSibling.children[0].cloneNode(true);

        button.addEventListener("click", function() {
          openOverlay(infoNodes);
        });
      }
    });
  </script>

<?php
} ?>
