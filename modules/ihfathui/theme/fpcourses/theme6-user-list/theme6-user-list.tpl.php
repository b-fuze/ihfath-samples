<?php

// Variables provided:
// $state = "thumb" | "canvas" | "public";
// $data = {region1: "Foo", region2: "Bar", region3: NULL, ...};
// $settings = {foo: "bar", ...};
// $style_policy = 0 | 1 | 2; // 0: No styles- only content, 1: Only styles- no content, 2: Both styles and content
// $theme_name = "...theme name...";
// $theme_meta = [ ... metadata ... ];
// $file_base = url("sites/default/files/ihfathusercontent");

// Include theme-required PHP:

// Variables provided:
// $state = "thumb" | "canvas" | "public";
// $data = {region1: "Foo", region2: "Bar", region3: NULL, ...};
// $settings = {foo: "bar", ...};
// $style_policy = 0 | 1 | 2; // 0: No styles- only content, 1: Only styles- no content, 2: Both styles and content
$regions = [];
$theme_name = basename(__FILE__, ".tpl.php");
$theme_meta = ihfathui_fp_course_theme_list()[$theme_name];
$file_base = url("sites/default/files/ihfathusercontent");

$region_visible = function($region_id) use ($data) {
  return isset($data[$region_id]) && (is_string($data[$region_id])
                                  ? trim($data[$region_id])
                                  : TRUE);
};

$region_control = function($id, $setting = FALSE) use ($state) {
  if ($state === "canvas") {
    return 'data-theme-editor-region' . ($setting ? '-setting' : '') . '="' . $id . '"';
  }

  return "";
};

$region = function($region_id, $dummy_text) use ($state, $data, $region_visible) {
  $print = $dummy_text;

  switch ($state) {
    case "thumb":
      // Thumbnail, just print
      return $print;
    case "canvas":
      if ($region_visible($region_id)) {
        $print = $data[$region_id];
      }

      return $print;
    case "public":
      if ($region_visible($region_id)) {
        return check_markup($data[$region_id], "ihfathui_markdown");
      }

      return "";
  }
};

$region_raw = function($region_id, $dummy_text) use ($state, $data, $region_visible) {
  $print = $dummy_text;

  switch ($state) {
    case "thumb":
      // Thumbnail, just print
      return $print;
    case "canvas":
      if ($region_visible($region_id)) {
        $print = $data[$region_id];
      }

      return $print;
    case "public":
      if ($region_visible($region_id)) {
        return $data[$region_id];
      }

      return "";
  }
};

$setting = function($setting, $default = "") use ($state, $settings, $data, $theme_meta) {
  if ($state === "public") {
    if (isset($settings->{$setting})) {
      return $settings->{$setting};
    }
  }

  return $default;
};

$link = function($sett, $default) use ($state, $setting, $settings) {
  if ($state === "canvas") {
    return "javascript:0[0]";
  } else {
    return $setting($sett, $default);
  }
};

$userlist = function($user_list) {
  $uids = array_filter(explode(",", $user_list), function($item) {
    return is_numeric($item);
  });

  if (count($uids)) {
    return user_load_multiple($uids);
  }

  return array();
};
;

// Theme functions:
//  - $region_control(string $id, bool $setting)           // Get a region element's attribute for use by the theme editor
//  - $region(string $id, string $dummy_text)              // Get a region's (markdown rendered) value
//  - $setting(string $setting_key, mixed $default_value)  // Get a setting's value
//  - $link(string $setting_key, mixed $default_value)     // Get a link/url, defaulting to `javascript:void 0` in a `canvas` state

$template_instance = uniqid();
$getfield = function($entity, $field, $default = "") {
  return $entity->{$field}[LANGUAGE_NONE][0]["value"] ?? $default;
};

// Remap user list to simpler values
$users = array_values(array_map(function($user) use($getfield) {
  $name = trim($getfield($user, "field_ihfath_firstname") . " " . $getfield($user, "field_ihfath_lastname"));

  return (object) array(
    "uid" => $user->uid,
    "picture" => $user->picture ? file_create_url($user->picture->uri) : "",
    "name" => $name ? $name : $user->name,
    "cv" => $getfield($user, "field_ihfath_cv"),
    "video" => $getfield($user, "field_ihfath_videodemo"),
  );
}, $userlist($setting('teachers', ''))));

// Render theme
if ($style_policy === 0 || $style_policy === 2) { ?>

  <section
    <?= $setting('course_name') ? 'id="' . $setting('course_name') . '"' : '' ?>
    class="koa-content theme6-user-list">
    <div class="constrain">
      <h2 class="title" <?= $region_control('title') ?>>
        <?= $region('title', 'Section Title') ?>
      </h2>
      <div class="teachers">
        <?php
        foreach ($users as $user) { ?>

          <div class="teacher">
            <div
              class="image"
              style="background-image: url('<?= $user->picture ?>')"></div>
            <h3 class="name"><?= $user->name ?></h3>
            <button
              class="open-cv">
              <span>Open CV</span>
              <svg
                xmlns="http://www.w3.org/2000/svg"
                viewBox="0 0 24 24">
                <path d="M16 16c0 1.104-.896 2-2 2h-12c-1.104 0-2-.896-2-2v-8c0-1.104.896-2 2-2h12c1.104 0 2 .896 2 2v8zm8-10l-6 4.223v3.554l6 4.223v-12z"/>
              </svg>
            </button>
            <div class="hidden">
              <div
                class="cv"
                data-video="<?= check_plain($user->video) ?>"
                data-name="<?= check_plain($user->name) ?>">
                <?= check_markup($user->cv, "ihfathui_markdown") ?>
              </div>
            </div>
          </div>

        <?php
        } ?>
      </div>
    </div>
  </section>

<?php
}

if ($style_policy === 1 || $style_policy === 2) { ?>

  <style>
  .theme6-user-list {
  display: block;
  background-color: #FFF;
}

.theme6-user-list .constrain {
  display: flex;
  flex-flow: column nowrap;
  align-items: center;
  padding: 6rem 0;
  max-width: 1024px;
  margin: 0 auto;
}

.theme6-user-list .constrain > .title {
  margin: 0;
  margin-bottom: 3rem;
  font-weight: 200;
  font-size: 3rem;
  font-family: "Savoye LET";
  color: #197098;
}

.theme6-user-list .constrain > .title p {
  margin: 0;
}

.theme6-user-list .constrain > .title::after {
  content: "";
  display: block;
  height: 4px;
  width: 3rem;
  margin-top: 1rem;
  margin-right: auto;
  margin-left: auto;
  background: #000;
}

.theme6-user-list .constrain > .title p {
  margin: 0;
}

.theme6-user-list .teachers {
  display: flex;
  justify-content: space-between;
  width: 100%;
  margin-top: 2rem;
}

.theme6-user-list .teachers .teacher {
  display: flex;
  flex-flow: column nowrap;
  align-items: center;
}

.theme6-user-list .teachers .teacher .image {
  width: 180px;
  height: 180px;
  border: 4px solid #197098;
  border-radius: 100%;
  background-size: cover;
  background-repeat: no-repeat;
  background-position: center;
}

.theme6-user-list .teachers .teacher .open-cv {
  display: flex;
  align-items: center;
  padding: 0.5rem 0.75rem;
  font-size: 1.2rem;
  border-radius: 3px;
  border: 0;
  background: #197098;
  cursor: pointer;
}

.theme6-user-list .teachers .teacher .open-cv span {
  margin-right: 0.5rem;
  color: #fff;
}

.theme6-user-list .teachers .teacher .open-cv svg {
  width: 2rem;
}

.theme6-user-list .teachers .teacher .open-cv svg path {
  fill: #fff;
}

.theme6-user-list .teachers .teacher .hidden {
  display: none;
}

.teacher-cv-overlay {
  position: fixed;
  z-index: 110;
  display: none;
  align-items: center;
  justify-content: center;
  left: 0;
  right: 0;
  bottom: 0;
  top: 0;
  background: rgba(0, 0, 0, 0.8);
}

.teacher-cv-overlay.visible {
  display: flex;
}

.teacher-cv-overlay .popup {
  position: relative;
  display: flex;
  flex-flow: row nowrap;
  width: 850px;
  height: 450px;
  background: #fff;
  box-shadow: 0 0.5rem 1rem rgba(0, 0, 0, 0.45);
}

.teacher-cv-overlay .popup .video {
  width: 70%;
  height: 100%;
  background: #AAA;
}

.teacher-cv-overlay .popup .video iframe {
  width: 100%;
  height: 100%;
  margin: 0;
  border: 0;
}

.teacher-cv-overlay .popup .details {
  width: 30%;
  height: 100%;
  box-sizing: border-box;
  padding: 1rem;
  padding-right: 1.5rem;
}

.teacher-cv-overlay .popup .details .name {
  color: #197098;
}

.teacher-cv-overlay .popup .details .info {
  color: #333;
}

.teacher-cv-overlay .popup .close {
  position: absolute;
  display: flex;
  align-items: center;
  right: 0;
  bottom: 100%;
  margin: 1.5rem;
  margin-right: 0;
  pointer-events: none;
}

.teacher-cv-overlay .popup .close svg {
  width: 25px;
}

.teacher-cv-overlay .popup .close svg path {
  fill: rgba(255, 255, 255, 0.7);
}

.teacher-cv-overlay .popup .close span {
  margin-right: 0.8rem;
  font-size: 1.5rem;
  font-family: Roboto;
  color: rgba(255, 255, 255, 0.7);
}

  </style>

  <div class="teacher-cv-overlay">
    <div class="popup">
      <div class="video">
        <iframe
          src=""
          frameborder="0"
          allowfullscreen></iframe>
      </div>
      <div class="details">
        <h2 class="name"></h2>
        <div class="info"></div>
      </div>
      <div class="close">
        <span><?= t("Close") ?></span>
        <svg
          xmlns="http://www.w3.org/2000/svg"
          viewBox="0 0 24 24">
          <path d="M24 20.188l-8.315-8.209 8.2-8.282-3.697-3.697-8.212 8.318-8.31-8.203-3.666 3.666 8.321 8.24-8.206 8.313 3.666 3.666 8.237-8.318 8.285 8.203z"/>
        </svg>
      </div>
    </div>
  </div>

  <script>
    window.addEventListener("DOMContentLoaded", function() {
      const overlay = document.querySelector(".teacher-cv-overlay");
      const video = overlay.querySelector("iframe");
      const name = overlay.querySelector(".name");
      const info = overlay.querySelector(".info");
      const triggerButtons = document.querySelectorAll(".theme6-user-list .open-cv");

      function openOverlay(infoNodes) {
        const profName = infoNodes.dataset.name;
        const profVideo = (infoNodes.dataset.video + "").match(/v=([^&]+)(?:\s+|&|$)/);

        video.src = "https://www.youtube.com/embed/" + ((profVideo && profVideo[1]) || "NiGbPT8w8mY");
        name.textContent = profName;
        info.innerHTML = "";
        info.appendChild(infoNodes);

        overlay.classList.add("visible");
      }

      function closeOverlay() {
        overlay.classList.remove("visible");
        video.src = "data:text/html;<html></html>";
      }

      // Close overlay when clicking on dark shade
      overlay.addEventListener("click", function(e) {
        if (e.target === this) {
          closeOverlay();
        }
      });

      // Open overlay when clicking a button
      for (let i=0; i<triggerButtons.length; i++) {
        const button = triggerButtons[i];
        const infoNodes = button.nextElementSibling.children[0].cloneNode(true);

        button.addEventListener("click", function() {
          openOverlay(infoNodes);
        });
      }
    });
  </script>

<?php
} ?>
