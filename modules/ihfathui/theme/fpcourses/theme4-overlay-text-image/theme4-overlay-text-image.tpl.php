<?php

// Variables provided:
// $state = "thumb" | "canvas" | "public";
// $data = {region1: "Foo", region2: "Bar", region3: NULL, ...};
// $settings = {foo: "bar", ...};
// $style_policy = 0 | 1 | 2; // 0: No styles- only content, 1: Only styles- no content, 2: Both styles and content
// $theme_name = "...theme name...";
// $theme_meta = [ ... metadata ... ];
// $file_base = url("sites/default/files/ihfathusercontent");

// Include theme-required PHP:

// Variables provided:
// $state = "thumb" | "canvas" | "public";
// $data = {region1: "Foo", region2: "Bar", region3: NULL, ...};
// $settings = {foo: "bar", ...};
// $style_policy = 0 | 1 | 2; // 0: No styles- only content, 1: Only styles- no content, 2: Both styles and content
$regions = [];
$theme_name = basename(__FILE__, ".tpl.php");
$theme_meta = ihfathui_fp_course_theme_list()[$theme_name];
$file_base = url("sites/default/files/ihfathusercontent");

$region_visible = function($region_id) use ($data) {
  return isset($data[$region_id]) && (is_string($data[$region_id])
                                  ? trim($data[$region_id])
                                  : TRUE);
};

$region_control = function($id, $setting = FALSE) use ($state) {
  if ($state === "canvas") {
    return 'data-theme-editor-region' . ($setting ? '-setting' : '') . '="' . $id . '"';
  }

  return "";
};

$region = function($region_id, $dummy_text) use ($state, $data, $region_visible) {
  $print = $dummy_text;

  switch ($state) {
    case "thumb":
      // Thumbnail, just print
      return $print;
    case "canvas":
      if ($region_visible($region_id)) {
        $print = $data[$region_id];
      }

      return $print;
    case "public":
      if ($region_visible($region_id)) {
        return check_markup($data[$region_id], "ihfathui_markdown");
      }

      return "";
  }
};

$region_raw = function($region_id, $dummy_text) use ($state, $data, $region_visible) {
  $print = $dummy_text;

  switch ($state) {
    case "thumb":
      // Thumbnail, just print
      return $print;
    case "canvas":
      if ($region_visible($region_id)) {
        $print = $data[$region_id];
      }

      return $print;
    case "public":
      if ($region_visible($region_id)) {
        return $data[$region_id];
      }

      return "";
  }
};

$setting = function($setting, $default = "") use ($state, $settings, $data, $theme_meta) {
  if ($state === "public") {
    if (isset($settings->{$setting})) {
      return $settings->{$setting};
    }
  }

  return $default;
};

$link = function($sett, $default) use ($state, $setting, $settings) {
  if ($state === "canvas") {
    return "javascript:0[0]";
  } else {
    return $setting($sett, $default);
  }
};

$userlist = function($user_list) {
  $uids = array_filter(explode(",", $user_list), function($item) {
    return is_numeric($item);
  });

  if (count($uids)) {
    return user_load_multiple($uids);
  }

  return array();
};
;

// Theme functions:
//  - $region_control(string $id, bool $setting)           // Get a region element's attribute for use by the theme editor
//  - $region(string $id, string $dummy_text)              // Get a region's (markdown rendered) value
//  - $setting(string $setting_key, mixed $default_value)  // Get a setting's value
//  - $link(string $setting_key, mixed $default_value)     // Get a link/url, defaulting to `javascript:void 0` in a `canvas` state

if ($style_policy === 0 || $style_policy === 2) { ?>

  <section
    class="koa-content theme4-overlay-text-image"
    <?= $setting('course_name') ? 'id="' . $setting('course_name') . '"' : '' ?>
    style="background-image: url('<?= $file_base . '/' . $setting('background', 'css/graphics/arabic-book-covers-2.png') ?>">
    <div class="constrain">
      <div
        class="picture"
        style="background-image: url('<?= $file_base . '/' . $setting('overlay_picture', 'css/graphics/arabic-book-covers-2.png') ?>');">
      </div>
      <div class="details">
        <h2 class="title" <?= $region_control('title') ?>>
          <?= $region('title', 'Title') ?>
        </h2>
        <div class="text" <?= $region_control('text') ?>>
          <div class="quotation-mark">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
              <path d="M13 14.725c0-5.141 3.892-10.519 10-11.725l.984 2.126c-2.215.835-4.163 3.742-4.38 5.746 2.491.392 4.396 2.547 4.396 5.149 0 3.182-2.584 4.979-5.199 4.979-3.015 0-5.801-2.305-5.801-6.275zm-13 0c0-5.141 3.892-10.519 10-11.725l.984 2.126c-2.215.835-4.163 3.742-4.38 5.746 2.491.392 4.396 2.547 4.396 5.149 0 3.182-2.584 4.979-5.199 4.979-3.015 0-5.801-2.305-5.801-6.275z"/>
            </svg>
          </div>

          <div class="text-body">
            <?= $region('text', '
            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
            eiusmod tempor incididunt ut labore et dolore magna aliqua.
            ') ?>
          </div>

          <div class="buttons">
            <a href="<?= $link("course_link", "#") ?>" class="button focused">
              <?= t("Learn more") ?>
            </a>
            <a href="<?= $link("course_register_link", "#") ?>" class="button">
              <?= t("Register") ?>
            </a>
          </div>
        </div>
      </div>
    </div>
  </section>

<?php
}

if ($style_policy === 1 || $style_policy === 2) { ?>

  <style>
  .theme4-overlay-text-image {
  position: relative;
  display: block;
  background-color: #197098;
  background-size: cover;
  background-position: center;
}

.theme4-overlay-text-image .constrain {
  display: flex;
  flex-flow: row nowrap;
  padding: 6rem 0;
  max-width: 1024px;
  margin: 0 auto;
}

.theme4-overlay-text-image .details {
  position: relative;
  z-index: 10;
  width: 70%;
  padding: 2rem;
  padding-left: 6rem;
  margin-left: auto;
  flex: 0 0 auto;
  border-bottom: 15px solid #FF4301;
  background: #fff;
}

.theme4-overlay-text-image .details > .title {
  margin: 0;
  margin-bottom: 2rem;
  font-weight: 200;
  font-size: 3rem;
  font-family: "Savoye LET";
  color: #FF4301;
}

.theme4-overlay-text-image .details > .title p {
  margin: 0;
}

.theme4-overlay-text-image .details > .title::after {
  content: "";
  display: block;
  height: 4px;
  width: 3rem;
  margin-top: 1.2rem;
  background: #000;
}

.theme4-overlay-text-image .details .quotation-mark {
  display: block;
  position: relative;
  max-width: 2rem;
  margin-right: 0.65rem;
  padding-left: 0.5rem;
  padding-bottom: 0.5rem;
  float: left;
}

.theme4-overlay-text-image .details .quotation-mark svg {
  width: 100%;
}

.theme4-overlay-text-image .details .quotation-mark path {
  fill: #FF4301;
}

.theme4-overlay-text-image .details .text-body {
  font-size: 1.2rem;
  line-height: 2.2rem;
}

.theme4-overlay-text-image .details .buttons {
  display: flex;
  justify-content: flex-start;
  margin-top: 4rem;
  padding-right: 4rem;
}

.theme4-overlay-text-image .details .buttons .button {
  display: block;
  padding: 0.6rem 0;
  width: 8.5rem;
  margin-right: 2rem;
  font-size: 1rem;
  color: #197098;
  border-radius: 0;
  border: 3px solid #166E97;
  background: #fff;
  text-decoration: none;
}

.theme4-overlay-text-image .details .buttons .button.focused {
  color: #fff;
  background: #166E97;
}

.theme4-overlay-text-image .picture {
  position: absolute;
  z-index: 0;
  width: 100%;
  height: 100%;
  left: 0;
  bottom: 0;
  flex: 1 1 auto;
  background-size: contain;
  background-position: 0 100%;
  background-repeat: no-repeat;
  pointer-events: none;
}

  </style>

<?php
} ?>
