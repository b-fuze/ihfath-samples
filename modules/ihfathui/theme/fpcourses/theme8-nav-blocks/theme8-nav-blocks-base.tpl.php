<?php

// Variables provided:
// $state = "thumb" | "canvas" | "public";
// $data = {region1: "Foo", region2: "Bar", region3: NULL, ...};
// $settings = {foo: "bar", ...};
// $style_policy = 0 | 1 | 2; // 0: No styles- only content, 1: Only styles- no content, 2: Both styles and content
// $theme_name = "...theme name...";
// $theme_meta = [ ... metadata ... ];
// $file_base = url("sites/default/files/ihfathusercontent");

// Include theme-required PHP:
$THEME_HEAD;

// Theme functions:
//  - $region_control(string $id, bool $setting)           // Get a region element's attribute for use by the theme editor
//  - $region(string $id, string $dummy_text)              // Get a region's (markdown rendered) value
//  - $setting(string $setting_key, mixed $default_value)  // Get a setting's value
//  - $link(string $setting_key, mixed $default_value)     // Get a link/url, defaulting to `javascript:void 0` in a `canvas` state

// Get some optional settings
$stamp_setting = $setting('stamp', '');

// Render theme
if ($style_policy === 0 || $style_policy === 2) { ?>

  <section 
    <?= $setting('course_name') ? 'id="' . $setting('course_name') . '"' : '' ?>
    class="koa-content theme8-nav-blocks">
    <div class="constrain">
      <h2 class="title" <?= $region_control('title') ?>>
        <?= $region('title', 'Section Title') ?>
      </h2>
      <ul class="blocks">
        <li class="block">
          <a
            class="visual"
            style="background-image: url('<?= $file_base . '/' . $region_raw('block-1-image', 'css/graphics/arabic-book-covers-2.png') ?>');"
            <?= $region_control('block-1-image') ?>
            href="<?= url($region_raw('block-1-link', '/')) ?>">
            <div class="meta">
              <h3 class="title" <?= $region_control('block-1-title') ?>>
                <?= $region('block-1-title', 'Block Title') ?>
              </h3>
              <button class="block-link" <?= $region_control('block-1-link') ?>>
                <?= t('Learn more') ?>
              </button>
            </div>
          </a>
        </li>
        <li class="block">
          <a
            class="visual"
            style="background-image: url('<?= $file_base . '/' . $region_raw('block-2-image', 'css/graphics/arabic-book-covers-2.png') ?>');"
            <?= $region_control('block-2-image') ?>
            href="<?= url($region_raw('block-2-link', '/')) ?>">
            <div class="meta">
              <h3 class="title" <?= $region_control('block-2-title') ?>>
                <?= $region('block-2-title', 'Block Title') ?>
              </h3>
              <button class="block-link" <?= $region_control('block-2-link') ?>>
                <?= t('Learn more') ?>
              </button>
            </div>
          </a>
        </li>
        <li class="block">
          <a
            class="visual"
            style="background-image: url('<?= $file_base . '/' . $region_raw('block-3-image', 'css/graphics/arabic-book-covers-2.png') ?>');"
            <?= $region_control('block-3-image') ?>
            href="<?= url($region_raw('block-3-link', '/')) ?>">
            <div class="meta">
              <h3 class="title" <?= $region_control('block-3-title') ?>>
                <?= $region('block-3-title', 'Block Title') ?>
              </h3>
              <button class="block-link" <?= $region_control('block-3-link') ?>>
                <?= t('Learn more') ?>
              </button>
            </div>
          </a>
        </li>
        <li class="block">
          <a
            class="visual"
            style="background-image: url('<?= $file_base . '/' . $region_raw('block-4-image', 'css/graphics/arabic-book-covers-2.png') ?>');"
            <?= $region_control('block-4-image') ?>
            href="<?= url($region_raw('block-4-link', '/')) ?>">
            <div class="meta">
              <h3 class="title" <?= $region_control('block-4-title') ?>>
                <?= $region('block-4-title', 'Block Title') ?>
              </h3>
              <button class="block-link" <?= $region_control('block-4-link') ?>>
                <?= t('Learn more') ?>
              </button>
            </div>
          </a>
        </li>
      </ul>
    </div>
  </section>

<?php
}

if ($style_policy === 1 || $style_policy === 2) { ?>

  <style>
  THEME_CSS{}
  </style>

<?php
} ?>
