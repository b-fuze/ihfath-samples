<?php

// Variables provided:
// $state = "thumb" | "canvas" | "public";
// $data = {region1: "Foo", region2: "Bar", region3: NULL, ...};
// $settings = {foo: "bar", ...};
// $style_policy = 0 | 1 | 2; // 0: No styles- only content, 1: Only styles- no content, 2: Both styles and content
// $theme_name = "...theme name...";
// $theme_meta = [ ... metadata ... ];
// $file_base = url("sites/default/files/ihfathusercontent");

// Include theme-required PHP:

// Variables provided:
// $state = "thumb" | "canvas" | "public";
// $data = {region1: "Foo", region2: "Bar", region3: NULL, ...};
// $settings = {foo: "bar", ...};
// $style_policy = 0 | 1 | 2; // 0: No styles- only content, 1: Only styles- no content, 2: Both styles and content
$regions = [];
$theme_name = basename(__FILE__, ".tpl.php");
$theme_meta = ihfathui_fp_course_theme_list()[$theme_name];
$file_base = url("sites/default/files/ihfathusercontent");

$region_visible = function($region_id) use ($data) {
  return isset($data[$region_id]) && (is_string($data[$region_id])
                                  ? trim($data[$region_id])
                                  : TRUE);
};

$region_control = function($id, $setting = FALSE) use ($state) {
  if ($state === "canvas") {
    return 'data-theme-editor-region' . ($setting ? '-setting' : '') . '="' . $id . '"';
  }

  return "";
};

$region = function($region_id, $dummy_text) use ($state, $data, $region_visible) {
  $print = $dummy_text;

  switch ($state) {
    case "thumb":
      // Thumbnail, just print
      return $print;
    case "canvas":
      if ($region_visible($region_id)) {
        $print = $data[$region_id];
      }

      return $print;
    case "public":
      if ($region_visible($region_id)) {
        return check_markup($data[$region_id], "ihfathui_markdown");
      }

      return "";
  }
};

$region_raw = function($region_id, $dummy_text) use ($state, $data, $region_visible) {
  $print = $dummy_text;

  switch ($state) {
    case "thumb":
      // Thumbnail, just print
      return $print;
    case "canvas":
      if ($region_visible($region_id)) {
        $print = $data[$region_id];
      }

      return $print;
    case "public":
      if ($region_visible($region_id)) {
        return $data[$region_id];
      }

      return "";
  }
};

$setting = function($setting, $default = "") use ($state, $settings, $data, $theme_meta) {
  if ($state === "public") {
    if (isset($settings->{$setting})) {
      return $settings->{$setting};
    }
  }

  return $default;
};

$link = function($sett, $default) use ($state, $setting, $settings) {
  if ($state === "canvas") {
    return "javascript:0[0]";
  } else {
    return $setting($sett, $default);
  }
};

$userlist = function($user_list) {
  $uids = array_filter(explode(",", $user_list), function($item) {
    return is_numeric($item);
  });

  if (count($uids)) {
    return user_load_multiple($uids);
  }

  return array();
};
;

// Theme functions:
//  - $region_control(string $id, bool $setting)           // Get a region element's attribute for use by the theme editor
//  - $region(string $id, string $dummy_text)              // Get a region's (markdown rendered) value
//  - $setting(string $setting_key, mixed $default_value)  // Get a setting's value
//  - $link(string $setting_key, mixed $default_value)     // Get a link/url, defaulting to `javascript:void 0` in a `canvas` state

// Get some optional settings
$stamp_setting = $setting('stamp', '');

// Render theme
if ($style_policy === 0 || $style_policy === 2) { ?>

  <section 
    <?= $setting('course_name') ? 'id="' . $setting('course_name') . '"' : '' ?>
    class="koa-content theme8-nav-blocks">
    <div class="constrain">
      <h2 class="title" <?= $region_control('title') ?>>
        <?= $region('title', 'Section Title') ?>
      </h2>
      <ul class="blocks">
        <li class="block">
          <a
            class="visual"
            style="background-image: url('<?= $file_base . '/' . $region_raw('block-1-image', 'css/graphics/arabic-book-covers-2.png') ?>');"
            <?= $region_control('block-1-image') ?>
            href="<?= url($region_raw('block-1-link', '/')) ?>">
            <div class="meta">
              <h3 class="title" <?= $region_control('block-1-title') ?>>
                <?= $region('block-1-title', 'Block Title') ?>
              </h3>
              <button class="block-link" <?= $region_control('block-1-link') ?>>
                <?= t('Learn more') ?>
              </button>
            </div>
          </a>
        </li>
        <li class="block">
          <a
            class="visual"
            style="background-image: url('<?= $file_base . '/' . $region_raw('block-2-image', 'css/graphics/arabic-book-covers-2.png') ?>');"
            <?= $region_control('block-2-image') ?>
            href="<?= url($region_raw('block-2-link', '/')) ?>">
            <div class="meta">
              <h3 class="title" <?= $region_control('block-2-title') ?>>
                <?= $region('block-2-title', 'Block Title') ?>
              </h3>
              <button class="block-link" <?= $region_control('block-2-link') ?>>
                <?= t('Learn more') ?>
              </button>
            </div>
          </a>
        </li>
        <li class="block">
          <a
            class="visual"
            style="background-image: url('<?= $file_base . '/' . $region_raw('block-3-image', 'css/graphics/arabic-book-covers-2.png') ?>');"
            <?= $region_control('block-3-image') ?>
            href="<?= url($region_raw('block-3-link', '/')) ?>">
            <div class="meta">
              <h3 class="title" <?= $region_control('block-3-title') ?>>
                <?= $region('block-3-title', 'Block Title') ?>
              </h3>
              <button class="block-link" <?= $region_control('block-3-link') ?>>
                <?= t('Learn more') ?>
              </button>
            </div>
          </a>
        </li>
        <li class="block">
          <a
            class="visual"
            style="background-image: url('<?= $file_base . '/' . $region_raw('block-4-image', 'css/graphics/arabic-book-covers-2.png') ?>');"
            <?= $region_control('block-4-image') ?>
            href="<?= url($region_raw('block-4-link', '/')) ?>">
            <div class="meta">
              <h3 class="title" <?= $region_control('block-4-title') ?>>
                <?= $region('block-4-title', 'Block Title') ?>
              </h3>
              <button class="block-link" <?= $region_control('block-4-link') ?>>
                <?= t('Learn more') ?>
              </button>
            </div>
          </a>
        </li>
      </ul>
    </div>
  </section>

<?php
}

if ($style_policy === 1 || $style_policy === 2) { ?>

  <style>
  .theme8-nav-blocks {
  display: block;
  background-color: #FFF;
}

.theme8-nav-blocks .constrain {
  display: flex;
  flex-flow: column nowrap;
  align-items: center;
  padding: 6rem 0 4rem;
  max-width: 1024px;
  margin: 0 auto;
}

.theme8-nav-blocks .constrain > .title {
  margin: 0;
  margin-bottom: 3rem;
  font-weight: 200;
  font-size: 3rem;
  font-family: "Savoye LET";
  color: #197098;
}

.theme8-nav-blocks .constrain > .title p {
  margin: 0;
}

.theme8-nav-blocks .constrain > .title::after {
  content: "";
  display: block;
  height: 4px;
  width: 3rem;
  margin-top: 1rem;
  margin-right: auto;
  margin-left: auto;
  background: #000;
}

.theme8-nav-blocks .blocks {
  display: flex;
  flex-flow: row wrap;
  padding: 0;
  width: 100%;
}

.theme8-nav-blocks .blocks .block {
  width: 50%;
  margin: 0;
  margin-bottom: 1rem;
  list-style: none;
}

.theme8-nav-blocks .blocks .block:nth-child(odd) {
  padding-right: 0.5rem;
}

.theme8-nav-blocks .blocks .block:nth-child(even) {
  padding-left: 0.5rem;
}

.theme8-nav-blocks .blocks .block .visual {
  display: block;
  position: relative;
  padding-bottom: 100%;
  overflow: hidden;
  background-color: #DDD;
  background-size: cover;
  background-position: center;
  text-decoration: none;
}

.theme8-nav-blocks .blocks .block .visual:hover .meta {
  transform: translate3d(0, -1.2rem, 0);
}

.theme8-nav-blocks .blocks .block .visual:hover .meta .block-link {
  background: #fff;
  color: #000;
}

.theme8-nav-blocks .blocks .block .visual .meta {
  position: absolute;
  display: flex;
  align-items: center;
  padding: 14rem 3rem;
  padding-bottom: 1.2rem;
  right: 0;
  left: 0;
  bottom: -1.2rem;
  transform: translate3d(0, 0, 0);
  transition: transform 250ms ease;
  background: linear-gradient(to top, rgba(0, 0, 0, 0.9) 0%, rgba(0, 0, 0, 0.9) 3.2rem, rgba(0, 0, 0, 0) 17.2rem);
  color: #fff;
}

.theme8-nav-blocks .blocks .block .visual .meta .title {
  font-size: 1.25rem;
}

.theme8-nav-blocks .blocks .block .visual .meta .block-link {
  padding: 0.5rem 0.75rem;
  margin-left: auto;
  box-sizing: border-box;
  flex: 0 0 auto;
  font-size: 1.25rem;
  border: 2px solid #fff;
  color: #fff;
  background: transparent;
  border-radius: 2px;
  transition: background 250ms ease, color 250ms ease;
}

  </style>

<?php
} ?>
