<?php

// Variables provided:
// $state = "thumb" | "canvas" | "public";
// $data = {region1: "Foo", region2: "Bar", region3: NULL, ...};
// $settings = {foo: "bar", ...};
// $style_policy = 0 | 1 | 2; // 0: No styles- only content, 1: Only styles- no content, 2: Both styles and content
// $theme_name = "...theme name...";
// $theme_meta = [ ... metadata ... ];
// $file_base = url("sites/default/files/ihfathusercontent");

// Include theme-required PHP:

// Variables provided:
// $state = "thumb" | "canvas" | "public";
// $data = {region1: "Foo", region2: "Bar", region3: NULL, ...};
// $settings = {foo: "bar", ...};
// $style_policy = 0 | 1 | 2; // 0: No styles- only content, 1: Only styles- no content, 2: Both styles and content
$regions = [];
$theme_name = basename(__FILE__, ".tpl.php");
$theme_meta = ihfathui_fp_course_theme_list()[$theme_name];
$file_base = url("sites/default/files/ihfathusercontent");

$region_visible = function($region_id) use ($data) {
  return isset($data[$region_id]) && (is_string($data[$region_id])
                                  ? trim($data[$region_id])
                                  : TRUE);
};

$region_control = function($id, $setting = FALSE) use ($state) {
  if ($state === "canvas") {
    return 'data-theme-editor-region' . ($setting ? '-setting' : '') . '="' . $id . '"';
  }

  return "";
};

$region = function($region_id, $dummy_text) use ($state, $data, $region_visible) {
  $print = $dummy_text;

  switch ($state) {
    case "thumb":
      // Thumbnail, just print
      return $print;
    case "canvas":
      if ($region_visible($region_id)) {
        $print = $data[$region_id];
      }

      return $print;
    case "public":
      if ($region_visible($region_id)) {
        return check_markup($data[$region_id], "ihfathui_markdown");
      }

      return "";
  }
};

$region_raw = function($region_id, $dummy_text) use ($state, $data, $region_visible) {
  $print = $dummy_text;

  switch ($state) {
    case "thumb":
      // Thumbnail, just print
      return $print;
    case "canvas":
      if ($region_visible($region_id)) {
        $print = $data[$region_id];
      }

      return $print;
    case "public":
      if ($region_visible($region_id)) {
        return $data[$region_id];
      }

      return "";
  }
};

$setting = function($setting, $default = "") use ($state, $settings, $data, $theme_meta) {
  if ($state === "public") {
    if (isset($settings->{$setting})) {
      return $settings->{$setting};
    }
  }

  return $default;
};

$link = function($sett, $default) use ($state, $setting, $settings) {
  if ($state === "canvas") {
    return "javascript:0[0]";
  } else {
    return $setting($sett, $default);
  }
};

$userlist = function($user_list) {
  $uids = array_filter(explode(",", $user_list), function($item) {
    return is_numeric($item);
  });

  if (count($uids)) {
    return user_load_multiple($uids);
  }

  return array();
};
;

// Theme functions:
//  - $region_control(string $id, bool $setting)           // Get a region element's attribute for use by the theme editor
//  - $region(string $id, string $dummy_text)              // Get a region's (markdown rendered) value
//  - $setting(string $setting_key, mixed $default_value)  // Get a setting's value
//  - $link(string $setting_key, mixed $default_value)     // Get a link/url, defaulting to `javascript:void 0` in a `canvas` state

// Get some optional settings
$stamp_setting = $setting('stamp', '');

// Render theme
if ($style_policy === 0 || $style_policy === 2) { ?>

  <section
    <?= $setting('course_name') ? 'id="' . $setting('course_name') . '"' : '' ?>
    class="koa-content theme2-block-overlap">
    <div class="constrain">
      <div class="back-pane">
        <div
          class="picture"
          style="background-image: url('<?= $file_base . '/' . $setting('picture', 'css/graphics/arabic-book-covers-2.png') ?>');">
        </div>

        <?php if ($stamp_setting) { ?>
          <div
            class="stamp"
            style="background-image: url('<?= $file_base . '/' . $stamp_setting ?>');">
          </div>
        <?php } ?>

        <div class="buttons">
          <a href="<?= $link("course_link", "#") ?>" class="button focused">
            <?= t("Learn more") ?>
          </a>
          <a href="<?= $link("course_register_link", "#") ?>" class="button">
            <?= t("Register") ?>
          </a>
        </div>

      </div>
      <div class="details-pane">
        <h2 class="title" <?= $region_control('title') ?>>
          <?= $region('title', 'Section Title') ?>
        </h2>
        <ul>
          <li>
            <div class="icon">
              <svg xmlns="http://www.w3.org/2000/svg" fill-rule="evenodd" clip-rule="evenodd" viewBox="0 0 24 24">
                <path d="M22 24h-17c-1.657 0-3-1.343-3-3v-18c0-1.657 1.343-3 3-3h17v24zm-2-4h-14.505c-1.375 0-1.375 2 0 2h14.505v-2zm-3-15h-10v3h10v-3z"/>
              </svg>
            </div>
            <h3 class="title" <?= $region_control('point-1-title') ?>>
              <?= $region('point-1-title', 'Point 1') ?>
            </h3>
            <div class="info" <?= $region_control('point-1') ?>>
              <?= $region('point-1', '
              Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
              eiusmod tempor incididunt ut labore et dolore magna aliqua.
              ') ?>
            </div>
          </li>
          <li>
            <div class="icon">
              <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
                <path d="M7 24h-6v-6h6v6zm8-9h-6v9h6v-9zm8-4h-6v13h6v-13zm0-11l-6 1.221 1.716 1.708-6.85 6.733-3.001-3.002-7.841 7.797 1.41 1.418 6.427-6.39 2.991 2.993 8.28-8.137 1.667 1.66 1.201-6.001z"/>
              </svg>
            </div>
            <h3 class="title" <?= $region_control('point-2-title') ?>>
              <?= $region('point-2-title', 'Point 2') ?>
            </h3>
            <div class="info" <?= $region_control('point-2') ?>>
              <?= $region('point-2', '
              Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
              eiusmod tempor incididunt ut labore et dolore magna aliqua.
              ') ?>
            </div>
          </li>
          <li>
            <div class="icon">
              <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
                <path d="M12 0c-6.627 0-12 5.373-12 12s5.373 12 12 12 12-5.373 12-12-5.373-12-12-12zm6 14h-7v-8h2v6h5v2z"/>
              </svg>
            </div>
            <h3 class="title" <?= $region_control('point-3-title') ?>>
              <?= $region('point-3-title', 'Point 3') ?>
            </h3>
            <div class="info" <?= $region_control('point-3') ?>>
              <?= $region('point-3', '
              Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
              eiusmod tempor incididunt ut labore et dolore magna aliqua.
              ') ?>
            </div>
          </li>
        </ul>
      </div>
    </div>
  </section>

<?php
}

if ($style_policy === 1 || $style_policy === 2) { ?>

  <style>
  .theme2-block-overlap {
  display: block;
  background-color: #F2F2F2;
}

.theme2-block-overlap .constrain {
  display: flex;
  flex-flow: row nowrap;
  padding: 6rem 0;
  max-width: 1024px;
  margin: 0 auto;
}

.theme2-block-overlap .back-pane {
  width: 50%;
}

.theme2-block-overlap .back-pane .picture {
  margin-right: -8rem;
  padding-bottom: 85%;
  border-bottom: 15px solid #197098;
  background-repeat: no-repeat;
  background-size: cover;
  background-position: center;
}

.theme2-block-overlap .back-pane .stamp {
  margin-top: 3rem;
  margin-right: 2rem;
  padding-bottom: 50%;
  background-repeat: no-repeat;
  background-size: contain;
  background-position: center;
}

.theme2-block-overlap .back-pane .buttons {
  display: flex;
  justify-content: flex-start;
  margin-top: 4rem;
  padding-right: 4rem;
}

.theme2-block-overlap .back-pane .buttons .button {
  display: block;
  padding: 0.6rem 0;
  width: 8.5rem;
  margin-right: 2rem;
  font-size: 1rem;
  color: #197098;
  border-radius: 0;
  border: 3px solid #166E97;
  background: #fff;
  text-decoration: none;
}

.theme2-block-overlap .back-pane .buttons .button.focused {
  color: #fff;
  background: #166E97;
}

.theme2-block-overlap .details-pane {
  width: 50%;
  margin-top: 5rem;
  padding: 2rem;
  background: #fff;
}

.theme2-block-overlap .details-pane > .title {
  font-weight: 200;
  font-size: 3rem;
  font-family: "Savoye LET";
  color: #197098;
  margin: 0;
  margin-bottom: 2rem;
}

.theme2-block-overlap .details-pane > .title p {
  margin: 0;
}

.theme2-block-overlap .details-pane > .title::after {
  content: "";
  display: block;
  height: 4px;
  width: 3rem;
  margin-top: 1.2rem;
  background: #000;
}

.theme2-block-overlap .details-pane ul {
  padding: 0;
  list-style: none;
}

.theme2-block-overlap .details-pane ul li {
  margin-bottom: 2rem;
  padding-top: 1.2rem;
  padding-left: 4.4rem;
  position: relative;
}

.theme2-block-overlap .details-pane ul li .icon {
  position: absolute;
  left: 0;
  top: 0;
  width: 3.4rem;
  height: 3.4rem;
  box-sizing: border-box;
  padding: 10px;
  border-radius: 100%;
  border: 3px solid #197098;
}

.theme2-block-overlap .details-pane ul li .icon path {
  fill: #197098;
}

.theme2-block-overlap .details-pane ul li .title {
  margin-top: 0;
  color: #1A2F4B;
  line-height: 1rem;
}

.theme2-block-overlap .details-pane ul li .title p {
  margin: 0;
}

.theme2-block-overlap .details-pane ul li .info {
  color: #444;
  font-size: 1.2rem;
  line-height: 1.9rem;
}

  </style>

<?php
} ?>
