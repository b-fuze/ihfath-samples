<?php

// Variables provided:
// $state = "thumb" | "canvas" | "public";
// $data = {region1: "Foo", region2: "Bar", region3: NULL, ...};
// $settings = {foo: "bar", ...};
// $style_policy = 0 | 1 | 2; // 0: No styles- only content, 1: Only styles- no content, 2: Both styles and content
// $theme_name = "...theme name...";
// $theme_meta = [ ... metadata ... ];
// $file_base = url("sites/default/files/ihfathusercontent");

// Include theme-required PHP:
$THEME_HEAD;

// Theme functions:
//  - $region_control(string $id, bool $setting)           // Get a region element's attribute for use by the theme editor
//  - $region(string $id, string $dummy_text)              // Get a region's (markdown rendered) value
//  - $setting(string $setting_key, mixed $default_value)  // Get a setting's value
//  - $link(string $setting_key, mixed $default_value)     // Get a link/url, defaulting to `javascript:void 0` in a `canvas` state

// Get some optional settings
$stamp_setting = $setting('stamp', '');

// Render theme
if ($style_policy === 0 || $style_policy === 2) { ?>

  <section
    <?= $setting('course_name') ? 'id="' . $setting('course_name') . '"' : '' ?>
    class="koa-content theme2-block-overlap">
    <div class="constrain">
      <div class="back-pane">
        <div
          class="picture"
          style="background-image: url('<?= $file_base . '/' . $setting('picture', 'css/graphics/arabic-book-covers-2.png') ?>');">
        </div>

        <?php if ($stamp_setting) { ?>
          <div
            class="stamp"
            style="background-image: url('<?= $file_base . '/' . $stamp_setting ?>');">
          </div>
        <?php } ?>

        <div class="buttons">
          <a href="<?= $link("course_link", "#") ?>" class="button focused">
            <?= t("Learn more") ?>
          </a>
          <a href="<?= $link("course_register_link", "#") ?>" class="button">
            <?= t("Register") ?>
          </a>
        </div>

      </div>
      <div class="details-pane">
        <h2 class="title" <?= $region_control('title') ?>>
          <?= $region('title', 'Section Title') ?>
        </h2>
        <ul>
          <li>
            <div class="icon">
              <svg xmlns="http://www.w3.org/2000/svg" fill-rule="evenodd" clip-rule="evenodd" viewBox="0 0 24 24">
                <path d="M22 24h-17c-1.657 0-3-1.343-3-3v-18c0-1.657 1.343-3 3-3h17v24zm-2-4h-14.505c-1.375 0-1.375 2 0 2h14.505v-2zm-3-15h-10v3h10v-3z"/>
              </svg>
            </div>
            <h3 class="title" <?= $region_control('point-1-title') ?>>
              <?= $region('point-1-title', 'Point 1') ?>
            </h3>
            <div class="info" <?= $region_control('point-1') ?>>
              <?= $region('point-1', '
              Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
              eiusmod tempor incididunt ut labore et dolore magna aliqua.
              ') ?>
            </div>
          </li>
          <li>
            <div class="icon">
              <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
                <path d="M7 24h-6v-6h6v6zm8-9h-6v9h6v-9zm8-4h-6v13h6v-13zm0-11l-6 1.221 1.716 1.708-6.85 6.733-3.001-3.002-7.841 7.797 1.41 1.418 6.427-6.39 2.991 2.993 8.28-8.137 1.667 1.66 1.201-6.001z"/>
              </svg>
            </div>
            <h3 class="title" <?= $region_control('point-2-title') ?>>
              <?= $region('point-2-title', 'Point 2') ?>
            </h3>
            <div class="info" <?= $region_control('point-2') ?>>
              <?= $region('point-2', '
              Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
              eiusmod tempor incididunt ut labore et dolore magna aliqua.
              ') ?>
            </div>
          </li>
          <li>
            <div class="icon">
              <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
                <path d="M12 0c-6.627 0-12 5.373-12 12s5.373 12 12 12 12-5.373 12-12-5.373-12-12-12zm6 14h-7v-8h2v6h5v2z"/>
              </svg>
            </div>
            <h3 class="title" <?= $region_control('point-3-title') ?>>
              <?= $region('point-3-title', 'Point 3') ?>
            </h3>
            <div class="info" <?= $region_control('point-3') ?>>
              <?= $region('point-3', '
              Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
              eiusmod tempor incididunt ut labore et dolore magna aliqua.
              ') ?>
            </div>
          </li>
        </ul>
      </div>
    </div>
  </section>

<?php
}

if ($style_policy === 1 || $style_policy === 2) { ?>

  <style>
  THEME_CSS{}
  </style>

<?php
} ?>
