<?php

// Variables provided:
// $state = "thumb" | "canvas" | "public";
// $data = {region1: "Foo", region2: "Bar", region3: NULL, ...};
// $settings = {foo: "bar", ...};
// $style_policy = 0 | 1 | 2; // 0: No styles- only content, 1: Only styles- no content, 2: Both styles and content
// $theme_name = "...theme name...";
// $theme_meta = [ ... metadata ... ];
// $file_base = url("sites/default/files/ihfathusercontent");

// Include theme-required PHP:
$THEME_HEAD;

// Theme functions:
//  - $region_control(string $id, bool $setting)           // Get a region element's attribute for use by the theme editor
//  - $region(string $id, string $dummy_text)              // Get a region's (markdown rendered) value
//  - $setting(string $setting_key, mixed $default_value)  // Get a setting's value
//  - $link(string $setting_key, mixed $default_value)     // Get a link/url, defaulting to `javascript:void 0` in a `canvas` state

if ($style_policy === 0 || $style_policy === 2) { ?>

  <section
    class="koa-content theme3-adjacent-frames"
    <?= $setting('course_name') ? 'id="' . $setting('course_name') . '"' : '' ?>
    style="background-image: url('<?= $file_base . '/' . $setting('background', 'css/graphics/arabic-book-covers-2.png') ?>">
    <div class="constrain">
      <div class="details">
        <h2 class="title" <?= $region_control('title') ?>>
          <?= $region('title', 'Title') ?>
        </h2>
        <div class="text" <?= $region_control('text') ?>>
          <div class="quotation-mark">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
              <path d="M13 14.725c0-5.141 3.892-10.519 10-11.725l.984 2.126c-2.215.835-4.163 3.742-4.38 5.746 2.491.392 4.396 2.547 4.396 5.149 0 3.182-2.584 4.979-5.199 4.979-3.015 0-5.801-2.305-5.801-6.275zm-13 0c0-5.141 3.892-10.519 10-11.725l.984 2.126c-2.215.835-4.163 3.742-4.38 5.746 2.491.392 4.396 2.547 4.396 5.149 0 3.182-2.584 4.979-5.199 4.979-3.015 0-5.801-2.305-5.801-6.275z"/>
            </svg>
          </div>

          <div class="text-body">
            <?= $region('text', '
            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
            eiusmod tempor incididunt ut labore et dolore magna aliqua.
            ') ?>
          </div>

          <div class="buttons">
            <a href="<?= $link("course_link", "#") ?>" class="button focused">
              <?= t("Learn more") ?>
            </a>
            <a href="<?= $link("course_register_link", "#") ?>" class="button">
              <?= t("Register") ?>
            </a>
          </div>
        </div>
      </div>
      <div
        class="picture"
        style="background-image: url('<?= $file_base . '/' . $setting('picture', 'css/graphics/arabic-book-covers-2.png') ?>');">
      </div>
    </div>
  </section>

<?php
}

if ($style_policy === 1 || $style_policy === 2) { ?>

  <style>
  THEME_CSS{}
  </style>

<?php
} ?>
