<?php
global $user;
global $base_url;
global $language;
$user_obj = user_load($user->uid);
$lang = $language->language;

// Get field value
$fv = function($field, $markup = TRUE, $column = "value") use($lang) {
  $use_lang = $lang;

  // global
  if (!isset($field[$lang])) {
    $use_lang = LANGUAGE_NONE;
  }

  if (!isset($field[$use_lang])) {
    return "";
  }

  $value = $field[$use_lang][0][$column];
  if ($markup) {
    return check_markup($value, "ihfathui_markdown");
  } else {
    return $value;
  }
};

$uid = $user->uid;
$user_full_name = $fv($user_obj->field_ihfath_firstname, FALSE) . " " . $fv($user_obj->field_ihfath_lastname, FALSE);

?>
<div class="ihfath-student-header-links">
  <div class="ihfath-ldc-header-clock"></div>
  <div class="ihfath-user-welcome">
    <span>

      <?php
      // "Hello user"
      echo str_replace(array(
        "[hilight%]",
        "[%hilight]"
        ), array(
          '<b>',
          '</b>'
        ),
        t("Hello [hilight%]@user[%hilight]", array(
          "@user" => $user_full_name,
        ))
      ); ?>

    </span>
  </div>
  <a href="<?php echo "{$base_url}/user/{$uid}/orders"; ?>" class="main-bg btn-grey">
    <span><?php echo t("My orders"); ?></span> <i class="fa fa-usd"></i>
  </a>
  <a href="<?php echo "{$base_url}/user/{$uid}/edit"; ?>" class="main-bg btn-grey">
    <span><?php echo t("My account"); ?></span> <i class="fa fa-user"></i>
  </a>
  <a href="<?php echo "{$base_url}/user/logout"; ?>" class="main-bg btn-grey">
    <span><?php echo t("Logout"); ?> <i class="fa fa-sign-out"></i></span>
  </a>
</div>

<style media="screen">
  .ihfath-student-header-links {
    display: flex;
    flex-flow: row nowrap;
    align-items: center;
    font-size: 0px;
  }

  .ihfath-user-welcome {
    display: inline-block;
    padding: 8px 15px;
    vertical-align: middle;

    /*font-style: italic;*/
    font-size: 12px;
    line-height: 18px;
    border-right: 1px solid rgba(0, 0, 0, 0.05);
  }

  .ihfath-user-welcome span {
    opacity: 0.65;
  }

  .ihfath-student-header-links a {
    display: inline-block;
    box-sizing: content-box;
    vertical-align: middle;
    padding: 8px 15px;
    margin-left: 15px;

    transform: translateX(-0.5);
    font-size: 15px;
    font-weight: bold;
    border-radius: 3px;
  }

  .ihfath-student-header-links a:not(:hover) {
    text-shadow: 1px 1px 1px rgba(0,0,0,.0);
    background: #F2F2F2;
    color: #666;
  }

  .ihfath-student-header-links a:hover {
    background: #a9bf04;
    color: #fff;
  }

  .responsive-nav.f-right {
    float: none;
    text-align: right;
  }
</style>
