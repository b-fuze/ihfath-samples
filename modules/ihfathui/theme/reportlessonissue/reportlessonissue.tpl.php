<?php

$lesson      = $form["#ihfath"]["lesson"];
$student     = $form["#ihfath"]["student"];
$professor   = $form["#ihfath"]["professor"];
$report_form = $form["#report_form"];

// Get user role
$user_role = "";

foreach ($user->roles as $role) {
  switch ($role) {
    case "administrator":
    case "professor":
    case "student":
      $user_role = $role;
  }

  if ($user_role) {
    break;
  }
}

global $user;
global $base_url;
?>

<div class="section">
  <div class="container ihfath-wrap">
    <div class="row">
      <div class="heading side-head">
        <div class="head-6">
          <h4 class="" style="float: right;"><?php echo '' . t(date("l", $lesson->start_date)) . ' <span class="main-color">' . date("d", $lesson->start_date) . '</span>'; ?> / <span class="main-color"><?php echo date("G", $lesson->start_date); ?></span>:<span class="main-color"><?php echo date("i", $lesson->start_date); ?></span></h4>
          <h4 class="uppercase">
            <?php echo str_replace(array(
              "[hilight%]",
              "[%hilight]"
            ), array(
              '<span class="main-color">',
              '</span>'
            ), t("Report Lesson [hilight%]Issues[%hilight]")); ?>
          </h4>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-md-6" style="padding-left: 0px;">
        <?php echo drupal_render($report_form); ?>
      </div>

      <div class="col-md-6" style="padding-right: 0px;">
        <div class="heading side-head">
          <div class="head-8 ihfath-close-heading">
            <h5 class="uppercase"><?php echo t("Professor name"); ?></h5>
          </div>
        </div>
        <div class="ihfath-close-heading-content">
          <h2 class="main-color"><?php echo $professor->name; ?></h2>
        </div>

        <div class="heading side-head">
          <div class="head-8 ihfath-close-heading">
            <h5 class="uppercase"><?php echo t("Student name"); ?></h5>
          </div>
        </div>
        <div class="ihfath-close-heading-content">
          <h2 class="main-color"><?php echo $student->name; ?></h2>
        </div>

        <div class="heading side-head">
          <div class="head-8 ihfath-close-heading">
            <h5 class="uppercase"><?php echo t("Lesson program"); ?></h5>
          </div>
        </div>
        <div class="ihfath-close-heading-content">
          <h2><span class="main-color"><?php echo $lesson->type_data->data["current"]->name; ?></span> <span><?php echo $lesson->program_data->data["current"]->name; ?></span></h2>
        </div>

        <div class="heading side-head">
          <div class="head-8 ihfath-close-heading">
            <h5 class="uppercase"><?php echo t("Lesson duration"); ?></h5>
          </div>
        </div>
        <div class="ihfath-close-heading-content">
          <h2><span class="main-color"><?php echo $lesson->paid_duration / 60; ?></span> <?php echo t("hours"); ?></h2>
        </div>

        <div class="heading side-head">
          <div class="head-8 ihfath-close-heading">
            <h5 class="uppercase"><?php echo t("Lesson link"); ?></h5>
          </div>
        </div>
        <div class="ihfath-close-heading-content">
          <?php /* TODO: Take this lesson link creation and encapsulate in a function */ ?>
          <h2>
            <span class="main-color uppercase">
              <a target="_blank" href="<?php echo url("koa/lesson/" . $lesson->lid); ?>">
                <?php echo t("View lesson"); ?> <i class="fa fa-angle-double-right"></i>
              </a>
            </span>
          </h2>
        </div>
      </div>
    </div>
  </div>
</div>

<style media="screen">
  div.form-item {
    padding-bottom: 20px;
  }

  div.form-item label {
    font-size: 15px;
  }

  .ihfath-report-text {
    border-bottom-left-radius: 0px !important;
  }

  /* Headings */
  div.head-8.ihfath-close-heading {
    margin-bottom: 0px;
    margin-top: 0px;
  }

  div.head-8.ihfath-close-heading {
    padding-left: 60px;
  }

  div.head-8.ihfath-close-heading > * {
    font-size: 13px;
    color: #888;
    margin-bottom: 0px;
  }

  div.ihfath-close-heading-content {
    margin-left: 60px;
    padding-bottom: 30px;
    margin-bottom: 40px;

    border-bottom: 1px solid #ddd;
  }

  div.ihfath-close-heading-content:last-child {
    border-width: 0px;
  }

  div.ihfath-close-heading-content > * {
    margin-bottom: 0px;
    font-size: 22px;
    font-weight: bold;
  }

  div.head-8.ihfath-close-heading::before {
    content: unset;
  }
</style>
