import Base, { Store, Role } from "./base"

export type SearchQuery = {
  fname: string,
  lname: string,
  email: string,
  phone: string,
  role: Role,
  studentsWithoutLessons: boolean,
  teachersWithoutStudents: boolean,

  page: number,
};

export type UserMap = {
  [user: number]: User;
};

export type SearchResults = {
  list: number[];
  users: UserMap;
  pager: Pager;
  courses: CourseMap;
};

export type Store = Store<{}, {
  search: SearchQuery;
  searchresults: SearchResults;
  baseUrl: string;
}>;

export interface Pager {
  totalItems: number;
  total: number;
  pageArray: number;
  limit: number;

  last: number;
  startSep: boolean;
  endSep: boolean;
  intermediatePages: number[];
}

export type CourseMap = {
  [course: number]: Course;
};

export interface Course {
  id: number;
  machineName: string;
  name: string;
  description: string;
}

export const enum UserRole {
  Professor = "2",
  Student = "3",
}

export interface User {
  id: number;
  name: string;
  firstname: string;
  lastname: string;
  email: string;
  address: string;
  phone: string;
  role: UserRole;
  created: number;

  professors?: number[];
  students?: number[];
  courses?: number[];
}

// Side effects to keep in Rollup
export const _ = () => {
  document.createComment("");
};
