import Base, { Role } from "../../base";
import { Store, User, Pager, Course, CourseMap } from "../../defaults"

type UserRow = User & {
  showDetails?: boolean;
};

type UserMap = {
  [user: number]: UserRow;
};

declare class App extends Base<{
  fname: string,
  lname: string,
  email: string,
  phone: string,
  role: Role,
  studentsWithoutLessons: boolean,
  teachersWithoutStudents: boolean,
  users: UserMap,
  list: number[],
  userList: UserRow[],
  reloadOnUpdate: boolean,
  columns: number,
  courses: CourseMap;

  pager: Pager,
  page: number,
}, {
  formUpdate: boolean;
}, Store> {
  search(keepPage?: boolean): void;
  showDetails(index: number): void;
  navigatePage(page: number): void;
}

export default App;

export function oncreate(this: App) {
  // Reload on form update
  let timeout: NodeJS.Timeout = null;

  this.on("formUpdate", (delay = true) => {
    const { reloadOnUpdate } = this.get();

    if (reloadOnUpdate) {
      clearTimeout(timeout);
      timeout = setTimeout(() => {
        this.search();
      }, delay ? 500 : 0);
    }
  });

  // Display search results
  this.store.on("searchresults", (data) => {
    const { page } = this.get();
    const pager = pagerLinks(page, data.pager);

    this.set({
      list: data.list,
      courses: data.courses,
      users: data.users,
      pager: pager,
      page: Math.min(page, pager.total)
    });
  });
}

function pagerLinks(page: number, pager: Pager) {
  // Change that to alter the max page numbers displayed (including start/end)
  // Must be an odd number, will otherwise be coerced into one
  let maxLinks = 7;

  // ...
  maxLinks = maxLinks % 2 === 0 ? maxLinks + 1 : maxLinks;
  const maxIntermediate = maxLinks - 2;
  const { total } = pager;
  // const last = Math.max(total - 1, 1); // FIXME: Something weird happening here
  const last = total;
  pager.last = last;

  // No intermediate pages to show
  if (last < 3) {
    pager.intermediatePages = [];
    pager.startSep = false;
    pager.endSep = false;
    return pager;
  }

  const half = (maxIntermediate - 1) / 2;

  const endOverflow = (last - page);
  const intermediateStart = Math.max(page - (half + Math.max((half + 1) - endOverflow, 0)), 2);
  const intermediateEnd = Math.min(intermediateStart + maxIntermediate, last);
  const intermediatePages = new Array(intermediateEnd - intermediateStart)
    .fill(0)
    .map((_, i) => intermediateStart + i);

  if (intermediateStart - 1 > 1) {
    pager.startSep = true;
  }

  if (intermediateEnd < last) {
    pager.endSep = true;
  }

  pager.intermediatePages = intermediatePages;
  return pager;
}

export const methods = {
  reset(this: App) {
    this.set({
      fname: "",
      lname: "",
      email: "",
      phone: "",
      role: Role.Both,
      studentsWithoutLessons: false,
    });

    this.search();
  },

  search(this: App, keepPage: boolean = false) {
    const {
      fname,
      lname,
      email,
      phone,
      role,
      studentsWithoutLessons,
      teachersWithoutStudents,

      page,
    } = this.get();

    if (!keepPage) {
      this.set({
        page: 1,
      });
    }

    this.store.fire("search", {
      fname,
      lname,
      email,
      phone,
      role,
      studentsWithoutLessons,
      teachersWithoutStudents,

      page: keepPage ? page - 1 : 0,
    });
  },

  showDetails(this: App, uid: number) {
    const { users } = this.get();
    const user = users[uid];

    if (user) {
      user.showDetails = !user.showDetails;
      this.set({
        users,
      });
    }
  },

  navigatePage(this: App, page: number) {
    this.set({
      page,
    });

    this.search(true);
  },

  relativePage(this: App, dir: -1 | 1) {
    const { page, pager } = this.get();
    const newPage = Math.min(Math.max(page + dir, 1), pager.last);

    if (newPage !== page) {
      this.navigatePage(newPage);
    }
  }
};
