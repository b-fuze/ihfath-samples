<?php
global $base_url;
global $user;

$ihfath = $form["#ihfath"];

$lesson = $ihfath["current_lesson"];
$lesson_is_current = $ihfath["is_current_lesson"];
$lesson_id = $lesson ? $lesson->lid : 0;
$lsn_c_day_caption = $ihfath["day_caption"];

$lessons_remaining = $ihfath["lessons_remaining"];

// Utility funcs
function jst(...$args) {
  return json_encode(t(...$args));
}

function jse(...$args) {
  return json_encode(...$args);
}
?>
<div class="section ihfath-current-lesson">
  <div class="container ihfath-wrap">
    <?php
    // FIXME: Link needs work to maintain consistency with professor dashboard links
    if ($lesson) { ?>

      <h4 class="ihfath-cl-lesson-title"><?= $lesson_is_current ? t("Ongoing lesson...") : t("Upcoming lesson...") ?></h4>

      <div class="ihfath-cl-row">
        <a href="<?= "{$base_url}/koa/lesson/{$lesson_id}" ?>" class="ihfath-cl-lesson main-bg">
          <h3 class="ihfath-cl-lesson-name">
            <span>
              <?php

              // "Type Program Lesson"
              echo strtoupper(str_replace(array(
                  "[hilight%]",
                  "[%hilight]"
                ), array(
                  '<b>',
                  '</b>'
                ),
                t("[hilight%]@type @program[%hilight] Lesson", array(
                  "@type"    => $lesson->type_name,
                  "@program" => $lesson->program_name
                ))
              ));

              ?>
            </span>
          </h3>
          <h3 class="ihfath-cl-lesson-details">
            <span>
              <?php

              echo str_replace(array(
                "[hilight%]",
                "[%hilight]",
                "[emph%]",
                "[%emph]"
                ), array(
                  '<b>',
                  '</b>',
                  '<span class="ihfath-cl-prof-highlight">',
                  '</span>'
                ),
                $lsn_c_day_caption
              )

              ?>
            </span>
          </h3>

          <div class="ihfath-cl-go">
            <div class="fa fa-angle-right"></div>
          </div>
        </a>

        <?php
        // =============================================
        //              POSTPONING BUTTON
        // =============================================

        // Don't show postponing button if it's than 24 hours till the lesson
        $time_till_lesson = ($lesson->start_date - time()) / 60 / 60;
        if ($time_till_lesson > 24) {
        ?>

          <a href="javascript:0[0]" class="ihfath-cl-switch-lesson main-bg">
            <i class="fa fa-history"></i>
            <span><?= t('Postpone lesson') ?></span>
          </a>

          <script type="text/javascript">
            window.addEventListener("DOMContentLoaded", function() {
              const message = document.querySelector(".ihfath-confirm-postpone-lesson");
              const show = document.querySelector(".ihfath-cl-switch-lesson");
              const confirm = document.querySelector(".ihfath-postpone-confirm-button");
              const cancel = document.querySelector(".ihfath-postpone-cancel-button");
              const apiUrl = <?= jse(url("ihfath/ajax/student/appendLesson")) ?>;
              const lid = <?= jse((int) $lesson->lid) ?>;

              show.addEventListener("click", function() {
                message.classList.add("ihfath-visible");
              });

              confirm.addEventListener("click", function() {
                fetch(apiUrl, {
                  method: "POST",
                  body: JSON.stringify({
                    lid,
                  }),
                }).then(res => {
                  if (res.ok) {
                    res.text().then(res => {
                      const data = JSON.parse(res);

                      if (data.error) {
                        console.error("Failed to postpone lesson", data.error);
                      } else {
                        // Success
                        document.location = ".";
                      }
                    });
                  }
                });
              });

              cancel.addEventListener("click", function() {
                message.classList.remove("ihfath-visible");
              });
            });
          </script>

        <?php
        }
        ?>

      </div>

      <?php
      if ($time_till_lesson > 24) {
      ?>

        <div class="ihfath-confirm-postpone-lesson">
          <div class="ihfath-wrap">
            <h3 class="ihfath-title">
              <?= t("Postpone this lesson?") ?>
            </h3>
            <div class="ihfath-buttons">
              <a href="#" class="ihfath-postpone-confirm-button cws-button btn btn-md btn-icon-right btn-round main-bg with-icon">
                <span><?= t("Yes, postpone this lesson") ?></span>
                <i class="fa fa-check"></i>
              </a>
              <a href="#" class="ihfath-postpone-cancel-button cws-button btn btn-md btn-icon-right btn-round btn-juicy_pink with-icon">
                <span><?= t("Cancel") ?></span>
                <i class="fa fa-times"></i>
              </a>
            </div>
          </div>
        </div>

      <?php
      }
      ?>
  <?php
  } else { ?>

    <h4 class="ihfath-center ihfath-no-lesson"><?php echo t("No upcoming lessons") ?></h4>

  <?php
  } ?>
  </div>
</div>
<!-- FIXME: IMPLEMENT NEWS SECTION -->
<div class="section ihfath-news ihfath-hidden">
  <div class="container ihfath-wrap">
    News...
  </div>
</div>

<div class="section ihfath-student-status">
  <div class="container ihfath-wrap">
    <h2>
      <?php
      if ($lessons_remaining) {
        $remaining_phrases = array();
        $lsn_rm_count = count($lessons_remaining);

        for ($i=0; $i<$lsn_rm_count; $i++) {
          $lsn_rm = $lessons_remaining[$i];

          $remaining = $lsn_rm->remaining;
          $hours = floor($remaining / 60);
          $minutes = $remaining % 60;

          $remaining_phrases[] = t((
            $i + 1 === $lsn_rm_count
              ? "and "
              : ""
          ) . "[%]@hours[/%] hours and [%]@minutes[/%] minutes left for @course", array(
            "@hours" => $hours,
            "@minutes" => $minutes,
            "@course" => $lsn_rm->course->data["current"]->name,
          ));
        }

        echo preg_replace(array(
          '/\[%\]/',
          '/\[\/%\]/',
        ), array(
          '<b>',
          '</b>',
        ), t("You have @remaining", array(
          "@remaining" => implode(", ", $remaining_phrases),
        )));
      } else {
        echo t("No time left on your package");
      }
      ?>
    </h2>
  </div>
</div>

<div class="section ihfath-big-btn-section">
  <div class="container ihfath-wrap">
    <div class="row ihfath-student-big-btn-wrap">
      <!-- Get more lessons! -->
      <div class="col-md-6">
        <a href="<?php echo $base_url; ?>/get-lessons">
          <button class="ihfath-student-big-btn btn-grey">
            <h1 class="ihfath-left" style="line-height: 1.3em; font-family: Roboto; font-weight: 500;">
              <?php

              echo str_replace(array(
                "[emph%]",
                "[%emph]"
                ), array(
                  '<span class="ihfath-highlight">',
                  '</span>'
                ),
                t("[emph%]Get more lessons![%emph]")
              ) ?>
            </h1>
          </button>
        </a>
      </div>

      <div class="col-md-6">
        <a href="<?php echo $base_url; ?>/ihfath/lesson/archive">
          <button class="ihfath-student-big-btn btn-grey">
            <h1>
              <span class="ihfath-highlight" style="font-weight: 300 !important; font-family: Roboto;">
                <?php echo t("Tutorials") ?>
              </span>
            </h1>
          </button>
        </a>
      </div>
    </div>
  </div>
</div>

<style media="screen">
  .ihfath-center {
    text-align: center;
  }

  .ihfath-float-right {
    float: right;
  }

  .ihfath-inline-block {
    display: inline-block;
    line-height: 1.5;
  }

  .ihfath-valign-middle {
    vertical-align: middle;
  }

  .ihfath-hidden {
    display: none !important;
  }

  .ihfath-current-lesson {
    background: #eee;
    padding-top: 25px;
    padding-bottom: 25px;
  }

  .ihfath-current-lesson h4 {
    font-weight: normal;
    margin-bottom: 15px;
  }

  .ihfath-cl-lesson-title {
    color: #999;
  }

  .ihfath-cl-row {
    position: relative;
    display: flex;
    flex-flow: row nowrap;
  }

  .ihfath-cl-lesson {
    position: relative;
    flex: 1 1 auto;
    display: block;
    overflow: hidden;
    padding: 20px 20px;
  }

  .ihfath-cl-lesson,
  .ihfath-cl-switch-lesson {
    border-radius: 4px;
    box-shadow: 0px 3px 3px rgba(0, 0, 0, 0.25);
    text-shadow: 1px 1px 1px rgba(0,0,0,.15);
  }

  .ihfath-cl-lesson h3 {
    color: inherit;
    font-weight: normal;
    margin: 0px;
  }

  .ihfath-cl-switch-lesson {
    display: flex;
    justify-content: center;
    flex-flow: column nowrap;
    flex: 0 0 auto;
    align-items: center;
    padding: 2rem;
    width: 136px;
    height: 136px;
    box-sizing: border-box;
    margin-left: 2rem;

    text-align: center;
    font-size: 1.25rem;
  }

  .ihfath-cl-switch-lesson .fa {
    transform: scale(-1, 1);
    font-size: 5rem;
    margin-bottom: 1rem;
    margin-top: 1rem;
  }

  h3.ihfath-cl-lesson-name {
    margin-bottom: 5px;
  }

  .ihfath-cl-lesson h3 span {
    font-size: 1em;
  }

  h3.ihfath-cl-lesson-name span {
    font-size: 1.55em;
  }

  .ihfath-cl-lesson-details {
    line-height: 1.5em;
  }

  .ihfath-cl-prof-highlight {
    display: inline;
    border-radius: 2px;
    padding: 3px 5px;

    background: rgba(255, 255, 255, 0.1);
    /* background: rgba(0, 0, 0, 0.1); */
  }

  .ihfath-cl-go {
    position: absolute;
    right: 0px;
    top: 0px;
    bottom: 0px;
    width: 300px;
  }

  .ihfath-cl-go .fa {
    position: absolute;
    display: block;
    top: 0px;
    right: 0px;
    bottom: 0px;
    left: 0px;
    margin: auto;
    height: 100px;
    width: 100px;

    line-height: 100px;
    text-align: center;
    font-size: 60px;
    color: #fff;
    opacity: 0.75;
  }

  .ihfath-cl-go::before {
    content: "";
    position: absolute;
    display: block;
    right: -50px;
    top: -100px;
    bottom: -100px;
    width: 350px;

    background: #fff;
    opacity: 0.1;
    transform: rotate(15deg);
    transform-origin: right center;
  }

  /* Postpone lesson button */
  .ihfath-confirm-postpone-lesson {
    display: none;
    align-items: center;
    justify-content: center;
    height: 160px;
  }

  .ihfath-confirm-postpone-lesson.ihfath-visible {
    display: flex;
  }

  .ihfath-confirm-postpone-lesson .ihfath-wrap > * {
    margin-top: 0;
    margin-bottom: 0;
  }

  .ihfath-confirm-postpone-lesson .ihfath-wrap {
    display: flex;
    flex-flow: column nowrap;
  }

  .ihfath-confirm-postpone-lesson .ihfath-wrap .ihfath-title {
    margin-bottom: 1rem;
  }

  .ihfath-confirm-postpone-lesson .ihfath-buttons {
    display: flex;
  }

  .ihfath-confirm-postpone-lesson .cws-button:first-child {
    margin-left: 0;
  }

  .ihfath-confirm-postpone-lesson .cws-button:not(:first-child) {
    margin-left: 2rem;
  }

  .ihfath-confirm-postpone-lesson .cws-button .fa {
    display: flex;
    align-items: center;
    justify-content: center;
  }

  /* No lesson message */
  .ihfath-no-lesson {
    margin-bottom: 0px !important;
    color: #999;
  }

  /* Student remaining status */
  div.section.ihfath-student-status {
    padding-top: 50px;
    padding-bottom: 0;
  }

  div.section.ihfath-student-status h2 {
    margin-bottom: 0;
    padding-left: 0.75em;
    max-width: 600px;

    color: #757575;
    font-size: 2.5rem;
    border-left: solid 5px #a9bf04;
  }

  /* Student big buttons */
  div.section.ihfath-big-btn-section {
    padding-top: 50px;
  }

  .ihfath-student-big-btn-wrap .col-md-6 {
    position: relative;
  }

  .ihfath-student-big-btn {
    display: block;
    width: 100%;
    height: 250px;
    box-sizing: border-box;
    padding: 0px 50px;

    border: 0px;
    border-radius: 3px;
    box-shadow: 0px 2px 4px rgba(0, 0, 0, 0.2);
  }

  .ihfath-student-big-btn:hover {
    background: #ebebeb !important;
    color: #a9bf04;
  }

  .ihfath-student-big-btn * {
    margin: 0px;
    padding: 0px 0px 0px 0px;

    font-size: 4em;
    color: inherit;
    text-shadow: none;
  }

  .ihfath-student-big-btn .ihfath-highlight {
    font-size: 1.3em;
  }

  .ihfath-left {
    text-align: left;
  }
</style>
