<?php
global $user;
global $base_url;

$cur_path = current_path();
$role = $form["#role"];

if ($cur_path === "index.html") {
  $cur_path = "";
} else {
  $cur_path = "/" . $cur_path;
}

$page_url = $base_url . $cur_path;

$ihfath = $form["#ihfath"];
$lessons = $ihfath["lessons"];
?>
<!-- Professor/Student Nav -->
<?php
$navblock = NULL;
$title    = drupal_set_title();

switch ($role) {
  case 1:
    $navblock = module_invoke("mod_background_page", "block_view", "mod_block_background");
    break;
  case 2:
    $navblock = module_invoke("ihfathui", "block_view", "ihfathblk_prof_nav");
    break;
  case 3:
    $navblock = module_invoke("ihfathui", "block_view", "ihfathblk_studentnav");
    break;
}

if (isset($navblock)) {
  echo render($navblock);
}

// Preserve title
drupal_set_title(t("Lesson List"));
?>

<?php
if ($role === 1): ?>

  <div class="section">
    <div class="container ihfath-wrap">
      Admin has no archive
    </div>
  </div>

<?php
else: ?>

<!-- Archive page -->
<div class="section ihfath-small-section">
  <div class="container">
    <div class="head-7 main-border ihfath-archive-header">
      <h4 class="uppercase bold" style="text-align: left;">
        <span class="main-bg "><i class="fa fa-list"></i> <?php echo t("Lesson List"); ?></span>
      </h4>

      <a class="ihfath-archive-switch main-bg" href="<?php echo "{$base_url}/koa/lesson/archive" ?>">
        <i class="fa fa-calendar"></i>
        <?php echo t("Calendar") ?>
      </a>
    </div>

    <?php
    foreach ($lessons as $lesson) {
      $lesson_id = $lesson->lid;

      // Generate today msg
      $prof = user_load($lesson->puid);
      $lsn_day = (int) date("z", $lesson->start_date);
      $today = (int) date("z");
      $caption_prefix = "";

      $duration = (int) $lesson->prof_dur;
      $hours = floor($duration / 60);
      $minutes = $duration % 60;

      $suffix = "at [hilight%]@time[%hilight], for [hilight%]@hours[%hilight] hours and [hilight%]@minutes[%hilight] minutes with [emph%]@prof[%emph]";

      if ($lsn_day === $today) {
        $day_caption_text = "Today, " . $suffix;
      } else if ($lsn_day === $today + 1) {
        $day_caption_text = "Tomorrow, " . $suffix;
      } else {
        $caption_prefix = t(date("l", $lesson->start_date)) . " " . t(date("M", $lesson->start_date));
        $day_caption_text = " @date, " . $suffix;
      }

      $day_caption = t($caption_prefix . $day_caption_text, array(
        "@time" => date("H:i", $lesson->start_date),
        "@prof" => $prof->name,
        "@date" => date("j Y", $lesson->start_date),
        "@hours" => $hours,
        "@minutes" => $minutes,
      )); ?>

      <div class="ihfath-li-lesson">
        <h3 class="ihfath-li-lesson-name">
          <span>
            <?php

            // "Type Program Lesson"
            echo str_replace(array(
                "[hilight%]",
                "[%hilight]",
                "[fade%]",
                "[%fade]",
              ), array(
                '<b>',
                '</b>',
                '<span class="ihfath-fade">',
                '</span>',
              ),
              t("[hilight%]@program[%hilight] [fade%]#@lid[%fade]", array(
                "@program" => $lesson->program_name,
                "@lid" => $lesson->lid,
              ))
            );

            ?>
          </span>
        </h3>
        <h3 class="ihfath-li-lesson-details">
          <span>
            <?php

            echo str_replace(array(
              "[hilight%]",
              "[%hilight]",
              "[emph%]",
              "[%emph]"
              ), array(
                '<b>',
                '</b>',
                '<span class="ihfath-li-prof-highlight">',
                '</span>'
              ),
              $day_caption
            )

            ?>
          </span>
        </h3>
      </div>

    <?php
    } ?>

    <!-- Pager -->
    <div class="ihfath-archive-pager">
      <?php echo theme("pager"); ?>
    </div>

    <!-- <pre>
      <?php var_dump($lessons); ?>
    </pre> -->
  </div>
</div>

<style media="screen">
  .ihfath-archive-header {
    position: relative;
  }

  .ihfath-archive-switch {
    position: absolute;
    display: inline-block;
    right: 0px;
    top: 5px;
    padding: 5px 10px;

    border-radius: 3px;
  }
</style>

<?php
endif; ?>
