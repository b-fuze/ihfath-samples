<div class="course-wrap">
  <section class="courses">
    <a href="<?= url('/') . '#arabe' ?>" class="course">
      <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
        <path d="M5.495 4c-1.375 0-1.375-2 0-2h16.505v-2h-17c-1.657 0-3 1.343-3 3v18c0 1.657 1.343 3 3 3h17v-20h-16.505z"/>
      </svg>
      <h2 class="title">Langue Arabe</h2>
    </a>
    <a href="<?= url('/') . '#ecriture' ?>" class="course">
      <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
        <path d="M9.963 8.261c-.566-.585-.536-1.503.047-2.07l5.948-5.768c.291-.281.664-.423 1.035-.423.376 0 .75.146 1.035.44l-8.065 7.821zm-9.778 14.696c-.123.118-.185.277-.185.436 0 .333.271.607.607.607.152 0 .305-.057.423-.171l.999-.972-.845-.872-.999.972zm8.44-11.234l-3.419 3.314c-1.837 1.781-2.774 3.507-3.64 5.916l1.509 1.559c2.434-.79 4.187-1.673 6.024-3.455l3.418-3.315-3.892-4.019zm9.97-10.212l-8.806 8.54 4.436 4.579 8.806-8.538c.645-.626.969-1.458.969-2.291 0-2.784-3.373-4.261-5.405-2.29z"/>
      </svg>
      <h2 class="title">Écriture</h2>
    </a>
    <a href="<?= url('/') . '#conversation' ?>" class="course">
      <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
        <path d="M12 1c-6.627 0-12 4.364-12 9.749 0 3.131 1.817 5.917 4.64 7.7.868 2.167-1.083 4.008-3.142 4.503 2.271.195 6.311-.121 9.374-2.498 7.095.538 13.128-3.997 13.128-9.705 0-5.385-5.373-9.749-12-9.749z"/>
      </svg>
      <h2 class="title">Conversation</h2>
    </a>
    <a href="<?= url('/') . '#coran' ?>" class="course">
      <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
        <path d="M23 5v13.883l-1 .117v-16c-3.895.119-7.505.762-10.002 2.316-2.496-1.554-6.102-2.197-9.998-2.316v16l-1-.117v-13.883h-1v15h9.057c1.479 0 1.641 1 2.941 1 1.304 0 1.461-1 2.942-1h9.06v-15h-1zm-12 13.645c-1.946-.772-4.137-1.269-7-1.484v-12.051c2.352.197 4.996.675 7 1.922v11.613zm9-1.484c-2.863.215-5.054.712-7 1.484v-11.613c2.004-1.247 4.648-1.725 7-1.922v12.051z"/>
      </svg>
      <h2 class="title">Coran</h2>
    </a>
  </section>
</div>
<script>
  window.addEventListener("load", function() {
    const courses = document.querySelector("section.courses");
    const coursesWrap = courses.parentNode;
    let coursesWrapRect = coursesWrap.getBoundingClientRect();
    const courseRect = courses.getBoundingClientRect();
    const navHeight = (document.body.classList.contains("admin-menu") ? 29 : 0) + 60;
    let top = coursesWrapRect.bottom + scrollY - navHeight;

    let floating = false;
    coursesWrap.style.height = (courseRect.bottom - courseRect.top) + "px";

    window.addEventListener("resize", function() {
      coursesWrapRect = coursesWrap.getBoundingClientRect();
      top = coursesWrapRect.top + scrollY - navHeight;
    });

    function floatSummary() {
      let newFloating = false;

      if (scrollY > top) {
        newFloating = true;
      }

      if (newFloating !== floating) {
        floating = newFloating;

        if (floating) {
          document.body.classList.add("floating-course-summary");
        } else {
          document.body.classList.remove("floating-course-summary");
        }
      }
    }

    window.addEventListener("scroll", floatSummary);

    // Check initially
    requestAnimationFrame(floatSummary);
  });
</script>
