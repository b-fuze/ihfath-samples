<?php

/**
 * Ihfath Custom Status Messages implementation
 */

$shape_map = array(
  "status"  => "fa-check-circle",
  "error"   => "fa-times-circle",
  "warning" => "fa-exclamation-circle"
);

 ?>
<div class="ihfath-custom-status-message-wrap">
  <?php
  foreach ($data as $type => $msgs) {
    foreach ($msgs as $msg) { ?>
      
      <div class="ihfath-csm-msg ihfath-csm-<?php echo $type; ?>">
        <i class="fa <?php echo isset($shape_map[$type]) ? $shape_map[$type] : "fa-cog"; ?>"></i><span class="ihfath-csm-content"><?php echo $msg; ?></span>
      </div>
      
  <?php
    }
  } ?>
</div>

<style media="screen">
  .ihfath-custom-status-message-wrap {
    font-size: 1px;
  }
  
  .ihfath-csm-msg {
    position: relative;
    padding: 15px;
    margin-bottom: 10px;
    color: #fff;
    
    border-radius: 3px;
    box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.15);
    text-shadow: 0px 1px 1px rgba(0, 0, 0, 0.25);
    background: #2a86db;
    
    font-size: 14.5em;
    font-weight: bold;
  }
  
  .ihfath-csm-msg a {
    color: #05B8EF;
  }
  
  .ihfath-csm-msg i,
  .ihfath-csm-content {
    display: inline-block;
    vertical-align: middle;
  }
  
  .ihfath-csm-msg i {
    position: absolute;
    margin: 14px 10px 0px 1px;
    top: 0px;
    font-size: 22px;
    height: 22px;
  }
  
  .ihfath-csm-content {
    margin-left: 33px;
  }
  
  .ihfath-csm-msg.ihfath-csm-status a {
    color: #61E419;
  }
  
  .ihfath-csm-msg.ihfath-csm-status {
    background: #5f9434;
  }
  
  .ihfath-csm-msg.ihfath-csm-error {
    background: #C72924;
  }
  
  .ihfath-csm-msg.ihfath-csm-error a {
    color: #FF7D79;
  }
  
  .ihfath-csm-msg.ihfath-csm-warning {
    background: #f46e33;
  }
  
  .ihfath-csm-msg.ihfath-csm-warning a {
    color: #FF8000;
  }
</style>
