// rollup.config.js
import typescript from "rollup-plugin-typescript";
import resolve from "rollup-plugin-node-resolve";
 
export default {
  entry: "./adminlessons.ts",
  output: {
    sourcemap: true,
    format: "iife",
    file: "../adminlessons.js",
    globals: {
      jshorts: "jSh",
      cellidi: "Cellidi",
    },
  },
  external: [
    "jshorts",
    "cellidi",
  ],
  plugins: [
    resolve(),
    typescript({

    }),
  ]
}

