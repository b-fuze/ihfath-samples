import {ConfigSchema} from "./misc/config";
import {construct} from "./misc/construction";
import {setApiUrl} from "./misc/req";
import {updateLocale} from "./locale";
import {state} from "./state";
import jSh from "jshorts";
import {stub} from "az-cancel-promise";

function InitLessonAdmin(config: ConfigSchema) {
  setApiUrl(config.baseUrl, config.apiUrl);
  updateLocale(config.locale);

  // Prevent form from submitting itself
  config.dom.form.addEventListener("submit", e => {
    e.preventDefault();
    e.stopPropagation();
  });

  construct(config);

  // Load initial user if available
  if (config.userId) {
    state.setState("user", config.userId);
  }

  // Use stub
  console.log(stub);
}

// Export to window
(window as any).InitLessonAdmin = InitLessonAdmin;
