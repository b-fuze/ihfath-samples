import Cellidi from "cellidi";
import {Component} from "lces";
import {LessonSummaryManager} from "./lessonsummary";
import jSh, {JshDiv} from "jshorts";
import {User, Lesson, UserRoles} from "../model";
import {state, CalendarUnavailableMessage} from "../state";
import {locale} from "../locale";
import {CalendarSideBar} from "./calendar-sidebar";
import {notifis} from "./notifi";

let lessonSummaryMgr: LessonSummaryManager;

// Lesson colors, gen script below
//    partitions = 24
//    console.log(
//      new Array(partitions).fill(1).map((a, i) =>
//        `hsl(${ ((360 - (360 / (partitions))) / (partitions - 1) * i) }deg, 65%, 35%)`
//      )
//    )
//
//
// Colors may be rearranged

const colors = [
  'hsl(0deg, 65%, 35%)',
  'hsl(15deg, 65%, 35%)',
  'hsl(30deg, 65%, 35%)',
  'hsl(45deg, 65%, 35%)',
  'hsl(60deg, 65%, 35%)',
  'hsl(75deg, 65%, 35%)',
  'hsl(90deg, 65%, 35%)',
  'hsl(105deg, 65%, 35%)',
  'hsl(120deg, 65%, 35%)',
  'hsl(135deg, 65%, 35%)',
  'hsl(150deg, 65%, 35%)',
  'hsl(165deg, 65%, 35%)',
  'hsl(180deg, 65%, 35%)',
  'hsl(195deg, 65%, 35%)',
  'hsl(210deg, 65%, 35%)',
  'hsl(225deg, 65%, 35%)',
  'hsl(240deg, 65%, 35%)',
  'hsl(255deg, 65%, 35%)',
  'hsl(270deg, 65%, 35%)',
  'hsl(285deg, 65%, 35%)',
  'hsl(300deg, 65%, 35%)',
  'hsl(315deg, 65%, 35%)',
  'hsl(330deg, 65%, 35%)',
  'hsl(345deg, 65%, 35%)',
];

export interface MonthCell {
  outMonth: boolean;
  curDay: boolean;
  date: number;
  lessons: Lesson[];
}

export interface CalendarStateModel {
  focused: boolean;
  choosingCell: boolean;
}

export
class MonthCalendar extends Component<CalendarStateModel> {
  table: Cellidi;
  domTableRoot: JshDiv;

  constructor(
    public domRoot: JshDiv,
    data: MonthCell[][],
  ) {
    super();

    this.domTableRoot = domRoot.jSh(".ihfath-schedule-root")[0];

    // Construct monthly calendar
    const table = this.table = new Cellidi<MonthCell>({
      element: this.domTableRoot,
      dimensions: [7, 6],
      history: true,
      headers: {
        top(x: number) {
          const d = jSh.d(null, locale.days[x]);

          return d;
        },
      },
      newCell(x, y, virtualX, virtualY, data, cellDisplay) {
        const date = jSh.d(".ihfath-cell-date");
        const lessonWrap = jSh.d(".ihfath-cell-lessons", null, [
          jSh.d(".ihfath-cell-lessons-col"),
        ]);
        const cell = jSh.d({
          child: [
            date,
            lessonWrap,
          ]
        });

        return {
          dom: cell,
          data: {
            date,
            lessonWrap,
          }
        };
      },
      diffStates(a, b) {
        // Compare basic properies
        if (a.outMonth !== b.outMonth
            || a.curDay !== b.curDay
            || a.date !== b.date
            || a.lessons.length !== b.lessons.length) {
          // Differing properties found
          return true;
        }

        const aLessons = a.lessons;
        const bLessons = b.lessons;

        // Compare lessons
        for (let i=0; i<aLessons.length; i++) {
          if (aLessons[i].lid !== bLessons[i].lid) {
            // Differing lessons found
            return true;
          }
        }

        // Both cell states are the same
        return false;
      },
      emptyState(renew, old) {
        if (renew && old && typeof old === "object") {
          const newCell: MonthCell = {
            outMonth: old.outMonth,
            curDay: old.curDay,
            date: old.date,
            lessons: old.lessons.slice(),
          };

          return newCell;
        } else {
          const newCell: MonthCell = {
            outMonth: false,
            curDay: false,
            date: 0,
            lessons: [],
          };

          return newCell;
        }
      },
      onCellStateChange(cellState, cellModels) {
        const userData = state.getState("userData");
        const usersInvolved = state.getState("usersInvolved");

        cellState.forEach((cellState, index) => {
          const model = cellModels[index];

          if (cellState.curDay) {
            model.dom.classList.add("ihfath-cell-current-day");
          } else {
            model.dom.classList.remove("ihfath-cell-current-day");
          }

          if (cellState.outMonth) {
            model.dom.classList.add("ihfath-cell-out-month");
          } else {
            model.dom.classList.remove("ihfath-cell-out-month");
          }

          const date: JshDiv = model.data.date;
          const lessonWrap: JshDiv = model.data.lessonWrap;

          date.textContent = cellState.date.toString();

          // Wipe any previous lessons
          lessonWrap.innerHTML = "";

          let lessonCount = 0;
          let lessonCol: JshDiv = null;

          // Add new lessons to cell
          for (const lesson of cellState.lessons) {
            if (lessonCount % 4 === 0) {
              lessonCol = jSh.d(".ihfath-cell-lessons-col");
              lessonWrap.appendChild(lessonCol);
            }

            const prof = usersInvolved[lesson.puid];
            const profTag: Node = userData.role === UserRoles.Student
                                  ? jSh.c("span", ".ihfath-prof", (prof.name.match(/\d+/) || ["0"])[0])
                                  : jSh.t("");

            const lessonTag = jSh.d({
              sel: ".ihfath-cell-lesson",
              attr: {
                style: `
                  background: ${ colors[(lesson.startDate / 60 / 60) % colors.length] };
                `,
              },
              child: [
                jSh.c("span", null, lesson.programName),
                profTag,
              ]
            });

            (<any>lessonTag).IHFATH_LESSON = lesson;
            lessonCol.appendChild(lessonTag, jSh.d(".ihfath-cell-break"));
            lessonCount++;
          }
        });
      },
    });

    table.on("activeinput", cell => {
      // Open week view when you click an inMonth cell
      const cellState = table.getCellState(cell.cell);

      if (!cellState.outMonth) {
        const weekDays: number[] = [];

        for (let i=0; i<7; i++) {
          const cellWeekStartModel = table.getCellModelFromCoords(i, cell.cell.coords[1]);
          weekDays.push(table.getCellState(cellWeekStartModel).date);
        }

        // Push the first day of the week to global state
        state.setState("firstWeekDay", weekDays);

        // Request week data
        state.setState("monthDay", cellState.date);
        state.triggerEvent("reqWeekData", true);
      }
    });

    // Lesson summary infotip
    let cancels = 0;
    this.domTableRoot.addEventListener("mousemove", e => {
      const cb = (v?: any) => {
        cancels = 0;

        let target: HTMLElement = <any>e.target;
        let lessonTag: HTMLElement & {
          IHFATH_LESSON: Lesson;
        } = null;

        while (target !== this.domTableRoot) {
          if ((<any>target).IHFATH_LESSON) {
            lessonTag = <any>target;
            break;
          }

          target = <any>target.parentNode;
        }

        const lessonNotifiDelay = 10000;

        if (lessonTag) {
          const lessonSummary = notifis.lessonSummary;
          if (lessonTag.IHFATH_LESSON.lid === lessonSummary.getState("lessonId")) {
            lessonSummary.open(lessonNotifiDelay);
            return;
          }

          const lesson = lessonTag.IHFATH_LESSON;

          const usersInvolved = state.getState("usersInvolved");
          const profUser = usersInvolved[lesson.puid];
          const studUser = usersInvolved[lesson.suid];

          lessonSummary.setState("anchor", lessonTag);
          lessonSummary.setState("professor", profUser.name);
          lessonSummary.setState("student", studUser.name);
          lessonSummary.setState("lessonId", lesson.lid);
          lessonSummary.setState("paidDuration", (lesson.paidDuration / 60) + " " + locale.sidebar.hours);
          lessonSummary.setState("orderId", lesson.orderId);

          // Calculate timezone offsets
          const tzOffset = (new Date().getTimezoneOffset()) * 60;
          const profStart = new Date((lesson.startDate + (profUser.timezoneOffset * 60 * 60) + tzOffset) * 1000);
          const studStart = new Date((lesson.startDate + (studUser.timezoneOffset * 60 * 60) + tzOffset) * 1000);
          const adminStart = new Date(lesson.startDate * 1000);

          // Set start times
          lessonSummary.setState("profStartTime", `${ profStart.toLocaleTimeString() } — ${ profStart.toLocaleDateString() }`);
          lessonSummary.setState("studStartTime", `${ studStart.toLocaleTimeString() } — ${ studStart.toLocaleDateString() }`);
          lessonSummary.setState("adminStartTime", `${ adminStart.toLocaleTimeString() } — ${ adminStart.toLocaleDateString() }`);

          lessonSummary.open(lessonNotifiDelay);
        } else {
          notifis.lessonSummary.close();
        }
      };

      if (cancels > 10) {
        cb();
      } else {
        cancels++;
        this.timeout("hoverLessonSummary", 100).then(cb);
      }
    });

    // States
    this.newState("focused", null);
    this.onState("focused", focused => {
      if (focused) {
        this.domRoot.classList.add("ihfath-focused");
      } else {
        this.domRoot.classList.remove("ihfath-focused");
      }
    });
  }

  setMonthData(data: MonthCell[][]) {
    this.table.setState("state", data);
  }
}

export interface WeekCell {
  curDay: boolean;
  weekDay: number;
  hour: number;
  lesson: Lesson;
  span: number;
  selectable: boolean;
}

export
class WeekCalendar extends Component<CalendarStateModel> {
  table: Cellidi;
  domTableRoot: JshDiv;
  domBackToMonth: JshDiv;

  constructor(
    public domRoot: JshDiv,
    public manager: CalendarManager,
  ) {
    super();

    this.domTableRoot = domRoot.jSh(".ihfath-schedule-root")[0];
    this.domBackToMonth = domRoot.jSh(".ihfath-back-to-month")[0];

    // Construct weekly calendar

    const table = this.table = new Cellidi<WeekCell>({
      element: this.domTableRoot,
      dimensions: [7, 24],
      history: true,
      headers: {
        top(x) {
          // Draw week header cell
          const cell = jSh.d<Node>({
            child: [
              jSh.t(locale.days[x] + " "),
              jSh.c("b", null, (state.getState("firstWeekDay")[x]) + ""),
            ]
          });

          return cell;
        },
        left(time) {
          const cell = jSh.d({
            sel: ".ihfath-time",
            text: time + ":00",
          });

          return cell;
        },
      },
      newCell(x, y, virtualX, virtualY, data, cellDisplay) {
        let children: Node[] = [];
        let sel = y % 2 ? "" : ".ihfath-checker";

        if (data && data.lesson) {
          const usersInvolved = state.getState("usersInvolved");
          const userData = state.getState("userData");
          const opposingParty = userData.role === UserRoles.Professor ? usersInvolved[data.lesson.suid].name : usersInvolved[data.lesson.puid].name;
          sel += ".ihfath-lesson.ihfath-dur-" + data.lesson.paidDuration;

          children.push(
            jSh.d(".ihfath-cell-wrap", null, jSh.d(".ihfath-cell-text", null, [
              jSh.c("b", null, data.lesson.programName + " "),
              jSh.c("span", null, opposingParty + " "),
              jSh.c("span", ".ihfath-dur", (data.lesson.paidDuration / 60) + "h"),
            ])),
          );
        } else {
          sel += ".ihfath-vacant-cell";
          children.push(jSh.t(
            virtualY + ":00"
          ));
          children.push(jSh.d(".ihfath-cell-move-selectable", null, [
            jSh.c("i", ".fa.fa-arrows"),
          ]));
        }

        const cell = jSh.d({
          sel,
          child: children,
          prop: {
            title: "",
          },
          events: {
            mousedown(e) {
              e.preventDefault();
            }
          }
        });
        return {
          dom: cell,
          spanY: data && data.span || 1,
        };
      },
      onCellStateChange(newState, models) {
        // Show arrows when choosing a cell
        for (let i=0; i<newState.length; i++) {
          const cellState = newState[i];
          const model = models[i];

          if (cellState.selectable) {
            model.dom.classList.add("ihfath-cell-show-selectable");
          } else {
            model.dom.classList.remove("ihfath-cell-show-selectable");
          }
        }
      },
      diffStates(a, b) {
        if (a.curDay !== b.curDay
            || a.weekDay !== b.weekDay
            || a.hour !== b.hour
            || (a.lesson && a.lesson.lid) !== (b.lesson && b.lesson.lid)
            || a.span !== b.span
            || a.selectable !== b.selectable) {
          return true;
        }

        // Both cellstates are the same
        return false;
      },
      emptyState(renew, old) {
        if (renew) {
          const newCell: WeekCell = {
            curDay: old.curDay,
            weekDay: old.weekDay,
            hour: old.hour,
            lesson: old.lesson,
            span: old.span,
            selectable: old.selectable,
          };

          return newCell;
        } else {
          const newCell: WeekCell = {
            curDay: false,
            weekDay: 0,
            hour: 0,
            lesson: null,
            span: 1,
            selectable: false,
          };

          return newCell;
        }
      },
    });

    // States
    this.newState("choosingCell", false);
    this.newState("focused", null);
    this.onState("focused", focused => {
      if (focused) {
        this.domRoot.classList.add("ihfath-focused");
      } else {
        this.domRoot.classList.remove("ihfath-focused");
      }
    });

    // Events

    // Go back to month calendar when back to calender is clicked
    this.domBackToMonth.addEventListener("click", click => {
      // TODO: Check if should link these two states
      state.setState("sideBarVisible", false);
      this.manager.setState("focused", FocusedCalendar.Month);
    });

    // Go back to month calendar when user is disabled
    state.onState("userActive", active => {
      if (!active) {
        state.setState("sideBarVisible", false);
        this.manager.setState("focused", FocusedCalendar.Month);
      }
    });

    table.on("activeinput", cellEvt => {
      const cell = cellEvt.cell;
      const cellState = table.getCellState(cell);

      if (cellState.lesson) {
        state.setState("focusedLesson", cellState.lesson);
        state.setState("sideBarVisible", true);
      } else {
        if (this.getState("choosingCell") && cellState.selectable) {
          const lesson = state.getState("focusedLesson");
          const coords = cell.coords;
          const weektime = (coords[0] + 1) * 1000 + coords[1] * 10;
          state.triggerEvent("moveLessonConfirm", {
            lesson,
            weektime,
          });
          this.setState("choosingCell", false);
        }
      }
    });

    // Start choosing the cell when move event si triggered
    state.on("moveLesson", lesson => {
      this.setState("choosingCell", !this.getState("choosingCell"));
    });

    // Toggle selectability of vacant cells
    this.onState("choosingCell", choosing => {
      const lesson = state.getState("focusedLesson");
      const twoCell = lesson.paidDuration > 60;

      // Find all selectable cells
      const oldTable = table.getState("state");
      const newTable = table.mapEachCell(oldTable, (cell, x, y) => {
        const newCell = {
          ...cell,
        };

        if (choosing) {
          // Display the selectable cell
          if (!cell.lesson) {
            let selectable = true;
            if (twoCell) {
              // Lesson is longer than an hour, make sure the lower cell is vacant
              selectable = !!(oldTable[x][y + 1] && !oldTable[x][y + 1].lesson);
            }

            newCell.selectable = selectable;
          }
        } else {
          newCell.selectable = false;
        }

        return newCell;
      });

      // Apply the selection
      table.setState("state", newTable);
    });
  }

  setWeekData(data: WeekCell[][]) {
    // TODO: Try to make this use .setState("state") instead of .rebuild()
    this.table.rebuild(data);
    // this.table.setState("state", data);
  }
}

export const enum FocusedCalendar {
  Month,
  Week,
}

interface CalendarManagerStateModel {
  focused: FocusedCalendar;
  year: number;
  month: number;
  week: number;
}

export
class CalendarManager extends Component<CalendarManagerStateModel> {
  month: MonthCalendar;
  week: WeekCalendar;
  sidebar: CalendarSideBar;

  private messages: {
    [msg: number]: Node[];
  };

  constructor(
    public domWrapper: JshDiv,
    public domMonth: JshDiv,
    public domWeek: JshDiv,
    public domControls: JshDiv,
    public domUnavailable: JshDiv,
    public domSideBar: JshDiv,
  ) {
    super();

    // Create calendar controllers
    const month = this.month = new MonthCalendar(domMonth, null);
    const week = this.week = new WeekCalendar(domWeek, this);

    // Create sidebar
    const sidebar = this.sidebar = new CalendarSideBar(domSideBar);

    // Toggle sidebar visibility when the state changes
    state.onState("sideBarVisible", visible => {
      if (visible) {
        this.domWrapper.classList.add("ihfath-show-sidebar");
        this.domSideBar.classList.remove("ihfath-hidden");
      } else {
        this.domWrapper.classList.remove("ihfath-show-sidebar");
        this.domSideBar.classList.add("ihfath-hidden");
      }
    });

    // Reference controls
    const monthInput = domControls.jSh<HTMLSelectElement>(".ihfath-month-select")[0];
    const yearInput = domControls.jSh<HTMLInputElement>(".ihfath-year-input")[0];
    const nextMonth = domControls.jSh<HTMLButtonElement>(".ihfath-go-next-month")[0];
    const prevMonth = domControls.jSh<HTMLButtonElement>(".ihfath-go-prev-month")[0];

    // Create lesson summary manager
    lessonSummaryMgr = new LessonSummaryManager();

    // Update appstate
    state.setState("year", parseInt(yearInput.value, 10));
    state.setState("month", parseInt(monthInput.value, 10));

    // Sync appstate with inputs
    monthInput.addEventListener("change", function() {
      const newValue = jSh.numOp<number>(parseInt(this.value, 10), null);

      if (newValue === null) {
        this.value = state.getState("month") + "";
      } else {
        state.setState("month", newValue);
      }
    });

    yearInput.addEventListener("change", function() {
      const newValue = jSh.numOp<number>(parseInt(this.value, 10), null);

      if (newValue === null) {
        this.value = state.getState("year") + "";
      } else {
        state.setState("year", newValue);
      }
    });

    const shiftMonth = (dir: -1 | 1) => {
      const month = state.getState("month");
      const year = state.getState("year");
      let newMonth = month + dir;
      let newYear = year;

      if (newMonth > 12) {
        newYear++;
        newMonth = 1;
      } else if (newMonth < 1) {
        newYear--;
        newMonth = 12;
      }

      state.setState("month", newMonth);
      state.setState("year", newYear);
    }

    nextMonth.addEventListener("click", function() {
      shiftMonth(1);
    });

    prevMonth.addEventListener("click", function() {
      shiftMonth(-1);
    });

    state.onState("month", month => {
      if (month + "" !== monthInput.value) {
        monthInput.value = month + "";
      }
    });

    state.onState("year", year => {
      if (year + "" !== yearInput.value) {
        yearInput.value = year + "";
      }
    });

    // Disable inputs when user is deactivated
    state.onState("userActive", userActive => {
      if (userActive) {
        monthInput.disabled = false;
        yearInput.disabled = false;
      } else {
        monthInput.disabled = true;
        yearInput.disabled = true;
      }
    });

    // Create states
    this.newState("focused", null);
    this.newState("month", 0);
    this.newState("year", 2010);

    this.onState("focused", focused => {
      const focusedMonth = focused === FocusedCalendar.Month;

      month.setState("focused", focusedMonth);
      week.setState("focused", !focusedMonth);
    });

    // Hide calendar when it's not available
    state.onState("calendarAvailable", available => {
      if (available) {
        this.domUnavailable.classList.add("ihfath-hidden");
      } else {
        this.domUnavailable.classList.remove("ihfath-hidden");
      }
    });

    // Initial setup
    this.setState("focused", FocusedCalendar.Month);
    state.setState("calendarAvailable", false);

    this.messages = {};
    this.setupUnvailableMessage();
    this.setCalendarUnavailableMsg(CalendarUnavailableMessage.UserNotLoaded);
  }

  setMonthData(data: MonthCell[][]) {
    this.month.setMonthData(data);
  }

  setWeekData(data: WeekCell[][]) {
    this.week.setWeekData(data);
  }

  setCalendarUnavailableMsg(aspect: CalendarUnavailableMessage) {
    const msgDiv = this.domUnavailable.jSh(0).jSh(0);
    msgDiv.innerHTML = "";

    for (const child of this.messages[aspect]) {
      msgDiv.appendChild(child);
    }
  }

  private setupUnvailableMessage() {
    const that = this;
    const messages: {
      [msg: number]: string;
    } = {
      [CalendarUnavailableMessage.UserNotLoaded]: locale.findAUser,
      [CalendarUnavailableMessage.UserNoCalendar]: locale.userNoCalendar,
      [CalendarUnavailableMessage.ErrorLoadingCalendar]: locale.errorGettingCalendar,
    };

    for (const key of Object.keys(messages)) {
      const string = messages[+key];
      const index: CalendarUnavailableMessage = +key;
      const elements: Node[] = [];

      switch (index) {
        case CalendarUnavailableMessage.UserNotLoaded: {
          const match = string.match(/^([^]*)\[click\]([^]+)\[\/click\]([^]*)$/);
          elements.push(
            jSh.t(match[1]),
            jSh.c<HTMLAnchorElement>("a", {
              text: match[2],
              prop: {
                href: "javascript:0[0]",
              },
              events: {
                click() {
                  window.scrollTo(0, 0);
                  state.focusUserField();
                },
              },
            }),
            jSh.t(match[3]),
          );
          break;
        }

        case CalendarUnavailableMessage.ErrorLoadingCalendar: {
          const match = string.split("@user");
          const name = jSh.c<HTMLSpanElement>("b", null, "");

          elements.push(
            jSh.t(match[0]),
            name,
            jSh.t(match[1]),
          );

          state.onState("userData", (data: User) => {
            if (data) {
              name.textContent = data.realName || data.name;
            }
          });
          break;
        }

        case CalendarUnavailableMessage.UserNoCalendar: {
          const match = string.split("@user");
          const name = jSh.c<HTMLSpanElement>("b", null, "");

          elements.push(
            jSh.t(match[0]),
            name,
            jSh.t(match[1]),
          );

          state.onState("userData", (data: User) => {
            if (data) {
              name.textContent = data.realName || data.name;
            }
          });
          break;
        }
      }

      this.messages[index] = elements;
    }
  }
}
