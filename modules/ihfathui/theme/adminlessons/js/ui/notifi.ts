import {Lesson} from "../model";
import {state} from "../state";
import {Notifi, NotifiUI, NotifiType} from "@azuga/notifi";
import {locale} from "../locale";
import jSh from "jshorts";

const sideBarWidth = 320;
const sideBarWidthPadding = sideBarWidth - 20;
const baseMargin = 10;

interface DelLesson {
  lesson: Lesson,
}

interface ConfirmModel {
  action: string;
  thing: string;
  callback: () => void;
}

interface CalendarMsgModel {
  msg: string;
}

interface LessonSummaryModel {
  professor: string;
  student: string;
  lessonId: number;
  profStartTime: string;
  studStartTime: string;
  adminStartTime: string;
  paidDuration: string;
  orderId: number;
}

export const notifis: {
  delLesson?: Notifi<DelLesson>,
  confirm?: Notifi<ConfirmModel>,
  calendarMsg?: Notifi<CalendarMsgModel>,
  lessonSummary?: Notifi<LessonSummaryModel>,
} = {};

export function setupNotifis() {
  // Confirm modal
  const confirm = notifis.confirm = NotifiUI.new<ConfirmModel>(NotifiUI.template<ConfirmModel>({
    type: NotifiType.Modal,
    children: [{
      type: NotifiType.VBox,
      children: [{
        type: NotifiType.Text,
        text: "Are you sure you want to {{ action }} {{ thing }}?",
      }, {
        type: NotifiType.HBox,
        children: [{
          type: NotifiType.Button,
          text: "Yes",
          click() {
            const cb = this.getState("callback");
            this.close();
            
            if (cb) {
              cb();
            }
          }
        }, {
          type: NotifiType.Button,
          text: "No",
          click() {
            this.close();
          }
        }]
      }]
    }]
  }), {
    positionFixed: true,
    position: ["center", "center"],
    visible: false,
  }, {
    action: "delete",
    thing: "this lesson",
    callback: null,
  });
  
  // Delete Lesson Series Popup
  const delLesson = notifis.delLesson = NotifiUI.new<DelLesson>(NotifiUI.template<DelLesson>({
    type: NotifiType.Minimal,
    children: [{
      type: NotifiType.HBox,
      children: [{
        type: NotifiType.Button,
        text: "Same Hour",
        click() {
          const lesson = this.getState("lesson");
          
          if (lesson) {
            confirm.setState("action", "delete");
            confirm.setState("thing", "these lessons");
            confirm.setState("callback", () => {
              state.triggerEvent("deleteSeries", {
                lesson,
                weektime: lesson.weektime,
              });
            });
            
            confirm.open();
          }
          
          this.close();
        }
      }, {
        type: NotifiType.Button,
        text: "All",
        click() {
          const lesson = this.getState("lesson");
          
          if (lesson) {
            confirm.setState("action", "delete");
            confirm.setState("thing", "these lessons");
            confirm.setState("callback", () => {
              state.triggerEvent("deleteSeries", {
                lesson,
              });
            });
            
            confirm.open();
          }
          
          this.close();
        }
      }]
    }]
  }), {
    position: [1, 2],
    anchorMargin: [-baseMargin, baseMargin],
    width: sideBarWidthPadding,
    visible: false,
    blurClose: true,
  }, {
    lesson: null,
  });
  
  const calendarMsg = notifis.calendarMsg = NotifiUI.new<CalendarMsgModel>(NotifiUI.template<CalendarMsgModel>({
    type: NotifiType.Minimal,
    children: [{
      type: NotifiType.HBox,
      children: [{
        type: NotifiType.Text,
        text: "{{ msg }}",
      }]
    }]
  }), {
    visible: false,
    position: [0.5, 0],
    anchorMargin: [0, -baseMargin],
    width: 350,
  }, {
    msg: "",
  });
  
  const leftColWidth = 4;
  const lessonSummary = notifis.lessonSummary = NotifiUI.new<LessonSummaryModel>(NotifiUI.template<LessonSummaryModel>({
    type: NotifiType.Minimal,
    children: [{
      type: NotifiType.VBox,
      children: [{
        type: NotifiType.HBox,
        children: [{
          type: NotifiType.Text,
          text: locale.roleProfessor,
          size: leftColWidth,
        }, {
          type: NotifiType.Text,
          text: "{{ professor : b }}",
        }]
      }, {
        type: NotifiType.HBox,
        children: [{
          type: NotifiType.Text,
          text: locale.sidebar.startTime,
          size: leftColWidth,
        }, {
          type: NotifiType.Text,
          text: "{{ profStartTime : b }}",
        }]
      }, {
        type: NotifiType.HBox,
        children: [{
          type: NotifiType.Text,
          text: locale.roleStudent,
          size: leftColWidth,
        }, {
          type: NotifiType.Text,
          text: "{{ student : b }}",
        }]
      }, {
        type: NotifiType.HBox,
        children: [{
          type: NotifiType.Text,
          text: locale.sidebar.startTime,
          size: leftColWidth,
        }, {
          type: NotifiType.Text,
          text: "{{ studStartTime : b }}",
        }]
      }, {
        type: NotifiType.HBox,
        children: [{
          type: NotifiType.Text,
          text: locale.sidebar.paidDuration,
          size: leftColWidth,
        }, {
          type: NotifiType.Text,
          text: "{{ paidDuration : b }}",
        }]
      }, {
        type: NotifiType.HBox,
        children: [{
          type: NotifiType.Text,
          text: locale.sidebar.lessonId,
          size: leftColWidth,
        }, {
          type: NotifiType.Text,
          text: "{{ lessonId : b }}",
        }]
      }, {
        type: NotifiType.HBox,
        children: [{
          type: NotifiType.Text,
          text: locale.sidebar.lessonOrderId,
          size: leftColWidth,
        }, {
          type: NotifiType.Text,
          text: "{{ orderId : b }}",
        }]
      }]
    }]
  }), {
    filters: {
      b(s) {
        return jSh.c("b", {
          text: s,
        });
      }
    },
    position: [0, 2],
    anchorMargin: [0, baseMargin],
    width: 350,
    visible: false,
    blurClose: true,
  }, {
    professor: "",
    student: "",
    lessonId: 0,
    profStartTime: "",
    studStartTime: "",
    adminStartTime: "",
    paidDuration: "",
    orderId: 0,
  });
}
