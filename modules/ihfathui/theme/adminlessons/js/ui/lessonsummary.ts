import {Lesson} from "../model";
import {Component} from "lces";
import jSh from "jshorts";

interface LessonSummaryHoverTimer {
  destroy: never;
}

export
class LessonSummaryHover extends Component<never, never, LessonSummaryHoverTimer> {
  private destroyed: boolean;
  
  constructor(
    public lesson: Lesson,
    public manager: LessonSummaryManager,
    public dom = jSh.d(".ihfath-lesson-summary"),
  ) {
    super();
    
    this.build();
    this.destroyed = false;
  }
  
  private build() {
    
  }
  
  private destroy() {
    if (!this.destroyed) {
      this.dom.parentNode.removeChild(this.dom);
      this.manager.destroy(this.lesson.lid);
      
      this.destroyed = true;
    }
  }
  
  destroyTimeout() {
    this.timeout("destroy", 750).then(stub => this.destroy());
  }
  
  clearDestroyTimeout() {
    this.clearTimeout("destroy");
  }
}

export
class LessonSummaryManager extends Component {
  private lessons: {
    [lid: number]: LessonSummaryHover;
  };
  
  constructor() {
    super();
  }
  
  hover(lesson: Lesson): LessonSummaryHover {
    const hover = this.lessons[lesson.lid]
                  || (this.lessons[lesson.lid] = new LessonSummaryHover(lesson, this));
    
    hover.clearTimeout("destroy");
    return hover;
  }
  
  destroy(lid: number) {
    this.lessons[lid] = undefined;
  }
}
