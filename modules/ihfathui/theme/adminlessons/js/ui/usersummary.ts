import jSh from "jshorts";
import {Component} from "lces";
import {state} from "../state";
import {locale} from "../locale";

interface UserSummaryStateModel {
  visible: boolean;
  data: {
    [title: string]: Node[]
  }
}

export
class UserSummary extends Component<UserSummaryStateModel> {
  constructor(
    public dom: HTMLDivElement
  ) {
    super();
    
    this.newState("data", null);
    this.newState("visible", false);
    
    // Event listeners
    this.onState("visible", visible => {
      if (visible) {
        this.dom.classList.remove("ihfath-hidden");
      } else {
        this.dom.classList.add("ihfath-hidden");
      }
    });
    
    // Populate with data entries
    this.onState("data", data => {
      const dom = this.dom;
      
      if (data) {
        // Clear current data table (if any)
        dom.innerHTML = "";
        
        const titles = Object.keys(data);
        const frag = jSh.docFrag();
        
        for (const title of titles) {
          const body = data[title];
          
          frag.appendChild(jSh.d({
            sel: ".ihfath-data",
            child: [
              jSh.d(".ihfath-title", title),
              jSh.d(".ihfath-body", null, body),
            ],
          }));
        }
        
        dom.appendChild(frag);
      } else {
        dom.appendChild(jSh.d({
          sel: ".ihfath-data",
          child: [
            jSh.d(".ihfath-body", null, locale.noData),
          ],
        }));
      }
    });
  }
}
