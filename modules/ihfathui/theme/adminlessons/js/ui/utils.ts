export function humanTime(ms: number) {
  const hourMs = 1000 * 60 * 60;
  const minMs = 1000 * 60;
  const secMs = 1000;
  const hours = Math.floor(ms / hourMs);
  const mins = Math.floor((ms - (hourMs * hours)) / minMs);
  const secs = Math.floor((ms - (hourMs * hours) - (minMs * mins)) / secMs);
  const padd = (v: number, len: number = 2) => ("" + v).padStart(len, "0");

  return `${ padd(hours) }:${ padd(mins) }:${ padd(secs) }`;
}
