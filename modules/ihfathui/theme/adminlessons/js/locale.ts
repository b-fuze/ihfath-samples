type LocaleString = string | string[] | {[string: string]: string};

export interface LocaleStrings {
  [str: string]: LocaleString;
  noData: string;

  days: string[];
  hours: string;

  userRealName: string;
  userGender: string;
  userMail: string;
  userLastLogin: string;
  userCreated: string;
  userRole: string;
  userTimezone: string;

  roleAdmin: string;
  roleProfessor: string;
  roleStudent: string;

  users: string;
  students: string;
  professors: string;

  sidebar: {
    actions: string;
    details: string;
    participants: string;
    lessonDetails: string;
    lessonActivity: string;

    actionSingle: string;
    actionSeries: string;
    actionMoveSingle: string;
    actionMoveSeries: string;
    actionDeleteSingle: string;
    actionDeleteSeries: string;
    actionPostponeLesson: string;
    actionGoToLesson: string;

    professor: string;
    student: string;
    admin: string;
    startTime: string;

    lessonId: string;
    lessonType: string;
    lessonProgram: string;
    lessonSeriesId: string;
    lessonOrderId: string;
    paidDuration: string;
    hours: string;

    lastVisit: string;
    notAvailableNA: string;
    duration: string;
  };

  findAUser: string;
  userNoCalendar: string;
  errorGettingCalendar: string;
};

export const locale = {} as LocaleStrings;
export function updateLocale(localeSrc: LocaleStrings) {
  Object.assign(locale, localeSrc);
}
