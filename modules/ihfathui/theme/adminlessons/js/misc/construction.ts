import {TabManager} from "../ui/tabs";
import {Userfield} from "../ui/userfield";
import {UserSummary} from "../ui/usersummary";
import {CalendarManager, MonthCell, WeekCell, FocusedCalendar} from "../ui/calendar";
import {ConfigSchema} from "./config";
import {RequestManager, EntityAspect, EntityAction, api} from "./req";
import {locale} from "../locale";
import {state, CalendarUnavailableMessage} from "../state";
import {UserRoles, User, userToUser, userSummaryPrettyMap, lessonToLesson, Lesson} from "../model";
import jSh from "jshorts";
import {stub} from "az-cancel-promise";
import {setupNotifis, notifis} from "../ui/notifi";

export
function construct(config: ConfigSchema) {
  // Setup notifis
  setupNotifis();

  // Setup tabs
  const manager = new TabManager();
  const tabConfig = config.dom.tabs;

  // Build tabs from the provided config
  for (const tabName of tabConfig.items) {
    const tab = tabConfig.tabs.jSh(".ihfath-tab-" + tabName)[0];
    const body = tabConfig.body.jSh(".ihfath-body-" + tabName)[0];

    manager.addTab(tabName, tab, body);
  }

  // Focus first tab
  manager.selectTab(0);

  // Setup userfield
  const user = new Userfield(config.dom.userInput, {
    editUser: {},
    messages: {},
    spinner: {},
    userReady: {},
  });

  // Make userfield accessible to all with access to state
  state.setUserField(user);

  // Setup user summary
  const userSummary = new UserSummary(config.dom.userSummary);

  // Setup calendars
  const calManager = new CalendarManager(
    config.dom.calendarWrap,
    config.dom.calendarMonth,
    config.dom.calendarWeek,
    config.dom.calendarInput,
    config.dom.calendarUnavailable,
    config.dom.calendarSideBar,
  );

  // Bind msg notifi to calendar
  notifis.calendarMsg.setState("anchor", config.dom.calendarWrap);

  // Delete lesson when the user clicks the delete action button
  state.on("deleteLesson", lesson => {
    RequestManager.request(
      RequestManager.actions.entityManipulate(
        EntityAspect.Lesson,
        EntityAction.Delete,
        lesson.lid,
      )
    ).xhr.then(data => {
      if (!data.error) {
        state.setState("sideBarVisible", false);

        // Update the week calendar
        if (calManager.getState("focused") === FocusedCalendar.Week) {
          state.triggerEvent("reqWeekData", true);
        }

        // Update month calendar
        updateCalendar(state.getState("userData"));

        notifis.calendarMsg.setState("msg", "Deleted 1 lesson successfully");
        notifis.calendarMsg.open(5000);
      } else {
        console.log("LESSON DELETE ERR: ", data);
      }
    });
  });

  // Delete series when the user clicks the delete action button
  state.on("deleteSeries", series => {
    const user = state.getState("userData");

    RequestManager.request(
      RequestManager.actions.entityManipulate(
        EntityAspect.Series,
        EntityAction.Delete,
        series.lesson.series,
        series.weektime,
      )
    ).xhr.then(data => {
      if (!data.error) {
        state.setState("sideBarVisible", false);

        // Update the week calendar
        if (calManager.getState("focused") === FocusedCalendar.Week) {
          state.triggerEvent("reqWeekData", true);
        }

        // Update month calendar
        updateCalendar(user);

        notifis.calendarMsg.setState("msg", "Deleted " + data.data.lessons + " lessons successfully");
        notifis.calendarMsg.open(5000);
      } else {
        console.log("SERIES DELETE ERR: ", data);
      }
    });
  });

  state.on("moveLessonConfirm", move => {
    const user = state.getState("userData");

    RequestManager.request(
      RequestManager.actions.entityManipulate(
        EntityAspect.Lesson,
        EntityAction.Move,
        move.lesson.lid,
        move.weektime,
        user.uid,
      )
    ).xhr.then(data => {
      if (!data.error) {
        state.setState("sideBarVisible", false);

        // Update the week calendar
        if (calManager.getState("focused") === FocusedCalendar.Week) {
          state.triggerEvent("reqWeekData", true);
        }

        // Update month calendar
        updateCalendar(user);

        notifis.calendarMsg.setState("msg", "Moved lesson successfully");
        notifis.calendarMsg.open(5000);
      } else {
        console.log("LESSON MOVE ERR: ", data);
      }
    });
  });

  // Show suggestions when the user updates the field
  user.on("change", (name: string) => {
    if (name) {
      RequestManager.request(
        RequestManager.actions.userFind(name)
      ).xhr.then(data => {
        // console.log("DATA", data);
        // Let everyone know of the new suggestions
        const names = jSh.type(data) === "array" ? [] : Object.keys(data);
        state.triggerEvent("userSuggestions", names);
      });
    } else {
      state.triggerEvent("userSuggestions", []);
    }
  });

  // Go get user data when a user is chosen from the suggestions
  state.onState("user", function(name: string) {
    if (name) {
      // Show spinner
      user.userActions.spinner.setState("visible", true);

      RequestManager.request(
        RequestManager.actions.user(name)
      ).xhr.then(data => {
        if (data && !data.error) {
          // Let everyone know of the user's data arrival
          state.setState("userActive", true);
          state.setState("userData", userToUser(data.data));

          // Hide spinner
          user.userActions.spinner.setState("visible", false);
        } else {
          console.error("Ihfath Request User Error", data.error);
        }
      });
    }
  });

  // Reference userrel tab
  const tabUserrel = manager.getTab("userrel");

  // Show "editUser" action when a new user is loaded
  state.onState("userData", function(userData) {
    const editUser = user.userActions.editUser;
    const messages = user.userActions.messages;

    if (userData) {
      (<HTMLAnchorElement>messages.dom).href = `${ api.base }/ihfath/messages/${ userData.uid }`;
      (<HTMLAnchorElement>editUser.dom).href = `${ api.base }/user/${ userData.uid }/edit`;
      editUser.setState("visible", true);
      messages.setState("visible", true);

      // Update summary
      userSummary.setState("data", userSummaryPrettyMap(userData));
      userSummary.setState("visible", true);

      const userRelMapping = {
        [UserRoles.Admin]: locale.users,
        [UserRoles.Professor]: locale.students,
        [UserRoles.Student]: locale.professors,
      };

      // Update userrel tab to a relevant title
      tabUserrel.setText(userRelMapping[userData.role]);

      updateCalendar(userData);
    } else {
      calManager.setCalendarUnavailableMsg(CalendarUnavailableMessage.UserNotLoaded);
      state.setState("calendarAvailable", false);
      editUser.setState("visible", false);
      messages.setState("visible", false);
      userSummary.setState("visible", false);
    }
  });

  state.onState("userActive", function(active) {
    const userReady = user.userActions.userReady;

    if (active) {
      userReady.setState("visible", true);
    } else {
      userReady.setState("visible", false);
    }
  });

  // Update monthly calendar
  function updateCalendar(userData: User): void {
    if (userData.role === UserRoles.Admin) {
      calManager.setCalendarUnavailableMsg(CalendarUnavailableMessage.UserNoCalendar);
      return;
    }

    RequestManager.request(
      RequestManager.actions.userMonth(userData.uid, state.getState("month"), state.getState("year"))
    ).xhr.then(data => {
      if (!data.error) {
        // Show calendar
        state.setState("calendarAvailable", true);

        const table = data.data.table;
        const users = data.data.users;
        const sanitizedUsers: {[u: number]: User} = {};

        // Sanitize raw lesson data to a more usable format
        table.forEach((row: MonthCell[]) => {
          for (const cell of row) {
            cell.lessons = cell.lessons.map<Lesson>(lessonRaw => lessonToLesson(lessonRaw));
          }
        });

        // Sanitize users provided by server
        for (const uid of Object.keys(users)) {
          const user = userToUser(users[uid]);
          sanitizedUsers[+uid] = user;
        }

        // NOTE: Push involved users _before_ you update the calendar, the
        // calendar will need this user data
        state.setState("usersInvolved", sanitizedUsers);

        // Push/update calendar data
        calManager.setMonthData(table);
      }
    });
  }

  // Week data event dispatched by user clicking a week row
  // on the calendar ui
  state.on("reqWeekData", stub => {
    const monthDay = state.getState("monthDay");
    const month = state.getState("month");
    const year = state.getState("year");
    const userData = state.getState("userData");

    if (userData && userData.role !== UserRoles.Admin) {
      RequestManager.request(
        RequestManager.actions.userWeek(userData.uid, monthDay, month, year)
      ).xhr.then(data => {
        if (!data.error) {
          const table = data.data.table;
          const users = data.data.users;
          const tableRemap: WeekCell[][] = (table as any[]).map((col: any[], day) => {
            // Shift monday-based to sunday-based
            day++;
            return col.map((cell: any, hour) => {
              if (jSh.type(cell) === "object") {
                const lesson = lessonToLesson(cell.lesson);

                return {
                  curDay: false,
                  weekDay: day === 7 ? 0 : day,
                  lesson: lesson,
                  span: lesson.paidDuration > 60 ? 2 : 1,
                  hour: hour,
                  selectable: false,
                };
              } else {
                return {
                  curDay: false,
                  weekDay: day === 7 ? 0 : day,
                  lesson: null,
                  span: 1,
                  hour: hour,
                  selectable: false,
                };
              }
            });
          });

          const usersInvolved = state.getState("usersInvolved") || {};
          const usersRemapped: {[u: number]: User} = {};
          for (const uid of Object.keys(users)) {
            usersRemapped[+uid] = userToUser(users[uid]);
          }

          // Push table and users to state
          state.setState("usersInvolved", usersRemapped);
          state.setState("weekData", tableRemap);
        } else {
          console.log("WEEK ERR", data.cause);
        }
      });
    }
  });

  // Update calendar when the app state changes and the user's available
  state.onState("month", () => {
    const userData = state.getState("userData");

    if (userData) {
      updateCalendar(userData);
    }
  });

  state.onState("year", () => {
    const userData = state.getState("userData");

    if (userData) {
      updateCalendar(userData);
    }
  });

  state.onState("weekData", weekData => {
    calManager.setState("focused", FocusedCalendar.Week);
    calManager.setWeekData(weekData);
  });
}
