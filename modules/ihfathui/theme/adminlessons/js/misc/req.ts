import {
  Component,
  request,
  ReqParams,
  RequestPromise
} from "lces";
import {stub as PromiseStub} from "az-cancel-promise";

interface func {
  (param: string): void;
}

interface requestMap {
  [propName: string]: Request;
}

const requests: Request[] = [];
const requestMap: requestMap = {};

interface RequestReqParams extends ReqParams {
  cancel?: () => void;
}

export
class Request<T = any> {
  type: RequestType;
  completed: boolean;
  canceled: boolean;
  onCancel: () => void;
  data: any;
  xhr: RequestPromise<T>;

  constructor(type: RequestType, params: RequestReqParams) {
    const that = this;
    const success = params.success;
    const fail = params.fail;

    // Remove callbacks
    params.success = undefined;
    params.fail = undefined;

    this.type = type;
    this.completed = false;
    this.canceled = false;
    this.onCancel = params.cancel;
    this.data = null;

    const xhr = request<T>(params);
    this.xhr = <RequestPromise<T>>(xhr.then(function(data) {
      that.completed = true;

      if (!that.canceled) {
        if (success) {
          success && success.call(that, data);
        }
      } else {
        return PromiseStub.CANCEL_TOKEN;
      }

      return data;
    }).catch(function(e) {
      that.completed = true;

      if (!that.canceled && fail) {
        fail.call(that);
      }

      throw e;
    }));

    // Ensure the xhr object is exposed
    this.xhr.xhr = xhr.xhr;

    requests.push(this);
  }

  cancel() {
    if (!this.completed) {
      if (this.onCancel) {
        this.onCancel();
      }

      return this.canceled = true;
    } else {
      return false;
    }
  }
}

export const api = {
  url: "",
  base: "",
};

export
function setApiUrl(base: string, url: string) {
  api.base = base;
  api.url = url;
}

class RequestTypes {
  static userFind(name: string, param?: ReqParams) {
    return new Request("userFind", {
      url: api.base + "/user/autocomplete/" + name,
      success: param && param.success,
      json: true,
    });
  }

  static user(name: string, param?: ReqParams): Request {
    return new Request("user", {
      method: "POST",
      url: api.url + "/userInfo",
      success: param && param.success,
      formData: {
        name: name,
      },
      json: true,
    });
  }

  static userRelations(uid: number, param?: ReqParams): Request {
    return new Request("userRelations", {
      method: "POST",
      url: api.url + "/userRelations",
      success: param && param.success,
      formData: {
        uid: uid,
      },
      json: true,
    });
  }

  static userMonth(uid: number, month: number, year: number, param?: ReqParams) {
    return new Request("userMonth", {
      method: "POST",
      url: api.url + "/userMonth",
      success: param && param.success,
      formData: {
        uid,
        month,
        year,
      },
      json: true,
    });
  }

  static userWeek(uid: number, day: number, month: number, year: number, param?: ReqParams) {
    return new Request("userWeek", {
      method: "POST",
      url: api.url + "/userWeek",
      success: param && param.success,
      formData: {
        uid,
        day,
        month,
        year,
      },
      json: true,
    });
  }

  static entityManipulate(aspect: EntityAspect, action: EntityAction, entityId: number, weektime?: number, uid?: number, param?: ReqParams) {
    const formData: {
      aspect: EntityAspect,
      action: EntityAction,
      entityId: number,
      weektime?: number,
      uid?: number,
    } = {
      aspect,
      action,
      entityId,
      uid,
    };

    if (weektime) {
      formData.weektime = weektime;
    }

    return new Request("entityManipulate", {
      method: "POST",
      url: api.url + "/entityManipulate",
      success: param && param.success,
      formData,
      json: true,
    });
  }
}

export const enum EntityAspect {
  Lesson = "lesson",
  Series = "series",
}

export const enum EntityAction {
  Move = "move",
  Delete = "delete",
}

type RequestType = "userFind" | "user" | "userRelations" | "userWeek" | "userMonth" | "entityManipulate";

export
class RequestManager {
  static getRequest(name: string) {
    return requestMap[name] || null;
  }

  static setRequest(request: Request) {
    return requestMap[request.type] = request;
  }

  static request(request: Request) {
    const curRequest = this.getRequest(request.type);

    if (curRequest) {
      curRequest.cancel();
    }

    return this.setRequest(request);
  }

  static cancel(request: string) {
    const curRequest = this.getRequest(request);

    if (curRequest) {
      curRequest.cancel();
    }
  }

  static get actions() {
    return RequestTypes;
  }
};
