import {
  Component,
  ComponentState,
  ComponentStateConfig
} from "lces";
import {Lesson, User} from "./model";
import {Userfield} from "./ui/userfield";

export const enum CalendarUnavailableMessage {
  UserNotLoaded,
  UserNoCalendar,
  ErrorLoadingCalendar,
}

interface AppStateModel {
  user: string;
  userData: User;
  userActive: boolean;
  year: number;
  month: number;
  monthDay: number;
  weekData: any;
  firstWeekDay: number[];
  tab: string;
  calendarAvailable: boolean;
  sideBarVisible: boolean;
  focusedLesson: Lesson;
  usersInvolved: {
    [user: number]: User;
  };
}

interface AppEventModel {
  user: User;
  userSuggestions: string[];
  reqWeekData: boolean;

  // Sidebar actions
  postponeLesson: Lesson;
  deleteLesson: Lesson;
  deleteSeries: {
    lesson: Lesson;
    weektime?: Lesson["weektime"];
  };
  moveLesson: Lesson;
  moveSeries: Lesson;
  moveLessonConfirm: {
    lesson: Lesson;
    weektime?: Lesson["weektime"];
  };
  moveSeriesConfirm: {
    lesson: Lesson;
    weektime?: Lesson["weektime"];
  };
}

class AppState extends Component<AppStateModel, AppEventModel> {
  private userField: Userfield;

  constructor() {
    super();

    // Create states
    this.newState("user", null);
    this.newState("userData", null);
    this.newState("year", null);
    this.newState("month", null);
    this.newState("monthDay", null);
    this.newState("weekData", null);
    this.newState("firstWeekDay", []);
    this.newState("tab", null);
    this.newState("userActive", false);
    this.newState("calendarAvailable", null);
    this.newState("sideBarVisible", null);
    this.newState("focusedLesson", null);
    this.newState("usersInvolved", null);

    // Create events
    this.newEvent("user");
    this.newEvent("userSuggestions");
    this.newEvent("reqWeekData");

    // Sidebar actions
    this.newEvent("deleteLesson");
    this.newEvent("deleteSeries");
    this.newEvent("moveLesson");
    this.newEvent("moveSeries");
    this.newEvent("moveLessonConfirm");
    this.newEvent("moveSeriesConfirm");
  }

  setUserField(userField: Userfield) {
    this.userField = userField;
  }

  focusUserField() {
    this.userField.dom.focus();
  }
}

// Expose state
export const state = new AppState();
