import {locale} from "./locale";
import jSh from "jshorts";

// Ihfath core user roles, maps to Ihfath Core User Role definitions
export const enum UserRoles {
  Admin = 1,
  Professor = 2,
  Student = 3,
}

export interface User {
  [field: string]: any;
  uid: number;
  name: string;
  role: UserRoles;
  mail: string;
  created: number;
  access: number;
  login: number;
  timezone: string;
  timezoneOffset: number;
  language: string;
  gender?: string;
  firstName?: string;
  lastName?: string;
  realName?: string;
}

export function userToUser(userData: any): User {
  const enum TypeMap {
    Number,
    Timestamp,
    String,
    FieldString,
    FieldRole,
  };

  const basicExternalKeys: {
    [propName: string]: TypeMap
  } = {
    uid: TypeMap.Number,
    name: TypeMap.String,
    role: TypeMap.FieldRole,
    mail: TypeMap.String,
    created: TypeMap.Timestamp,
    access: TypeMap.Timestamp,
    login: TypeMap.Timestamp,
    timezone: TypeMap.String,
    timezoneOffset: TypeMap.Number,
    language: TypeMap.String,
    gender: TypeMap.FieldString,
    firstName: TypeMap.FieldString,
    lastName: TypeMap.FieldString,
    realName: TypeMap.FieldString, // Should probably remove this
  };

  const externalRemap: {
    [p: string]: string;
  } = {
    roles: "role",
    field_ihfath_gender: "gender",
    field_ihfath_realname: "realName",
    field_ihfath_firstname: "firstName",
    field_ihfath_lastname: "lastName",
    timezone_offset: "timezoneOffset",
  };

  // Copy/map basic properties from server user data to User shaped object
  const userBase: any = {};
  for (const key of Object.keys(userData)) {
    const remappedKey = key in externalRemap ? externalRemap[key] : key;

    switch (basicExternalKeys[remappedKey]) {
      case TypeMap.String:
        userBase[remappedKey] = userData[key];
        break;

      case TypeMap.Number:
        userBase[remappedKey] = parseInt(userData[key], 10);
        break;

      case TypeMap.Timestamp:
        userBase[remappedKey] = parseInt(userData[key], 10) * 1000;
        break;

      case TypeMap.FieldString:
        userBase[remappedKey] = userData[key]["und"] ? userData[key]["und"][0]["value"] : null;
        break;

      case TypeMap.FieldRole:
        const roles: any = userData[key];

        // Set the user's role from the user's data role object
        roleLoop:
        for (const key of Object.keys(roles)) {
          switch(roles[key]) {
            case "administrator":
              userBase.role = UserRoles.Admin;
              break roleLoop;
            case "professor":
              userBase.role = UserRoles.Professor;
              break roleLoop;
            case "student":
              userBase.role = UserRoles.Student;
              break roleLoop;
          }
        }
        break;
    }
  }

  if (!userBase.firstName && !userBase.lastName) {
    userBase.firstName = "";
    userBase.lastName = "";
    userBase.realName = "";
  } else {
    userBase.realName = ((userBase.firstName || "") + " " + (userBase.lastName || "")).trim();
  }

  return userBase;
}

type UserSummaryDict = {
  [title: string]: Node[]
};

export function userSummaryPrettyMap(user: User): UserSummaryDict {
  const enum TypeRenderMap {
    Normal,
    NormalCapital,
    Date,
    Role,
    Timezone,
  };

  interface Field {
    field: string;
    type: TypeRenderMap;
  };

  const userFields: {
    [p: string]: Field
  } = {
    realName: {
      field: "userRealName",
      type: TypeRenderMap.Normal,
    },
    gender: {
      field: "userGender",
      type: TypeRenderMap.NormalCapital,
    },
    mail: {
      field: "userMail",
      type: TypeRenderMap.Normal,
    },
    login: {
      field: "userLastLogin",
      type: TypeRenderMap.Date,
    },
    created: {
      field: "userCreated",
      type: TypeRenderMap.Date,
    },
    role: {
      field: "userRole",
      type: TypeRenderMap.Role,
    },
    timezone: {
      field: "userTimezone",
      type: TypeRenderMap.Timezone,
    },
  };

  const userSummary: UserSummaryDict = {};
  const roleMap: {
    [role: number]: string;
  } = {
    [UserRoles.Admin]: locale.roleAdmin,
    [UserRoles.Professor]: locale.roleProfessor,
    [UserRoles.Student]: locale.roleStudent,
  };

  let fieldWrapperCallback: (title: string) => Node;
  const fieldWrapper = jSh.t;
  const fieldEmWrapper = (text: string) => jSh.c("i", {
    text: text
  });

  for (const field of Object.keys(userFields)) {
    const fieldData = userFields[field];
    let fieldValue = user[field];

    if (fieldValue === null) {
      fieldValue = "Empty";
      fieldWrapperCallback = fieldEmWrapper;
    } else {
      fieldWrapperCallback = fieldWrapper;
    }

    const fieldTitle: string = locale[fieldData.field] as string;
    switch (fieldData.type) {
      case TypeRenderMap.Normal:
        userSummary[fieldTitle] = [
          fieldWrapperCallback(fieldValue)
        ];
        break;

      case TypeRenderMap.NormalCapital:
        userSummary[fieldTitle] = [
          fieldWrapperCallback(jSh.strCapitalize(fieldValue))
        ];
        break;

      case TypeRenderMap.Date:
        userSummary[fieldTitle] = [
          fieldWrapperCallback(new Date(fieldValue || new Date().getTime()).toLocaleString())
        ];
        break;

      case TypeRenderMap.Role:
        userSummary[fieldTitle] = [
          fieldWrapperCallback(roleMap[fieldValue])
        ];
        break;

      case TypeRenderMap.Timezone:
        userSummary[fieldTitle] = [
          fieldWrapperCallback(fieldValue),
          fieldEmWrapper(` (UTC${ user.timezoneOffset })`),
        ];
        break;
    }
  }

  return userSummary;
}

export interface LessonTypeData {
  tid: number;
  available: boolean;
  deleted: boolean;
  weight: number;
  data: {
    [lang: string]: {
      name: string;
      description: string;
    }
  }
}

export interface LessonProgramData {
  pid: number;
  type: number;
  available: boolean;
  deleted: boolean;
  weight: number;
  data: {
    [lang: string]: {
      name: string;
      description: string;
    }
  }
}

export interface Lesson {
  lid: number;
  type: number;
  series: number;
  seriesWeektimeId: number;
  program: number;
  status: number;
  puid: number;
  suid: number;
  orderId: number;
  notesProfessor: string;
  notesStudent: string;
  ratingProfessor: number;
  ratingStudent: number;
  startDate: number;
  paidDuration: 30 | 60 | 90 | 120;
  duration: number;
  profStart: number;
  profDur: number;
  profLast: number;
  studentStart: number;
  studentDur: number;
  studentLast: number;
  penalty: number;
  cancelled: boolean;
  deleted: boolean;
  substitutedBy: number;
  weektime: number;
  typeName: string;
  typeData: LessonTypeData;
  programName: string;
  programData: LessonProgramData;
}

export function lessonToLesson(lessonData: any): Lesson {
  const enum TypeRenderMap {
    Number,
    NumberTime,
    String,
    Boolean,
    Depth,
  };

  interface TypeRenderMapLiteral {
    [key: string]: TypeRenderMap;
  }

  // Partial name remapping as needed
  const lessonKeyRemapping: {[p: string]: string} = {
    seriesWeektimeId: "series_weektime_id",
    notesProfessor: "notes_professor",
    notesStudent: "notes_student",
    ratingProfessor: "rating_professor",
    ratingStudent: "rating_student",
    startDate: "start_date",
    paidDuration: "paid_duration",
    orderId: "order_id",
    profStart: "prof_start",
    profDur: "prof_dur",
    profLast: "prof_last",
    studentStart: "student_start",
    studentDur: "student_dur",
    studentLast: "student_last",
    substitutedBy: "substituted_by",
    typeName: "type_name",
    typeData: "type_data",
    programName: "program_name",
    programData: "program_data",
  };

  const lessonKeys: TypeRenderMapLiteral = {
    lid: TypeRenderMap.Number,
    type: TypeRenderMap.Number,
    series: TypeRenderMap.Number,
    seriesWeektimeId: TypeRenderMap.Number,
    program: TypeRenderMap.Number,
    puid: TypeRenderMap.Number,
    suid: TypeRenderMap.Number,
    orderId: TypeRenderMap.Number,
    notesProfessor: TypeRenderMap.String,
    notesStudent: TypeRenderMap.String,
    ratingProfessor: TypeRenderMap.Number,
    ratingStudent: TypeRenderMap.Number,
    startDate: TypeRenderMap.Number,
    paidDuration: TypeRenderMap.Number,
    duration: TypeRenderMap.Number,
    // FIXME: *Start/Last should also be NumberTime
    profStart: TypeRenderMap.Number,
    profDur: TypeRenderMap.NumberTime,
    profLast: TypeRenderMap.Number,
    studentStart: TypeRenderMap.Number,
    studentDur: TypeRenderMap.NumberTime,
    studentLast: TypeRenderMap.Number,
    penalty: TypeRenderMap.Number,
    cancelled: TypeRenderMap.Boolean,
    deleted: TypeRenderMap.Boolean,
    substitutedBy: TypeRenderMap.Number,
    weektime: TypeRenderMap.Number,
    typeName: TypeRenderMap.String,
    typeData: TypeRenderMap.Depth,
    programName: TypeRenderMap.String,
    programData: TypeRenderMap.Depth,
  };

  const lessonTypeKeys: TypeRenderMapLiteral = {
    tid: TypeRenderMap.Number,
    available: TypeRenderMap.Boolean,
    deleted: TypeRenderMap.Boolean,
    weight: TypeRenderMap.Number,
    data: TypeRenderMap.Depth,
  };

  const lessonProgramKeys: TypeRenderMapLiteral = {
    pid: TypeRenderMap.Number,
    available: TypeRenderMap.Boolean,
    deleted: TypeRenderMap.Boolean,
    weight: TypeRenderMap.Number,
    data: TypeRenderMap.Depth,
  };

  const lessonMetaDataValueKeys: TypeRenderMapLiteral = {
    name: TypeRenderMap.String,
    description: TypeRenderMap.String,
  };

  const lessonMetaDataKeys: TypeRenderMapLiteral = {
    en: TypeRenderMap.Depth,
    fr: TypeRenderMap.Depth,
  };

  const depthMap: {[map: string]: TypeRenderMapLiteral} = {
    typeData: lessonTypeKeys,
    programData: lessonProgramKeys,
    data: lessonMetaDataKeys,
    en: lessonMetaDataValueKeys,
    fr: lessonMetaDataValueKeys,
  };

  function remap(mapping: TypeRenderMapLiteral, source: any, dest: any, isLesson: boolean) {
    for (const key of Object.keys(mapping)) {
      const oldKey = isLesson && key in lessonKeyRemapping ? lessonKeyRemapping[key] : key;
      const type = mapping[key];
      let value: any;

      switch (type) {
        case TypeRenderMap.Number:
          value = parseFloat(source[oldKey]);
          break;
        case TypeRenderMap.NumberTime:
          value = parseFloat(source[oldKey]) * 1000;
          break;
        case TypeRenderMap.String:
          if (!source) {
            console.error("LESSON ERR", mapping, dest);
          }
          value = source[oldKey];
          break;
        case TypeRenderMap.Boolean:
          value = !!parseInt(source[oldKey], 10);
          break;
        case TypeRenderMap.Depth:
          const depthMapping = depthMap[key];
          value = oldKey in source ? remap(depthMapping, source[oldKey], {}, false) : {};
          break;
      }

      dest[key] = value;
    }

    return dest;
  }

  return remap(lessonKeys, lessonData, {}, true);
}
