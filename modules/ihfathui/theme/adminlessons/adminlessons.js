(function (jSh, Cellidi) {
  'use strict';

  jSh = jSh && jSh.hasOwnProperty('default') ? jSh['default'] : jSh;
  Cellidi = Cellidi && Cellidi.hasOwnProperty('default') ? Cellidi['default'] : Cellidi;

  // LCES Reimplemented/Lightly Revised in TypeScript
  class ComponentState {
      /**
       * Component State
       * @param name   Name of the state
       * @param config State config
       */
      constructor(name, defaultValue, config) {
          // Create callback arrays
          this.conditionCallbacks = [];
          this.callbacks = [];
          // Default config
          this.oldValue = undefined;
          this.value = defaultValue;
          this.writable = true;
          this.writableStrict = false;
          this.forceTrigger = false;
          // Set name
          this.name = name;
          // Add config
          if (config) {
              Object.assign(this, config);
          }
      }
      // Methods
      addCallback(callback, isConditional) {
          const callbacks = isConditional ? this.conditionCallbacks : this.callbacks;
          callbacks.push(callback);
      }
      removeCallback(callback, isConditional) {
          const callbacks = isConditional ? this.conditionCallbacks : this.callbacks;
          const index = callbacks.indexOf(callback);
          if (index !== -1) {
              return callbacks.splice(index, 1)[0];
          }
          else {
              return null;
          }
      }
      addConditionalCallback(callback) {
          this.addCallback(callback, true);
      }
      removeConditionalCallback(callback) {
          return this.removeCallback(callback, true);
      }
  }
  class ComponentEvent {
      constructor(name) {
          this.name = name;
          this.callbacks = [];
      }
      // Methods
      addCallback(callback) {
          this.callbacks.push(callback);
      }
      removeCallback(callback) {
          const index = this.callbacks.indexOf(callback);
          if (index !== -1) {
              return this.callbacks.splice(index, 1)[0];
          }
          else {
              return null;
          }
      }
  }
  class Component {
      constructor() {
          this.states = {};
          this.events = {};
          this.timeouts = {
              timers: {},
              names: {},
          };
      }
      // Events
      newEvent(event) {
          this.eventExists(event, null, () => {
              const eventName = event.trim();
              if (eventName) {
                  const eventObj = new ComponentEvent(event);
                  this.events[eventName] = eventObj;
              }
              else {
                  throw new Error(`LCES: "${eventName}" isn't a valid event name`);
              }
          });
      }
      on(event, callback) {
          this.eventExists(event, eventObj => {
              eventObj.addCallback(callback);
          });
      }
      triggerEvent(event, data) {
          this.eventExists(event, eventObj => {
              for (const callback of eventObj.callbacks) {
                  callback(data);
              }
          });
      }
      removeEventListener(event, callback) {
          return this.eventExists(event, eventObj => {
              return eventObj.removeCallback(callback);
          });
      }
      // States
      newState(state, defaultValue, config) {
          this.stateExists(state, () => {
              // FIXME: Do nothing... For now...
          }, () => {
              const stateObj = new ComponentState(state, defaultValue, config);
              this.states[state] = stateObj;
          });
      }
      onState(state, callback) {
          this.stateExists(state, stateObj => {
              stateObj.addCallback(callback);
          });
      }
      stateCondition(state, callback) {
          this.stateExists(state, stateObj => {
              stateObj.addConditionalCallback(callback);
          });
      }
      removeStateListener(state, callback) {
          return this.stateExists(state, stateObj => {
              return stateObj.removeCallback(callback);
          });
      }
      removeConditionListener(state, callback) {
          return this.stateExists(state, stateObj => {
              return stateObj.removeConditionalCallback(callback);
          });
      }
      // State getters/setters
      getState(state) {
          return this.stateExists(state, stateObj => {
              return stateObj.value;
          });
      }
      setState(state, value, forceRecur = false) {
          return this.stateExists(state, stateObj => {
              let newValue = value;
              function alter(value) {
                  newValue = value;
              }
              // Check state conditions
              for (const callback of stateObj.conditionCallbacks) {
                  const result = callback.call(stateObj, newValue, alter);
                  if (result) {
                      // Cancel
                      return;
                  }
              }
              if (forceRecur || stateObj.forceTrigger || stateObj.value !== newValue) {
                  stateObj.oldValue = stateObj.value;
                  stateObj.value = newValue;
                  // Trigger events
                  for (const callback of stateObj.callbacks) {
                      callback.call(stateObj, newValue);
                  }
              }
              return value;
          });
      }
      // Existence methods
      hasState(state) {
          return this.stateExists(state, () => true, () => false);
      }
      // Private meta methods
      eventExists(event, callback, voidCallback) {
          const eventObj = this.events[event];
          if (eventObj) {
              if (callback) {
                  return callback(eventObj);
              }
          }
          else {
              if (voidCallback) {
                  return voidCallback();
              }
              else {
                  throw new ReferenceError(`LCES: Event "${event}" doesn't exist`);
              }
          }
      }
      stateExists(state, callback, voidCallback) {
          const stateObj = this.states[state];
          if (stateObj) {
              if (callback) {
                  return callback(stateObj);
              }
          }
          else {
              if (voidCallback) {
                  return voidCallback();
              }
              else {
                  throw new ReferenceError(`LCES: State "${state}" doesn't exist`);
              }
          }
      }
      // Timeouts
      timeout(name, delay = 0, concat = false, reqAnimFrame = false) {
          if (!name) {
              throw new TypeError("Component.addTimeout: name must be a valid non-empty string");
          }
          // Update name timer delay list (if any)
          const nameList = this.timeouts.names[name] || (this.timeouts.names[name] = []);
          if (nameList.indexOf(delay) === -1) {
              nameList.push(delay);
          }
          // Get timer obj (if any) or create a new one
          const nameRef = name + (delay || 0);
          const timeoutObj = this.timeouts.timers[nameRef] || (this.timeouts.timers[nameRef] = {
              timeout: null,
              animFrame: {
                  canceled: false,
                  enabled: false,
              },
              callbacks: [],
          });
          // Create promise
          let promiseResolve = null;
          const promise = new Promise((res, rej) => {
              promiseResolve = res;
          });
          const callback = () => {
              promiseResolve();
          };
          // Clear any running timers if we aren't concatenating ours
          if (!concat && timeoutObj.timeout) {
              if (timeoutObj.animFrame.enabled) {
                  timeoutObj.animFrame.canceled = true;
              }
              else {
                  clearTimeout(timeoutObj.timeout);
              }
              timeoutObj.timeout = null;
              timeoutObj.animFrame = {
                  canceled: false,
                  enabled: false,
              };
              timeoutObj.callbacks = [];
          }
          // Add setTimeout timer if none are available
          if (timeoutObj.timeout === null) {
              if (reqAnimFrame) {
                  timeoutObj.timeout = 1;
                  timeoutObj.animFrame = {
                      canceled: false,
                      enabled: true,
                  };
                  requestAnimationFrame(() => {
                      if (!timeoutObj.animFrame.canceled) {
                          for (const callback of timeoutObj.callbacks) {
                              callback.callback();
                          }
                      }
                  });
              }
              else {
                  timeoutObj.timeout = setTimeout(() => {
                      for (const callback of timeoutObj.callbacks) {
                          callback.callback();
                      }
                  }, delay || 0);
              }
          }
          // Finally concatenate our callback
          timeoutObj.callbacks.push({
              callback
          });
          return promise;
      }
      clearTimeout(name, delay) {
          if (!(name in this.timeouts.names)) {
              return;
          }
          const timeoutObjs = []; // this.timeouts.timers[name];
          if (typeof delay === "number") {
              // Remove one specific timeout
              const timeoutObj = this.timeouts.timers[name + delay];
              if (timeoutObj) {
                  timeoutObjs.push(timeoutObj);
              }
          }
          else {
              // Remove all timeouts with the same name regardless of delay
              for (const timeoutDelays of this.timeouts.names[name]) {
                  timeoutObjs.push(this.timeouts.timers[name + timeoutDelays]);
              }
          }
          // Clear timers
          for (const timeoutObj of timeoutObjs) {
              if (timeoutObj.animFrame.enabled) {
                  timeoutObj.animFrame.canceled = true;
              }
              else {
                  clearTimeout(timeoutObj.timeout);
              }
              timeoutObj.timeout = null;
              timeoutObj.callbacks = [];
          }
      }
  }
  function request(reqParam) {
      const xhr = new XMLHttpRequest();
      const method = reqParam.method || "GET";
      // Create a RequestPromise
      let promise = null;
      let promiseResolve = null;
      let promiseReject = null;
      promise = (new Promise((res, rej) => {
          promiseResolve = res;
          promiseReject = rej;
      }));
      let success = function (data) {
          promiseResolve(data);
      };
      let fail = function () {
          promiseReject(xhr);
      };
      // Add xhr to promise
      promise.xhr = xhr;
      // Setup
      if (reqParam.setup) {
          reqParam.setup(xhr);
      }
      // Querystring
      let queryData = "";
      if (reqParam.query) {
          function recursion(obj) {
              if (jSh.type(obj) === "array") {
                  return encodeURIComponent(obj.join(","));
              }
              if (jSh.type(obj) !== "object") {
                  return encodeURIComponent(obj.toString());
              }
              var qs = "";
              for (const prop in obj) {
                  if (obj.hasOwnProperty(prop)) {
                      const objValue = obj[prop];
                      switch (jSh.type(objValue)) {
                          case "string":
                              qs += "&" + prop + "=" + encodeURIComponent(objValue);
                              break;
                          case "number":
                              qs += "&" + prop + "=" + objValue;
                              break;
                          case "array":
                              qs += "&" + prop + "=" + encodeURIComponent(objValue.join(";"));
                              break;
                          case "object":
                              qs += "";
                              break;
                          case "null":
                              qs += "&" + prop + "=null";
                              break;
                          case "undefined":
                              qs += "";
                              break;
                          default:
                              qs += "";
                      }
                  }
              }
              return qs;
          }
          queryData = recursion(reqParam.query).substr(1);
      }
      else {
          queryData = "formData" in reqParam ? reqParam.formData : "";
          // Stringify data if it's not FormData
          if (queryData !== "" && !(queryData instanceof FormData)) {
              queryData = JSON.stringify(queryData);
          }
      }
      // Open url
      let url = reqParam.url;
      if (method === "GET" && queryData) {
          url += "?" + queryData;
      }
      xhr.open(method, reqParam.url, reqParam.async !== undefined ? reqParam.async : true);
      // Invoke callbacks
      xhr.onreadystatechange = function () {
          if (typeof reqParam.statechange === "function") {
              reqParam.statechange.call(this);
          }
          if (this.readyState === 4) {
              if (this.status === 200) {
                  success(reqParam.json ? jSh.parseJSON(this.responseText) : this.responseText);
              }
              else {
                  if (fail) {
                      fail(xhr);
                  }
              }
          }
      };
      // Add form header
      if (reqParam.form) {
          xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
      }
      // Send
      const oldCookies = document.cookie.split(/\s*;\s*/).map(c => [
          c.split("=")[0],
          c.split("=")[1]
      ]);
      // Strip off cookies
      if (reqParam.cookies === false) {
          const time = new Date();
          time.setTime(0);
          oldCookies.forEach(cookie => document.cookie = cookie[0] + "=; expires=" + time + "; path=/");
      }
      xhr.send(method === "POST" ? queryData : undefined);
      // Return cookies
      if (reqParam.cookies === false) {
          setTimeout(() => {
              oldCookies.forEach(cookie => {
                  document.cookie = cookie[0] + "=" + cookie[1] + "; expires=; path=/";
              });
          }, 50);
      }
      return promise;
  }
  //# sourceMappingURL=index.js.map

  var CalendarUnavailableMessage;
  (function (CalendarUnavailableMessage) {
      CalendarUnavailableMessage[CalendarUnavailableMessage["UserNotLoaded"] = 0] = "UserNotLoaded";
      CalendarUnavailableMessage[CalendarUnavailableMessage["UserNoCalendar"] = 1] = "UserNoCalendar";
      CalendarUnavailableMessage[CalendarUnavailableMessage["ErrorLoadingCalendar"] = 2] = "ErrorLoadingCalendar";
  })(CalendarUnavailableMessage || (CalendarUnavailableMessage = {}));
  class AppState extends Component {
      constructor() {
          super();
          // Create states
          this.newState("user", null);
          this.newState("userData", null);
          this.newState("year", null);
          this.newState("month", null);
          this.newState("monthDay", null);
          this.newState("weekData", null);
          this.newState("firstWeekDay", []);
          this.newState("tab", null);
          this.newState("userActive", false);
          this.newState("calendarAvailable", null);
          this.newState("sideBarVisible", null);
          this.newState("focusedLesson", null);
          this.newState("usersInvolved", null);
          // Create events
          this.newEvent("user");
          this.newEvent("userSuggestions");
          this.newEvent("reqWeekData");
          // Sidebar actions
          this.newEvent("deleteLesson");
          this.newEvent("deleteSeries");
          this.newEvent("moveLesson");
          this.newEvent("moveSeries");
          this.newEvent("moveLessonConfirm");
          this.newEvent("moveSeriesConfirm");
      }
      setUserField(userField) {
          this.userField = userField;
      }
      focusUserField() {
          this.userField.dom.focus();
      }
  }
  // Expose state
  const state = new AppState();
  //# sourceMappingURL=state.js.map

  class Tab extends Component {
      constructor(name, domTab, domBody, index, manager) {
          super();
          this.name = name;
          this.domTab = domTab;
          this.domBody = domBody;
          this.index = index;
          this.manager = manager;
          // Toggle body visibility and tab highlight
          this.newState("selected", null);
          this.onState("selected", selected => {
              if (selected) {
                  this.domTab.classList.add("ihfath-selected");
                  this.domBody.classList.remove("ihfath-hidden");
              }
              else {
                  this.domTab.classList.remove("ihfath-selected");
                  this.domBody.classList.add("ihfath-hidden");
              }
          });
          // Select when clicked
          this.domTab.addEventListener("click", () => {
              this.select();
          });
      }
      setText(text) {
          this.domTab.textContent = text;
      }
      select() {
          this.manager.selectTab(this.index);
      }
  }
  class TabManager extends Component {
      constructor() {
          super();
          const that = this;
          // Initialize props
          this.tabs = [];
          this.tabMap = {};
          this.clearedAll = false;
          // Change tabs' selected state
          state.onState("tab", function (tabName) {
              const tab = that.tabMap[tabName];
              const oldTab = that.tabMap[this.oldValue];
              if (oldTab) {
                  oldTab.setState("selected", false);
              }
              tab.setState("selected", true);
          });
          // Hide all unrelated tabs the first time
          state.onState("tab", function clearAll(tabName) {
              for (const tab of that.tabs) {
                  if (tab.name !== tabName) {
                      tab.setState("selected", false);
                  }
              }
              state.removeStateListener("tab", clearAll);
          });
      }
      addTab(name, tab, body) {
          const newTab = new Tab(name, tab, body, this.tabs.length, this);
          // Create references for our new tab
          this.tabs.push(newTab);
          this.tabMap[name] = newTab;
          return newTab;
      }
      getTab(name) {
          return this.tabMap[name];
      }
      selectTab(index) {
          let tab = null;
          if (typeof index === "number") {
              tab = this.tabs[index];
          }
          else {
              tab = this.tabMap[index];
          }
          if (tab) {
              // Select the tab
              state.setState("tab", tab.name);
          }
      }
  }
  //# sourceMappingURL=tabs.js.map

  class Userfield extends Component {
      constructor(dom, actions, userActions = {}, domSuggestions, domActions, domChangeBtn) {
          super();
          this.dom = dom;
          this.actions = actions;
          this.userActions = userActions;
          this.domSuggestions = domSuggestions;
          this.domActions = domActions;
          this.domChangeBtn = domChangeBtn;
          this.newState("value", null);
          this.newState("suggestionsVisible", true);
          this.newEvent("suggestions");
          this.newEvent("change");
          // Add dom
          this.addDom(dom);
          this.addActions(actions);
          // Disable input when a user activated
          state.onState("userActive", userActive => {
              if (userActive) {
                  this.dom.disabled = true;
              }
              else {
                  this.dom.disabled = false;
              }
          });
          // Initialize suggestionsVisible
          this.setState("suggestionsVisible", false);
      }
      addDom(dom) {
          const that = this;
          // Trigger value state when user changes textfield
          let delayTimeout = null;
          dom.addEventListener("input", e => {
              clearTimeout(delayTimeout);
              const name = dom.value.trim();
              delayTimeout = setTimeout(() => {
                  this.setState("value", name);
                  this.triggerEvent("change", name);
              }, 150);
          });
          // Reference dom nodes
          const parentNode = jSh(dom.parentNode.parentNode);
          const suggestions = this.domSuggestions = parentNode.jSh(".ihfath-suggestions")[0];
          const actions = this.domActions = parentNode.jSh(".ihfath-actions")[0];
          const changeBtn = this.domChangeBtn = parentNode.jSh(".ihfath-change-btn")[0];
          // Bind curUser state to the input
          state.onState("user", (user) => {
              this.dom.value = user;
          });
          // Render new username suggestions
          state.on("userSuggestions", suggList => {
              // Show suggestion box
              this.setState("suggestionsVisible", true);
              if (suggList) {
                  suggestions.innerHTML = "";
                  const frag = jSh.docFrag();
                  if (suggList.length) {
                      for (const suggestion of suggList) {
                          frag.appendChild(jSh.d({
                              sel: ".ihfath-user-suggestion",
                              child: jSh.c("span", {
                                  text: suggestion
                              }),
                              prop: {
                                  ihfathSuggestion: suggestion
                              }
                          }));
                      }
                      // Add suggestions
                      suggestions.appendChild(frag);
                  }
                  else {
                      this.setState("suggestionsVisible", false);
                  }
              }
          });
          // Toggle suggestions box visibility
          this.onState("suggestionsVisible", (visible) => {
              if (visible) {
                  this.domSuggestions.classList.remove("ihfath-hidden");
              }
              else {
                  this.domSuggestions.classList.add("ihfath-hidden");
              }
          });
          // Hide suggestions when input is blurred
          dom.addEventListener("blur", () => {
              this.timeout("hideSuggestionsOnBlur", 1000).then(() => {
                  this.setState("suggestionsVisible", false);
              });
          });
          // Cancel suggestion box hiding when refocused
          dom.addEventListener("focus", () => {
              this.clearTimeout("hideSuggestionsOnBlur");
          });
          // Set the user to the clicked suggestion
          suggestions.addEventListener("click", evt => {
              let target = evt.target;
              let suggestion = null;
              while (target !== suggestions) {
                  if ("ihfathSuggestion" in target) {
                      suggestion = target.ihfathSuggestion;
                      break;
                  }
                  target = target.parentNode;
              }
              if (suggestion !== null) {
                  state.setState("user", suggestion);
                  // Hide suggestion box
                  this.setState("suggestionsVisible", false);
              }
          });
          // Toggle change btn visibility
          state.onState("userActive", userActive => {
              if (userActive) {
                  changeBtn.classList.remove("ihfath-hidden");
              }
              else {
                  changeBtn.classList.add("ihfath-hidden");
              }
          });
          // Disable userActive state when changeBtn is clicked
          changeBtn.addEventListener("click", function (e) {
              state.setState("userActive", false);
              state.setState("userData", null);
              state.setState("user", "");
              that.dom.focus();
              e.preventDefault();
          });
      }
      addActions(actions) {
          const actionWrap = jSh(this.domActions);
          for (const action of Object.keys(actions)) {
              const actionCallbacks = actions[action];
              const className = "ihfath-" + action.replace(/([A-Z])/g, "-$1").toLowerCase();
              const setup = actionCallbacks.setup;
              const callback = actionCallbacks.click;
              const actionDom = actionWrap.jSh("." + className)[0];
              const userActionInst = new UserAction(actionDom, false, actionCallbacks);
              if (setup) {
                  setup(userActionInst);
              }
              if (callback) {
                  actionDom.addEventListener("click", callback);
              }
              // Save to user actions
              this.userActions[action] = userActionInst;
          }
      }
  }
  class UserAction extends Component {
      constructor(dom, visible = false, callbacks) {
          super();
          this.dom = dom;
          this.callbacks = callbacks;
          this.newState("visible", visible);
          this.onState("visible", visible => {
              if (visible) {
                  this.dom.classList.remove("ihfath-hidden");
              }
              else {
                  this.dom.classList.add("ihfath-hidden");
              }
          });
      }
  }
  //# sourceMappingURL=userfield.js.map

  const locale = {};
  function updateLocale(localeSrc) {
      Object.assign(locale, localeSrc);
  }
  //# sourceMappingURL=locale.js.map

  class UserSummary extends Component {
      constructor(dom) {
          super();
          this.dom = dom;
          this.newState("data", null);
          this.newState("visible", false);
          // Event listeners
          this.onState("visible", visible => {
              if (visible) {
                  this.dom.classList.remove("ihfath-hidden");
              }
              else {
                  this.dom.classList.add("ihfath-hidden");
              }
          });
          // Populate with data entries
          this.onState("data", data => {
              const dom = this.dom;
              if (data) {
                  // Clear current data table (if any)
                  dom.innerHTML = "";
                  const titles = Object.keys(data);
                  const frag = jSh.docFrag();
                  for (const title of titles) {
                      const body = data[title];
                      frag.appendChild(jSh.d({
                          sel: ".ihfath-data",
                          child: [
                              jSh.d(".ihfath-title", title),
                              jSh.d(".ihfath-body", null, body),
                          ],
                      }));
                  }
                  dom.appendChild(frag);
              }
              else {
                  dom.appendChild(jSh.d({
                      sel: ".ihfath-data",
                      child: [
                          jSh.d(".ihfath-body", null, locale.noData),
                      ],
                  }));
              }
          });
      }
  }
  //# sourceMappingURL=usersummary.js.map

  class LessonSummaryHover extends Component {
      constructor(lesson, manager, dom = jSh.d(".ihfath-lesson-summary")) {
          super();
          this.lesson = lesson;
          this.manager = manager;
          this.dom = dom;
          this.build();
          this.destroyed = false;
      }
      build() {
      }
      destroy() {
          if (!this.destroyed) {
              this.dom.parentNode.removeChild(this.dom);
              this.manager.destroy(this.lesson.lid);
              this.destroyed = true;
          }
      }
      destroyTimeout() {
          this.timeout("destroy", 750).then(stub => this.destroy());
      }
      clearDestroyTimeout() {
          this.clearTimeout("destroy");
      }
  }
  class LessonSummaryManager extends Component {
      constructor() {
          super();
      }
      hover(lesson) {
          const hover = this.lessons[lesson.lid]
              || (this.lessons[lesson.lid] = new LessonSummaryHover(lesson, this));
          hover.clearTimeout("destroy");
          return hover;
      }
      destroy(lid) {
          this.lessons[lid] = undefined;
      }
  }
  //# sourceMappingURL=lessonsummary.js.map

  // Ihfath core user roles, maps to Ihfath Core User Role definitions
  var UserRoles;
  (function (UserRoles) {
      UserRoles[UserRoles["Admin"] = 1] = "Admin";
      UserRoles[UserRoles["Professor"] = 2] = "Professor";
      UserRoles[UserRoles["Student"] = 3] = "Student";
  })(UserRoles || (UserRoles = {}));
  function userToUser(userData) {
      let TypeMap;
      (function (TypeMap) {
          TypeMap[TypeMap["Number"] = 0] = "Number";
          TypeMap[TypeMap["Timestamp"] = 1] = "Timestamp";
          TypeMap[TypeMap["String"] = 2] = "String";
          TypeMap[TypeMap["FieldString"] = 3] = "FieldString";
          TypeMap[TypeMap["FieldRole"] = 4] = "FieldRole";
      })(TypeMap || (TypeMap = {}));
      const basicExternalKeys = {
          uid: TypeMap.Number,
          name: TypeMap.String,
          role: TypeMap.FieldRole,
          mail: TypeMap.String,
          created: TypeMap.Timestamp,
          access: TypeMap.Timestamp,
          login: TypeMap.Timestamp,
          timezone: TypeMap.String,
          timezoneOffset: TypeMap.Number,
          language: TypeMap.String,
          gender: TypeMap.FieldString,
          firstName: TypeMap.FieldString,
          lastName: TypeMap.FieldString,
          realName: TypeMap.FieldString,
      };
      const externalRemap = {
          roles: "role",
          field_ihfath_gender: "gender",
          field_ihfath_realname: "realName",
          field_ihfath_firstname: "firstName",
          field_ihfath_lastname: "lastName",
          timezone_offset: "timezoneOffset",
      };
      // Copy/map basic properties from server user data to User shaped object
      const userBase = {};
      for (const key of Object.keys(userData)) {
          const remappedKey = key in externalRemap ? externalRemap[key] : key;
          switch (basicExternalKeys[remappedKey]) {
              case TypeMap.String:
                  userBase[remappedKey] = userData[key];
                  break;
              case TypeMap.Number:
                  userBase[remappedKey] = parseInt(userData[key], 10);
                  break;
              case TypeMap.Timestamp:
                  userBase[remappedKey] = parseInt(userData[key], 10) * 1000;
                  break;
              case TypeMap.FieldString:
                  userBase[remappedKey] = userData[key]["und"] ? userData[key]["und"][0]["value"] : null;
                  break;
              case TypeMap.FieldRole:
                  const roles = userData[key];
                  // Set the user's role from the user's data role object
                  roleLoop: for (const key of Object.keys(roles)) {
                      switch (roles[key]) {
                          case "administrator":
                              userBase.role = UserRoles.Admin;
                              break roleLoop;
                          case "professor":
                              userBase.role = UserRoles.Professor;
                              break roleLoop;
                          case "student":
                              userBase.role = UserRoles.Student;
                              break roleLoop;
                      }
                  }
                  break;
          }
      }
      if (!userBase.firstName && !userBase.lastName) {
          userBase.firstName = "";
          userBase.lastName = "";
          userBase.realName = "";
      }
      else {
          userBase.realName = ((userBase.firstName || "") + " " + (userBase.lastName || "")).trim();
      }
      return userBase;
  }
  function userSummaryPrettyMap(user) {
      let TypeRenderMap;
      (function (TypeRenderMap) {
          TypeRenderMap[TypeRenderMap["Normal"] = 0] = "Normal";
          TypeRenderMap[TypeRenderMap["NormalCapital"] = 1] = "NormalCapital";
          TypeRenderMap[TypeRenderMap["Date"] = 2] = "Date";
          TypeRenderMap[TypeRenderMap["Role"] = 3] = "Role";
          TypeRenderMap[TypeRenderMap["Timezone"] = 4] = "Timezone";
      })(TypeRenderMap || (TypeRenderMap = {}));
      const userFields = {
          realName: {
              field: "userRealName",
              type: TypeRenderMap.Normal,
          },
          gender: {
              field: "userGender",
              type: TypeRenderMap.NormalCapital,
          },
          mail: {
              field: "userMail",
              type: TypeRenderMap.Normal,
          },
          login: {
              field: "userLastLogin",
              type: TypeRenderMap.Date,
          },
          created: {
              field: "userCreated",
              type: TypeRenderMap.Date,
          },
          role: {
              field: "userRole",
              type: TypeRenderMap.Role,
          },
          timezone: {
              field: "userTimezone",
              type: TypeRenderMap.Timezone,
          },
      };
      const userSummary = {};
      const roleMap = {
          [UserRoles.Admin]: locale.roleAdmin,
          [UserRoles.Professor]: locale.roleProfessor,
          [UserRoles.Student]: locale.roleStudent,
      };
      let fieldWrapperCallback;
      const fieldWrapper = jSh.t;
      const fieldEmWrapper = (text) => jSh.c("i", {
          text: text
      });
      for (const field of Object.keys(userFields)) {
          const fieldData = userFields[field];
          let fieldValue = user[field];
          if (fieldValue === null) {
              fieldValue = "Empty";
              fieldWrapperCallback = fieldEmWrapper;
          }
          else {
              fieldWrapperCallback = fieldWrapper;
          }
          const fieldTitle = locale[fieldData.field];
          switch (fieldData.type) {
              case TypeRenderMap.Normal:
                  userSummary[fieldTitle] = [
                      fieldWrapperCallback(fieldValue)
                  ];
                  break;
              case TypeRenderMap.NormalCapital:
                  userSummary[fieldTitle] = [
                      fieldWrapperCallback(jSh.strCapitalize(fieldValue))
                  ];
                  break;
              case TypeRenderMap.Date:
                  userSummary[fieldTitle] = [
                      fieldWrapperCallback(new Date(fieldValue || new Date().getTime()).toLocaleString())
                  ];
                  break;
              case TypeRenderMap.Role:
                  userSummary[fieldTitle] = [
                      fieldWrapperCallback(roleMap[fieldValue])
                  ];
                  break;
              case TypeRenderMap.Timezone:
                  userSummary[fieldTitle] = [
                      fieldWrapperCallback(fieldValue),
                      fieldEmWrapper(` (UTC${user.timezoneOffset})`),
                  ];
                  break;
          }
      }
      return userSummary;
  }
  function lessonToLesson(lessonData) {
      let TypeRenderMap;
      (function (TypeRenderMap) {
          TypeRenderMap[TypeRenderMap["Number"] = 0] = "Number";
          TypeRenderMap[TypeRenderMap["NumberTime"] = 1] = "NumberTime";
          TypeRenderMap[TypeRenderMap["String"] = 2] = "String";
          TypeRenderMap[TypeRenderMap["Boolean"] = 3] = "Boolean";
          TypeRenderMap[TypeRenderMap["Depth"] = 4] = "Depth";
      })(TypeRenderMap || (TypeRenderMap = {}));
      // Partial name remapping as needed
      const lessonKeyRemapping = {
          seriesWeektimeId: "series_weektime_id",
          notesProfessor: "notes_professor",
          notesStudent: "notes_student",
          ratingProfessor: "rating_professor",
          ratingStudent: "rating_student",
          startDate: "start_date",
          paidDuration: "paid_duration",
          orderId: "order_id",
          profStart: "prof_start",
          profDur: "prof_dur",
          profLast: "prof_last",
          studentStart: "student_start",
          studentDur: "student_dur",
          studentLast: "student_last",
          substitutedBy: "substituted_by",
          typeName: "type_name",
          typeData: "type_data",
          programName: "program_name",
          programData: "program_data",
      };
      const lessonKeys = {
          lid: TypeRenderMap.Number,
          type: TypeRenderMap.Number,
          series: TypeRenderMap.Number,
          seriesWeektimeId: TypeRenderMap.Number,
          program: TypeRenderMap.Number,
          puid: TypeRenderMap.Number,
          suid: TypeRenderMap.Number,
          orderId: TypeRenderMap.Number,
          notesProfessor: TypeRenderMap.String,
          notesStudent: TypeRenderMap.String,
          ratingProfessor: TypeRenderMap.Number,
          ratingStudent: TypeRenderMap.Number,
          startDate: TypeRenderMap.Number,
          paidDuration: TypeRenderMap.Number,
          duration: TypeRenderMap.Number,
          // FIXME: *Start/Last should also be NumberTime
          profStart: TypeRenderMap.Number,
          profDur: TypeRenderMap.NumberTime,
          profLast: TypeRenderMap.Number,
          studentStart: TypeRenderMap.Number,
          studentDur: TypeRenderMap.NumberTime,
          studentLast: TypeRenderMap.Number,
          penalty: TypeRenderMap.Number,
          cancelled: TypeRenderMap.Boolean,
          deleted: TypeRenderMap.Boolean,
          substitutedBy: TypeRenderMap.Number,
          weektime: TypeRenderMap.Number,
          typeName: TypeRenderMap.String,
          typeData: TypeRenderMap.Depth,
          programName: TypeRenderMap.String,
          programData: TypeRenderMap.Depth,
      };
      const lessonTypeKeys = {
          tid: TypeRenderMap.Number,
          available: TypeRenderMap.Boolean,
          deleted: TypeRenderMap.Boolean,
          weight: TypeRenderMap.Number,
          data: TypeRenderMap.Depth,
      };
      const lessonProgramKeys = {
          pid: TypeRenderMap.Number,
          available: TypeRenderMap.Boolean,
          deleted: TypeRenderMap.Boolean,
          weight: TypeRenderMap.Number,
          data: TypeRenderMap.Depth,
      };
      const lessonMetaDataValueKeys = {
          name: TypeRenderMap.String,
          description: TypeRenderMap.String,
      };
      const lessonMetaDataKeys = {
          en: TypeRenderMap.Depth,
          fr: TypeRenderMap.Depth,
      };
      const depthMap = {
          typeData: lessonTypeKeys,
          programData: lessonProgramKeys,
          data: lessonMetaDataKeys,
          en: lessonMetaDataValueKeys,
          fr: lessonMetaDataValueKeys,
      };
      function remap(mapping, source, dest, isLesson) {
          for (const key of Object.keys(mapping)) {
              const oldKey = isLesson && key in lessonKeyRemapping ? lessonKeyRemapping[key] : key;
              const type = mapping[key];
              let value;
              switch (type) {
                  case TypeRenderMap.Number:
                      value = parseFloat(source[oldKey]);
                      break;
                  case TypeRenderMap.NumberTime:
                      value = parseFloat(source[oldKey]) * 1000;
                      break;
                  case TypeRenderMap.String:
                      if (!source) {
                          console.error("LESSON ERR", mapping, dest);
                      }
                      value = source[oldKey];
                      break;
                  case TypeRenderMap.Boolean:
                      value = !!parseInt(source[oldKey], 10);
                      break;
                  case TypeRenderMap.Depth:
                      const depthMapping = depthMap[key];
                      value = oldKey in source ? remap(depthMapping, source[oldKey], {}, false) : {};
                      break;
              }
              dest[key] = value;
          }
          return dest;
      }
      return remap(lessonKeys, lessonData, {}, true);
  }
  //# sourceMappingURL=model.js.map

  // Add cancelation to promises
  const PromiseProxy = Promise;
  PromiseProxy.prototype.CANCEL_TOKEN = {};
  PromiseProxy.CANCEL_TOKEN = PromiseProxy.prototype.CANCEL_TOKEN;
  PromiseProxy.prototype.persistentThen = Promise.prototype.then;
  PromiseProxy.prototype.then = function (onfulfilled, onrejected) {
      let promiseResolve = null;
      let promiseReject = null;
      this.persistentThen(function (value) {
          if (value !== PromiseProxy.CANCEL_TOKEN) {
              if (typeof onfulfilled === "function") {
                  promiseResolve(onfulfilled(value));
              }
              else {
                  promiseResolve(value);
              }
          }
      }, function (reason) {
          if (typeof onrejected === "function") {
              promiseReject(onrejected(reason));
          }
          else {
              promiseReject(reason);
          }
      });
      return new Promise(function (resolve, reject) {
          promiseResolve = resolve;
          promiseReject = reject;
      });
  };
  Promise.prototype.catch;
  // Mark this as a module
  const stub = PromiseProxy;
  //# sourceMappingURL=index.js.map

  const requestMap = {};
  class Request {
      constructor(type, params) {
          const that = this;
          const success = params.success;
          const fail = params.fail;
          // Remove callbacks
          params.success = undefined;
          params.fail = undefined;
          this.type = type;
          this.completed = false;
          this.canceled = false;
          this.onCancel = params.cancel;
          this.data = null;
          const xhr = request(params);
          this.xhr = (xhr.then(function (data) {
              that.completed = true;
              if (!that.canceled) {
                  if (success) {
                      success && success.call(that, data);
                  }
              }
              else {
                  return stub.CANCEL_TOKEN;
              }
              return data;
          }).catch(function (e) {
              that.completed = true;
              if (!that.canceled && fail) {
                  fail.call(that);
              }
              throw e;
          }));
          // Ensure the xhr object is exposed
          this.xhr.xhr = xhr.xhr;
      }
      cancel() {
          if (!this.completed) {
              if (this.onCancel) {
                  this.onCancel();
              }
              return this.canceled = true;
          }
          else {
              return false;
          }
      }
  }
  const api = {
      url: "",
      base: "",
  };
  function setApiUrl(base, url) {
      api.base = base;
      api.url = url;
  }
  class RequestTypes {
      static userFind(name, param) {
          return new Request("userFind", {
              url: api.base + "/user/autocomplete/" + name,
              success: param && param.success,
              json: true,
          });
      }
      static user(name, param) {
          return new Request("user", {
              method: "POST",
              url: api.url + "/userInfo",
              success: param && param.success,
              formData: {
                  name: name,
              },
              json: true,
          });
      }
      static userRelations(uid, param) {
          return new Request("userRelations", {
              method: "POST",
              url: api.url + "/userRelations",
              success: param && param.success,
              formData: {
                  uid: uid,
              },
              json: true,
          });
      }
      static userMonth(uid, month, year, param) {
          return new Request("userMonth", {
              method: "POST",
              url: api.url + "/userMonth",
              success: param && param.success,
              formData: {
                  uid,
                  month,
                  year,
              },
              json: true,
          });
      }
      static userWeek(uid, day, month, year, param) {
          return new Request("userWeek", {
              method: "POST",
              url: api.url + "/userWeek",
              success: param && param.success,
              formData: {
                  uid,
                  day,
                  month,
                  year,
              },
              json: true,
          });
      }
      static entityManipulate(aspect, action, entityId, weektime, uid, param) {
          const formData = {
              aspect,
              action,
              entityId,
              uid,
          };
          if (weektime) {
              formData.weektime = weektime;
          }
          return new Request("entityManipulate", {
              method: "POST",
              url: api.url + "/entityManipulate",
              success: param && param.success,
              formData,
              json: true,
          });
      }
  }
  var EntityAspect;
  (function (EntityAspect) {
      EntityAspect["Lesson"] = "lesson";
      EntityAspect["Series"] = "series";
  })(EntityAspect || (EntityAspect = {}));
  var EntityAction;
  (function (EntityAction) {
      EntityAction["Move"] = "move";
      EntityAction["Delete"] = "delete";
  })(EntityAction || (EntityAction = {}));
  class RequestManager {
      static getRequest(name) {
          return requestMap[name] || null;
      }
      static setRequest(request$$1) {
          return requestMap[request$$1.type] = request$$1;
      }
      static request(request$$1) {
          const curRequest = this.getRequest(request$$1.type);
          if (curRequest) {
              curRequest.cancel();
          }
          return this.setRequest(request$$1);
      }
      static cancel(request$$1) {
          const curRequest = this.getRequest(request$$1);
          if (curRequest) {
              curRequest.cancel();
          }
      }
      static get actions() {
          return RequestTypes;
      }
  }
  //# sourceMappingURL=req.js.map

  function humanTime(ms) {
      const hourMs = 1000 * 60 * 60;
      const minMs = 1000 * 60;
      const secMs = 1000;
      const hours = Math.floor(ms / hourMs);
      const mins = Math.floor((ms - (hourMs * hours)) / minMs);
      const secs = Math.floor((ms - (hourMs * hours) - (minMs * mins)) / secMs);
      const padd = (v, len = 2) => ("" + v).padStart(len, "0");
      return `${padd(hours)}:${padd(mins)}:${padd(secs)}`;
  }

  class Notifi extends Component {
      constructor(templ, config, defaultValues) {
          super();
          this.templ = templ;
          this.config = config;
          const that = this;
          // Add filters (if any)
          this.filters = config.filters || {};
          this.position = this.normalizePos(config.position) || [0, 0];
          this.anchor = config.anchor || null;
          // Add initial state
          this.newState("visible", this.config.visible === undefined ? true : this.config.visible);
          this.newState("position", this.position);
          this.newState("anchor", this.anchor);
          // Apply default values (if any)
          if (defaultValues) {
              for (const key of Object.keys(defaultValues)) {
                  this.newState(key, defaultValues[key]);
              }
          }
          // Build template
          this.dom = templ.build(templ, this);
          this.dom.classList.add("notifi-no-animation");
          this.dom.style.transformOrigin = `${Math.min(Math.max(this.position[0] * 100, 0), 100)}% ${(100 - Math.min(Math.max(this.position[1] * 100, 0), 100))}%`;
          if (config.positionFixed) {
              this.dom.classList.add("notifi-pos-fixed");
          }
          // Bind dom to vsible state
          this.onState("visible", function toggleVisibility(visible) {
              if (visible && !that.boundRect) {
                  // Get boundRect _then_ render
                  that.boundRect = {
                      width: that.dom.offsetWidth,
                      height: that.dom.offsetHeight,
                      top: that.dom.offsetTop,
                      left: that.dom.offsetLeft,
                      right: 0,
                      bottom: 0,
                  };
                  console.log("RECT", that.boundRect);
              }
              that.dom.classList.remove("notifi-no-animation");
              that.timeout("render", 0, false, true).then(v => {
                  if (visible) {
                      that.dom.classList.remove("notifi-container-hidden");
                      that.timeout("render", 0, false, true).then(v => {
                          that.render(true);
                      });
                      if (that.config.blurClose) {
                          that.blurClose();
                      }
                  }
                  else {
                      that.dom.classList.add("notifi-container-hidden");
                  }
              });
          });
          // Append to window
          document.body.appendChild(this.dom);
          // Render
          if (this.getState("visible")) {
              this.timeout("render", 0, false, true).then(v => {
                  this.render();
              });
              if (this.config.blurClose) {
                  this.blurClose();
              }
          }
          else {
              // Hide initially
              this.dom.classList.add("notifi-container-hidden");
          }
          // Bind anchor state
          this.onState("anchor", anchor => {
              this.anchor = anchor;
              if (this.getState("visible")) {
                  this.timeout("render", 0, false, true).then(v => {
                      this.render(true);
                  });
              }
          });
      }
      render(maintainSize = false) {
          if (!this.dom) {
              return;
          }
          const pos = this.position;
          let anchorBox = {
              left: 0,
              top: 0,
              right: window.innerWidth,
              bottom: window.innerHeight,
              width: window.innerWidth,
              height: window.innerHeight,
          };
          if (this.anchor) {
              // Position relative to the anchor element
              if (this.anchor !== document.body) {
                  anchorBox = this.anchor.getBoundingClientRect();
              }
              // TODO: Check how to apply this
              // if (!("notifiAnchorIdMapping" in <any>this.config.anchor)) {
              //   anchorId = `${ anchors.length }:${ this.config.anchor.tagName.toLowerCase() }:${ pos[0] }:${ pos[1] }`;
              //   (<any>this.config.anchor).notifiAnchorIdMapping = {
              //     [anchorId]: [],
              //     _index: anchors.length,
              //   };
              //
              //   anchors.push(null);
              // } else {
              //   anchorId = `${ (<any>this.config.anchor).notifiAnchorIdMapping._index }:${ this.config.anchor.tagName.toLowerCase() }:${ pos[0] }:${ pos[1] }`;
              //   anchorId = (<any>this.config.anchor).notifiAnchorId;
              // }
              //
              // if (!anchorMap[anchorId]) {
              //   anchorMap[anchorId] = [];
              //   // FIXME: Finish this
              // }
          }
          // Add repositioning class
          this.dom.classList.add("notifi-repos-base");
          this.timeout("render", 10, false, true).then(v => {
              let notifiBox = null;
              if (maintainSize && this.boundRect) {
                  notifiBox = this.boundRect;
              }
              else {
                  // notifiBox = this.boundRect = this.dom.getBoundingClientRect();
                  notifiBox = this.boundRect = {
                      width: this.dom.offsetWidth,
                      height: this.dom.offsetHeight,
                      top: this.dom.offsetTop,
                      left: this.dom.offsetLeft,
                      right: 0,
                      bottom: 0,
                  };
              }
              const width = notifiBox.width;
              const height = notifiBox.height;
              const margin = this.config.anchorMargin || [0, 0];
              const posFixed = this.config.positionFixed;
              const isBody = this.anchor === document.body;
              const posXBase = pos[0];
              const posYBase = pos[1];
              let endX = 0;
              let endY = 0;
              if (posXBase > 0 && posXBase < 1) {
                  const begin = anchorBox.left;
                  const end = anchorBox.right - width;
                  endX = begin + ((end - begin) * posXBase);
              }
              else if (posXBase <= 0) {
                  const XBase = 1 + posXBase;
                  const begin = anchorBox.left - width;
                  const end = anchorBox.left;
                  endX = begin + ((end - begin) * XBase) - margin[0];
              }
              else if (posXBase >= 1) {
                  const XBase = posXBase - 1;
                  const begin = anchorBox.right - width;
                  const end = anchorBox.right;
                  endX = begin + ((end - begin) * XBase) + margin[0];
              }
              if (posYBase > 0 && posYBase < 1) {
                  const begin = anchorBox.top;
                  const end = anchorBox.bottom - height;
                  endY = begin + ((end - begin) * posYBase);
              }
              else if (posYBase <= 0) {
                  const YBase = 1 + posYBase;
                  const begin = anchorBox.top - height;
                  const end = anchorBox.top;
                  endY = begin + ((end - begin) * YBase) - margin[1];
              }
              else if (posYBase >= 1) {
                  const YBase = posYBase - 1;
                  const begin = anchorBox.bottom - height;
                  const end = anchorBox.bottom;
                  endY = begin + ((end - begin) * YBase) + margin[1];
              }
              // Apply offsets
              this.dom.style.left = endX + "px";
              if (isBody) {
                  if (posYBase >= 0.5) {
                      this.dom.style.top = "unset";
                      this.dom.style.bottom = (innerHeight - (endY + (posFixed ? 0 : scrollY)) - height) + "px";
                  }
                  else {
                      this.dom.style.bottom = "unset";
                      this.dom.style.top = (endY + (posFixed ? 0 : scrollY)) + "px";
                  }
              }
              else {
                  this.dom.style.top = (endY + (posFixed ? 0 : scrollY)) + "px";
              }
              this.dom.classList.remove("notifi-repos-base");
          });
      }
      normalizePos(pos) {
          return pos.map((pos, isY) => {
              if (typeof pos === "string") {
                  return ({
                      top: 0,
                      bottom: 1,
                      left: 0,
                      right: 1,
                      center: 0.5,
                  })[pos];
              }
              else {
                  return pos;
              }
          });
      }
      blurClose() {
          const that = this;
          window.addEventListener("click", function listener(e) {
              let target = e.target;
              let close = true;
              while (target !== document.body) {
                  if (target === that.dom) {
                      close = false;
                      break;
                  }
                  target = target.parentNode;
              }
              if (close) {
                  that.close();
              }
              window.removeEventListener("click", listener);
          });
      }
      open(delay) {
          this.setState("visible", true);
          if (delay) {
              this.timeout("close", delay).then(v => {
                  this.close();
              });
          }
      }
      close() {
          this.setState("visible", false);
      }
      getById(id) {
          return this.idMaps[id];
      }
      addIdMapping(id, dom) {
          this.idMaps[id] = dom;
      }
  }
  var NotifiType;
  (function (NotifiType) {
      NotifiType[NotifiType["Modal"] = 0] = "Modal";
      NotifiType[NotifiType["Minimal"] = 1] = "Minimal";
      // UI controls
      NotifiType[NotifiType["TextInput"] = 2] = "TextInput";
      NotifiType[NotifiType["Button"] = 3] = "Button";
      NotifiType[NotifiType["Text"] = 4] = "Text";
      NotifiType[NotifiType["HBox"] = 5] = "HBox";
      NotifiType[NotifiType["VBox"] = 6] = "VBox";
      NotifiType[NotifiType["Separator"] = 7] = "Separator";
      NotifiType[NotifiType["ArbitraryControls"] = 8] = "ArbitraryControls";
  })(NotifiType || (NotifiType = {}));
  class NotifiTextTemplate extends Component {
      constructor(input, context, update, async = true) {
          super();
          this.input = input;
          this.context = context;
          const that = this;
          const filters = context.filters;
          const { template, refs, ctxNames, ctxMeta } = this.parse(input);
          this.templateFilters = {};
          this.template = template;
          this.refs = refs;
          this.render = [];
          function render() {
              that.render = that.template.map((token, index) => {
                  return (that.templateFilters[index] || (s => jSh.t(s)))(token + "");
              });
          }
          // Bind ctx
          ctxNames.forEach((name, index) => {
              const filterName = ctxMeta[index];
              const filter = filterName && filters[filterName] || (input => jSh.t(input));
              // Save our filter function
              this.templateFilters[this.refs[index]] = filter;
              context.onState(name, value => {
                  this.template[this.refs[index]] = value;
                  render();
                  if (async) {
                      // Delegate to the end to avoid running update callback redundantly
                      this.timeout("update", 0).then(v => {
                          update(this.render);
                      });
                  }
                  else {
                      update(this.render);
                  }
              });
              // Set to initial value
              this.template[this.refs[index]] = context.getState(name) + "";
          });
          // Update
          render();
          update(this.render);
      }
      parse(input) {
          const template = [];
          const refs = [];
          const ctxNames = [];
          const ctxMeta = [];
          const regIdentifier = /[a-z\d_]/i;
          let curToken = "";
          let curTokenMeta = "";
          let inRef = false;
          let nameStart = false;
          mainLoop: for (let i = 0; i < input.length; i++) {
              const char = input[i];
              const nextChar = input[i + 1];
              if (inRef) {
                  if (nameStart) {
                      if (!regIdentifier.test(char)) {
                          i--;
                          nameStart = false;
                      }
                      else {
                          curToken += char;
                      }
                  }
                  else {
                      if (curToken && char === ":") {
                          let metaBegun = false;
                          let curChar = char;
                          // Get token meta
                          for (;; i++) {
                              curChar = input[i];
                              if (metaBegun) {
                                  if (regIdentifier.test(curChar)) {
                                      curTokenMeta += curChar;
                                  }
                                  else {
                                      continue mainLoop;
                                  }
                              }
                              else {
                                  if (regIdentifier.test(curChar)) {
                                      i--;
                                      metaBegun = true;
                                  }
                              }
                          }
                      }
                      else if (char === "}" && nextChar === "}") {
                          i++;
                          refs.push(template.length);
                          template.push(ctxNames.length);
                          ctxNames.push(curToken);
                          ctxMeta.push(curTokenMeta);
                          curToken = "";
                          curTokenMeta = "";
                          inRef = false;
                      }
                      if (regIdentifier.test(char)) {
                          i--;
                          nameStart = true;
                      }
                  }
              }
              else {
                  if (char === "{" && nextChar === "{") {
                      i++;
                      template.push(curToken);
                      curToken = "";
                      inRef = true;
                  }
                  else {
                      curToken += char;
                  }
              }
          }
          if (curToken) {
              if (inRef) ;
              else {
                  template.push(curToken);
              }
          }
          return { template, refs, ctxNames, ctxMeta };
      }
  }
  class NotifiTemplate {
      constructor(config, parent) {
          this.config = config;
          this.children = [];
          if (config.children) {
              for (const child of config.children) {
                  this.children.push(new NotifiTemplate(child));
              }
          }
      }
      build(template, context) {
          const config = template.config;
          const domBase = jSh.d(".notifi-control");
          let domCtrl = null;
          let traverseChildren = false;
          let reprocessChildren = null;
          switch (config.type) {
              case NotifiType.Modal:
                  // Do nothing, these are just shell types
                  traverseChildren = true;
                  if (context.config.width) {
                      domBase.style.width = context.config.width + "px";
                  }
                  domBase.classList.add("notifi-modal");
                  break;
              case NotifiType.Minimal:
                  // Do nothing, these are just shell types
                  traverseChildren = true;
                  if (context.config.width) {
                      domBase.style.width = context.config.width + "px";
                  }
                  domBase.classList.add("notifi-minimal");
                  break;
              case NotifiType.TextInput:
                  const inputConfig = {
                      sel: ".notifi-textinput",
                      prop: {
                          type: "text",
                      },
                      events: {
                      // input() {
                      //
                      // }
                      },
                  };
                  if (config.placeholder) {
                      inputConfig.prop.placeholder = config.placeholder;
                  }
                  if (config.value) {
                      inputConfig.events.input = function () {
                          context.setState(config.value, this.value);
                      };
                      context.onState(config.value, value => {
                          if (domCtrl.value !== value) {
                              domCtrl.value = value;
                          }
                      });
                  }
                  domCtrl = jSh.c("input", inputConfig);
                  break;
              case NotifiType.Button:
                  domCtrl = jSh.c("button", {
                      sel: ".notifi-button",
                  });
                  if (config.text) {
                      new NotifiTextTemplate(config.text, context, contents => {
                          domCtrl.innerHTML = "";
                          domCtrl.appendChild(contents);
                          // Update parent's position
                          context.render();
                      });
                  }
                  if (config.click) {
                      domCtrl.addEventListener("click", () => {
                          config.click.call(context);
                      });
                  }
                  break;
              case NotifiType.Text:
                  domCtrl = jSh.d(".notifi-text");
                  if (config.text) {
                      new NotifiTextTemplate(config.text, context, contents => {
                          domCtrl.innerHTML = "";
                          domCtrl.appendChild(contents);
                          // Update parent's position
                          context.render();
                      });
                  }
                  break;
              case NotifiType.HBox:
                  // Do nothing, these are just shell types
                  traverseChildren = true;
                  domBase.classList.add("notifi-hbox");
                  // Recalculate sizes
                  reprocessChildren = children => {
                      let fullSize = 12;
                      let sharingChildren = 0;
                      // See how much space was already taken
                      for (const child of children) {
                          const childConfig = child.config;
                          // Subtract from the overall size if the child has a size defined
                          if (childConfig.size) {
                              fullSize -= childConfig.size;
                              childConfig._computedSize = childConfig.size;
                          }
                          else {
                              sharingChildren++;
                              childConfig._computedSize = 0;
                          }
                      }
                      // Distribute the remaining unclaimed space to the rest of the
                      // children
                      const sharedFraction = fullSize / sharingChildren;
                      for (const child of children) {
                          const childConfig = child.config;
                          if (!childConfig._computedSize) {
                              childConfig._computedSize = sharedFraction;
                          }
                      }
                  };
                  break;
              case NotifiType.VBox:
                  // Do nothing, these are just shell types
                  traverseChildren = true;
                  domBase.classList.add("notifi-vbox");
                  break;
              case NotifiType.Separator:
                  domBase.classList.add("notifi-separator-base");
                  domCtrl = jSh.d(".notifi-separator");
                  break;
              case NotifiType.ArbitraryControls:
                  // Do nothing, these are just shell types
                  traverseChildren = true;
                  if (config.id) {
                      context.addIdMapping(config.id, domBase);
                  }
                  break;
          }
          if (domCtrl) {
              domCtrl.classList.add("notifi-control-element");
              domBase.appendChild(domCtrl);
          }
          if (config._computedSize) {
              domBase.style.width = (100 * (config._computedSize / 12)) + "%";
          }
          if (config.visible) {
              context.onState(config.visible, visible => {
                  if (visible) {
                      domBase.classList.remove("notifi-hidden");
                  }
                  else {
                      domBase.classList.add("notifi-hidden");
                  }
                  // Update parent's position
                  context.render();
              });
              if (!context.getState(config.visible)) {
                  domBase.classList.add("notifi-hidden");
              }
          }
          if (traverseChildren && template.children) {
              if (reprocessChildren) {
                  reprocessChildren(template.children);
              }
              for (const child of template.children) {
                  const childType = child.config.type;
                  // Prevent modal and minimal modal template types to be treated as children
                  if (childType !== NotifiType.Modal && childType !== NotifiType.Minimal) {
                      domBase.appendChild(this.build(child, context));
                  }
              }
          }
          return domBase;
      }
  }
  class NotifiUI {
      // Stuff
      static new(templ, config, defaultValues) {
          return new Notifi(templ, config, defaultValues);
      }
      static msg(msg, options) {
          let margin = options.anchorMargin || [0, 0];
          // Position (internally) relative to <body> with no anchor
          if (!options.anchor && !options.anchorMargin) {
              margin = [-15, -15];
          }
          options.positionFixed = jSh.boolOp(options.positionFixed, !options.anchor);
          options.position = options.position || ["left", "bottom"];
          options.anchorMargin = margin;
          options.anchor = options.anchor || document.body;
          const templConfig = {
              type: NotifiType.Minimal,
              children: [{
                      type: NotifiType.VBox,
                      children: [{
                              type: NotifiType.Text,
                              text: msg,
                          }],
                  }]
          };
          if (options.controls) {
              const hbox = {
                  type: NotifiType.HBox,
                  children: [],
              };
              for (const button of options.controls) {
                  hbox.children.push(button);
              }
              templConfig.children[0].children.push(hbox);
          }
          const templ = NotifiUI.template(templConfig);
          const defaultValues = options.defaultValues || undefined;
          return NotifiUI.new(templ, options, defaultValues);
      }
      static template(config) {
          return new NotifiTemplate(config);
      }
  }
  //# sourceMappingURL=index.js.map

  const sideBarWidth = 320;
  const sideBarWidthPadding = sideBarWidth - 20;
  const baseMargin = 10;
  const notifis = {};
  function setupNotifis() {
      // Confirm modal
      const confirm = notifis.confirm = NotifiUI.new(NotifiUI.template({
          type: NotifiType.Modal,
          children: [{
                  type: NotifiType.VBox,
                  children: [{
                          type: NotifiType.Text,
                          text: "Are you sure you want to {{ action }} {{ thing }}?",
                      }, {
                          type: NotifiType.HBox,
                          children: [{
                                  type: NotifiType.Button,
                                  text: "Yes",
                                  click() {
                                      const cb = this.getState("callback");
                                      this.close();
                                      if (cb) {
                                          cb();
                                      }
                                  }
                              }, {
                                  type: NotifiType.Button,
                                  text: "No",
                                  click() {
                                      this.close();
                                  }
                              }]
                      }]
              }]
      }), {
          positionFixed: true,
          position: ["center", "center"],
          visible: false,
      }, {
          action: "delete",
          thing: "this lesson",
          callback: null,
      });
      // Delete Lesson Series Popup
      const delLesson = notifis.delLesson = NotifiUI.new(NotifiUI.template({
          type: NotifiType.Minimal,
          children: [{
                  type: NotifiType.HBox,
                  children: [{
                          type: NotifiType.Button,
                          text: "Same Hour",
                          click() {
                              const lesson = this.getState("lesson");
                              if (lesson) {
                                  confirm.setState("action", "delete");
                                  confirm.setState("thing", "these lessons");
                                  confirm.setState("callback", () => {
                                      state.triggerEvent("deleteSeries", {
                                          lesson,
                                          weektime: lesson.weektime,
                                      });
                                  });
                                  confirm.open();
                              }
                              this.close();
                          }
                      }, {
                          type: NotifiType.Button,
                          text: "All",
                          click() {
                              const lesson = this.getState("lesson");
                              if (lesson) {
                                  confirm.setState("action", "delete");
                                  confirm.setState("thing", "these lessons");
                                  confirm.setState("callback", () => {
                                      state.triggerEvent("deleteSeries", {
                                          lesson,
                                      });
                                  });
                                  confirm.open();
                              }
                              this.close();
                          }
                      }]
              }]
      }), {
          position: [1, 2],
          anchorMargin: [-baseMargin, baseMargin],
          width: sideBarWidthPadding,
          visible: false,
          blurClose: true,
      }, {
          lesson: null,
      });
      const calendarMsg = notifis.calendarMsg = NotifiUI.new(NotifiUI.template({
          type: NotifiType.Minimal,
          children: [{
                  type: NotifiType.HBox,
                  children: [{
                          type: NotifiType.Text,
                          text: "{{ msg }}",
                      }]
              }]
      }), {
          visible: false,
          position: [0.5, 0],
          anchorMargin: [0, -baseMargin],
          width: 350,
      }, {
          msg: "",
      });
      const leftColWidth = 4;
      const lessonSummary = notifis.lessonSummary = NotifiUI.new(NotifiUI.template({
          type: NotifiType.Minimal,
          children: [{
                  type: NotifiType.VBox,
                  children: [{
                          type: NotifiType.HBox,
                          children: [{
                                  type: NotifiType.Text,
                                  text: locale.roleProfessor,
                                  size: leftColWidth,
                              }, {
                                  type: NotifiType.Text,
                                  text: "{{ professor : b }}",
                              }]
                      }, {
                          type: NotifiType.HBox,
                          children: [{
                                  type: NotifiType.Text,
                                  text: locale.sidebar.startTime,
                                  size: leftColWidth,
                              }, {
                                  type: NotifiType.Text,
                                  text: "{{ profStartTime : b }}",
                              }]
                      }, {
                          type: NotifiType.HBox,
                          children: [{
                                  type: NotifiType.Text,
                                  text: locale.roleStudent,
                                  size: leftColWidth,
                              }, {
                                  type: NotifiType.Text,
                                  text: "{{ student : b }}",
                              }]
                      }, {
                          type: NotifiType.HBox,
                          children: [{
                                  type: NotifiType.Text,
                                  text: locale.sidebar.startTime,
                                  size: leftColWidth,
                              }, {
                                  type: NotifiType.Text,
                                  text: "{{ studStartTime : b }}",
                              }]
                      }, {
                          type: NotifiType.HBox,
                          children: [{
                                  type: NotifiType.Text,
                                  text: locale.sidebar.paidDuration,
                                  size: leftColWidth,
                              }, {
                                  type: NotifiType.Text,
                                  text: "{{ paidDuration : b }}",
                              }]
                      }, {
                          type: NotifiType.HBox,
                          children: [{
                                  type: NotifiType.Text,
                                  text: locale.sidebar.lessonId,
                                  size: leftColWidth,
                              }, {
                                  type: NotifiType.Text,
                                  text: "{{ lessonId : b }}",
                              }]
                      }, {
                          type: NotifiType.HBox,
                          children: [{
                                  type: NotifiType.Text,
                                  text: locale.sidebar.lessonOrderId,
                                  size: leftColWidth,
                              }, {
                                  type: NotifiType.Text,
                                  text: "{{ orderId : b }}",
                              }]
                      }]
              }]
      }), {
          filters: {
              b(s) {
                  return jSh.c("b", {
                      text: s,
                  });
              }
          },
          position: [0, 2],
          anchorMargin: [0, baseMargin],
          width: 350,
          visible: false,
          blurClose: true,
      }, {
          professor: "",
          student: "",
          lessonId: 0,
          profStartTime: "",
          studStartTime: "",
          adminStartTime: "",
          paidDuration: "",
          orderId: 0,
      });
  }
  //# sourceMappingURL=notifi.js.map

  class CalendarSideBar extends Component {
      constructor(dom) {
          super();
          this.dom = dom;
          this.sections = {};
          this.domContent = this.dom.jSh(".ihfath-sidebar-inner")[0];
          this.domActions = [];
          this.domClose = this.dom.jSh(".ihfath-sidebar-close")[0];
          state.onState("focusedLesson", lesson => {
              this.lesson = lesson;
              this.render(lesson);
          });
          this.domClose.addEventListener("click", e => {
              state.setState("sideBarVisible", false);
          });
          // Setup
          this.buildActions();
      }
      buildActions() {
          const that = this;
          const actions = this.domActions;
          const lessonLink = jSh.c("a", {
              sel: ".ihfath-action.ihfath-half",
              child: [
                  jSh.c("i", ".fa.fa-external-link-square"),
                  jSh.c("span", null, " " + locale.sidebar.actionGoToLesson),
              ],
              prop: {
                  href: "#",
                  title: locale.sidebar.actionGoToLesson,
                  target: "_blank",
              },
          });
          const delLessonSeries = jSh.d({
              sel: ".ihfath-action.ihfath-half",
              child: [
                  jSh.c("i", ".fa.fa-trash-o"),
                  jSh.c("span", null, " " + locale.sidebar.actionSeries),
              ],
              prop: {
                  title: locale.sidebar.actionDeleteSeries,
              },
              events: {
                  click() {
                      notifis.delLesson.setState("lesson", that.lesson);
                      notifis.delLesson.open();
                      // notifis.delLesson.render(true);
                  }
              }
          });
          // Set anchor to delLessonSeries button
          notifis.delLesson.setState("anchor", delLessonSeries);
          state.onState("focusedLesson", lesson => {
              lessonLink.href = api.base + "/koa/lesson/" + lesson.lid;
          });
          actions.push(this.buildSection("Actions", ".ihfath-rows", [
              // Move lesson/series
              jSh.d(null, null, [
                  jSh.d({
                      sel: ".ihfath-action.ihfath-half-first",
                      child: [
                          jSh.c("i", ".fa.fa-arrows"),
                          jSh.c("span", null, " " + locale.sidebar.actionSingle),
                      ],
                      prop: {
                          title: locale.sidebar.actionMoveSingle
                      },
                      events: {
                          click() {
                              state.triggerEvent("moveLesson", that.lesson);
                          }
                      }
                  }),
                  jSh.d({
                      sel: ".ihfath-action.ihfath-half",
                      child: [
                          jSh.c("i", ".fa.fa-arrows"),
                          jSh.c("span", null, " " + locale.sidebar.actionSeries),
                      ],
                      prop: {
                          title: locale.sidebar.actionMoveSeries
                      },
                      events: {
                          click() {
                          }
                      }
                  }),
              ]),
              // Delete lesson/series
              jSh.d(null, null, [
                  jSh.d({
                      sel: ".ihfath-action.ihfath-half-first",
                      child: [
                          jSh.c("i", ".fa.fa-trash-o"),
                          jSh.c("span", null, " " + locale.sidebar.actionSingle),
                      ],
                      prop: {
                          title: locale.sidebar.actionDeleteSingle
                      },
                      events: {
                          click() {
                              const confirm = notifis.confirm;
                              confirm.setState("action", "delete");
                              confirm.setState("thing", "this lesson");
                              confirm.setState("callback", () => {
                                  state.triggerEvent("deleteLesson", that.lesson);
                              });
                              confirm.open();
                          }
                      }
                  }),
                  delLessonSeries,
              ]),
              jSh.d(null, null, [
                  // Postpone lesson
                  jSh.d({
                      sel: ".ihfath-action.ihfath-half-first",
                      child: [
                          jSh.c("i", ".fa.fa-clock-o"),
                          jSh.c("span", null, " " + locale.sidebar.actionPostponeLesson),
                      ],
                      prop: {
                          title: locale.sidebar.actionPostponeLesson,
                      },
                      events: {
                          click() {
                              state.triggerEvent("postponeLesson", that.lesson);
                          }
                      }
                  }),
                  // Lesson link
                  lessonLink,
              ])
          ]));
      }
      infoStrip(label, value = null) {
          // Render plain label/value
          if (value !== null) {
              return jSh.d(".ihfath-info", null, [
                  jSh.c("span", ".ihfath-label", label),
                  jSh.c("span", ".ihfath-value", value),
              ]);
          }
          else {
              // Render header
              return jSh.d({
                  sel: ".ihfath-section-header",
                  child: jSh.c("span", null, label + " "),
              });
          }
      }
      buildSection(header, sel, content) {
          let sectionComponent = this.sections[header];
          if (!sectionComponent) {
              sectionComponent = this.sections[header] = new Component();
              sectionComponent.newState("collapsed", false);
              sectionComponent.newState("domHeader", null);
              sectionComponent.newState("domContent", null);
              const headerClick = () => {
                  sectionComponent.setState("collapsed", !sectionComponent.getState("collapsed"));
              };
              sectionComponent.onState("domHeader", function (header) {
                  const oldDiv = this.oldValue;
                  if (oldDiv) {
                      oldDiv.removeEventListener("click", headerClick);
                  }
                  if (header) {
                      header.addEventListener("click", headerClick);
                  }
              });
              // Toggle visiblity of section content
              sectionComponent.onState("collapsed", collapsed => {
                  const headerDom = sectionComponent.getState("domHeader");
                  const contentDom = sectionComponent.getState("domContent");
                  if (collapsed) {
                      headerDom && headerDom.classList.add("ihfath-collapsed");
                      contentDom && contentDom.classList.add("ihfath-hidden");
                  }
                  else {
                      headerDom && headerDom.classList.remove("ihfath-collapsed");
                      contentDom && contentDom.classList.remove("ihfath-hidden");
                  }
              });
          }
          // FIXME: Possible memory leak? Direct ref from callback to dom and vice versa
          const sectionState = sectionComponent;
          const headerDom = sectionState.setState("domHeader", jSh.d({
              sel: ".ihfath-section-header",
              events: {
                  mousedown(e) {
                      e.preventDefault();
                  },
              },
              child: [
                  jSh.c("span", null, header + " "),
                  jSh.c("i", {
                      sel: ".fa.fa-caret-down",
                  }),
              ],
          }));
          const contentDom = sectionState.setState("domContent", jSh.d({
              sel: ".ihfath-content" + (sel || ""),
              child: content,
          }));
          const base = jSh.d({
              sel: ".ihfath-sidebar-section",
              child: [
                  headerDom,
                  contentDom,
              ],
          });
          // Reapply state
          sectionState.setState("collapsed", sectionState.getState("collapsed"), true);
          return base;
      }
      render(lesson) {
          const content = this.domContent;
          const profUser = state.getState("usersInvolved")[lesson.puid];
          const studUser = state.getState("usersInvolved")[lesson.suid];
          const tzOffset = (new Date().getTimezoneOffset()) * 60;
          const shiftTzDate = (date, offset) => new Date((date + (offset * 60 * 60) + tzOffset) * 1000);
          const profStart = shiftTzDate(lesson.startDate, profUser.timezoneOffset);
          const studStart = shiftTzDate(lesson.startDate, studUser.timezoneOffset);
          const adminStart = new Date(lesson.startDate * 1000);
          content.innerHTML = "";
          // Add actions
          content.appendChild(this.domActions);
          // Participants
          content.appendChild(this.buildSection(locale.sidebar.participants, ".ihfath-rows", [
              this.infoStrip(locale.sidebar.professor, profUser.realName || profUser.name),
              this.infoStrip(locale.sidebar.startTime, `${profStart.toLocaleTimeString()} — ${profStart.toLocaleDateString()}`),
              this.infoStrip(locale.sidebar.student, studUser.name),
              this.infoStrip(locale.sidebar.startTime, `${studStart.toLocaleTimeString()} — ${studStart.toLocaleDateString()}`),
              this.infoStrip(locale.sidebar.admin, `${adminStart.toLocaleTimeString()} — ${adminStart.toLocaleDateString()}`),
          ]));
          const profVisit = lesson.profLast ? shiftTzDate(lesson.profLast, profUser.timezoneOffset).toLocaleTimeString() : locale.sidebar.notAvailableNA;
          const studentVisit = lesson.studentLast ? shiftTzDate(lesson.studentLast, profUser.timezoneOffset).toLocaleTimeString() : locale.sidebar.notAvailableNA;
          // Activity
          content.appendChild(this.buildSection(locale.sidebar.lessonActivity, ".ihfath-rows", [
              this.infoStrip(locale.sidebar.lastVisit),
              this.infoStrip(locale.sidebar.professor, profVisit),
              this.infoStrip(locale.sidebar.student, studentVisit),
              this.infoStrip(locale.sidebar.duration),
              this.infoStrip(locale.sidebar.professor, humanTime(lesson.profDur)),
              this.infoStrip(locale.sidebar.student, humanTime(lesson.studentDur)),
          ]));
          // Lesson details
          content.appendChild(this.buildSection(locale.sidebar.lessonDetails, ".ihfath-rows", [
              this.infoStrip(locale.sidebar.lessonId, lesson.lid + ""),
              this.infoStrip(locale.sidebar.lessonSeriesId, lesson.series + ""),
              this.infoStrip(locale.sidebar.lessonOrderId, lesson.orderId + ""),
              this.infoStrip(locale.sidebar.paidDuration, (lesson.paidDuration / 60) + " " + locale.sidebar.hours),
              this.infoStrip(locale.sidebar.lessonProgram, lesson.programName),
              this.infoStrip(locale.sidebar.lessonType, lesson.typeName),
          ]));
      }
  }
  //# sourceMappingURL=calendar-sidebar.js.map

  let lessonSummaryMgr;
  // Lesson colors, gen script below
  //    partitions = 24
  //    console.log(
  //      new Array(partitions).fill(1).map((a, i) =>
  //        `hsl(${ ((360 - (360 / (partitions))) / (partitions - 1) * i) }deg, 65%, 35%)`
  //      )
  //    )
  //
  //
  // Colors may be rearranged
  const colors = [
      'hsl(0deg, 65%, 35%)',
      'hsl(15deg, 65%, 35%)',
      'hsl(30deg, 65%, 35%)',
      'hsl(45deg, 65%, 35%)',
      'hsl(60deg, 65%, 35%)',
      'hsl(75deg, 65%, 35%)',
      'hsl(90deg, 65%, 35%)',
      'hsl(105deg, 65%, 35%)',
      'hsl(120deg, 65%, 35%)',
      'hsl(135deg, 65%, 35%)',
      'hsl(150deg, 65%, 35%)',
      'hsl(165deg, 65%, 35%)',
      'hsl(180deg, 65%, 35%)',
      'hsl(195deg, 65%, 35%)',
      'hsl(210deg, 65%, 35%)',
      'hsl(225deg, 65%, 35%)',
      'hsl(240deg, 65%, 35%)',
      'hsl(255deg, 65%, 35%)',
      'hsl(270deg, 65%, 35%)',
      'hsl(285deg, 65%, 35%)',
      'hsl(300deg, 65%, 35%)',
      'hsl(315deg, 65%, 35%)',
      'hsl(330deg, 65%, 35%)',
      'hsl(345deg, 65%, 35%)',
  ];
  class MonthCalendar extends Component {
      constructor(domRoot, data) {
          super();
          this.domRoot = domRoot;
          this.domTableRoot = domRoot.jSh(".ihfath-schedule-root")[0];
          // Construct monthly calendar
          const table = this.table = new Cellidi({
              element: this.domTableRoot,
              dimensions: [7, 6],
              history: true,
              headers: {
                  top(x) {
                      const d = jSh.d(null, locale.days[x]);
                      return d;
                  },
              },
              newCell(x, y, virtualX, virtualY, data, cellDisplay) {
                  const date = jSh.d(".ihfath-cell-date");
                  const lessonWrap = jSh.d(".ihfath-cell-lessons", null, [
                      jSh.d(".ihfath-cell-lessons-col"),
                  ]);
                  const cell = jSh.d({
                      child: [
                          date,
                          lessonWrap,
                      ]
                  });
                  return {
                      dom: cell,
                      data: {
                          date,
                          lessonWrap,
                      }
                  };
              },
              diffStates(a, b) {
                  // Compare basic properies
                  if (a.outMonth !== b.outMonth
                      || a.curDay !== b.curDay
                      || a.date !== b.date
                      || a.lessons.length !== b.lessons.length) {
                      // Differing properties found
                      return true;
                  }
                  const aLessons = a.lessons;
                  const bLessons = b.lessons;
                  // Compare lessons
                  for (let i = 0; i < aLessons.length; i++) {
                      if (aLessons[i].lid !== bLessons[i].lid) {
                          // Differing lessons found
                          return true;
                      }
                  }
                  // Both cell states are the same
                  return false;
              },
              emptyState(renew, old) {
                  if (renew && old && typeof old === "object") {
                      const newCell = {
                          outMonth: old.outMonth,
                          curDay: old.curDay,
                          date: old.date,
                          lessons: old.lessons.slice(),
                      };
                      return newCell;
                  }
                  else {
                      const newCell = {
                          outMonth: false,
                          curDay: false,
                          date: 0,
                          lessons: [],
                      };
                      return newCell;
                  }
              },
              onCellStateChange(cellState, cellModels) {
                  const userData = state.getState("userData");
                  const usersInvolved = state.getState("usersInvolved");
                  cellState.forEach((cellState, index) => {
                      const model = cellModels[index];
                      if (cellState.curDay) {
                          model.dom.classList.add("ihfath-cell-current-day");
                      }
                      else {
                          model.dom.classList.remove("ihfath-cell-current-day");
                      }
                      if (cellState.outMonth) {
                          model.dom.classList.add("ihfath-cell-out-month");
                      }
                      else {
                          model.dom.classList.remove("ihfath-cell-out-month");
                      }
                      const date = model.data.date;
                      const lessonWrap = model.data.lessonWrap;
                      date.textContent = cellState.date.toString();
                      // Wipe any previous lessons
                      lessonWrap.innerHTML = "";
                      let lessonCount = 0;
                      let lessonCol = null;
                      // Add new lessons to cell
                      for (const lesson of cellState.lessons) {
                          if (lessonCount % 4 === 0) {
                              lessonCol = jSh.d(".ihfath-cell-lessons-col");
                              lessonWrap.appendChild(lessonCol);
                          }
                          const prof = usersInvolved[lesson.puid];
                          const profTag = userData.role === UserRoles.Student
                              ? jSh.c("span", ".ihfath-prof", (prof.name.match(/\d+/) || ["0"])[0])
                              : jSh.t("");
                          const lessonTag = jSh.d({
                              sel: ".ihfath-cell-lesson",
                              attr: {
                                  style: `
                  background: ${colors[(lesson.startDate / 60 / 60) % colors.length]};
                `,
                              },
                              child: [
                                  jSh.c("span", null, lesson.programName),
                                  profTag,
                              ]
                          });
                          lessonTag.IHFATH_LESSON = lesson;
                          lessonCol.appendChild(lessonTag, jSh.d(".ihfath-cell-break"));
                          lessonCount++;
                      }
                  });
              },
          });
          table.on("activeinput", cell => {
              // Open week view when you click an inMonth cell
              const cellState = table.getCellState(cell.cell);
              if (!cellState.outMonth) {
                  const weekDays = [];
                  for (let i = 0; i < 7; i++) {
                      const cellWeekStartModel = table.getCellModelFromCoords(i, cell.cell.coords[1]);
                      weekDays.push(table.getCellState(cellWeekStartModel).date);
                  }
                  // Push the first day of the week to global state
                  state.setState("firstWeekDay", weekDays);
                  // Request week data
                  state.setState("monthDay", cellState.date);
                  state.triggerEvent("reqWeekData", true);
              }
          });
          // Lesson summary infotip
          let cancels = 0;
          this.domTableRoot.addEventListener("mousemove", e => {
              const cb = (v) => {
                  cancels = 0;
                  let target = e.target;
                  let lessonTag = null;
                  while (target !== this.domTableRoot) {
                      if (target.IHFATH_LESSON) {
                          lessonTag = target;
                          break;
                      }
                      target = target.parentNode;
                  }
                  const lessonNotifiDelay = 10000;
                  if (lessonTag) {
                      const lessonSummary = notifis.lessonSummary;
                      if (lessonTag.IHFATH_LESSON.lid === lessonSummary.getState("lessonId")) {
                          lessonSummary.open(lessonNotifiDelay);
                          return;
                      }
                      const lesson = lessonTag.IHFATH_LESSON;
                      const usersInvolved = state.getState("usersInvolved");
                      const profUser = usersInvolved[lesson.puid];
                      const studUser = usersInvolved[lesson.suid];
                      lessonSummary.setState("anchor", lessonTag);
                      lessonSummary.setState("professor", profUser.name);
                      lessonSummary.setState("student", studUser.name);
                      lessonSummary.setState("lessonId", lesson.lid);
                      lessonSummary.setState("paidDuration", (lesson.paidDuration / 60) + " " + locale.sidebar.hours);
                      lessonSummary.setState("orderId", lesson.orderId);
                      // Calculate timezone offsets
                      const tzOffset = (new Date().getTimezoneOffset()) * 60;
                      const profStart = new Date((lesson.startDate + (profUser.timezoneOffset * 60 * 60) + tzOffset) * 1000);
                      const studStart = new Date((lesson.startDate + (studUser.timezoneOffset * 60 * 60) + tzOffset) * 1000);
                      const adminStart = new Date(lesson.startDate * 1000);
                      // Set start times
                      lessonSummary.setState("profStartTime", `${profStart.toLocaleTimeString()} — ${profStart.toLocaleDateString()}`);
                      lessonSummary.setState("studStartTime", `${studStart.toLocaleTimeString()} — ${studStart.toLocaleDateString()}`);
                      lessonSummary.setState("adminStartTime", `${adminStart.toLocaleTimeString()} — ${adminStart.toLocaleDateString()}`);
                      lessonSummary.open(lessonNotifiDelay);
                  }
                  else {
                      notifis.lessonSummary.close();
                  }
              };
              if (cancels > 10) {
                  cb();
              }
              else {
                  cancels++;
                  this.timeout("hoverLessonSummary", 100).then(cb);
              }
          });
          // States
          this.newState("focused", null);
          this.onState("focused", focused => {
              if (focused) {
                  this.domRoot.classList.add("ihfath-focused");
              }
              else {
                  this.domRoot.classList.remove("ihfath-focused");
              }
          });
      }
      setMonthData(data) {
          this.table.setState("state", data);
      }
  }
  class WeekCalendar extends Component {
      constructor(domRoot, manager) {
          super();
          this.domRoot = domRoot;
          this.manager = manager;
          this.domTableRoot = domRoot.jSh(".ihfath-schedule-root")[0];
          this.domBackToMonth = domRoot.jSh(".ihfath-back-to-month")[0];
          // Construct weekly calendar
          const table = this.table = new Cellidi({
              element: this.domTableRoot,
              dimensions: [7, 24],
              history: true,
              headers: {
                  top(x) {
                      // Draw week header cell
                      const cell = jSh.d({
                          child: [
                              jSh.t(locale.days[x] + " "),
                              jSh.c("b", null, (state.getState("firstWeekDay")[x]) + ""),
                          ]
                      });
                      return cell;
                  },
                  left(time) {
                      const cell = jSh.d({
                          sel: ".ihfath-time",
                          text: time + ":00",
                      });
                      return cell;
                  },
              },
              newCell(x, y, virtualX, virtualY, data, cellDisplay) {
                  let children = [];
                  let sel = y % 2 ? "" : ".ihfath-checker";
                  if (data && data.lesson) {
                      const usersInvolved = state.getState("usersInvolved");
                      const userData = state.getState("userData");
                      const opposingParty = userData.role === UserRoles.Professor ? usersInvolved[data.lesson.suid].name : usersInvolved[data.lesson.puid].name;
                      sel += ".ihfath-lesson.ihfath-dur-" + data.lesson.paidDuration;
                      children.push(jSh.d(".ihfath-cell-wrap", null, jSh.d(".ihfath-cell-text", null, [
                          jSh.c("b", null, data.lesson.programName + " "),
                          jSh.c("span", null, opposingParty + " "),
                          jSh.c("span", ".ihfath-dur", (data.lesson.paidDuration / 60) + "h"),
                      ])));
                  }
                  else {
                      sel += ".ihfath-vacant-cell";
                      children.push(jSh.t(virtualY + ":00"));
                      children.push(jSh.d(".ihfath-cell-move-selectable", null, [
                          jSh.c("i", ".fa.fa-arrows"),
                      ]));
                  }
                  const cell = jSh.d({
                      sel,
                      child: children,
                      prop: {
                          title: "",
                      },
                      events: {
                          mousedown(e) {
                              e.preventDefault();
                          }
                      }
                  });
                  return {
                      dom: cell,
                      spanY: data && data.span || 1,
                  };
              },
              onCellStateChange(newState, models) {
                  // Show arrows when choosing a cell
                  for (let i = 0; i < newState.length; i++) {
                      const cellState = newState[i];
                      const model = models[i];
                      if (cellState.selectable) {
                          model.dom.classList.add("ihfath-cell-show-selectable");
                      }
                      else {
                          model.dom.classList.remove("ihfath-cell-show-selectable");
                      }
                  }
              },
              diffStates(a, b) {
                  if (a.curDay !== b.curDay
                      || a.weekDay !== b.weekDay
                      || a.hour !== b.hour
                      || (a.lesson && a.lesson.lid) !== (b.lesson && b.lesson.lid)
                      || a.span !== b.span
                      || a.selectable !== b.selectable) {
                      return true;
                  }
                  // Both cellstates are the same
                  return false;
              },
              emptyState(renew, old) {
                  if (renew) {
                      const newCell = {
                          curDay: old.curDay,
                          weekDay: old.weekDay,
                          hour: old.hour,
                          lesson: old.lesson,
                          span: old.span,
                          selectable: old.selectable,
                      };
                      return newCell;
                  }
                  else {
                      const newCell = {
                          curDay: false,
                          weekDay: 0,
                          hour: 0,
                          lesson: null,
                          span: 1,
                          selectable: false,
                      };
                      return newCell;
                  }
              },
          });
          // States
          this.newState("choosingCell", false);
          this.newState("focused", null);
          this.onState("focused", focused => {
              if (focused) {
                  this.domRoot.classList.add("ihfath-focused");
              }
              else {
                  this.domRoot.classList.remove("ihfath-focused");
              }
          });
          // Events
          // Go back to month calendar when back to calender is clicked
          this.domBackToMonth.addEventListener("click", click => {
              // TODO: Check if should link these two states
              state.setState("sideBarVisible", false);
              this.manager.setState("focused", FocusedCalendar.Month);
          });
          // Go back to month calendar when user is disabled
          state.onState("userActive", active => {
              if (!active) {
                  state.setState("sideBarVisible", false);
                  this.manager.setState("focused", FocusedCalendar.Month);
              }
          });
          table.on("activeinput", cellEvt => {
              const cell = cellEvt.cell;
              const cellState = table.getCellState(cell);
              if (cellState.lesson) {
                  state.setState("focusedLesson", cellState.lesson);
                  state.setState("sideBarVisible", true);
              }
              else {
                  if (this.getState("choosingCell") && cellState.selectable) {
                      const lesson = state.getState("focusedLesson");
                      const coords = cell.coords;
                      const weektime = (coords[0] + 1) * 1000 + coords[1] * 10;
                      state.triggerEvent("moveLessonConfirm", {
                          lesson,
                          weektime,
                      });
                      this.setState("choosingCell", false);
                  }
              }
          });
          // Start choosing the cell when move event si triggered
          state.on("moveLesson", lesson => {
              this.setState("choosingCell", !this.getState("choosingCell"));
          });
          // Toggle selectability of vacant cells
          this.onState("choosingCell", choosing => {
              const lesson = state.getState("focusedLesson");
              const twoCell = lesson.paidDuration > 60;
              // Find all selectable cells
              const oldTable = table.getState("state");
              const newTable = table.mapEachCell(oldTable, (cell, x, y) => {
                  const newCell = Object.assign({}, cell);
                  if (choosing) {
                      // Display the selectable cell
                      if (!cell.lesson) {
                          let selectable = true;
                          if (twoCell) {
                              // Lesson is longer than an hour, make sure the lower cell is vacant
                              selectable = !!(oldTable[x][y + 1] && !oldTable[x][y + 1].lesson);
                          }
                          newCell.selectable = selectable;
                      }
                  }
                  else {
                      newCell.selectable = false;
                  }
                  return newCell;
              });
              // Apply the selection
              table.setState("state", newTable);
          });
      }
      setWeekData(data) {
          // TODO: Try to make this use .setState("state") instead of .rebuild()
          this.table.rebuild(data);
          // this.table.setState("state", data);
      }
  }
  var FocusedCalendar;
  (function (FocusedCalendar) {
      FocusedCalendar[FocusedCalendar["Month"] = 0] = "Month";
      FocusedCalendar[FocusedCalendar["Week"] = 1] = "Week";
  })(FocusedCalendar || (FocusedCalendar = {}));
  class CalendarManager extends Component {
      constructor(domWrapper, domMonth, domWeek, domControls, domUnavailable, domSideBar) {
          super();
          this.domWrapper = domWrapper;
          this.domMonth = domMonth;
          this.domWeek = domWeek;
          this.domControls = domControls;
          this.domUnavailable = domUnavailable;
          this.domSideBar = domSideBar;
          // Create calendar controllers
          const month = this.month = new MonthCalendar(domMonth, null);
          const week = this.week = new WeekCalendar(domWeek, this);
          // Create sidebar
          const sidebar = this.sidebar = new CalendarSideBar(domSideBar);
          // Toggle sidebar visibility when the state changes
          state.onState("sideBarVisible", visible => {
              if (visible) {
                  this.domWrapper.classList.add("ihfath-show-sidebar");
                  this.domSideBar.classList.remove("ihfath-hidden");
              }
              else {
                  this.domWrapper.classList.remove("ihfath-show-sidebar");
                  this.domSideBar.classList.add("ihfath-hidden");
              }
          });
          // Reference controls
          const monthInput = domControls.jSh(".ihfath-month-select")[0];
          const yearInput = domControls.jSh(".ihfath-year-input")[0];
          const nextMonth = domControls.jSh(".ihfath-go-next-month")[0];
          const prevMonth = domControls.jSh(".ihfath-go-prev-month")[0];
          // Create lesson summary manager
          lessonSummaryMgr = new LessonSummaryManager();
          // Update appstate
          state.setState("year", parseInt(yearInput.value, 10));
          state.setState("month", parseInt(monthInput.value, 10));
          // Sync appstate with inputs
          monthInput.addEventListener("change", function () {
              const newValue = jSh.numOp(parseInt(this.value, 10), null);
              if (newValue === null) {
                  this.value = state.getState("month") + "";
              }
              else {
                  state.setState("month", newValue);
              }
          });
          yearInput.addEventListener("change", function () {
              const newValue = jSh.numOp(parseInt(this.value, 10), null);
              if (newValue === null) {
                  this.value = state.getState("year") + "";
              }
              else {
                  state.setState("year", newValue);
              }
          });
          const shiftMonth = (dir) => {
              const month = state.getState("month");
              const year = state.getState("year");
              let newMonth = month + dir;
              let newYear = year;
              if (newMonth > 12) {
                  newYear++;
                  newMonth = 1;
              }
              else if (newMonth < 1) {
                  newYear--;
                  newMonth = 12;
              }
              state.setState("month", newMonth);
              state.setState("year", newYear);
          };
          nextMonth.addEventListener("click", function () {
              shiftMonth(1);
          });
          prevMonth.addEventListener("click", function () {
              shiftMonth(-1);
          });
          state.onState("month", month => {
              if (month + "" !== monthInput.value) {
                  monthInput.value = month + "";
              }
          });
          state.onState("year", year => {
              if (year + "" !== yearInput.value) {
                  yearInput.value = year + "";
              }
          });
          // Disable inputs when user is deactivated
          state.onState("userActive", userActive => {
              if (userActive) {
                  monthInput.disabled = false;
                  yearInput.disabled = false;
              }
              else {
                  monthInput.disabled = true;
                  yearInput.disabled = true;
              }
          });
          // Create states
          this.newState("focused", null);
          this.newState("month", 0);
          this.newState("year", 2010);
          this.onState("focused", focused => {
              const focusedMonth = focused === FocusedCalendar.Month;
              month.setState("focused", focusedMonth);
              week.setState("focused", !focusedMonth);
          });
          // Hide calendar when it's not available
          state.onState("calendarAvailable", available => {
              if (available) {
                  this.domUnavailable.classList.add("ihfath-hidden");
              }
              else {
                  this.domUnavailable.classList.remove("ihfath-hidden");
              }
          });
          // Initial setup
          this.setState("focused", FocusedCalendar.Month);
          state.setState("calendarAvailable", false);
          this.messages = {};
          this.setupUnvailableMessage();
          this.setCalendarUnavailableMsg(CalendarUnavailableMessage.UserNotLoaded);
      }
      setMonthData(data) {
          this.month.setMonthData(data);
      }
      setWeekData(data) {
          this.week.setWeekData(data);
      }
      setCalendarUnavailableMsg(aspect) {
          const msgDiv = this.domUnavailable.jSh(0).jSh(0);
          msgDiv.innerHTML = "";
          for (const child of this.messages[aspect]) {
              msgDiv.appendChild(child);
          }
      }
      setupUnvailableMessage() {
          const messages = {
              [CalendarUnavailableMessage.UserNotLoaded]: locale.findAUser,
              [CalendarUnavailableMessage.UserNoCalendar]: locale.userNoCalendar,
              [CalendarUnavailableMessage.ErrorLoadingCalendar]: locale.errorGettingCalendar,
          };
          for (const key of Object.keys(messages)) {
              const string = messages[+key];
              const index = +key;
              const elements = [];
              switch (index) {
                  case CalendarUnavailableMessage.UserNotLoaded: {
                      const match = string.match(/^([^]*)\[click\]([^]+)\[\/click\]([^]*)$/);
                      elements.push(jSh.t(match[1]), jSh.c("a", {
                          text: match[2],
                          prop: {
                              href: "javascript:0[0]",
                          },
                          events: {
                              click() {
                                  window.scrollTo(0, 0);
                                  state.focusUserField();
                              },
                          },
                      }), jSh.t(match[3]));
                      break;
                  }
                  case CalendarUnavailableMessage.ErrorLoadingCalendar: {
                      const match = string.split("@user");
                      const name = jSh.c("b", null, "");
                      elements.push(jSh.t(match[0]), name, jSh.t(match[1]));
                      state.onState("userData", (data) => {
                          if (data) {
                              name.textContent = data.realName || data.name;
                          }
                      });
                      break;
                  }
                  case CalendarUnavailableMessage.UserNoCalendar: {
                      const match = string.split("@user");
                      const name = jSh.c("b", null, "");
                      elements.push(jSh.t(match[0]), name, jSh.t(match[1]));
                      state.onState("userData", (data) => {
                          if (data) {
                              name.textContent = data.realName || data.name;
                          }
                      });
                      break;
                  }
              }
              this.messages[index] = elements;
          }
      }
  }
  //# sourceMappingURL=calendar.js.map

  function construct(config) {
      // Setup notifis
      setupNotifis();
      // Setup tabs
      const manager = new TabManager();
      const tabConfig = config.dom.tabs;
      // Build tabs from the provided config
      for (const tabName of tabConfig.items) {
          const tab = tabConfig.tabs.jSh(".ihfath-tab-" + tabName)[0];
          const body = tabConfig.body.jSh(".ihfath-body-" + tabName)[0];
          manager.addTab(tabName, tab, body);
      }
      // Focus first tab
      manager.selectTab(0);
      // Setup userfield
      const user = new Userfield(config.dom.userInput, {
          editUser: {},
          messages: {},
          spinner: {},
          userReady: {},
      });
      // Make userfield accessible to all with access to state
      state.setUserField(user);
      // Setup user summary
      const userSummary = new UserSummary(config.dom.userSummary);
      // Setup calendars
      const calManager = new CalendarManager(config.dom.calendarWrap, config.dom.calendarMonth, config.dom.calendarWeek, config.dom.calendarInput, config.dom.calendarUnavailable, config.dom.calendarSideBar);
      // Bind msg notifi to calendar
      notifis.calendarMsg.setState("anchor", config.dom.calendarWrap);
      // Delete lesson when the user clicks the delete action button
      state.on("deleteLesson", lesson => {
          RequestManager.request(RequestManager.actions.entityManipulate(EntityAspect.Lesson, EntityAction.Delete, lesson.lid)).xhr.then(data => {
              if (!data.error) {
                  state.setState("sideBarVisible", false);
                  // Update the week calendar
                  if (calManager.getState("focused") === FocusedCalendar.Week) {
                      state.triggerEvent("reqWeekData", true);
                  }
                  // Update month calendar
                  updateCalendar(state.getState("userData"));
                  notifis.calendarMsg.setState("msg", "Deleted 1 lesson successfully");
                  notifis.calendarMsg.open(5000);
              }
              else {
                  console.log("LESSON DELETE ERR: ", data);
              }
          });
      });
      // Delete series when the user clicks the delete action button
      state.on("deleteSeries", series => {
          const user = state.getState("userData");
          RequestManager.request(RequestManager.actions.entityManipulate(EntityAspect.Series, EntityAction.Delete, series.lesson.series, series.weektime)).xhr.then(data => {
              if (!data.error) {
                  state.setState("sideBarVisible", false);
                  // Update the week calendar
                  if (calManager.getState("focused") === FocusedCalendar.Week) {
                      state.triggerEvent("reqWeekData", true);
                  }
                  // Update month calendar
                  updateCalendar(user);
                  notifis.calendarMsg.setState("msg", "Deleted " + data.data.lessons + " lessons successfully");
                  notifis.calendarMsg.open(5000);
              }
              else {
                  console.log("SERIES DELETE ERR: ", data);
              }
          });
      });
      state.on("moveLessonConfirm", move => {
          const user = state.getState("userData");
          RequestManager.request(RequestManager.actions.entityManipulate(EntityAspect.Lesson, EntityAction.Move, move.lesson.lid, move.weektime, user.uid)).xhr.then(data => {
              if (!data.error) {
                  state.setState("sideBarVisible", false);
                  // Update the week calendar
                  if (calManager.getState("focused") === FocusedCalendar.Week) {
                      state.triggerEvent("reqWeekData", true);
                  }
                  // Update month calendar
                  updateCalendar(user);
                  notifis.calendarMsg.setState("msg", "Moved lesson successfully");
                  notifis.calendarMsg.open(5000);
              }
              else {
                  console.log("LESSON MOVE ERR: ", data);
              }
          });
      });
      // Show suggestions when the user updates the field
      user.on("change", (name) => {
          if (name) {
              RequestManager.request(RequestManager.actions.userFind(name)).xhr.then(data => {
                  // console.log("DATA", data);
                  // Let everyone know of the new suggestions
                  const names = jSh.type(data) === "array" ? [] : Object.keys(data);
                  state.triggerEvent("userSuggestions", names);
              });
          }
          else {
              state.triggerEvent("userSuggestions", []);
          }
      });
      // Go get user data when a user is chosen from the suggestions
      state.onState("user", function (name) {
          if (name) {
              // Show spinner
              user.userActions.spinner.setState("visible", true);
              RequestManager.request(RequestManager.actions.user(name)).xhr.then(data => {
                  if (data && !data.error) {
                      // Let everyone know of the user's data arrival
                      state.setState("userActive", true);
                      state.setState("userData", userToUser(data.data));
                      // Hide spinner
                      user.userActions.spinner.setState("visible", false);
                  }
                  else {
                      console.error("Ihfath Request User Error", data.error);
                  }
              });
          }
      });
      // Reference userrel tab
      const tabUserrel = manager.getTab("userrel");
      // Show "editUser" action when a new user is loaded
      state.onState("userData", function (userData) {
          const editUser = user.userActions.editUser;
          const messages = user.userActions.messages;
          if (userData) {
              messages.dom.href = `${api.base}/ihfath/messages/${userData.uid}`;
              editUser.dom.href = `${api.base}/user/${userData.uid}/edit`;
              editUser.setState("visible", true);
              messages.setState("visible", true);
              // Update summary
              userSummary.setState("data", userSummaryPrettyMap(userData));
              userSummary.setState("visible", true);
              const userRelMapping = {
                  [UserRoles.Admin]: locale.users,
                  [UserRoles.Professor]: locale.students,
                  [UserRoles.Student]: locale.professors,
              };
              // Update userrel tab to a relevant title
              tabUserrel.setText(userRelMapping[userData.role]);
              updateCalendar(userData);
          }
          else {
              calManager.setCalendarUnavailableMsg(CalendarUnavailableMessage.UserNotLoaded);
              state.setState("calendarAvailable", false);
              editUser.setState("visible", false);
              messages.setState("visible", false);
              userSummary.setState("visible", false);
          }
      });
      state.onState("userActive", function (active) {
          const userReady = user.userActions.userReady;
          if (active) {
              userReady.setState("visible", true);
          }
          else {
              userReady.setState("visible", false);
          }
      });
      // Update monthly calendar
      function updateCalendar(userData) {
          if (userData.role === UserRoles.Admin) {
              calManager.setCalendarUnavailableMsg(CalendarUnavailableMessage.UserNoCalendar);
              return;
          }
          RequestManager.request(RequestManager.actions.userMonth(userData.uid, state.getState("month"), state.getState("year"))).xhr.then(data => {
              if (!data.error) {
                  // Show calendar
                  state.setState("calendarAvailable", true);
                  const table = data.data.table;
                  const users = data.data.users;
                  const sanitizedUsers = {};
                  // Sanitize raw lesson data to a more usable format
                  table.forEach((row) => {
                      for (const cell of row) {
                          cell.lessons = cell.lessons.map(lessonRaw => lessonToLesson(lessonRaw));
                      }
                  });
                  // Sanitize users provided by server
                  for (const uid of Object.keys(users)) {
                      const user = userToUser(users[uid]);
                      sanitizedUsers[+uid] = user;
                  }
                  // NOTE: Push involved users _before_ you update the calendar, the
                  // calendar will need this user data
                  state.setState("usersInvolved", sanitizedUsers);
                  // Push/update calendar data
                  calManager.setMonthData(table);
              }
          });
      }
      // Week data event dispatched by user clicking a week row
      // on the calendar ui
      state.on("reqWeekData", stub => {
          const monthDay = state.getState("monthDay");
          const month = state.getState("month");
          const year = state.getState("year");
          const userData = state.getState("userData");
          if (userData && userData.role !== UserRoles.Admin) {
              RequestManager.request(RequestManager.actions.userWeek(userData.uid, monthDay, month, year)).xhr.then(data => {
                  if (!data.error) {
                      const table = data.data.table;
                      const users = data.data.users;
                      const tableRemap = table.map((col, day) => {
                          // Shift monday-based to sunday-based
                          day++;
                          return col.map((cell, hour) => {
                              if (jSh.type(cell) === "object") {
                                  const lesson = lessonToLesson(cell.lesson);
                                  return {
                                      curDay: false,
                                      weekDay: day === 7 ? 0 : day,
                                      lesson: lesson,
                                      span: lesson.paidDuration > 60 ? 2 : 1,
                                      hour: hour,
                                      selectable: false,
                                  };
                              }
                              else {
                                  return {
                                      curDay: false,
                                      weekDay: day === 7 ? 0 : day,
                                      lesson: null,
                                      span: 1,
                                      hour: hour,
                                      selectable: false,
                                  };
                              }
                          });
                      });
                      const usersInvolved = state.getState("usersInvolved") || {};
                      const usersRemapped = {};
                      for (const uid of Object.keys(users)) {
                          usersRemapped[+uid] = userToUser(users[uid]);
                      }
                      // Push table and users to state
                      state.setState("usersInvolved", usersRemapped);
                      state.setState("weekData", tableRemap);
                  }
                  else {
                      console.log("WEEK ERR", data.cause);
                  }
              });
          }
      });
      // Update calendar when the app state changes and the user's available
      state.onState("month", () => {
          const userData = state.getState("userData");
          if (userData) {
              updateCalendar(userData);
          }
      });
      state.onState("year", () => {
          const userData = state.getState("userData");
          if (userData) {
              updateCalendar(userData);
          }
      });
      state.onState("weekData", weekData => {
          calManager.setState("focused", FocusedCalendar.Week);
          calManager.setWeekData(weekData);
      });
  }
  //# sourceMappingURL=construction.js.map

  function InitLessonAdmin(config) {
      setApiUrl(config.baseUrl, config.apiUrl);
      updateLocale(config.locale);
      // Prevent form from submitting itself
      config.dom.form.addEventListener("submit", e => {
          e.preventDefault();
          e.stopPropagation();
      });
      construct(config);
      // Load initial user if available
      if (config.userId) {
          state.setState("user", config.userId);
      }
      // Use stub
      console.log(stub);
  }
  // Export to window
  window.InitLessonAdmin = InitLessonAdmin;
  //# sourceMappingURL=adminlessons.js.map

}(jSh, Cellidi));
//# sourceMappingURL=adminlessons.js.map
