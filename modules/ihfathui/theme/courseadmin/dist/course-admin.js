(function () {
	'use strict';

	function noop() {}

	function assign(tar, src) {
		for (var k in src) tar[k] = src[k];
		return tar;
	}

	function assignTrue(tar, src) {
		for (var k in src) tar[k] = 1;
		return tar;
	}

	function addLoc(element, file, line, column, char) {
		element.__svelte_meta = {
			loc: { file, line, column, char }
		};
	}

	function append(target, node) {
		target.appendChild(node);
	}

	function insert(target, node, anchor) {
		target.insertBefore(node, anchor);
	}

	function detachNode(node) {
		node.parentNode.removeChild(node);
	}

	function destroyEach(iterations, detach) {
		for (var i = 0; i < iterations.length; i += 1) {
			if (iterations[i]) iterations[i].d(detach);
		}
	}

	function createElement(name) {
		return document.createElement(name);
	}

	function createText(data) {
		return document.createTextNode(data);
	}

	function createComment() {
		return document.createComment('');
	}

	function addListener(node, event, handler, options) {
		node.addEventListener(event, handler, options);
	}

	function removeListener(node, event, handler, options) {
		node.removeEventListener(event, handler, options);
	}

	function setAttribute(node, attribute, value) {
		if (value == null) node.removeAttribute(attribute);
		else node.setAttribute(attribute, value);
	}

	function setData(text, data) {
		text.data = '' + data;
	}

	function setStyle(node, key, value) {
		node.style.setProperty(key, value);
	}

	function toggleClass(element, name, toggle) {
		element.classList.toggle(name, !!toggle);
	}

	function blankObject() {
		return Object.create(null);
	}

	function destroy(detach) {
		this.destroy = noop;
		this.fire('destroy');
		this.set = noop;

		this._fragment.d(detach !== false);
		this._fragment = null;
		this._state = {};
	}

	function destroyDev(detach) {
		destroy.call(this, detach);
		this.destroy = function() {
			console.warn('Component was already destroyed');
		};
	}

	function _differs(a, b) {
		return a != a ? b == b : a !== b || ((a && typeof a === 'object') || typeof a === 'function');
	}

	function _differsImmutable(a, b) {
		return a != a ? b == b : a !== b;
	}

	function fire(eventName, data) {
		var handlers =
			eventName in this._handlers && this._handlers[eventName].slice();
		if (!handlers) return;

		for (var i = 0; i < handlers.length; i += 1) {
			var handler = handlers[i];

			if (!handler.__calling) {
				try {
					handler.__calling = true;
					handler.call(this, data);
				} finally {
					handler.__calling = false;
				}
			}
		}
	}

	function flush(component) {
		component._lock = true;
		callAll(component._beforecreate);
		callAll(component._oncreate);
		callAll(component._aftercreate);
		component._lock = false;
	}

	function get() {
		return this._state;
	}

	function init(component, options) {
		component._handlers = blankObject();
		component._slots = blankObject();
		component._bind = options._bind;
		component._staged = {};

		component.options = options;
		component.root = options.root || component;
		component.store = options.store || component.root.store;

		if (!options.root) {
			component._beforecreate = [];
			component._oncreate = [];
			component._aftercreate = [];
		}
	}

	function on(eventName, handler) {
		var handlers = this._handlers[eventName] || (this._handlers[eventName] = []);
		handlers.push(handler);

		return {
			cancel: function() {
				var index = handlers.indexOf(handler);
				if (~index) handlers.splice(index, 1);
			}
		};
	}

	function set(newState) {
		this._set(assign({}, newState));
		if (this.root._lock) return;
		flush(this.root);
	}

	function _set(newState) {
		var oldState = this._state,
			changed = {},
			dirty = false;

		newState = assign(this._staged, newState);
		this._staged = {};

		for (var key in newState) {
			if (this._differs(newState[key], oldState[key])) changed[key] = dirty = true;
		}
		if (!dirty) return;

		this._state = assign(assign({}, oldState), newState);
		this._recompute(changed, this._state);
		if (this._bind) this._bind(changed, this._state);

		if (this._fragment) {
			this.fire("state", { changed: changed, current: this._state, previous: oldState });
			this._fragment.p(changed, this._state);
			this.fire("update", { changed: changed, current: this._state, previous: oldState });
		}
	}

	function _stage(newState) {
		assign(this._staged, newState);
	}

	function setDev(newState) {
		if (typeof newState !== 'object') {
			throw new Error(
				this._debugName + '.set was called without an object of data key-values to update.'
			);
		}

		this._checkReadOnly(newState);
		set.call(this, newState);
	}

	function callAll(fns) {
		while (fns && fns.length) fns.shift()();
	}

	function _mount(target, anchor) {
		this._fragment[this._fragment.i ? 'i' : 'm'](target, anchor || null);
	}

	function removeFromStore() {
		this.store._remove(this);
	}

	var protoDev = {
		destroy: destroyDev,
		get,
		fire,
		on,
		set: setDev,
		_recompute: noop,
		_set,
		_stage,
		_mount,
		_differs
	};

	/* src/app/FieldLabel.html generated by Svelte v2.15.3 */

	const file = "src/app/FieldLabel.html";

	function create_main_fragment(component, ctx) {
		var h2, text;

		return {
			c: function create() {
				h2 = createElement("h2");
				text = createText(ctx.label);
				h2.className = "small-label title is-7 has-text-grey-lighter svelte-2t3k6x";
				addLoc(h2, file, 0, 0, 0);
			},

			m: function mount(target, anchor) {
				insert(target, h2, anchor);
				append(h2, text);
			},

			p: function update(changed, ctx) {
				if (changed.label) {
					setData(text, ctx.label);
				}
			},

			d: function destroy$$1(detach) {
				if (detach) {
					detachNode(h2);
				}
			}
		};
	}

	function FieldLabel(options) {
		this._debugName = '<FieldLabel>';
		if (!options || (!options.target && !options.root)) {
			throw new Error("'target' is a required option");
		}

		init(this, options);
		this._state = assign({}, options.data);
		if (!('label' in this._state)) console.warn("<FieldLabel> was created without expected data property 'label'");
		this._intro = true;

		this._fragment = create_main_fragment(this, this._state);

		if (options.target) {
			if (options.hydrate) throw new Error("options.hydrate only works if the component was compiled with the `hydratable: true` option");
			this._fragment.c();
			this._mount(options.target, options.anchor);
		}
	}

	assign(FieldLabel.prototype, protoDev);

	FieldLabel.prototype._checkReadOnly = function _checkReadOnly(newState) {
	};

	/* src/app/EditableHeaderTextField.html generated by Svelte v2.15.3 */

	function data() {
	  return {
	    label: "",
	    value: "",
	    oldValue: "",
	    edit: false,
	    small: true,
	    isNormal: false,
	    isNew: false,
	  }
	}
	var methods = {
	  isFinished(evt) {
	    const code = evt.keyCode;

	    if (code === 13 || code === 27) {
	      this.set({
	        edit: false,
	        isNew: false,
	      });

	      this.fire("change");
	    }
	  },

	  maybeClose() {
	    const { value, oldValue } = this.get();

	    // Close if no edits were made
	    if (value === oldValue) {
	      this.set({
	        edit: false,
	      });
	    }
	  }
	};

	function focus(node) {
	  const { isNew } = this.get();
	  node.focus();

	  if (isNew) {
	    setTimeout(() => {
	      node.select();
	    });
	  }
	}
	const file$1 = "src/app/EditableHeaderTextField.html";

	function create_main_fragment$1(component, ctx) {
		var text, if_block1_anchor;

		var if_block0 = (ctx.label) && create_if_block_1(component, ctx);

		function select_block_type(ctx) {
			if (ctx.edit) return create_if_block;
			return create_else_block;
		}

		var current_block_type = select_block_type(ctx);
		var if_block1 = current_block_type(component, ctx);

		return {
			c: function create() {
				if (if_block0) if_block0.c();
				text = createText("\n\n");
				if_block1.c();
				if_block1_anchor = createComment();
			},

			m: function mount(target, anchor) {
				if (if_block0) if_block0.m(target, anchor);
				insert(target, text, anchor);
				if_block1.m(target, anchor);
				insert(target, if_block1_anchor, anchor);
			},

			p: function update(changed, ctx) {
				if (ctx.label) {
					if (if_block0) {
						if_block0.p(changed, ctx);
					} else {
						if_block0 = create_if_block_1(component, ctx);
						if_block0.c();
						if_block0.m(text.parentNode, text);
					}
				} else if (if_block0) {
					if_block0.d(1);
					if_block0 = null;
				}

				if (current_block_type === (current_block_type = select_block_type(ctx)) && if_block1) {
					if_block1.p(changed, ctx);
				} else {
					if_block1.d(1);
					if_block1 = current_block_type(component, ctx);
					if_block1.c();
					if_block1.m(if_block1_anchor.parentNode, if_block1_anchor);
				}
			},

			d: function destroy$$1(detach) {
				if (if_block0) if_block0.d(detach);
				if (detach) {
					detachNode(text);
				}

				if_block1.d(detach);
				if (detach) {
					detachNode(if_block1_anchor);
				}
			}
		};
	}

	// (1:0) {#if label}
	function create_if_block_1(component, ctx) {

		var fieldlabel_initial_data = { label: ctx.label };
		var fieldlabel = new FieldLabel({
			root: component.root,
			store: component.store,
			data: fieldlabel_initial_data
		});

		return {
			c: function create() {
				fieldlabel._fragment.c();
			},

			m: function mount(target, anchor) {
				fieldlabel._mount(target, anchor);
			},

			p: function update(changed, ctx) {
				var fieldlabel_changes = {};
				if (changed.label) fieldlabel_changes.label = ctx.label;
				fieldlabel._set(fieldlabel_changes);
			},

			d: function destroy$$1(detach) {
				fieldlabel.destroy(detach);
			}
		};
	}

	// (14:0) {:else}
	function create_else_block(component, ctx) {
		var h2, text0, text1, button;

		function click_handler(event) {
			component.set({edit: true, oldValue: ctx.value});
		}

		return {
			c: function create() {
				h2 = createElement("h2");
				text0 = createText(ctx.value);
				text1 = createText("\n    ");
				button = createElement("button");
				button.textContent = "Edit";
				addListener(button, "click", click_handler);
				button.className = "edit-btn button is-link is-normal svelte-fpksqn";
				button.type = "button";
				addLoc(button, file$1, 20, 4, 365);
				h2.className = "title is-2 editable svelte-fpksqn";
				toggleClass(h2, "is-3", !ctx.small);
				toggleClass(h2, "is-5", ctx.small);
				toggleClass(h2, "title-normal", ctx.isNormal);
				addLoc(h2, file$1, 14, 2, 231);
			},

			m: function mount(target, anchor) {
				insert(target, h2, anchor);
				append(h2, text0);
				append(h2, text1);
				append(h2, button);
			},

			p: function update(changed, _ctx) {
				ctx = _ctx;
				if (changed.value) {
					setData(text0, ctx.value);
				}

				if (changed.small) {
					toggleClass(h2, "is-3", !ctx.small);
					toggleClass(h2, "is-5", ctx.small);
				}

				if (changed.isNormal) {
					toggleClass(h2, "title-normal", ctx.isNormal);
				}
			},

			d: function destroy$$1(detach) {
				if (detach) {
					detachNode(h2);
				}

				removeListener(button, "click", click_handler);
			}
		};
	}

	// (5:0) {#if edit}
	function create_if_block(component, ctx) {
		var input, input_updating = false, focus_action;

		function input_input_handler() {
			input_updating = true;
			component.set({ value: input.value });
			input_updating = false;
		}

		function keyup_handler(event) {
			component.isFinished(event);
		}

		function blur_handler(event) {
			component.maybeClose();
		}

		return {
			c: function create() {
				input = createElement("input");
				addListener(input, "input", input_input_handler);
				addListener(input, "keyup", keyup_handler);
				addListener(input, "blur", blur_handler);
				setAttribute(input, "type", "text");
				input.className = "input svelte-fpksqn";
				toggleClass(input, "is-large", !ctx.small);
				addLoc(input, file$1, 5, 2, 56);
			},

			m: function mount(target, anchor) {
				insert(target, input, anchor);
				focus_action = focus.call(component, input) || {};

				input.value = ctx.value;
			},

			p: function update(changed, ctx) {
				if (!input_updating && changed.value) input.value = ctx.value;
				if (changed.small) {
					toggleClass(input, "is-large", !ctx.small);
				}
			},

			d: function destroy$$1(detach) {
				if (detach) {
					detachNode(input);
				}

				removeListener(input, "input", input_input_handler);
				removeListener(input, "keyup", keyup_handler);
				removeListener(input, "blur", blur_handler);
				if (focus_action && typeof focus_action.destroy === 'function') focus_action.destroy.call(component);
			}
		};
	}

	function EditableHeaderTextField(options) {
		this._debugName = '<EditableHeaderTextField>';
		if (!options || (!options.target && !options.root)) {
			throw new Error("'target' is a required option");
		}

		init(this, options);
		this._state = assign(data(), options.data);
		if (!('label' in this._state)) console.warn("<EditableHeaderTextField> was created without expected data property 'label'");
		if (!('edit' in this._state)) console.warn("<EditableHeaderTextField> was created without expected data property 'edit'");
		if (!('small' in this._state)) console.warn("<EditableHeaderTextField> was created without expected data property 'small'");
		if (!('value' in this._state)) console.warn("<EditableHeaderTextField> was created without expected data property 'value'");
		if (!('isNormal' in this._state)) console.warn("<EditableHeaderTextField> was created without expected data property 'isNormal'");
		this._intro = true;

		this._fragment = create_main_fragment$1(this, this._state);

		if (options.target) {
			if (options.hydrate) throw new Error("options.hydrate only works if the component was compiled with the `hydratable: true` option");
			this._fragment.c();
			this._mount(options.target, options.anchor);

			flush(this);
		}
	}

	assign(EditableHeaderTextField.prototype, protoDev);
	assign(EditableHeaderTextField.prototype, methods);

	EditableHeaderTextField.prototype._checkReadOnly = function _checkReadOnly(newState) {
	};

	/* src/app/EditableTextAreaField.html generated by Svelte v2.15.3 */

	const displayStripSingleNewlinesRegexp = /([^\n])\n(?!\s*[\-*+])([^\n])/g;
	const displayMultipleNewlinesToOneRegexp = /\n+/g;
	const displayScriptRegexp = /<script((?:\s*(?:[^>"]+|"[^"]*"))*)>/g;
	const displayStyleRegexp = /<style((?:\s*(?:[^>"]+|"[^"]*"))*)>/g;

	function displayValue({ value }) {
	  // Single newlines to a space
	  value = value.replace(displayStripSingleNewlinesRegexp, "$1 $2");

	  // Put multiple newlines into one newlines
	  value = value.replace(displayMultipleNewlinesToOneRegexp, "\n");

	  // Make scripts and styles ineffective
	  value = value.replace(displayScriptRegexp, "&lt;script$1&gt;");
	  value = value.replace(displayStyleRegexp, "&lt;style$1&gt;");

	  // Convert newlines into `<br>`
	  value = value.replace(/\n/g, '</p>\n<p class="new-paragraph">');

	  return "<p>" + value + "</p>";
	}

	function data$1() {
	  return {
	    label: "",
	    edit: false,
	    small: true,
	    value: "",
	  }
	}
	var methods$1 = {
	  isFinished(evt) {
	    const code = evt.keyCode;

	    if (code === 27) {
	      this.set({
	        edit: false,
	      });
	    }
	  },
	};

	function focus$1(node) {
	  node.focus();
	}
	const file$2 = "src/app/EditableTextAreaField.html";

	function create_main_fragment$2(component, ctx) {
		var text, if_block1_anchor;

		var if_block0 = (ctx.label) && create_if_block_1$1(component, ctx);

		function select_block_type(ctx) {
			if (ctx.edit) return create_if_block$1;
			return create_else_block$1;
		}

		var current_block_type = select_block_type(ctx);
		var if_block1 = current_block_type(component, ctx);

		return {
			c: function create() {
				if (if_block0) if_block0.c();
				text = createText("\n\n");
				if_block1.c();
				if_block1_anchor = createComment();
			},

			m: function mount(target, anchor) {
				if (if_block0) if_block0.m(target, anchor);
				insert(target, text, anchor);
				if_block1.m(target, anchor);
				insert(target, if_block1_anchor, anchor);
			},

			p: function update(changed, ctx) {
				if (ctx.label) {
					if (if_block0) {
						if_block0.p(changed, ctx);
					} else {
						if_block0 = create_if_block_1$1(component, ctx);
						if_block0.c();
						if_block0.m(text.parentNode, text);
					}
				} else if (if_block0) {
					if_block0.d(1);
					if_block0 = null;
				}

				if (current_block_type === (current_block_type = select_block_type(ctx)) && if_block1) {
					if_block1.p(changed, ctx);
				} else {
					if_block1.d(1);
					if_block1 = current_block_type(component, ctx);
					if_block1.c();
					if_block1.m(if_block1_anchor.parentNode, if_block1_anchor);
				}
			},

			d: function destroy$$1(detach) {
				if (if_block0) if_block0.d(detach);
				if (detach) {
					detachNode(text);
				}

				if_block1.d(detach);
				if (detach) {
					detachNode(if_block1_anchor);
				}
			}
		};
	}

	// (1:0) {#if label}
	function create_if_block_1$1(component, ctx) {

		var fieldlabel_initial_data = { label: ctx.label };
		var fieldlabel = new FieldLabel({
			root: component.root,
			store: component.store,
			data: fieldlabel_initial_data
		});

		return {
			c: function create() {
				fieldlabel._fragment.c();
			},

			m: function mount(target, anchor) {
				fieldlabel._mount(target, anchor);
			},

			p: function update(changed, ctx) {
				var fieldlabel_changes = {};
				if (changed.label) fieldlabel_changes.label = ctx.label;
				fieldlabel._set(fieldlabel_changes);
			},

			d: function destroy$$1(detach) {
				fieldlabel.destroy(detach);
			}
		};
	}

	// (25:0) {:else}
	function create_else_block$1(component, ctx) {
		var div1, div0, text0, button, text1_value = ctx.$locale.edit, text1;

		function click_handler(event) {
			component.set({edit: true});
		}

		return {
			c: function create() {
				div1 = createElement("div");
				div0 = createElement("div");
				text0 = createText("\n\n    ");
				button = createElement("button");
				text1 = createText(text1_value);
				div0.className = "displayed-data svelte-1e9q0su";
				addLoc(div0, file$2, 26, 4, 516);
				addListener(button, "click", click_handler);
				button.className = "edit-btn button is-link svelte-1e9q0su";
				button.type = "button";
				addLoc(button, file$2, 30, 4, 588);
				div1.className = "editable has-text-grey svelte-1e9q0su";
				addLoc(div1, file$2, 25, 2, 475);
			},

			m: function mount(target, anchor) {
				insert(target, div1, anchor);
				append(div1, div0);
				div0.innerHTML = ctx.displayValue;
				append(div1, text0);
				append(div1, button);
				append(button, text1);
			},

			p: function update(changed, ctx) {
				if (changed.displayValue) {
					div0.innerHTML = ctx.displayValue;
				}

				if ((changed.$locale) && text1_value !== (text1_value = ctx.$locale.edit)) {
					setData(text1, text1_value);
				}
			},

			d: function destroy$$1(detach) {
				if (detach) {
					detachNode(div1);
				}

				removeListener(button, "click", click_handler);
			}
		};
	}

	// (5:0) {#if edit}
	function create_if_block$1(component, ctx) {
		var textarea, textarea_updating = false, focus_action, text0, div3, div0, text1, div2, div1, button, text2_value = ctx.$locale.done, text2;

		function textarea_input_handler() {
			textarea_updating = true;
			component.set({ value: textarea.value });
			textarea_updating = false;
		}

		function keyup_handler(event) {
			component.isFinished(event);
		}

		function click_handler(event) {
			component.set({edit: false});
		}

		return {
			c: function create() {
				textarea = createElement("textarea");
				text0 = createText("\n\n  ");
				div3 = createElement("div");
				div0 = createElement("div");
				text1 = createText("\n    ");
				div2 = createElement("div");
				div1 = createElement("div");
				button = createElement("button");
				text2 = createText(text2_value);
				addListener(textarea, "input", textarea_input_handler);
				addListener(textarea, "keyup", keyup_handler);
				setAttribute(textarea, "type", "text");
				textarea.className = "textarea svelte-1e9q0su";
				addLoc(textarea, file$2, 5, 2, 56);
				div0.className = "level-left";
				addLoc(div0, file$2, 14, 4, 203);
				addListener(button, "click", click_handler);
				button.type = "button";
				button.className = "button is-link";
				addLoc(button, file$2, 17, 8, 303);
				div1.className = "level-item";
				addLoc(div1, file$2, 16, 6, 270);
				div2.className = "level-right";
				addLoc(div2, file$2, 15, 4, 238);
				div3.className = "level";
				addLoc(div3, file$2, 13, 2, 179);
			},

			m: function mount(target, anchor) {
				insert(target, textarea, anchor);
				focus_action = focus$1.call(component, textarea) || {};

				textarea.value = ctx.value;

				insert(target, text0, anchor);
				insert(target, div3, anchor);
				append(div3, div0);
				append(div3, text1);
				append(div3, div2);
				append(div2, div1);
				append(div1, button);
				append(button, text2);
			},

			p: function update(changed, ctx) {
				if (!textarea_updating && changed.value) textarea.value = ctx.value;
				if ((changed.$locale) && text2_value !== (text2_value = ctx.$locale.done)) {
					setData(text2, text2_value);
				}
			},

			d: function destroy$$1(detach) {
				if (detach) {
					detachNode(textarea);
				}

				removeListener(textarea, "input", textarea_input_handler);
				removeListener(textarea, "keyup", keyup_handler);
				if (focus_action && typeof focus_action.destroy === 'function') focus_action.destroy.call(component);
				if (detach) {
					detachNode(text0);
					detachNode(div3);
				}

				removeListener(button, "click", click_handler);
			}
		};
	}

	function EditableTextAreaField(options) {
		this._debugName = '<EditableTextAreaField>';
		if (!options || (!options.target && !options.root)) {
			throw new Error("'target' is a required option");
		}
		if (!options.store) {
			throw new Error("<EditableTextAreaField> references store properties, but no store was provided");
		}

		init(this, options);
		this._state = assign(assign(this.store._init(["locale"]), data$1()), options.data);
		this.store._add(this, ["locale"]);

		this._recompute({ value: 1 }, this._state);
		if (!('value' in this._state)) console.warn("<EditableTextAreaField> was created without expected data property 'value'");
		if (!('label' in this._state)) console.warn("<EditableTextAreaField> was created without expected data property 'label'");
		if (!('edit' in this._state)) console.warn("<EditableTextAreaField> was created without expected data property 'edit'");
		if (!('$locale' in this._state)) console.warn("<EditableTextAreaField> was created without expected data property '$locale'");
		this._intro = true;

		this._handlers.destroy = [removeFromStore];

		this._fragment = create_main_fragment$2(this, this._state);

		if (options.target) {
			if (options.hydrate) throw new Error("options.hydrate only works if the component was compiled with the `hydratable: true` option");
			this._fragment.c();
			this._mount(options.target, options.anchor);

			flush(this);
		}
	}

	assign(EditableTextAreaField.prototype, protoDev);
	assign(EditableTextAreaField.prototype, methods$1);

	EditableTextAreaField.prototype._checkReadOnly = function _checkReadOnly(newState) {
		if ('displayValue' in newState && !this._updatingReadonlyProperty) throw new Error("<EditableTextAreaField>: Cannot set read-only property 'displayValue'");
	};

	EditableTextAreaField.prototype._recompute = function _recompute(changed, state) {
		if (changed.value) {
			if (this._differs(state.displayValue, (state.displayValue = displayValue(state)))) changed.displayValue = true;
		}
	};

	/* src/app/EditableTextField.html generated by Svelte v2.15.3 */

	function data$2() {
	  return {
	    label: "",
	    value: "",
	    oldValue: "",
	    prefix: "",
	    prefixFaded: false,

	    edit: false,
	    isNormal: false,
	    isNew: false,
	    isMono: false,
	  }
	}
	var methods$2 = {
	  isFinished(evt) {
	    const code = evt.keyCode;

	    if (code === 13 || code === 27) {
	      this.set({
	        edit: false,
	        isNew: false,
	      });

	      this.fire("change");
	    }
	  },

	  maybeClose() {
	    const { value, oldValue } = this.get();

	    // Close if no edits were made
	    if (value === oldValue) {
	      this.set({
	        edit: false,
	      });
	    }
	  }
	};

	function focus$2(node) {
	  const { isNew } = this.get();
	  node.focus();

	  if (isNew) {
	    setTimeout(() => {
	      node.select();
	    });
	  }
	}
	const file$3 = "src/app/EditableTextField.html";

	function create_main_fragment$3(component, ctx) {
		var div1, div0, text0, text1;

		var if_block0 = (ctx.label) && create_if_block_3(component, ctx);

		function select_block_type(ctx) {
			if (ctx.edit) return create_if_block_1$2;
			return create_else_block$2;
		}

		var current_block_type = select_block_type(ctx);
		var if_block1 = current_block_type(component, ctx);

		var if_block2 = (!ctx.edit) && create_if_block$2(component, ctx);

		return {
			c: function create() {
				div1 = createElement("div");
				div0 = createElement("div");
				if (if_block0) if_block0.c();
				text0 = createText("\n\n    ");
				if_block1.c();
				text1 = createText("\n\n  ");
				if (if_block2) if_block2.c();
				div0.className = "vertical-wrap svelte-yqvl6";
				toggleClass(div0, "is-editing", ctx.edit);
				addLoc(div0, file$3, 1, 2, 36);
				div1.className = "editable-field-wrap svelte-yqvl6";
				addLoc(div1, file$3, 0, 0, 0);
			},

			m: function mount(target, anchor) {
				insert(target, div1, anchor);
				append(div1, div0);
				if (if_block0) if_block0.m(div0, null);
				append(div0, text0);
				if_block1.m(div0, null);
				append(div1, text1);
				if (if_block2) if_block2.m(div1, null);
			},

			p: function update(changed, ctx) {
				if (ctx.label) {
					if (if_block0) {
						if_block0.p(changed, ctx);
					} else {
						if_block0 = create_if_block_3(component, ctx);
						if_block0.c();
						if_block0.m(div0, text0);
					}
				} else if (if_block0) {
					if_block0.d(1);
					if_block0 = null;
				}

				if (current_block_type === (current_block_type = select_block_type(ctx)) && if_block1) {
					if_block1.p(changed, ctx);
				} else {
					if_block1.d(1);
					if_block1 = current_block_type(component, ctx);
					if_block1.c();
					if_block1.m(div0, null);
				}

				if (changed.edit) {
					toggleClass(div0, "is-editing", ctx.edit);
				}

				if (!ctx.edit) {
					if (if_block2) {
						if_block2.p(changed, ctx);
					} else {
						if_block2 = create_if_block$2(component, ctx);
						if_block2.c();
						if_block2.m(div1, null);
					}
				} else if (if_block2) {
					if_block2.d(1);
					if_block2 = null;
				}
			},

			d: function destroy$$1(detach) {
				if (detach) {
					detachNode(div1);
				}

				if (if_block0) if_block0.d();
				if_block1.d();
				if (if_block2) if_block2.d();
			}
		};
	}

	// (5:4) {#if label}
	function create_if_block_3(component, ctx) {

		var fieldlabel_initial_data = { label: ctx.label };
		var fieldlabel = new FieldLabel({
			root: component.root,
			store: component.store,
			data: fieldlabel_initial_data
		});

		return {
			c: function create() {
				fieldlabel._fragment.c();
			},

			m: function mount(target, anchor) {
				fieldlabel._mount(target, anchor);
			},

			p: function update(changed, ctx) {
				var fieldlabel_changes = {};
				if (changed.label) fieldlabel_changes.label = ctx.label;
				fieldlabel._set(fieldlabel_changes);
			},

			d: function destroy$$1(detach) {
				fieldlabel.destroy(detach);
			}
		};
	}

	// (17:4) {:else}
	function create_else_block$2(component, ctx) {
		var h2, text0, text1;

		var if_block = (ctx.prefix) && create_if_block_2(component, ctx);

		return {
			c: function create() {
				h2 = createElement("h2");
				if (if_block) if_block.c();
				text0 = createText("\n        ");
				text1 = createText(ctx.value);
				h2.className = "title is-6 editable svelte-yqvl6";
				toggleClass(h2, "title-normal", ctx.isNormal);
				toggleClass(h2, "is-mono", ctx.isMono);
				addLoc(h2, file$3, 17, 6, 351);
			},

			m: function mount(target, anchor) {
				insert(target, h2, anchor);
				if (if_block) if_block.m(h2, null);
				append(h2, text0);
				append(h2, text1);
			},

			p: function update(changed, ctx) {
				if (ctx.prefix) {
					if (if_block) {
						if_block.p(changed, ctx);
					} else {
						if_block = create_if_block_2(component, ctx);
						if_block.c();
						if_block.m(h2, text0);
					}
				} else if (if_block) {
					if_block.d(1);
					if_block = null;
				}

				if (changed.value) {
					setData(text1, ctx.value);
				}

				if (changed.isNormal) {
					toggleClass(h2, "title-normal", ctx.isNormal);
				}

				if (changed.isMono) {
					toggleClass(h2, "is-mono", ctx.isMono);
				}
			},

			d: function destroy$$1(detach) {
				if (detach) {
					detachNode(h2);
				}

				if (if_block) if_block.d();
			}
		};
	}

	// (9:4) {#if edit}
	function create_if_block_1$2(component, ctx) {
		var input, input_updating = false, focus_action;

		function input_input_handler() {
			input_updating = true;
			component.set({ value: input.value });
			input_updating = false;
		}

		function keyup_handler(event) {
			component.isFinished(event);
		}

		function blur_handler(event) {
			component.maybeClose();
		}

		return {
			c: function create() {
				input = createElement("input");
				addListener(input, "input", input_input_handler);
				addListener(input, "keyup", keyup_handler);
				addListener(input, "blur", blur_handler);
				setAttribute(input, "type", "text");
				input.className = "input";
				addLoc(input, file$3, 9, 6, 172);
			},

			m: function mount(target, anchor) {
				insert(target, input, anchor);
				focus_action = focus$2.call(component, input) || {};

				input.value = ctx.value;
			},

			p: function update(changed, ctx) {
				if (!input_updating && changed.value) input.value = ctx.value;
			},

			d: function destroy$$1(detach) {
				if (detach) {
					detachNode(input);
				}

				removeListener(input, "input", input_input_handler);
				removeListener(input, "keyup", keyup_handler);
				removeListener(input, "blur", blur_handler);
				if (focus_action && typeof focus_action.destroy === 'function') focus_action.destroy.call(component);
			}
		};
	}

	// (22:8) {#if prefix}
	function create_if_block_2(component, ctx) {
		var span, text;

		return {
			c: function create() {
				span = createElement("span");
				text = createText(ctx.prefix);
				span.className = "editable-prefix svelte-yqvl6";
				toggleClass(span, "is-faded-prefix", ctx.prefixFaded);
				addLoc(span, file$3, 22, 10, 492);
			},

			m: function mount(target, anchor) {
				insert(target, span, anchor);
				append(span, text);
			},

			p: function update(changed, ctx) {
				if (changed.prefix) {
					setData(text, ctx.prefix);
				}

				if (changed.prefixFaded) {
					toggleClass(span, "is-faded-prefix", ctx.prefixFaded);
				}
			},

			d: function destroy$$1(detach) {
				if (detach) {
					detachNode(span);
				}
			}
		};
	}

	// (34:2) {#if !edit}
	function create_if_block$2(component, ctx) {
		var button, text_value = ctx.$locale.edit, text;

		function click_handler(event) {
			component.set({edit: true, oldValue: ctx.value});
		}

		return {
			c: function create() {
				button = createElement("button");
				text = createText(text_value);
				addListener(button, "click", click_handler);
				button.className = "edit-btn button is-link is-normal svelte-yqvl6";
				button.type = "button";
				addLoc(button, file$3, 34, 4, 702);
			},

			m: function mount(target, anchor) {
				insert(target, button, anchor);
				append(button, text);
			},

			p: function update(changed, _ctx) {
				ctx = _ctx;
				if ((changed.$locale) && text_value !== (text_value = ctx.$locale.edit)) {
					setData(text, text_value);
				}
			},

			d: function destroy$$1(detach) {
				if (detach) {
					detachNode(button);
				}

				removeListener(button, "click", click_handler);
			}
		};
	}

	function EditableTextField(options) {
		this._debugName = '<EditableTextField>';
		if (!options || (!options.target && !options.root)) {
			throw new Error("'target' is a required option");
		}
		if (!options.store) {
			throw new Error("<EditableTextField> references store properties, but no store was provided");
		}

		init(this, options);
		this._state = assign(assign(this.store._init(["locale"]), data$2()), options.data);
		this.store._add(this, ["locale"]);
		if (!('edit' in this._state)) console.warn("<EditableTextField> was created without expected data property 'edit'");
		if (!('label' in this._state)) console.warn("<EditableTextField> was created without expected data property 'label'");
		if (!('value' in this._state)) console.warn("<EditableTextField> was created without expected data property 'value'");
		if (!('isNormal' in this._state)) console.warn("<EditableTextField> was created without expected data property 'isNormal'");
		if (!('isMono' in this._state)) console.warn("<EditableTextField> was created without expected data property 'isMono'");
		if (!('prefix' in this._state)) console.warn("<EditableTextField> was created without expected data property 'prefix'");
		if (!('prefixFaded' in this._state)) console.warn("<EditableTextField> was created without expected data property 'prefixFaded'");
		if (!('$locale' in this._state)) console.warn("<EditableTextField> was created without expected data property '$locale'");
		this._intro = true;

		this._handlers.destroy = [removeFromStore];

		this._fragment = create_main_fragment$3(this, this._state);

		if (options.target) {
			if (options.hydrate) throw new Error("options.hydrate only works if the component was compiled with the `hydratable: true` option");
			this._fragment.c();
			this._mount(options.target, options.anchor);

			flush(this);
		}
	}

	assign(EditableTextField.prototype, protoDev);
	assign(EditableTextField.prototype, methods$2);

	EditableTextField.prototype._checkReadOnly = function _checkReadOnly(newState) {
	};

	/* src/app/Tabs.html generated by Svelte v2.15.3 */

	function data$3() {
	  return {
	    current: null,
	  }
	}
	const file$4 = "src/app/Tabs.html";

	function click_handler(event) {
		const { component, ctx } = this._svelte;

		component.set({current: ctx.tab.id});
	}

	function get_each_context(ctx, list, i) {
		const child_ctx = Object.create(ctx);
		child_ctx.tab = list[i];
		return child_ctx;
	}

	function create_main_fragment$4(component, ctx) {
		var div, ul;

		var each_value = ctx.tabs;

		var each_blocks = [];

		for (var i = 0; i < each_value.length; i += 1) {
			each_blocks[i] = create_each_block(component, get_each_context(ctx, each_value, i));
		}

		return {
			c: function create() {
				div = createElement("div");
				ul = createElement("ul");

				for (var i = 0; i < each_blocks.length; i += 1) {
					each_blocks[i].c();
				}
				addLoc(ul, file$4, 1, 2, 83);
				div.className = "tabs is-fullwidth is-toggle is-toggle-rounded is-small is-centered svelte-z1z0bp";
				addLoc(div, file$4, 0, 0, 0);
			},

			m: function mount(target, anchor) {
				insert(target, div, anchor);
				append(div, ul);

				for (var i = 0; i < each_blocks.length; i += 1) {
					each_blocks[i].m(ul, null);
				}
			},

			p: function update(changed, ctx) {
				if (changed.tabs || changed.current) {
					each_value = ctx.tabs;

					for (var i = 0; i < each_value.length; i += 1) {
						const child_ctx = get_each_context(ctx, each_value, i);

						if (each_blocks[i]) {
							each_blocks[i].p(changed, child_ctx);
						} else {
							each_blocks[i] = create_each_block(component, child_ctx);
							each_blocks[i].c();
							each_blocks[i].m(ul, null);
						}
					}

					for (; i < each_blocks.length; i += 1) {
						each_blocks[i].d(1);
					}
					each_blocks.length = each_value.length;
				}
			},

			d: function destroy$$1(detach) {
				if (detach) {
					detachNode(div);
				}

				destroyEach(each_blocks, detach);
			}
		};
	}

	// (3:4) {#each tabs as tab}
	function create_each_block(component, ctx) {
		var li, a, span, text0_value = ctx.tab.text, text0, text1;

		return {
			c: function create() {
				li = createElement("li");
				a = createElement("a");
				span = createElement("span");
				text0 = createText(text0_value);
				text1 = createText("\n      ");
				addLoc(span, file$4, 7, 10, 255);
				a.href = "javascript:0[0]";
				addLoc(a, file$4, 6, 8, 218);

				li._svelte = { component, ctx };

				addListener(li, "click", click_handler);
				toggleClass(li, "is-active", ctx.tab.id === ctx.current);
				addLoc(li, file$4, 3, 6, 118);
			},

			m: function mount(target, anchor) {
				insert(target, li, anchor);
				append(li, a);
				append(a, span);
				append(span, text0);
				append(li, text1);
			},

			p: function update(changed, _ctx) {
				ctx = _ctx;
				if ((changed.tabs) && text0_value !== (text0_value = ctx.tab.text)) {
					setData(text0, text0_value);
				}

				li._svelte.ctx = ctx;
				if ((changed.tabs || changed.current)) {
					toggleClass(li, "is-active", ctx.tab.id === ctx.current);
				}
			},

			d: function destroy$$1(detach) {
				if (detach) {
					detachNode(li);
				}

				removeListener(li, "click", click_handler);
			}
		};
	}

	function Tabs(options) {
		this._debugName = '<Tabs>';
		if (!options || (!options.target && !options.root)) {
			throw new Error("'target' is a required option");
		}

		init(this, options);
		this._state = assign(data$3(), options.data);
		if (!('tabs' in this._state)) console.warn("<Tabs> was created without expected data property 'tabs'");
		if (!('current' in this._state)) console.warn("<Tabs> was created without expected data property 'current'");
		this._intro = true;

		this._fragment = create_main_fragment$4(this, this._state);

		if (options.target) {
			if (options.hydrate) throw new Error("options.hydrate only works if the component was compiled with the `hydratable: true` option");
			this._fragment.c();
			this._mount(options.target, options.anchor);
		}
	}

	assign(Tabs.prototype, protoDev);

	Tabs.prototype._checkReadOnly = function _checkReadOnly(newState) {
	};

	/* src/app/SaveDiscardButtons.html generated by Svelte v2.15.3 */

	const file$5 = "src/app/SaveDiscardButtons.html";

	function create_main_fragment$5(component, ctx) {
		var div4, div0, text0, div3, div1, button0, text1_value = ctx.$locale.save, text1, text2, div2, button1, text3_value = ctx.$locale.cancel, text3;

		function click_handler(event) {
			component.fire('save');
		}

		function click_handler_1(event) {
			component.set({editing: false});
		}

		return {
			c: function create() {
				div4 = createElement("div");
				div0 = createElement("div");
				text0 = createText("\n  ");
				div3 = createElement("div");
				div1 = createElement("div");
				button0 = createElement("button");
				text1 = createText(text1_value);
				text2 = createText("\n    ");
				div2 = createElement("div");
				button1 = createElement("button");
				text3 = createText(text3_value);
				div0.className = "level-left";
				addLoc(div0, file$5, 1, 2, 32);
				addListener(button0, "click", click_handler);
				button0.className = "button is-link svelte-9jzqto";
				button0.type = "button";
				toggleClass(button0, "is-loading", ctx.$savingCourse);
				addLoc(button0, file$5, 4, 6, 126);
				div1.className = "level-item";
				addLoc(div1, file$5, 3, 4, 95);
				addListener(button1, "click", click_handler_1);
				button1.className = "button is-danger";
				button1.type = "button";
				addLoc(button1, file$5, 13, 6, 346);
				div2.className = "level-item";
				addLoc(div2, file$5, 12, 4, 315);
				div3.className = "level-right";
				addLoc(div3, file$5, 2, 2, 65);
				div4.className = "level is-mobile";
				addLoc(div4, file$5, 0, 0, 0);
			},

			m: function mount(target, anchor) {
				insert(target, div4, anchor);
				append(div4, div0);
				append(div4, text0);
				append(div4, div3);
				append(div3, div1);
				append(div1, button0);
				append(button0, text1);
				append(div3, text2);
				append(div3, div2);
				append(div2, button1);
				append(button1, text3);
			},

			p: function update(changed, ctx) {
				if ((changed.$locale) && text1_value !== (text1_value = ctx.$locale.save)) {
					setData(text1, text1_value);
				}

				if (changed.$savingCourse) {
					toggleClass(button0, "is-loading", ctx.$savingCourse);
				}

				if ((changed.$locale) && text3_value !== (text3_value = ctx.$locale.cancel)) {
					setData(text3, text3_value);
				}
			},

			d: function destroy$$1(detach) {
				if (detach) {
					detachNode(div4);
				}

				removeListener(button0, "click", click_handler);
				removeListener(button1, "click", click_handler_1);
			}
		};
	}

	function SaveDiscardButtons(options) {
		this._debugName = '<SaveDiscardButtons>';
		if (!options || (!options.target && !options.root)) {
			throw new Error("'target' is a required option");
		}
		if (!options.store) {
			throw new Error("<SaveDiscardButtons> references store properties, but no store was provided");
		}

		init(this, options);
		this._state = assign(this.store._init(["savingCourse","locale"]), options.data);
		this.store._add(this, ["savingCourse","locale"]);
		if (!('$savingCourse' in this._state)) console.warn("<SaveDiscardButtons> was created without expected data property '$savingCourse'");
		if (!('$locale' in this._state)) console.warn("<SaveDiscardButtons> was created without expected data property '$locale'");
		this._intro = true;

		this._handlers.destroy = [removeFromStore];

		this._fragment = create_main_fragment$5(this, this._state);

		if (options.target) {
			if (options.hydrate) throw new Error("options.hydrate only works if the component was compiled with the `hydratable: true` option");
			this._fragment.c();
			this._mount(options.target, options.anchor);
		}
	}

	assign(SaveDiscardButtons.prototype, protoDev);

	SaveDiscardButtons.prototype._checkReadOnly = function _checkReadOnly(newState) {
	};

	/* src/app/CourseList.html generated by Svelte v2.15.3 */

	var methods$3 = {
	  toggleCategoryAvailability(tid, available) {
	    this.store.fire("setcategoryavailability", {
	      tid,
	      available: !available,
	    });
	  },

	  courseAction(e, { pid }) {
	    let target = e.target;
	    let action = "loadcourse";

	    while (!target.classList.contains("course-row")) {
	      if (target.dataset.action) {
	        action = target.dataset.action;
	        break;
	      }

	      target = target.parentNode;
	    }

	    this.store.fire(action, {pid});
	  },
	};

	const file$6 = "src/app/CourseList.html";

	function click_handler_2(event) {
		const { component, ctx } = this._svelte;

		component.courseAction(event, {pid: ctx.course.pid});
	}

	function get_each_context_1(ctx, list, i) {
		const child_ctx = Object.create(ctx);
		child_ctx.course = list[i];
		return child_ctx;
	}

	function click_handler_1(event) {
		const { component, ctx } = this._svelte;

		component.store.fire('newcourse', {category: +ctx.category.tid});
	}

	function click_handler$1(event) {
		const { component, ctx } = this._svelte;

		component.toggleCategoryAvailability(ctx.category.tid, ctx.category.available);
	}

	function get_each_context$1(ctx, list, i) {
		const child_ctx = Object.create(ctx);
		child_ctx.category = list[i];
		return child_ctx;
	}

	function create_main_fragment$6(component, ctx) {
		var div;

		var each_value = ctx.list;

		var each_blocks = [];

		for (var i = 0; i < each_value.length; i += 1) {
			each_blocks[i] = create_each_block$1(component, get_each_context$1(ctx, each_value, i));
		}

		return {
			c: function create() {
				div = createElement("div");

				for (var i = 0; i < each_blocks.length; i += 1) {
					each_blocks[i].c();
				}
				div.className = "menu";
				addLoc(div, file$6, 0, 0, 0);
			},

			m: function mount(target, anchor) {
				insert(target, div, anchor);

				for (var i = 0; i < each_blocks.length; i += 1) {
					each_blocks[i].m(div, null);
				}
			},

			p: function update(changed, ctx) {
				if (changed.list || changed.$locale) {
					each_value = ctx.list;

					for (var i = 0; i < each_value.length; i += 1) {
						const child_ctx = get_each_context$1(ctx, each_value, i);

						if (each_blocks[i]) {
							each_blocks[i].p(changed, child_ctx);
						} else {
							each_blocks[i] = create_each_block$1(component, child_ctx);
							each_blocks[i].c();
							each_blocks[i].m(div, null);
						}
					}

					for (; i < each_blocks.length; i += 1) {
						each_blocks[i].d(1);
					}
					each_blocks.length = each_value.length;
				}
			},

			d: function destroy$$1(detach) {
				if (detach) {
					detachNode(div);
				}

				destroyEach(each_blocks, detach);
			}
		};
	}

	// (37:6) {#each category.programs as course}
	function create_each_block_1(component, ctx) {
		var li, a, span0, i0, text0, text1_value = ctx.course.data.current.name, text1, text2, div, button, span1, i1, button_title_value;

		return {
			c: function create() {
				li = createElement("li");
				a = createElement("a");
				span0 = createElement("span");
				i0 = createElement("i");
				text0 = createText("\n            ");
				text1 = createText(text1_value);
				text2 = createText("\n\n            ");
				div = createElement("div");
				button = createElement("button");
				span1 = createElement("span");
				i1 = createElement("i");
				i0.className = "fa fa-book";
				addLoc(i0, file$6, 43, 14, 1234);
				span0.className = "icon";
				addLoc(span0, file$6, 42, 12, 1200);
				i1.className = "fa fa-trash-o";
				addLoc(i1, file$6, 54, 18, 1593);
				span1.className = "icon";
				addLoc(span1, file$6, 53, 16, 1555);
				button.className = "action svelte-1m83cem";
				button.dataset.action = "deletecourse";
				button.type = "button";
				button.title = button_title_value = ctx.$locale.courseActionDelete;
				addLoc(button, file$6, 48, 14, 1373);
				div.className = "button-list svelte-1m83cem";
				addLoc(div, file$6, 47, 12, 1333);

				a._svelte = { component, ctx };

				addListener(a, "click", click_handler_2);
				a.className = "has-text-link course-row svelte-1m83cem";
				a.href = "javascript:0[0]";
				addLoc(a, file$6, 38, 10, 1042);
				li.className = "svelte-1m83cem";
				addLoc(li, file$6, 37, 8, 1027);
			},

			m: function mount(target, anchor) {
				insert(target, li, anchor);
				append(li, a);
				append(a, span0);
				append(span0, i0);
				append(a, text0);
				append(a, text1);
				append(a, text2);
				append(a, div);
				append(div, button);
				append(button, span1);
				append(span1, i1);
			},

			p: function update(changed, _ctx) {
				ctx = _ctx;
				if ((changed.list) && text1_value !== (text1_value = ctx.course.data.current.name)) {
					setData(text1, text1_value);
				}

				if ((changed.$locale) && button_title_value !== (button_title_value = ctx.$locale.courseActionDelete)) {
					button.title = button_title_value;
				}

				a._svelte.ctx = ctx;
			},

			d: function destroy$$1(detach) {
				if (detach) {
					detachNode(li);
				}

				removeListener(a, "click", click_handler_2);
			}
		};
	}

	// (2:2) {#each list as category}
	function create_each_block$1(component, ctx) {
		var div1, text0_value = ctx.category.data.current.name, text0, text1, div0, button, span0, i0, button_title_value, text2, ul, li, a, span1, i1, text3, text4_value = ctx.$locale.addCourse, text4, text5, text6;

		var each_value_1 = ctx.category.programs;

		var each_blocks = [];

		for (var i = 0; i < each_value_1.length; i += 1) {
			each_blocks[i] = create_each_block_1(component, get_each_context_1(ctx, each_value_1, i));
		}

		return {
			c: function create() {
				div1 = createElement("div");
				text0 = createText(text0_value);
				text1 = createText("\n\n      ");
				div0 = createElement("div");
				button = createElement("button");
				span0 = createElement("span");
				i0 = createElement("i");
				text2 = createText("\n    ");
				ul = createElement("ul");
				li = createElement("li");
				a = createElement("a");
				span1 = createElement("span");
				i1 = createElement("i");
				text3 = createText("\n          ");
				text4 = createText(text4_value);
				text5 = createText("\n\n      \n      ");

				for (var i = 0; i < each_blocks.length; i += 1) {
					each_blocks[i].c();
				}

				text6 = createText("\n    ");
				i0.className = "fa fa-lock svelte-1m83cem";
				addLoc(i0, file$6, 15, 12, 512);
				span0.className = "icon";
				toggleClass(span0, "has-text-danger", !ctx.category.available);
				toggleClass(span0, "has-text-grey-lighter", ctx.category.available);
				addLoc(span0, file$6, 11, 10, 351);

				button._svelte = { component, ctx };

				addListener(button, "click", click_handler$1);
				button.className = "action svelte-1m83cem";
				button.type = "button";
				button.title = button_title_value = ctx.$locale.categoryActionDisable;
				addLoc(button, file$6, 6, 8, 151);
				div0.className = "button-list svelte-1m83cem";
				addLoc(div0, file$6, 5, 6, 117);
				div1.className = "menu-label svelte-1m83cem";
				addLoc(div1, file$6, 2, 4, 50);
				i1.className = "fa fa-plus";
				addLoc(i1, file$6, 29, 12, 849);
				span1.className = "icon";
				addLoc(span1, file$6, 28, 10, 817);

				a._svelte = { component, ctx };

				addListener(a, "click", click_handler_1);
				a.href = "javascript:0[0]";
				a.className = "is-active svelte-1m83cem";
				addLoc(a, file$6, 24, 8, 675);
				li.className = "svelte-1m83cem";
				addLoc(li, file$6, 23, 6, 662);
				ul.className = "menu-list svelte-1m83cem";
				addLoc(ul, file$6, 20, 4, 603);
			},

			m: function mount(target, anchor) {
				insert(target, div1, anchor);
				append(div1, text0);
				append(div1, text1);
				append(div1, div0);
				append(div0, button);
				append(button, span0);
				append(span0, i0);
				insert(target, text2, anchor);
				insert(target, ul, anchor);
				append(ul, li);
				append(li, a);
				append(a, span1);
				append(span1, i1);
				append(a, text3);
				append(a, text4);
				append(ul, text5);

				for (var i = 0; i < each_blocks.length; i += 1) {
					each_blocks[i].m(ul, null);
				}

				append(ul, text6);
			},

			p: function update(changed, _ctx) {
				ctx = _ctx;
				if ((changed.list) && text0_value !== (text0_value = ctx.category.data.current.name)) {
					setData(text0, text0_value);
				}

				if (changed.list) {
					toggleClass(span0, "has-text-danger", !ctx.category.available);
					toggleClass(span0, "has-text-grey-lighter", ctx.category.available);
				}

				button._svelte.ctx = ctx;
				if ((changed.$locale) && button_title_value !== (button_title_value = ctx.$locale.categoryActionDisable)) {
					button.title = button_title_value;
				}

				if ((changed.$locale) && text4_value !== (text4_value = ctx.$locale.addCourse)) {
					setData(text4, text4_value);
				}

				a._svelte.ctx = ctx;

				if (changed.list || changed.$locale) {
					each_value_1 = ctx.category.programs;

					for (var i = 0; i < each_value_1.length; i += 1) {
						const child_ctx = get_each_context_1(ctx, each_value_1, i);

						if (each_blocks[i]) {
							each_blocks[i].p(changed, child_ctx);
						} else {
							each_blocks[i] = create_each_block_1(component, child_ctx);
							each_blocks[i].c();
							each_blocks[i].m(ul, text6);
						}
					}

					for (; i < each_blocks.length; i += 1) {
						each_blocks[i].d(1);
					}
					each_blocks.length = each_value_1.length;
				}
			},

			d: function destroy$$1(detach) {
				if (detach) {
					detachNode(div1);
				}

				removeListener(button, "click", click_handler$1);
				if (detach) {
					detachNode(text2);
					detachNode(ul);
				}

				removeListener(a, "click", click_handler_1);

				destroyEach(each_blocks, detach);
			}
		};
	}

	function CourseList(options) {
		this._debugName = '<CourseList>';
		if (!options || (!options.target && !options.root)) {
			throw new Error("'target' is a required option");
		}
		if (!options.store) {
			throw new Error("<CourseList> references store properties, but no store was provided");
		}

		init(this, options);
		this._state = assign(this.store._init(["locale"]), options.data);
		this.store._add(this, ["locale"]);
		if (!('list' in this._state)) console.warn("<CourseList> was created without expected data property 'list'");
		if (!('$locale' in this._state)) console.warn("<CourseList> was created without expected data property '$locale'");
		this._intro = true;

		this._handlers.destroy = [removeFromStore];

		this._fragment = create_main_fragment$6(this, this._state);

		if (options.target) {
			if (options.hydrate) throw new Error("options.hydrate only works if the component was compiled with the `hydratable: true` option");
			this._fragment.c();
			this._mount(options.target, options.anchor);
		}
	}

	assign(CourseList.prototype, protoDev);
	assign(CourseList.prototype, methods$3);

	CourseList.prototype._checkReadOnly = function _checkReadOnly(newState) {
	};

	/* src/app/Dropdown.html generated by Svelte v2.15.3 */

	function _index({ value, list }) {
	  let index = 0;

	  for (let i=0; i<list.length; i++) {
	    const item = list[i];

	    if (item[0] === value) {
	      index = i;
	      break;
	    }
	  }

	  return index;
	}

	function data$4() {
	  return {
	    value: null,
	    list: [],
	    visible: false,
	    disabled: false,
	    right: false,
	  }
	}
	var methods$4 = {
	  change(value) {
	    this.set({
	      value,
	      visible: false,
	    });
	    this.fire("change", {
	      value,
	    });
	  }
	};

	function oncreate() {
	  const listener = (e) => {
	    let target = e.target;

	    while (target !== document.body) {
	      if (target === this.refs.dropdown) {
	        return;
	      }

	      target = target.parentNode;
	    }

	    this.set({
	      visible: false,
	    });
	  };

	  this.on("state", ({ current }) => {
	    if (current.visible) {
	      window.addEventListener("click", listener);
	    } else {
	      window.removeEventListener("click", listener);
	    }
	  });

	  this.on("destroy", () => {
	    window.removeEventListener("click", listener);
	  });
	}
	const file$7 = "src/app/Dropdown.html";

	function click_handler$2(event) {
		const { component, ctx } = this._svelte;

		component.change(ctx.item[0]);
	}

	function get_each_context$2(ctx, list, i) {
		const child_ctx = Object.create(ctx);
		child_ctx.item = list[i];
		return child_ctx;
	}

	function create_main_fragment$7(component, ctx) {
		var div3, div0, button, span0, text0_value = (ctx.list[ctx._index] || "  ")[1], text0, text1, span1, i, text2, div2, div1;

		function click_handler(event) {
			component.set({visible: !ctx.disabled});
		}

		var each_value = ctx.list;

		var each_blocks = [];

		for (var i_1 = 0; i_1 < each_value.length; i_1 += 1) {
			each_blocks[i_1] = create_each_block$2(component, get_each_context$2(ctx, each_value, i_1));
		}

		return {
			c: function create() {
				div3 = createElement("div");
				div0 = createElement("div");
				button = createElement("button");
				span0 = createElement("span");
				text0 = createText(text0_value);
				text1 = createText("\n\n      ");
				span1 = createElement("span");
				i = createElement("i");
				text2 = createText("\n  ");
				div2 = createElement("div");
				div1 = createElement("div");

				for (var i_1 = 0; i_1 < each_blocks.length; i_1 += 1) {
					each_blocks[i_1].c();
				}
				addLoc(span0, file$7, 14, 6, 312);
				i.className = "fa fa-angle-down";
				addLoc(i, file$7, 17, 8, 399);
				span1.className = "icon is-small";
				addLoc(span1, file$7, 16, 6, 362);
				addListener(button, "click", click_handler);
				button.className = "button";
				button.type = "button";
				addLoc(button, file$7, 8, 4, 182);
				div0.className = "dropdown-trigger";
				addLoc(div0, file$7, 7, 2, 147);
				div1.className = "dropdown-content";
				addLoc(div1, file$7, 22, 4, 503);
				div2.className = "dropdown-menu";
				addLoc(div2, file$7, 21, 2, 471);
				div3.className = "dropdown svelte-1811gh4";
				div3.tabIndex = "0";
				toggleClass(div3, "dropdown-disabled", ctx.disabled);
				toggleClass(div3, "is-active", ctx.visible);
				toggleClass(div3, "is-right", ctx.right);
				addLoc(div3, file$7, 0, 0, 0);
			},

			m: function mount(target, anchor) {
				insert(target, div3, anchor);
				append(div3, div0);
				append(div0, button);
				append(button, span0);
				append(span0, text0);
				append(button, text1);
				append(button, span1);
				append(span1, i);
				append(div3, text2);
				append(div3, div2);
				append(div2, div1);

				for (var i_1 = 0; i_1 < each_blocks.length; i_1 += 1) {
					each_blocks[i_1].m(div1, null);
				}

				component.refs.dropdown = div3;
			},

			p: function update(changed, _ctx) {
				ctx = _ctx;
				if ((changed.list || changed._index) && text0_value !== (text0_value = (ctx.list[ctx._index] || "  ")[1])) {
					setData(text0, text0_value);
				}

				if (changed.list) {
					each_value = ctx.list;

					for (var i_1 = 0; i_1 < each_value.length; i_1 += 1) {
						const child_ctx = get_each_context$2(ctx, each_value, i_1);

						if (each_blocks[i_1]) {
							each_blocks[i_1].p(changed, child_ctx);
						} else {
							each_blocks[i_1] = create_each_block$2(component, child_ctx);
							each_blocks[i_1].c();
							each_blocks[i_1].m(div1, null);
						}
					}

					for (; i_1 < each_blocks.length; i_1 += 1) {
						each_blocks[i_1].d(1);
					}
					each_blocks.length = each_value.length;
				}

				if (changed.disabled) {
					toggleClass(div3, "dropdown-disabled", ctx.disabled);
				}

				if (changed.visible) {
					toggleClass(div3, "is-active", ctx.visible);
				}

				if (changed.right) {
					toggleClass(div3, "is-right", ctx.right);
				}
			},

			d: function destroy$$1(detach) {
				if (detach) {
					detachNode(div3);
				}

				removeListener(button, "click", click_handler);

				destroyEach(each_blocks, detach);

				if (component.refs.dropdown === div3) component.refs.dropdown = null;
			}
		};
	}

	// (27:8) {:else}
	function create_else_block$3(component, ctx) {
		var a, text0_value = ctx.item[1], text0, text1;

		return {
			c: function create() {
				a = createElement("a");
				text0 = createText(text0_value);
				text1 = createText("\n          ");
				a._svelte = { component, ctx };

				addListener(a, "click", click_handler$2);
				a.href = "javascript:0[0]";
				a.className = "dropdown-item";
				addLoc(a, file$7, 27, 10, 652);
			},

			m: function mount(target, anchor) {
				insert(target, a, anchor);
				append(a, text0);
				append(a, text1);
			},

			p: function update(changed, _ctx) {
				ctx = _ctx;
				if ((changed.list) && text0_value !== (text0_value = ctx.item[1])) {
					setData(text0, text0_value);
				}

				a._svelte.ctx = ctx;
			},

			d: function destroy$$1(detach) {
				if (detach) {
					detachNode(a);
				}

				removeListener(a, "click", click_handler$2);
			}
		};
	}

	// (25:8) {#if item === 0}
	function create_if_block$3(component, ctx) {
		var hr;

		return {
			c: function create() {
				hr = createElement("hr");
				hr.className = "dropdown-divider";
				addLoc(hr, file$7, 25, 10, 596);
			},

			m: function mount(target, anchor) {
				insert(target, hr, anchor);
			},

			p: noop,

			d: function destroy$$1(detach) {
				if (detach) {
					detachNode(hr);
				}
			}
		};
	}

	// (24:6) {#each list as item}
	function create_each_block$2(component, ctx) {
		var if_block_anchor;

		function select_block_type(ctx) {
			if (ctx.item === 0) return create_if_block$3;
			return create_else_block$3;
		}

		var current_block_type = select_block_type(ctx);
		var if_block = current_block_type(component, ctx);

		return {
			c: function create() {
				if_block.c();
				if_block_anchor = createComment();
			},

			m: function mount(target, anchor) {
				if_block.m(target, anchor);
				insert(target, if_block_anchor, anchor);
			},

			p: function update(changed, ctx) {
				if (current_block_type === (current_block_type = select_block_type(ctx)) && if_block) {
					if_block.p(changed, ctx);
				} else {
					if_block.d(1);
					if_block = current_block_type(component, ctx);
					if_block.c();
					if_block.m(if_block_anchor.parentNode, if_block_anchor);
				}
			},

			d: function destroy$$1(detach) {
				if_block.d(detach);
				if (detach) {
					detachNode(if_block_anchor);
				}
			}
		};
	}

	function Dropdown(options) {
		this._debugName = '<Dropdown>';
		if (!options || (!options.target && !options.root)) {
			throw new Error("'target' is a required option");
		}

		init(this, options);
		this.refs = {};
		this._state = assign(data$4(), options.data);

		this._recompute({ value: 1, list: 1 }, this._state);
		if (!('value' in this._state)) console.warn("<Dropdown> was created without expected data property 'value'");
		if (!('list' in this._state)) console.warn("<Dropdown> was created without expected data property 'list'");
		if (!('disabled' in this._state)) console.warn("<Dropdown> was created without expected data property 'disabled'");
		if (!('visible' in this._state)) console.warn("<Dropdown> was created without expected data property 'visible'");
		if (!('right' in this._state)) console.warn("<Dropdown> was created without expected data property 'right'");
		this._intro = true;

		this._fragment = create_main_fragment$7(this, this._state);

		this.root._oncreate.push(() => {
			oncreate.call(this);
			this.fire("update", { changed: assignTrue({}, this._state), current: this._state });
		});

		if (options.target) {
			if (options.hydrate) throw new Error("options.hydrate only works if the component was compiled with the `hydratable: true` option");
			this._fragment.c();
			this._mount(options.target, options.anchor);

			flush(this);
		}
	}

	assign(Dropdown.prototype, protoDev);
	assign(Dropdown.prototype, methods$4);

	Dropdown.prototype._checkReadOnly = function _checkReadOnly(newState) {
		if ('_index' in newState && !this._updatingReadonlyProperty) throw new Error("<Dropdown>: Cannot set read-only property '_index'");
	};

	Dropdown.prototype._recompute = function _recompute(changed, state) {
		if (changed.value || changed.list) {
			if (this._differs(state._index, (state._index = _index(state)))) changed._index = true;
		}
	};

	/* src/app/FileField.html generated by Svelte v2.15.3 */

	function data$5() {
	  return {
	    id: -1,
	    value: "",
	    label: "",
	    fileSelected: false,
	    accept: null,

	    uploading: false,
	    progress: 0,

	    preview: true,
	    previewUrl: "",
	    maxWidth: 0,

	    fileHash: "",
	    fileData: {
	      name: "",
	      size: 0,
	    },
	    formData: {
	      buildId: "",
	      id: "", // Drupal 7 form_id
	      token: "",
	      file: null,
	    },
	  };
	}
	function humanBytes(bytes) {
	  const kb = 1024;
	  const mb = kb * 1024;
	  const gb = mb * 1024;

	  // Make sure bytes isn't a string
	  bytes = +bytes;

	  // FIXME: Probably shouldn't round, check this
	  if (bytes > gb) {
	    const value = bytes / gb;
	    return (Math.round(value * 10) / 10) + "GiB";
	  } else if (bytes > mb) {
	    const value = bytes / mb;
	    return (Math.round(value * 10) / 10) + "MiB";
	  } else if (bytes > kb) {
	    return Math.round(bytes / kb) + "KiB";
	  }

	  return bytes + "B";
	}
	var methods$5 = {
	  requestForm() {
	    const { id, value } = this.get();
	    this.store.fire("requestfileform", {
	      id,
	      oldFileHash: value || null,
	    });
	  },

	  uploadFile() {
	    const { id, formData: form } = this.get();
	    const input = this.refs.input;
	    const file = input.files[0];

	    if (file) {
	      form.file = file;
	      this.store.fire("uploadfile", {
	        id,
	        form,
	      });
	      this.set({
	        uploading: true,
	        fileSelected: false,
	        progress: 0,
	      });
	    }
	  },
	};

	function oncreate$1() {
	  let { fileFields } = this.store.get();

	  const id = ++fileFields;
	  const form = this.refs.form;

	  this.on("reset", () => {
	    form.reset();
	  });

	  // Adjust state when the value changes
	  this.on("state", ({ changed, current, previous }) => {
	    if (changed.value) {
	      if (current.value) {
	        this.requestForm();
	      } else {
	        this.set({
	          uploading: false,
	          fileSelected: false,
	        });
	      }
	    }
	  });

	  // Update form data
	  this.store.on("requestfileformloaded", ({ id: reqId, form }) => {
	    if (reqId === id) {
	      if (form.current) {
	        // Add current file data
	        const { fileHash, fileName, fileSize } = form.current;

	        this.set({
	          uploading: false,
	          fileSelected: true,
	          fileHash: fileHash,
	          fileData: {
	            name: fileName,
	            size: fileSize,
	          },
	          formData: form,
	        });
	      } else {
	        // Just supply the new form data
	        this.set({
	          formData: form,
	        });
	      }
	    }
	  });

	  // Show upload progress
	  this.store.on("uploadfileprogress", ({ id: reqId, loaded, total }) => {
	    if (reqId === id) {
	      this.set({
	        progress: Math.round(loaded / total * 100),
	      });
	    }
	  });

	  // Display newly uploaded file
	  this.store.on("uploadedfile", ({ id: reqId, file}) => {
	    if (reqId === id) {
	      const { fileHash, fileName, fileSize } = file;

	      this.set({
	        uploading: false,
	        fileSelected: true,
	        value: fileHash,
	        fileHash,
	        fileData: {
	          name: fileName,
	          size: fileSize,
	        },
	      });
	      this.fire("change");
	    }
	  });

	  // Set defaults
	  this.set({
	    id,
	  });
	  this.store.set({
	    fileFields,
	  });

	  // Request
	  this.requestForm();
	}
	const file$8 = "src/app/FileField.html";

	function create_main_fragment$8(component, ctx) {
		var text0, div, text1, form, input, div_style_value;

		var if_block0 = (ctx.label) && create_if_block_3$1(component, ctx);

		function select_block_type(ctx) {
			if (ctx.fileSelected) return create_if_block$4;
			if (ctx.uploading) return create_if_block_2$1;
			return create_else_block$4;
		}

		var current_block_type = select_block_type(ctx);
		var if_block1 = current_block_type(component, ctx);

		function change_handler(event) {
			component.uploadFile();
		}

		return {
			c: function create() {
				if (if_block0) if_block0.c();
				text0 = createText("\n");
				div = createElement("div");
				if_block1.c();
				text1 = createText("\n\n  ");
				form = createElement("form");
				input = createElement("input");
				addListener(input, "change", change_handler);
				setAttribute(input, "type", "file");
				input.className = "ihfath-file-input svelte-14yjp8n";
				input.accept = ctx.accept;
				addLoc(input, file$8, 36, 4, 985);
				addLoc(form, file$8, 34, 2, 961);
				div.className = "box has-background-dark file-selector svelte-14yjp8n";
				div.style.cssText = div_style_value = ctx.maxWidth ? `max-width: ${ctx.maxWidth}px;` : '';
				toggleClass(div, "has-file-selected", ctx.fileSelected);
				addLoc(div, file$8, 3, 0, 42);
			},

			m: function mount(target, anchor) {
				if (if_block0) if_block0.m(target, anchor);
				insert(target, text0, anchor);
				insert(target, div, anchor);
				if_block1.m(div, null);
				append(div, text1);
				append(div, form);
				append(form, input);
				component.refs.input = input;
				component.refs.form = form;
			},

			p: function update(changed, ctx) {
				if (ctx.label) {
					if (if_block0) {
						if_block0.p(changed, ctx);
					} else {
						if_block0 = create_if_block_3$1(component, ctx);
						if_block0.c();
						if_block0.m(text0.parentNode, text0);
					}
				} else if (if_block0) {
					if_block0.d(1);
					if_block0 = null;
				}

				if (current_block_type === (current_block_type = select_block_type(ctx)) && if_block1) {
					if_block1.p(changed, ctx);
				} else {
					if_block1.d(1);
					if_block1 = current_block_type(component, ctx);
					if_block1.c();
					if_block1.m(div, text1);
				}

				if (changed.accept) {
					input.accept = ctx.accept;
				}

				if ((changed.maxWidth) && div_style_value !== (div_style_value = ctx.maxWidth ? `max-width: ${ctx.maxWidth}px;` : '')) {
					div.style.cssText = div_style_value;
				}

				if (changed.fileSelected) {
					toggleClass(div, "has-file-selected", ctx.fileSelected);
				}
			},

			d: function destroy$$1(detach) {
				if (if_block0) if_block0.d(detach);
				if (detach) {
					detachNode(text0);
					detachNode(div);
				}

				if_block1.d();
				removeListener(input, "change", change_handler);
				if (component.refs.input === input) component.refs.input = null;
				if (component.refs.form === form) component.refs.form = null;
			}
		};
	}

	// (1:0) {#if label}
	function create_if_block_3$1(component, ctx) {

		var fieldlabel_initial_data = { label: ctx.label };
		var fieldlabel = new FieldLabel({
			root: component.root,
			store: component.store,
			data: fieldlabel_initial_data
		});

		return {
			c: function create() {
				fieldlabel._fragment.c();
			},

			m: function mount(target, anchor) {
				fieldlabel._mount(target, anchor);
			},

			p: function update(changed, ctx) {
				var fieldlabel_changes = {};
				if (changed.label) fieldlabel_changes.label = ctx.label;
				fieldlabel._set(fieldlabel_changes);
			},

			d: function destroy$$1(detach) {
				fieldlabel.destroy(detach);
			}
		};
	}

	// (24:2) {:else}
	function create_else_block$4(component, ctx) {
		var h5, span, text0_value = ctx.$locale.fisNofileselectedLabel, text0, text1, a, text2_value = ctx.$locale.fisSelectfileLabel, text2;

		return {
			c: function create() {
				h5 = createElement("h5");
				span = createElement("span");
				text0 = createText(text0_value);
				text1 = createText("\n      ");
				a = createElement("a");
				text2 = createText(text2_value);
				addLoc(span, file$8, 25, 6, 798);
				a.className = "has-text-link";
				addLoc(a, file$8, 28, 6, 866);
				h5.className = "title is-5 has-text-grey svelte-14yjp8n";
				addLoc(h5, file$8, 24, 4, 754);
			},

			m: function mount(target, anchor) {
				insert(target, h5, anchor);
				append(h5, span);
				append(span, text0);
				append(h5, text1);
				append(h5, a);
				append(a, text2);
			},

			p: function update(changed, ctx) {
				if ((changed.$locale) && text0_value !== (text0_value = ctx.$locale.fisNofileselectedLabel)) {
					setData(text0, text0_value);
				}

				if ((changed.$locale) && text2_value !== (text2_value = ctx.$locale.fisSelectfileLabel)) {
					setData(text2, text2_value);
				}
			},

			d: function destroy$$1(detach) {
				if (detach) {
					detachNode(h5);
				}
			}
		};
	}

	// (19:21) 
	function create_if_block_2$1(component, ctx) {
		var div, span, text0_value = ctx.$locale.fisUploadingStatus, text0, text1, b, text2, text3;

		return {
			c: function create() {
				div = createElement("div");
				span = createElement("span");
				text0 = createText(text0_value);
				text1 = createText("\n      ");
				b = createElement("b");
				text2 = createText(ctx.progress);
				text3 = createText("%");
				addLoc(span, file$8, 20, 6, 640);
				b.className = "has-text-link svelte-14yjp8n";
				addLoc(b, file$8, 21, 6, 688);
				div.className = "file-is-uploading has-text-grey-light svelte-14yjp8n";
				addLoc(div, file$8, 19, 4, 582);
			},

			m: function mount(target, anchor) {
				insert(target, div, anchor);
				append(div, span);
				append(span, text0);
				append(div, text1);
				append(div, b);
				append(b, text2);
				append(b, text3);
			},

			p: function update(changed, ctx) {
				if ((changed.$locale) && text0_value !== (text0_value = ctx.$locale.fisUploadingStatus)) {
					setData(text0, text0_value);
				}

				if (changed.progress) {
					setData(text2, ctx.progress);
				}
			},

			d: function destroy$$1(detach) {
				if (detach) {
					detachNode(div);
				}
			}
		};
	}

	// (8:2) {#if fileSelected}
	function create_if_block$4(component, ctx) {
		var div, h5, text0_value = ctx.fileData.name, text0, text1, h6, text2_value = humanBytes(ctx.fileData.size), text2, text3, if_block_anchor;

		var if_block = (ctx.preview) && create_if_block_1$3(component, ctx);

		return {
			c: function create() {
				div = createElement("div");
				h5 = createElement("h5");
				text0 = createText(text0_value);
				text1 = createText("\n      ");
				h6 = createElement("h6");
				text2 = createText(text2_value);
				text3 = createText("\n\n    ");
				if (if_block) if_block.c();
				if_block_anchor = createComment();
				h5.className = "title is-5 ihfath-file-name svelte-14yjp8n";
				addLoc(h5, file$8, 9, 6, 251);
				h6.className = "title is-6 ihfath-file-size svelte-14yjp8n";
				addLoc(h6, file$8, 10, 6, 318);
				div.className = "file-details svelte-14yjp8n";
				addLoc(div, file$8, 8, 4, 218);
			},

			m: function mount(target, anchor) {
				insert(target, div, anchor);
				append(div, h5);
				append(h5, text0);
				append(div, text1);
				append(div, h6);
				append(h6, text2);
				insert(target, text3, anchor);
				if (if_block) if_block.m(target, anchor);
				insert(target, if_block_anchor, anchor);
			},

			p: function update(changed, ctx) {
				if ((changed.fileData) && text0_value !== (text0_value = ctx.fileData.name)) {
					setData(text0, text0_value);
				}

				if ((changed.fileData) && text2_value !== (text2_value = humanBytes(ctx.fileData.size))) {
					setData(text2, text2_value);
				}

				if (ctx.preview) {
					if (if_block) {
						if_block.p(changed, ctx);
					} else {
						if_block = create_if_block_1$3(component, ctx);
						if_block.c();
						if_block.m(if_block_anchor.parentNode, if_block_anchor);
					}
				} else if (if_block) {
					if_block.d(1);
					if_block = null;
				}
			},

			d: function destroy$$1(detach) {
				if (detach) {
					detachNode(div);
					detachNode(text3);
				}

				if (if_block) if_block.d(detach);
				if (detach) {
					detachNode(if_block_anchor);
				}
			}
		};
	}

	// (14:4) {#if preview}
	function create_if_block_1$3(component, ctx) {
		var div;

		return {
			c: function create() {
				div = createElement("div");
				div.className = "file-thumb svelte-14yjp8n";
				setStyle(div, "background-image", "url('" + (ctx.previewUrl || ctx.$fileBase + '/' + ctx.fileHash) + "')");
				addLoc(div, file$8, 14, 6, 427);
			},

			m: function mount(target, anchor) {
				insert(target, div, anchor);
			},

			p: function update(changed, ctx) {
				if (changed.previewUrl || changed.$fileBase || changed.fileHash) {
					setStyle(div, "background-image", "url('" + (ctx.previewUrl || ctx.$fileBase + '/' + ctx.fileHash) + "')");
				}
			},

			d: function destroy$$1(detach) {
				if (detach) {
					detachNode(div);
				}
			}
		};
	}

	function FileField(options) {
		this._debugName = '<FileField>';
		if (!options || (!options.target && !options.root)) {
			throw new Error("'target' is a required option");
		}
		if (!options.store) {
			throw new Error("<FileField> references store properties, but no store was provided");
		}

		init(this, options);
		this.refs = {};
		this._state = assign(assign(this.store._init(["fileBase","locale"]), data$5()), options.data);
		this.store._add(this, ["fileBase","locale"]);
		if (!('label' in this._state)) console.warn("<FileField> was created without expected data property 'label'");
		if (!('fileSelected' in this._state)) console.warn("<FileField> was created without expected data property 'fileSelected'");
		if (!('maxWidth' in this._state)) console.warn("<FileField> was created without expected data property 'maxWidth'");
		if (!('fileData' in this._state)) console.warn("<FileField> was created without expected data property 'fileData'");
		if (!('preview' in this._state)) console.warn("<FileField> was created without expected data property 'preview'");
		if (!('previewUrl' in this._state)) console.warn("<FileField> was created without expected data property 'previewUrl'");
		if (!('$fileBase' in this._state)) console.warn("<FileField> was created without expected data property '$fileBase'");
		if (!('fileHash' in this._state)) console.warn("<FileField> was created without expected data property 'fileHash'");
		if (!('uploading' in this._state)) console.warn("<FileField> was created without expected data property 'uploading'");
		if (!('$locale' in this._state)) console.warn("<FileField> was created without expected data property '$locale'");
		if (!('progress' in this._state)) console.warn("<FileField> was created without expected data property 'progress'");
		if (!('accept' in this._state)) console.warn("<FileField> was created without expected data property 'accept'");
		this._intro = true;

		this._handlers.destroy = [removeFromStore];

		this._fragment = create_main_fragment$8(this, this._state);

		this.root._oncreate.push(() => {
			oncreate$1.call(this);
			this.fire("update", { changed: assignTrue({}, this._state), current: this._state });
		});

		if (options.target) {
			if (options.hydrate) throw new Error("options.hydrate only works if the component was compiled with the `hydratable: true` option");
			this._fragment.c();
			this._mount(options.target, options.anchor);

			flush(this);
		}
	}

	assign(FileField.prototype, protoDev);
	assign(FileField.prototype, methods$5);

	FileField.prototype._checkReadOnly = function _checkReadOnly(newState) {
	};

	// Utils

	function machineName(name) {
	  return name.trim().replace(/^[\d- ]+|[^a-z\d- ]|[- ]+$/ig, "").replace(/ +|-+/g, "-").toLowerCase();
	}

	function courseToJs(courseRaw, langList) {
	  const course = {
	    id: +courseRaw.pid,
	    category: +courseRaw.type,
	    available: !!(+courseRaw.available),
	    locale: {
	      // ...
	    },
	  };

	  const map = {
	    name: "name",
	    description: "slogan",

	    field_ihfathlsn_c_cpdesc: "cpDescription",
	    field_ihfathlsn_c_cpfeat: "cpFeatures",
	    field_ihfathlsn_c_fpdata: "fpData",
	    field_ihfathlsn_c_cpimage: "cpImage",
	    field_ihfathlsn_c_prices: "prices",
	    field_ihfathlsn_c_f_price: "featuredPrice",
	    // field_ihfathlsn_c_cptitle: "name", // NOTE: Handled in Drupal saveCourse AJAX handler
	  };

	  const baseEntries = {
	    name: 1,
	    description: 1,
	  };
	  const fieldEntries = {
	    field_ihfathlsn_c_cpdesc: 1,
	    field_ihfathlsn_c_cpimage: 1,
	    field_ihfathlsn_c_cpfeat: 1,
	    field_ihfathlsn_c_fpdata: 1,
	    field_ihfathlsn_c_prices: 1,
	    field_ihfathlsn_c_f_price: 1,
	  };
	  const preprocess = {
	    fpData(data) {
	      const parsed = jSh.parseJSON(data);

	      if (parsed.error) {
	        throw new Error("Failed parsing FrontPage data", parsed);
	      }

	      return parsed;
	    },
	    cpFeatures(data) {
	      const parsed = jSh.parseJSON(data);

	      if (parsed.error) {
	        throw new Error("Failed parsing CoursePage Feature data", parsed);
	      }

	      return parsed;
	    },
	    prices(data) {
	      const parsed = jSh.parseJSON(data);

	      if (parsed.error) {
	        throw new Error("Failed parsing price data", parsed);
	      }

	      return parsed;
	    },
	  };

	  // Map base data
	  for (const [lang, data] of Object.entries(courseRaw.data)) {
	    const langData = course.locale[lang] || (course.locale[lang] = {});

	    for (let [entry, value] of Object.entries(data)) {
	      if (baseEntries.hasOwnProperty(entry)) {
	        const prep = preprocess[map[entry]];

	        if (prep) {
	          value = prep(value);
	        }

	        langData[map[entry]] = value;
	      }
	    }

	    // Add slug
	    langData.slug = courseRaw.name;
	  }

	  // Map fields
	  for (const field of Object.keys(courseRaw)) {
	    if (fieldEntries.hasOwnProperty(field)) {
	      const fieldData = courseRaw[field];

	      // Make sure this field isn't empty
	      if (!Array.isArray(fieldData)) {
	        const prep = preprocess[map[field]];

	        for (let [lang, {0: {value}}] of Object.entries(fieldData)) {
	          if (prep) {
	            value = prep(value);
	          }

	          if (lang === "und") {
	            lang = "en";
	          }

	          const langData = course.locale[lang] || (course.locale[lang] = {});
	          langData[map[field]] = value;
	        }
	      }
	    }
	  }

	  // Map info data (levels & FAQ) to JS
	  const infoData = [
	    "levels",
	    "faq",
	  ];

	  const infoDataFieldMap = {
	    levels: {
	      title: "field_ihfathlsn_c_lvl_title",
	      desc: "field_ihfathlsn_c_lvl_desc",
	      books: "field_ihfathlsn_c_lvl_books",
	    },
	    faq: {
	      title: "field_ihfathlsn_c_faq_title",
	      desc: "field_ihfathlsn_c_faq_desc",
	    },
	  };
	  const infoDataPreprocess = {
	    books(data) {
	      if (!data) {
	        return [];
	      }

	      const parsed = jSh.parseJSON(data);

	      if (parsed.error) {
	        throw new Error("Failed parsing course level book data", parsed);
	      }

	      return parsed;
	    },
	  };

	  for (const lang of Object.keys(langList)) {
	    const locale = course.locale[lang] || (course.locale[lang] = {});

	    for (const infoType of infoData) {
	      const infoLocale = (locale[infoType] = []);

	      for (const data of Object.values(courseRaw[infoType])) {
	        const dataTitle = data[infoDataFieldMap[infoType].title][lang];
	        const dataDesc = data[infoDataFieldMap[infoType].desc][lang];

	        const infoDataDeserialized = {
	          id: data.id,
	          title: dataTitle ? dataTitle[0].value : "",
	          desc: dataDesc ? dataDesc[0].value : "",
	        };

	        if (infoType === "levels") {
	          const dataBooks = data[infoDataFieldMap[infoType].books][lang];
	          infoDataDeserialized.books = infoDataPreprocess.books(dataBooks ? dataBooks[0].value : "[]");
	        }

	        infoLocale.push(infoDataDeserialized);
	      }
	    }
	  }

	  // Default data if course is older uninitialized with this data
	  const defaultData = {
	    cpFeatures: [],
	  };

	  for (const [key, defaultValue] of Object.entries(defaultData)) {
	    for (const langData of Object.values(course.locale)) {
	      if (!(key in langData)) {
	        langData[key] = deepCopy(defaultValue);
	      }
	    }
	  }

	  console.log("COURSEJS", course);
	  return course;
	}

	function getThemeUserFields(themeId, store) {
	  const { themeIdMap } = store.get();
	  const theme = themeIdMap[themeId];

	  const fields = [];
	  const themeMap = {};
	  const themeVariables = {};
	  const themeVariableTypes = {};

	  for (const [fieldId, field] of Object.entries(theme.regions)) {
	    themeMap[fieldId] = fields.length;

	    fields.push({
	      // @ts-ignore
	      name: field.name,
	      value: "",
	      // @ts-ignore
	      type: field.type,
	    });
	  }

	  for (const [varName, varMeta] of Object.entries(theme.variables)) {
	    themeVariables[varName] = "";
	    themeVariableTypes[varName] = {
	      type: varMeta[0],
	      defaultValue: varMeta[1],
	    };
	  }

	  return {
	    fields,
	    themeMap,
	    themeVariables,
	    themeVariableTypes,
	  };
	}

	function type(value) {
	    const base = typeof value;

	    if (base === "object") {
	        return value === null ? "null" : (Array.isArray(value) ? "array" : "object");
	    } else {
	        return base;
	    }
	}

	function deepCopy(obj, deepArray = true) {
	  let copy = {};

	  switch (obj.constructor) {
	    case Date:
	      copy = new Date(obj.getTime());
	      break;
	    case RegExp:
	      copy = new RegExp(obj.source, obj.pattern);
	      break;
	    case Array:
	      // `obj` is an array, deepcopy it
	      return deepCopy({a: obj}).a;
	  }

	  for (const key in obj) {
	    if (obj.hasOwnProperty(key)) {
	      const value = obj[key];

	      switch (type(value)) {
	        case "object":
	          copy[key] = deepCopy(value, deepArray);
	          break;

	        case "array":
	          if (deepArray) {
	            const backlog = [
	              [value, copy[key] = new Array(value.length), 0],
	            ];

	            let currentMeta = backlog[0];
	            let current = currentMeta[0];
	            let copyArr = currentMeta[1];

	            arrayLoop:
	            while (true) {
	              for (let i = currentMeta[2]; i < current.length; i++) {
	                const item = current[i];

	                switch (type(item)) {
	                  case "object":
	                    copyArr[i] = deepCopy(item, deepArray);
	                    break;

	                  case "array":
	                    currentMeta[2] = i + 1;

	                    const newArr = new Array(item.length);
	                    backlog.push(currentMeta = [item, newArr, 0]);

	                    copyArr[i] = newArr;
	                    current = item;
	                    copyArr = newArr;
	                    continue arrayLoop;

	                  default:
	                    copyArr[i] = item;
	                }
	              }

	              backlog.pop();

	              if (backlog.length) {
	                currentMeta = backlog[backlog.length - 1];
	                current = currentMeta[0];
	                copyArr = currentMeta[1];
	              } else {
	                break arrayLoop;
	              }
	            }
	          } else {
	            copy[key] = value.slice();
	          }
	          break;

	        default:
	          copy[key] = value;
	      }
	    }
	  }

	  return copy;
	}

	/* src/app/FrontPageEditor.html generated by Svelte v2.15.3 */



	function data$6() {
	  return {
	    courseId: 0,
	    isNew: false,
	    data: {
	      fields: [],
	      theme: "",
	      themeMap: {},
	      themeVariables: {},
	    },
	    themeVariableTypes: {},
	  }
	}
	var methods$6 = {
	  openThemeEditor() {
	    const { themes, themeIdMap } = this.store.get();
	    const { data } = this.get();

	    // The canvas propagates events that require these set beforehand, so
	    // ensure they're already set
	    this.store.set({
	      currentThemeVariables: data.themeVariables,
	      currentThemeMap: data.themeMap,
	    });

	    this.store.fire("setthemeeditor", {
	      visible: true,
	      fields: data.fields,
	      variables: data.themeVariables,
	      themeData: themeIdMap[data.theme],
	      themeMap: data.themeMap,
	    });
	  },

	  createThemeUserFields() {
	    const { data } = this.get();
	    const store = this.store;

	    if (data.theme) {
	      const themeFields = getThemeUserFields(data.theme, store);

	      this.set({
	        data: {
	          fields: themeFields.fields,
	          theme: data.theme,
	          themeMap: themeFields.themeMap,
	          themeVariables: themeFields.themeVariables,
	        },
	        themeVariableTypes: themeFields.themeVariableTypes,
	      });
	    }
	  },
	};

	function oncreate$2() {
	  this.on("state", ({ changed, current, previous }) => {
	    if (current.courseId !== previous.courseId) {
	      updateThemeVariableTypes();
	    }
	  });

	  const updateThemeVariableTypes = () => {
	    const { data } = this.get();

	    if (data.theme === "") {
	      this.set({
	        themeVariableTypes: {},
	      });
	    } else {
	      const themeFields = getThemeUserFields(data.theme, this.store);
	      this.set({
	        themeVariableTypes: themeFields.themeVariableTypes,
	      });
	    }
	  };

	  // Initial update
	  updateThemeVariableTypes();
	}
	const file$9 = "src/app/FrontPageEditor.html";

	function get_each_context_1$1(ctx, list, i) {
		const child_ctx = Object.create(ctx);
		child_ctx.field = list[i];
		child_ctx.each_value_1 = list;
		child_ctx.fieldIndex = i;
		return child_ctx;
	}

	function get_each_context$3(ctx, list, i) {
		const child_ctx = Object.create(ctx);
		child_ctx.variable = list[i];
		child_ctx.variableIndex = i;
		return child_ctx;
	}

	function create_main_fragment$9(component, ctx) {
		var div5, div1, div0, text0, div4, div2, button0, text1_value = ctx.$locale.fpeFieldCreateThemeFieldsButton, text1, text2, div3, button1, text3_value = ctx.$locale.fpeFieldOpenThemeEditorButton, text3, text4, div6, h30, text6, text7, h31, text9;

		function click_handler(event) {
			component.createThemeUserFields();
		}

		function click_handler_1(event) {
			component.openThemeEditor();
		}

		var each_value = ctx.Object.entries(ctx.themeVariableTypes);

		var each_blocks = [];

		for (var i = 0; i < each_value.length; i += 1) {
			each_blocks[i] = create_each_block_1$1(component, get_each_context$3(ctx, each_value, i));
		}

		var if_block = (ctx.data.fields) && create_if_block$5(component, ctx);

		return {
			c: function create() {
				div5 = createElement("div");
				div1 = createElement("div");
				div0 = createElement("div");
				text0 = createText("\n  ");
				div4 = createElement("div");
				div2 = createElement("div");
				button0 = createElement("button");
				text1 = createText(text1_value);
				text2 = createText("\n    ");
				div3 = createElement("div");
				button1 = createElement("button");
				text3 = createText(text3_value);
				text4 = createText("\n\n");
				div6 = createElement("div");
				h30 = createElement("h3");
				h30.textContent = "Theme Settings";
				text6 = createText("\n\n  ");

				for (var i = 0; i < each_blocks.length; i += 1) {
					each_blocks[i].c();
				}

				text7 = createText("\n\n  ");
				h31 = createElement("h3");
				h31.textContent = "Theme Regions";
				text9 = createText("\n\n  \n  ");
				if (if_block) if_block.c();
				div0.className = "level-item";
				addLoc(div0, file$9, 3, 4, 91);
				div1.className = "level-left";
				addLoc(div1, file$9, 2, 2, 62);
				addListener(button0, "click", click_handler);
				button0.className = "button is-link";
				addLoc(button0, file$9, 9, 6, 222);
				div2.className = "level-item";
				addLoc(div2, file$9, 8, 4, 191);
				addListener(button1, "click", click_handler_1);
				button1.className = "button is-link";
				addLoc(button1, file$9, 16, 6, 417);
				div3.className = "level-item";
				addLoc(div3, file$9, 15, 4, 386);
				div4.className = "level-right";
				addLoc(div4, file$9, 7, 2, 161);
				div5.className = "level header-buttons svelte-6d06tn";
				addLoc(div5, file$9, 1, 0, 25);
				h30.className = "title is-3 has-text-centered svelte-6d06tn";
				addLoc(h30, file$9, 26, 2, 613);
				h31.className = "title is-3 has-text-centered svelte-6d06tn";
				addLoc(h31, file$9, 43, 2, 1375);
				div6.className = "field-list svelte-6d06tn";
				addLoc(div6, file$9, 25, 0, 586);
			},

			m: function mount(target, anchor) {
				insert(target, div5, anchor);
				append(div5, div1);
				append(div1, div0);
				append(div5, text0);
				append(div5, div4);
				append(div4, div2);
				append(div2, button0);
				append(button0, text1);
				append(div4, text2);
				append(div4, div3);
				append(div3, button1);
				append(button1, text3);
				insert(target, text4, anchor);
				insert(target, div6, anchor);
				append(div6, h30);
				append(div6, text6);

				for (var i = 0; i < each_blocks.length; i += 1) {
					each_blocks[i].m(div6, null);
				}

				append(div6, text7);
				append(div6, h31);
				append(div6, text9);
				if (if_block) if_block.m(div6, null);
			},

			p: function update(changed, ctx) {
				if ((changed.$locale) && text1_value !== (text1_value = ctx.$locale.fpeFieldCreateThemeFieldsButton)) {
					setData(text1, text1_value);
				}

				if ((changed.$locale) && text3_value !== (text3_value = ctx.$locale.fpeFieldOpenThemeEditorButton)) {
					setData(text3, text3_value);
				}

				if (changed.Object || changed.themeVariableTypes || changed.$locale || changed.data) {
					each_value = ctx.Object.entries(ctx.themeVariableTypes);

					for (var i = 0; i < each_value.length; i += 1) {
						const child_ctx = get_each_context$3(ctx, each_value, i);

						if (each_blocks[i]) {
							each_blocks[i].p(changed, child_ctx);
						} else {
							each_blocks[i] = create_each_block_1$1(component, child_ctx);
							each_blocks[i].c();
							each_blocks[i].m(div6, text7);
						}
					}

					for (; i < each_blocks.length; i += 1) {
						each_blocks[i].d(1);
					}
					each_blocks.length = each_value.length;
				}

				if (ctx.data.fields) {
					if (if_block) {
						if_block.p(changed, ctx);
					} else {
						if_block = create_if_block$5(component, ctx);
						if_block.c();
						if_block.m(div6, null);
					}
				} else if (if_block) {
					if_block.d(1);
					if_block = null;
				}
			},

			d: function destroy$$1(detach) {
				if (detach) {
					detachNode(div5);
				}

				removeListener(button0, "click", click_handler);
				removeListener(button1, "click", click_handler_1);
				if (detach) {
					detachNode(text4);
					detachNode(div6);
				}

				destroyEach(each_blocks, detach);

				if (if_block) if_block.d();
			}
		};
	}

	// (38:44) 
	function create_if_block_6(component, ctx) {
		var filefield_updating = {};

		var filefield_initial_data = {};
		if (ctx.data.themeVariables[ctx.variable[0]] !== void 0) {
			filefield_initial_data.value = ctx.data.themeVariables[ctx.variable[0]];
			filefield_updating.value = true;
		}
		var filefield = new FileField({
			root: component.root,
			store: component.store,
			data: filefield_initial_data,
			_bind(changed, childState) {
				var newState = {};
				if (!filefield_updating.value && changed.value) {
					ctx.data.themeVariables[ctx.variable[0]] = childState.value;
					newState.data = ctx.data;
				}
				component._set(newState);
				filefield_updating = {};
			}
		});

		component.root._beforecreate.push(() => {
			filefield._bind({ value: 1 }, filefield.get());
		});

		return {
			c: function create() {
				filefield._fragment.c();
			},

			m: function mount(target, anchor) {
				filefield._mount(target, anchor);
			},

			p: function update(changed, _ctx) {
				ctx = _ctx;
				var filefield_changes = {};
				if (!filefield_updating.value && changed.data || changed.Object || changed.themeVariableTypes) {
					filefield_changes.value = ctx.data.themeVariables[ctx.variable[0]];
					filefield_updating.value = ctx.data.themeVariables[ctx.variable[0]] !== void 0;
				}
				filefield._set(filefield_changes);
				filefield_updating = {};
			},

			d: function destroy$$1(detach) {
				filefield.destroy(detach);
			}
		};
	}

	// (36:46) 
	function create_if_block_5(component, ctx) {
		var editabletextareafield_updating = {};

		var editabletextareafield_initial_data = { label: ctx.$locale.fpeFieldValueLabel };
		if (ctx.data.themeVariables[ctx.variable[0]] !== void 0) {
			editabletextareafield_initial_data.value = ctx.data.themeVariables[ctx.variable[0]];
			editabletextareafield_updating.value = true;
		}
		var editabletextareafield = new EditableTextAreaField({
			root: component.root,
			store: component.store,
			data: editabletextareafield_initial_data,
			_bind(changed, childState) {
				var newState = {};
				if (!editabletextareafield_updating.value && changed.value) {
					ctx.data.themeVariables[ctx.variable[0]] = childState.value;
					newState.data = ctx.data;
				}
				component._set(newState);
				editabletextareafield_updating = {};
			}
		});

		component.root._beforecreate.push(() => {
			editabletextareafield._bind({ value: 1 }, editabletextareafield.get());
		});

		return {
			c: function create() {
				editabletextareafield._fragment.c();
			},

			m: function mount(target, anchor) {
				editabletextareafield._mount(target, anchor);
			},

			p: function update(changed, _ctx) {
				ctx = _ctx;
				var editabletextareafield_changes = {};
				if (changed.$locale) editabletextareafield_changes.label = ctx.$locale.fpeFieldValueLabel;
				if (!editabletextareafield_updating.value && changed.data || changed.Object || changed.themeVariableTypes) {
					editabletextareafield_changes.value = ctx.data.themeVariables[ctx.variable[0]];
					editabletextareafield_updating.value = ctx.data.themeVariables[ctx.variable[0]] !== void 0;
				}
				editabletextareafield._set(editabletextareafield_changes);
				editabletextareafield_updating = {};
			},

			d: function destroy$$1(detach) {
				editabletextareafield.destroy(detach);
			}
		};
	}

	// (34:6) {#if variable[1].type === "text"}
	function create_if_block_4(component, ctx) {
		var editabletextfield_updating = {};

		var editabletextfield_initial_data = { label: ctx.$locale.fpeFieldValueLabel };
		if (ctx.data.themeVariables[ctx.variable[0]] !== void 0) {
			editabletextfield_initial_data.value = ctx.data.themeVariables[ctx.variable[0]];
			editabletextfield_updating.value = true;
		}
		var editabletextfield = new EditableTextField({
			root: component.root,
			store: component.store,
			data: editabletextfield_initial_data,
			_bind(changed, childState) {
				var newState = {};
				if (!editabletextfield_updating.value && changed.value) {
					ctx.data.themeVariables[ctx.variable[0]] = childState.value;
					newState.data = ctx.data;
				}
				component._set(newState);
				editabletextfield_updating = {};
			}
		});

		component.root._beforecreate.push(() => {
			editabletextfield._bind({ value: 1 }, editabletextfield.get());
		});

		return {
			c: function create() {
				editabletextfield._fragment.c();
			},

			m: function mount(target, anchor) {
				editabletextfield._mount(target, anchor);
			},

			p: function update(changed, _ctx) {
				ctx = _ctx;
				var editabletextfield_changes = {};
				if (changed.$locale) editabletextfield_changes.label = ctx.$locale.fpeFieldValueLabel;
				if (!editabletextfield_updating.value && changed.data || changed.Object || changed.themeVariableTypes) {
					editabletextfield_changes.value = ctx.data.themeVariables[ctx.variable[0]];
					editabletextfield_updating.value = ctx.data.themeVariables[ctx.variable[0]] !== void 0;
				}
				editabletextfield._set(editabletextfield_changes);
				editabletextfield_updating = {};
			},

			d: function destroy$$1(detach) {
				editabletextfield.destroy(detach);
			}
		};
	}

	// (29:2) {#each Object.entries(themeVariableTypes) as variable, variableIndex}
	function create_each_block_1$1(component, ctx) {
		var div, h3, text0_value = ctx.variable[0], text0, text1;

		function select_block_type(ctx) {
			if (ctx.variable[1].type === "text") return create_if_block_4;
			if (ctx.variable[1].type === "bigtext") return create_if_block_5;
			if (ctx.variable[1].type === "image") return create_if_block_6;
		}

		var current_block_type = select_block_type(ctx);
		var if_block = current_block_type && current_block_type(component, ctx);

		return {
			c: function create() {
				div = createElement("div");
				h3 = createElement("h3");
				text0 = createText(text0_value);
				text1 = createText("\n      \n\n      ");
				if (if_block) if_block.c();
				h3.className = "title is-4 svelte-6d06tn";
				addLoc(h3, file$9, 30, 6, 780);
				div.className = "fp-field svelte-6d06tn";
				addLoc(div, file$9, 29, 4, 751);
			},

			m: function mount(target, anchor) {
				insert(target, div, anchor);
				append(div, h3);
				append(h3, text0);
				append(div, text1);
				if (if_block) if_block.m(div, null);
			},

			p: function update(changed, ctx) {
				if ((changed.Object || changed.themeVariableTypes) && text0_value !== (text0_value = ctx.variable[0])) {
					setData(text0, text0_value);
				}

				if (current_block_type === (current_block_type = select_block_type(ctx)) && if_block) {
					if_block.p(changed, ctx);
				} else {
					if (if_block) if_block.d(1);
					if_block = current_block_type && current_block_type(component, ctx);
					if (if_block) if_block.c();
					if (if_block) if_block.m(div, null);
				}
			},

			d: function destroy$$1(detach) {
				if (detach) {
					detachNode(div);
				}

				if (if_block) if_block.d();
			}
		};
	}

	// (47:2) {#if data.fields}
	function create_if_block$5(component, ctx) {
		var each_anchor;

		var each_value_1 = ctx.data.fields;

		var each_blocks = [];

		for (var i = 0; i < each_value_1.length; i += 1) {
			each_blocks[i] = create_each_block$3(component, get_each_context_1$1(ctx, each_value_1, i));
		}

		return {
			c: function create() {
				for (var i = 0; i < each_blocks.length; i += 1) {
					each_blocks[i].c();
				}

				each_anchor = createComment();
			},

			m: function mount(target, anchor) {
				for (var i = 0; i < each_blocks.length; i += 1) {
					each_blocks[i].m(target, anchor);
				}

				insert(target, each_anchor, anchor);
			},

			p: function update(changed, ctx) {
				if (changed.data || changed.$locale) {
					each_value_1 = ctx.data.fields;

					for (var i = 0; i < each_value_1.length; i += 1) {
						const child_ctx = get_each_context_1$1(ctx, each_value_1, i);

						if (each_blocks[i]) {
							each_blocks[i].p(changed, child_ctx);
						} else {
							each_blocks[i] = create_each_block$3(component, child_ctx);
							each_blocks[i].c();
							each_blocks[i].m(each_anchor.parentNode, each_anchor);
						}
					}

					for (; i < each_blocks.length; i += 1) {
						each_blocks[i].d(1);
					}
					each_blocks.length = each_value_1.length;
				}
			},

			d: function destroy$$1(detach) {
				destroyEach(each_blocks, detach);

				if (detach) {
					detachNode(each_anchor);
				}
			}
		};
	}

	// (57:40) 
	function create_if_block_3$2(component, ctx) {
		var filefield_updating = {};

		var filefield_initial_data = {};
		if (ctx.field.value !== void 0) {
			filefield_initial_data.value = ctx.field.value;
			filefield_updating.value = true;
		}
		var filefield = new FileField({
			root: component.root,
			store: component.store,
			data: filefield_initial_data,
			_bind(changed, childState) {
				var newState = {};
				if (!filefield_updating.value && changed.value) {
					ctx.field.value = childState.value;

					newState.data = ctx.data;
				}
				component._set(newState);
				filefield_updating = {};
			}
		});

		component.root._beforecreate.push(() => {
			filefield._bind({ value: 1 }, filefield.get());
		});

		return {
			c: function create() {
				filefield._fragment.c();
			},

			m: function mount(target, anchor) {
				filefield._mount(target, anchor);
			},

			p: function update(changed, _ctx) {
				ctx = _ctx;
				var filefield_changes = {};
				if (!filefield_updating.value && changed.data) {
					filefield_changes.value = ctx.field.value;
					filefield_updating.value = ctx.field.value !== void 0;
				}
				filefield._set(filefield_changes);
				filefield_updating = {};
			},

			d: function destroy$$1(detach) {
				filefield.destroy(detach);
			}
		};
	}

	// (55:42) 
	function create_if_block_2$2(component, ctx) {
		var editabletextareafield_updating = {};

		var editabletextareafield_initial_data = { label: ctx.$locale.fpeFieldValueLabel };
		if (ctx.field.value !== void 0) {
			editabletextareafield_initial_data.value = ctx.field.value;
			editabletextareafield_updating.value = true;
		}
		var editabletextareafield = new EditableTextAreaField({
			root: component.root,
			store: component.store,
			data: editabletextareafield_initial_data,
			_bind(changed, childState) {
				var newState = {};
				if (!editabletextareafield_updating.value && changed.value) {
					ctx.field.value = childState.value;

					newState.data = ctx.data;
				}
				component._set(newState);
				editabletextareafield_updating = {};
			}
		});

		component.root._beforecreate.push(() => {
			editabletextareafield._bind({ value: 1 }, editabletextareafield.get());
		});

		return {
			c: function create() {
				editabletextareafield._fragment.c();
			},

			m: function mount(target, anchor) {
				editabletextareafield._mount(target, anchor);
			},

			p: function update(changed, _ctx) {
				ctx = _ctx;
				var editabletextareafield_changes = {};
				if (changed.$locale) editabletextareafield_changes.label = ctx.$locale.fpeFieldValueLabel;
				if (!editabletextareafield_updating.value && changed.data) {
					editabletextareafield_changes.value = ctx.field.value;
					editabletextareafield_updating.value = ctx.field.value !== void 0;
				}
				editabletextareafield._set(editabletextareafield_changes);
				editabletextareafield_updating = {};
			},

			d: function destroy$$1(detach) {
				editabletextareafield.destroy(detach);
			}
		};
	}

	// (53:8) {#if field.type === "text"}
	function create_if_block_1$4(component, ctx) {
		var editabletextfield_updating = {};

		var editabletextfield_initial_data = { label: ctx.$locale.fpeFieldValueLabel };
		if (ctx.field.value !== void 0) {
			editabletextfield_initial_data.value = ctx.field.value;
			editabletextfield_updating.value = true;
		}
		var editabletextfield = new EditableTextField({
			root: component.root,
			store: component.store,
			data: editabletextfield_initial_data,
			_bind(changed, childState) {
				var newState = {};
				if (!editabletextfield_updating.value && changed.value) {
					ctx.field.value = childState.value;

					newState.data = ctx.data;
				}
				component._set(newState);
				editabletextfield_updating = {};
			}
		});

		component.root._beforecreate.push(() => {
			editabletextfield._bind({ value: 1 }, editabletextfield.get());
		});

		return {
			c: function create() {
				editabletextfield._fragment.c();
			},

			m: function mount(target, anchor) {
				editabletextfield._mount(target, anchor);
			},

			p: function update(changed, _ctx) {
				ctx = _ctx;
				var editabletextfield_changes = {};
				if (changed.$locale) editabletextfield_changes.label = ctx.$locale.fpeFieldValueLabel;
				if (!editabletextfield_updating.value && changed.data) {
					editabletextfield_changes.value = ctx.field.value;
					editabletextfield_updating.value = ctx.field.value !== void 0;
				}
				editabletextfield._set(editabletextfield_changes);
				editabletextfield_updating = {};
			},

			d: function destroy$$1(detach) {
				editabletextfield.destroy(detach);
			}
		};
	}

	// (48:4) {#each data.fields as field, fieldIndex}
	function create_each_block$3(component, ctx) {
		var div, h3, text0_value = ctx.field.name, text0, text1, text2;

		function select_block_type_1(ctx) {
			if (ctx.field.type === "text") return create_if_block_1$4;
			if (ctx.field.type === "bigtext") return create_if_block_2$2;
			if (ctx.field.type === "image") return create_if_block_3$2;
		}

		var current_block_type = select_block_type_1(ctx);
		var if_block = current_block_type && current_block_type(component, ctx);

		return {
			c: function create() {
				div = createElement("div");
				h3 = createElement("h3");
				text0 = createText(text0_value);
				text1 = createText("\n        \n\n        ");
				if (if_block) if_block.c();
				text2 = createText("\n      ");
				h3.className = "title is-5 svelte-6d06tn";
				addLoc(h3, file$9, 49, 8, 1556);
				div.className = "fp-field svelte-6d06tn";
				addLoc(div, file$9, 48, 6, 1525);
			},

			m: function mount(target, anchor) {
				insert(target, div, anchor);
				append(div, h3);
				append(h3, text0);
				append(div, text1);
				if (if_block) if_block.m(div, null);
				append(div, text2);
			},

			p: function update(changed, ctx) {
				if ((changed.data) && text0_value !== (text0_value = ctx.field.name)) {
					setData(text0, text0_value);
				}

				if (current_block_type === (current_block_type = select_block_type_1(ctx)) && if_block) {
					if_block.p(changed, ctx);
				} else {
					if (if_block) if_block.d(1);
					if_block = current_block_type && current_block_type(component, ctx);
					if (if_block) if_block.c();
					if (if_block) if_block.m(div, text2);
				}
			},

			d: function destroy$$1(detach) {
				if (detach) {
					detachNode(div);
				}

				if (if_block) if_block.d();
			}
		};
	}

	function FrontPageEditor(options) {
		this._debugName = '<FrontPageEditor>';
		if (!options || (!options.target && !options.root)) {
			throw new Error("'target' is a required option");
		}
		if (!options.store) {
			throw new Error("<FrontPageEditor> references store properties, but no store was provided");
		}

		init(this, options);
		this._state = assign(assign(assign({ Object : Object }, this.store._init(["locale"])), data$6()), options.data);
		this.store._add(this, ["locale"]);
		if (!('$locale' in this._state)) console.warn("<FrontPageEditor> was created without expected data property '$locale'");

		if (!('themeVariableTypes' in this._state)) console.warn("<FrontPageEditor> was created without expected data property 'themeVariableTypes'");
		if (!('data' in this._state)) console.warn("<FrontPageEditor> was created without expected data property 'data'");
		this._intro = true;

		this._handlers.destroy = [removeFromStore];

		this._fragment = create_main_fragment$9(this, this._state);

		this.root._oncreate.push(() => {
			oncreate$2.call(this);
			this.fire("update", { changed: assignTrue({}, this._state), current: this._state });
		});

		if (options.target) {
			if (options.hydrate) throw new Error("options.hydrate only works if the component was compiled with the `hydratable: true` option");
			this._fragment.c();
			this._mount(options.target, options.anchor);

			flush(this);
		}
	}

	assign(FrontPageEditor.prototype, protoDev);
	assign(FrontPageEditor.prototype, methods$6);

	FrontPageEditor.prototype._checkReadOnly = function _checkReadOnly(newState) {
	};

	/* src/app/UserListField.html generated by Svelte v2.15.3 */

	const fieldId = Symbol();
	let revision = 0;
	let searchTimeout = null;

	function data$7() {
	  return {
	    searchTerm: "",
	    lastSearchTerm: "",
	    userList: [
	      // {uid: 0, name: ""},
	    ],
	    searchList: [
	      // {uid: 0, name: ""},
	    ],
	    value: "", // "1,2,3,4,..." uids
	    revision,
	  };
	}
	var methods$7 = {
	  addUser(uid, name) {
	    const { userList } = this.get();
	    const uniqueUsers = new Set(userList.map(u => u.uid).concat(uid));

	    if (uniqueUsers.size > userList.length) {
	      revision++;
	      const newList = userList.concat({
	        uid,
	        name,
	      });

	      this.set({
	        userList: newList,
	        value: newList.map(u => u.uid).join(","),

	        // Empty user list
	        searchList: [],
	        searchTerm: "",
	      });
	    }
	  },

	  removeUser(index) {
	    const { userList } = this.get();
	    userList.splice(index, 1);

	    revision++;
	    this.set({
	      userList,
	      value: userList.map(u => u.uid).join(","),
	    });
	  },

	  searchUsers() {
	    clearTimeout(searchTimeout);
	    searchTimeout = setTimeout(() => {
	      const { searchTerm, lastSearchTerm } = this.get();

	      if (searchTerm !== lastSearchTerm) {
	        if (searchTerm.trim()) {
	          this.store.fire("usersearch", {
	            name: searchTerm,
	            fieldId,
	          });
	        } else {
	          this.set({
	            searchList: [],
	          });
	        }
	        this.set({
	          lastSearchTerm: searchTerm,
	        });
	      }
	    }, 350);
	  },
	};

	function oncreate$3() {
	  this.on("state", ({ changed, current }) => {
	    // Apply this change if the revision hasn't changed (outside influence)
	    if (changed.value && current.revision === revision) {
	      // Request new userlist from server
	      externalUpdate(current.value);
	    } else {
	      // The change was internal, update to the new revision
	      this.set({
	        revision,
	      });
	    }
	  });

	  const externalUpdate = (value) => {
	    if (value.trim()) {
	      this.store.fire("userlistnamesrequest", {
	        users: value.split(",").map(uid => +uid),
	        fieldId,
	      });
	    } else {
	      this.set({
	        userList: [],
	      });
	    }
	  };

	  const l1 = this.store.on("userlistnamesresponse", ({ users, fieldId: resFieldId }) => {
	    if (resFieldId === fieldId) {
	      this.set({
	        userList: users,
	      });
	    }
	  });

	  const l2 = this.store.on("searchresults", ({ users, fieldId: resFieldId }) => {
	    if (resFieldId === fieldId) {
	      this.set({
	        searchList: users,
	      });
	    }
	  });

	  this.on("destroy", () => {
	    l1.cancel();
	    l2.cancel();
	  });

	  // Display initial value
	  const { value } = this.get();
	  externalUpdate(value);
	}
	const file$a = "src/app/UserListField.html";

	function click_handler_1$1(event) {
		const { component, ctx } = this._svelte;

		component.removeUser(ctx.index);
	}

	function get_each_context_1$2(ctx, list, i) {
		const child_ctx = Object.create(ctx);
		child_ctx.user = list[i];
		child_ctx.index = i;
		return child_ctx;
	}

	function click_handler$3(event) {
		const { component, ctx } = this._svelte;

		component.addUser(ctx.user.uid, ctx.user.name);
	}

	function get_each_context$4(ctx, list, i) {
		const child_ctx = Object.create(ctx);
		child_ctx.user = list[i];
		return child_ctx;
	}

	function create_main_fragment$a(component, ctx) {
		var div1, input, input_updating = false, text0, text1, div0;

		function input_input_handler() {
			input_updating = true;
			component.set({ searchTerm: input.value });
			input_updating = false;
		}

		function input_handler(event) {
			component.searchUsers();
		}

		var if_block = (ctx.searchList.length) && create_if_block$6(component, ctx);

		var each_value_1 = ctx.userList;

		var each_blocks = [];

		for (var i = 0; i < each_value_1.length; i += 1) {
			each_blocks[i] = create_each_block$4(component, get_each_context_1$2(ctx, each_value_1, i));
		}

		var each_else = null;

		if (!each_value_1.length) {
			each_else = create_else_block$5(component, ctx);
			each_else.c();
		}

		return {
			c: function create() {
				div1 = createElement("div");
				input = createElement("input");
				text0 = createText("\n  ");
				if (if_block) if_block.c();
				text1 = createText("\n  ");
				div0 = createElement("div");

				for (var i = 0; i < each_blocks.length; i += 1) {
					each_blocks[i].c();
				}
				addListener(input, "input", input_input_handler);
				addListener(input, "input", input_handler);
				setAttribute(input, "type", "text");
				input.className = "input svelte-sx2020";
				input.placeholder = "Search";
				addLoc(input, file$a, 1, 2, 32);
				div0.className = "selected-users svelte-sx2020";
				addLoc(div0, file$a, 16, 2, 384);
				div1.className = "user-list-field svelte-sx2020";
				addLoc(div1, file$a, 0, 0, 0);
			},

			m: function mount(target, anchor) {
				insert(target, div1, anchor);
				append(div1, input);

				input.value = ctx.searchTerm;

				append(div1, text0);
				if (if_block) if_block.m(div1, null);
				append(div1, text1);
				append(div1, div0);

				for (var i = 0; i < each_blocks.length; i += 1) {
					each_blocks[i].m(div0, null);
				}

				if (each_else) {
					each_else.m(div0, null);
				}
			},

			p: function update(changed, ctx) {
				if (!input_updating && changed.searchTerm) input.value = ctx.searchTerm;

				if (ctx.searchList.length) {
					if (if_block) {
						if_block.p(changed, ctx);
					} else {
						if_block = create_if_block$6(component, ctx);
						if_block.c();
						if_block.m(div1, text1);
					}
				} else if (if_block) {
					if_block.d(1);
					if_block = null;
				}

				if (changed.$basePath || changed.userList) {
					each_value_1 = ctx.userList;

					for (var i = 0; i < each_value_1.length; i += 1) {
						const child_ctx = get_each_context_1$2(ctx, each_value_1, i);

						if (each_blocks[i]) {
							each_blocks[i].p(changed, child_ctx);
						} else {
							each_blocks[i] = create_each_block$4(component, child_ctx);
							each_blocks[i].c();
							each_blocks[i].m(div0, null);
						}
					}

					for (; i < each_blocks.length; i += 1) {
						each_blocks[i].d(1);
					}
					each_blocks.length = each_value_1.length;
				}

				if (each_value_1.length) {
					if (each_else) {
						each_else.d(1);
						each_else = null;
					}
				} else if (!each_else) {
					each_else = create_else_block$5(component, ctx);
					each_else.c();
					each_else.m(div0, null);
				}
			},

			d: function destroy$$1(detach) {
				if (detach) {
					detachNode(div1);
				}

				removeListener(input, "input", input_input_handler);
				removeListener(input, "input", input_handler);
				if (if_block) if_block.d();

				destroyEach(each_blocks, detach);

				if (each_else) each_else.d();
			}
		};
	}

	// (8:2) {#if searchList.length}
	function create_if_block$6(component, ctx) {
		var div;

		var each_value = ctx.searchList;

		var each_blocks = [];

		for (var i = 0; i < each_value.length; i += 1) {
			each_blocks[i] = create_each_block_1$2(component, get_each_context$4(ctx, each_value, i));
		}

		return {
			c: function create() {
				div = createElement("div");

				for (var i = 0; i < each_blocks.length; i += 1) {
					each_blocks[i].c();
				}
				div.className = "user-matches svelte-sx2020";
				addLoc(div, file$a, 8, 4, 184);
			},

			m: function mount(target, anchor) {
				insert(target, div, anchor);

				for (var i = 0; i < each_blocks.length; i += 1) {
					each_blocks[i].m(div, null);
				}
			},

			p: function update(changed, ctx) {
				if (changed.searchList) {
					each_value = ctx.searchList;

					for (var i = 0; i < each_value.length; i += 1) {
						const child_ctx = get_each_context$4(ctx, each_value, i);

						if (each_blocks[i]) {
							each_blocks[i].p(changed, child_ctx);
						} else {
							each_blocks[i] = create_each_block_1$2(component, child_ctx);
							each_blocks[i].c();
							each_blocks[i].m(div, null);
						}
					}

					for (; i < each_blocks.length; i += 1) {
						each_blocks[i].d(1);
					}
					each_blocks.length = each_value.length;
				}
			},

			d: function destroy$$1(detach) {
				if (detach) {
					detachNode(div);
				}

				destroyEach(each_blocks, detach);
			}
		};
	}

	// (10:6) {#each searchList as user}
	function create_each_block_1$2(component, ctx) {
		var div, text_value = ctx.user.name, text;

		return {
			c: function create() {
				div = createElement("div");
				text = createText(text_value);
				div._svelte = { component, ctx };

				addListener(div, "click", click_handler$3);
				div.className = "match svelte-sx2020";
				addLoc(div, file$a, 10, 8, 252);
			},

			m: function mount(target, anchor) {
				insert(target, div, anchor);
				append(div, text);
			},

			p: function update(changed, _ctx) {
				ctx = _ctx;
				if ((changed.searchList) && text_value !== (text_value = ctx.user.name)) {
					setData(text, text_value);
				}

				div._svelte.ctx = ctx;
			},

			d: function destroy$$1(detach) {
				if (detach) {
					detachNode(div);
				}

				removeListener(div, "click", click_handler$3);
			}
		};
	}

	// (28:4) {:else}
	function create_else_block$5(component, ctx) {
		var div, span, text_1;

		return {
			c: function create() {
				div = createElement("div");
				span = createElement("span");
				span.textContent = "No users selected";
				text_1 = createText("\n      ");
				span.className = "svelte-sx2020";
				addLoc(span, file$a, 29, 8, 830);
				div.className = "no-users-found has-text-grey-dark svelte-sx2020";
				addLoc(div, file$a, 28, 6, 774);
			},

			m: function mount(target, anchor) {
				insert(target, div, anchor);
				append(div, span);
				append(div, text_1);
			},

			d: function destroy$$1(detach) {
				if (detach) {
					detachNode(div);
				}
			}
		};
	}

	// (18:4) {#each userList as user, index}
	function create_each_block$4(component, ctx) {
		var div, a, text0_value = ctx.user.name, text0, text1, i, a_href_value, text2, button, text4;

		return {
			c: function create() {
				div = createElement("div");
				a = createElement("a");
				text0 = createText(text0_value);
				text1 = createText("\n          ");
				i = createElement("i");
				text2 = createText("\n        ");
				button = createElement("button");
				button.textContent = "×";
				text4 = createText("\n      ");
				i.className = "fa fa-pencil";
				addLoc(i, file$a, 21, 10, 610);
				a.href = a_href_value = "" + ctx.$basePath + "user/" + ctx.user.uid + "/edit";
				a.target = "_blank";
				a.className = "name svelte-sx2020";
				addLoc(a, file$a, 19, 8, 506);

				button._svelte = { component, ctx };

				addListener(button, "click", click_handler_1$1);
				button.className = "remove svelte-sx2020";
				addLoc(button, file$a, 23, 8, 660);
				div.className = "user-tag has-background-link svelte-sx2020";
				addLoc(div, file$a, 18, 6, 455);
			},

			m: function mount(target, anchor) {
				insert(target, div, anchor);
				append(div, a);
				append(a, text0);
				append(a, text1);
				append(a, i);
				append(div, text2);
				append(div, button);
				append(div, text4);
			},

			p: function update(changed, _ctx) {
				ctx = _ctx;
				if ((changed.userList) && text0_value !== (text0_value = ctx.user.name)) {
					setData(text0, text0_value);
				}

				if ((changed.$basePath || changed.userList) && a_href_value !== (a_href_value = "" + ctx.$basePath + "user/" + ctx.user.uid + "/edit")) {
					a.href = a_href_value;
				}

				button._svelte.ctx = ctx;
			},

			d: function destroy$$1(detach) {
				if (detach) {
					detachNode(div);
				}

				removeListener(button, "click", click_handler_1$1);
			}
		};
	}

	function UserListField(options) {
		this._debugName = '<UserListField>';
		if (!options || (!options.target && !options.root)) {
			throw new Error("'target' is a required option");
		}
		if (!options.store) {
			throw new Error("<UserListField> references store properties, but no store was provided");
		}

		init(this, options);
		this._state = assign(assign(this.store._init(["basePath"]), data$7()), options.data);
		this.store._add(this, ["basePath"]);
		if (!('searchTerm' in this._state)) console.warn("<UserListField> was created without expected data property 'searchTerm'");
		if (!('searchList' in this._state)) console.warn("<UserListField> was created without expected data property 'searchList'");
		if (!('userList' in this._state)) console.warn("<UserListField> was created without expected data property 'userList'");
		if (!('$basePath' in this._state)) console.warn("<UserListField> was created without expected data property '$basePath'");
		this._intro = true;

		this._handlers.destroy = [removeFromStore];

		this._fragment = create_main_fragment$a(this, this._state);

		this.root._oncreate.push(() => {
			oncreate$3.call(this);
			this.fire("update", { changed: assignTrue({}, this._state), current: this._state });
		});

		if (options.target) {
			if (options.hydrate) throw new Error("options.hydrate only works if the component was compiled with the `hydratable: true` option");
			this._fragment.c();
			this._mount(options.target, options.anchor);

			flush(this);
		}
	}

	assign(UserListField.prototype, protoDev);
	assign(UserListField.prototype, methods$7);

	UserListField.prototype._checkReadOnly = function _checkReadOnly(newState) {
	};

	/* src/app/FrontPageLayoutEditor.html generated by Svelte v2.15.3 */



	function data$8() {
	  return {
	    visible: false,

	    // Drag-reordering
	    reordering: false,
	    triggerIndex: -1,
	    dragHoverIndex: -1,

	    // Any previous state saved
	    oldState: null, // { items }

	    // FIXME: Rename to `courses_list`
	    courses: [
	      // ["id", "Course Name"],
	    ],
	    themes: [
	      // ["id", "Theme Name"],
	    ],
	    items: [
	      // {
	      //   _editing: false,
	      //   // oldFields = Fields from other themes when the user changes the theme
	      //   _oldFields: { _lastTheme: 'theme-id', 'theme-id': { ...data } },
	      //
	      //   // Generally 1-to-1 correspondance with drupal db schema
	      //   id: null,
	      //   name: "Arabic",
	      //   type: "custom",
	      //   themeId: "theme1",
	      //   courseId: 0,
	      //   data: {
	      //     en: {
	      //       fields: [
	      //         {
	      //           name: "left feld",
	      //           value: "Nothing to be said",
	      //           type: "text",
	      //         },
	      //         {
	      //           name: "funny fald",
	      //           value: "Things of value",
	      //           type: "image",
	      //         },
	      //       ],
	      //       theme: "theme1",
	      //       themeMap: {
	      //         "0": 0,
	      //       },
	      //       themeVariables: {
	      //         setting: "No value",
	      //       },
	      //     },
	      //   },
	      // },
	    ],
	  };
	}
	var methods$8 = {
	  addCourseItem() {
	    const { items } = this.get();
	    const { categories } = this.store.get();
	    const firstCourseId = categories[0].programs[0].pid;

	    const newItem = {
	      _editing: false,
	      _oldFields: {
	        _lastTheme: "",
	      },

	      // Generally 1-to-1 correspondance with drupal db schema
	      id: null,
	      name: "New Course",
	      type: "course",
	      themeId: "",
	      courseId: firstCourseId,
	      data: {},
	    };

	    items.push(newItem);
	    this.set({
	      items,
	    });
	  },

	  addCustomItem() {
	    const { items } = this.get();
	    const themeId = this.getFirstThemeId();
	    const themeFields = this.getThemeUserFields(themeId);

	    const newItem = {
	      _editing: false,
	      _oldFields: {
	        _lastTheme: themeId,
	      },

	      // Generally 1-to-1 correspondance with drupal db schema
	      id: null,
	      name: "New Name",
	      type: "custom",
	      themeId: themeId,
	      courseId: 0,
	      data: {
	        en: {
	          theme: themeId,
	          fields: themeFields.fields,
	          themeMap: themeFields.themeMap,
	          themeVariables: themeFields.themeVariables,
	          themeVariableTypes: themeFields.themeVariableTypes,
	        },
	      },
	    };

	    items.push(newItem);
	    this.set({
	      items,
	    });
	  },

	  removeItem(index) {
	    const { items } = this.get();

	    items.splice(index, 1);
	    this.set({
	      items,
	    });
	  },

	  editItem(index) {
	    const { items } = this.get();
	    const item = items[index];

	    item._editing = !item._editing;
	    this.set({
	      items,
	    });
	  },

	  save() {
	    const { items } = this.get();
	    const savedItems = [];

	    for (const item of items) {
	      const {
	        _editing,
	        _oldFields,
	        ...savedItem
	      } = item;

	      savedItems.push(savedItem);
	    }

	    this.store.fire("savefrontpagelayout", {
	      items: savedItems,
	    });

	    // Save items
	    console.log(savedItems);

	    this.set({
	      visible: false,
	      oldState: null
	    });
	  },

	  cancel() {
	    this.set({
	      visible: false,
	    });
	  },

	  // Themes
	  themeChange(index) {
	    const { items } = this.get();

	    const item = items[index];
	    const themeId = item.themeId;
	    const oldFields = item._oldFields;
	    const lastThemeId = oldFields._lastThemeId;

	    if (themeId === lastThemeId) {
	      // Nothing changed
	      return;
	    }

	    // "Save" current fields
	    oldFields[lastThemeId] = item.data;

	    if (oldFields[themeId]) {
	      // Load old fields for this theme
	      item.data = oldFields[themeId];
	    } else {
	      // Create new fields for this theme
	      const themeFields = this.getThemeUserFields(themeId);

	      // FIXME: !!!LOCALIZE!!!
	      item.data = {
	        en: {
	          theme: themeId,
	          fields: themeFields.fields,
	          themeMap: themeFields.themeMap,
	          themeVariables: themeFields.themeVariables,
	          themeVariableTypes: themeFields.themeVariableTypes,
	        },
	      };
	    }

	    item._oldFields._lastTheme = themeId;
	    console.log("THEME CHANGE", items[index].themeId, items[index].data);
	    this.set({
	      items,
	    });
	  },

	  getFirstThemeId() {
	    const { themes } = this.store.get();
	    const firstTheme = themes[themes.length - 1];

	    return firstTheme.id;
	  },

	  getThemeList() {
	    const { themes } = this.store.get();

	    return themes.map(t => [t.id, t.name]);
	  },

	  getTheme(themeId) {
	    const { themeIdMap } = this.store.get();
	    return themeIdMap[themeId];
	  },

	  getThemeUserFields(themeId) {
	    return getThemeUserFields(themeId, this.store);
	  },

	  openThemePreview(index) {
	    const { items } = this.get();
	    const item = items[index];

	    // Open theme preview here...
	  },

	  // Drag-reordering
	  dragReorder(index, evt) {
	    evt.preventDefault();
	    this.set({
	      reordering: true,
	      triggerIndex: index,
	      dragHoverIndex: -1,
	    });

	    const moveCb = (evt) => {
	      evt.preventDefault();
	    };

	    const cb = () => {
	      const { dragHoverIndex, items } = this.get();

	      if (dragHoverIndex >= 0) {
	        const current = items[index];
	        const replace = items[dragHoverIndex];
	        let oldIndex = index;

	        if (dragHoverIndex < index) {
	          // Adjust index to accomodate for the shift
	          oldIndex++;
	        }

	        if (dragHoverIndex < index) {
	          // After item to replace
	          items.splice(dragHoverIndex, 0, current);
	        } else {
	          // After item to repace
	          items.splice(dragHoverIndex + 1, 0, current);
	        }

	        items.splice(oldIndex, 1);

	        // Apply final reordering
	        this.set({
	          reordering: false,
	          items,
	        });
	      } else {
	        this.set({
	          reordering: false,
	        });
	      }

	      window.removeEventListener("mouseup", cb);
	      window.removeEventListener("mousemove", moveCb);
	    };

	    window.addEventListener("mouseup", cb);
	    window.addEventListener("mousemove", moveCb);
	  },

	  dragHover(index) {
	    const { triggerIndex } = this.get();

	    if (index !== triggerIndex) {
	      this.set({
	        dragHoverIndex: index,
	      });
	    }
	  },

	  dragBlur(index) {
	    const { dragHoverIndex } = this.get();

	    if (dragHoverIndex === index) {
	      this.set({
	        dragHoverIndex: -1,
	      });
	    }
	  },
	};

	function oncreate$4() {
	  const listener = this.store.on("setfrontpagelayouteditor", ({ visible }) => {
	    let extraState = {};

	    if (visible) {
	      const { oldState, items } = this.get();

	      if (oldState) {
	        // Load previously saved items
	        extraState = {
	          items: collapsedItems(oldState.items),
	          oldState: null,
	        };
	      } else {
	        // Copy current items in case we'll need to revert back to them later
	        extraState = {
	          oldState: deepCopy({
	            items: collapsedItems(items),
	          }),
	        };
	      }
	    }

	    this.set({
	      visible,
	      ...extraState
	    });
	  });

	  const collapsedItems = (items) => items.map(item => (item._editing = false, item));

	  // Sync courses dropdown with new/removed courses
	  const listener2 = this.store.on("state", ({ changed, current }) => {
	    if (changed.localCourses) {
	      updateCourseList();
	    }
	  });

	  // Update course list when categories are loaded from the server
	  const listener3 = this.store.on("categoriesloaded", () => {
	    updateCourseList();
	  });

	  const updateCourseList = () => {
	    const courses = [];
	    const { language, categories } = this.store.get();

	    for (const { programs } of categories) {
	      for (const { pid: id, data: { current: { name } } } of programs) {
	        // @ts-ignore
	        courses.push([+id, name]);
	      }
	    }

	    this.set({
	      courses,
	    });
	  };

	  // Update new items with their new IDs when saved on the
	  // server
	  const listener4 = this.store.on("frontpagelayoutsaved", ({ ids }) => {
	    const { items } = this.get();

	    for (let i=0; i<ids.length; i++) {
	      items[i].id = ids[i];
	    }

	    this.set({
	      items,
	    });
	  });

	  this.on("destroy", () => {
	    listener.cancel();
	    listener2.cancel();
	    listener3.cancel();
	    listener4.cancel();
	  });

	  // Get initial course list
	  updateCourseList();

	  // Get theme list
	  const { themes } = this.store.get();
	  this.set({
	    themes: themes.map(t => [t.id, t.name]),
	  });

	  // Set items
	  const frontPageLayoutItems = this.store.get().frontPageLayout;
	  this.set({
	    items: frontPageLayoutItems.map(item => ({
	        _editing: false,
	        _oldFields: {
	          _lastTheme: item.themeId,
	          [item.themeId]: item.data,
	        },

	        ...item,
	      })),
	  });
	}
	const file$b = "src/app/FrontPageLayoutEditor.html";

	function mouseout_handler(event) {
		const { component, ctx } = this._svelte;

		component.dragBlur(ctx.index);
	}

	function mouseover_handler(event) {
		const { component, ctx } = this._svelte;

		component.dragHover(ctx.index);
	}

	function get_each_context_2(ctx, list, i) {
		const child_ctx = Object.create(ctx);
		child_ctx.field = list[i];
		child_ctx.each_value_2 = list;
		child_ctx.field_index = i;
		return child_ctx;
	}

	function get_each_context_1$3(ctx, list, i) {
		const child_ctx = Object.create(ctx);
		child_ctx.variable = list[i];
		return child_ctx;
	}

	function click_handler_2$1(event) {
		const { component, ctx } = this._svelte;

		component.openThemePreview(ctx.index);
	}

	function click_handler_1$2(event) {
		const { component, ctx } = this._svelte;

		component.editItem(ctx.index);
	}

	function click_handler$4(event) {
		const { component, ctx } = this._svelte;

		component.removeItem(ctx.index);
	}

	function mousedown_handler(event) {
		const { component, ctx } = this._svelte;

		component.dragReorder(ctx.index, event);
	}

	function get_each_context$5(ctx, list, i) {
		const child_ctx = Object.create(ctx);
		child_ctx.item = list[i];
		child_ctx.each_value = list;
		child_ctx.index = i;
		return child_ctx;
	}

	function create_main_fragment$b(component, ctx) {
		var div11, div0, text0, div10, div9, div1, text1, div8, div3, div2, button0, text3, div7, div4, button1, text5, div5, button2, text7, div6, button3;

		var each_value = ctx.items;

		var each_blocks = [];

		for (var i = 0; i < each_value.length; i += 1) {
			each_blocks[i] = create_each_block$5(component, get_each_context$5(ctx, each_value, i));
		}

		function click_handler_3(event) {
			component.cancel();
		}

		function click_handler_4(event) {
			component.save();
		}

		function click_handler_5(event) {
			component.addCustomItem();
		}

		function click_handler_6(event) {
			component.addCourseItem();
		}

		return {
			c: function create() {
				div11 = createElement("div");
				div0 = createElement("div");
				text0 = createText("\n  ");
				div10 = createElement("div");
				div9 = createElement("div");
				div1 = createElement("div");

				for (var i = 0; i < each_blocks.length; i += 1) {
					each_blocks[i].c();
				}

				text1 = createText("\n\n      ");
				div8 = createElement("div");
				div3 = createElement("div");
				div2 = createElement("div");
				button0 = createElement("button");
				button0.textContent = "Cancel";
				text3 = createText("\n        ");
				div7 = createElement("div");
				div4 = createElement("div");
				button1 = createElement("button");
				button1.textContent = "Save";
				text5 = createText("\n          ");
				div5 = createElement("div");
				button2 = createElement("button");
				button2.textContent = "Add Custom";
				text7 = createText("\n          ");
				div6 = createElement("div");
				button3 = createElement("button");
				button3.textContent = "Add Course";
				div0.className = "modal-background";
				addLoc(div0, file$b, 3, 2, 50);
				div1.className = "fpl-item-wrap svelte-8dpd95";
				addLoc(div1, file$b, 10, 6, 292);
				addListener(button0, "click", click_handler_3);
				button0.className = "button is-warning";
				addLoc(button0, file$b, 139, 12, 5430);
				div2.className = "level-item";
				addLoc(div2, file$b, 138, 10, 5393);
				div3.className = "level-left";
				addLoc(div3, file$b, 137, 8, 5358);
				addListener(button1, "click", click_handler_4);
				button1.className = "button is-link";
				addLoc(button1, file$b, 148, 12, 5669);
				div4.className = "level-item";
				addLoc(div4, file$b, 147, 10, 5632);
				addListener(button2, "click", click_handler_5);
				button2.className = "button is-link";
				addLoc(button2, file$b, 155, 12, 5852);
				div5.className = "level-item";
				addLoc(div5, file$b, 154, 10, 5815);
				addListener(button3, "click", click_handler_6);
				button3.className = "button is-link";
				addLoc(button3, file$b, 162, 12, 6050);
				div6.className = "level-item";
				addLoc(div6, file$b, 161, 10, 6013);
				div7.className = "level-right";
				addLoc(div7, file$b, 146, 8, 5596);
				div8.className = "level button-wrap svelte-8dpd95";
				addLoc(div8, file$b, 136, 6, 5318);
				div9.className = "box front-page-layout-window";
				addLoc(div9, file$b, 9, 4, 243);
				div10.className = "modal-content svelte-8dpd95";
				addLoc(div10, file$b, 8, 2, 211);
				div11.className = "modal";
				toggleClass(div11, "is-active", ctx.visible);
				addLoc(div11, file$b, 0, 0, 0);
			},

			m: function mount(target, anchor) {
				insert(target, div11, anchor);
				append(div11, div0);
				append(div11, text0);
				append(div11, div10);
				append(div10, div9);
				append(div9, div1);

				for (var i = 0; i < each_blocks.length; i += 1) {
					each_blocks[i].m(div1, null);
				}

				append(div9, text1);
				append(div9, div8);
				append(div8, div3);
				append(div3, div2);
				append(div2, button0);
				append(div8, text3);
				append(div8, div7);
				append(div7, div4);
				append(div4, button1);
				append(div7, text5);
				append(div7, div5);
				append(div5, button2);
				append(div7, text7);
				append(div7, div6);
				append(div6, button3);
			},

			p: function update(changed, ctx) {
				if (changed.reordering || changed.dragHoverIndex || changed.items || changed.$locale || changed.Object || changed.themes || changed.courses || changed.$themeIdMap) {
					each_value = ctx.items;

					for (var i = 0; i < each_value.length; i += 1) {
						const child_ctx = get_each_context$5(ctx, each_value, i);

						if (each_blocks[i]) {
							each_blocks[i].p(changed, child_ctx);
						} else {
							each_blocks[i] = create_each_block$5(component, child_ctx);
							each_blocks[i].c();
							each_blocks[i].m(div1, null);
						}
					}

					for (; i < each_blocks.length; i += 1) {
						each_blocks[i].d(1);
					}
					each_blocks.length = each_value.length;
				}

				if (changed.visible) {
					toggleClass(div11, "is-active", ctx.visible);
				}
			},

			d: function destroy$$1(detach) {
				if (detach) {
					detachNode(div11);
				}

				destroyEach(each_blocks, detach);

				removeListener(button0, "click", click_handler_3);
				removeListener(button1, "click", click_handler_4);
				removeListener(button2, "click", click_handler_5);
				removeListener(button3, "click", click_handler_6);
			}
		};
	}

	// (32:18) {#if item.id === null}
	function create_if_block_13(component, ctx) {
		var span;

		return {
			c: function create() {
				span = createElement("span");
				span.textContent = "*";
				span.className = "has-text-link";
				addLoc(span, file$b, 32, 20, 1097);
			},

			m: function mount(target, anchor) {
				insert(target, span, anchor);
			},

			d: function destroy$$1(detach) {
				if (detach) {
					detachNode(span);
				}
			}
		};
	}

	// (54:12) {#if item._editing}
	function create_if_block$7(component, ctx) {
		var div2, editabletextfield_updating = {}, text0, text1, div1, h4, text3, div0, button, i, text4, dropdown_updating = {}, text5;

		var editabletextfield_initial_data = { label: "Item Name" };
		if (ctx.item.name !== void 0) {
			editabletextfield_initial_data.value = ctx.item.name;
			editabletextfield_updating.value = true;
		}
		var editabletextfield = new EditableTextField({
			root: component.root,
			store: component.store,
			data: editabletextfield_initial_data,
			_bind(changed, childState) {
				var newState = {};
				if (!editabletextfield_updating.value && changed.value) {
					ctx.item.name = childState.value;

					newState.items = ctx.items;
				}
				component._set(newState);
				editabletextfield_updating = {};
			}
		});

		component.root._beforecreate.push(() => {
			editabletextfield._bind({ value: 1 }, editabletextfield.get());
		});

		var if_block0 = (ctx.item.type === "course") && create_if_block_12(component, ctx);

		var dropdown_initial_data = {
		 	list: ctx.themes,
		 	right: true
		 };
		if (ctx.item.themeId !== void 0) {
			dropdown_initial_data.value = ctx.item.themeId;
			dropdown_updating.value = true;
		}
		var dropdown = new Dropdown({
			root: component.root,
			store: component.store,
			data: dropdown_initial_data,
			_bind(changed, childState) {
				var newState = {};
				if (!dropdown_updating.value && changed.value) {
					ctx.item.themeId = childState.value;

					newState.items = ctx.items;
				}
				component._set(newState);
				dropdown_updating = {};
			}
		});

		component.root._beforecreate.push(() => {
			dropdown._bind({ value: 1 }, dropdown.get());
		});

		dropdown.on("change", function(event) {
			component.themeChange(ctx.index);
		});

		var if_block1 = (ctx.item.type === "custom") && create_if_block_1$5(component, ctx);

		return {
			c: function create() {
				div2 = createElement("div");
				editabletextfield._fragment.c();
				text0 = createText("\n\n                ");
				if (if_block0) if_block0.c();
				text1 = createText("\n\n                ");
				div1 = createElement("div");
				h4 = createElement("h4");
				h4.textContent = "Theme";
				text3 = createText("\n                  ");
				div0 = createElement("div");
				button = createElement("button");
				i = createElement("i");
				text4 = createText("\n                    ");
				dropdown._fragment.c();
				text5 = createText("\n\n                ");
				if (if_block1) if_block1.c();
				h4.className = "title is-5 svelte-8dpd95";
				addLoc(h4, file$b, 70, 18, 2462);
				i.className = "fa fa-desktop";
				addLoc(i, file$b, 75, 22, 2673);

				button._svelte = { component, ctx };

				addListener(button, "click", click_handler_2$1);
				button.className = "button is-link";
				addLoc(button, file$b, 72, 20, 2540);
				addLoc(div0, file$b, 71, 18, 2514);
				div1.className = "fpl-property svelte-8dpd95";
				addLoc(div1, file$b, 69, 16, 2417);
				div2.className = "fpl-item-fields has-background-primary svelte-8dpd95";
				addLoc(div2, file$b, 54, 14, 1864);
			},

			m: function mount(target, anchor) {
				insert(target, div2, anchor);
				editabletextfield._mount(div2, null);
				append(div2, text0);
				if (if_block0) if_block0.m(div2, null);
				append(div2, text1);
				append(div2, div1);
				append(div1, h4);
				append(div1, text3);
				append(div1, div0);
				append(div0, button);
				append(button, i);
				append(div0, text4);
				dropdown._mount(div0, null);
				append(div2, text5);
				if (if_block1) if_block1.m(div2, null);
			},

			p: function update(changed, _ctx) {
				ctx = _ctx;
				var editabletextfield_changes = {};
				if (!editabletextfield_updating.value && changed.items) {
					editabletextfield_changes.value = ctx.item.name;
					editabletextfield_updating.value = ctx.item.name !== void 0;
				}
				editabletextfield._set(editabletextfield_changes);
				editabletextfield_updating = {};

				if (ctx.item.type === "course") {
					if (if_block0) {
						if_block0.p(changed, ctx);
					} else {
						if_block0 = create_if_block_12(component, ctx);
						if_block0.c();
						if_block0.m(div2, text1);
					}
				} else if (if_block0) {
					if_block0.d(1);
					if_block0 = null;
				}

				button._svelte.ctx = ctx;

				var dropdown_changes = {};
				if (changed.themes) dropdown_changes.list = ctx.themes;
				if (!dropdown_updating.value && changed.items) {
					dropdown_changes.value = ctx.item.themeId;
					dropdown_updating.value = ctx.item.themeId !== void 0;
				}
				dropdown._set(dropdown_changes);
				dropdown_updating = {};

				if (ctx.item.type === "custom") {
					if (if_block1) {
						if_block1.p(changed, ctx);
					} else {
						if_block1 = create_if_block_1$5(component, ctx);
						if_block1.c();
						if_block1.m(div2, null);
					}
				} else if (if_block1) {
					if_block1.d(1);
					if_block1 = null;
				}
			},

			d: function destroy$$1(detach) {
				if (detach) {
					detachNode(div2);
				}

				editabletextfield.destroy();
				if (if_block0) if_block0.d();
				removeListener(button, "click", click_handler_2$1);
				dropdown.destroy();
				if (if_block1) if_block1.d();
			}
		};
	}

	// (58:16) {#if item.type === "course"}
	function create_if_block_12(component, ctx) {
		var div1, h4, text_1, div0, dropdown_updating = {};

		var dropdown_initial_data = {
		 	list: ctx.courses,
		 	right: true
		 };
		if (ctx.item.courseId !== void 0) {
			dropdown_initial_data.value = ctx.item.courseId;
			dropdown_updating.value = true;
		}
		var dropdown = new Dropdown({
			root: component.root,
			store: component.store,
			data: dropdown_initial_data,
			_bind(changed, childState) {
				var newState = {};
				if (!dropdown_updating.value && changed.value) {
					ctx.item.courseId = childState.value;

					newState.items = ctx.items;
				}
				component._set(newState);
				dropdown_updating = {};
			}
		});

		component.root._beforecreate.push(() => {
			dropdown._bind({ value: 1 }, dropdown.get());
		});

		return {
			c: function create() {
				div1 = createElement("div");
				h4 = createElement("h4");
				h4.textContent = "Course";
				text_1 = createText("\n                    ");
				div0 = createElement("div");
				dropdown._fragment.c();
				h4.className = "title is-5 svelte-8dpd95";
				addLoc(h4, file$b, 59, 20, 2105);
				addLoc(div0, file$b, 60, 20, 2160);
				div1.className = "fpl-property svelte-8dpd95";
				addLoc(div1, file$b, 58, 18, 2058);
			},

			m: function mount(target, anchor) {
				insert(target, div1, anchor);
				append(div1, h4);
				append(div1, text_1);
				append(div1, div0);
				dropdown._mount(div0, null);
			},

			p: function update(changed, _ctx) {
				ctx = _ctx;
				var dropdown_changes = {};
				if (changed.courses) dropdown_changes.list = ctx.courses;
				if (!dropdown_updating.value && changed.items) {
					dropdown_changes.value = ctx.item.courseId;
					dropdown_updating.value = ctx.item.courseId !== void 0;
				}
				dropdown._set(dropdown_changes);
				dropdown_updating = {};
			},

			d: function destroy$$1(detach) {
				if (detach) {
					detachNode(div1);
				}

				dropdown.destroy();
			}
		};
	}

	// (86:16) {#if item.type === "custom"}
	function create_if_block_1$5(component, ctx) {
		var text, if_block1_anchor;

		var if_block0 = (ctx.item.data.en.themeVariableTypes) && create_if_block_7(component, ctx);

		var if_block1 = ((ctx.item.data.en.fields || "").length) && create_if_block_2$3(component, ctx);

		return {
			c: function create() {
				if (if_block0) if_block0.c();
				text = createText("\n\n                  ");
				if (if_block1) if_block1.c();
				if_block1_anchor = createComment();
			},

			m: function mount(target, anchor) {
				if (if_block0) if_block0.m(target, anchor);
				insert(target, text, anchor);
				if (if_block1) if_block1.m(target, anchor);
				insert(target, if_block1_anchor, anchor);
			},

			p: function update(changed, ctx) {
				if (ctx.item.data.en.themeVariableTypes) {
					if (if_block0) {
						if_block0.p(changed, ctx);
					} else {
						if_block0 = create_if_block_7(component, ctx);
						if_block0.c();
						if_block0.m(text.parentNode, text);
					}
				} else if (if_block0) {
					if_block0.d(1);
					if_block0 = null;
				}

				if ((ctx.item.data.en.fields || "").length) {
					if (if_block1) {
						if_block1.p(changed, ctx);
					} else {
						if_block1 = create_if_block_2$3(component, ctx);
						if_block1.c();
						if_block1.m(if_block1_anchor.parentNode, if_block1_anchor);
					}
				} else if (if_block1) {
					if_block1.d(1);
					if_block1 = null;
				}
			},

			d: function destroy$$1(detach) {
				if (if_block0) if_block0.d(detach);
				if (detach) {
					detachNode(text);
				}

				if (if_block1) if_block1.d(detach);
				if (detach) {
					detachNode(if_block1_anchor);
				}
			}
		};
	}

	// (90:18) {#if item.data.en.themeVariableTypes}
	function create_if_block_7(component, ctx) {
		var hr, text, each_anchor;

		var each_value_1 = ctx.Object.entries(ctx.item.data.en.themeVariableTypes);

		var each_blocks = [];

		for (var i = 0; i < each_value_1.length; i += 1) {
			each_blocks[i] = create_each_block_2(component, get_each_context_1$3(ctx, each_value_1, i));
		}

		return {
			c: function create() {
				hr = createElement("hr");
				text = createText("\n\n                    ");

				for (var i = 0; i < each_blocks.length; i += 1) {
					each_blocks[i].c();
				}

				each_anchor = createComment();
				hr.className = "svelte-8dpd95";
				addLoc(hr, file$b, 90, 20, 3221);
			},

			m: function mount(target, anchor) {
				insert(target, hr, anchor);
				insert(target, text, anchor);

				for (var i = 0; i < each_blocks.length; i += 1) {
					each_blocks[i].m(target, anchor);
				}

				insert(target, each_anchor, anchor);
			},

			p: function update(changed, ctx) {
				if (changed.Object || changed.items || changed.$locale) {
					each_value_1 = ctx.Object.entries(ctx.item.data.en.themeVariableTypes);

					for (var i = 0; i < each_value_1.length; i += 1) {
						const child_ctx = get_each_context_1$3(ctx, each_value_1, i);

						if (each_blocks[i]) {
							each_blocks[i].p(changed, child_ctx);
						} else {
							each_blocks[i] = create_each_block_2(component, child_ctx);
							each_blocks[i].c();
							each_blocks[i].m(each_anchor.parentNode, each_anchor);
						}
					}

					for (; i < each_blocks.length; i += 1) {
						each_blocks[i].d(1);
					}
					each_blocks.length = each_value_1.length;
				}
			},

			d: function destroy$$1(detach) {
				if (detach) {
					detachNode(hr);
					detachNode(text);
				}

				destroyEach(each_blocks, detach);

				if (detach) {
					detachNode(each_anchor);
				}
			}
		};
	}

	// (103:65) 
	function create_if_block_11(component, ctx) {
		var userlistfield_updating = {};

		var userlistfield_initial_data = {};
		if (ctx.item.data.en.themeVariables[ctx.variable[0]] !== void 0) {
			userlistfield_initial_data.value = ctx.item.data.en.themeVariables[ctx.variable[0]];
			userlistfield_updating.value = true;
		}
		var userlistfield = new UserListField({
			root: component.root,
			store: component.store,
			data: userlistfield_initial_data,
			_bind(changed, childState) {
				var newState = {};
				if (!userlistfield_updating.value && changed.value) {
					ctx.item.data.en.themeVariables[ctx.variable[0]] = childState.value;

					newState.items = ctx.items;
					newState.Object = ctx.Object;
				}
				component._set(newState);
				userlistfield_updating = {};
			}
		});

		component.root._beforecreate.push(() => {
			userlistfield._bind({ value: 1 }, userlistfield.get());
		});

		return {
			c: function create() {
				userlistfield._fragment.c();
			},

			m: function mount(target, anchor) {
				userlistfield._mount(target, anchor);
			},

			p: function update(changed, _ctx) {
				ctx = _ctx;
				var userlistfield_changes = {};
				if (!userlistfield_updating.value && changed.items || changed.Object) {
					userlistfield_changes.value = ctx.item.data.en.themeVariables[ctx.variable[0]];
					userlistfield_updating.value = ctx.item.data.en.themeVariables[ctx.variable[0]] !== void 0;
				}
				userlistfield._set(userlistfield_changes);
				userlistfield_updating = {};
			},

			d: function destroy$$1(detach) {
				userlistfield.destroy(detach);
			}
		};
	}

	// (101:62) 
	function create_if_block_10(component, ctx) {
		var filefield_updating = {};

		var filefield_initial_data = {};
		if (ctx.item.data.en.themeVariables[ctx.variable[0]] !== void 0) {
			filefield_initial_data.value = ctx.item.data.en.themeVariables[ctx.variable[0]];
			filefield_updating.value = true;
		}
		var filefield = new FileField({
			root: component.root,
			store: component.store,
			data: filefield_initial_data,
			_bind(changed, childState) {
				var newState = {};
				if (!filefield_updating.value && changed.value) {
					ctx.item.data.en.themeVariables[ctx.variable[0]] = childState.value;

					newState.items = ctx.items;
					newState.Object = ctx.Object;
				}
				component._set(newState);
				filefield_updating = {};
			}
		});

		component.root._beforecreate.push(() => {
			filefield._bind({ value: 1 }, filefield.get());
		});

		return {
			c: function create() {
				filefield._fragment.c();
			},

			m: function mount(target, anchor) {
				filefield._mount(target, anchor);
			},

			p: function update(changed, _ctx) {
				ctx = _ctx;
				var filefield_changes = {};
				if (!filefield_updating.value && changed.items || changed.Object) {
					filefield_changes.value = ctx.item.data.en.themeVariables[ctx.variable[0]];
					filefield_updating.value = ctx.item.data.en.themeVariables[ctx.variable[0]] !== void 0;
				}
				filefield._set(filefield_changes);
				filefield_updating = {};
			},

			d: function destroy$$1(detach) {
				filefield.destroy(detach);
			}
		};
	}

	// (99:64) 
	function create_if_block_9(component, ctx) {
		var editabletextareafield_updating = {};

		var editabletextareafield_initial_data = { label: ctx.$locale.fpeFieldValueLabel };
		if (ctx.item.data.en.themeVariables[ctx.variable[0]] !== void 0) {
			editabletextareafield_initial_data.value = ctx.item.data.en.themeVariables[ctx.variable[0]];
			editabletextareafield_updating.value = true;
		}
		var editabletextareafield = new EditableTextAreaField({
			root: component.root,
			store: component.store,
			data: editabletextareafield_initial_data,
			_bind(changed, childState) {
				var newState = {};
				if (!editabletextareafield_updating.value && changed.value) {
					ctx.item.data.en.themeVariables[ctx.variable[0]] = childState.value;

					newState.items = ctx.items;
					newState.Object = ctx.Object;
				}
				component._set(newState);
				editabletextareafield_updating = {};
			}
		});

		component.root._beforecreate.push(() => {
			editabletextareafield._bind({ value: 1 }, editabletextareafield.get());
		});

		return {
			c: function create() {
				editabletextareafield._fragment.c();
			},

			m: function mount(target, anchor) {
				editabletextareafield._mount(target, anchor);
			},

			p: function update(changed, _ctx) {
				ctx = _ctx;
				var editabletextareafield_changes = {};
				if (changed.$locale) editabletextareafield_changes.label = ctx.$locale.fpeFieldValueLabel;
				if (!editabletextareafield_updating.value && changed.items || changed.Object) {
					editabletextareafield_changes.value = ctx.item.data.en.themeVariables[ctx.variable[0]];
					editabletextareafield_updating.value = ctx.item.data.en.themeVariables[ctx.variable[0]] !== void 0;
				}
				editabletextareafield._set(editabletextareafield_changes);
				editabletextareafield_updating = {};
			},

			d: function destroy$$1(detach) {
				editabletextareafield.destroy(detach);
			}
		};
	}

	// (97:24) {#if variable[1].type === "text"}
	function create_if_block_8(component, ctx) {
		var editabletextfield_updating = {};

		var editabletextfield_initial_data = { label: ctx.$locale.fpeFieldValueLabel };
		if (ctx.item.data.en.themeVariables[ctx.variable[0]] !== void 0) {
			editabletextfield_initial_data.value = ctx.item.data.en.themeVariables[ctx.variable[0]];
			editabletextfield_updating.value = true;
		}
		var editabletextfield = new EditableTextField({
			root: component.root,
			store: component.store,
			data: editabletextfield_initial_data,
			_bind(changed, childState) {
				var newState = {};
				if (!editabletextfield_updating.value && changed.value) {
					ctx.item.data.en.themeVariables[ctx.variable[0]] = childState.value;

					newState.items = ctx.items;
					newState.Object = ctx.Object;
				}
				component._set(newState);
				editabletextfield_updating = {};
			}
		});

		component.root._beforecreate.push(() => {
			editabletextfield._bind({ value: 1 }, editabletextfield.get());
		});

		return {
			c: function create() {
				editabletextfield._fragment.c();
			},

			m: function mount(target, anchor) {
				editabletextfield._mount(target, anchor);
			},

			p: function update(changed, _ctx) {
				ctx = _ctx;
				var editabletextfield_changes = {};
				if (changed.$locale) editabletextfield_changes.label = ctx.$locale.fpeFieldValueLabel;
				if (!editabletextfield_updating.value && changed.items || changed.Object) {
					editabletextfield_changes.value = ctx.item.data.en.themeVariables[ctx.variable[0]];
					editabletextfield_updating.value = ctx.item.data.en.themeVariables[ctx.variable[0]] !== void 0;
				}
				editabletextfield._set(editabletextfield_changes);
				editabletextfield_updating = {};
			},

			d: function destroy$$1(detach) {
				editabletextfield.destroy(detach);
			}
		};
	}

	// (93:20) {#each Object.entries(item.data.en.themeVariableTypes) as variable}
	function create_each_block_2(component, ctx) {
		var div, h5, text0_value = ctx.variable[0], text0, text1;

		function select_block_type(ctx) {
			if (ctx.variable[1].type === "text") return create_if_block_8;
			if (ctx.variable[1].type === "bigtext") return create_if_block_9;
			if (ctx.variable[1].type === "image") return create_if_block_10;
			if (ctx.variable[1].type === "userlist") return create_if_block_11;
		}

		var current_block_type = select_block_type(ctx);
		var if_block = current_block_type && current_block_type(component, ctx);

		return {
			c: function create() {
				div = createElement("div");
				h5 = createElement("h5");
				text0 = createText(text0_value);
				text1 = createText("\n\n                        ");
				if (if_block) if_block.c();
				h5.className = "title is-5 svelte-8dpd95";
				addLoc(h5, file$b, 94, 24, 3390);
				div.className = "fpl-item-field svelte-8dpd95";
				addLoc(div, file$b, 93, 22, 3337);
			},

			m: function mount(target, anchor) {
				insert(target, div, anchor);
				append(div, h5);
				append(h5, text0);
				append(div, text1);
				if (if_block) if_block.m(div, null);
			},

			p: function update(changed, ctx) {
				if ((changed.Object || changed.items) && text0_value !== (text0_value = ctx.variable[0])) {
					setData(text0, text0_value);
				}

				if (current_block_type === (current_block_type = select_block_type(ctx)) && if_block) {
					if_block.p(changed, ctx);
				} else {
					if (if_block) if_block.d(1);
					if_block = current_block_type && current_block_type(component, ctx);
					if (if_block) if_block.c();
					if (if_block) if_block.m(div, null);
				}
			},

			d: function destroy$$1(detach) {
				if (detach) {
					detachNode(div);
				}

				if (if_block) if_block.d();
			}
		};
	}

	// (110:18) {#if (item.data.en.fields || "").length}
	function create_if_block_2$3(component, ctx) {
		var hr, text, each_anchor;

		var each_value_2 = ctx.item.data.en.fields;

		var each_blocks = [];

		for (var i = 0; i < each_value_2.length; i += 1) {
			each_blocks[i] = create_each_block_1$3(component, get_each_context_2(ctx, each_value_2, i));
		}

		return {
			c: function create() {
				hr = createElement("hr");
				text = createText("\n\n                    ");

				for (var i = 0; i < each_blocks.length; i += 1) {
					each_blocks[i].c();
				}

				each_anchor = createComment();
				hr.className = "svelte-8dpd95";
				addLoc(hr, file$b, 110, 20, 4338);
			},

			m: function mount(target, anchor) {
				insert(target, hr, anchor);
				insert(target, text, anchor);

				for (var i = 0; i < each_blocks.length; i += 1) {
					each_blocks[i].m(target, anchor);
				}

				insert(target, each_anchor, anchor);
			},

			p: function update(changed, ctx) {
				if (changed.items || changed.$locale) {
					each_value_2 = ctx.item.data.en.fields;

					for (var i = 0; i < each_value_2.length; i += 1) {
						const child_ctx = get_each_context_2(ctx, each_value_2, i);

						if (each_blocks[i]) {
							each_blocks[i].p(changed, child_ctx);
						} else {
							each_blocks[i] = create_each_block_1$3(component, child_ctx);
							each_blocks[i].c();
							each_blocks[i].m(each_anchor.parentNode, each_anchor);
						}
					}

					for (; i < each_blocks.length; i += 1) {
						each_blocks[i].d(1);
					}
					each_blocks.length = each_value_2.length;
				}
			},

			d: function destroy$$1(detach) {
				if (detach) {
					detachNode(hr);
					detachNode(text);
				}

				destroyEach(each_blocks, detach);

				if (detach) {
					detachNode(each_anchor);
				}
			}
		};
	}

	// (123:59) 
	function create_if_block_6$1(component, ctx) {
		var userlistfield_updating = {};

		var userlistfield_initial_data = {};
		if (ctx.field.value !== void 0) {
			userlistfield_initial_data.value = ctx.field.value;
			userlistfield_updating.value = true;
		}
		var userlistfield = new UserListField({
			root: component.root,
			store: component.store,
			data: userlistfield_initial_data,
			_bind(changed, childState) {
				var newState = {};
				if (!userlistfield_updating.value && changed.value) {
					ctx.field.value = childState.value;

					newState.items = ctx.items;
				}
				component._set(newState);
				userlistfield_updating = {};
			}
		});

		component.root._beforecreate.push(() => {
			userlistfield._bind({ value: 1 }, userlistfield.get());
		});

		return {
			c: function create() {
				userlistfield._fragment.c();
			},

			m: function mount(target, anchor) {
				userlistfield._mount(target, anchor);
			},

			p: function update(changed, _ctx) {
				ctx = _ctx;
				var userlistfield_changes = {};
				if (!userlistfield_updating.value && changed.items) {
					userlistfield_changes.value = ctx.field.value;
					userlistfield_updating.value = ctx.field.value !== void 0;
				}
				userlistfield._set(userlistfield_changes);
				userlistfield_updating = {};
			},

			d: function destroy$$1(detach) {
				userlistfield.destroy(detach);
			}
		};
	}

	// (121:56) 
	function create_if_block_5$1(component, ctx) {
		var filefield_updating = {};

		var filefield_initial_data = {};
		if (ctx.field.value !== void 0) {
			filefield_initial_data.value = ctx.field.value;
			filefield_updating.value = true;
		}
		var filefield = new FileField({
			root: component.root,
			store: component.store,
			data: filefield_initial_data,
			_bind(changed, childState) {
				var newState = {};
				if (!filefield_updating.value && changed.value) {
					ctx.field.value = childState.value;

					newState.items = ctx.items;
				}
				component._set(newState);
				filefield_updating = {};
			}
		});

		component.root._beforecreate.push(() => {
			filefield._bind({ value: 1 }, filefield.get());
		});

		return {
			c: function create() {
				filefield._fragment.c();
			},

			m: function mount(target, anchor) {
				filefield._mount(target, anchor);
			},

			p: function update(changed, _ctx) {
				ctx = _ctx;
				var filefield_changes = {};
				if (!filefield_updating.value && changed.items) {
					filefield_changes.value = ctx.field.value;
					filefield_updating.value = ctx.field.value !== void 0;
				}
				filefield._set(filefield_changes);
				filefield_updating = {};
			},

			d: function destroy$$1(detach) {
				filefield.destroy(detach);
			}
		};
	}

	// (119:58) 
	function create_if_block_4$1(component, ctx) {
		var editabletextareafield_updating = {};

		var editabletextareafield_initial_data = { label: ctx.$locale.fpeFieldValueLabel };
		if (ctx.field.value !== void 0) {
			editabletextareafield_initial_data.value = ctx.field.value;
			editabletextareafield_updating.value = true;
		}
		var editabletextareafield = new EditableTextAreaField({
			root: component.root,
			store: component.store,
			data: editabletextareafield_initial_data,
			_bind(changed, childState) {
				var newState = {};
				if (!editabletextareafield_updating.value && changed.value) {
					ctx.field.value = childState.value;

					newState.items = ctx.items;
				}
				component._set(newState);
				editabletextareafield_updating = {};
			}
		});

		component.root._beforecreate.push(() => {
			editabletextareafield._bind({ value: 1 }, editabletextareafield.get());
		});

		return {
			c: function create() {
				editabletextareafield._fragment.c();
			},

			m: function mount(target, anchor) {
				editabletextareafield._mount(target, anchor);
			},

			p: function update(changed, _ctx) {
				ctx = _ctx;
				var editabletextareafield_changes = {};
				if (changed.$locale) editabletextareafield_changes.label = ctx.$locale.fpeFieldValueLabel;
				if (!editabletextareafield_updating.value && changed.items) {
					editabletextareafield_changes.value = ctx.field.value;
					editabletextareafield_updating.value = ctx.field.value !== void 0;
				}
				editabletextareafield._set(editabletextareafield_changes);
				editabletextareafield_updating = {};
			},

			d: function destroy$$1(detach) {
				editabletextareafield.destroy(detach);
			}
		};
	}

	// (117:24) {#if field.type === "text"}
	function create_if_block_3$3(component, ctx) {
		var editabletextfield_updating = {};

		var editabletextfield_initial_data = { label: ctx.$locale.fpeFieldValueLabel };
		if (ctx.field.value !== void 0) {
			editabletextfield_initial_data.value = ctx.field.value;
			editabletextfield_updating.value = true;
		}
		var editabletextfield = new EditableTextField({
			root: component.root,
			store: component.store,
			data: editabletextfield_initial_data,
			_bind(changed, childState) {
				var newState = {};
				if (!editabletextfield_updating.value && changed.value) {
					ctx.field.value = childState.value;

					newState.items = ctx.items;
				}
				component._set(newState);
				editabletextfield_updating = {};
			}
		});

		component.root._beforecreate.push(() => {
			editabletextfield._bind({ value: 1 }, editabletextfield.get());
		});

		return {
			c: function create() {
				editabletextfield._fragment.c();
			},

			m: function mount(target, anchor) {
				editabletextfield._mount(target, anchor);
			},

			p: function update(changed, _ctx) {
				ctx = _ctx;
				var editabletextfield_changes = {};
				if (changed.$locale) editabletextfield_changes.label = ctx.$locale.fpeFieldValueLabel;
				if (!editabletextfield_updating.value && changed.items) {
					editabletextfield_changes.value = ctx.field.value;
					editabletextfield_updating.value = ctx.field.value !== void 0;
				}
				editabletextfield._set(editabletextfield_changes);
				editabletextfield_updating = {};
			},

			d: function destroy$$1(detach) {
				editabletextfield.destroy(detach);
			}
		};
	}

	// (113:20) {#each item.data.en.fields as field}
	function create_each_block_1$3(component, ctx) {
		var div, h5, text0_value = ctx.field.name, text0, text1;

		function select_block_type_1(ctx) {
			if (ctx.field.type === "text") return create_if_block_3$3;
			if (ctx.field.type === "bigtext") return create_if_block_4$1;
			if (ctx.field.type === "image") return create_if_block_5$1;
			if (ctx.field.type === "userlist") return create_if_block_6$1;
		}

		var current_block_type = select_block_type_1(ctx);
		var if_block = current_block_type && current_block_type(component, ctx);

		return {
			c: function create() {
				div = createElement("div");
				h5 = createElement("h5");
				text0 = createText(text0_value);
				text1 = createText("\n\n                        ");
				if (if_block) if_block.c();
				h5.className = "title is-5 svelte-8dpd95";
				addLoc(h5, file$b, 114, 24, 4476);
				div.className = "fpl-item-field svelte-8dpd95";
				addLoc(div, file$b, 113, 22, 4423);
			},

			m: function mount(target, anchor) {
				insert(target, div, anchor);
				append(div, h5);
				append(h5, text0);
				append(div, text1);
				if (if_block) if_block.m(div, null);
			},

			p: function update(changed, ctx) {
				if ((changed.items) && text0_value !== (text0_value = ctx.field.name)) {
					setData(text0, text0_value);
				}

				if (current_block_type === (current_block_type = select_block_type_1(ctx)) && if_block) {
					if_block.p(changed, ctx);
				} else {
					if (if_block) if_block.d(1);
					if_block = current_block_type && current_block_type(component, ctx);
					if (if_block) if_block.c();
					if (if_block) if_block.m(div, null);
				}
			},

			d: function destroy$$1(detach) {
				if (detach) {
					detachNode(div);
				}

				if (if_block) if_block.d();
			}
		};
	}

	// (12:8) {#each items as item, index}
	function create_each_block$5(component, ctx) {
		var div7, div6, div0, i0, text0, i1, text1, div1, text2_value = ctx.item.type, text2, text3, div2, h3, text4_value = ctx.item.name, text4, text5, text6, h4, text7, b, text8_value = (ctx.$themeIdMap[ctx.item.themeId] || {name: ""}).name, text8, text9, div5, div3, i2, text10, div4, i3, text11;

		var if_block0 = (ctx.item.id === null) && create_if_block_13(component, ctx);

		var if_block1 = (ctx.item._editing) && create_if_block$7(component, ctx);

		return {
			c: function create() {
				div7 = createElement("div");
				div6 = createElement("div");
				div0 = createElement("div");
				i0 = createElement("i");
				text0 = createText("\n                ");
				i1 = createElement("i");
				text1 = createText("\n              ");
				div1 = createElement("div");
				text2 = createText(text2_value);
				text3 = createText("\n              ");
				div2 = createElement("div");
				h3 = createElement("h3");
				text4 = createText(text4_value);
				text5 = createText("\n\n                  ");
				if (if_block0) if_block0.c();
				text6 = createText("\n                ");
				h4 = createElement("h4");
				text7 = createText("Theme: ");
				b = createElement("b");
				text8 = createText(text8_value);
				text9 = createText("\n              ");
				div5 = createElement("div");
				div3 = createElement("div");
				i2 = createElement("i");
				text10 = createText("\n                ");
				div4 = createElement("div");
				i3 = createElement("i");
				text11 = createText("\n\n            ");
				if (if_block1) if_block1.c();
				i0.className = "fa fa-ellipsis-v svelte-8dpd95";
				addLoc(i0, file$b, 21, 16, 728);
				i1.className = "fa fa-ellipsis-v svelte-8dpd95";
				addLoc(i1, file$b, 22, 16, 777);

				div0._svelte = { component, ctx };

				addListener(div0, "mousedown", mousedown_handler);
				div0.className = "fpl-handle svelte-8dpd95";
				addLoc(div0, file$b, 18, 14, 614);
				div1.className = "fpl-item-type svelte-8dpd95";
				addLoc(div1, file$b, 24, 14, 845);
				h3.className = "fpl-item-name svelte-8dpd95";
				addLoc(h3, file$b, 28, 16, 978);
				addLoc(b, file$b, 36, 25, 1249);
				h4.className = "fpl-item-theme svelte-8dpd95";
				addLoc(h4, file$b, 35, 16, 1196);
				div2.className = "fpl-details svelte-8dpd95";
				addLoc(div2, file$b, 27, 14, 936);
				i2.className = "fa fa-times";
				addLoc(i2, file$b, 43, 18, 1532);

				div3._svelte = { component, ctx };

				addListener(div3, "click", click_handler$4);
				div3.className = "fpl-action has-background-link svelte-8dpd95";
				addLoc(div3, file$b, 40, 16, 1404);
				i3.className = "fa fa-pencil";
				addLoc(i3, file$b, 48, 18, 1725);

				div4._svelte = { component, ctx };

				addListener(div4, "click", click_handler_1$2);
				div4.className = "fpl-action has-background-link svelte-8dpd95";
				addLoc(div4, file$b, 45, 16, 1599);
				div5.className = "fpl-actions svelte-8dpd95";
				addLoc(div5, file$b, 39, 14, 1362);
				div6.className = "fpl-item-summary svelte-8dpd95";
				addLoc(div6, file$b, 17, 12, 569);

				div7._svelte = { component, ctx };

				addListener(div7, "mouseover", mouseover_handler);
				addListener(div7, "mouseout", mouseout_handler);
				div7.className = "fpl-item svelte-8dpd95";
				toggleClass(div7, "fpl-hover", ctx.reordering && ctx.dragHoverIndex === ctx.index);
				addLoc(div7, file$b, 12, 10, 367);
			},

			m: function mount(target, anchor) {
				insert(target, div7, anchor);
				append(div7, div6);
				append(div6, div0);
				append(div0, i0);
				append(div0, text0);
				append(div0, i1);
				append(div6, text1);
				append(div6, div1);
				append(div1, text2);
				append(div6, text3);
				append(div6, div2);
				append(div2, h3);
				append(h3, text4);
				append(h3, text5);
				if (if_block0) if_block0.m(h3, null);
				append(div2, text6);
				append(div2, h4);
				append(h4, text7);
				append(h4, b);
				append(b, text8);
				append(div6, text9);
				append(div6, div5);
				append(div5, div3);
				append(div3, i2);
				append(div5, text10);
				append(div5, div4);
				append(div4, i3);
				append(div7, text11);
				if (if_block1) if_block1.m(div7, null);
			},

			p: function update(changed, _ctx) {
				ctx = _ctx;
				div0._svelte.ctx = ctx;
				if ((changed.items) && text2_value !== (text2_value = ctx.item.type)) {
					setData(text2, text2_value);
				}

				if ((changed.items) && text4_value !== (text4_value = ctx.item.name)) {
					setData(text4, text4_value);
				}

				if (ctx.item.id === null) {
					if (!if_block0) {
						if_block0 = create_if_block_13(component, ctx);
						if_block0.c();
						if_block0.m(h3, null);
					}
				} else if (if_block0) {
					if_block0.d(1);
					if_block0 = null;
				}

				if ((changed.$themeIdMap || changed.items) && text8_value !== (text8_value = (ctx.$themeIdMap[ctx.item.themeId] || {name: ""}).name)) {
					setData(text8, text8_value);
				}

				div3._svelte.ctx = ctx;
				div4._svelte.ctx = ctx;

				if (ctx.item._editing) {
					if (if_block1) {
						if_block1.p(changed, ctx);
					} else {
						if_block1 = create_if_block$7(component, ctx);
						if_block1.c();
						if_block1.m(div7, null);
					}
				} else if (if_block1) {
					if_block1.d(1);
					if_block1 = null;
				}

				div7._svelte.ctx = ctx;
				if ((changed.reordering || changed.dragHoverIndex)) {
					toggleClass(div7, "fpl-hover", ctx.reordering && ctx.dragHoverIndex === ctx.index);
				}
			},

			d: function destroy$$1(detach) {
				if (detach) {
					detachNode(div7);
				}

				removeListener(div0, "mousedown", mousedown_handler);
				if (if_block0) if_block0.d();
				removeListener(div3, "click", click_handler$4);
				removeListener(div4, "click", click_handler_1$2);
				if (if_block1) if_block1.d();
				removeListener(div7, "mouseover", mouseover_handler);
				removeListener(div7, "mouseout", mouseout_handler);
			}
		};
	}

	function FrontPageLayoutEditor(options) {
		this._debugName = '<FrontPageLayoutEditor>';
		if (!options || (!options.target && !options.root)) {
			throw new Error("'target' is a required option");
		}
		if (!options.store) {
			throw new Error("<FrontPageLayoutEditor> references store properties, but no store was provided");
		}

		init(this, options);
		this._state = assign(assign(assign({ Object : Object }, this.store._init(["themeIdMap","locale"])), data$8()), options.data);
		this.store._add(this, ["themeIdMap","locale"]);
		if (!('visible' in this._state)) console.warn("<FrontPageLayoutEditor> was created without expected data property 'visible'");
		if (!('items' in this._state)) console.warn("<FrontPageLayoutEditor> was created without expected data property 'items'");
		if (!('reordering' in this._state)) console.warn("<FrontPageLayoutEditor> was created without expected data property 'reordering'");
		if (!('dragHoverIndex' in this._state)) console.warn("<FrontPageLayoutEditor> was created without expected data property 'dragHoverIndex'");
		if (!('$themeIdMap' in this._state)) console.warn("<FrontPageLayoutEditor> was created without expected data property '$themeIdMap'");
		if (!('courses' in this._state)) console.warn("<FrontPageLayoutEditor> was created without expected data property 'courses'");
		if (!('themes' in this._state)) console.warn("<FrontPageLayoutEditor> was created without expected data property 'themes'");

		if (!('$locale' in this._state)) console.warn("<FrontPageLayoutEditor> was created without expected data property '$locale'");
		this._intro = true;

		this._handlers.destroy = [removeFromStore];

		this._fragment = create_main_fragment$b(this, this._state);

		this.root._oncreate.push(() => {
			oncreate$4.call(this);
			this.fire("update", { changed: assignTrue({}, this._state), current: this._state });
		});

		if (options.target) {
			if (options.hydrate) throw new Error("options.hydrate only works if the component was compiled with the `hydratable: true` option");
			this._fragment.c();
			this._mount(options.target, options.anchor);

			flush(this);
		}
	}

	assign(FrontPageLayoutEditor.prototype, protoDev);
	assign(FrontPageLayoutEditor.prototype, methods$8);

	FrontPageLayoutEditor.prototype._checkReadOnly = function _checkReadOnly(newState) {
	};

	/* src/app/InfoData.html generated by Svelte v2.15.3 */



	function bookList({ books }) {
	  return books.map(b => [b.id, b.title]);
	}

	function data$9() {
	  return {
	    dataType: "levels",
	    books: [],
	    data: [
	      // {
	      //   id: null,
	      //   title: "",
	      //   desc: "",
	      // },
	    ],
	  }
	}
	var methods$9 = {
	  addEntry() {
	    const { data, dataType } = this.get();
	    const { locale } = this.store.get();

	    const entry = {
	      id: null,
	      title: locale.ltTitleDefault,
	      desc: locale.ltDescriptionDefault,
	    };

	    if (dataType === "levels") {
	      // @ts-ignore
	      entry.books = [];
	    }
	    data.push(entry);

	    this.set({
	      data,
	    });
	  },

	  removeEntry(index) {
	    const { data } = this.get();
	    data.splice(index, 1);

	    this.set({
	      data,
	    });
	  },

	  addBook(index) {
	    const { books, data } = this.get();
	    const entry = data[index];
	    entry.books.push(books[0].id);

	    this.set({
	      data,
	    });
	  },

	  removeBook(index, book) {
	    const { books, data } = this.get();
	    const entry = data[index];
	    const bookIndex = entry.books.indexOf(book);

	    entry.books.splice(bookIndex, 1);
	    this.set({
	      data,
	    });
	  },

	  openBook(id) {
	    this.store.fire("openbook", {
	      bookId: id,
	    });
	  },
	};

	function oncreate$5() {
	  const { books } = this.store.get();
	  this.set({
	    books,
	  });
	}
	const file$c = "src/app/InfoData.html";

	function click_handler_3(event) {
		const { component, ctx } = this._svelte;

		component.removeEntry(ctx.index);
	}

	function click_handler_2$2(event) {
		const { component, ctx } = this._svelte;

		component.addBook(ctx.index);
	}

	function click_handler_1$3(event) {
		const { component, ctx } = this._svelte;

		component.openBook(ctx.book);
	}

	function click_handler$5(event) {
		const { component, ctx } = this._svelte;

		component.removeBook(ctx.index, ctx.book);
	}

	function get_each_context_1$4(ctx, list, i) {
		const child_ctx = Object.create(ctx);
		child_ctx.book = list[i];
		child_ctx.each_value_1 = list;
		child_ctx.book_index = i;
		return child_ctx;
	}

	function get_each_context$6(ctx, list, i) {
		const child_ctx = Object.create(ctx);
		child_ctx.entry = list[i];
		child_ctx.each_value = list;
		child_ctx.index = i;
		return child_ctx;
	}

	function create_main_fragment$c(component, ctx) {
		var div, text0, button, span0, i, text1, span1, text2_value = ctx.$locale.ltAddEntryButton, text2;

		var each_value = ctx.data;

		var each_blocks = [];

		for (var i_1 = 0; i_1 < each_value.length; i_1 += 1) {
			each_blocks[i_1] = create_each_block$6(component, get_each_context$6(ctx, each_value, i_1));
		}

		function click_handler_4(event) {
			component.addEntry();
		}

		return {
			c: function create() {
				div = createElement("div");

				for (var i_1 = 0; i_1 < each_blocks.length; i_1 += 1) {
					each_blocks[i_1].c();
				}

				text0 = createText("\n\n");
				button = createElement("button");
				span0 = createElement("span");
				i = createElement("i");
				text1 = createText("\n  ");
				span1 = createElement("span");
				text2 = createText(text2_value);
				div.className = "info-data";
				addLoc(div, file$c, 0, 0, 0);
				i.className = "fa fa-plus";
				addLoc(i, file$c, 54, 4, 1709);
				span0.className = "icon";
				addLoc(span0, file$c, 53, 2, 1685);
				addLoc(span1, file$c, 56, 2, 1748);
				addListener(button, "click", click_handler_4);
				button.className = "button is-fullwidth is-medium is-link is-outlined";
				button.type = "button";
				addLoc(button, file$c, 49, 0, 1574);
			},

			m: function mount(target, anchor) {
				insert(target, div, anchor);

				for (var i_1 = 0; i_1 < each_blocks.length; i_1 += 1) {
					each_blocks[i_1].m(div, null);
				}

				insert(target, text0, anchor);
				insert(target, button, anchor);
				append(button, span0);
				append(span0, i);
				append(button, text1);
				append(button, span1);
				append(span1, text2);
			},

			p: function update(changed, ctx) {
				if (changed.$locale || changed.dataType || changed.data || changed.bookList) {
					each_value = ctx.data;

					for (var i_1 = 0; i_1 < each_value.length; i_1 += 1) {
						const child_ctx = get_each_context$6(ctx, each_value, i_1);

						if (each_blocks[i_1]) {
							each_blocks[i_1].p(changed, child_ctx);
						} else {
							each_blocks[i_1] = create_each_block$6(component, child_ctx);
							each_blocks[i_1].c();
							each_blocks[i_1].m(div, null);
						}
					}

					for (; i_1 < each_blocks.length; i_1 += 1) {
						each_blocks[i_1].d(1);
					}
					each_blocks.length = each_value.length;
				}

				if ((changed.$locale) && text2_value !== (text2_value = ctx.$locale.ltAddEntryButton)) {
					setData(text2, text2_value);
				}
			},

			d: function destroy$$1(detach) {
				if (detach) {
					detachNode(div);
				}

				destroyEach(each_blocks, detach);

				if (detach) {
					detachNode(text0);
					detachNode(button);
				}

				removeListener(button, "click", click_handler_4);
			}
		};
	}

	// (8:10) {#if dataType === "levels"}
	function create_if_block$8(component, ctx) {
		var text0, div, button, text1_value = ctx.$locale.ltAddBookButton, text1;

		var each_value_1 = ctx.entry.books;

		var each_blocks = [];

		for (var i = 0; i < each_value_1.length; i += 1) {
			each_blocks[i] = create_each_block_1$4(component, get_each_context_1$4(ctx, each_value_1, i));
		}

		return {
			c: function create() {
				for (var i = 0; i < each_blocks.length; i += 1) {
					each_blocks[i].c();
				}

				text0 = createText("\n\n            ");
				div = createElement("div");
				button = createElement("button");
				text1 = createText(text1_value);
				button._svelte = { component, ctx };

				addListener(button, "click", click_handler_2$2);
				button.className = "button is-link";
				addLoc(button, file$c, 27, 14, 1056);
				div.className = "level-item";
				addLoc(div, file$c, 26, 12, 1017);
			},

			m: function mount(target, anchor) {
				for (var i = 0; i < each_blocks.length; i += 1) {
					each_blocks[i].m(target, anchor);
				}

				insert(target, text0, anchor);
				insert(target, div, anchor);
				append(div, button);
				append(button, text1);
			},

			p: function update(changed, _ctx) {
				ctx = _ctx;
				if (changed.data || changed.bookList) {
					each_value_1 = ctx.entry.books;

					for (var i = 0; i < each_value_1.length; i += 1) {
						const child_ctx = get_each_context_1$4(ctx, each_value_1, i);

						if (each_blocks[i]) {
							each_blocks[i].p(changed, child_ctx);
						} else {
							each_blocks[i] = create_each_block_1$4(component, child_ctx);
							each_blocks[i].c();
							each_blocks[i].m(text0.parentNode, text0);
						}
					}

					for (; i < each_blocks.length; i += 1) {
						each_blocks[i].d(1);
					}
					each_blocks.length = each_value_1.length;
				}

				if ((changed.$locale) && text1_value !== (text1_value = ctx.$locale.ltAddBookButton)) {
					setData(text1, text1_value);
				}

				button._svelte.ctx = ctx;
			},

			d: function destroy$$1(detach) {
				destroyEach(each_blocks, detach);

				if (detach) {
					detachNode(text0);
					detachNode(div);
				}

				removeListener(button, "click", click_handler_2$2);
			}
		};
	}

	// (9:12) {#each entry.books as book}
	function create_each_block_1$4(component, ctx) {
		var div, dropdown_updating = {}, text0, button0, i0, text1, button1, i1;

		var dropdown_initial_data = { list: ctx.bookList };
		if (ctx.book !== void 0) {
			dropdown_initial_data.value = ctx.book;
			dropdown_updating.value = true;
		}
		var dropdown = new Dropdown({
			root: component.root,
			store: component.store,
			data: dropdown_initial_data,
			_bind(changed, childState) {
				var newState = {};
				if (!dropdown_updating.value && changed.value) {
					ctx.each_value_1[ctx.book_index] = childState.value = childState.value;

					newState.data = ctx.data;
				}
				component._set(newState);
				dropdown_updating = {};
			}
		});

		component.root._beforecreate.push(() => {
			dropdown._bind({ value: 1 }, dropdown.get());
		});

		return {
			c: function create() {
				div = createElement("div");
				dropdown._fragment.c();
				text0 = createText("\n                ");
				button0 = createElement("button");
				i0 = createElement("i");
				text1 = createText("\n                ");
				button1 = createElement("button");
				i1 = createElement("i");
				i0.className = "fa fa-times";
				addLoc(i0, file$c, 16, 18, 710);

				button0._svelte = { component, ctx };

				addListener(button0, "click", click_handler$5);
				button0.className = "button preview-button ihf-del-btn svelte-4ytzl0";
				addLoc(button0, file$c, 13, 16, 570);
				i1.className = "fa fa-book";
				addLoc(i1, file$c, 21, 18, 910);

				button1._svelte = { component, ctx };

				addListener(button1, "click", click_handler_1$3);
				button1.className = "button preview-button is-primary svelte-4ytzl0";
				addLoc(button1, file$c, 18, 16, 780);
				div.className = "level-item book-item svelte-4ytzl0";
				addLoc(div, file$c, 9, 14, 422);
			},

			m: function mount(target, anchor) {
				insert(target, div, anchor);
				dropdown._mount(div, null);
				append(div, text0);
				append(div, button0);
				append(button0, i0);
				append(div, text1);
				append(div, button1);
				append(button1, i1);
			},

			p: function update(changed, _ctx) {
				ctx = _ctx;
				var dropdown_changes = {};
				if (changed.bookList) dropdown_changes.list = ctx.bookList;
				if (!dropdown_updating.value && changed.data) {
					dropdown_changes.value = ctx.book;
					dropdown_updating.value = ctx.book !== void 0;
				}
				dropdown._set(dropdown_changes);
				dropdown_updating = {};

				button0._svelte.ctx = ctx;
				button1._svelte.ctx = ctx;
			},

			d: function destroy$$1(detach) {
				if (detach) {
					detachNode(div);
				}

				dropdown.destroy();
				removeListener(button0, "click", click_handler$5);
				removeListener(button1, "click", click_handler_1$3);
			}
		};
	}

	// (2:2) {#each data as entry, index}
	function create_each_block$6(component, ctx) {
		var div4, editabletextfield_updating = {}, text0, editabletextareafield_updating = {}, text1, div3, div0, text2, div2, div1, button, text3_value = ctx.$locale.courseActionDelete, text3;

		var editabletextfield_initial_data = { label: ctx.$locale.ltTitleLabel };
		if (ctx.entry.title !== void 0) {
			editabletextfield_initial_data.value = ctx.entry.title;
			editabletextfield_updating.value = true;
		}
		var editabletextfield = new EditableTextField({
			root: component.root,
			store: component.store,
			data: editabletextfield_initial_data,
			_bind(changed, childState) {
				var newState = {};
				if (!editabletextfield_updating.value && changed.value) {
					ctx.entry.title = childState.value;

					newState.data = ctx.data;
				}
				component._set(newState);
				editabletextfield_updating = {};
			}
		});

		component.root._beforecreate.push(() => {
			editabletextfield._bind({ value: 1 }, editabletextfield.get());
		});

		var editabletextareafield_initial_data = { label: ctx.$locale.ltDescriptionLabel };
		if (ctx.entry.desc !== void 0) {
			editabletextareafield_initial_data.value = ctx.entry.desc;
			editabletextareafield_updating.value = true;
		}
		var editabletextareafield = new EditableTextAreaField({
			root: component.root,
			store: component.store,
			data: editabletextareafield_initial_data,
			_bind(changed, childState) {
				var newState = {};
				if (!editabletextareafield_updating.value && changed.value) {
					ctx.entry.desc = childState.value;

					newState.data = ctx.data;
				}
				component._set(newState);
				editabletextareafield_updating = {};
			}
		});

		component.root._beforecreate.push(() => {
			editabletextareafield._bind({ value: 1 }, editabletextareafield.get());
		});

		var if_block = (ctx.dataType === "levels") && create_if_block$8(component, ctx);

		return {
			c: function create() {
				div4 = createElement("div");
				editabletextfield._fragment.c();
				text0 = createText("\n      ");
				editabletextareafield._fragment.c();
				text1 = createText("\n      ");
				div3 = createElement("div");
				div0 = createElement("div");
				if (if_block) if_block.c();
				text2 = createText("\n        ");
				div2 = createElement("div");
				div1 = createElement("div");
				button = createElement("button");
				text3 = createText(text3_value);
				div0.className = "level-left book-buttons svelte-4ytzl0";
				addLoc(div0, file$c, 6, 8, 292);

				button._svelte = { component, ctx };

				addListener(button, "click", click_handler_3);
				button.className = "button is-danger";
				addLoc(button, file$c, 37, 12, 1343);
				div1.className = "level-item";
				addLoc(div1, file$c, 36, 10, 1306);
				div2.className = "level-right";
				addLoc(div2, file$c, 35, 8, 1270);
				div3.className = "level";
				addLoc(div3, file$c, 5, 6, 264);
				div4.className = "info-data-entry svelte-4ytzl0";
				addLoc(div4, file$c, 2, 4, 59);
			},

			m: function mount(target, anchor) {
				insert(target, div4, anchor);
				editabletextfield._mount(div4, null);
				append(div4, text0);
				editabletextareafield._mount(div4, null);
				append(div4, text1);
				append(div4, div3);
				append(div3, div0);
				if (if_block) if_block.m(div0, null);
				append(div3, text2);
				append(div3, div2);
				append(div2, div1);
				append(div1, button);
				append(button, text3);
			},

			p: function update(changed, _ctx) {
				ctx = _ctx;
				var editabletextfield_changes = {};
				if (changed.$locale) editabletextfield_changes.label = ctx.$locale.ltTitleLabel;
				if (!editabletextfield_updating.value && changed.data) {
					editabletextfield_changes.value = ctx.entry.title;
					editabletextfield_updating.value = ctx.entry.title !== void 0;
				}
				editabletextfield._set(editabletextfield_changes);
				editabletextfield_updating = {};

				var editabletextareafield_changes = {};
				if (changed.$locale) editabletextareafield_changes.label = ctx.$locale.ltDescriptionLabel;
				if (!editabletextareafield_updating.value && changed.data) {
					editabletextareafield_changes.value = ctx.entry.desc;
					editabletextareafield_updating.value = ctx.entry.desc !== void 0;
				}
				editabletextareafield._set(editabletextareafield_changes);
				editabletextareafield_updating = {};

				if (ctx.dataType === "levels") {
					if (if_block) {
						if_block.p(changed, ctx);
					} else {
						if_block = create_if_block$8(component, ctx);
						if_block.c();
						if_block.m(div0, null);
					}
				} else if (if_block) {
					if_block.d(1);
					if_block = null;
				}

				if ((changed.$locale) && text3_value !== (text3_value = ctx.$locale.courseActionDelete)) {
					setData(text3, text3_value);
				}

				button._svelte.ctx = ctx;
			},

			d: function destroy$$1(detach) {
				if (detach) {
					detachNode(div4);
				}

				editabletextfield.destroy();
				editabletextareafield.destroy();
				if (if_block) if_block.d();
				removeListener(button, "click", click_handler_3);
			}
		};
	}

	function InfoData(options) {
		this._debugName = '<InfoData>';
		if (!options || (!options.target && !options.root)) {
			throw new Error("'target' is a required option");
		}
		if (!options.store) {
			throw new Error("<InfoData> references store properties, but no store was provided");
		}

		init(this, options);
		this._state = assign(assign(this.store._init(["locale"]), data$9()), options.data);
		this.store._add(this, ["locale"]);

		this._recompute({ books: 1 }, this._state);
		if (!('books' in this._state)) console.warn("<InfoData> was created without expected data property 'books'");
		if (!('data' in this._state)) console.warn("<InfoData> was created without expected data property 'data'");
		if (!('$locale' in this._state)) console.warn("<InfoData> was created without expected data property '$locale'");
		if (!('dataType' in this._state)) console.warn("<InfoData> was created without expected data property 'dataType'");
		this._intro = true;

		this._handlers.destroy = [removeFromStore];

		this._fragment = create_main_fragment$c(this, this._state);

		this.root._oncreate.push(() => {
			oncreate$5.call(this);
			this.fire("update", { changed: assignTrue({}, this._state), current: this._state });
		});

		if (options.target) {
			if (options.hydrate) throw new Error("options.hydrate only works if the component was compiled with the `hydratable: true` option");
			this._fragment.c();
			this._mount(options.target, options.anchor);

			flush(this);
		}
	}

	assign(InfoData.prototype, protoDev);
	assign(InfoData.prototype, methods$9);

	InfoData.prototype._checkReadOnly = function _checkReadOnly(newState) {
		if ('bookList' in newState && !this._updatingReadonlyProperty) throw new Error("<InfoData>: Cannot set read-only property 'bookList'");
	};

	InfoData.prototype._recompute = function _recompute(changed, state) {
		if (changed.books) {
			if (this._differs(state.bookList, (state.bookList = bookList(state)))) changed.bookList = true;
		}
	};

	/* src/app/ThemeEditor.html generated by Svelte v2.15.3 */

	function themeVariables({ theme }) {
		return theme ? Object.entries(theme.variables) : [];
	}

	function data$a() {
	  return {
	    visible: false,
	    theme: null, // themeData
	    themeName: "",
	    curThemeMapping: {},
	    fields: [],
	    variables: {},
	    canvasReady: false,
	    canvasMarkup: "",
	    requestToken: 0,
	    iframeRegistry: {},
	  };
	}
	function capitilize(string) {
	  return string.slice(0, 1).toUpperCase() + string.slice(1);
	}
	var methods$a = {
	  selectTheme(id) {
	    const { themeIdMap } = this.store.get();
	    const { theme } = this.get();

	    let newTheme = themeIdMap[id];

	    if (theme) {
	      if (theme.id === id) {
	        // Toggle behaviour- i.e. no theme selected
	        newTheme = null;
	      }
	    }

	    this.set({
	      theme: newTheme,
	    });

	    // FIXME: Make message passing between components more consistent
	    // Let other components know about selected theme
	    this.store.fire("themeupdated", {
	      theme: newTheme,
	    });
	  },

	  updateCanvas() {
	    const { theme, curThemeMapping, canvasMarkup, fields } = this.get();

	    if (theme) {
	      this.messageCanvas({
	        action: "settheme",
	        markup: canvasMarkup,
	        fields,
	        themeMap: curThemeMapping,
	      });
	    } else {
	      this.messageCanvas({
	        action: "removetheme",
	      });
	    }
	  },

	  messageCanvas(msg) {
	    const canvas = this.refs.themeCanvas;

	    if (canvas) {
	      canvas.contentWindow.postMessage(msg, "*");
	    }
	  },

	  setThemeVariable(name, type, value) {
	    const { currentThemeVariables } = this.store.get();

	    currentThemeVariables[name] = value;
	    this.store.set({
	      currentThemeVariables,
	    });
	    this.store.fire("themevariableupdated");
	  },
	};

	function oncreate$6() {
	  const { locale, themes, themeCanvasResources } = this.store.get();

	  // Load theme markup from the cache or database (if required)
	  const loadTheme = (id, token = null) => {
	    if (token === null) {
	      let { requestToken } = this.get();
	      this.set({
	        requestToken: ++requestToken,
	      });

	      token = requestToken;
	    }

	    this.store.fire("loadtheme", {
	      token,
	      id,
	    });
	  };

	  // Show theme name, or a "unselected" message when there's no theme selected
	  // and update the canvas to reflect the newly selected theme
	  this.on("state", ({ changed, current }) => {
	    if (changed.theme) {
	      let { theme, requestToken } = current;

	      this.set({
	        requestToken: ++requestToken,
	        themeName: theme ? theme.name : locale.teNoThemeSelectedThemeNameHeader,
	      });

	      if (theme) {
	        const token = requestToken;
	        loadTheme(theme.id, token);
	      } else {
	        this.updateCanvas();
	      }
	    }
	  });

	  // Show theme editor populated with new data when "setthemeeditor" event is triggered
	  const listener = this.store.on("setthemeeditor", ({ visible, fields, variables, themeData, themeMap }) => {
	    if (typeof visible === "boolean") {
	      this.set({
	        visible,
	        fields,
	        variables,
	        theme: themeData || null,
	        curThemeMapping: themeMap,
	      });
	    }
	  });

	  // Render theme thumbnails when they're receieved from the server
	  const listener2 = this.store.on("thumbsloaded", ({ thumbs }) => {
	    const { iframeRegistry } = this.get();
	    const wrapId = "--ihfath-theme--wrap";
	    const baseWidth = 720; // FIXME: Too biased?

	    // Injected function that adjusts the psuedo-DPI (zoomed out look) via CSS transforms
	    function resizeContent() {
	      setTimeout(() => {
	        const div = document.querySelector("#" + wrapId);
	        const scale = innerWidth / baseWidth;

	        div.id = "";
	        div["style"].transform = `scale(${ scale }, ${ scale })`;
	        div["style"].top = ((innerHeight - (div["offsetHeight"] * scale)) / 2) + `px`;
	      }, 1);
	    }

	    // Prepare and inject new HTML markup into thumbnail iframes
	    for (const [theme, iframe] of Object.entries(iframeRegistry)) {
	      const markup = `<div
                        id="${ wrapId }"
                        class="${ wrapId }"
                        style="width: ${ baseWidth }px;">
                        ${ thumbs[theme] }
                      </div>

                      <style>
                        html, body {
                          padding: 0;
                          margin: 0;
                        }

                        body {
                          width: 100vh;
                          height: 100vh;
                          overflow: hidden;

                          /* color: #fff; */
                        }

                        #${ wrapId } {
                          opacity: 0;
                        }

                        .${ wrapId } {
                          position: absolute;
                          left: 0;
                          top: 0;
                          transform-origin: 0 0 !important;
                        }
                      </style>

                      <script>
                        const wrapId = "${ wrapId }";
                        const baseWidth = "${ baseWidth }";
                        ${ resizeContent.toString() };
                        resizeContent();
                      <\/script>`;

	      // Set base markup
	      iframe.srcdoc = markup;
	    }
	  });

	  // Render theme markup from chosen theme on canvas when loaded from server/cache
	  const listener3 = this.store.on("themeloaded", ({ id, markup, token }) => {
	    const { requestToken, theme } = this.get();

	    if (theme && theme.id === id && token === requestToken) {
	      this.set({
	        canvasMarkup: markup,
	      });

	      // Paint theme canvas
	      this.updateCanvas();
	    }
	  });

	  // Prepare theme canvas
	  {
	    const baseWidth = 1366;
	    const canvas = this.refs.themeCanvas;
	    const urlPrefix = `${ location.protocol }//${ location.hostname }`;
	    const srcdoc = `
      <!DOCTYPE html>
      <html>
        <head>
          <script>
            const _ihfathLocale = ${ JSON.stringify(locale) };
            const _ihfathBasewidth = ${ baseWidth };
          <\/script>
          <script src="${ urlPrefix + themeCanvasResources.script }"><\/script>
          <link  href="${ urlPrefix + themeCanvasResources.style }" rel="stylesheet"/>
          <link  href="${ urlPrefix + themeCanvasResources.fastyle }" rel="stylesheet"/>
        </head>
        <body></body>
      </html>
    `;

	    // Respond to messages from theme canvas
	    window.addEventListener("message", (evt) => {
	      const { data: { action, ...data } } = evt;

	      if (action === "canvasprepared") {
	        const { theme } = this.get();
	        this.set({
	          canvasReady: true,
	        });

	        if (theme) {
	          loadTheme(theme.id);
	        }
	      } else if (action === "update") {
	        this.store.set({
	          currentThemeMap: data.themeMap,
	        });
	        this.store.fire("thememappingupdated");
	      }
	    });
	    canvas.srcdoc = srcdoc;
	  }

	  this.on("destroy", () => {
	    listener.cancel();
	    listener2.cancel();
	    listener3.cancel();
	  });

	  // Set defaults
	  this.set({
	    themeName: locale.teNoThemeSelectedThemeNameHeader,
	  });
	}
	function registerFrame(iframe, { id, registry }) {
	  registry[id] = iframe;
	}
	const file$d = "src/app/ThemeEditor.html";

	function click_handler$6(event) {
		const { component, ctx } = this._svelte;

		component.selectTheme(ctx.theme.id);
	}

	function get_each_context_1$5(ctx, list, i) {
		const child_ctx = Object.create(ctx);
		child_ctx.theme = list[i];
		return child_ctx;
	}

	function get_each_context$7(ctx, list, i) {
		const child_ctx = Object.create(ctx);
		child_ctx.tv = list[i];
		return child_ctx;
	}

	function create_main_fragment$d(component, ctx) {
		var div6, div0, text0, div5, div4, h2, text1, text2, div1, iframe, text3, text4, div3, div2;

		function click_handler(event) {
			component.set({visible: false});
		}

		var if_block = (0) && create_if_block$9(component, ctx);

		var each_value_1 = ctx.$themes;

		var each_blocks = [];

		for (var i = 0; i < each_value_1.length; i += 1) {
			each_blocks[i] = create_each_block$7(component, get_each_context_1$5(ctx, each_value_1, i));
		}

		return {
			c: function create() {
				div6 = createElement("div");
				div0 = createElement("div");
				text0 = createText("\n  ");
				div5 = createElement("div");
				div4 = createElement("div");
				h2 = createElement("h2");
				text1 = createText(ctx.themeName);
				text2 = createText("\n\n      ");
				div1 = createElement("div");
				iframe = createElement("iframe");
				text3 = createText("\n\n      \n      ");
				if (if_block) if_block.c();
				text4 = createText("\n\n      ");
				div3 = createElement("div");
				div2 = createElement("div");

				for (var i = 0; i < each_blocks.length; i += 1) {
					each_blocks[i].c();
				}
				addListener(div0, "click", click_handler);
				div0.className = "modal-background svelte-l7aofb";
				addLoc(div0, file$d, 4, 2, 71);
				h2.className = "title is-4";
				addLoc(h2, file$d, 10, 6, 230);
				setAttribute(iframe, "frameborder", "0");
				iframe.className = "svelte-l7aofb";
				addLoc(iframe, file$d, 15, 8, 355);
				div1.className = "theme-canvas svelte-l7aofb";
				toggleClass(div1, "empty-canvas", ctx.theme);
				addLoc(div1, file$d, 12, 6, 277);
				div2.className = "theme-thumb-wrap svelte-l7aofb";
				addLoc(div2, file$d, 39, 8, 1185);
				div3.className = "theme-chooser svelte-l7aofb";
				addLoc(div3, file$d, 38, 6, 1149);
				div4.className = "box theme-editor-window svelte-l7aofb";
				addLoc(div4, file$d, 9, 4, 186);
				div5.className = "modal-content svelte-l7aofb";
				addLoc(div5, file$d, 8, 2, 154);
				div6.className = "modal svelte-l7aofb";
				toggleClass(div6, "is-active", ctx.visible);
				addLoc(div6, file$d, 1, 0, 21);
			},

			m: function mount(target, anchor) {
				insert(target, div6, anchor);
				append(div6, div0);
				append(div6, text0);
				append(div6, div5);
				append(div5, div4);
				append(div4, h2);
				append(h2, text1);
				append(div4, text2);
				append(div4, div1);
				append(div1, iframe);
				component.refs.themeCanvas = iframe;
				append(div4, text3);
				if (if_block) if_block.m(div4, null);
				append(div4, text4);
				append(div4, div3);
				append(div3, div2);

				for (var i = 0; i < each_blocks.length; i += 1) {
					each_blocks[i].m(div2, null);
				}
			},

			p: function update(changed, ctx) {
				if (changed.themeName) {
					setData(text1, ctx.themeName);
				}

				if (changed.theme) {
					toggleClass(div1, "empty-canvas", ctx.theme);
				}

				if (if_block) {
					if_block.d(1);
					if_block = null;
				}

				if (changed.$themes || changed.iframeRegistry) {
					each_value_1 = ctx.$themes;

					for (var i = 0; i < each_value_1.length; i += 1) {
						const child_ctx = get_each_context_1$5(ctx, each_value_1, i);

						if (each_blocks[i]) {
							each_blocks[i].p(changed, child_ctx);
						} else {
							each_blocks[i] = create_each_block$7(component, child_ctx);
							each_blocks[i].c();
							each_blocks[i].m(div2, null);
						}
					}

					for (; i < each_blocks.length; i += 1) {
						each_blocks[i].d(1);
					}
					each_blocks.length = each_value_1.length;
				}

				if (changed.visible) {
					toggleClass(div6, "is-active", ctx.visible);
				}
			},

			d: function destroy$$1(detach) {
				if (detach) {
					detachNode(div6);
				}

				removeListener(div0, "click", click_handler);
				if (component.refs.themeCanvas === iframe) component.refs.themeCanvas = null;
				if (if_block) if_block.d();

				destroyEach(each_blocks, detach);
			}
		};
	}

	// (24:6) {#if 0}
	function create_if_block$9(component, ctx) {
		var div;

		var each_value = ctx.themeVariables;

		var each_blocks = [];

		for (var i = 0; i < each_value.length; i += 1) {
			each_blocks[i] = create_each_block_1$5(component, get_each_context$7(ctx, each_value, i));
		}

		return {
			c: function create() {
				div = createElement("div");

				for (var i = 0; i < each_blocks.length; i += 1) {
					each_blocks[i].c();
				}
				div.className = "variable-editor svelte-l7aofb";
				addLoc(div, file$d, 24, 8, 548);
			},

			m: function mount(target, anchor) {
				insert(target, div, anchor);

				for (var i = 0; i < each_blocks.length; i += 1) {
					each_blocks[i].m(div, null);
				}
			},

			p: function update(changed, ctx) {
				if (changed.themeVariables || changed.$locale || changed.fields || changed.variables) {
					each_value = ctx.themeVariables;

					for (var i = 0; i < each_value.length; i += 1) {
						const child_ctx = get_each_context$7(ctx, each_value, i);

						if (each_blocks[i]) {
							each_blocks[i].p(changed, child_ctx);
						} else {
							each_blocks[i] = create_each_block_1$5(component, child_ctx);
							each_blocks[i].c();
							each_blocks[i].m(div, null);
						}
					}

					for (; i < each_blocks.length; i += 1) {
						each_blocks[i].d(1);
					}
					each_blocks.length = each_value.length;
				}
			},

			d: function destroy$$1(detach) {
				if (detach) {
					detachNode(div);
				}

				destroyEach(each_blocks, detach);
			}
		};
	}

	// (27:12) {#if tv[1][0] === "field"}
	function create_if_block_1$6(component, ctx) {
		var span, text0_value = capitilize(ctx.tv[0]), text0, text1;

		var dropdown_initial_data = { list: [[-1, ctx.$locale.teVariablesNoFieldSelected]].concat(ctx.fields.map((f, i) => [i, f.name])), value: ctx.variables && ctx.tv[0] in ctx.variables ? ctx.variables[ctx.tv[0]] : -1 };
		var dropdown = new Dropdown({
			root: component.root,
			store: component.store,
			data: dropdown_initial_data
		});

		dropdown.on("change", function(event) {
			component.setThemeVariable(ctx.tv[0], ctx.tv[1][0], event.value);
		});

		return {
			c: function create() {
				span = createElement("span");
				text0 = createText(text0_value);
				text1 = createText("\n              \n              ");
				dropdown._fragment.c();
				span.className = "svelte-l7aofb";
				addLoc(span, file$d, 27, 14, 670);
			},

			m: function mount(target, anchor) {
				insert(target, span, anchor);
				append(span, text0);
				insert(target, text1, anchor);
				dropdown._mount(target, anchor);
			},

			p: function update(changed, _ctx) {
				ctx = _ctx;
				if ((changed.themeVariables) && text0_value !== (text0_value = capitilize(ctx.tv[0]))) {
					setData(text0, text0_value);
				}

				var dropdown_changes = {};
				if (changed.$locale || changed.fields) dropdown_changes.list = [[-1, ctx.$locale.teVariablesNoFieldSelected]].concat(ctx.fields.map((f, i) => [i, f.name]));
				if (changed.variables || changed.themeVariables) dropdown_changes.value = ctx.variables && ctx.tv[0] in ctx.variables ? ctx.variables[ctx.tv[0]] : -1;
				dropdown._set(dropdown_changes);
			},

			d: function destroy$$1(detach) {
				if (detach) {
					detachNode(span);
					detachNode(text1);
				}

				dropdown.destroy(detach);
			}
		};
	}

	// (26:10) {#each themeVariables as tv}
	function create_each_block_1$5(component, ctx) {
		var if_block_anchor;

		var if_block = (ctx.tv[1][0] === "field") && create_if_block_1$6(component, ctx);

		return {
			c: function create() {
				if (if_block) if_block.c();
				if_block_anchor = createComment();
			},

			m: function mount(target, anchor) {
				if (if_block) if_block.m(target, anchor);
				insert(target, if_block_anchor, anchor);
			},

			p: function update(changed, ctx) {
				if (ctx.tv[1][0] === "field") {
					if (if_block) {
						if_block.p(changed, ctx);
					} else {
						if_block = create_if_block_1$6(component, ctx);
						if_block.c();
						if_block.m(if_block_anchor.parentNode, if_block_anchor);
					}
				} else if (if_block) {
					if_block.d(1);
					if_block = null;
				}
			},

			d: function destroy$$1(detach) {
				if (if_block) if_block.d(detach);
				if (detach) {
					detachNode(if_block_anchor);
				}
			}
		};
	}

	// (47:10) {#each $themes as theme}
	function create_each_block$7(component, ctx) {
		var div1, iframe, iframe_data_theme_value, registerFrame_action, text0, h2, div0, text1_value = ctx.theme.name, text1, text2;

		return {
			c: function create() {
				div1 = createElement("div");
				iframe = createElement("iframe");
				text0 = createText("\n              ");
				h2 = createElement("h2");
				div0 = createElement("div");
				text1 = createText(text1_value);
				text2 = createText("\n            ");
				setAttribute(iframe, "frameborder", "0");
				iframe.dataset.theme = iframe_data_theme_value = ctx.theme.id;
				iframe.className = "svelte-l7aofb";
				addLoc(iframe, file$d, 50, 14, 1628);
				div0.className = "svelte-l7aofb";
				addLoc(div0, file$d, 56, 16, 1885);
				h2.className = "has-text-centered has-text-grey svelte-l7aofb";
				addLoc(h2, file$d, 55, 14, 1824);

				div1._svelte = { component, ctx };

				addListener(div1, "click", click_handler$6);
				div1.className = "theme-thumb svelte-l7aofb";
				addLoc(div1, file$d, 47, 12, 1527);
			},

			m: function mount(target, anchor) {
				insert(target, div1, anchor);
				append(div1, iframe);
				registerFrame_action = registerFrame.call(component, iframe, {id: ctx.theme.id, registry: ctx.iframeRegistry}) || {};
				append(div1, text0);
				append(div1, h2);
				append(h2, div0);
				append(div0, text1);
				append(div1, text2);
			},

			p: function update(changed, _ctx) {
				ctx = _ctx;
				if ((changed.$themes) && iframe_data_theme_value !== (iframe_data_theme_value = ctx.theme.id)) {
					iframe.dataset.theme = iframe_data_theme_value;
				}

				if (typeof registerFrame_action.update === 'function' && (changed.$themes || changed.iframeRegistry)) {
					registerFrame_action.update.call(component, {id: ctx.theme.id, registry: ctx.iframeRegistry});
				}

				if ((changed.$themes) && text1_value !== (text1_value = ctx.theme.name)) {
					setData(text1, text1_value);
				}

				div1._svelte.ctx = ctx;
			},

			d: function destroy$$1(detach) {
				if (detach) {
					detachNode(div1);
				}

				if (registerFrame_action && typeof registerFrame_action.destroy === 'function') registerFrame_action.destroy.call(component);
				removeListener(div1, "click", click_handler$6);
			}
		};
	}

	function ThemeEditor(options) {
		this._debugName = '<ThemeEditor>';
		if (!options || (!options.target && !options.root)) {
			throw new Error("'target' is a required option");
		}
		if (!options.store) {
			throw new Error("<ThemeEditor> references store properties, but no store was provided");
		}

		init(this, options);
		this.refs = {};
		this._state = assign(assign(this.store._init(["locale","themes"]), data$a()), options.data);
		this.store._add(this, ["locale","themes"]);

		this._recompute({ theme: 1 }, this._state);
		if (!('theme' in this._state)) console.warn("<ThemeEditor> was created without expected data property 'theme'");
		if (!('visible' in this._state)) console.warn("<ThemeEditor> was created without expected data property 'visible'");
		if (!('themeName' in this._state)) console.warn("<ThemeEditor> was created without expected data property 'themeName'");

		if (!('$locale' in this._state)) console.warn("<ThemeEditor> was created without expected data property '$locale'");
		if (!('fields' in this._state)) console.warn("<ThemeEditor> was created without expected data property 'fields'");
		if (!('variables' in this._state)) console.warn("<ThemeEditor> was created without expected data property 'variables'");
		if (!('$themes' in this._state)) console.warn("<ThemeEditor> was created without expected data property '$themes'");
		if (!('iframeRegistry' in this._state)) console.warn("<ThemeEditor> was created without expected data property 'iframeRegistry'");
		this._intro = true;

		this._handlers.destroy = [removeFromStore];

		this._fragment = create_main_fragment$d(this, this._state);

		this.root._oncreate.push(() => {
			oncreate$6.call(this);
			this.fire("update", { changed: assignTrue({}, this._state), current: this._state });
		});

		if (options.target) {
			if (options.hydrate) throw new Error("options.hydrate only works if the component was compiled with the `hydratable: true` option");
			this._fragment.c();
			this._mount(options.target, options.anchor);

			flush(this);
		}
	}

	assign(ThemeEditor.prototype, protoDev);
	assign(ThemeEditor.prototype, methods$a);

	ThemeEditor.prototype._checkReadOnly = function _checkReadOnly(newState) {
		if ('themeVariables' in newState && !this._updatingReadonlyProperty) throw new Error("<ThemeEditor>: Cannot set read-only property 'themeVariables'");
	};

	ThemeEditor.prototype._recompute = function _recompute(changed, state) {
		if (changed.theme) {
			if (this._differs(state.themeVariables, (state.themeVariables = themeVariables(state)))) changed.themeVariables = true;
		}
	};

	/* src/app/Prices.html generated by Svelte v2.15.3 */

	function available({ priceList, value, notAvailableMsg }) {
	  const ids = {};
	  value.forEach(id => (ids[id] = 1));

	  const list = priceList.filter(p => !(p.id in ids)).map(p => [
	    p.id,
	    p.hours + "h — " + (p.price / 100) + p.currency,
	  ]);

	  return list.length ? list : [[-1, notAvailableMsg]];
	}

	function priceIdMap({ priceList }) {
	  const map = {};
	  priceList.forEach(p => (map[p.id] = p));

	  return map;
	}

	function data$b() {
	  return {
	    value: [],
	    selectedPriceId: 1,
	    featuredPrice: -1,
	    priceList: [],
	    notAvailableMsg: "",
	  };
	}
	var methods$b = {
	  removePrice(index) {
	    const { value } = this.get();

	    value.splice(index, 1);
	    this.set({
	      value,
	    });
	  },

	  addPrice() {
	    let { selectedPriceId, value, priceIdMap } = this.get();
	    value.push(selectedPriceId);

	    this.set({
	      value,
	    });

	    // Update selectedPriceId
	    const { available } = this.get();
	    this.set({
	      selectedPriceId: available[0][0],
	    });
	  }
	};

	function oncreate$7() {
	  const { available } = this.get();
	  const { locale } = this.store.get();

	  this.set({
	    selectedPriceId: available[0][0],
	    notAvailableMsg: locale.ptNoAvailableHours,
	  });
	}
	const file$e = "src/app/Prices.html";

	function click_handler_1$4(event) {
		const { component, ctx } = this._svelte;

		component.removePrice(ctx.index);
	}

	function click_handler$7(event) {
		const { component, ctx } = this._svelte;

		component.set({featuredPrice: ctx.priceId});
	}

	function get_each_context$8(ctx, list, i) {
		const child_ctx = Object.create(ctx);
		child_ctx.priceId = list[i];
		child_ctx.index = i;
		return child_ctx;
	}

	function create_main_fragment$e(component, ctx) {
		var div, dropdown_updating = {}, text0, button, text1_value = ctx.$locale.ptAddPriceButton, text1, button_disabled_value, text2, each_anchor;

		var dropdown_initial_data = { list: ctx.available };
		if (ctx.selectedPriceId !== void 0) {
			dropdown_initial_data.value = ctx.selectedPriceId;
			dropdown_updating.value = true;
		}
		var dropdown = new Dropdown({
			root: component.root,
			store: component.store,
			data: dropdown_initial_data,
			_bind(changed, childState) {
				var newState = {};
				if (!dropdown_updating.value && changed.value) {
					newState.selectedPriceId = childState.value;
				}
				component._set(newState);
				dropdown_updating = {};
			}
		});

		component.root._beforecreate.push(() => {
			dropdown._bind({ value: 1 }, dropdown.get());
		});

		function click_handler(event) {
			component.addPrice();
		}

		var each_value = ctx.value;

		var each_blocks = [];

		for (var i = 0; i < each_value.length; i += 1) {
			each_blocks[i] = create_each_block$8(component, get_each_context$8(ctx, each_value, i));
		}

		return {
			c: function create() {
				div = createElement("div");
				dropdown._fragment.c();
				text0 = createText("\n\n  ");
				button = createElement("button");
				text1 = createText(text1_value);
				text2 = createText("\n\n");

				for (var i = 0; i < each_blocks.length; i += 1) {
					each_blocks[i].c();
				}

				each_anchor = createComment();
				addListener(button, "click", click_handler);
				button.className = "button is-link";
				button.disabled = button_disabled_value = ctx.selectedPriceId === -1;
				addLoc(button, file$e, 5, 2, 114);
				div.className = "buttons-top has-text-centered svelte-1ttameg";
				addLoc(div, file$e, 0, 0, 0);
			},

			m: function mount(target, anchor) {
				insert(target, div, anchor);
				dropdown._mount(div, null);
				append(div, text0);
				append(div, button);
				append(button, text1);
				insert(target, text2, anchor);

				for (var i = 0; i < each_blocks.length; i += 1) {
					each_blocks[i].m(target, anchor);
				}

				insert(target, each_anchor, anchor);
			},

			p: function update(changed, _ctx) {
				ctx = _ctx;
				var dropdown_changes = {};
				if (changed.available) dropdown_changes.list = ctx.available;
				if (!dropdown_updating.value && changed.selectedPriceId) {
					dropdown_changes.value = ctx.selectedPriceId;
					dropdown_updating.value = ctx.selectedPriceId !== void 0;
				}
				dropdown._set(dropdown_changes);
				dropdown_updating = {};

				if ((changed.$locale) && text1_value !== (text1_value = ctx.$locale.ptAddPriceButton)) {
					setData(text1, text1_value);
				}

				if ((changed.selectedPriceId) && button_disabled_value !== (button_disabled_value = ctx.selectedPriceId === -1)) {
					button.disabled = button_disabled_value;
				}

				if (changed.$locale || changed.featuredPrice || changed.value || changed.priceIdMap) {
					each_value = ctx.value;

					for (var i = 0; i < each_value.length; i += 1) {
						const child_ctx = get_each_context$8(ctx, each_value, i);

						if (each_blocks[i]) {
							each_blocks[i].p(changed, child_ctx);
						} else {
							each_blocks[i] = create_each_block$8(component, child_ctx);
							each_blocks[i].c();
							each_blocks[i].m(each_anchor.parentNode, each_anchor);
						}
					}

					for (; i < each_blocks.length; i += 1) {
						each_blocks[i].d(1);
					}
					each_blocks.length = each_value.length;
				}
			},

			d: function destroy$$1(detach) {
				if (detach) {
					detachNode(div);
				}

				dropdown.destroy();
				removeListener(button, "click", click_handler);
				if (detach) {
					detachNode(text2);
				}

				destroyEach(each_blocks, detach);

				if (detach) {
					detachNode(each_anchor);
				}
			}
		};
	}

	// (14:0) {#each value as priceId, index}
	function create_each_block$8(component, ctx) {
		var div1, h50, span, text0_value = ctx.priceIdMap[ctx.priceId].hours, text0, text1, text2, h51, text3_value = (ctx.priceIdMap[ctx.priceId].price / 100) + ctx.priceIdMap[ctx.priceId].currency, text3, text4, div0, button0, text5_value = ctx.$locale.ptFeaturedPriceButton + (ctx.featuredPrice == ctx.priceId ? " ✓": ""), text5, text6, button1, text7_value = ctx.$locale.fpeFieldDeleteButton, text7, text8;

		return {
			c: function create() {
				div1 = createElement("div");
				h50 = createElement("h5");
				span = createElement("span");
				text0 = createText(text0_value);
				text1 = createText("h");
				text2 = createText("\n    ");
				h51 = createElement("h5");
				text3 = createText(text3_value);
				text4 = createText("\n\n    ");
				div0 = createElement("div");
				button0 = createElement("button");
				text5 = createText(text5_value);
				text6 = createText("\n      ");
				button1 = createElement("button");
				text7 = createText(text7_value);
				text8 = createText("\n  ");
				span.className = "has-text-link";
				addLoc(span, file$e, 15, 27, 344);
				h50.className = "title is-5 svelte-1ttameg";
				addLoc(h50, file$e, 15, 4, 321);
				h51.className = "title is-5 svelte-1ttameg";
				addLoc(h51, file$e, 16, 4, 417);

				button0._svelte = { component, ctx };

				addListener(button0, "click", click_handler$7);
				button0.className = "button";
				toggleClass(button0, "is-link", ctx.featuredPrice == ctx.priceId);
				addLoc(button0, file$e, 19, 6, 545);

				button1._svelte = { component, ctx };

				addListener(button1, "click", click_handler_1$4);
				button1.className = "button is-danger";
				addLoc(button1, file$e, 25, 6, 777);
				div0.className = "buttons svelte-1ttameg";
				addLoc(div0, file$e, 18, 4, 517);
				div1.className = "box svelte-1ttameg";
				addLoc(div1, file$e, 14, 2, 299);
			},

			m: function mount(target, anchor) {
				insert(target, div1, anchor);
				append(div1, h50);
				append(h50, span);
				append(span, text0);
				append(h50, text1);
				append(div1, text2);
				append(div1, h51);
				append(h51, text3);
				append(div1, text4);
				append(div1, div0);
				append(div0, button0);
				append(button0, text5);
				append(div0, text6);
				append(div0, button1);
				append(button1, text7);
				append(div1, text8);
			},

			p: function update(changed, _ctx) {
				ctx = _ctx;
				if ((changed.priceIdMap || changed.value) && text0_value !== (text0_value = ctx.priceIdMap[ctx.priceId].hours)) {
					setData(text0, text0_value);
				}

				if ((changed.priceIdMap || changed.value) && text3_value !== (text3_value = (ctx.priceIdMap[ctx.priceId].price / 100) + ctx.priceIdMap[ctx.priceId].currency)) {
					setData(text3, text3_value);
				}

				if ((changed.$locale || changed.featuredPrice || changed.value) && text5_value !== (text5_value = ctx.$locale.ptFeaturedPriceButton + (ctx.featuredPrice == ctx.priceId ? " ✓": ""))) {
					setData(text5, text5_value);
				}

				button0._svelte.ctx = ctx;
				if ((changed.featuredPrice || changed.value)) {
					toggleClass(button0, "is-link", ctx.featuredPrice == ctx.priceId);
				}

				if ((changed.$locale) && text7_value !== (text7_value = ctx.$locale.fpeFieldDeleteButton)) {
					setData(text7, text7_value);
				}

				button1._svelte.ctx = ctx;
			},

			d: function destroy$$1(detach) {
				if (detach) {
					detachNode(div1);
				}

				removeListener(button0, "click", click_handler$7);
				removeListener(button1, "click", click_handler_1$4);
			}
		};
	}

	function Prices(options) {
		this._debugName = '<Prices>';
		if (!options || (!options.target && !options.root)) {
			throw new Error("'target' is a required option");
		}
		if (!options.store) {
			throw new Error("<Prices> references store properties, but no store was provided");
		}

		init(this, options);
		this._state = assign(assign(this.store._init(["locale"]), data$b()), options.data);
		this.store._add(this, ["locale"]);

		this._recompute({ priceList: 1, value: 1, notAvailableMsg: 1 }, this._state);
		if (!('priceList' in this._state)) console.warn("<Prices> was created without expected data property 'priceList'");
		if (!('value' in this._state)) console.warn("<Prices> was created without expected data property 'value'");
		if (!('notAvailableMsg' in this._state)) console.warn("<Prices> was created without expected data property 'notAvailableMsg'");

		if (!('selectedPriceId' in this._state)) console.warn("<Prices> was created without expected data property 'selectedPriceId'");
		if (!('$locale' in this._state)) console.warn("<Prices> was created without expected data property '$locale'");

		if (!('featuredPrice' in this._state)) console.warn("<Prices> was created without expected data property 'featuredPrice'");
		this._intro = true;

		this._handlers.destroy = [removeFromStore];

		this._fragment = create_main_fragment$e(this, this._state);

		this.root._oncreate.push(() => {
			oncreate$7.call(this);
			this.fire("update", { changed: assignTrue({}, this._state), current: this._state });
		});

		if (options.target) {
			if (options.hydrate) throw new Error("options.hydrate only works if the component was compiled with the `hydratable: true` option");
			this._fragment.c();
			this._mount(options.target, options.anchor);

			flush(this);
		}
	}

	assign(Prices.prototype, protoDev);
	assign(Prices.prototype, methods$b);

	Prices.prototype._checkReadOnly = function _checkReadOnly(newState) {
		if ('available' in newState && !this._updatingReadonlyProperty) throw new Error("<Prices>: Cannot set read-only property 'available'");
		if ('priceIdMap' in newState && !this._updatingReadonlyProperty) throw new Error("<Prices>: Cannot set read-only property 'priceIdMap'");
	};

	Prices.prototype._recompute = function _recompute(changed, state) {
		if (changed.priceList || changed.value || changed.notAvailableMsg) {
			if (this._differs(state.available, (state.available = available(state)))) changed.available = true;
		}

		if (changed.priceList) {
			if (this._differs(state.priceIdMap, (state.priceIdMap = priceIdMap(state)))) changed.priceIdMap = true;
		}
	};

	/* src/app/App.html generated by Svelte v2.15.3 */



	function categoryDropdown({ $categories }) {
	  return $categories.map(c => [+c.tid, c.data.current.name]);
	}
	function data$c() {
	  return {
	    editing: false,
	    isNew: false,
	    course: {
	      id: null,
	      category: 1,
	      available: false,
	      name: "",
	      slogan: "",
	      slug: "",
	      cpDescription: "",
	      cpImage: "",
	    },

	    addingCategory: false,
	    newCategoryName: "",
	    categoryPending: false,
	    // categoryDropdown: [
	    //   // [0, "Item"], // Value, Item
	    //   // 0,      // Divider
	    // ],

	    firstFieldEdit: false,
	    tabs: [],
	    currentTab: "",
	  }
	}
	var methods$c = {
	  newCourse(evt) {
	    const { category } = evt || {category: null};
	    const { locale } = this.store.get();

	    this.set({
	      editing: true,
	      firstFieldEdit: true,
	      isNew: true,
	      course: {
	        id: null,
	        category: category === null ? 1 : category,
	        available: false,
	        name: locale.newName,
	        slogan: locale.newSlogan,
	        slug: locale.newName,
	        prices: [
	          // 1, // Commerce Product ID
	          // 2,
	        ],
	        featuredPrice: -1,
	        cpDescription: locale.newDescription,
	        cpFeatures: [
	          // { title: "", description: "" },
	          // ...
	        ],
	        cpImage: "",
	        fpData: {
	          fields: [
	            // {name: "", value: ..., type: "text" | "bigtext" | "image"},
	            // ...
	          ],
	          theme: "",
	          themeMap: {
	            // id1: 1,
	            // id2: null, // Nothing in this region
	            // id3: 2,
	            // id4: 3,
	          },
	          themeVariables: {
	            // var1: value,
	            // var2: value,
	          },
	        },
	        levels: [
	          // {
	          //   id: null,
	          //   title: "",
	          //   desc: "",
	          //   books: ["hash1", "hash2", ...],
	          // },
	          // // ...
	        ],
	        faq: [
	          // {
	          //   id: null,
	          //   title: "",
	          //   desc: "",
	          // },
	          // // ...
	        ],
	      }
	    });
	  },

	  newCategory() {
	    this.set({
	      addingCategory: true,
	    });

	    this.store.set({
	      newCategoryName: "",
	    });
	  },

	  saveCourse() {
	    const { course } = this.get();
	    const { categories } = this.store.get();
	    const { id, available, category, ...courseData } = course;

	    this.set({
	      isNew: false,
	    });

	    const curId = id === null ? "new" : id;
	    this.store.set({
	      savingCourse: true,
	      localCourses: {
	        [curId]: {
	          id: curId,
	          category,
	          available,
	          locale: {
	            en: courseData,
	          }
	        }
	      }
	    });
	    this.store.fire("savecourse", curId);
	  },

	  addCourseFeature() {
	    const { locale } = this.store.get();
	    const { course } = this.get();
	    const { cpFeatures } = course;

	    cpFeatures.push({
	      title: locale.cpeFeatureDefaultNewFeature,
	      description: locale.cpeFeatureDefaultNewDescription,
	    });

	    this.set({
	      course,
	    });
	  },

	  removeCourseFeature(index) {
	    const { course } = this.get();

	    course.cpFeatures.splice(index, 1);
	    this.set({
	      course,
	    });
	  },

	  isFinished(evt) {
	    const code = evt.keyCode;
	    evt.preventDefault();
	    evt.stopPropagation();

	    if (code === 13) {
	      this.store.fire("newcategory");
	      this.set({
	        categoryPending: true,
	      });
	    } else if (code === 27) {
	      this.set({
	        addingCategory: false,
	      });
	    }
	  },

	  openFrontPageLayoutEditor() {
	    this.store.fire("setfrontpagelayouteditor", {
	      visible: true,
	    });
	  },
	};

	function oncreate$8() {
	  this.on("namechange", () => {
	    // Keep slug in sync with course name changes
	    const { course } = this.get();
	    course.slug = machineName(course.name);

	    this.set({
	      course,
	    });
	  });

	  this.on("slugchange", () => {
	    // Ensure slug is always a valid machine name
	    const { course } = this.get();
	    course.slug = machineName(course.slug);

	    this.set({
	      course,
	    });
	  });

	  const listener = this.store.on("newcourse", ({ category }) => {
	    this.newCourse({category});
	  });

	  const listener2 = this.store.on("selectcourse", ({ pid }) => {
	    const { localCourses, language } = this.store.get();
	    const courseLocal = localCourses[pid];

	    if (courseLocal) {
	      const course = {
	        id: pid,
	        category: courseLocal.category,
	        available: courseLocal.available,
	        ...courseLocal.locale[language],
	      };

	      this.set({
	        course,
	        editing: true,
	        isNew: false,
	      });
	    }
	  });

	  const listener3 = this.store.on("savedcourse", () => {
	    this.set({
	      editing: false,
	    });
	  });

	  // Update front page data when theme editor changes
	  const updateThemeData = () => {
	    const { currentThemeMap, currentThemeVariables } = this.store.get();
	    const { course } = this.get();

	    course.fpData.themeMap = currentThemeMap;
	    course.fpData.themeVariables = currentThemeVariables;

	    this.set({
	      course,
	    });
	  };

	  const listener4 = this.store.on("thememappingupdated", updateThemeData);
	  const listener5 = this.store.on("themevariableupdated", updateThemeData);

	  const listener6 = this.store.on("themeupdated", ({ theme }) => {
	    // Sync theme of themeeditor and course data stored here
	    const { course } = this.get();

	    course.fpData.theme = theme ? theme.id : "";
	    this.set({
	      course,
	    });
	  });

	  this.on("destroy", () => {
	    listener.cancel();
	    listener2.cancel();
	    listener3.cancel();
	    listener4.cancel();
	    listener5.cancel();
	    listener6.cancel();
	  });
	}
	function focus$3(node) {
	  const { isNew } = this.get();
	  node.focus();

	  if (isNew) {
	    setTimeout(() => {
	      node.select();
	    });
	  }
	}
	const file$f = "src/app/App.html";

	function click_handler$8(event) {
		const { component, ctx } = this._svelte;

		component.removeCourseFeature(ctx.featureIndex);
	}

	function get_each_context$9(ctx, list, i) {
		const child_ctx = Object.create(ctx);
		child_ctx.feature = list[i];
		child_ctx.each_value = list;
		child_ctx.featureIndex = i;
		return child_ctx;
	}

	function create_main_fragment$f(component, ctx) {
		var div5, div0, button0, span0, i0, text0, span1, text1_value = ctx.$locale.editFrontpageLayout, text1, text2, div4, div3, text3, div1, button1, span2, i1, text4, span3, text5_value = ctx.$locale.addCategory, text5, text6, div2, button2, span4, i2, text7, span5, text8_value = ctx.$locale.addCourse, text8, text9, div8, div6, h2, text10_value = ctx.$locale.courseList, text10, text11, text12, div7, text13, text14;

		function click_handler(event) {
			component.openFrontPageLayoutEditor();
		}

		var if_block0 = (ctx.addingCategory) && create_if_block_7$1(component, ctx);

		function click_handler_1(event) {
			component.newCategory();
		}

		function click_handler_2(event) {
			component.newCourse();
		}

		var if_block1 = (ctx.$categories.length) && create_if_block_6$2(component, ctx);

		function select_block_type(ctx) {
			if (ctx.editing) return create_if_block$a;
			return create_else_block$6;
		}

		var current_block_type = select_block_type(ctx);
		var if_block2 = current_block_type(component, ctx);

		var themeeditor_initial_data = { courseId: "course.pid" };
		var themeeditor = new ThemeEditor({
			root: component.root,
			store: component.store,
			data: themeeditor_initial_data
		});

		var frontpagelayouteditor = new FrontPageLayoutEditor({
			root: component.root,
			store: component.store
		});

		return {
			c: function create() {
				div5 = createElement("div");
				div0 = createElement("div");
				button0 = createElement("button");
				span0 = createElement("span");
				i0 = createElement("i");
				text0 = createText("\n      ");
				span1 = createElement("span");
				text1 = createText(text1_value);
				text2 = createText("\n  ");
				div4 = createElement("div");
				div3 = createElement("div");
				if (if_block0) if_block0.c();
				text3 = createText("\n\n      ");
				div1 = createElement("div");
				button1 = createElement("button");
				span2 = createElement("span");
				i1 = createElement("i");
				text4 = createText("\n          ");
				span3 = createElement("span");
				text5 = createText(text5_value);
				text6 = createText("\n\n      ");
				div2 = createElement("div");
				button2 = createElement("button");
				span4 = createElement("span");
				i2 = createElement("i");
				text7 = createText("\n          ");
				span5 = createElement("span");
				text8 = createText(text8_value);
				text9 = createText("\n\n");
				div8 = createElement("div");
				div6 = createElement("div");
				h2 = createElement("h2");
				text10 = createText(text10_value);
				text11 = createText("\n\n    ");
				if (if_block1) if_block1.c();
				text12 = createText("\n  ");
				div7 = createElement("div");
				if_block2.c();
				text13 = createText("\n\n\n");
				themeeditor._fragment.c();
				text14 = createText("\n");
				frontpagelayouteditor._fragment.c();
				i0.className = "fa fa-columns";
				setStyle(i0, "transform", "rotate(-90deg)");
				addLoc(i0, file$f, 8, 8, 222);
				span0.className = "icon";
				addLoc(span0, file$f, 7, 6, 194);
				addLoc(span1, file$f, 13, 6, 338);
				addListener(button0, "click", click_handler);
				button0.className = "button is-link";
				button0.type = "button";
				addLoc(button0, file$f, 3, 4, 85);
				div0.className = "level-left";
				addLoc(div0, file$f, 2, 2, 56);
				i1.className = "fa fa-plus";
				addLoc(i1, file$f, 43, 12, 1112);
				span2.className = "icon";
				addLoc(span2, file$f, 42, 10, 1080);
				addLoc(span3, file$f, 45, 10, 1167);
				addListener(button1, "click", click_handler_1);
				button1.className = "button is-link";
				button1.type = "button";
				addLoc(button1, file$f, 38, 8, 969);
				div1.className = "level-item";
				addLoc(div1, file$f, 37, 6, 936);
				i2.className = "fa fa-plus";
				addLoc(i2, file$f, 57, 12, 1438);
				span4.className = "icon";
				addLoc(span4, file$f, 56, 10, 1406);
				addLoc(span5, file$f, 59, 10, 1493);
				addListener(button2, "click", click_handler_2);
				button2.className = "button is-link";
				button2.type = "button";
				addLoc(button2, file$f, 52, 8, 1297);
				div2.className = "level-item";
				addLoc(div2, file$f, 51, 6, 1264);
				div3.className = "level-item";
				addLoc(div3, file$f, 20, 4, 478);
				div4.className = "level-right";
				addLoc(div4, file$f, 18, 2, 422);
				div5.className = "level";
				addLoc(div5, file$f, 1, 0, 34);
				h2.className = "title is-5";
				addLoc(h2, file$f, 70, 4, 1677);
				div6.className = "column course-list-column svelte-wu7uai";
				addLoc(div6, file$f, 69, 2, 1633);
				div7.className = "column is-8";
				addLoc(div7, file$f, 78, 2, 1864);
				div8.className = "columns";
				addLoc(div8, file$f, 68, 0, 1609);
			},

			m: function mount(target, anchor) {
				insert(target, div5, anchor);
				append(div5, div0);
				append(div0, button0);
				append(button0, span0);
				append(span0, i0);
				append(button0, text0);
				append(button0, span1);
				append(span1, text1);
				append(div5, text2);
				append(div5, div4);
				append(div4, div3);
				if (if_block0) if_block0.m(div3, null);
				append(div3, text3);
				append(div3, div1);
				append(div1, button1);
				append(button1, span2);
				append(span2, i1);
				append(button1, text4);
				append(button1, span3);
				append(span3, text5);
				append(div3, text6);
				append(div3, div2);
				append(div2, button2);
				append(button2, span4);
				append(span4, i2);
				append(button2, text7);
				append(button2, span5);
				append(span5, text8);
				insert(target, text9, anchor);
				insert(target, div8, anchor);
				append(div8, div6);
				append(div6, h2);
				append(h2, text10);
				append(div6, text11);
				if (if_block1) if_block1.m(div6, null);
				append(div8, text12);
				append(div8, div7);
				if_block2.m(div7, null);
				insert(target, text13, anchor);
				themeeditor._mount(target, anchor);
				insert(target, text14, anchor);
				frontpagelayouteditor._mount(target, anchor);
			},

			p: function update(changed, ctx) {
				if ((changed.$locale) && text1_value !== (text1_value = ctx.$locale.editFrontpageLayout)) {
					setData(text1, text1_value);
				}

				if (ctx.addingCategory) {
					if (if_block0) {
						if_block0.p(changed, ctx);
					} else {
						if_block0 = create_if_block_7$1(component, ctx);
						if_block0.c();
						if_block0.m(div3, text3);
					}
				} else if (if_block0) {
					if_block0.d(1);
					if_block0 = null;
				}

				if ((changed.$locale) && text5_value !== (text5_value = ctx.$locale.addCategory)) {
					setData(text5, text5_value);
				}

				if ((changed.$locale) && text8_value !== (text8_value = ctx.$locale.addCourse)) {
					setData(text8, text8_value);
				}

				if ((changed.$locale) && text10_value !== (text10_value = ctx.$locale.courseList)) {
					setData(text10, text10_value);
				}

				if (ctx.$categories.length) {
					if (if_block1) {
						if_block1.p(changed, ctx);
					} else {
						if_block1 = create_if_block_6$2(component, ctx);
						if_block1.c();
						if_block1.m(div6, null);
					}
				} else if (if_block1) {
					if_block1.d(1);
					if_block1 = null;
				}

				if (current_block_type === (current_block_type = select_block_type(ctx)) && if_block2) {
					if_block2.p(changed, ctx);
				} else {
					if_block2.d(1);
					if_block2 = current_block_type(component, ctx);
					if_block2.c();
					if_block2.m(div7, null);
				}
			},

			d: function destroy$$1(detach) {
				if (detach) {
					detachNode(div5);
				}

				removeListener(button0, "click", click_handler);
				if (if_block0) if_block0.d();
				removeListener(button1, "click", click_handler_1);
				removeListener(button2, "click", click_handler_2);
				if (detach) {
					detachNode(text9);
					detachNode(div8);
				}

				if (if_block1) if_block1.d();
				if_block2.d();
				if (detach) {
					detachNode(text13);
				}

				themeeditor.destroy(detach);
				if (detach) {
					detachNode(text14);
				}

				frontpagelayouteditor.destroy(detach);
			}
		};
	}

	// (22:6) {#if addingCategory}
	function create_if_block_7$1(component, ctx) {
		var div1, div0, input, input_updating = false, input_placeholder_value, focus_action;

		function input_input_handler() {
			input_updating = true;
			component.store.set({ newCategoryName: input.value });
			input_updating = false;
		}

		function keyup_handler(event) {
			component.isFinished(event);
		}

		return {
			c: function create() {
				div1 = createElement("div");
				div0 = createElement("div");
				input = createElement("input");
				addListener(input, "input", input_input_handler);
				addListener(input, "keyup", keyup_handler);
				setAttribute(input, "type", "text");
				input.className = "input";
				input.placeholder = input_placeholder_value = ctx.$locale.newCategory;
				addLoc(input, file$f, 26, 12, 666);
				div0.className = "control";
				toggleClass(div0, "is-loading", ctx.categoryPending);
				addLoc(div0, file$f, 23, 10, 573);
				div1.className = "level-item";
				addLoc(div1, file$f, 22, 8, 538);
			},

			m: function mount(target, anchor) {
				insert(target, div1, anchor);
				append(div1, div0);
				append(div0, input);
				focus_action = focus$3.call(component, input) || {};

				input.value = ctx.$newCategoryName;
			},

			p: function update(changed, ctx) {
				if (!input_updating && changed.$newCategoryName) input.value = ctx.$newCategoryName;
				if ((changed.$locale) && input_placeholder_value !== (input_placeholder_value = ctx.$locale.newCategory)) {
					input.placeholder = input_placeholder_value;
				}

				if (changed.categoryPending) {
					toggleClass(div0, "is-loading", ctx.categoryPending);
				}
			},

			d: function destroy$$1(detach) {
				if (detach) {
					detachNode(div1);
				}

				removeListener(input, "input", input_input_handler);
				removeListener(input, "keyup", keyup_handler);
				if (focus_action && typeof focus_action.destroy === 'function') focus_action.destroy.call(component);
			}
		};
	}

	// (73:4) {#if $categories.length}
	function create_if_block_6$2(component, ctx) {
		var div;

		var courselist_initial_data = { list: ctx.$categories };
		var courselist = new CourseList({
			root: component.root,
			store: component.store,
			data: courselist_initial_data
		});

		return {
			c: function create() {
				div = createElement("div");
				courselist._fragment.c();
				div.className = "course-list svelte-wu7uai";
				addLoc(div, file$f, 73, 6, 1762);
			},

			m: function mount(target, anchor) {
				insert(target, div, anchor);
				courselist._mount(div, null);
			},

			p: function update(changed, ctx) {
				var courselist_changes = {};
				if (changed.$categories) courselist_changes.list = ctx.$categories;
				courselist._set(courselist_changes);
			},

			d: function destroy$$1(detach) {
				if (detach) {
					detachNode(div);
				}

				courselist.destroy();
			}
		};
	}

	// (205:4) {:else}
	function create_else_block$6(component, ctx) {
		var div, h2, text_value = ctx.$locale.selectOrNew, text;

		return {
			c: function create() {
				div = createElement("div");
				h2 = createElement("h2");
				text = createText(text_value);
				h2.className = "title is-4 has-text-grey-lighter has-text-centered empty-course-view-title svelte-wu7uai";
				addLoc(h2, file$f, 208, 8, 5725);
				div.className = "box empty-course-view svelte-wu7uai";
				addLoc(div, file$f, 207, 6, 5681);
			},

			m: function mount(target, anchor) {
				insert(target, div, anchor);
				append(div, h2);
				append(h2, text);
			},

			p: function update(changed, ctx) {
				if ((changed.$locale) && text_value !== (text_value = ctx.$locale.selectOrNew)) {
					setData(text, text_value);
				}
			},

			d: function destroy$$1(detach) {
				if (detach) {
					detachNode(div);
				}
			}
		};
	}

	// (80:4) {#if editing}
	function create_if_block$a(component, ctx) {
		var div5, editableheadertextfield0_updating = {}, text0, editableheadertextfield1_updating = {}, text1, editabletextfield_updating = {}, text2, div4, div1, div0, label, input, text3, span, text4_value = ctx.$locale.availableCheckboxLabel, text4, text5, div3, div2, dropdown_updating = {}, text6, tabs_updating = {}, text7, text8, hr, text9, savediscardbuttons_updating = {};

		var editableheadertextfield0_initial_data = { small: false, isNew: "isNew" };
		if (ctx.course.name !== void 0) {
			editableheadertextfield0_initial_data.value = ctx.course.name;
			editableheadertextfield0_updating.value = true;
		}
		if (ctx.firstFieldEdit !== void 0) {
			editableheadertextfield0_initial_data.edit = ctx.firstFieldEdit;
			editableheadertextfield0_updating.edit = true;
		}
		var editableheadertextfield0 = new EditableHeaderTextField({
			root: component.root,
			store: component.store,
			data: editableheadertextfield0_initial_data,
			_bind(changed, childState) {
				var newState = {};
				if (!editableheadertextfield0_updating.value && changed.value) {
					ctx.course.name = childState.value;
					newState.course = ctx.course;
				}

				if (!editableheadertextfield0_updating.edit && changed.edit) {
					newState.firstFieldEdit = childState.edit;
				}
				component._set(newState);
				editableheadertextfield0_updating = {};
			}
		});

		component.root._beforecreate.push(() => {
			editableheadertextfield0._bind({ value: 1, edit: 1 }, editableheadertextfield0.get());
		});

		editableheadertextfield0.on("change", function(event) {
			component.fire('namechange');
		});

		var editableheadertextfield1_initial_data = { isNormal: true, isNew: "isNew" };
		if (ctx.course.slogan !== void 0) {
			editableheadertextfield1_initial_data.value = ctx.course.slogan;
			editableheadertextfield1_updating.value = true;
		}
		var editableheadertextfield1 = new EditableHeaderTextField({
			root: component.root,
			store: component.store,
			data: editableheadertextfield1_initial_data,
			_bind(changed, childState) {
				var newState = {};
				if (!editableheadertextfield1_updating.value && changed.value) {
					ctx.course.slogan = childState.value;
					newState.course = ctx.course;
				}
				component._set(newState);
				editableheadertextfield1_updating = {};
			}
		});

		component.root._beforecreate.push(() => {
			editableheadertextfield1._bind({ value: 1 }, editableheadertextfield1.get());
		});

		var editabletextfield_initial_data = {
		 	isMono: true,
		 	prefix: "/course/",
		 	prefixFaded: true,
		 	label: ctx.$locale.slugInputLabel
		 };
		if (ctx.course.slug !== void 0) {
			editabletextfield_initial_data.value = ctx.course.slug;
			editabletextfield_updating.value = true;
		}
		var editabletextfield = new EditableTextField({
			root: component.root,
			store: component.store,
			data: editabletextfield_initial_data,
			_bind(changed, childState) {
				var newState = {};
				if (!editabletextfield_updating.value && changed.value) {
					ctx.course.slug = childState.value;
					newState.course = ctx.course;
				}
				component._set(newState);
				editabletextfield_updating = {};
			}
		});

		component.root._beforecreate.push(() => {
			editabletextfield._bind({ value: 1 }, editabletextfield.get());
		});

		editabletextfield.on("change", function(event) {
			component.fire('slugchange');
		});

		function input_change_handler() {
			ctx.course.available = input.checked;
			component.set({ course: ctx.course });
		}

		var dropdown_initial_data = {
		 	list: ctx.categoryDropdown,
		 	right: true,
		 	disabled: !ctx.isNew
		 };
		if (ctx.course.category !== void 0) {
			dropdown_initial_data.value = ctx.course.category;
			dropdown_updating.value = true;
		}
		var dropdown = new Dropdown({
			root: component.root,
			store: component.store,
			data: dropdown_initial_data,
			_bind(changed, childState) {
				var newState = {};
				if (!dropdown_updating.value && changed.value) {
					ctx.course.category = childState.value;
					newState.course = ctx.course;
				}
				component._set(newState);
				dropdown_updating = {};
			}
		});

		component.root._beforecreate.push(() => {
			dropdown._bind({ value: 1 }, dropdown.get());
		});

		var tabs_initial_data = { tabs: ctx.tabs };
		if (ctx.currentTab !== void 0) {
			tabs_initial_data.current = ctx.currentTab;
			tabs_updating.current = true;
		}
		var tabs = new Tabs({
			root: component.root,
			store: component.store,
			data: tabs_initial_data,
			_bind(changed, childState) {
				var newState = {};
				if (!tabs_updating.current && changed.current) {
					newState.currentTab = childState.current;
				}
				component._set(newState);
				tabs_updating = {};
			}
		});

		component.root._beforecreate.push(() => {
			tabs._bind({ current: 1 }, tabs.get());
		});

		function select_block_type_1(ctx) {
			if (ctx.currentTab === "cp") return create_if_block_1$7;
			if (ctx.currentTab === "fp") return create_if_block_2$4;
			if (ctx.currentTab === "cl") return create_if_block_3$4;
			if (ctx.currentTab === "cf") return create_if_block_4$2;
			if (ctx.currentTab === "pr") return create_if_block_5$2;
		}

		var current_block_type = select_block_type_1(ctx);
		var if_block = current_block_type && current_block_type(component, ctx);

		var savediscardbuttons_initial_data = {};
		if (ctx.editing !== void 0) {
			savediscardbuttons_initial_data.editing = ctx.editing;
			savediscardbuttons_updating.editing = true;
		}
		var savediscardbuttons = new SaveDiscardButtons({
			root: component.root,
			store: component.store,
			data: savediscardbuttons_initial_data,
			_bind(changed, childState) {
				var newState = {};
				if (!savediscardbuttons_updating.editing && changed.editing) {
					newState.editing = childState.editing;
				}
				component._set(newState);
				savediscardbuttons_updating = {};
			}
		});

		component.root._beforecreate.push(() => {
			savediscardbuttons._bind({ editing: 1 }, savediscardbuttons.get());
		});

		savediscardbuttons.on("save", function(event) {
			component.saveCourse();
		});

		return {
			c: function create() {
				div5 = createElement("div");
				editableheadertextfield0._fragment.c();
				text0 = createText("\n        ");
				editableheadertextfield1._fragment.c();
				text1 = createText("\n        ");
				editabletextfield._fragment.c();
				text2 = createText("\n\n        \n        ");
				div4 = createElement("div");
				div1 = createElement("div");
				div0 = createElement("div");
				label = createElement("label");
				input = createElement("input");
				text3 = createText("\n                ");
				span = createElement("span");
				text4 = createText(text4_value);
				text5 = createText("\n          ");
				div3 = createElement("div");
				div2 = createElement("div");
				dropdown._fragment.c();
				text6 = createText("\n\n        \n        ");
				tabs._fragment.c();
				text7 = createText("\n\n        \n        ");
				if (if_block) if_block.c();
				text8 = createText("\n        ");
				hr = createElement("hr");
				text9 = createText("\n\n        ");
				savediscardbuttons._fragment.c();
				addListener(input, "change", input_change_handler);
				setAttribute(input, "type", "checkbox");
				input.className = "svelte-wu7uai";
				addLoc(input, file$f, 105, 16, 2676);
				span.className = "svelte-wu7uai";
				addLoc(span, file$f, 108, 16, 2782);
				label.className = "svelte-wu7uai";
				addLoc(label, file$f, 104, 14, 2652);
				div0.className = "level-item svelte-wu7uai";
				addLoc(div0, file$f, 103, 12, 2613);
				div1.className = "level-left svelte-wu7uai";
				addLoc(div1, file$f, 102, 10, 2576);
				div2.className = "level-item svelte-wu7uai";
				addLoc(div2, file$f, 115, 12, 2971);
				div3.className = "level-right svelte-wu7uai";
				addLoc(div3, file$f, 114, 10, 2933);
				div4.className = "level";
				addLoc(div4, file$f, 101, 8, 2546);
				addLoc(hr, file$f, 199, 8, 5517);
				div5.className = "box course-editor svelte-wu7uai";
				addLoc(div5, file$f, 81, 6, 1915);
			},

			m: function mount(target, anchor) {
				insert(target, div5, anchor);
				editableheadertextfield0._mount(div5, null);
				append(div5, text0);
				editableheadertextfield1._mount(div5, null);
				append(div5, text1);
				editabletextfield._mount(div5, null);
				append(div5, text2);
				append(div5, div4);
				append(div4, div1);
				append(div1, div0);
				append(div0, label);
				append(label, input);

				input.checked = ctx.course.available;

				append(label, text3);
				append(label, span);
				append(span, text4);
				append(div4, text5);
				append(div4, div3);
				append(div3, div2);
				dropdown._mount(div2, null);
				append(div5, text6);
				tabs._mount(div5, null);
				append(div5, text7);
				if (if_block) if_block.m(div5, null);
				append(div5, text8);
				append(div5, hr);
				append(div5, text9);
				savediscardbuttons._mount(div5, null);
			},

			p: function update(changed, _ctx) {
				ctx = _ctx;
				var editableheadertextfield0_changes = {};
				if (!editableheadertextfield0_updating.value && changed.course) {
					editableheadertextfield0_changes.value = ctx.course.name;
					editableheadertextfield0_updating.value = ctx.course.name !== void 0;
				}
				if (!editableheadertextfield0_updating.edit && changed.firstFieldEdit) {
					editableheadertextfield0_changes.edit = ctx.firstFieldEdit;
					editableheadertextfield0_updating.edit = ctx.firstFieldEdit !== void 0;
				}
				editableheadertextfield0._set(editableheadertextfield0_changes);
				editableheadertextfield0_updating = {};

				var editableheadertextfield1_changes = {};
				if (!editableheadertextfield1_updating.value && changed.course) {
					editableheadertextfield1_changes.value = ctx.course.slogan;
					editableheadertextfield1_updating.value = ctx.course.slogan !== void 0;
				}
				editableheadertextfield1._set(editableheadertextfield1_changes);
				editableheadertextfield1_updating = {};

				var editabletextfield_changes = {};
				if (changed.$locale) editabletextfield_changes.label = ctx.$locale.slugInputLabel;
				if (!editabletextfield_updating.value && changed.course) {
					editabletextfield_changes.value = ctx.course.slug;
					editabletextfield_updating.value = ctx.course.slug !== void 0;
				}
				editabletextfield._set(editabletextfield_changes);
				editabletextfield_updating = {};

				if (changed.course) input.checked = ctx.course.available;
				if ((changed.$locale) && text4_value !== (text4_value = ctx.$locale.availableCheckboxLabel)) {
					setData(text4, text4_value);
				}

				var dropdown_changes = {};
				if (changed.categoryDropdown) dropdown_changes.list = ctx.categoryDropdown;
				if (changed.isNew) dropdown_changes.disabled = !ctx.isNew;
				if (!dropdown_updating.value && changed.course) {
					dropdown_changes.value = ctx.course.category;
					dropdown_updating.value = ctx.course.category !== void 0;
				}
				dropdown._set(dropdown_changes);
				dropdown_updating = {};

				var tabs_changes = {};
				if (changed.tabs) tabs_changes.tabs = ctx.tabs;
				if (!tabs_updating.current && changed.currentTab) {
					tabs_changes.current = ctx.currentTab;
					tabs_updating.current = ctx.currentTab !== void 0;
				}
				tabs._set(tabs_changes);
				tabs_updating = {};

				if (current_block_type === (current_block_type = select_block_type_1(ctx)) && if_block) {
					if_block.p(changed, ctx);
				} else {
					if (if_block) if_block.d(1);
					if_block = current_block_type && current_block_type(component, ctx);
					if (if_block) if_block.c();
					if (if_block) if_block.m(div5, text8);
				}

				var savediscardbuttons_changes = {};
				if (!savediscardbuttons_updating.editing && changed.editing) {
					savediscardbuttons_changes.editing = ctx.editing;
					savediscardbuttons_updating.editing = ctx.editing !== void 0;
				}
				savediscardbuttons._set(savediscardbuttons_changes);
				savediscardbuttons_updating = {};
			},

			d: function destroy$$1(detach) {
				if (detach) {
					detachNode(div5);
				}

				editableheadertextfield0.destroy();
				editableheadertextfield1.destroy();
				editabletextfield.destroy();
				removeListener(input, "change", input_change_handler);
				dropdown.destroy();
				tabs.destroy();
				if (if_block) if_block.d();
				savediscardbuttons.destroy();
			}
		};
	}

	// (191:37) 
	function create_if_block_5$2(component, ctx) {
		var prices_updating = {};

		var prices_initial_data = { priceList: ctx.$prices };
		if (ctx.course.prices !== void 0) {
			prices_initial_data.value = ctx.course.prices;
			prices_updating.value = true;
		}
		if (ctx.course.featuredPrice !== void 0) {
			prices_initial_data.featuredPrice = ctx.course.featuredPrice;
			prices_updating.featuredPrice = true;
		}
		var prices = new Prices({
			root: component.root,
			store: component.store,
			data: prices_initial_data,
			_bind(changed, childState) {
				var newState = {};
				if (!prices_updating.value && changed.value) {
					ctx.course.prices = childState.value;
					newState.course = ctx.course;
				}

				if (!prices_updating.featuredPrice && changed.featuredPrice) {
					ctx.course.featuredPrice = childState.featuredPrice;
					newState.course = ctx.course;
				}
				component._set(newState);
				prices_updating = {};
			}
		});

		component.root._beforecreate.push(() => {
			prices._bind({ value: 1, featuredPrice: 1 }, prices.get());
		});

		return {
			c: function create() {
				prices._fragment.c();
			},

			m: function mount(target, anchor) {
				prices._mount(target, anchor);
			},

			p: function update(changed, _ctx) {
				ctx = _ctx;
				var prices_changes = {};
				if (changed.$prices) prices_changes.priceList = ctx.$prices;
				if (!prices_updating.value && changed.course) {
					prices_changes.value = ctx.course.prices;
					prices_updating.value = ctx.course.prices !== void 0;
				}
				if (!prices_updating.featuredPrice && changed.course) {
					prices_changes.featuredPrice = ctx.course.featuredPrice;
					prices_updating.featuredPrice = ctx.course.featuredPrice !== void 0;
				}
				prices._set(prices_changes);
				prices_updating = {};
			},

			d: function destroy$$1(detach) {
				prices.destroy(detach);
			}
		};
	}

	// (185:37) 
	function create_if_block_4$2(component, ctx) {
		var courseinfodata_updating = {};

		var courseinfodata_initial_data = { dataType: "faq" };
		if (ctx.course.faq !== void 0) {
			courseinfodata_initial_data.data = ctx.course.faq;
			courseinfodata_updating.data = true;
		}
		var courseinfodata = new InfoData({
			root: component.root,
			store: component.store,
			data: courseinfodata_initial_data,
			_bind(changed, childState) {
				var newState = {};
				if (!courseinfodata_updating.data && changed.data) {
					ctx.course.faq = childState.data;
					newState.course = ctx.course;
				}
				component._set(newState);
				courseinfodata_updating = {};
			}
		});

		component.root._beforecreate.push(() => {
			courseinfodata._bind({ data: 1 }, courseinfodata.get());
		});

		return {
			c: function create() {
				courseinfodata._fragment.c();
			},

			m: function mount(target, anchor) {
				courseinfodata._mount(target, anchor);
			},

			p: function update(changed, _ctx) {
				ctx = _ctx;
				var courseinfodata_changes = {};
				if (!courseinfodata_updating.data && changed.course) {
					courseinfodata_changes.data = ctx.course.faq;
					courseinfodata_updating.data = ctx.course.faq !== void 0;
				}
				courseinfodata._set(courseinfodata_changes);
				courseinfodata_updating = {};
			},

			d: function destroy$$1(detach) {
				courseinfodata.destroy(detach);
			}
		};
	}

	// (179:37) 
	function create_if_block_3$4(component, ctx) {
		var courseinfodata_updating = {};

		var courseinfodata_initial_data = { dataType: "levels" };
		if (ctx.course.levels !== void 0) {
			courseinfodata_initial_data.data = ctx.course.levels;
			courseinfodata_updating.data = true;
		}
		var courseinfodata = new InfoData({
			root: component.root,
			store: component.store,
			data: courseinfodata_initial_data,
			_bind(changed, childState) {
				var newState = {};
				if (!courseinfodata_updating.data && changed.data) {
					ctx.course.levels = childState.data;
					newState.course = ctx.course;
				}
				component._set(newState);
				courseinfodata_updating = {};
			}
		});

		component.root._beforecreate.push(() => {
			courseinfodata._bind({ data: 1 }, courseinfodata.get());
		});

		return {
			c: function create() {
				courseinfodata._fragment.c();
			},

			m: function mount(target, anchor) {
				courseinfodata._mount(target, anchor);
			},

			p: function update(changed, _ctx) {
				ctx = _ctx;
				var courseinfodata_changes = {};
				if (!courseinfodata_updating.data && changed.course) {
					courseinfodata_changes.data = ctx.course.levels;
					courseinfodata_updating.data = ctx.course.levels !== void 0;
				}
				courseinfodata._set(courseinfodata_changes);
				courseinfodata_updating = {};
			},

			d: function destroy$$1(detach) {
				courseinfodata.destroy(detach);
			}
		};
	}

	// (173:37) 
	function create_if_block_2$4(component, ctx) {
		var frontpageeditor_updating = {};

		var frontpageeditor_initial_data = {};
		if (ctx.course.id !== void 0) {
			frontpageeditor_initial_data.courseId = ctx.course.id;
			frontpageeditor_updating.courseId = true;
		}
		if (ctx.course.fpData !== void 0) {
			frontpageeditor_initial_data.data = ctx.course.fpData;
			frontpageeditor_updating.data = true;
		}
		var frontpageeditor = new FrontPageEditor({
			root: component.root,
			store: component.store,
			data: frontpageeditor_initial_data,
			_bind(changed, childState) {
				var newState = {};
				if (!frontpageeditor_updating.courseId && changed.courseId) {
					ctx.course.id = childState.courseId;
					newState.course = ctx.course;
				}

				if (!frontpageeditor_updating.data && changed.data) {
					ctx.course.fpData = childState.data;
					newState.course = ctx.course;
				}
				component._set(newState);
				frontpageeditor_updating = {};
			}
		});

		component.root._beforecreate.push(() => {
			frontpageeditor._bind({ courseId: 1, data: 1 }, frontpageeditor.get());
		});

		return {
			c: function create() {
				frontpageeditor._fragment.c();
			},

			m: function mount(target, anchor) {
				frontpageeditor._mount(target, anchor);
			},

			p: function update(changed, _ctx) {
				ctx = _ctx;
				var frontpageeditor_changes = {};
				if (!frontpageeditor_updating.courseId && changed.course) {
					frontpageeditor_changes.courseId = ctx.course.id;
					frontpageeditor_updating.courseId = ctx.course.id !== void 0;
				}
				if (!frontpageeditor_updating.data && changed.course) {
					frontpageeditor_changes.data = ctx.course.fpData;
					frontpageeditor_updating.data = ctx.course.fpData !== void 0;
				}
				frontpageeditor._set(frontpageeditor_changes);
				frontpageeditor_updating = {};
			},

			d: function destroy$$1(detach) {
				frontpageeditor.destroy(detach);
			}
		};
	}

	// (130:8) {#if currentTab === "cp"}
	function create_if_block_1$7(component, ctx) {
		var editabletextareafield_updating = {}, text0, hr0, text1, div, text2, button, span0, i, text3, span1, text4_value = ctx.$locale.cpeAddFeatureButton, text4, text5, hr1, text6, filefield_updating = {};

		var editabletextareafield_initial_data = { label: ctx.$locale.coursePageDescription };
		if (ctx.course.cpDescription !== void 0) {
			editabletextareafield_initial_data.value = ctx.course.cpDescription;
			editabletextareafield_updating.value = true;
		}
		var editabletextareafield = new EditableTextAreaField({
			root: component.root,
			store: component.store,
			data: editabletextareafield_initial_data,
			_bind(changed, childState) {
				var newState = {};
				if (!editabletextareafield_updating.value && changed.value) {
					ctx.course.cpDescription = childState.value;
					newState.course = ctx.course;
				}
				component._set(newState);
				editabletextareafield_updating = {};
			}
		});

		component.root._beforecreate.push(() => {
			editabletextareafield._bind({ value: 1 }, editabletextareafield.get());
		});

		var each_value = ctx.course.cpFeatures;

		var each_blocks = [];

		for (var i_1 = 0; i_1 < each_value.length; i_1 += 1) {
			each_blocks[i_1] = create_each_block$9(component, get_each_context$9(ctx, each_value, i_1));
		}

		function click_handler_1(event) {
			component.addCourseFeature();
		}

		var filefield_initial_data = { label: ctx.$locale.cpImageField };
		if (ctx.course.cpImage !== void 0) {
			filefield_initial_data.value = ctx.course.cpImage;
			filefield_updating.value = true;
		}
		var filefield = new FileField({
			root: component.root,
			store: component.store,
			data: filefield_initial_data,
			_bind(changed, childState) {
				var newState = {};
				if (!filefield_updating.value && changed.value) {
					ctx.course.cpImage = childState.value;
					newState.course = ctx.course;
				}
				component._set(newState);
				filefield_updating = {};
			}
		});

		component.root._beforecreate.push(() => {
			filefield._bind({ value: 1 }, filefield.get());
		});

		return {
			c: function create() {
				editabletextareafield._fragment.c();
				text0 = createText("\n\n          ");
				hr0 = createElement("hr");
				text1 = createText("\n          \n          ");
				div = createElement("div");

				for (var i_1 = 0; i_1 < each_blocks.length; i_1 += 1) {
					each_blocks[i_1].c();
				}

				text2 = createText("\n\n            ");
				button = createElement("button");
				span0 = createElement("span");
				i = createElement("i");
				text3 = createText("\n              ");
				span1 = createElement("span");
				text4 = createText(text4_value);
				text5 = createText("\n\n          ");
				hr1 = createElement("hr");
				text6 = createText("\n          ");
				filefield._fragment.c();
				addLoc(hr0, file$f, 134, 10, 3515);
				i.className = "fa fa-plus";
				addLoc(i, file$f, 161, 16, 4603);
				span0.className = "icon";
				addLoc(span0, file$f, 160, 14, 4567);
				addLoc(span1, file$f, 163, 14, 4666);
				addListener(button, "click", click_handler_1);
				button.className = "button is-fullwidth is-medium is-link is-outlined";
				button.type = "button";
				addLoc(button, file$f, 156, 12, 4391);
				div.className = "course-features svelte-wu7uai";
				addLoc(div, file$f, 136, 10, 3565);
				addLoc(hr1, file$f, 169, 10, 4791);
			},

			m: function mount(target, anchor) {
				editabletextareafield._mount(target, anchor);
				insert(target, text0, anchor);
				insert(target, hr0, anchor);
				insert(target, text1, anchor);
				insert(target, div, anchor);

				for (var i_1 = 0; i_1 < each_blocks.length; i_1 += 1) {
					each_blocks[i_1].m(div, null);
				}

				append(div, text2);
				append(div, button);
				append(button, span0);
				append(span0, i);
				append(button, text3);
				append(button, span1);
				append(span1, text4);
				insert(target, text5, anchor);
				insert(target, hr1, anchor);
				insert(target, text6, anchor);
				filefield._mount(target, anchor);
			},

			p: function update(changed, _ctx) {
				ctx = _ctx;
				var editabletextareafield_changes = {};
				if (changed.$locale) editabletextareafield_changes.label = ctx.$locale.coursePageDescription;
				if (!editabletextareafield_updating.value && changed.course) {
					editabletextareafield_changes.value = ctx.course.cpDescription;
					editabletextareafield_updating.value = ctx.course.cpDescription !== void 0;
				}
				editabletextareafield._set(editabletextareafield_changes);
				editabletextareafield_updating = {};

				if (changed.$locale || changed.course) {
					each_value = ctx.course.cpFeatures;

					for (var i_1 = 0; i_1 < each_value.length; i_1 += 1) {
						const child_ctx = get_each_context$9(ctx, each_value, i_1);

						if (each_blocks[i_1]) {
							each_blocks[i_1].p(changed, child_ctx);
						} else {
							each_blocks[i_1] = create_each_block$9(component, child_ctx);
							each_blocks[i_1].c();
							each_blocks[i_1].m(div, text2);
						}
					}

					for (; i_1 < each_blocks.length; i_1 += 1) {
						each_blocks[i_1].d(1);
					}
					each_blocks.length = each_value.length;
				}

				if ((changed.$locale) && text4_value !== (text4_value = ctx.$locale.cpeAddFeatureButton)) {
					setData(text4, text4_value);
				}

				var filefield_changes = {};
				if (changed.$locale) filefield_changes.label = ctx.$locale.cpImageField;
				if (!filefield_updating.value && changed.course) {
					filefield_changes.value = ctx.course.cpImage;
					filefield_updating.value = ctx.course.cpImage !== void 0;
				}
				filefield._set(filefield_changes);
				filefield_updating = {};
			},

			d: function destroy$$1(detach) {
				editabletextareafield.destroy(detach);
				if (detach) {
					detachNode(text0);
					detachNode(hr0);
					detachNode(text1);
					detachNode(div);
				}

				destroyEach(each_blocks, detach);

				removeListener(button, "click", click_handler_1);
				if (detach) {
					detachNode(text5);
					detachNode(hr1);
					detachNode(text6);
				}

				filefield.destroy(detach);
			}
		};
	}

	// (140:12) {#each course.cpFeatures as feature, featureIndex}
	function create_each_block$9(component, ctx) {
		var div1, editabletextfield_updating = {}, text0, editabletextareafield_updating = {}, text1, div0, button, text2_value = ctx.$locale.fpeFieldDeleteButton, text2, text3, hr;

		var editabletextfield_initial_data = { label: ctx.$locale.cpeFeatureNameLabel };
		if (ctx.feature.title !== void 0) {
			editabletextfield_initial_data.value = ctx.feature.title;
			editabletextfield_updating.value = true;
		}
		var editabletextfield = new EditableTextField({
			root: component.root,
			store: component.store,
			data: editabletextfield_initial_data,
			_bind(changed, childState) {
				var newState = {};
				if (!editabletextfield_updating.value && changed.value) {
					ctx.feature.title = childState.value;

					newState.course = ctx.course;
				}
				component._set(newState);
				editabletextfield_updating = {};
			}
		});

		component.root._beforecreate.push(() => {
			editabletextfield._bind({ value: 1 }, editabletextfield.get());
		});

		var editabletextareafield_initial_data = { label: ctx.$locale.cpeFeatureValueLabel };
		if (ctx.feature.description !== void 0) {
			editabletextareafield_initial_data.value = ctx.feature.description;
			editabletextareafield_updating.value = true;
		}
		var editabletextareafield = new EditableTextAreaField({
			root: component.root,
			store: component.store,
			data: editabletextareafield_initial_data,
			_bind(changed, childState) {
				var newState = {};
				if (!editabletextareafield_updating.value && changed.value) {
					ctx.feature.description = childState.value;

					newState.course = ctx.course;
				}
				component._set(newState);
				editabletextareafield_updating = {};
			}
		});

		component.root._beforecreate.push(() => {
			editabletextareafield._bind({ value: 1 }, editabletextareafield.get());
		});

		return {
			c: function create() {
				div1 = createElement("div");
				editabletextfield._fragment.c();
				text0 = createText("\n                ");
				editabletextareafield._fragment.c();
				text1 = createText("\n                \n                ");
				div0 = createElement("div");
				button = createElement("button");
				text2 = createText(text2_value);
				text3 = createText("\n                ");
				hr = createElement("hr");
				button._svelte = { component, ctx };

				addListener(button, "click", click_handler$8);
				button.className = "button is-danger svelte-wu7uai";
				button.type = "button";
				addLoc(button, file$f, 145, 18, 4061);
				div0.className = "cp-feature-buttons svelte-wu7uai";
				addLoc(div0, file$f, 144, 16, 4010);
				hr.className = "svelte-wu7uai";
				addLoc(hr, file$f, 152, 16, 4332);
				div1.className = "cp-feature svelte-wu7uai";
				addLoc(div1, file$f, 140, 14, 3743);
			},

			m: function mount(target, anchor) {
				insert(target, div1, anchor);
				editabletextfield._mount(div1, null);
				append(div1, text0);
				editabletextareafield._mount(div1, null);
				append(div1, text1);
				append(div1, div0);
				append(div0, button);
				append(button, text2);
				append(div1, text3);
				append(div1, hr);
			},

			p: function update(changed, _ctx) {
				ctx = _ctx;
				var editabletextfield_changes = {};
				if (changed.$locale) editabletextfield_changes.label = ctx.$locale.cpeFeatureNameLabel;
				if (!editabletextfield_updating.value && changed.course) {
					editabletextfield_changes.value = ctx.feature.title;
					editabletextfield_updating.value = ctx.feature.title !== void 0;
				}
				editabletextfield._set(editabletextfield_changes);
				editabletextfield_updating = {};

				var editabletextareafield_changes = {};
				if (changed.$locale) editabletextareafield_changes.label = ctx.$locale.cpeFeatureValueLabel;
				if (!editabletextareafield_updating.value && changed.course) {
					editabletextareafield_changes.value = ctx.feature.description;
					editabletextareafield_updating.value = ctx.feature.description !== void 0;
				}
				editabletextareafield._set(editabletextareafield_changes);
				editabletextareafield_updating = {};

				if ((changed.$locale) && text2_value !== (text2_value = ctx.$locale.fpeFieldDeleteButton)) {
					setData(text2, text2_value);
				}

				button._svelte.ctx = ctx;
			},

			d: function destroy$$1(detach) {
				if (detach) {
					detachNode(div1);
				}

				editabletextfield.destroy();
				editabletextareafield.destroy();
				removeListener(button, "click", click_handler$8);
			}
		};
	}

	function App(options) {
		this._debugName = '<App>';
		if (!options || (!options.target && !options.root)) {
			throw new Error("'target' is a required option");
		}
		if (!options.store) {
			throw new Error("<App> references store properties, but no store was provided");
		}

		init(this, options);
		this._state = assign(assign(this.store._init(["categories","locale","newCategoryName","prices"]), data$c()), options.data);
		this.store._add(this, ["categories","locale","newCategoryName","prices"]);

		this._recompute({ $categories: 1 }, this._state);
		if (!('$categories' in this._state)) console.warn("<App> was created without expected data property '$categories'");
		if (!('$locale' in this._state)) console.warn("<App> was created without expected data property '$locale'");
		if (!('addingCategory' in this._state)) console.warn("<App> was created without expected data property 'addingCategory'");
		if (!('categoryPending' in this._state)) console.warn("<App> was created without expected data property 'categoryPending'");
		if (!('$newCategoryName' in this._state)) console.warn("<App> was created without expected data property '$newCategoryName'");
		if (!('editing' in this._state)) console.warn("<App> was created without expected data property 'editing'");
		if (!('course' in this._state)) console.warn("<App> was created without expected data property 'course'");
		if (!('firstFieldEdit' in this._state)) console.warn("<App> was created without expected data property 'firstFieldEdit'");

		if (!('isNew' in this._state)) console.warn("<App> was created without expected data property 'isNew'");
		if (!('tabs' in this._state)) console.warn("<App> was created without expected data property 'tabs'");
		if (!('currentTab' in this._state)) console.warn("<App> was created without expected data property 'currentTab'");
		if (!('$prices' in this._state)) console.warn("<App> was created without expected data property '$prices'");
		this._intro = true;

		this._handlers.destroy = [removeFromStore];

		this._fragment = create_main_fragment$f(this, this._state);

		this.root._oncreate.push(() => {
			oncreate$8.call(this);
			this.fire("update", { changed: assignTrue({}, this._state), current: this._state });
		});

		if (options.target) {
			if (options.hydrate) throw new Error("options.hydrate only works if the component was compiled with the `hydratable: true` option");
			this._fragment.c();
			this._mount(options.target, options.anchor);

			flush(this);
		}
	}

	assign(App.prototype, protoDev);
	assign(App.prototype, methods$c);

	App.prototype._checkReadOnly = function _checkReadOnly(newState) {
		if ('categoryDropdown' in newState && !this._updatingReadonlyProperty) throw new Error("<App>: Cannot set read-only property 'categoryDropdown'");
	};

	App.prototype._recompute = function _recompute(changed, state) {
		if (changed.$categories) {
			if (this._differs(state.categoryDropdown, (state.categoryDropdown = categoryDropdown(state)))) changed.categoryDropdown = true;
		}
	};

	function Store(state, options) {
		this._handlers = {};
		this._dependents = [];

		this._computed = blankObject();
		this._sortedComputedProperties = [];

		this._state = assign({}, state);
		this._differs = options && options.immutable ? _differsImmutable : _differs;
	}

	assign(Store.prototype, {
		_add(component, props) {
			this._dependents.push({
				component: component,
				props: props
			});
		},

		_init(props) {
			const state = {};
			for (let i = 0; i < props.length; i += 1) {
				const prop = props[i];
				state['$' + prop] = this._state[prop];
			}
			return state;
		},

		_remove(component) {
			let i = this._dependents.length;
			while (i--) {
				if (this._dependents[i].component === component) {
					this._dependents.splice(i, 1);
					return;
				}
			}
		},

		_set(newState, changed) {
			const previous = this._state;
			this._state = assign(assign({}, previous), newState);

			for (let i = 0; i < this._sortedComputedProperties.length; i += 1) {
				this._sortedComputedProperties[i].update(this._state, changed);
			}

			this.fire('state', {
				changed,
				previous,
				current: this._state
			});

			this._dependents
				.filter(dependent => {
					const componentState = {};
					let dirty = false;

					for (let j = 0; j < dependent.props.length; j += 1) {
						const prop = dependent.props[j];
						if (prop in changed) {
							componentState['$' + prop] = this._state[prop];
							dirty = true;
						}
					}

					if (dirty) {
						dependent.component._stage(componentState);
						return true;
					}
				})
				.forEach(dependent => {
					dependent.component.set({});
				});

			this.fire('update', {
				changed,
				previous,
				current: this._state
			});
		},

		_sortComputedProperties() {
			const computed = this._computed;
			const sorted = this._sortedComputedProperties = [];
			const visited = blankObject();
			let currentKey;

			function visit(key) {
				const c = computed[key];

				if (c) {
					c.deps.forEach(dep => {
						if (dep === currentKey) {
							throw new Error(`Cyclical dependency detected between ${dep} <-> ${key}`);
						}

						visit(dep);
					});

					if (!visited[key]) {
						visited[key] = true;
						sorted.push(c);
					}
				}
			}

			for (const key in this._computed) {
				visit(currentKey = key);
			}
		},

		compute(key, deps, fn) {
			let value;

			const c = {
				deps,
				update: (state, changed, dirty) => {
					const values = deps.map(dep => {
						if (dep in changed) dirty = true;
						return state[dep];
					});

					if (dirty) {
						const newValue = fn.apply(null, values);
						if (this._differs(newValue, value)) {
							value = newValue;
							changed[key] = true;
							state[key] = value;
						}
					}
				}
			};

			this._computed[key] = c;
			this._sortComputedProperties();

			const state = assign({}, this._state);
			const changed = {};
			c.update(state, changed, true);
			this._set(state, changed);
		},

		fire,

		get,

		on,

		set(newState) {
			const oldState = this._state;
			const changed = this._changed = {};
			let dirty = false;

			for (const key in newState) {
				if (this._computed[key]) throw new Error(`'${key}' is a read-only computed property`);
				if (this._differs(newState[key], oldState[key])) changed[key] = dirty = true;
			}
			if (!dirty) return;

			this._set(newState, changed);
		}
	});

	// Request lib

	// Drupal apiBase for requests
	let apiBase = "";
	let generalApiBase = "";
	let rootApiBase = "";

	// Language list
	let langList = {};

	// Utilities
	async function ok(response, isAsync = true) {
	  if (isAsync && !response.ok) {
	    throw new Error("HTTP status: " + response.status);
	  }

	  let data;
	  if (isAsync) {
	    data = jSh.parseJSON(await response.text());
	  } else {
	    data = jSh.parseJSON(response);
	  }

	  if (data.error) {
	    throw new Error(data.error);
	  }

	  return data.data;
	}

	function string(thing) {
	  return JSON.stringify(thing);
	}

	// Request directives
	const request = {
	  setBase(url, _langList) {
	    apiBase = url[0];
	    generalApiBase = url[1];
	    rootApiBase = url[2];
	    langList = _langList;
	  },

	  async list() {
	    const response = await fetch(apiBase + "/listCategories", {
	      method: "POST",
	    });

	    return await ok(response);
	  },

	  async saveCategory(id, name, machineName$$1, available) {
	    const response = await fetch(apiBase + "/saveCategory", {
	      method: "POST",
	      body: string({
	        id,
	        name,
	        machineName: machineName$$1,
	        available,
	      }),
	    });

	    return (await ok(response)).id;
	  },

	  async saveCourse(
	      id,
	      category,
	      machineName$$1,
	      available,
	      data,
	    ) {
	    const response = await fetch(apiBase + "/saveCourse", {
	      method: "POST",
	      body: string({
	        pid: id,
	        type: category,
	        machineName: machineName$$1,
	        available: Number(available),
	        data,
	      }),
	    });

	    return (await ok(response));
	  },

	  async deleteCourse(pid) {
	    const response = await fetch(apiBase + "/deleteCourse", {
	      method: "POST",
	      body: string({
	        pid,
	      }),
	    });

	    return await ok(response);
	  },

	  async loadCourse(pid) {
	    const response = await fetch(apiBase + "/loadCourse", {
	      method: "POST",
	      body: string({
	        pid,
	      }),
	    });

	    return courseToJs(await ok(response), langList);
	  },

	  async themeThumbs() {
	    const response = await fetch(apiBase + "/themeThumbs", {
	      method: "POST",
	      body: string({}),
	    });

	    return await ok(response);
	  },

	  async themeCanvas(id) {
	    const response = await fetch(apiBase + "/themeCanvas", {
	      method: "POST",
	      body: string({
	        id,
	      }),
	    });

	    return await ok(response);
	  },

	  async saveFrontPageLayout(items) {
	    const response = await fetch(apiBase + "/saveFrontPageLayout", {
	      method: "POST",
	      body: string({
	        items,
	      }),
	    });

	    return await ok(response);
	  },

	  async loadBook(id) {
	    const response = await fetch(generalApiBase + "/loadBook", {
	      method: "POST",
	      body: string({
	        id,
	      }),
	    });

	    return await ok(response);
	  },

	  // Book viewer
	  async loadBookPage(page, id) {
	    const response = await fetch(generalApiBase + "/loadBookPage", {
	      method: "POST",
	      body: string({
	        page,
	        id,
	      }),
	    });

	    return await ok(response);
	  },

	  // Users
	  async searchUsers(name) {
	    const response = await fetch(apiBase + "/searchUsers", {
	      method: "POST",
	      body: string({
	        name,
	      }),
	    });

	    return await ok(response);
	  },

	  async userListNames(users) {
	    const response = await fetch(apiBase + "/userListNames", {
	      method: "POST",
	      body: string({
	        users,
	      }),
	    });

	    return await ok(response);
	  },

	  // Files
	  async getFileForm(curFileHash = null) {
	    const response = await fetch(apiBase + "/getFileForm", {
	      method: "POST",
	      body: string({
	        curFileHash,
	      }),
	    });

	    return await ok(response);
	  },

	  uploadFile(form, callback = null) {
	    const data = new FormData();
	    const keyMap = {
	      buildId: "form_build_id",
	      token: "form_token",
	      id: "form_id",
	      file: "files[file1]",
	    };

	    for (const keyBase of Object.keys(form)) {
	      if (keyBase !== "actual" && keyBase !== "old") {
	        const key = keyMap.hasOwnProperty(keyBase) ? keyMap[keyBase] : keyBase;

	        data.append(key, form[keyBase]);
	      }
	    }

	    const xhr = new XMLHttpRequest();

	    // Attach progress cb
	    if (callback) {
	      xhr.upload.addEventListener("progress", (e) => {
	        callback(e.loaded, e.total);
	      });
	    }

	    const promise = new Promise((resolve, reject) => {
	      xhr.onreadystatechange = function() {
	        if (xhr.readyState === 4) {
	          if (xhr.status === 200) {
	            resolve(ok(xhr.responseText, false));
	          } else {
	            reject(new Error("Failed with status code " + xhr.status));
	          }
	        }
	      };

	      xhr.open("POST", apiBase + "/uploadFile");
	      xhr.send(data);
	    });

	    return promise;
	  },
	};

	// Course admin logic

	function main(env) {
	  const locale = env.locale;
	  const themeIdMap = {};

	  for (const theme of env.themes) {
	    themeIdMap[theme.id] = theme;
	  }

	  const store = new Store({
	    language: locale._langauge,
	  	locale: locale,
	    prices: env.prices,
	    books: env.books,

	    fileFields: 0,
	    fileBase: env.fileBase,
	    basePath: env.apiBase[2],

	    themes: env.themes,
	    themeIdMap,
	    themeCanvasResources: env.themeCanvasResources,
	    themeMarkup: {},
	    currentCourse: 0,
	    currentThemeMap: {},
	    currentThemeVariables: {},

	    frontPageLayout: env.frontPageLayout,

	    savingCourse: false,
	    newCategoryName: "",
	    categories: [
	      // {
	      //   tid: 0,
	      //   name: "",
	      //   machineName: "",
	      //   available: false,
	      //   programs: [ // Courses
	      //     {
	      //       pid: 0,
	      //       name: "",
	      //     }
	      //   ],
	      // }
	    ],
	    localCourses: {
	      // 1: {
	      //   id: 1,
	      //   category: 1,
	      //   locale: {
	      //     en: {
	      //       id: null,
	      //       name: "",
	      //       slogan: "",
	      //       cpDescription: "",
	      //     }
	      //   }
	      // }
	    },
	  });
	  request.setBase(env.apiBase, env.locale._languageList);

	  const app = new App({
	    target: env.root,
	    store,
	    data: {
	      addingCategory: false,
	      course: {
	        name: "Course",
	        slogan: "Slogan",
	        cpDescription: "Description...",
	        list: null,
	      },
	      tabs: [
	        {id: "cp", text: locale.coursePage},
	        {id: "fp", text: locale.frontPage},
	        {id: "cl", text: locale.courseLevels},
	        {id: "cf", text: locale.courseFaq},
	        {id: "pr", text: locale.pricingPage},
	      ],
	      currentTab: "cp",
	    },
	  });

	  // Create viewer
	  const viewer = ihfathBookViewer({
	    root: env.viewerRoot,
	    pageBase: env.pageBase,
	  });

	  viewer.onpage((pageNum, bookId) => {
	    request
	      .loadBookPage(pageNum, bookId)
	      .then(data => {
	        viewer.openPage(pageNum, data.hash);
	      })
	      .catch((e) => {
	        console.error("Failed to load book page", e);
	      });
	  });

	  // Load categories and courses
	  request
	    .list()
	    .then(list => {
	      for (const category of list) {
	        category.available = +category.available;
	      }

	      store.set({
	        categories: list,
	      });

	      store.fire("categoriesloaded", {
	        list,
	      });

	      // Load theme thumbs after to prevent
	      // needless load on the server
	      loadThemeThumbs();
	    })
	    .catch(err => {
	      console.error("Error fetching list", err);
	    });

	  function loadThemeThumbs() {
	    request
	      .themeThumbs()
	      .then(thumbs => {
	        store.fire("thumbsloaded", {
	          thumbs,
	        });
	      })
	      .catch(err => {
	        console.error("Error loading theme thumbs", err);
	      });
	  }

	  // Add new category
	  store.on("newcategory", function() {
	    const { newCategoryName, categories } = this.get();
	    const mName = machineName(newCategoryName);
	    const available = 0; // Default

	    if (mName) {
	      request
	        .saveCategory(null, newCategoryName, mName, available)
	        .then(tid => {
	          categories.push({
	            tid,
	            name: mName,
	            available: available,
	            data: {
	              current: {
	                name: newCategoryName,
	                description: "",
	              },
	            },
	            programs: [],
	          });

	          this.set({
	            categories,
	          });
	        })
	        .catch(err => {
	          console.error("Error creating new category", err);
	        })
	        .finally(() => {
	          app.set({
	            addingCategory: false,
	            categoryPending: false,
	          });
	        });
	    }
	  });

	  store.on("setcategoryavailability", function({ tid, available }) {
	    const { categories } = this.get();
	    const category = categories.find(c => c.tid === tid);

	    available = Number(available);
	    if (category) {
	      request
	        .saveCategory(tid, category.data.current.name, category.name, available)
	        .then(tid => {
	          category.available = available;

	          this.set({
	            categories,
	          });
	        })
	        .catch(err => {
	          console.error("Failed to toggle category availability", err);
	        });
	    }
	  });

	  store.on("savecourse", function(courseId) {
	    const { localCourses, categories } = store.get();
	    const course = localCourses[courseId];

	    const name = course.locale[locale._langauge].name;
	    const mName = course.locale.en.slug; // FIXME: This language isn't guaranteed to exist
	    const { id, category, available, locale: localeData } = course;

	    if (mName) {
	      request
	        .saveCourse(id, category, mName, available, localeData)
	        .then(data => {
	          if (courseId === "new") {
	            localCourses[data.id] = course;
	            localCourses.new = null;
	          }

	          // Update levels and FAQ with the (maybe new) IDs
	          const langs = Object.keys(locale._languageList);
	          const infoData = {
	            levels: data.levels,
	            faq: data.faq,
	          };
	          for (const lang of langs) {
	            const infoLocaleData = localeData[lang];

	            if (infoLocaleData) {
	              for (const [infoType, ids] of Object.entries(infoData)) {
	                const infoDataData = infoLocaleData[infoType];
	                if (infoDataData) {
	                  for (let i=0; i<infoDataData.length; i++) {
	                    const curData = infoDataData[i];
	                    curData.id = +ids[i];
	                  }
	                }
	              }
	            }
	          }

	          // Update local category list
	          let curProgram;
	          for (const cat of categories) {
	            if (cat.tid == category) {
	              curProgram = {
	                pid: "new",
	                data: {
	                  current: {
	                    name,
	                  }
	                }
	              };

	              // See if the program already exists in the category, if it does
	              // replace the new placeholder data with the existing copy
	              for (const program of cat.programs) {
	                if (program.pid == data.id) {
	                  curProgram = program;
	                }
	              }

	              if (curProgram.pid === "new") {
	                cat.programs.push(curProgram);
	                curProgram.pid = data.id;
	              } else {
	                curProgram.data.current.name = name;
	              }

	              break;
	            }
	          }

	          store.set({
	            categories,
	            localCourses,
	            savingCourse: false,
	          });
	          store.fire("savedcourse");
	        })
	        .catch(e => {
	          console.error("Saving course failed", e);
	        });
	    }
	  });

	  store.on("loadcourse", function({ pid }) {
	    const { localCourses } = store.get();

	    if (pid in localCourses) {

	      store.set({
	        currentCourse: pid,
	      });
	      store.fire("selectcourse", { pid });
	      return;
	    }

	    request
	      .loadCourse(pid)
	      .then(course => {
	        localCourses[pid] = course;

	        store.set({
	          localCourses,
	          currentCourse: pid,
	        });
	        store.fire("selectcourse", { pid });
	      })
	      .catch(e => {
	        console.error("Loading course failed", e);
	      });
	  });

	  store.on("deletecourse", function({ pid }) {
	    request
	      .deleteCourse(pid)
	      .then(data => {
	        if (data.id == pid) {
	          const { localCourses, categories } = store.get();
	          let course = localCourses[pid];
	          let index = null;

	          if (!course) {
	            // Find course index since it hasn't been loaded yet FIXME: Don't need to find category
	            for (const category of categories) {
	              const checkIndex = category.programs.findIndex(c => c.pid == pid);

	              if (checkIndex !== -1) {
	                index = checkIndex;
	                course = {
	                  category: category.tid,
	                };

	                break;
	              }
	            }
	          }

	          const category = categories.find(c => c.tid == course.category);

	          if (index === null) {
	            index = category.programs.findIndex(c => c.pid == pid);
	          }

	          category.programs.splice(index, 1);
	          delete localCourses[pid];

	          store.set({
	            localCourses,
	            categories,
	          });
	        }
	        console.log("Deletion successful", data);
	      })
	      .catch(e => {
	        console.error("Deleting course failed", e);
	      });
	  });

	  store.on("loadtheme", function({ id, token }) {
	    const { themeMarkup } = store.get();

	    if (themeMarkup[id]) {
	      store.fire("themeloaded", {
	        id,
	        markup: themeMarkup[id],
	        token,
	      });
	    } else {
	      request
	        .themeCanvas(id)
	        .then(({ markup }) => {
	          themeMarkup[id] = markup;

	          store.set({
	            themeMarkup,
	          });
	          store.fire("themeloaded", {
	            id,
	            markup,
	            token,
	          });
	        })
	        .catch(err => {
	          console.error("Error loading theme", err);
	        });
	    }
	  });

	  store.on("savefrontpagelayout", ({ items }) => {
	    request
	      .saveFrontPageLayout(items)
	      .then(({ ids }) => {
	        store.fire("frontpagelayoutsaved", { ids });
	      })
	      .catch((e) => {
	        console.error("Failed to save front page layout", e);
	      });
	  });

	  // Load books
	  const loadedBooks = {
	    // id: bookData,
	  };
	  store.on("openbook", ({ bookId }) => {
	    function open(book) {
	      viewer.open(book);
	    }

	    if (loadedBooks[bookId]) {
	      open(loadedBooks[bookId]);
	    } else {
	      request
	        .loadBook(bookId)
	        .then(data => {
	          loadedBooks[bookId] = data.book;
	          open(data.book);
	        })
	        .catch(e => {
	          console.error("Failed to load book " + bookId, e);
	        });
	    }
	  });

	  store.on("requestfileform", function({ id, oldFileHash }) {
	    request
	      .getFileForm(oldFileHash)
	      .then(data => {
	        store.fire("requestfileformloaded", {
	          id,
	          form: data,
	        });
	      });
	  });

	  store.on("userlistnamesrequest", ({ users, fieldId }) => {
	    request
	      .userListNames(users)
	      .then(({ users }) => {
	        store.fire("userlistnamesresponse", { users, fieldId });
	      })
	      .catch(e => {
	        console.error("Failed to get userlist's names", e);
	      });
	  });

	  store.on("usersearch", ({ name, fieldId }) => {
	    request
	      .searchUsers(name)
	      .then(({ users }) => {
	        store.fire("searchresults", { users, fieldId });
	      })
	      .catch(e => {
	        console.error("Failed to load search results", e);
	      });
	  });

	  store.on("uploadfile", function({ id, form }) {
	    request
	      .uploadFile(form, (loaded, total) => {
	        store.fire("uploadfileprogress", {
	          id,
	          loaded,
	          total,
	        });
	      })
	      .then(data => {
	        store.fire("uploadedfile", {
	          id,
	          file: data,
	        });
	      })
	      .catch(e => {
	        console.error("Upload failed", e);
	      });
	  });
	}

	window.ihfathCourseAdmin = main;

}());
//# sourceMappingURL=course-admin.js.map
