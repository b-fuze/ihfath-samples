import svelte from 'rollup-plugin-svelte';
import resolve from 'rollup-plugin-node-resolve';
import commonjs from 'rollup-plugin-commonjs';
import { sass } from 'svelte-preprocess-sass';

const production = !process.env.ROLLUP_WATCH;

const buildConfig = (src, out, outCss = null) => ({
  input: src,
  output: {
    sourcemap: true,
    format: "iife",
    // name: "course_admin",
    file: out,
  },
  plugins: [
    svelte({
      // skipIntroByDefault: true,
      // nestedTransitions: true,
      preprocess: {
        style: sass({
          // No sass options to pass here...
        }, {
          name: "scss",
        }),
      },

      dev: !production,

      css: outCss ? ((css) => {
        css.write(outCss);
      }) : null,
    }),

    resolve(),
    commonjs(),
  ]
});

export default [
  buildConfig("src/main.js", "../dist/course-admin.js", "../dist/course-admin.css"),
  buildConfig("src/main.theme-canvas.js", "../dist/course-admin-theme-canvas.js", "../dist/course-admin-theme-canvas.css"),
];
