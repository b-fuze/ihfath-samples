// Course admin logic
import App from "./app/App.html";
import { Store } from 'svelte/store.js';
import { request } from "./request";
import { machineName } from "./utils";

function main(env) {
  const locale = env.locale;
  const themeIdMap = {};

  for (const theme of env.themes) {
    themeIdMap[theme.id] = theme;
  }

  const store = new Store({
    language: locale._langauge,
  	locale: locale,
    prices: env.prices,
    books: env.books,

    fileFields: 0,
    fileBase: env.fileBase,
    basePath: env.apiBase[2],

    themes: env.themes,
    themeIdMap,
    themeCanvasResources: env.themeCanvasResources,
    themeMarkup: {},
    currentCourse: 0,
    currentThemeMap: {},
    currentThemeVariables: {},

    frontPageLayout: env.frontPageLayout,

    savingCourse: false,
    newCategoryName: "",
    categories: [
      // {
      //   tid: 0,
      //   name: "",
      //   machineName: "",
      //   available: false,
      //   programs: [ // Courses
      //     {
      //       pid: 0,
      //       name: "",
      //     }
      //   ],
      // }
    ],
    localCourses: {
      // 1: {
      //   id: 1,
      //   category: 1,
      //   locale: {
      //     en: {
      //       id: null,
      //       name: "",
      //       slogan: "",
      //       cpDescription: "",
      //     }
      //   }
      // }
    },
  });
  request.setBase(env.apiBase, env.locale._languageList);

  const app = new App({
    target: env.root,
    store,
    data: {
      addingCategory: false,
      course: {
        name: "Course",
        slogan: "Slogan",
        cpDescription: "Description...",
        list: null,
      },
      tabs: [
        {id: "cp", text: locale.coursePage},
        {id: "fp", text: locale.frontPage},
        {id: "cl", text: locale.courseLevels},
        {id: "cf", text: locale.courseFaq},
        {id: "pr", text: locale.pricingPage},
      ],
      currentTab: "cp",
    },
  });

  // Create viewer
  const viewer = ihfathBookViewer({
    root: env.viewerRoot,
    pageBase: env.pageBase,
  });

  viewer.onpage((pageNum, bookId) => {
    request
      .loadBookPage(pageNum, bookId)
      .then(data => {
        viewer.openPage(pageNum, data.hash);
      })
      .catch((e) => {
        console.error("Failed to load book page", e);
      });
  });

  // Load categories and courses
  request
    .list()
    .then(list => {
      for (const category of list) {
        category.available = +category.available;
      }

      store.set({
        categories: list,
      });

      store.fire("categoriesloaded", {
        list,
      });

      // Load theme thumbs after to prevent
      // needless load on the server
      loadThemeThumbs();
    })
    .catch(err => {
      console.error("Error fetching list", err);
    });

  function loadThemeThumbs() {
    request
      .themeThumbs()
      .then(thumbs => {
        store.fire("thumbsloaded", {
          thumbs,
        });
      })
      .catch(err => {
        console.error("Error loading theme thumbs", err);
      });
  }

  // Add new category
  store.on("newcategory", function() {
    const { newCategoryName, categories } = this.get();
    const mName = machineName(newCategoryName);
    const available = 0; // Default

    if (mName) {
      request
        .saveCategory(null, newCategoryName, mName, available)
        .then(tid => {
          categories.push({
            tid,
            name: mName,
            available: available,
            data: {
              current: {
                name: newCategoryName,
                description: "",
              },
            },
            programs: [],
          });

          this.set({
            categories,
          });
        })
        .catch(err => {
          console.error("Error creating new category", err);
        })
        .finally(() => {
          app.set({
            addingCategory: false,
            categoryPending: false,
          });
        });
    }
  });

  store.on("setcategoryavailability", function({ tid, available }) {
    const { categories } = this.get();
    const category = categories.find(c => c.tid === tid);

    available = Number(available);
    if (category) {
      request
        .saveCategory(tid, category.data.current.name, category.name, available)
        .then(tid => {
          category.available = available;

          this.set({
            categories,
          });
        })
        .catch(err => {
          console.error("Failed to toggle category availability", err);
        });
    }
  });

  store.on("savecourse", function(courseId) {
    const { localCourses, categories } = store.get();
    const course = localCourses[courseId];

    const name = course.locale[locale._langauge].name;
    const mName = course.locale.en.slug; // FIXME: This language isn't guaranteed to exist
    const { id, category, available, locale: localeData } = course;

    if (mName) {
      request
        .saveCourse(id, category, mName, available, localeData)
        .then(data => {
          if (courseId === "new") {
            localCourses[data.id] = course;
            localCourses.new = null;
          }

          // Update levels and FAQ with the (maybe new) IDs
          const langs = Object.keys(locale._languageList);
          const infoData = {
            levels: data.levels,
            faq: data.faq,
          };
          for (const lang of langs) {
            const infoLocaleData = localeData[lang];

            if (infoLocaleData) {
              for (const [infoType, ids] of Object.entries(infoData)) {
                const infoDataData = infoLocaleData[infoType];
                if (infoDataData) {
                  for (let i=0; i<infoDataData.length; i++) {
                    const curData = infoDataData[i];
                    curData.id = +ids[i];
                  }
                }
              }
            }
          }

          // Update local category list
          let curProgram;
          for (const cat of categories) {
            if (cat.tid == category) {
              curProgram = {
                pid: "new",
                data: {
                  current: {
                    name,
                  }
                }
              };

              // See if the program already exists in the category, if it does
              // replace the new placeholder data with the existing copy
              for (const program of cat.programs) {
                if (program.pid == data.id) {
                  curProgram = program;
                }
              }

              if (curProgram.pid === "new") {
                cat.programs.push(curProgram);
                curProgram.pid = data.id;
              } else {
                curProgram.data.current.name = name;
              }

              break;
            }
          }

          store.set({
            categories,
            localCourses,
            savingCourse: false,
          });
          store.fire("savedcourse");
        })
        .catch(e => {
          console.error("Saving course failed", e);
        });
    }
  });

  store.on("loadcourse", function({ pid }) {
    const { localCourses } = store.get();

    if (pid in localCourses) {

      store.set({
        currentCourse: pid,
      });
      store.fire("selectcourse", { pid });
      return;
    }

    request
      .loadCourse(pid)
      .then(course => {
        localCourses[pid] = course;

        store.set({
          localCourses,
          currentCourse: pid,
        });
        store.fire("selectcourse", { pid });
      })
      .catch(e => {
        console.error("Loading course failed", e);
      });
  });

  store.on("deletecourse", function({ pid }) {
    request
      .deleteCourse(pid)
      .then(data => {
        if (data.id == pid) {
          const { localCourses, categories } = store.get();
          let course = localCourses[pid];
          let index = null;

          if (!course) {
            // Find course index since it hasn't been loaded yet FIXME: Don't need to find category
            for (const category of categories) {
              const checkIndex = category.programs.findIndex(c => c.pid == pid);

              if (checkIndex !== -1) {
                index = checkIndex;
                course = {
                  category: category.tid,
                };

                break;
              }
            }
          }

          const category = categories.find(c => c.tid == course.category);

          if (index === null) {
            index = category.programs.findIndex(c => c.pid == pid);
          }

          category.programs.splice(index, 1);
          delete localCourses[pid];

          store.set({
            localCourses,
            categories,
          });
        }
        console.log("Deletion successful", data);
      })
      .catch(e => {
        console.error("Deleting course failed", e);
      });
  });

  store.on("loadtheme", function({ id, token }) {
    const { themeMarkup } = store.get();

    if (themeMarkup[id]) {
      store.fire("themeloaded", {
        id,
        markup: themeMarkup[id],
        token,
      });
    } else {
      request
        .themeCanvas(id)
        .then(({ markup }) => {
          themeMarkup[id] = markup;

          store.set({
            themeMarkup,
          });
          store.fire("themeloaded", {
            id,
            markup,
            token,
          });
        })
        .catch(err => {
          console.error("Error loading theme", err);
        });
    }
  });

  store.on("savefrontpagelayout", ({ items }) => {
    request
      .saveFrontPageLayout(items)
      .then(({ ids }) => {
        store.fire("frontpagelayoutsaved", { ids });
      })
      .catch((e) => {
        console.error("Failed to save front page layout", e);
      });
  });

  // Load books
  const loadedBooks = {
    // id: bookData,
  };
  store.on("openbook", ({ bookId }) => {
    function open(book) {
      viewer.open(book);
    }

    if (loadedBooks[bookId]) {
      open(loadedBooks[bookId]);
    } else {
      request
        .loadBook(bookId)
        .then(data => {
          loadedBooks[bookId] = data.book;
          open(data.book);
        })
        .catch(e => {
          console.error("Failed to load book " + bookId, e);
        });
    }
  });

  store.on("requestfileform", function({ id, oldFileHash }) {
    request
      .getFileForm(oldFileHash)
      .then(data => {
        store.fire("requestfileformloaded", {
          id,
          form: data,
        });
      });
  });

  store.on("userlistnamesrequest", ({ users, fieldId }) => {
    request
      .userListNames(users)
      .then(({ users }) => {
        store.fire("userlistnamesresponse", { users, fieldId });
      })
      .catch(e => {
        console.error("Failed to get userlist's names", e);
      });
  });

  store.on("usersearch", ({ name, fieldId }) => {
    request
      .searchUsers(name)
      .then(({ users }) => {
        store.fire("searchresults", { users, fieldId });
      })
      .catch(e => {
        console.error("Failed to load search results", e);
      });
  });

  store.on("uploadfile", function({ id, form }) {
    request
      .uploadFile(form, (loaded, total) => {
        store.fire("uploadfileprogress", {
          id,
          loaded,
          total,
        });
      })
      .then(data => {
        store.fire("uploadedfile", {
          id,
          file: data,
        });
      })
      .catch(e => {
        console.error("Upload failed", e);
      });
  });
}

window.ihfathCourseAdmin = main;
