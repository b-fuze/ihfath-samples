import ThemeCanvas from "./theme-canvas/Canvas.html";
import { Store } from 'svelte/store.js';

window.addEventListener("DOMContentLoaded", () => {
  const store = new Store({
    locale: _ihfathLocale,
  });

  const app = new ThemeCanvas({
    target: document.body,
    store,
    data: {
      fields: [],
      baseWidth: _ihfathBasewidth,
    },
  });

  // Sync region mapping data to parent document
  app.on("update", ({ changed, current, previous }) => {
    if (changed.regionMapping) {
      msg("update", {
        themeMap: current.regionMapping,
      });
    }
  });

  // Respond to messages from the parent document
  window.addEventListener("message", (evt) => {
    const msg = evt.data;

    if (msg.action === "removetheme") {
      app.set({
        editing: false,
        selecting: false,
      });
    } else if (msg.action === "settheme") {
      const { markup, themeMap, fields } = msg;
      app.set({
        editing: true,
        selecting: false,
        markup,
        fields,
        regionMapping: themeMap,
      });
    }
  });

  // Done
  msg("canvasprepared");

  function msg(action, data) {
    window.parent.postMessage({
      action,
      ...data,
    }, "*");
  }
});
