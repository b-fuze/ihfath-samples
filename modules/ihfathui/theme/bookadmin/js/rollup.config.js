import svelte from 'rollup-plugin-svelte';
import resolve from 'rollup-plugin-node-resolve';
import commonjs from 'rollup-plugin-commonjs';
import sass from '@nsivertsen/svelte-preprocess-sass';

const production = !process.env.ROLLUP_WATCH;

const buildConfig = (src, out, outCss = null) => ({
  input: src,
  output: {
    sourcemap: true,
    format: "iife",
    file: out,
  },
  plugins: [
    svelte({
      // skipIntroByDefault: true,
      // nestedTransitions: true,
      preprocess: {
        style: sass(),
      },

      dev: !production,

      css: outCss ? ((css) => {
        css.write(outCss);
      }) : null,
    }),

    resolve(),
    commonjs(),
  ]
});

export default [
  buildConfig("src/main.js", "../dist/book-admin.js", "../dist/book-admin.css"),
  buildConfig("src/main.viewer.js", "../../../../js-common/book-viewer.js", "../../../../js-common/book-viewer.css"),
];
