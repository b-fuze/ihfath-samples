// @ts-check
// Book viewer logic
import App from "./app-viewer/App.html";

function main(env) {
  const locale = env.locale;
  const pageCache = {
    // "BOOK_ID": {
    //   "PAGE_NUM": "PAGE_ID",
    // },
  };

  const app = new App({
    target: env.root,
    data: {
      pageBase: env.pageBase,
      overlay: false,
      useOverlay: "useOverlay" in env ? env.useOverlay : true,
      headerHeight: "headerHeight" in env ? env.headerHeight : 60,
      showClose: "showClose" in env ? env.showClose : false,
    },
  });

  return {
    open(book, page = 1) {
      if (!pageCache[book.id]) {
        pageCache[book.id] = {};
      }

      // Add preview page (first page) to cache
      pageCache[book.id][1] = book.previewPage;

      const newState = {
        overlay: true,
        book,
      };

      if (page === 1) {
        Object.assign(newState, {
          currentPage: page,
          userInputPage: page,
          currentPageHash: book.previewPage,
        });
      } else {
        // Request page after this tick
        setTimeout(() => {
          app.fire("pagerequest", {
            page,
          });
        }, 0);
      }

      app.set(newState);
    },

    openPage(page, hash) {
      const { book } = app.get();
      pageCache[book.id][page] = hash;

      app.set({
        currentPage: page,
        userInputPage: page,
        currentPageHash: hash,
      });
    },

    setBooklist(bookList) {
      console.log("BOOKLIST", bookList);
      app.set({
        bookList,
      });
    },

    onrequest(cb) {
      app.on("bookrequest", cb);
    },

    onpage(cb) {
      app.on("pagerequest", function({ page }) {
        const { book } = app.get();
        const cache = pageCache[book.id];

        if (cache[page]) {
          book._lastPage = page;

          app.set({
            currentPage: page,
            userInputPage: page,
            currentPageHash: cache[page],
            book,
          });
        } else {
          cb(page, book.id);
        }
      });
    },

    onclose(cb) {
      app.on("close", cb);
    },

    onmove(cb) {
      app.on("moving", cb);
    },

    page(number) {
      app.setPage(number);
    },

    toggle() {
      const { overlay } = app.get();

      app.set({
        overlay: !overlay,
      });
    },

    show() {
      app.set({
        overlay: true,
      });
    },

    hide() {
      app.set({
        overlay: false,
      });
    }
  };
}

window.ihfathBookViewer = main;
