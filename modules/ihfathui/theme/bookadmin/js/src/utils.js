// Utils

export function machineName(name) {
  return name.trim().replace(/^\d+|[^a-z\d ]/ig, "").replace(/ +/g, " ").replace(/ /g, "_").toLowerCase();
}
