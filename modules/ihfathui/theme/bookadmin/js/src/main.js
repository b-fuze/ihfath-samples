// Book admin logic
import App from "./app/App.html";
import { Store } from 'svelte/store.js';
import { request } from "./request";
import { machineName } from "./utils";

// TODO: Generalize request API
function main(env) {
  const locale = env.locale;

  const store = new Store({
    language: locale._langauge,
  	locale: locale,
    pageBase: env.pageBase,

    fileFields: 0,
    fileBase: env.fileBase,
  });
  request.setBase(env.apiBase, env.locale._languageList);

  // Create app
  const app = new App({
    target: env.root,
    store,
    data: {
      books: env.books,
    },
  });

  // Create viewer
  const viewer = ihfathBookViewer({
    root: env.viewerRoot,
    pageBase: env.pageBase,
  });

  viewer.onpage((pageNum, bookId) => {
    request
      .loadBookPage(pageNum, bookId)
      .then(data => {
        viewer.openPage(pageNum, data.hash);
      })
      .catch((e) => {
        console.error("Failed to load book page", e);
      });
  });

  store.on("savebook", ({ book }) => {
    request
      .saveBook(book)
      .then((data) => {
        book.id = +data.book.id;
        book.bhid = data.book.bhid;
        book.pages = +data.book.pages;
        book.title = data.book.title;

        store.fire("booksaved", {
          book,
        });
      })
      .catch(e => {
        console.error("Failed saving book", book);
      });
  });

  store.on("deletebook", ({ book }) => {
    request
      .deleteBook(book)
      .then(data => {
        if (data.success) {
          store.fire("bookdeleted", {
            book,
          });
        }
      })
      .catch(e => {
        console.error("Failed to delete book", e, book);
      });
  });

  store.on("processpages", (book) => {
    processPages(book, store);
  });

  const loadedBooks = {
    // id: bookData,
  };
  store.on("openbook", ({ book }) => {
    const bookId = book.bhid;

    function open(book) {
      viewer.open(book);
    }

    if (loadedBooks[bookId]) {
      open(loadedBooks[bookId]);
    } else {
      request
        .loadBook(bookId)
        .then(data => {
          loadedBooks[bookId] = data.book;
          open(data.book);
        })
        .catch(e => {
          console.error("Failed to load book " + bookId, e);
        });
    }
  });

  // File chooser
  store.on("requestfileform", function({ id, oldFileHash }) {
    request
      .getFileForm(oldFileHash)
      .then(data => {
        store.fire("requestfileformloaded", {
          id,
          form: data,
        });
        console.log("FILEDATA", data);
      });
  });

  store.on("uploadfile", function({ id, form }) {
    request
      .uploadFile(form, (loaded, total) => {
        store.fire("uploadfileprogress", {
          id,
          loaded,
          total,
        });
      })
      .then(data => {
        store.fire("uploadedfile", {
          id,
          file: data,
        });
      })
      .catch(e => {
        console.error("Upload failed", e);
      });
  });
}

function processPages(book, store) {
  request.processPages(book, function(progress) {
    store.fire("bookprogress", {
      book,
      progress: progress.progress,
      previewPage: progress.preview_page,
    });
  }, function() {
    store.fire("bookfinished", {
      book,
    });
  });
}

window.ihfathBookAdmin = main;
