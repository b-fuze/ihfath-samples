// Request lib

// Drupal apiBase for requests
let apiBase = "";
let generalApiBase = "";

// Language list
let langList = {};

// Utilities
async function ok(response, isAsync = true) {
  if (isAsync && !response.ok) {
    throw new Error("HTTP status: " + response.status);
  }

  let data;
  if (isAsync) {
    data = jSh.parseJSON(await response.text());
  } else {
    data = jSh.parseJSON(response);
  }

  if (data.error) {
    throw new Error(data.error);
  }

  return data.data;
}

function string(thing) {
  return JSON.stringify(thing);
}

// Request directives
export const request = {
  setBase(url, _langList) {
    apiBase = url[0];
    generalApiBase = url[1];
    langList = _langList;
  },

  async saveBook(book) {
    const response = await fetch(apiBase + "/saveBook", {
      method: "POST",
      body: string({
        ...book,
      }),
    });

    return await ok(response);
  },

  async deleteBook(book) {
    const response = await fetch(apiBase + "/deleteBook", {
      method: "POST",
      body: string({
        id: book.id,
      }),
    });

    return await ok(response);
  },

  async processPages(book, onprogress, onfinish) {
    const evt = new EventSource(apiBase + "-stream/processPages?id=" + book.id);
    evt.onmessage = function(e) {
      const data = JSON.parse(e.data);

      if (!data.error) {
        onprogress(data.data);
      } else {
        // Error?
      }

      // Close connection
      if (data.data && data.data.close) {
        evt.close();
        onprogress && onprogress();
      }
    };
  },

  async loadBook(id) {
    const response = await fetch(generalApiBase + "/loadBook", {
      method: "POST",
      body: string({
        id,
      }),
    });

    return await ok(response);
  },

  async loadBookPage(page, id) {
    const response = await fetch(generalApiBase + "/loadBookPage", {
      method: "POST",
      body: string({
        page,
        id,
      }),
    });

    return await ok(response);
  },

  // Files
  async getFileForm(curFileHash = null) {
    const response = await fetch(apiBase + "/getFileForm", {
      method: "POST",
      body: string({
        curFileHash,
      }),
    });

    return await ok(response);
  },

  uploadFile(form, callback = null) {
    const data = new FormData();
    const keyMap = {
      buildId: "form_build_id",
      token: "form_token",
      id: "form_id",
      file: "files[file1]",
    };

    for (const keyBase of Object.keys(form)) {
      if (keyBase !== "actual" && keyBase !== "old") {
        const key = keyMap.hasOwnProperty(keyBase) ? keyMap[keyBase] : keyBase;

        data.append(key, form[keyBase]);
      }
    }

    const xhr = new XMLHttpRequest();

    // Attach progress cb
    if (callback) {
      xhr.upload.addEventListener("progress", (e) => {
        callback(e.loaded, e.total);
      });
    }

    const promise = new Promise((resolve, reject) => {
      xhr.onreadystatechange = function() {
        if (xhr.readyState === 4) {
          if (xhr.status === 200) {
            resolve(ok(xhr.responseText, false));
          } else {
            reject(new Error("Failed with status code " + xhr.status));
          }
        }
      };

      xhr.open("POST", apiBase + "/uploadFile");
      xhr.send(data);
    });

    return promise;
  },
};
