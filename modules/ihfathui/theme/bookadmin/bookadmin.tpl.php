<?php
// Vars...
global $language;

// Exposed
//    - $langs    All available languages
//    - $books    All Ihfath books

$base_path = drupal_get_path("module", "ihfathui") . "/";
$base_la_path = $base_path . "theme/bookadmin";

function jst(...$args) {
  return json_encode(t(...$args));
}

function jse(...$args) {
  return json_encode(...$args);
}

function mapBooksToJS($books) {
  return array_map(function($book) {
    return (object) array(
      "id" => (int) $book->bid,
      "bhid" => $book->bhid,
      "fileHashes" => $book->hashes,
      "title" => $book->field_book_title_field[LANGUAGE_NONE][0]["value"],
      "pages" => (int) $book->pages,
      "processed" => (int) $book->processed,
      "direction" => (int) $book->direction,
      "previewPage" => $book->preview_page,
    );
  }, array_values($books));
}

?>
<div class="app-root bulma-root">
  <!-- Svelte-populated -->
</div>

<script type="text/javascript">
  window.addEventListener("load", () => {
    // Make viewer root node and put it outside the page for fullscreen effect
    const page = document.querySelector("#page");
    const viewerRoot = document.createElement("div");
    page.parentNode.appendChild(viewerRoot);

    ihfathBookAdmin({
      apiBase: <?= jse(array(
        url("ihfath/ajax/bookadmin"),
        url("ihfath/ajax/general"),
      )) ?>,
      fileBase: <?= jse(url("sites/default/files/ihfathusercontent")) ?>,
      pageBase: <?= jse(url("sites/default/files/ihfathbooks")) ?>,
      books: <?= jse(mapBooksToJS($books)) ?>,
      root: document.querySelector(".app-root"),
      viewerRoot,

      locale: {
        _langauge: <?= jst($language->language) ?>,
        _languageList: <?= jse($langs) ?>,

        addNewBookButton: <?= jst("New book") ?>,
        saveBookButton: <?= jst("Save book") ?>,
        deleteBookButton: <?= jst("Delete book") ?>,
        cancelEditingBookButton: <?= jst("Cancel") ?>,
        searchInputPlaceholder: <?= jst("Search...") ?>,
        bookTitleInputPlaceholder: <?= jst("Book title...") ?>,
        invalidInputMessage: <?= jst("Title is required") ?>,

        directionSelector: <?= jst("Direction:") ?>,
        directionRtl: <?= jst("Right to left") ?>,
        directionLtr: <?= jst("Left to right") ?>,

        // File input selector
        fisNofileselectedLabel: <?= jst("No file selected") ?>,
        fisSelectfileLabel: <?= jst("select file") ?>,
        fisUploadingStatus: <?= jst("Uploading...") ?>,
      },
    });
  });
</script>
