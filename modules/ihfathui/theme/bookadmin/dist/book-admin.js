(function () {
	'use strict';

	function noop() {}

	function assign(tar, src) {
		for (var k in src) tar[k] = src[k];
		return tar;
	}

	function assignTrue(tar, src) {
		for (var k in src) tar[k] = 1;
		return tar;
	}

	function addLoc(element, file, line, column, char) {
		element.__svelte_meta = {
			loc: { file, line, column, char }
		};
	}

	function append(target, node) {
		target.appendChild(node);
	}

	function insert(target, node, anchor) {
		target.insertBefore(node, anchor);
	}

	function detachNode(node) {
		node.parentNode.removeChild(node);
	}

	function destroyEach(iterations, detach) {
		for (var i = 0; i < iterations.length; i += 1) {
			if (iterations[i]) iterations[i].d(detach);
		}
	}

	function createElement(name) {
		return document.createElement(name);
	}

	function createText(data) {
		return document.createTextNode(data);
	}

	function createComment() {
		return document.createComment('');
	}

	function addListener(node, event, handler, options) {
		node.addEventListener(event, handler, options);
	}

	function removeListener(node, event, handler, options) {
		node.removeEventListener(event, handler, options);
	}

	function setAttribute(node, attribute, value) {
		if (value == null) node.removeAttribute(attribute);
		else node.setAttribute(attribute, value);
	}

	function setData(text, data) {
		text.data = '' + data;
	}

	function setStyle(node, key, value) {
		node.style.setProperty(key, value);
	}

	function toggleClass(element, name, toggle) {
		element.classList.toggle(name, !!toggle);
	}

	function blankObject() {
		return Object.create(null);
	}

	function destroy(detach) {
		this.destroy = noop;
		this.fire('destroy');
		this.set = noop;

		this._fragment.d(detach !== false);
		this._fragment = null;
		this._state = {};
	}

	function destroyDev(detach) {
		destroy.call(this, detach);
		this.destroy = function() {
			console.warn('Component was already destroyed');
		};
	}

	function _differs(a, b) {
		return a != a ? b == b : a !== b || ((a && typeof a === 'object') || typeof a === 'function');
	}

	function _differsImmutable(a, b) {
		return a != a ? b == b : a !== b;
	}

	function fire(eventName, data) {
		var handlers =
			eventName in this._handlers && this._handlers[eventName].slice();
		if (!handlers) return;

		for (var i = 0; i < handlers.length; i += 1) {
			var handler = handlers[i];

			if (!handler.__calling) {
				try {
					handler.__calling = true;
					handler.call(this, data);
				} finally {
					handler.__calling = false;
				}
			}
		}
	}

	function flush(component) {
		component._lock = true;
		callAll(component._beforecreate);
		callAll(component._oncreate);
		callAll(component._aftercreate);
		component._lock = false;
	}

	function get() {
		return this._state;
	}

	function init(component, options) {
		component._handlers = blankObject();
		component._slots = blankObject();
		component._bind = options._bind;
		component._staged = {};

		component.options = options;
		component.root = options.root || component;
		component.store = options.store || component.root.store;

		if (!options.root) {
			component._beforecreate = [];
			component._oncreate = [];
			component._aftercreate = [];
		}
	}

	function on(eventName, handler) {
		var handlers = this._handlers[eventName] || (this._handlers[eventName] = []);
		handlers.push(handler);

		return {
			cancel: function() {
				var index = handlers.indexOf(handler);
				if (~index) handlers.splice(index, 1);
			}
		};
	}

	function set(newState) {
		this._set(assign({}, newState));
		if (this.root._lock) return;
		flush(this.root);
	}

	function _set(newState) {
		var oldState = this._state,
			changed = {},
			dirty = false;

		newState = assign(this._staged, newState);
		this._staged = {};

		for (var key in newState) {
			if (this._differs(newState[key], oldState[key])) changed[key] = dirty = true;
		}
		if (!dirty) return;

		this._state = assign(assign({}, oldState), newState);
		this._recompute(changed, this._state);
		if (this._bind) this._bind(changed, this._state);

		if (this._fragment) {
			this.fire("state", { changed: changed, current: this._state, previous: oldState });
			this._fragment.p(changed, this._state);
			this.fire("update", { changed: changed, current: this._state, previous: oldState });
		}
	}

	function _stage(newState) {
		assign(this._staged, newState);
	}

	function setDev(newState) {
		if (typeof newState !== 'object') {
			throw new Error(
				this._debugName + '.set was called without an object of data key-values to update.'
			);
		}

		this._checkReadOnly(newState);
		set.call(this, newState);
	}

	function callAll(fns) {
		while (fns && fns.length) fns.shift()();
	}

	function _mount(target, anchor) {
		this._fragment[this._fragment.i ? 'i' : 'm'](target, anchor || null);
	}

	function removeFromStore() {
		this.store._remove(this);
	}

	var protoDev = {
		destroy: destroyDev,
		get,
		fire,
		on,
		set: setDev,
		_recompute: noop,
		_set,
		_stage,
		_mount,
		_differs
	};

	/* src/app/FieldLabel.html generated by Svelte v2.15.3 */

	const file = "src/app/FieldLabel.html";

	function create_main_fragment(component, ctx) {
		var h2, text;

		return {
			c: function create() {
				h2 = createElement("h2");
				text = createText(ctx.label);
				h2.className = "small-label title is-7 has-text-grey-lighter svelte-2t3k6x";
				addLoc(h2, file, 0, 0, 0);
			},

			m: function mount(target, anchor) {
				insert(target, h2, anchor);
				append(h2, text);
			},

			p: function update(changed, ctx) {
				if (changed.label) {
					setData(text, ctx.label);
				}
			},

			d: function destroy$$1(detach) {
				if (detach) {
					detachNode(h2);
				}
			}
		};
	}

	function FieldLabel(options) {
		this._debugName = '<FieldLabel>';
		if (!options || (!options.target && !options.root)) {
			throw new Error("'target' is a required option");
		}

		init(this, options);
		this._state = assign({}, options.data);
		if (!('label' in this._state)) console.warn("<FieldLabel> was created without expected data property 'label'");
		this._intro = true;

		this._fragment = create_main_fragment(this, this._state);

		if (options.target) {
			if (options.hydrate) throw new Error("options.hydrate only works if the component was compiled with the `hydratable: true` option");
			this._fragment.c();
			this._mount(options.target, options.anchor);
		}
	}

	assign(FieldLabel.prototype, protoDev);

	FieldLabel.prototype._checkReadOnly = function _checkReadOnly(newState) {
	};

	/* src/app/FileField.html generated by Svelte v2.15.3 */

	function data() {
	  return {
	    id: -1,
	    value: "",
	    label: "",
	    fileSelected: false,
	    accept: null,

	    uploading: false,
	    progress: 0,

	    preview: true,
	    previewUrl: "",
	    maxWidth: 0,

	    fileHash: "",
	    fileData: {
	      name: "",
	      size: 0,
	    },
	    formData: {
	      buildId: "",
	      id: "", // Drupal 7 form_id
	      token: "",
	      file: null,
	    },
	  };
	}
	function humanBytes(bytes) {
	  const kb = 1024;
	  const mb = kb * 1024;
	  const gb = mb * 1024;

	  // Make sure bytes isn't a string
	  bytes = +bytes;

	  // FIXME: Probably shouldn't round, check this
	  if (bytes > gb) {
	    const value = bytes / gb;
	    return (Math.round(value * 10) / 10) + "GiB";
	  } else if (bytes > mb) {
	    const value = bytes / mb;
	    return (Math.round(value * 10) / 10) + "MiB";
	  } else if (bytes > kb) {
	    return Math.round(bytes / kb) + "KiB";
	  }

	  return bytes + "B";
	}
	var methods = {
	  requestForm() {
	    const { id, value } = this.get();
	    this.store.fire("requestfileform", {
	      id,
	      oldFileHash: value || null,
	    });
	  },

	  uploadFile() {
	    const { id, formData: form } = this.get();
	    const input = this.refs.input;
	    const file = input.files[0];

	    if (file) {
	      form.file = file;
	      this.store.fire("uploadfile", {
	        id,
	        form,
	      });
	      this.set({
	        uploading: true,
	        progress: 0,
	      });
	    }
	  },
	};

	function oncreate() {
	  let { fileFields } = this.store.get();

	  const id = ++fileFields;
	  const form = this.refs.form;

	  this.on("reset", () => {
	    form.reset();
	  });

	  // Adjust state when the value changes
	  this.on("state", ({ changed, current, previous }) => {
	    if (changed.value) {
	      if (current.value) {
	        this.requestForm();
	      } else {
	        this.set({
	          uploading: false,
	          fileSelected: false,
	        });
	      }
	    }
	  });

	  // Update form data
	  this.store.on("requestfileformloaded", ({ id: reqId, form }) => {
	    if (reqId === id) {
	      if (form.current) {
	        // Add current file data
	        const { fileHash, fileName, fileSize } = form.current;

	        this.set({
	          uploading: false,
	          fileSelected: true,
	          fileHash: fileHash,
	          fileData: {
	            name: fileName,
	            size: fileSize,
	          },
	          formData: form,
	        });
	      } else {
	        // Just supply the new form data
	        this.set({
	          formData: form,
	        });
	      }
	    }
	  });

	  // Show upload progress
	  this.store.on("uploadfileprogress", ({ id: reqId, loaded, total }) => {
	    if (reqId === id) {
	      this.set({
	        progress: Math.round(loaded / total * 100),
	      });
	    }
	  });

	  // Display newly uploaded file
	  this.store.on("uploadedfile", ({ id: reqId, file}) => {
	    if (reqId === id) {
	      const { fileHash, fileName, fileSize } = file;

	      this.set({
	        uploading: false,
	        fileSelected: true,
	        value: fileHash,
	        fileHash,
	        fileData: {
	          name: fileName,
	          size: fileSize,
	        },
	      });
	      this.fire("change");
	    }
	  });

	  // Set defaults
	  this.set({
	    id,
	  });
	  this.store.set({
	    fileFields,
	  });

	  // Request
	  this.requestForm();
	}
	const file$1 = "src/app/FileField.html";

	function create_main_fragment$1(component, ctx) {
		var text0, div, text1, form, input, div_style_value;

		var if_block0 = (ctx.label) && create_if_block_3(component, ctx);

		function select_block_type(ctx) {
			if (ctx.fileSelected) return create_if_block;
			if (ctx.uploading) return create_if_block_2;
			return create_else_block;
		}

		var current_block_type = select_block_type(ctx);
		var if_block1 = current_block_type(component, ctx);

		function change_handler(event) {
			component.uploadFile();
		}

		return {
			c: function create() {
				if (if_block0) if_block0.c();
				text0 = createText("\n");
				div = createElement("div");
				if_block1.c();
				text1 = createText("\n\n  ");
				form = createElement("form");
				input = createElement("input");
				addListener(input, "change", change_handler);
				setAttribute(input, "type", "file");
				input.className = "ihfath-file-input svelte-vl54kx";
				input.accept = ctx.accept;
				addLoc(input, file$1, 36, 4, 981);
				addLoc(form, file$1, 34, 2, 957);
				div.className = "box has-background-dark file-selector svelte-vl54kx";
				div.style.cssText = div_style_value = ctx.maxWidth ? `width: ${ctx.maxWidth}px;` : '';
				toggleClass(div, "has-file-selected", ctx.fileSelected);
				addLoc(div, file$1, 3, 0, 42);
			},

			m: function mount(target, anchor) {
				if (if_block0) if_block0.m(target, anchor);
				insert(target, text0, anchor);
				insert(target, div, anchor);
				if_block1.m(div, null);
				append(div, text1);
				append(div, form);
				append(form, input);
				component.refs.input = input;
				component.refs.form = form;
			},

			p: function update(changed, ctx) {
				if (ctx.label) {
					if (if_block0) {
						if_block0.p(changed, ctx);
					} else {
						if_block0 = create_if_block_3(component, ctx);
						if_block0.c();
						if_block0.m(text0.parentNode, text0);
					}
				} else if (if_block0) {
					if_block0.d(1);
					if_block0 = null;
				}

				if (current_block_type === (current_block_type = select_block_type(ctx)) && if_block1) {
					if_block1.p(changed, ctx);
				} else {
					if_block1.d(1);
					if_block1 = current_block_type(component, ctx);
					if_block1.c();
					if_block1.m(div, text1);
				}

				if (changed.accept) {
					input.accept = ctx.accept;
				}

				if ((changed.maxWidth) && div_style_value !== (div_style_value = ctx.maxWidth ? `width: ${ctx.maxWidth}px;` : '')) {
					div.style.cssText = div_style_value;
				}

				if (changed.fileSelected) {
					toggleClass(div, "has-file-selected", ctx.fileSelected);
				}
			},

			d: function destroy$$1(detach) {
				if (if_block0) if_block0.d(detach);
				if (detach) {
					detachNode(text0);
					detachNode(div);
				}

				if_block1.d();
				removeListener(input, "change", change_handler);
				if (component.refs.input === input) component.refs.input = null;
				if (component.refs.form === form) component.refs.form = null;
			}
		};
	}

	// (1:0) {#if label}
	function create_if_block_3(component, ctx) {

		var fieldlabel_initial_data = { label: ctx.label };
		var fieldlabel = new FieldLabel({
			root: component.root,
			store: component.store,
			data: fieldlabel_initial_data
		});

		return {
			c: function create() {
				fieldlabel._fragment.c();
			},

			m: function mount(target, anchor) {
				fieldlabel._mount(target, anchor);
			},

			p: function update(changed, ctx) {
				var fieldlabel_changes = {};
				if (changed.label) fieldlabel_changes.label = ctx.label;
				fieldlabel._set(fieldlabel_changes);
			},

			d: function destroy$$1(detach) {
				fieldlabel.destroy(detach);
			}
		};
	}

	// (24:2) {:else}
	function create_else_block(component, ctx) {
		var h5, span, text0_value = ctx.$locale.fisNofileselectedLabel, text0, text1, a, text2_value = ctx.$locale.fisSelectfileLabel, text2;

		return {
			c: function create() {
				h5 = createElement("h5");
				span = createElement("span");
				text0 = createText(text0_value);
				text1 = createText("\n      ");
				a = createElement("a");
				text2 = createText(text2_value);
				addLoc(span, file$1, 25, 6, 794);
				a.className = "has-text-link";
				addLoc(a, file$1, 28, 6, 862);
				h5.className = "title is-5 has-text-grey svelte-vl54kx";
				addLoc(h5, file$1, 24, 4, 750);
			},

			m: function mount(target, anchor) {
				insert(target, h5, anchor);
				append(h5, span);
				append(span, text0);
				append(h5, text1);
				append(h5, a);
				append(a, text2);
			},

			p: function update(changed, ctx) {
				if ((changed.$locale) && text0_value !== (text0_value = ctx.$locale.fisNofileselectedLabel)) {
					setData(text0, text0_value);
				}

				if ((changed.$locale) && text2_value !== (text2_value = ctx.$locale.fisSelectfileLabel)) {
					setData(text2, text2_value);
				}
			},

			d: function destroy$$1(detach) {
				if (detach) {
					detachNode(h5);
				}
			}
		};
	}

	// (19:21) 
	function create_if_block_2(component, ctx) {
		var div, span, text0_value = ctx.$locale.fisUploadingStatus, text0, text1, b, text2, text3;

		return {
			c: function create() {
				div = createElement("div");
				span = createElement("span");
				text0 = createText(text0_value);
				text1 = createText("\n      ");
				b = createElement("b");
				text2 = createText(ctx.progress);
				text3 = createText("%");
				addLoc(span, file$1, 20, 6, 636);
				b.className = "has-text-link svelte-vl54kx";
				addLoc(b, file$1, 21, 6, 684);
				div.className = "file-is-uploading has-text-grey-light svelte-vl54kx";
				addLoc(div, file$1, 19, 4, 578);
			},

			m: function mount(target, anchor) {
				insert(target, div, anchor);
				append(div, span);
				append(span, text0);
				append(div, text1);
				append(div, b);
				append(b, text2);
				append(b, text3);
			},

			p: function update(changed, ctx) {
				if ((changed.$locale) && text0_value !== (text0_value = ctx.$locale.fisUploadingStatus)) {
					setData(text0, text0_value);
				}

				if (changed.progress) {
					setData(text2, ctx.progress);
				}
			},

			d: function destroy$$1(detach) {
				if (detach) {
					detachNode(div);
				}
			}
		};
	}

	// (8:2) {#if fileSelected}
	function create_if_block(component, ctx) {
		var div, h5, text0_value = ctx.fileData.name, text0, text1, h6, text2_value = humanBytes(ctx.fileData.size), text2, text3, if_block_anchor;

		var if_block = (ctx.preview) && create_if_block_1(component, ctx);

		return {
			c: function create() {
				div = createElement("div");
				h5 = createElement("h5");
				text0 = createText(text0_value);
				text1 = createText("\n      ");
				h6 = createElement("h6");
				text2 = createText(text2_value);
				text3 = createText("\n\n    ");
				if (if_block) if_block.c();
				if_block_anchor = createComment();
				h5.className = "title is-5 ihfath-file-name svelte-vl54kx";
				addLoc(h5, file$1, 9, 6, 247);
				h6.className = "title is-6 ihfath-file-size svelte-vl54kx";
				addLoc(h6, file$1, 10, 6, 314);
				div.className = "file-details svelte-vl54kx";
				addLoc(div, file$1, 8, 4, 214);
			},

			m: function mount(target, anchor) {
				insert(target, div, anchor);
				append(div, h5);
				append(h5, text0);
				append(div, text1);
				append(div, h6);
				append(h6, text2);
				insert(target, text3, anchor);
				if (if_block) if_block.m(target, anchor);
				insert(target, if_block_anchor, anchor);
			},

			p: function update(changed, ctx) {
				if ((changed.fileData) && text0_value !== (text0_value = ctx.fileData.name)) {
					setData(text0, text0_value);
				}

				if ((changed.fileData) && text2_value !== (text2_value = humanBytes(ctx.fileData.size))) {
					setData(text2, text2_value);
				}

				if (ctx.preview) {
					if (if_block) {
						if_block.p(changed, ctx);
					} else {
						if_block = create_if_block_1(component, ctx);
						if_block.c();
						if_block.m(if_block_anchor.parentNode, if_block_anchor);
					}
				} else if (if_block) {
					if_block.d(1);
					if_block = null;
				}
			},

			d: function destroy$$1(detach) {
				if (detach) {
					detachNode(div);
					detachNode(text3);
				}

				if (if_block) if_block.d(detach);
				if (detach) {
					detachNode(if_block_anchor);
				}
			}
		};
	}

	// (14:4) {#if preview}
	function create_if_block_1(component, ctx) {
		var div;

		return {
			c: function create() {
				div = createElement("div");
				div.className = "file-thumb svelte-vl54kx";
				setStyle(div, "background-image", "url('" + (ctx.previewUrl || ctx.$fileBase + '/' + ctx.fileHash) + "')");
				addLoc(div, file$1, 14, 6, 423);
			},

			m: function mount(target, anchor) {
				insert(target, div, anchor);
			},

			p: function update(changed, ctx) {
				if (changed.previewUrl || changed.$fileBase || changed.fileHash) {
					setStyle(div, "background-image", "url('" + (ctx.previewUrl || ctx.$fileBase + '/' + ctx.fileHash) + "')");
				}
			},

			d: function destroy$$1(detach) {
				if (detach) {
					detachNode(div);
				}
			}
		};
	}

	function FileField(options) {
		this._debugName = '<FileField>';
		if (!options || (!options.target && !options.root)) {
			throw new Error("'target' is a required option");
		}
		if (!options.store) {
			throw new Error("<FileField> references store properties, but no store was provided");
		}

		init(this, options);
		this.refs = {};
		this._state = assign(assign(this.store._init(["fileBase","locale"]), data()), options.data);
		this.store._add(this, ["fileBase","locale"]);
		if (!('label' in this._state)) console.warn("<FileField> was created without expected data property 'label'");
		if (!('fileSelected' in this._state)) console.warn("<FileField> was created without expected data property 'fileSelected'");
		if (!('maxWidth' in this._state)) console.warn("<FileField> was created without expected data property 'maxWidth'");
		if (!('fileData' in this._state)) console.warn("<FileField> was created without expected data property 'fileData'");
		if (!('preview' in this._state)) console.warn("<FileField> was created without expected data property 'preview'");
		if (!('previewUrl' in this._state)) console.warn("<FileField> was created without expected data property 'previewUrl'");
		if (!('$fileBase' in this._state)) console.warn("<FileField> was created without expected data property '$fileBase'");
		if (!('fileHash' in this._state)) console.warn("<FileField> was created without expected data property 'fileHash'");
		if (!('uploading' in this._state)) console.warn("<FileField> was created without expected data property 'uploading'");
		if (!('$locale' in this._state)) console.warn("<FileField> was created without expected data property '$locale'");
		if (!('progress' in this._state)) console.warn("<FileField> was created without expected data property 'progress'");
		if (!('accept' in this._state)) console.warn("<FileField> was created without expected data property 'accept'");
		this._intro = true;

		this._handlers.destroy = [removeFromStore];

		this._fragment = create_main_fragment$1(this, this._state);

		this.root._oncreate.push(() => {
			oncreate.call(this);
			this.fire("update", { changed: assignTrue({}, this._state), current: this._state });
		});

		if (options.target) {
			if (options.hydrate) throw new Error("options.hydrate only works if the component was compiled with the `hydratable: true` option");
			this._fragment.c();
			this._mount(options.target, options.anchor);

			flush(this);
		}
	}

	assign(FileField.prototype, protoDev);
	assign(FileField.prototype, methods);

	FileField.prototype._checkReadOnly = function _checkReadOnly(newState) {
	};

	/* src/app/App.html generated by Svelte v2.15.3 */

	const bookModel = {
	  id: null,
	  bhid: null,
	  title: "",
	  previewPage: "",
	  direction: 1, // 0: ltr, 1: rtl
	  fileHashes: [
	    "",
	  ],

	  // Populated when the book gets "processed"
	  pages: 0,
	  processed: 0,
	  processing: false,
	  new: true,
	};

	function hasDocuments({ book }) {
		return !!book.fileHashes.join("");
	}

	function bookDirectionString({ book }) {
		return "" + book.direction;
	}

	function data$1() {
	  return {
	    editing: false,
	    confirming: false,
	    invalidInput: false,
	    newBookFileUrl: "",
	    removeIndex: -1,

	    book: bookModel,
	    books: [],
	  };
	}
	var methods$1 = {
	  edit(book = null) {
	    this.set({
	      editing: true,
	      book: book ? {
	        ...book,
	        fileHashes: book.fileHashes.slice(),
	      } : {
	        ...bookModel,
	        fileHashes: [
	          "",
	        ],
	      },
	    });
	  },

	  cancelEdit() {
	    this.set({
	      editing: false,
	    });
	  },

	  changeDirection(dir) {
	    const { book } = this.get();
	    book.direction = +dir;

	    this.set({
	      book,
	    });
	  },

	  addDocument() {
	    const { book } = this.get();

	    // Add new "empty" document
	    book.fileHashes.push("");

	    // Apply book
	    this.set({
	      book,
	    });
	  },

	  removeDocument(index) {
	    const { book } = this.get();

	    book.fileHashes.splice(index, 1);
	    this.set({
	      book,
	    });
	  },

	  save() {
	    const { book } = this.get();

	    if (!book.title.trim()) {
	      return this.set({
	        invalidInput: true,
	      });
	    }

	    this.store.fire("savebook", {
	      book,
	    });
	    this.set({
	      invalidInput: false,
	      editing: false,
	    });
	  },

	  deleteBook(book) {
	    this.set({
	      confirming: true,
	      removeIndex: book.index,
	    });
	  },

	  processPages(book) {
	    const { books } = this.get();
	    book.processing = true;
	    books[book.index] = book;

	    this.set({
	      books,
	    });
	    this.store.fire("processpages", book);
	  },

	  closeDialog() {
	    this.set({
	      confirming: false,
	    });
	  },

	  confirmDelete() {
	    const { books, removeIndex } = this.get();
	    const book = books[removeIndex];

	    this.set({
	      editing: false,
	      confirming: false,
	    });
	    this.store.fire("deletebook", {
	      book,
	    });
	  },

	  openBook(book) {
	    this.store.fire("openbook", {
	      book,
	    });
	  },
	};

	function oncreate$1() {
	  const { books } = this.get();

	  // Set indexes
	  for (let i=0; i<books.length; i++) {
	    const book = books[i];
	    book.index = i;
	  }

	  this.set({
	    books,
	  });

	  // A book was saved on the server succesfully
	  this.store.on("booksaved", ({ book }) => {
	    if (book.new) {
	      const { books } = this.get();

	      for (const book of books) {
	        book.index++;
	      }

	      book.new = false;
	      book.index = 0;
	      books.splice(0, 0, book);

	      this.set({
	        editing: false,
	        books,
	      });
	    }

	    this.processPages(book);
	  });

	  // Server deleted book successfully
	  this.store.on("bookdeleted", ({ book }) => {
	    const { books, book: curBook, editing } = this.get();
	    books.splice(book.index, 1);

	    for (let i=0; i<books.length; i++) {
	      books[i].index = i;
	    }

	    this.set({
	      books,
	      editing: book.id === curBook.id ? false : editing,
	    });
	  });

	  // Book decode process
	  this.store.on("bookprogress", ({ book, progress, previewPage }) => {
	    const { books } = this.get();

	    console.log("BOOOK PROGRESS", book);
	    book.processed = progress;

	    if (!book.previewPage && previewPage) {
	      book.previewPage = previewPage;
	    }

	    books[book.index] = book;

	    this.set({
	      books,
	    });
	  });

	  // Book finished processing
	  this.store.on("bookfinished", ({ book }) => {
	    const { books } = this.get();
	    book.processing = false;
	    books[book.index] = book;

	    this.set({
	      books,
	    });
	  });
	}
	const file$2 = "src/app/App.html";

	function click_handler_3(event) {
		const { component, ctx } = this._svelte;

		component.processPages(ctx.book);
	}

	function click_handler_2(event) {
		const { component, ctx } = this._svelte;

		component.openBook(ctx.book);
	}

	function click_handler_1(event) {
		const { component, ctx } = this._svelte;

		component.edit(ctx.book);
	}

	function get_each_context_1(ctx, list, i) {
		const child_ctx = Object.create(ctx);
		child_ctx.book = list[i];
		return child_ctx;
	}

	function click_handler(event) {
		const { component, ctx } = this._svelte;

		component.removeDocument(ctx.index);
	}

	function get_each_context(ctx, list, i) {
		const child_ctx = Object.create(ctx);
		child_ctx.file = list[i];
		child_ctx.index = i;
		return child_ctx;
	}

	function create_main_fragment$2(component, ctx) {
		var div5, div4, div1, div0, button0, i, text0, span, text1_value = ctx.$locale.addNewBookButton, text1, text2, div3, div2, input, input_placeholder_value, text3, text4, div6, text5, div15, div7, text6, div14, div8, h2, text8, div13, div10, div9, button1, text10, div12, div11, button2;

		function click_handler(event) {
			component.edit();
		}

		var if_block = (ctx.editing) && create_if_block_2$1(component, ctx);

		var each_value_1 = ctx.books;

		var each_blocks = [];

		for (var i_1 = 0; i_1 < each_value_1.length; i_1 += 1) {
			each_blocks[i_1] = create_each_block(component, get_each_context_1(ctx, each_value_1, i_1));
		}

		function click_handler_4(event) {
			component.closeDialog();
		}

		function click_handler_5(event) {
			component.confirmDelete();
		}

		return {
			c: function create() {
				div5 = createElement("div");
				div4 = createElement("div");
				div1 = createElement("div");
				div0 = createElement("div");
				button0 = createElement("button");
				i = createElement("i");
				text0 = createText("\n          ");
				span = createElement("span");
				text1 = createText(text1_value);
				text2 = createText("\n\n    ");
				div3 = createElement("div");
				div2 = createElement("div");
				input = createElement("input");
				text3 = createText("\n\n");
				if (if_block) if_block.c();
				text4 = createText("\n\n");
				div6 = createElement("div");

				for (var i_1 = 0; i_1 < each_blocks.length; i_1 += 1) {
					each_blocks[i_1].c();
				}

				text5 = createText("\n\n\n");
				div15 = createElement("div");
				div7 = createElement("div");
				text6 = createText("\n  ");
				div14 = createElement("div");
				div8 = createElement("div");
				h2 = createElement("h2");
				h2.textContent = "Are you sure you want to delete this book?";
				text8 = createText("\n    ");
				div13 = createElement("div");
				div10 = createElement("div");
				div9 = createElement("div");
				button1 = createElement("button");
				button1.textContent = "No";
				text10 = createText("\n      ");
				div12 = createElement("div");
				div11 = createElement("div");
				button2 = createElement("button");
				button2.textContent = "Yes, delete the book";
				i.className = "fa fa-upload svelte-66cuc0";
				addLoc(i, file$2, 7, 10, 195);
				addLoc(span, file$2, 8, 10, 234);
				addListener(button0, "click", click_handler);
				button0.className = "button is-link";
				addLoc(button0, file$2, 4, 8, 115);
				div0.className = "level-item svelte-66cuc0";
				addLoc(div0, file$2, 3, 6, 82);
				div1.className = "level-left svelte-66cuc0";
				addLoc(div1, file$2, 2, 4, 51);
				setAttribute(input, "type", "text");
				input.placeholder = input_placeholder_value = ctx.$locale.searchInputPlaceholder;
				input.className = "input";
				addLoc(input, file$2, 15, 8, 386);
				div2.className = "level-item svelte-66cuc0";
				addLoc(div2, file$2, 14, 6, 353);
				div3.className = "level-right svelte-66cuc0";
				addLoc(div3, file$2, 13, 4, 321);
				div4.className = "level svelte-66cuc0";
				addLoc(div4, file$2, 1, 2, 27);
				div5.className = "books-head";
				addLoc(div5, file$2, 0, 0, 0);
				div6.className = "books-list svelte-66cuc0";
				addLoc(div6, file$2, 108, 0, 2839);
				div7.className = "modal-background";
				addLoc(div7, file$2, 155, 2, 4087);
				h2.className = "title is-4";
				addLoc(h2, file$2, 158, 6, 4204);
				div8.className = "content";
				addLoc(div8, file$2, 157, 4, 4176);
				addListener(button1, "click", click_handler_4);
				button1.className = "button is-link";
				addLoc(button1, file$2, 165, 10, 4414);
				div9.className = "level-item svelte-66cuc0";
				addLoc(div9, file$2, 164, 8, 4379);
				div10.className = "level-left svelte-66cuc0";
				addLoc(div10, file$2, 163, 6, 4346);
				addListener(button2, "click", click_handler_5);
				button2.className = "button is-danger";
				addLoc(button2, file$2, 174, 10, 4633);
				div11.className = "level-item svelte-66cuc0";
				addLoc(div11, file$2, 173, 8, 4598);
				div12.className = "level-right svelte-66cuc0";
				addLoc(div12, file$2, 172, 6, 4564);
				div13.className = "level is-marginless svelte-66cuc0";
				addLoc(div13, file$2, 162, 4, 4306);
				div14.className = "box modal-content delete-dialog svelte-66cuc0";
				addLoc(div14, file$2, 156, 2, 4126);
				div15.className = "modal";
				toggleClass(div15, "is-active", ctx.confirming);
				addLoc(div15, file$2, 152, 0, 4032);
			},

			m: function mount(target, anchor) {
				insert(target, div5, anchor);
				append(div5, div4);
				append(div4, div1);
				append(div1, div0);
				append(div0, button0);
				append(button0, i);
				append(button0, text0);
				append(button0, span);
				append(span, text1);
				append(div4, text2);
				append(div4, div3);
				append(div3, div2);
				append(div2, input);
				insert(target, text3, anchor);
				if (if_block) if_block.m(target, anchor);
				insert(target, text4, anchor);
				insert(target, div6, anchor);

				for (var i_1 = 0; i_1 < each_blocks.length; i_1 += 1) {
					each_blocks[i_1].m(div6, null);
				}

				insert(target, text5, anchor);
				insert(target, div15, anchor);
				append(div15, div7);
				append(div15, text6);
				append(div15, div14);
				append(div14, div8);
				append(div8, h2);
				append(div14, text8);
				append(div14, div13);
				append(div13, div10);
				append(div10, div9);
				append(div9, button1);
				append(div13, text10);
				append(div13, div12);
				append(div12, div11);
				append(div11, button2);
			},

			p: function update(changed, ctx) {
				if ((changed.$locale) && text1_value !== (text1_value = ctx.$locale.addNewBookButton)) {
					setData(text1, text1_value);
				}

				if ((changed.$locale) && input_placeholder_value !== (input_placeholder_value = ctx.$locale.searchInputPlaceholder)) {
					input.placeholder = input_placeholder_value;
				}

				if (ctx.editing) {
					if (if_block) {
						if_block.p(changed, ctx);
					} else {
						if_block = create_if_block_2$1(component, ctx);
						if_block.c();
						if_block.m(text4.parentNode, text4);
					}
				} else if (if_block) {
					if_block.d(1);
					if_block = null;
				}

				if (changed.books || changed.$pageBase) {
					each_value_1 = ctx.books;

					for (var i_1 = 0; i_1 < each_value_1.length; i_1 += 1) {
						const child_ctx = get_each_context_1(ctx, each_value_1, i_1);

						if (each_blocks[i_1]) {
							each_blocks[i_1].p(changed, child_ctx);
						} else {
							each_blocks[i_1] = create_each_block(component, child_ctx);
							each_blocks[i_1].c();
							each_blocks[i_1].m(div6, null);
						}
					}

					for (; i_1 < each_blocks.length; i_1 += 1) {
						each_blocks[i_1].d(1);
					}
					each_blocks.length = each_value_1.length;
				}

				if (changed.confirming) {
					toggleClass(div15, "is-active", ctx.confirming);
				}
			},

			d: function destroy$$1(detach) {
				if (detach) {
					detachNode(div5);
				}

				removeListener(button0, "click", click_handler);
				if (detach) {
					detachNode(text3);
				}

				if (if_block) if_block.d(detach);
				if (detach) {
					detachNode(text4);
					detachNode(div6);
				}

				destroyEach(each_blocks, detach);

				if (detach) {
					detachNode(text5);
					detachNode(div15);
				}

				removeListener(button1, "click", click_handler_4);
				removeListener(button2, "click", click_handler_5);
			}
		};
	}

	// (25:0) {#if editing}
	function create_if_block_2$1(component, ctx) {
		var div6, text0, button0, i0, text1, text2, div0, text3, div1, span0, text4_value = ctx.$locale.directionSelector, text4, text5, select, option0, text6_value = ctx.$locale.directionRtl, text6, option1, text7_value = ctx.$locale.directionLtr, text7, select_value_value, text8, text9, div5, div3, text10, div2, button1, i1, text11, span1, text12_value = ctx.$locale.cancelEditingBookButton, text12, text13, div4;

		var if_block0 = (ctx.hasDocuments) && create_if_block_6(component, ctx);

		function click_handler(event) {
			component.addDocument();
		}

		var each_value = ctx.book.fileHashes;

		var each_blocks = [];

		for (var i_1 = 0; i_1 < each_value.length; i_1 += 1) {
			each_blocks[i_1] = create_each_block_1(component, get_each_context(ctx, each_value, i_1));
		}

		function change_handler(event) {
			component.changeDirection(event.target.value);
		}

		var if_block1 = (ctx.invalidInput) && create_if_block_5(component, ctx);

		var if_block2 = (ctx.hasDocuments) && create_if_block_4(component, ctx);

		function click_handler_1(event) {
			component.cancelEdit();
		}

		var if_block3 = (ctx.book.id && !ctx.book.processing) && create_if_block_3$1(component, ctx);

		return {
			c: function create() {
				div6 = createElement("div");
				if (if_block0) if_block0.c();
				text0 = createText("\n\n    ");
				button0 = createElement("button");
				i0 = createElement("i");
				text1 = createText("\n      Add document");
				text2 = createText("\n\n    ");
				div0 = createElement("div");

				for (var i_1 = 0; i_1 < each_blocks.length; i_1 += 1) {
					each_blocks[i_1].c();
				}

				text3 = createText("\n\n    ");
				div1 = createElement("div");
				span0 = createElement("span");
				text4 = createText(text4_value);
				text5 = createText("\n      ");
				select = createElement("select");
				option0 = createElement("option");
				text6 = createText(text6_value);
				option1 = createElement("option");
				text7 = createText(text7_value);
				text8 = createText("\n\n    ");
				if (if_block1) if_block1.c();
				text9 = createText("\n\n    ");
				div5 = createElement("div");
				div3 = createElement("div");
				if (if_block2) if_block2.c();
				text10 = createText("\n        ");
				div2 = createElement("div");
				button1 = createElement("button");
				i1 = createElement("i");
				text11 = createText("\n            ");
				span1 = createElement("span");
				text12 = createText(text12_value);
				text13 = createText("\n      ");
				div4 = createElement("div");
				if (if_block3) if_block3.c();
				i0.className = "fa fa-plus svelte-66cuc0";
				addLoc(i0, file$2, 37, 6, 844);
				addListener(button0, "click", click_handler);
				button0.className = "button is-link";
				addLoc(button0, file$2, 34, 4, 769);
				div0.className = "content";
				addLoc(div0, file$2, 41, 4, 909);
				addLoc(span0, file$2, 58, 6, 1379);
				option0.__value = "1";
				option0.value = option0.__value;
				addLoc(option0, file$2, 62, 8, 1535);
				option1.__value = "0";
				option1.value = option1.__value;
				addLoc(option1, file$2, 63, 8, 1593);
				addListener(select, "change", change_handler);
				addLoc(select, file$2, 59, 6, 1426);
				div1.className = "content";
				addLoc(div1, file$2, 57, 4, 1351);
				i1.className = "fa fa-times svelte-66cuc0";
				addLoc(i1, file$2, 87, 12, 2288);
				addLoc(span1, file$2, 88, 12, 2328);
				addListener(button1, "click", click_handler_1);
				button1.className = "button is-link save-book-btn";
				addLoc(button1, file$2, 84, 10, 2182);
				div2.className = "level-item svelte-66cuc0";
				addLoc(div2, file$2, 83, 8, 2147);
				div3.className = "level-left svelte-66cuc0";
				addLoc(div3, file$2, 72, 6, 1798);
				div4.className = "level-right svelte-66cuc0";
				addLoc(div4, file$2, 92, 6, 2429);
				div5.className = "level is-marginless svelte-66cuc0";
				addLoc(div5, file$2, 71, 4, 1758);
				div6.className = "book-editor svelte-66cuc0";
				addLoc(div6, file$2, 25, 2, 552);
			},

			m: function mount(target, anchor) {
				insert(target, div6, anchor);
				if (if_block0) if_block0.m(div6, null);
				append(div6, text0);
				append(div6, button0);
				append(button0, i0);
				append(button0, text1);
				append(div6, text2);
				append(div6, div0);

				for (var i_1 = 0; i_1 < each_blocks.length; i_1 += 1) {
					each_blocks[i_1].m(div0, null);
				}

				append(div6, text3);
				append(div6, div1);
				append(div1, span0);
				append(span0, text4);
				append(div1, text5);
				append(div1, select);
				append(select, option0);
				append(option0, text6);
				append(select, option1);
				append(option1, text7);

				select_value_value = ctx.bookDirectionString;
				for (var i = 0; i < select.options.length; i += 1) {
					var option = select.options[i];

					if (option.__value === select_value_value) {
						option.selected = true;
						break;
					}
				}

				append(div6, text8);
				if (if_block1) if_block1.m(div6, null);
				append(div6, text9);
				append(div6, div5);
				append(div5, div3);
				if (if_block2) if_block2.m(div3, null);
				append(div3, text10);
				append(div3, div2);
				append(div2, button1);
				append(button1, i1);
				append(button1, text11);
				append(button1, span1);
				append(span1, text12);
				append(div5, text13);
				append(div5, div4);
				if (if_block3) if_block3.m(div4, null);
			},

			p: function update(changed, ctx) {
				if (ctx.hasDocuments) {
					if (if_block0) {
						if_block0.p(changed, ctx);
					} else {
						if_block0 = create_if_block_6(component, ctx);
						if_block0.c();
						if_block0.m(div6, text0);
					}
				} else if (if_block0) {
					if_block0.d(1);
					if_block0 = null;
				}

				if (changed.book) {
					each_value = ctx.book.fileHashes;

					for (var i_1 = 0; i_1 < each_value.length; i_1 += 1) {
						const child_ctx = get_each_context(ctx, each_value, i_1);

						if (each_blocks[i_1]) {
							each_blocks[i_1].p(changed, child_ctx);
						} else {
							each_blocks[i_1] = create_each_block_1(component, child_ctx);
							each_blocks[i_1].c();
							each_blocks[i_1].m(div0, null);
						}
					}

					for (; i_1 < each_blocks.length; i_1 += 1) {
						each_blocks[i_1].d(1);
					}
					each_blocks.length = each_value.length;
				}

				if ((changed.$locale) && text4_value !== (text4_value = ctx.$locale.directionSelector)) {
					setData(text4, text4_value);
				}

				if ((changed.$locale) && text6_value !== (text6_value = ctx.$locale.directionRtl)) {
					setData(text6, text6_value);
				}

				if ((changed.$locale) && text7_value !== (text7_value = ctx.$locale.directionLtr)) {
					setData(text7, text7_value);
				}

				if ((changed.bookDirectionString) && select_value_value !== (select_value_value = ctx.bookDirectionString)) {
					for (var i = 0; i < select.options.length; i += 1) {
						var option = select.options[i];

						if (option.__value === select_value_value) {
							option.selected = true;
							break;
						}
					}
				}

				if (ctx.invalidInput) {
					if (if_block1) {
						if_block1.p(changed, ctx);
					} else {
						if_block1 = create_if_block_5(component, ctx);
						if_block1.c();
						if_block1.m(div6, text9);
					}
				} else if (if_block1) {
					if_block1.d(1);
					if_block1 = null;
				}

				if (ctx.hasDocuments) {
					if (if_block2) {
						if_block2.p(changed, ctx);
					} else {
						if_block2 = create_if_block_4(component, ctx);
						if_block2.c();
						if_block2.m(div3, text10);
					}
				} else if (if_block2) {
					if_block2.d(1);
					if_block2 = null;
				}

				if ((changed.$locale) && text12_value !== (text12_value = ctx.$locale.cancelEditingBookButton)) {
					setData(text12, text12_value);
				}

				if (ctx.book.id && !ctx.book.processing) {
					if (if_block3) {
						if_block3.p(changed, ctx);
					} else {
						if_block3 = create_if_block_3$1(component, ctx);
						if_block3.c();
						if_block3.m(div4, null);
					}
				} else if (if_block3) {
					if_block3.d(1);
					if_block3 = null;
				}
			},

			d: function destroy$$1(detach) {
				if (detach) {
					detachNode(div6);
				}

				if (if_block0) if_block0.d();
				removeListener(button0, "click", click_handler);

				destroyEach(each_blocks, detach);

				removeListener(select, "change", change_handler);
				if (if_block1) if_block1.d();
				if (if_block2) if_block2.d();
				removeListener(button1, "click", click_handler_1);
				if (if_block3) if_block3.d();
			}
		};
	}

	// (27:4) {#if hasDocuments}
	function create_if_block_6(component, ctx) {
		var input, input_updating = false, input_placeholder_value;

		function input_input_handler() {
			input_updating = true;
			ctx.book.title = input.value;
			component.set({ book: ctx.book });
			input_updating = false;
		}

		return {
			c: function create() {
				input = createElement("input");
				addListener(input, "input", input_input_handler);
				input.className = "input book-title svelte-66cuc0";
				input.placeholder = input_placeholder_value = ctx.$locale.bookTitleInputPlaceholder;
				setAttribute(input, "type", "text");
				addLoc(input, file$2, 27, 6, 607);
			},

			m: function mount(target, anchor) {
				insert(target, input, anchor);

				input.value = ctx.book.title;
			},

			p: function update(changed, _ctx) {
				ctx = _ctx;
				if (!input_updating && changed.book) input.value = ctx.book.title;
				if ((changed.$locale) && input_placeholder_value !== (input_placeholder_value = ctx.$locale.bookTitleInputPlaceholder)) {
					input.placeholder = input_placeholder_value;
				}
			},

			d: function destroy$$1(detach) {
				if (detach) {
					detachNode(input);
				}

				removeListener(input, "input", input_input_handler);
			}
		};
	}

	// (43:6) {#each book.fileHashes as file, index}
	function create_each_block_1(component, ctx) {
		var div, filefield_updating = {}, text, button;

		var filefield_initial_data = {
		 	preview: false,
		 	maxWidth: 400
		 };
		if (ctx.book.fileHashes[ctx.index] !== void 0) {
			filefield_initial_data.value = ctx.book.fileHashes[ctx.index];
			filefield_updating.value = true;
		}
		var filefield = new FileField({
			root: component.root,
			store: component.store,
			data: filefield_initial_data,
			_bind(changed, childState) {
				var newState = {};
				if (!filefield_updating.value && changed.value) {
					ctx.book.fileHashes[ctx.index] = childState.value;
					newState.book = ctx.book;
				}
				component._set(newState);
				filefield_updating = {};
			}
		});

		component.root._beforecreate.push(() => {
			filefield._bind({ value: 1 }, filefield.get());
		});

		return {
			c: function create() {
				div = createElement("div");
				filefield._fragment.c();
				text = createText("\n          ");
				button = createElement("button");
				button.textContent = "Remove document";
				button._svelte = { component, ctx };

				addListener(button, "click", click_handler);
				button.className = "button is-danger is-rounded svelte-66cuc0";
				addLoc(button, file$2, 48, 10, 1156);
				div.className = "container doc-file-row svelte-66cuc0";
				addLoc(div, file$2, 43, 8, 984);
			},

			m: function mount(target, anchor) {
				insert(target, div, anchor);
				filefield._mount(div, null);
				append(div, text);
				append(div, button);
			},

			p: function update(changed, _ctx) {
				ctx = _ctx;
				var filefield_changes = {};
				if (!filefield_updating.value && changed.book) {
					filefield_changes.value = ctx.book.fileHashes[ctx.index];
					filefield_updating.value = ctx.book.fileHashes[ctx.index] !== void 0;
				}
				filefield._set(filefield_changes);
				filefield_updating = {};

				button._svelte.ctx = ctx;
			},

			d: function destroy$$1(detach) {
				if (detach) {
					detachNode(div);
				}

				filefield.destroy();
				removeListener(button, "click", click_handler);
			}
		};
	}

	// (68:4) {#if invalidInput}
	function create_if_block_5(component, ctx) {
		var span, text_value = ctx.$locale.invalidInputMessage, text;

		return {
			c: function create() {
				span = createElement("span");
				text = createText(text_value);
				addLoc(span, file$2, 68, 6, 1700);
			},

			m: function mount(target, anchor) {
				insert(target, span, anchor);
				append(span, text);
			},

			p: function update(changed, ctx) {
				if ((changed.$locale) && text_value !== (text_value = ctx.$locale.invalidInputMessage)) {
					setData(text, text_value);
				}
			},

			d: function destroy$$1(detach) {
				if (detach) {
					detachNode(span);
				}
			}
		};
	}

	// (74:8) {#if hasDocuments}
	function create_if_block_4(component, ctx) {
		var div, button, i, text0, span, text1_value = ctx.$locale.saveBookButton, text1;

		function click_handler_1(event) {
			component.save();
		}

		return {
			c: function create() {
				div = createElement("div");
				button = createElement("button");
				i = createElement("i");
				text0 = createText("\n              ");
				span = createElement("span");
				text1 = createText(text1_value);
				i.className = "fa fa-floppy-o svelte-66cuc0";
				addLoc(i, file$2, 78, 14, 2003);
				addLoc(span, file$2, 79, 14, 2048);
				addListener(button, "click", click_handler_1);
				button.className = "button is-link save-book-btn";
				addLoc(button, file$2, 75, 12, 1897);
				div.className = "level-item svelte-66cuc0";
				addLoc(div, file$2, 74, 10, 1860);
			},

			m: function mount(target, anchor) {
				insert(target, div, anchor);
				append(div, button);
				append(button, i);
				append(button, text0);
				append(button, span);
				append(span, text1);
			},

			p: function update(changed, ctx) {
				if ((changed.$locale) && text1_value !== (text1_value = ctx.$locale.saveBookButton)) {
					setData(text1, text1_value);
				}
			},

			d: function destroy$$1(detach) {
				if (detach) {
					detachNode(div);
				}

				removeListener(button, "click", click_handler_1);
			}
		};
	}

	// (94:8) {#if book.id && !book.processing}
	function create_if_block_3$1(component, ctx) {
		var div, button, i, text0, span, text1_value = ctx.$locale.deleteBookButton, text1;

		function click_handler_1(event) {
			component.deleteBook(ctx.book);
		}

		return {
			c: function create() {
				div = createElement("div");
				button = createElement("button");
				i = createElement("i");
				text0 = createText("\n              ");
				span = createElement("span");
				text1 = createText(text1_value);
				i.className = "fa fa-trash-o svelte-66cuc0";
				addLoc(i, file$2, 98, 14, 2662);
				addLoc(span, file$2, 99, 14, 2706);
				addListener(button, "click", click_handler_1);
				button.className = "button is-danger save-book-btn";
				addLoc(button, file$2, 95, 12, 2544);
				div.className = "level-item svelte-66cuc0";
				addLoc(div, file$2, 94, 10, 2507);
			},

			m: function mount(target, anchor) {
				insert(target, div, anchor);
				append(div, button);
				append(button, i);
				append(button, text0);
				append(button, span);
				append(span, text1);
			},

			p: function update(changed, _ctx) {
				ctx = _ctx;
				if ((changed.$locale) && text1_value !== (text1_value = ctx.$locale.deleteBookButton)) {
					setData(text1, text1_value);
				}
			},

			d: function destroy$$1(detach) {
				if (detach) {
					detachNode(div);
				}

				removeListener(button, "click", click_handler_1);
			}
		};
	}

	// (129:8) {#if book.processed < book.pages}
	function create_if_block$1(component, ctx) {
		var div, text0, span, text1_value = ctx.book.processed, text1, text2, text3_value = ctx.book.pages, text3;

		var if_block = (!ctx.book.processing) && create_if_block_1$1(component, ctx);

		return {
			c: function create() {
				div = createElement("div");
				if (if_block) if_block.c();
				text0 = createText("\n\n            ");
				span = createElement("span");
				text1 = createText(text1_value);
				text2 = createText(" / ");
				text3 = createText(text3_value);
				span.className = "svelte-66cuc0";
				addLoc(span, file$2, 139, 12, 3727);
				div.className = "page-processed svelte-66cuc0";
				addLoc(div, file$2, 129, 10, 3409);
			},

			m: function mount(target, anchor) {
				insert(target, div, anchor);
				if (if_block) if_block.m(div, null);
				append(div, text0);
				append(div, span);
				append(span, text1);
				append(span, text2);
				append(span, text3);
			},

			p: function update(changed, ctx) {
				if (!ctx.book.processing) {
					if (if_block) {
						if_block.p(changed, ctx);
					} else {
						if_block = create_if_block_1$1(component, ctx);
						if_block.c();
						if_block.m(div, text0);
					}
				} else if (if_block) {
					if_block.d(1);
					if_block = null;
				}

				if ((changed.books) && text1_value !== (text1_value = ctx.book.processed)) {
					setData(text1, text1_value);
				}

				if ((changed.books) && text3_value !== (text3_value = ctx.book.pages)) {
					setData(text3, text3_value);
				}
			},

			d: function destroy$$1(detach) {
				if (detach) {
					detachNode(div);
				}

				if (if_block) if_block.d();
			}
		};
	}

	// (131:12) {#if !book.processing}
	function create_if_block_1$1(component, ctx) {
		var span, i;

		return {
			c: function create() {
				span = createElement("span");
				i = createElement("i");
				i.className = "fa fa-refresh svelte-66cuc0";
				addLoc(i, file$2, 135, 16, 3644);

				span._svelte = { component, ctx };

				addListener(span, "click", click_handler_3);
				span.className = "finish-processing-btn svelte-66cuc0";
				span.title = "Finish processing";
				addLoc(span, file$2, 131, 14, 3487);
			},

			m: function mount(target, anchor) {
				insert(target, span, anchor);
				append(span, i);
			},

			p: function update(changed, _ctx) {
				ctx = _ctx;
				span._svelte.ctx = ctx;
			},

			d: function destroy$$1(detach) {
				if (detach) {
					detachNode(span);
				}

				removeListener(span, "click", click_handler_3);
			}
		};
	}

	// (110:2) {#each books as book}
	function create_each_block(component, ctx) {
		var div5, div1, div0, button0, text1, button1, text3, div1_style_value, text4, div4, div2, text5_value = ctx.book.title, text5, text6, div3, text7_value = ctx.book.pages + "p", text7;

		var if_block = (ctx.book.processed < ctx.book.pages) && create_if_block$1(component, ctx);

		return {
			c: function create() {
				div5 = createElement("div");
				div1 = createElement("div");
				div0 = createElement("div");
				button0 = createElement("button");
				button0.textContent = "Edit";
				text1 = createText("\n          ");
				button1 = createElement("button");
				button1.textContent = "Open";
				text3 = createText("\n\n        ");
				if (if_block) if_block.c();
				text4 = createText("\n      ");
				div4 = createElement("div");
				div2 = createElement("div");
				text5 = createText(text5_value);
				text6 = createText("\n        ");
				div3 = createElement("div");
				text7 = createText(text7_value);
				button0._svelte = { component, ctx };

				addListener(button0, "click", click_handler_1);
				button0.className = "button edit is-link svelte-66cuc0";
				addLoc(button0, file$2, 116, 10, 3084);

				button1._svelte = { component, ctx };

				addListener(button1, "click", click_handler_2);
				button1.className = "button open is-success svelte-66cuc0";
				addLoc(button1, file$2, 121, 10, 3214);
				div0.className = "book-actions svelte-66cuc0";
				addLoc(div0, file$2, 115, 8, 3047);
				div1.className = "preview svelte-66cuc0";
				div1.style.cssText = div1_style_value = `background-image: url('${ctx.$pageBase}/${ctx.book.previewPage}');`;
				addLoc(div1, file$2, 112, 6, 2932);
				div2.className = "book-title svelte-66cuc0";
				addLoc(div2, file$2, 144, 8, 3852);
				div3.className = "pages has-text-link svelte-66cuc0";
				addLoc(div3, file$2, 145, 8, 3903);
				div4.className = "details svelte-66cuc0";
				addLoc(div4, file$2, 143, 6, 3822);
				div5.className = "book-card box svelte-66cuc0";
				addLoc(div5, file$2, 110, 4, 2892);
			},

			m: function mount(target, anchor) {
				insert(target, div5, anchor);
				append(div5, div1);
				append(div1, div0);
				append(div0, button0);
				append(div0, text1);
				append(div0, button1);
				append(div1, text3);
				if (if_block) if_block.m(div1, null);
				append(div5, text4);
				append(div5, div4);
				append(div4, div2);
				append(div2, text5);
				append(div4, text6);
				append(div4, div3);
				append(div3, text7);
			},

			p: function update(changed, _ctx) {
				ctx = _ctx;
				button0._svelte.ctx = ctx;
				button1._svelte.ctx = ctx;

				if (ctx.book.processed < ctx.book.pages) {
					if (if_block) {
						if_block.p(changed, ctx);
					} else {
						if_block = create_if_block$1(component, ctx);
						if_block.c();
						if_block.m(div1, null);
					}
				} else if (if_block) {
					if_block.d(1);
					if_block = null;
				}

				if ((changed.$pageBase || changed.books) && div1_style_value !== (div1_style_value = `background-image: url('${ctx.$pageBase}/${ctx.book.previewPage}');`)) {
					div1.style.cssText = div1_style_value;
				}

				if ((changed.books) && text5_value !== (text5_value = ctx.book.title)) {
					setData(text5, text5_value);
				}

				if ((changed.books) && text7_value !== (text7_value = ctx.book.pages + "p")) {
					setData(text7, text7_value);
				}
			},

			d: function destroy$$1(detach) {
				if (detach) {
					detachNode(div5);
				}

				removeListener(button0, "click", click_handler_1);
				removeListener(button1, "click", click_handler_2);
				if (if_block) if_block.d();
			}
		};
	}

	function App(options) {
		this._debugName = '<App>';
		if (!options || (!options.target && !options.root)) {
			throw new Error("'target' is a required option");
		}
		if (!options.store) {
			throw new Error("<App> references store properties, but no store was provided");
		}

		init(this, options);
		this._state = assign(assign(this.store._init(["locale","pageBase"]), data$1()), options.data);
		this.store._add(this, ["locale","pageBase"]);

		this._recompute({ book: 1 }, this._state);
		if (!('book' in this._state)) console.warn("<App> was created without expected data property 'book'");
		if (!('$locale' in this._state)) console.warn("<App> was created without expected data property '$locale'");
		if (!('editing' in this._state)) console.warn("<App> was created without expected data property 'editing'");


		if (!('invalidInput' in this._state)) console.warn("<App> was created without expected data property 'invalidInput'");
		if (!('books' in this._state)) console.warn("<App> was created without expected data property 'books'");
		if (!('$pageBase' in this._state)) console.warn("<App> was created without expected data property '$pageBase'");
		if (!('confirming' in this._state)) console.warn("<App> was created without expected data property 'confirming'");
		this._intro = true;

		this._handlers.destroy = [removeFromStore];

		this._fragment = create_main_fragment$2(this, this._state);

		this.root._oncreate.push(() => {
			oncreate$1.call(this);
			this.fire("update", { changed: assignTrue({}, this._state), current: this._state });
		});

		if (options.target) {
			if (options.hydrate) throw new Error("options.hydrate only works if the component was compiled with the `hydratable: true` option");
			this._fragment.c();
			this._mount(options.target, options.anchor);

			flush(this);
		}
	}

	assign(App.prototype, protoDev);
	assign(App.prototype, methods$1);

	App.prototype._checkReadOnly = function _checkReadOnly(newState) {
		if ('hasDocuments' in newState && !this._updatingReadonlyProperty) throw new Error("<App>: Cannot set read-only property 'hasDocuments'");
		if ('bookDirectionString' in newState && !this._updatingReadonlyProperty) throw new Error("<App>: Cannot set read-only property 'bookDirectionString'");
	};

	App.prototype._recompute = function _recompute(changed, state) {
		if (changed.book) {
			if (this._differs(state.hasDocuments, (state.hasDocuments = hasDocuments(state)))) changed.hasDocuments = true;
			if (this._differs(state.bookDirectionString, (state.bookDirectionString = bookDirectionString(state)))) changed.bookDirectionString = true;
		}
	};

	function Store(state, options) {
		this._handlers = {};
		this._dependents = [];

		this._computed = blankObject();
		this._sortedComputedProperties = [];

		this._state = assign({}, state);
		this._differs = options && options.immutable ? _differsImmutable : _differs;
	}

	assign(Store.prototype, {
		_add(component, props) {
			this._dependents.push({
				component: component,
				props: props
			});
		},

		_init(props) {
			const state = {};
			for (let i = 0; i < props.length; i += 1) {
				const prop = props[i];
				state['$' + prop] = this._state[prop];
			}
			return state;
		},

		_remove(component) {
			let i = this._dependents.length;
			while (i--) {
				if (this._dependents[i].component === component) {
					this._dependents.splice(i, 1);
					return;
				}
			}
		},

		_set(newState, changed) {
			const previous = this._state;
			this._state = assign(assign({}, previous), newState);

			for (let i = 0; i < this._sortedComputedProperties.length; i += 1) {
				this._sortedComputedProperties[i].update(this._state, changed);
			}

			this.fire('state', {
				changed,
				previous,
				current: this._state
			});

			this._dependents
				.filter(dependent => {
					const componentState = {};
					let dirty = false;

					for (let j = 0; j < dependent.props.length; j += 1) {
						const prop = dependent.props[j];
						if (prop in changed) {
							componentState['$' + prop] = this._state[prop];
							dirty = true;
						}
					}

					if (dirty) {
						dependent.component._stage(componentState);
						return true;
					}
				})
				.forEach(dependent => {
					dependent.component.set({});
				});

			this.fire('update', {
				changed,
				previous,
				current: this._state
			});
		},

		_sortComputedProperties() {
			const computed = this._computed;
			const sorted = this._sortedComputedProperties = [];
			const visited = blankObject();
			let currentKey;

			function visit(key) {
				const c = computed[key];

				if (c) {
					c.deps.forEach(dep => {
						if (dep === currentKey) {
							throw new Error(`Cyclical dependency detected between ${dep} <-> ${key}`);
						}

						visit(dep);
					});

					if (!visited[key]) {
						visited[key] = true;
						sorted.push(c);
					}
				}
			}

			for (const key in this._computed) {
				visit(currentKey = key);
			}
		},

		compute(key, deps, fn) {
			let value;

			const c = {
				deps,
				update: (state, changed, dirty) => {
					const values = deps.map(dep => {
						if (dep in changed) dirty = true;
						return state[dep];
					});

					if (dirty) {
						const newValue = fn.apply(null, values);
						if (this._differs(newValue, value)) {
							value = newValue;
							changed[key] = true;
							state[key] = value;
						}
					}
				}
			};

			this._computed[key] = c;
			this._sortComputedProperties();

			const state = assign({}, this._state);
			const changed = {};
			c.update(state, changed, true);
			this._set(state, changed);
		},

		fire,

		get,

		on,

		set(newState) {
			const oldState = this._state;
			const changed = this._changed = {};
			let dirty = false;

			for (const key in newState) {
				if (this._computed[key]) throw new Error(`'${key}' is a read-only computed property`);
				if (this._differs(newState[key], oldState[key])) changed[key] = dirty = true;
			}
			if (!dirty) return;

			this._set(newState, changed);
		}
	});

	// Request lib

	// Drupal apiBase for requests
	let apiBase = "";
	let generalApiBase = "";

	// Utilities
	async function ok(response, isAsync = true) {
	  if (isAsync && !response.ok) {
	    throw new Error("HTTP status: " + response.status);
	  }

	  let data;
	  if (isAsync) {
	    data = jSh.parseJSON(await response.text());
	  } else {
	    data = jSh.parseJSON(response);
	  }

	  if (data.error) {
	    throw new Error(data.error);
	  }

	  return data.data;
	}

	function string(thing) {
	  return JSON.stringify(thing);
	}

	// Request directives
	const request = {
	  setBase(url, _langList) {
	    apiBase = url[0];
	    generalApiBase = url[1];
	  },

	  async saveBook(book) {
	    const response = await fetch(apiBase + "/saveBook", {
	      method: "POST",
	      body: string({
	        ...book,
	      }),
	    });

	    return await ok(response);
	  },

	  async deleteBook(book) {
	    const response = await fetch(apiBase + "/deleteBook", {
	      method: "POST",
	      body: string({
	        id: book.id,
	      }),
	    });

	    return await ok(response);
	  },

	  async processPages(book, onprogress, onfinish) {
	    const evt = new EventSource(apiBase + "-stream/processPages?id=" + book.id);
	    evt.onmessage = function(e) {
	      const data = JSON.parse(e.data);

	      if (!data.error) {
	        onprogress(data.data);
	      }

	      // Close connection
	      if (data.data && data.data.close) {
	        evt.close();
	        onprogress && onprogress();
	      }
	    };
	  },

	  async loadBook(id) {
	    const response = await fetch(generalApiBase + "/loadBook", {
	      method: "POST",
	      body: string({
	        id,
	      }),
	    });

	    return await ok(response);
	  },

	  async loadBookPage(page, id) {
	    const response = await fetch(generalApiBase + "/loadBookPage", {
	      method: "POST",
	      body: string({
	        page,
	        id,
	      }),
	    });

	    return await ok(response);
	  },

	  // Files
	  async getFileForm(curFileHash = null) {
	    const response = await fetch(apiBase + "/getFileForm", {
	      method: "POST",
	      body: string({
	        curFileHash,
	      }),
	    });

	    return await ok(response);
	  },

	  uploadFile(form, callback = null) {
	    const data = new FormData();
	    const keyMap = {
	      buildId: "form_build_id",
	      token: "form_token",
	      id: "form_id",
	      file: "files[file1]",
	    };

	    for (const keyBase of Object.keys(form)) {
	      if (keyBase !== "actual" && keyBase !== "old") {
	        const key = keyMap.hasOwnProperty(keyBase) ? keyMap[keyBase] : keyBase;

	        data.append(key, form[keyBase]);
	      }
	    }

	    const xhr = new XMLHttpRequest();

	    // Attach progress cb
	    if (callback) {
	      xhr.upload.addEventListener("progress", (e) => {
	        callback(e.loaded, e.total);
	      });
	    }

	    const promise = new Promise((resolve, reject) => {
	      xhr.onreadystatechange = function() {
	        if (xhr.readyState === 4) {
	          if (xhr.status === 200) {
	            resolve(ok(xhr.responseText, false));
	          } else {
	            reject(new Error("Failed with status code " + xhr.status));
	          }
	        }
	      };

	      xhr.open("POST", apiBase + "/uploadFile");
	      xhr.send(data);
	    });

	    return promise;
	  },
	};

	// Utils

	// Book admin logic

	// TODO: Generalize request API
	function main(env) {
	  const locale = env.locale;

	  const store = new Store({
	    language: locale._langauge,
	  	locale: locale,
	    pageBase: env.pageBase,

	    fileFields: 0,
	    fileBase: env.fileBase,
	  });
	  request.setBase(env.apiBase, env.locale._languageList);

	  // Create app
	  const app = new App({
	    target: env.root,
	    store,
	    data: {
	      books: env.books,
	    },
	  });

	  // Create viewer
	  const viewer = ihfathBookViewer({
	    root: env.viewerRoot,
	    pageBase: env.pageBase,
	  });

	  viewer.onpage((pageNum, bookId) => {
	    request
	      .loadBookPage(pageNum, bookId)
	      .then(data => {
	        viewer.openPage(pageNum, data.hash);
	      })
	      .catch((e) => {
	        console.error("Failed to load book page", e);
	      });
	  });

	  store.on("savebook", ({ book }) => {
	    request
	      .saveBook(book)
	      .then((data) => {
	        book.id = +data.book.id;
	        book.bhid = data.book.bhid;
	        book.pages = +data.book.pages;
	        book.title = data.book.title;

	        store.fire("booksaved", {
	          book,
	        });
	      })
	      .catch(e => {
	        console.error("Failed saving book", book);
	      });
	  });

	  store.on("deletebook", ({ book }) => {
	    request
	      .deleteBook(book)
	      .then(data => {
	        if (data.success) {
	          store.fire("bookdeleted", {
	            book,
	          });
	        }
	      })
	      .catch(e => {
	        console.error("Failed to delete book", e, book);
	      });
	  });

	  store.on("processpages", (book) => {
	    processPages(book, store);
	  });

	  const loadedBooks = {
	    // id: bookData,
	  };
	  store.on("openbook", ({ book }) => {
	    const bookId = book.bhid;

	    function open(book) {
	      viewer.open(book);
	    }

	    if (loadedBooks[bookId]) {
	      open(loadedBooks[bookId]);
	    } else {
	      request
	        .loadBook(bookId)
	        .then(data => {
	          loadedBooks[bookId] = data.book;
	          open(data.book);
	        })
	        .catch(e => {
	          console.error("Failed to load book " + bookId, e);
	        });
	    }
	  });

	  // File chooser
	  store.on("requestfileform", function({ id, oldFileHash }) {
	    request
	      .getFileForm(oldFileHash)
	      .then(data => {
	        store.fire("requestfileformloaded", {
	          id,
	          form: data,
	        });
	        console.log("FILEDATA", data);
	      });
	  });

	  store.on("uploadfile", function({ id, form }) {
	    request
	      .uploadFile(form, (loaded, total) => {
	        store.fire("uploadfileprogress", {
	          id,
	          loaded,
	          total,
	        });
	      })
	      .then(data => {
	        store.fire("uploadedfile", {
	          id,
	          file: data,
	        });
	      })
	      .catch(e => {
	        console.error("Upload failed", e);
	      });
	  });
	}

	function processPages(book, store) {
	  request.processPages(book, function(progress) {
	    store.fire("bookprogress", {
	      book,
	      progress: progress.progress,
	      previewPage: progress.preview_page,
	    });
	  }, function() {
	    store.fire("bookfinished", {
	      book,
	    });
	  });
	}

	window.ihfathBookAdmin = main;

}());
//# sourceMappingURL=book-admin.js.map
