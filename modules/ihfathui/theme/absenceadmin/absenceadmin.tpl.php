<?php
// ...
function jst(...$args) {
  return json_encode(t(...$args));
}

function jse(...$args) {
  return json_encode(...$args);
}
?>
<div class="app-root bulma-root"></div>

<script type="text/javascript">
  window.addEventListener("load", () => {
    IhfathAbsence({
      root: document.querySelector(".app-root"),
      apiBase: <?= jse(url("ihfath/ajax/absenceadmin")) ?>,
      baseUrl: <?= jse(url("<front>")) ?>,

      locale: {
        months: [
          <?= jst("January") ?>,
          <?= jst("February") ?>,
          <?= jst("March") ?>,
          <?= jst("April") ?>,
          <?= jst("May") ?>,
          <?= jst("June") ?>,
          <?= jst("July") ?>,
          <?= jst("August") ?>,
          <?= jst("September") ?>,
          <?= jst("October") ?>,
          <?= jst("November") ?>,
          <?= jst("December") ?>,
        ],
      },
    });
  });
</script>
