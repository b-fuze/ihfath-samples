import Base, { Role } from "../../base";
import { Store, User, Pager, Course, CourseMap } from "../../defaults"

type UserRow = User & {
  showDetails?: boolean;
};

type UserMap = {
  [user: number]: UserRow;
};

declare class App extends Base<{
  start: {
    day: number;
    month: number;
    year: number;
  };
  end: {
    day: number;
    month: number;
    year: number;
  };
  fname: string,

  // Old fields (likely to be removed)
  lname: string,
  email: string,
  phone: string,
  role: Role,
  studentsWithoutLessons: boolean,
  teachersWithoutStudents: boolean,
  users: UserMap,
  list: number[],
  userList: UserRow[],
  reloadOnUpdate: boolean,
  columns: number,
  courses: CourseMap;

  months: [number, string][];

  pager: Pager,
  page: number,
}, {
  formUpdate: boolean;
}, Store> {
  search(keepPage?: boolean): void;
  showDetails(index: number): void;
  navigatePage(page: number): void;
}

export default App;

export function oncreate(this: App) {
  // Setup months
  const { locale } = this.store.get();

  console.log("MONTHS", locale.months.map((m, i) => [i, m]));
  this.set({
    months: <[number, string][]> locale.months.map((m, i) => [i, m]),
  });

  // Reload on form update
  let timeout: NodeJS.Timeout = null;

  this.on("formUpdate", (delay = true) => {
    const { reloadOnUpdate } = this.get();

    if (reloadOnUpdate) {
      clearTimeout(timeout);
      timeout = setTimeout(() => {
        this.search();
      }, delay ? 500 : 0);
    }
  });

  // Display search results
  this.store.on("searchresults", (data) => {
    const { page } = this.get();
    // const pager = pagerLinks(page, data.pager);

    this.set({
      list: data.list,
      courses: data.courses,
      users: data.users,
      // pager: pager,
      // page: Math.min(page, pager.total)
    });
  });
}

export function minutesToHours(seconds: number) {
  const minutes = Math.floor(seconds / 60);
  const hours = Math.floor((minutes / 60));
  const partialHour = minutes % 60;

  return `${ hours.toLocaleString() }:${ partialHour }`;
}

export function salary(seconds: number, hourPrice: string) {
  const minutes = Math.floor(seconds / 60);
  const hourPriceParsed = parseInt(hourPrice, 10);
  if (isNaN(hourPriceParsed)) {
    return "N/A";
  }

  return Math.floor((minutes / 60) * parseInt(hourPrice + "", 10)).toLocaleString();
}

export const methods = {
  reset(this: App) {
    this.set({
      fname: "",
      lname: "",
      email: "",
      phone: "",
      role: Role.Both,
      studentsWithoutLessons: false,
    });

    this.search();
  },

  search(this: App) {
    const { start, end } = this.get();

    this.store.fire("absencequery", {
      name: "",
      startDay: start.day,
      startMonth: start.month,
      startYear: start.year,
      endDay: end.day,
      endMonth: end.month,
      endYear: end.year,
    });
  },

  search_old(this: App, keepPage: boolean = false) {
    const {
      fname,
      lname,
      email,
      phone,
      role,
      studentsWithoutLessons,
      teachersWithoutStudents,

      page,
    } = this.get();

    if (!keepPage) {
      this.set({
        page: 1,
      });
    }

    this.store.fire("search", {
      fname,
      lname,
      email,
      phone,
      role,
      studentsWithoutLessons,
      teachersWithoutStudents,

      page: keepPage ? page - 1 : 0,
    });
  },

  navigatePage(this: App, page: number) {
    this.set({
      page,
    });

    this.search(true);
  },

  relativePage(this: App, dir: -1 | 1) {
    const { page, pager } = this.get();
    const newPage = Math.min(Math.max(page + dir, 1), pager.last);

    if (newPage !== page) {
      this.navigatePage(newPage);
    }
  }
};
