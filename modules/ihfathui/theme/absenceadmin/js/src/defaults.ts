import Base, { Store, Role } from "./base"

export type SearchQuery = {
  fname: string,
  lname: string,
  email: string,
  phone: string,
  role: Role,
  studentsWithoutLessons: boolean,
  teachersWithoutStudents: boolean,

  page: number,
};

export type AbsenceQuery = {
  name: string,
  startDay: number,
  startMonth: number,
  startYear: number,
  endDay: number,
  endMonth: number,
  endYear: number,
};

export type UserMap = {
  [user: number]: User;
};

export type SearchResults = {
  list: number[];
  users: UserMap;
  pager: Pager;
  courses: CourseMap;
};

export type AbsenceResults = {
  start: number;
  end: number;
  absences: {
    uid: number;
    first_name: string;
    last_name: string;
    cancelled: number;
    duration: number;
    lesson_count: number;
    students: {
      uid: number;
      first_name: string;
      last_name: string;
      cancelled: number;
      duration: number;
    }[];
  }[];
};

export type Store = Store<{
  baseUrl: string;
  locale: {
    months: string[];
  };
  absenceData: AbsenceResults;
}, {
  search: SearchQuery;
  searchresults: SearchResults;

  absencequery: AbsenceQuery;
  absenceresults: AbsenceResults;
}>;

export interface Pager {
  totalItems: number;
  total: number;
  pageArray: number;
  limit: number;

  last: number;
  startSep: boolean;
  endSep: boolean;
  intermediatePages: number[];
}

export type CourseMap = {
  [course: number]: Course;
};

export interface Course {
  id: number;
  machineName: string;
  name: string;
  description: string;
}

export const enum UserRole {
  Professor = "2",
  Student = "3",
}

export interface User {
  id: number;
  name: string;
  firstname: string;
  lastname: string;
  email: string;
  address: string;
  phone: string;
  role: UserRole;
  created: number;

  professors?: number[];
  students?: number[];
  courses?: number[];
}

// Side effects to keep in Rollup
export const _ = () => {
  document.createComment("");
};
