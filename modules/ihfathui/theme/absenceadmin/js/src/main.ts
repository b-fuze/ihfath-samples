import { Store } from "svelte/store"
import { Store as AppStore, User, SearchQuery, CourseMap, _ } from "./defaults"
import App from "./components/app/App.html"
import { request } from "./request"
import { userToJs, courseToJs } from "./utils"

function main(env: any) {
  const store: AppStore = <AppStore> <unknown> new Store({
    baseUrl: env.baseUrl,
    locale: env.locale,
    absenceData: {
      start: 0,
      end: 0,
      absences: [],
    },
  });

  const app = new App({
    target: env.root,
    store,
  });

  request.setBase(env.apiBase);

  store.on("absencequery", (query) => {
    request
      .absences(
        query.name,
        query.startDay,
        query.startMonth,
        query.startYear,
        query.endDay,
        query.endMonth,
        query.endYear,
      )
      .then((data) => {
        store.set({
          absenceData: data,
        });
        store.fire("absenceresults", data);
      })
      .catch((e) => {
        console.error("Failed to load absence data", e);
      });
  });

  // Do first search on load
  setTimeout(() => {
    app.search();
  }, 350);
}

(window as any).IhfathAbsence = main;

// Side effects for Rollup
_();
