import svelte from 'rollup-plugin-svelte';
import resolve from 'rollup-plugin-node-resolve';
import commonjs from 'rollup-plugin-commonjs';
import typescript from 'rollup-plugin-typescript';
import sass from '@nsivertsen/svelte-preprocess-sass';

const production = !process.env.ROLLUP_WATCH;

export default {
  input: "src/main.ts",
  output: {
    sourcemap: true,
    format: "iife",
    file: "../dist/absenceadmin.js",
  },
  plugins: [
    typescript(),
    svelte({
      preprocess: {
        style: sass(),
      },

      dev: !production,

      css: ((css) => {
        css.write("../dist/absenceadmin.css");
      }),
    }),

    resolve(),
    commonjs(),
  ]
};
