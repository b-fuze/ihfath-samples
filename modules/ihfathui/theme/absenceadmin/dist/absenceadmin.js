(function () {
	'use strict';

	function noop() {}

	function assign(tar, src) {
		for (var k in src) tar[k] = src[k];
		return tar;
	}

	function assignTrue(tar, src) {
		for (var k in src) tar[k] = 1;
		return tar;
	}

	function addLoc(element, file, line, column, char) {
		element.__svelte_meta = {
			loc: { file, line, column, char }
		};
	}

	function append(target, node) {
		target.appendChild(node);
	}

	function insert(target, node, anchor) {
		target.insertBefore(node, anchor);
	}

	function detachNode(node) {
		node.parentNode.removeChild(node);
	}

	function destroyEach(iterations, detach) {
		for (var i = 0; i < iterations.length; i += 1) {
			if (iterations[i]) iterations[i].d(detach);
		}
	}

	function createElement(name) {
		return document.createElement(name);
	}

	function createText(data) {
		return document.createTextNode(data);
	}

	function createComment() {
		return document.createComment('');
	}

	function addListener(node, event, handler, options) {
		node.addEventListener(event, handler, options);
	}

	function removeListener(node, event, handler, options) {
		node.removeEventListener(event, handler, options);
	}

	function setAttribute(node, attribute, value) {
		if (value == null) node.removeAttribute(attribute);
		else node.setAttribute(attribute, value);
	}

	function setData(text, data) {
		text.data = '' + data;
	}

	function toggleClass(element, name, toggle) {
		element.classList[toggle ? 'add' : 'remove'](name);
	}

	function blankObject() {
		return Object.create(null);
	}

	function destroy(detach) {
		this.destroy = noop;
		this.fire('destroy');
		this.set = noop;

		this._fragment.d(detach !== false);
		this._fragment = null;
		this._state = {};
	}

	function destroyDev(detach) {
		destroy.call(this, detach);
		this.destroy = function() {
			console.warn('Component was already destroyed');
		};
	}

	function _differs(a, b) {
		return a != a ? b == b : a !== b || ((a && typeof a === 'object') || typeof a === 'function');
	}

	function _differsImmutable(a, b) {
		return a != a ? b == b : a !== b;
	}

	function fire(eventName, data) {
		var handlers =
			eventName in this._handlers && this._handlers[eventName].slice();
		if (!handlers) return;

		for (var i = 0; i < handlers.length; i += 1) {
			var handler = handlers[i];

			if (!handler.__calling) {
				try {
					handler.__calling = true;
					handler.call(this, data);
				} finally {
					handler.__calling = false;
				}
			}
		}
	}

	function flush(component) {
		component._lock = true;
		callAll(component._beforecreate);
		callAll(component._oncreate);
		callAll(component._aftercreate);
		component._lock = false;
	}

	function get() {
		return this._state;
	}

	function init(component, options) {
		component._handlers = blankObject();
		component._slots = blankObject();
		component._bind = options._bind;
		component._staged = {};

		component.options = options;
		component.root = options.root || component;
		component.store = options.store || component.root.store;

		if (!options.root) {
			component._beforecreate = [];
			component._oncreate = [];
			component._aftercreate = [];
		}
	}

	function on(eventName, handler) {
		var handlers = this._handlers[eventName] || (this._handlers[eventName] = []);
		handlers.push(handler);

		return {
			cancel: function() {
				var index = handlers.indexOf(handler);
				if (~index) handlers.splice(index, 1);
			}
		};
	}

	function set(newState) {
		this._set(assign({}, newState));
		if (this.root._lock) return;
		flush(this.root);
	}

	function _set(newState) {
		var oldState = this._state,
			changed = {},
			dirty = false;

		newState = assign(this._staged, newState);
		this._staged = {};

		for (var key in newState) {
			if (this._differs(newState[key], oldState[key])) changed[key] = dirty = true;
		}
		if (!dirty) return;

		this._state = assign(assign({}, oldState), newState);
		this._recompute(changed, this._state);
		if (this._bind) this._bind(changed, this._state);

		if (this._fragment) {
			this.fire("state", { changed: changed, current: this._state, previous: oldState });
			this._fragment.p(changed, this._state);
			this.fire("update", { changed: changed, current: this._state, previous: oldState });
		}
	}

	function _stage(newState) {
		assign(this._staged, newState);
	}

	function setDev(newState) {
		if (typeof newState !== 'object') {
			throw new Error(
				this._debugName + '.set was called without an object of data key-values to update.'
			);
		}

		this._checkReadOnly(newState);
		set.call(this, newState);
	}

	function callAll(fns) {
		while (fns && fns.length) fns.shift()();
	}

	function _mount(target, anchor) {
		this._fragment[this._fragment.i ? 'i' : 'm'](target, anchor || null);
	}

	function removeFromStore() {
		this.store._remove(this);
	}

	var protoDev = {
		destroy: destroyDev,
		get,
		fire,
		on,
		set: setDev,
		_recompute: noop,
		_set,
		_stage,
		_mount,
		_differs
	};

	function Store(state, options) {
		this._handlers = {};
		this._dependents = [];

		this._computed = blankObject();
		this._sortedComputedProperties = [];

		this._state = assign({}, state);
		this._differs = options && options.immutable ? _differsImmutable : _differs;
	}

	assign(Store.prototype, {
		_add(component, props) {
			this._dependents.push({
				component: component,
				props: props
			});
		},

		_init(props) {
			const state = {};
			for (let i = 0; i < props.length; i += 1) {
				const prop = props[i];
				state['$' + prop] = this._state[prop];
			}
			return state;
		},

		_remove(component) {
			let i = this._dependents.length;
			while (i--) {
				if (this._dependents[i].component === component) {
					this._dependents.splice(i, 1);
					return;
				}
			}
		},

		_set(newState, changed) {
			const previous = this._state;
			this._state = assign(assign({}, previous), newState);

			for (let i = 0; i < this._sortedComputedProperties.length; i += 1) {
				this._sortedComputedProperties[i].update(this._state, changed);
			}

			this.fire('state', {
				changed,
				previous,
				current: this._state
			});

			this._dependents
				.filter(dependent => {
					const componentState = {};
					let dirty = false;

					for (let j = 0; j < dependent.props.length; j += 1) {
						const prop = dependent.props[j];
						if (prop in changed) {
							componentState['$' + prop] = this._state[prop];
							dirty = true;
						}
					}

					if (dirty) {
						dependent.component._stage(componentState);
						return true;
					}
				})
				.forEach(dependent => {
					dependent.component.set({});
				});

			this.fire('update', {
				changed,
				previous,
				current: this._state
			});
		},

		_sortComputedProperties() {
			const computed = this._computed;
			const sorted = this._sortedComputedProperties = [];
			const visited = blankObject();
			let currentKey;

			function visit(key) {
				const c = computed[key];

				if (c) {
					c.deps.forEach(dep => {
						if (dep === currentKey) {
							throw new Error(`Cyclical dependency detected between ${dep} <-> ${key}`);
						}

						visit(dep);
					});

					if (!visited[key]) {
						visited[key] = true;
						sorted.push(c);
					}
				}
			}

			for (const key in this._computed) {
				visit(currentKey = key);
			}
		},

		compute(key, deps, fn) {
			let value;

			const c = {
				deps,
				update: (state, changed, dirty) => {
					const values = deps.map(dep => {
						if (dep in changed) dirty = true;
						return state[dep];
					});

					if (dirty) {
						const newValue = fn.apply(null, values);
						if (this._differs(newValue, value)) {
							value = newValue;
							changed[key] = true;
							state[key] = value;
						}
					}
				}
			};

			this._computed[key] = c;
			this._sortComputedProperties();

			const state = assign({}, this._state);
			const changed = {};
			c.update(state, changed, true);
			this._set(state, changed);
		},

		fire,

		get,

		on,

		set(newState) {
			const oldState = this._state;
			const changed = this._changed = {};
			let dirty = false;

			for (const key in newState) {
				if (this._computed[key]) throw new Error(`'${key}' is a read-only computed property`);
				if (this._differs(newState[key], oldState[key])) changed[key] = dirty = true;
			}
			if (!dirty) return;

			this._set(newState, changed);
		}
	});

	var UserRole;
	(function (UserRole) {
	    UserRole["Professor"] = "2";
	    UserRole["Student"] = "3";
	})(UserRole || (UserRole = {}));
	// Side effects to keep in Rollup
	const _ = () => {
	    document.createComment("");
	};

	/* src/components/Dropdown/Dropdown.html generated by Svelte v2.16.0 */

	function _index({ value, list }) {
	  let index = 0;

	  for (let i=0; i<list.length; i++) {
	    const item = list[i];

	    if (item[0] === value) {
	      index = i;
	      break;
	    }
	  }

	  return index;
	}

	function data() {
	  return {
	    value: null,
	    list: [],
	    visible: false,
	    disabled: false,
	    right: false,
	  }
	}
	var methods = {
	  change(value) {
	    this.set({
	      value,
	      visible: false,
	    });
	    this.fire("change", {
	      value,
	    });
	  }
	};

	function oncreate() {
	  const listener = (e) => {
	    let target = e.target;

	    while (target !== document.body) {
	      if (target === this.refs.dropdown) {
	        return;
	      }

	      target = target.parentNode;
	    }

	    this.set({
	      visible: false,
	    });
	  };

	  this.on("state", ({ current }) => {
	    if (current.visible) {
	      window.addEventListener("click", listener);
	    } else {
	      window.removeEventListener("click", listener);
	    }
	  });

	  this.on("destroy", () => {
	    window.removeEventListener("click", listener);
	  });
	}
	const file = "src/components/Dropdown/Dropdown.html";

	function click_handler(event) {
		const { component, ctx } = this._svelte;

		component.change(ctx.item[0]);
	}

	function get_each_context(ctx, list, i) {
		const child_ctx = Object.create(ctx);
		child_ctx.item = list[i];
		return child_ctx;
	}

	function create_main_fragment(component, ctx) {
		var div3, div0, button, span0, text0_value = (ctx.list[ctx._index] || "  ")[1], text0, text1, span1, i, text2, div2, div1;

		function click_handler(event) {
			component.set({visible: !ctx.disabled});
		}

		var each_value = ctx.list;

		var each_blocks = [];

		for (var i_1 = 0; i_1 < each_value.length; i_1 += 1) {
			each_blocks[i_1] = create_each_block(component, get_each_context(ctx, each_value, i_1));
		}

		return {
			c: function create() {
				div3 = createElement("div");
				div0 = createElement("div");
				button = createElement("button");
				span0 = createElement("span");
				text0 = createText(text0_value);
				text1 = createText("\n\n      ");
				span1 = createElement("span");
				i = createElement("i");
				text2 = createText("\n  ");
				div2 = createElement("div");
				div1 = createElement("div");

				for (var i_1 = 0; i_1 < each_blocks.length; i_1 += 1) {
					each_blocks[i_1].c();
				}
				addLoc(span0, file, 14, 6, 312);
				i.className = "fa fa-angle-down";
				addLoc(i, file, 17, 8, 399);
				span1.className = "icon is-small";
				addLoc(span1, file, 16, 6, 362);
				addListener(button, "click", click_handler);
				button.className = "button";
				button.type = "button";
				addLoc(button, file, 8, 4, 182);
				div0.className = "dropdown-trigger";
				addLoc(div0, file, 7, 2, 147);
				div1.className = "dropdown-content";
				addLoc(div1, file, 22, 4, 503);
				div2.className = "dropdown-menu";
				addLoc(div2, file, 21, 2, 471);
				div3.className = "dropdown svelte-3v9op2";
				div3.tabIndex = "0";
				toggleClass(div3, "dropdown-disabled", ctx.disabled);
				toggleClass(div3, "is-active", ctx.visible);
				toggleClass(div3, "is-right", ctx.right);
				addLoc(div3, file, 0, 0, 0);
			},

			m: function mount(target, anchor) {
				insert(target, div3, anchor);
				append(div3, div0);
				append(div0, button);
				append(button, span0);
				append(span0, text0);
				append(button, text1);
				append(button, span1);
				append(span1, i);
				append(div3, text2);
				append(div3, div2);
				append(div2, div1);

				for (var i_1 = 0; i_1 < each_blocks.length; i_1 += 1) {
					each_blocks[i_1].m(div1, null);
				}

				component.refs.dropdown = div3;
			},

			p: function update(changed, _ctx) {
				ctx = _ctx;
				if ((changed.list || changed._index) && text0_value !== (text0_value = (ctx.list[ctx._index] || "  ")[1])) {
					setData(text0, text0_value);
				}

				if (changed.list) {
					each_value = ctx.list;

					for (var i_1 = 0; i_1 < each_value.length; i_1 += 1) {
						const child_ctx = get_each_context(ctx, each_value, i_1);

						if (each_blocks[i_1]) {
							each_blocks[i_1].p(changed, child_ctx);
						} else {
							each_blocks[i_1] = create_each_block(component, child_ctx);
							each_blocks[i_1].c();
							each_blocks[i_1].m(div1, null);
						}
					}

					for (; i_1 < each_blocks.length; i_1 += 1) {
						each_blocks[i_1].d(1);
					}
					each_blocks.length = each_value.length;
				}

				if (changed.disabled) {
					toggleClass(div3, "dropdown-disabled", ctx.disabled);
				}

				if (changed.visible) {
					toggleClass(div3, "is-active", ctx.visible);
				}

				if (changed.right) {
					toggleClass(div3, "is-right", ctx.right);
				}
			},

			d: function destroy$$1(detach) {
				if (detach) {
					detachNode(div3);
				}

				removeListener(button, "click", click_handler);

				destroyEach(each_blocks, detach);

				if (component.refs.dropdown === div3) component.refs.dropdown = null;
			}
		};
	}

	// (27:8) {:else}
	function create_else_block(component, ctx) {
		var a, text0_value = ctx.item[1], text0, text1;

		return {
			c: function create() {
				a = createElement("a");
				text0 = createText(text0_value);
				text1 = createText("\n          ");
				a._svelte = { component, ctx };

				addListener(a, "click", click_handler);
				a.href = "javascript:0[0]";
				a.className = "dropdown-item";
				addLoc(a, file, 27, 10, 652);
			},

			m: function mount(target, anchor) {
				insert(target, a, anchor);
				append(a, text0);
				append(a, text1);
			},

			p: function update(changed, _ctx) {
				ctx = _ctx;
				if ((changed.list) && text0_value !== (text0_value = ctx.item[1])) {
					setData(text0, text0_value);
				}

				a._svelte.ctx = ctx;
			},

			d: function destroy$$1(detach) {
				if (detach) {
					detachNode(a);
				}

				removeListener(a, "click", click_handler);
			}
		};
	}

	// (25:8) {#if item === 0}
	function create_if_block(component, ctx) {
		var hr;

		return {
			c: function create() {
				hr = createElement("hr");
				hr.className = "dropdown-divider";
				addLoc(hr, file, 25, 10, 596);
			},

			m: function mount(target, anchor) {
				insert(target, hr, anchor);
			},

			p: noop,

			d: function destroy$$1(detach) {
				if (detach) {
					detachNode(hr);
				}
			}
		};
	}

	// (24:6) {#each list as item}
	function create_each_block(component, ctx) {
		var if_block_anchor;

		function select_block_type(ctx) {
			if (ctx.item === 0) return create_if_block;
			return create_else_block;
		}

		var current_block_type = select_block_type(ctx);
		var if_block = current_block_type(component, ctx);

		return {
			c: function create() {
				if_block.c();
				if_block_anchor = createComment();
			},

			m: function mount(target, anchor) {
				if_block.m(target, anchor);
				insert(target, if_block_anchor, anchor);
			},

			p: function update(changed, ctx) {
				if (current_block_type === (current_block_type = select_block_type(ctx)) && if_block) {
					if_block.p(changed, ctx);
				} else {
					if_block.d(1);
					if_block = current_block_type(component, ctx);
					if_block.c();
					if_block.m(if_block_anchor.parentNode, if_block_anchor);
				}
			},

			d: function destroy$$1(detach) {
				if_block.d(detach);
				if (detach) {
					detachNode(if_block_anchor);
				}
			}
		};
	}

	function Dropdown(options) {
		this._debugName = '<Dropdown>';
		if (!options || (!options.target && !options.root)) {
			throw new Error("'target' is a required option");
		}

		init(this, options);
		this.refs = {};
		this._state = assign(data(), options.data);

		this._recompute({ value: 1, list: 1 }, this._state);
		if (!('value' in this._state)) console.warn("<Dropdown> was created without expected data property 'value'");
		if (!('list' in this._state)) console.warn("<Dropdown> was created without expected data property 'list'");
		if (!('disabled' in this._state)) console.warn("<Dropdown> was created without expected data property 'disabled'");
		if (!('visible' in this._state)) console.warn("<Dropdown> was created without expected data property 'visible'");
		if (!('right' in this._state)) console.warn("<Dropdown> was created without expected data property 'right'");
		this._intro = true;

		this._fragment = create_main_fragment(this, this._state);

		this.root._oncreate.push(() => {
			oncreate.call(this);
			this.fire("update", { changed: assignTrue({}, this._state), current: this._state });
		});

		if (options.target) {
			if (options.hydrate) throw new Error("options.hydrate only works if the component was compiled with the `hydratable: true` option");
			this._fragment.c();
			this._mount(options.target, options.anchor);

			flush(this);
		}
	}

	assign(Dropdown.prototype, protoDev);
	assign(Dropdown.prototype, methods);

	Dropdown.prototype._checkReadOnly = function _checkReadOnly(newState) {
		if ('_index' in newState && !this._updatingReadonlyProperty) throw new Error("<Dropdown>: Cannot set read-only property '_index'");
	};

	Dropdown.prototype._recompute = function _recompute(changed, state) {
		if (changed.value || changed.list) {
			if (this._differs(state._index, (state._index = _index(state)))) changed._index = true;
		}
	};

	// import { Store } from "svelte/store";
	var Role;
	(function (Role) {
	    Role["Both"] = "0";
	    Role["Students"] = "2";
	    Role["Teachers"] = "3";
	})(Role || (Role = {}));

	function oncreate$1() {
	    // Setup months
	    const { locale } = this.store.get();
	    console.log("MONTHS", locale.months.map((m, i) => [i, m]));
	    this.set({
	        months: locale.months.map((m, i) => [i, m]),
	    });
	    // Reload on form update
	    let timeout = null;
	    this.on("formUpdate", (delay = true) => {
	        const { reloadOnUpdate } = this.get();
	        if (reloadOnUpdate) {
	            clearTimeout(timeout);
	            timeout = setTimeout(() => {
	                this.search();
	            }, delay ? 500 : 0);
	        }
	    });
	    // Display search results
	    this.store.on("searchresults", (data) => {
	        const { page } = this.get();
	        // const pager = pagerLinks(page, data.pager);
	        this.set({
	            list: data.list,
	            courses: data.courses,
	            users: data.users,
	        });
	    });
	}
	function minutesToHours(seconds) {
	    const minutes = Math.floor(seconds / 60);
	    const hours = Math.floor((minutes / 60));
	    const partialHour = minutes % 60;
	    return `${hours.toLocaleString()}:${partialHour}`;
	}
	function salary(seconds, hourPrice) {
	    const minutes = Math.floor(seconds / 60);
	    const hourPriceParsed = parseInt(hourPrice, 10);
	    if (isNaN(hourPriceParsed)) {
	        return "N/A";
	    }
	    return Math.floor((minutes / 60) * parseInt(hourPrice + "", 10)).toLocaleString();
	}
	const methods$1 = {
	    reset() {
	        this.set({
	            fname: "",
	            lname: "",
	            email: "",
	            phone: "",
	            role: Role.Both,
	            studentsWithoutLessons: false,
	        });
	        this.search();
	    },
	    search() {
	        const { start, end } = this.get();
	        this.store.fire("absencequery", {
	            name: "",
	            startDay: start.day,
	            startMonth: start.month,
	            startYear: start.year,
	            endDay: end.day,
	            endMonth: end.month,
	            endYear: end.year,
	        });
	    },
	    search_old(keepPage = false) {
	        const { fname, lname, email, phone, role, studentsWithoutLessons, teachersWithoutStudents, page, } = this.get();
	        if (!keepPage) {
	            this.set({
	                page: 1,
	            });
	        }
	        this.store.fire("search", {
	            fname,
	            lname,
	            email,
	            phone,
	            role,
	            studentsWithoutLessons,
	            teachersWithoutStudents,
	            page: keepPage ? page - 1 : 0,
	        });
	    },
	    navigatePage(page) {
	        this.set({
	            page,
	        });
	        this.search(true);
	    },
	    relativePage(dir) {
	        const { page, pager } = this.get();
	        const newPage = Math.min(Math.max(page + dir, 1), pager.last);
	        if (newPage !== page) {
	            this.navigatePage(newPage);
	        }
	    }
	};

	/* src/components/app/App.html generated by Svelte v2.16.0 */



	const now = new Date();
	const date = {
	  day: 1,
	  month: now.getMonth(),
	  year: now.getFullYear(),
	};

	const days = new Array(31).fill(0).map((x, i) => [i + 1, (i + 1) + ""]);

	function data$1() {
	  return {
	    start: {
	      ...date,
	    },
	    end: {
	      ...date,
	      month: date.month + 1,
	    },
	    fname: "",
	    hourPrice: "20EGP",

	    // Old fields (to be removed)
	    lname: "",
	    email: "",
	    phone: "",
	    role: "0",
	    studentsWithoutLessons: false,
	    teachersWithoutStudents: true,
	    users: {},
	    reloadOnUpdate: true,

	    days,
	    // @ts-ignore
	    months: [],
	  }
	}
	var methods_1 = {
	  reset: methods$1.reset,
	  search: methods$1.search,
	  navigatePage: methods$1.navigatePage,
	  relativePage: methods$1.relativePage,
	};

	const file$1 = "src/components/app/App.html";

	function get_each_context_1(ctx, list, i) {
		const child_ctx = Object.create(ctx);
		child_ctx.student = list[i];
		return child_ctx;
	}

	function get_each_context$1(ctx, list, i) {
		const child_ctx = Object.create(ctx);
		child_ctx.prof = list[i];
		child_ctx.index = i;
		return child_ctx;
	}

	function create_main_fragment$1(component, ctx) {
		var div11, div8, div4, div1, h40, text1, div0, dropdown0_updating = {}, text2, dropdown1_updating = {}, text3, input0, input0_updating = false, text4, div3, h41, text6, div2, dropdown2_updating = {}, text7, dropdown3_updating = {}, text8, input1, input1_updating = false, text9, div7, div5, h42, text11, input2, input2_updating = false, text12, div6, h43, text14, input3, input3_updating = false, text15, div10, div9, label, input4, text16, span, text18, div17, div16, div12, text19, div15, div13, button0, text21, div14, button1, text23, each_anchor;

		var dropdown0_initial_data = { list: ctx.days };
		if (ctx.start.day !== void 0) {
			dropdown0_initial_data.value = ctx.start.day;
			dropdown0_updating.value = true;
		}
		var dropdown0 = new Dropdown({
			root: component.root,
			store: component.store,
			data: dropdown0_initial_data,
			_bind(changed, childState) {
				var newState = {};
				if (!dropdown0_updating.value && changed.value) {
					ctx.start.day = childState.value;
					newState.start = ctx.start;
				}
				component._set(newState);
				dropdown0_updating = {};
			}
		});

		component.root._beforecreate.push(() => {
			dropdown0._bind({ value: 1 }, dropdown0.get());
		});

		dropdown0.on("change", function(event) {
			component.fire('formUpdate', true);
		});

		var dropdown1_initial_data = { list: ctx.months };
		if (ctx.start.month !== void 0) {
			dropdown1_initial_data.value = ctx.start.month;
			dropdown1_updating.value = true;
		}
		var dropdown1 = new Dropdown({
			root: component.root,
			store: component.store,
			data: dropdown1_initial_data,
			_bind(changed, childState) {
				var newState = {};
				if (!dropdown1_updating.value && changed.value) {
					ctx.start.month = childState.value;
					newState.start = ctx.start;
				}
				component._set(newState);
				dropdown1_updating = {};
			}
		});

		component.root._beforecreate.push(() => {
			dropdown1._bind({ value: 1 }, dropdown1.get());
		});

		dropdown1.on("change", function(event) {
			component.fire('formUpdate', true);
		});

		function input0_input_handler() {
			input0_updating = true;
			ctx.start.year = input0.value;
			component.set({ start: ctx.start });
			input0_updating = false;
		}

		function input_handler(event) {
			component.fire('formUpdate', true);
		}

		var dropdown2_initial_data = { list: ctx.days };
		if (ctx.end.day !== void 0) {
			dropdown2_initial_data.value = ctx.end.day;
			dropdown2_updating.value = true;
		}
		var dropdown2 = new Dropdown({
			root: component.root,
			store: component.store,
			data: dropdown2_initial_data,
			_bind(changed, childState) {
				var newState = {};
				if (!dropdown2_updating.value && changed.value) {
					ctx.end.day = childState.value;
					newState.end = ctx.end;
				}
				component._set(newState);
				dropdown2_updating = {};
			}
		});

		component.root._beforecreate.push(() => {
			dropdown2._bind({ value: 1 }, dropdown2.get());
		});

		dropdown2.on("change", function(event) {
			component.fire('formUpdate', true);
		});

		var dropdown3_initial_data = { list: ctx.months };
		if (ctx.end.month !== void 0) {
			dropdown3_initial_data.value = ctx.end.month;
			dropdown3_updating.value = true;
		}
		var dropdown3 = new Dropdown({
			root: component.root,
			store: component.store,
			data: dropdown3_initial_data,
			_bind(changed, childState) {
				var newState = {};
				if (!dropdown3_updating.value && changed.value) {
					ctx.end.month = childState.value;
					newState.end = ctx.end;
				}
				component._set(newState);
				dropdown3_updating = {};
			}
		});

		component.root._beforecreate.push(() => {
			dropdown3._bind({ value: 1 }, dropdown3.get());
		});

		dropdown3.on("change", function(event) {
			component.fire('formUpdate', true);
		});

		function input1_input_handler() {
			input1_updating = true;
			ctx.end.year = input1.value;
			component.set({ end: ctx.end });
			input1_updating = false;
		}

		function input_handler_1(event) {
			component.fire('formUpdate', true);
		}

		function input2_input_handler() {
			input2_updating = true;
			component.set({ fname: input2.value });
			input2_updating = false;
		}

		function input_handler_2(event) {
			component.fire('formUpdate', true);
		}

		function input3_input_handler() {
			input3_updating = true;
			component.set({ hourPrice: input3.value });
			input3_updating = false;
		}

		function input4_change_handler() {
			component.set({ reloadOnUpdate: input4.checked });
		}

		function click_handler(event) {
			component.reset();
		}

		function click_handler_1(event) {
			component.search();
		}

		var each_value = ctx.$absenceData.absences;

		var each_blocks = [];

		for (var i = 0; i < each_value.length; i += 1) {
			each_blocks[i] = create_each_block$1(component, get_each_context$1(ctx, each_value, i));
		}

		return {
			c: function create() {
				div11 = createElement("div");
				div8 = createElement("div");
				div4 = createElement("div");
				div1 = createElement("div");
				h40 = createElement("h4");
				h40.textContent = "Start date";
				text1 = createText("\n        ");
				div0 = createElement("div");
				dropdown0._fragment.c();
				text2 = createText("\n          ");
				dropdown1._fragment.c();
				text3 = createText("\n          ");
				input0 = createElement("input");
				text4 = createText("\n      ");
				div3 = createElement("div");
				h41 = createElement("h4");
				h41.textContent = "End date";
				text6 = createText("\n        ");
				div2 = createElement("div");
				dropdown2._fragment.c();
				text7 = createText("\n          ");
				dropdown3._fragment.c();
				text8 = createText("\n          ");
				input1 = createElement("input");
				text9 = createText("\n    ");
				div7 = createElement("div");
				div5 = createElement("div");
				h42 = createElement("h4");
				h42.textContent = "Professor";
				text11 = createText("\n        ");
				input2 = createElement("input");
				text12 = createText("\n      ");
				div6 = createElement("div");
				h43 = createElement("h4");
				h43.textContent = "Salary";
				text14 = createText("\n        ");
				input3 = createElement("input");
				text15 = createText("\n  ");
				div10 = createElement("div");
				div9 = createElement("div");
				label = createElement("label");
				input4 = createElement("input");
				text16 = createText("\n        ");
				span = createElement("span");
				span.textContent = "Auto update";
				text18 = createText("\n\n\n");
				div17 = createElement("div");
				div16 = createElement("div");
				div12 = createElement("div");
				text19 = createText("\n    ");
				div15 = createElement("div");
				div13 = createElement("div");
				button0 = createElement("button");
				button0.textContent = "Reset";
				text21 = createText("\n      ");
				div14 = createElement("div");
				button1 = createElement("button");
				button1.textContent = "Search";
				text23 = createText("\n\n\n");

				for (var i = 0; i < each_blocks.length; i += 1) {
					each_blocks[i].c();
				}

				each_anchor = createComment();
				h40.className = "subtitle";
				addLoc(h40, file$1, 4, 8, 152);
				addListener(input0, "input", input0_input_handler);
				addListener(input0, "input", input_handler);
				setAttribute(input0, "type", "text");
				input0.className = "input year-input svelte-1hqiqhm";
				input0.placeholder = "YYYY";
				addLoc(input0, file$1, 14, 10, 493);
				div0.className = "input-row svelte-1hqiqhm";
				addLoc(div0, file$1, 5, 8, 197);
				div1.className = "column";
				addLoc(div1, file$1, 3, 6, 123);
				h41.className = "subtitle";
				addLoc(h41, file$1, 23, 8, 738);
				addListener(input1, "input", input1_input_handler);
				addListener(input1, "input", input_handler_1);
				setAttribute(input1, "type", "text");
				input1.className = "input year-input svelte-1hqiqhm";
				input1.placeholder = "YYYY";
				addLoc(input1, file$1, 33, 10, 1073);
				div2.className = "input-row svelte-1hqiqhm";
				addLoc(div2, file$1, 24, 8, 781);
				div3.className = "column";
				addLoc(div3, file$1, 22, 6, 709);
				div4.className = "columns input-data svelte-1hqiqhm";
				addLoc(div4, file$1, 2, 4, 84);
				h42.className = "subtitle";
				addLoc(h42, file$1, 44, 8, 1364);
				addListener(input2, "input", input2_input_handler);
				addListener(input2, "input", input_handler_2);
				setAttribute(input2, "type", "text");
				input2.className = "input";
				input2.placeholder = "Name...";
				addLoc(input2, file$1, 45, 8, 1408);
				div5.className = "column";
				addLoc(div5, file$1, 43, 6, 1335);
				h43.className = "subtitle";
				addLoc(h43, file$1, 53, 8, 1615);
				addListener(input3, "input", input3_input_handler);
				setAttribute(input3, "type", "text");
				input3.className = "input";
				input3.placeholder = "e.g 14EGP";
				addLoc(input3, file$1, 54, 8, 1656);
				div6.className = "column";
				addLoc(div6, file$1, 52, 6, 1586);
				div7.className = "columns input-data svelte-1hqiqhm";
				addLoc(div7, file$1, 42, 4, 1296);
				div8.className = "user-search-form-control svelte-1hqiqhm";
				addLoc(div8, file$1, 1, 2, 41);
				addListener(input4, "change", input4_change_handler);
				setAttribute(input4, "type", "checkbox");
				addLoc(input4, file$1, 65, 8, 1897);
				addLoc(span, file$1, 68, 8, 1977);
				addLoc(label, file$1, 64, 6, 1881);
				div9.className = "content";
				addLoc(div9, file$1, 63, 4, 1853);
				div10.className = "user-search-form-options svelte-1hqiqhm";
				addLoc(div10, file$1, 62, 2, 1810);
				div11.className = "content user-search-form svelte-1hqiqhm";
				addLoc(div11, file$1, 0, 0, 0);
				div12.className = "level-left";
				addLoc(div12, file$1, 77, 4, 2125);
				addListener(button0, "click", click_handler);
				button0.className = "button is-link is-normal";
				addLoc(button0, file$1, 82, 8, 2249);
				div13.className = "level-item";
				addLoc(div13, file$1, 81, 6, 2216);
				addListener(button1, "click", click_handler_1);
				button1.className = "button is-link is-normal";
				addLoc(button1, file$1, 89, 8, 2416);
				div14.className = "level-item";
				addLoc(div14, file$1, 88, 6, 2383);
				div15.className = "level-right";
				addLoc(div15, file$1, 80, 4, 2184);
				div16.className = "level";
				addLoc(div16, file$1, 76, 2, 2101);
				div17.className = "content";
				addLoc(div17, file$1, 75, 0, 2077);
			},

			m: function mount(target, anchor) {
				insert(target, div11, anchor);
				append(div11, div8);
				append(div8, div4);
				append(div4, div1);
				append(div1, h40);
				append(div1, text1);
				append(div1, div0);
				dropdown0._mount(div0, null);
				append(div0, text2);
				dropdown1._mount(div0, null);
				append(div0, text3);
				append(div0, input0);

				input0.value = ctx.start.year;

				append(div4, text4);
				append(div4, div3);
				append(div3, h41);
				append(div3, text6);
				append(div3, div2);
				dropdown2._mount(div2, null);
				append(div2, text7);
				dropdown3._mount(div2, null);
				append(div2, text8);
				append(div2, input1);

				input1.value = ctx.end.year;

				append(div8, text9);
				append(div8, div7);
				append(div7, div5);
				append(div5, h42);
				append(div5, text11);
				append(div5, input2);

				input2.value = ctx.fname;

				append(div7, text12);
				append(div7, div6);
				append(div6, h43);
				append(div6, text14);
				append(div6, input3);

				input3.value = ctx.hourPrice;

				append(div11, text15);
				append(div11, div10);
				append(div10, div9);
				append(div9, label);
				append(label, input4);

				input4.checked = ctx.reloadOnUpdate;

				append(label, text16);
				append(label, span);
				insert(target, text18, anchor);
				insert(target, div17, anchor);
				append(div17, div16);
				append(div16, div12);
				append(div16, text19);
				append(div16, div15);
				append(div15, div13);
				append(div13, button0);
				append(div15, text21);
				append(div15, div14);
				append(div14, button1);
				insert(target, text23, anchor);

				for (var i = 0; i < each_blocks.length; i += 1) {
					each_blocks[i].m(target, anchor);
				}

				insert(target, each_anchor, anchor);
			},

			p: function update(changed, _ctx) {
				ctx = _ctx;
				var dropdown0_changes = {};
				if (changed.days) dropdown0_changes.list = ctx.days;
				if (!dropdown0_updating.value && changed.start) {
					dropdown0_changes.value = ctx.start.day;
					dropdown0_updating.value = ctx.start.day !== void 0;
				}
				dropdown0._set(dropdown0_changes);
				dropdown0_updating = {};

				var dropdown1_changes = {};
				if (changed.months) dropdown1_changes.list = ctx.months;
				if (!dropdown1_updating.value && changed.start) {
					dropdown1_changes.value = ctx.start.month;
					dropdown1_updating.value = ctx.start.month !== void 0;
				}
				dropdown1._set(dropdown1_changes);
				dropdown1_updating = {};

				if (!input0_updating && changed.start) input0.value = ctx.start.year;

				var dropdown2_changes = {};
				if (changed.days) dropdown2_changes.list = ctx.days;
				if (!dropdown2_updating.value && changed.end) {
					dropdown2_changes.value = ctx.end.day;
					dropdown2_updating.value = ctx.end.day !== void 0;
				}
				dropdown2._set(dropdown2_changes);
				dropdown2_updating = {};

				var dropdown3_changes = {};
				if (changed.months) dropdown3_changes.list = ctx.months;
				if (!dropdown3_updating.value && changed.end) {
					dropdown3_changes.value = ctx.end.month;
					dropdown3_updating.value = ctx.end.month !== void 0;
				}
				dropdown3._set(dropdown3_changes);
				dropdown3_updating = {};

				if (!input1_updating && changed.end) input1.value = ctx.end.year;
				if (!input2_updating && changed.fname) input2.value = ctx.fname;
				if (!input3_updating && changed.hourPrice) input3.value = ctx.hourPrice;
				if (changed.reloadOnUpdate) input4.checked = ctx.reloadOnUpdate;

				if (changed.$absenceData || changed.$baseUrl || changed.hourPrice) {
					each_value = ctx.$absenceData.absences;

					for (var i = 0; i < each_value.length; i += 1) {
						const child_ctx = get_each_context$1(ctx, each_value, i);

						if (each_blocks[i]) {
							each_blocks[i].p(changed, child_ctx);
						} else {
							each_blocks[i] = create_each_block$1(component, child_ctx);
							each_blocks[i].c();
							each_blocks[i].m(each_anchor.parentNode, each_anchor);
						}
					}

					for (; i < each_blocks.length; i += 1) {
						each_blocks[i].d(1);
					}
					each_blocks.length = each_value.length;
				}
			},

			d: function destroy$$1(detach) {
				if (detach) {
					detachNode(div11);
				}

				dropdown0.destroy();
				dropdown1.destroy();
				removeListener(input0, "input", input0_input_handler);
				removeListener(input0, "input", input_handler);
				dropdown2.destroy();
				dropdown3.destroy();
				removeListener(input1, "input", input1_input_handler);
				removeListener(input1, "input", input_handler_1);
				removeListener(input2, "input", input2_input_handler);
				removeListener(input2, "input", input_handler_2);
				removeListener(input3, "input", input3_input_handler);
				removeListener(input4, "change", input4_change_handler);
				if (detach) {
					detachNode(text18);
					detachNode(div17);
				}

				removeListener(button0, "click", click_handler);
				removeListener(button1, "click", click_handler_1);
				if (detach) {
					detachNode(text23);
				}

				destroyEach(each_blocks, detach);

				if (detach) {
					detachNode(each_anchor);
				}
			}
		};
	}

	// (103:4) {#if index === 0}
	function create_if_block$1(component, ctx) {
		var thead, tr, th0, text1, th1, text3, th2;

		return {
			c: function create() {
				thead = createElement("thead");
				tr = createElement("tr");
				th0 = createElement("th");
				th0.textContent = "Name";
				text1 = createText("\n          ");
				th1 = createElement("th");
				th1.textContent = "Hours taught";
				text3 = createText("\n          ");
				th2 = createElement("th");
				th2.textContent = "Lessons absent";
				addLoc(th0, file$1, 105, 10, 2746);
				addLoc(th1, file$1, 106, 10, 2770);
				addLoc(th2, file$1, 107, 10, 2802);
				addLoc(tr, file$1, 104, 8, 2731);
				thead.className = "svelte-1hqiqhm";
				addLoc(thead, file$1, 103, 6, 2715);
			},

			m: function mount(target, anchor) {
				insert(target, thead, anchor);
				append(thead, tr);
				append(tr, th0);
				append(tr, text1);
				append(tr, th1);
				append(tr, text3);
				append(tr, th2);
			},

			d: function destroy$$1(detach) {
				if (detach) {
					detachNode(thead);
				}
			}
		};
	}

	// (126:6) {#each prof.students as student}
	function create_each_block_1(component, ctx) {
		var tr, td0, a, span, text0_value = ctx.student.first_name, text0, text1, text2_value = ctx.student.last_name, text2, text3, i, a_href_value, text4, td1, text5_value = minutesToHours(ctx.student.duration), text5, text6, td2, text7_value = ctx.student.cancelled, text7;

		return {
			c: function create() {
				tr = createElement("tr");
				td0 = createElement("td");
				a = createElement("a");
				span = createElement("span");
				text0 = createText(text0_value);
				text1 = createText(" ");
				text2 = createText(text2_value);
				text3 = createText("\n              ");
				i = createElement("i");
				text4 = createText("\n          ");
				td1 = createElement("td");
				text5 = createText(text5_value);
				text6 = createText("\n          ");
				td2 = createElement("td");
				text7 = createText(text7_value);
				addLoc(span, file$1, 129, 14, 3503);
				i.className = "fa fa-pencil-square-o";
				addLoc(i, file$1, 130, 14, 3571);
				a.href = a_href_value = "" + ctx.$baseUrl + "user/" + ctx.student.uid + "/edit";
				addLoc(a, file$1, 128, 12, 3444);
				td0.className = "svelte-1hqiqhm";
				addLoc(td0, file$1, 127, 10, 3427);
				td1.className = "hours svelte-1hqiqhm";
				addLoc(td1, file$1, 133, 10, 3652);
				td2.className = "cancelled svelte-1hqiqhm";
				addLoc(td2, file$1, 134, 10, 3720);
				tr.className = "student-row";
				addLoc(tr, file$1, 126, 8, 3392);
			},

			m: function mount(target, anchor) {
				insert(target, tr, anchor);
				append(tr, td0);
				append(td0, a);
				append(a, span);
				append(span, text0);
				append(span, text1);
				append(span, text2);
				append(a, text3);
				append(a, i);
				append(tr, text4);
				append(tr, td1);
				append(td1, text5);
				append(tr, text6);
				append(tr, td2);
				append(td2, text7);
			},

			p: function update(changed, ctx) {
				if ((changed.$absenceData) && text0_value !== (text0_value = ctx.student.first_name)) {
					setData(text0, text0_value);
				}

				if ((changed.$absenceData) && text2_value !== (text2_value = ctx.student.last_name)) {
					setData(text2, text2_value);
				}

				if ((changed.$baseUrl || changed.$absenceData) && a_href_value !== (a_href_value = "" + ctx.$baseUrl + "user/" + ctx.student.uid + "/edit")) {
					a.href = a_href_value;
				}

				if ((changed.$absenceData) && text5_value !== (text5_value = minutesToHours(ctx.student.duration))) {
					setData(text5, text5_value);
				}

				if ((changed.$absenceData) && text7_value !== (text7_value = ctx.student.cancelled)) {
					setData(text7, text7_value);
				}
			},

			d: function destroy$$1(detach) {
				if (detach) {
					detachNode(tr);
				}
			}
		};
	}

	// (101:0) {#each $absenceData.absences as prof, index}
	function create_each_block$1(component, ctx) {
		var table, text0, tbody, tr, td0, a, span0, text1_value = ctx.prof.first_name, text1, text2, text3_value = ctx.prof.last_name, text3, text4, i, a_href_value, text5, td1, text6_value = minutesToHours(ctx.prof.duration), text6, text7, span1, text8_value = salary(ctx.prof.duration, ctx.hourPrice), text8, text9, b, text11, td2, text12_value = ctx.prof.cancelled, text12, text13, text14;

		var if_block = (ctx.index === 0) && create_if_block$1(component, ctx);

		var each_value_1 = ctx.prof.students;

		var each_blocks = [];

		for (var i_1 = 0; i_1 < each_value_1.length; i_1 += 1) {
			each_blocks[i_1] = create_each_block_1(component, get_each_context_1(ctx, each_value_1, i_1));
		}

		return {
			c: function create() {
				table = createElement("table");
				if (if_block) if_block.c();
				text0 = createText("\n    ");
				tbody = createElement("tbody");
				tr = createElement("tr");
				td0 = createElement("td");
				a = createElement("a");
				span0 = createElement("span");
				text1 = createText(text1_value);
				text2 = createText(" ");
				text3 = createText(text3_value);
				text4 = createText("\n            ");
				i = createElement("i");
				text5 = createText("\n        ");
				td1 = createElement("td");
				text6 = createText(text6_value);
				text7 = createText("\n          ");
				span1 = createElement("span");
				text8 = createText(text8_value);
				text9 = createText(" ");
				b = createElement("b");
				b.textContent = "EGP";
				text11 = createText("\n        ");
				td2 = createElement("td");
				text12 = createText(text12_value);
				text13 = createText("\n      ");

				for (var i_1 = 0; i_1 < each_blocks.length; i_1 += 1) {
					each_blocks[i_1].c();
				}

				text14 = createText("\n  ");
				addLoc(span0, file$1, 115, 12, 2987);
				i.className = "fa fa-pencil-square-o";
				addLoc(i, file$1, 116, 12, 3047);
				a.href = a_href_value = "" + ctx.$baseUrl + "user/" + ctx.prof.uid + "/edit";
				addLoc(a, file$1, 114, 10, 2933);
				td0.className = "svelte-1hqiqhm";
				addLoc(td0, file$1, 113, 8, 2918);
				b.className = "svelte-1hqiqhm";
				addLoc(b, file$1, 121, 66, 3249);
				span1.className = "salary svelte-1hqiqhm";
				addLoc(span1, file$1, 121, 10, 3193);
				td1.className = "hours svelte-1hqiqhm";
				addLoc(td1, file$1, 119, 8, 3122);
				td2.className = "cancelled svelte-1hqiqhm";
				addLoc(td2, file$1, 123, 8, 3289);
				tr.className = "professor-row svelte-1hqiqhm";
				addLoc(tr, file$1, 112, 6, 2883);
				addLoc(tbody, file$1, 111, 4, 2869);
				table.className = "table is-fullwidth is-bordered svelte-1hqiqhm";
				addLoc(table, file$1, 101, 2, 2640);
			},

			m: function mount(target, anchor) {
				insert(target, table, anchor);
				if (if_block) if_block.m(table, null);
				append(table, text0);
				append(table, tbody);
				append(tbody, tr);
				append(tr, td0);
				append(td0, a);
				append(a, span0);
				append(span0, text1);
				append(span0, text2);
				append(span0, text3);
				append(a, text4);
				append(a, i);
				append(tr, text5);
				append(tr, td1);
				append(td1, text6);
				append(td1, text7);
				append(td1, span1);
				append(span1, text8);
				append(span1, text9);
				append(span1, b);
				append(tr, text11);
				append(tr, td2);
				append(td2, text12);
				append(tbody, text13);

				for (var i_1 = 0; i_1 < each_blocks.length; i_1 += 1) {
					each_blocks[i_1].m(tbody, null);
				}

				append(table, text14);
			},

			p: function update(changed, ctx) {
				if (ctx.index === 0) {
					if (!if_block) {
						if_block = create_if_block$1(component, ctx);
						if_block.c();
						if_block.m(table, text0);
					}
				} else if (if_block) {
					if_block.d(1);
					if_block = null;
				}

				if ((changed.$absenceData) && text1_value !== (text1_value = ctx.prof.first_name)) {
					setData(text1, text1_value);
				}

				if ((changed.$absenceData) && text3_value !== (text3_value = ctx.prof.last_name)) {
					setData(text3, text3_value);
				}

				if ((changed.$baseUrl || changed.$absenceData) && a_href_value !== (a_href_value = "" + ctx.$baseUrl + "user/" + ctx.prof.uid + "/edit")) {
					a.href = a_href_value;
				}

				if ((changed.$absenceData) && text6_value !== (text6_value = minutesToHours(ctx.prof.duration))) {
					setData(text6, text6_value);
				}

				if ((changed.$absenceData || changed.hourPrice) && text8_value !== (text8_value = salary(ctx.prof.duration, ctx.hourPrice))) {
					setData(text8, text8_value);
				}

				if ((changed.$absenceData) && text12_value !== (text12_value = ctx.prof.cancelled)) {
					setData(text12, text12_value);
				}

				if (changed.$absenceData || changed.$baseUrl) {
					each_value_1 = ctx.prof.students;

					for (var i_1 = 0; i_1 < each_value_1.length; i_1 += 1) {
						const child_ctx = get_each_context_1(ctx, each_value_1, i_1);

						if (each_blocks[i_1]) {
							each_blocks[i_1].p(changed, child_ctx);
						} else {
							each_blocks[i_1] = create_each_block_1(component, child_ctx);
							each_blocks[i_1].c();
							each_blocks[i_1].m(tbody, null);
						}
					}

					for (; i_1 < each_blocks.length; i_1 += 1) {
						each_blocks[i_1].d(1);
					}
					each_blocks.length = each_value_1.length;
				}
			},

			d: function destroy$$1(detach) {
				if (detach) {
					detachNode(table);
				}

				if (if_block) if_block.d();

				destroyEach(each_blocks, detach);
			}
		};
	}

	function App$1(options) {
		this._debugName = '<App>';
		if (!options || (!options.target && !options.root)) {
			throw new Error("'target' is a required option");
		}
		if (!options.store) {
			throw new Error("<App> references store properties, but no store was provided");
		}

		init(this, options);
		this._state = assign(assign(this.store._init(["absenceData","baseUrl"]), data$1()), options.data);
		this.store._add(this, ["absenceData","baseUrl"]);
		if (!('days' in this._state)) console.warn("<App> was created without expected data property 'days'");
		if (!('start' in this._state)) console.warn("<App> was created without expected data property 'start'");
		if (!('months' in this._state)) console.warn("<App> was created without expected data property 'months'");
		if (!('end' in this._state)) console.warn("<App> was created without expected data property 'end'");
		if (!('fname' in this._state)) console.warn("<App> was created without expected data property 'fname'");
		if (!('hourPrice' in this._state)) console.warn("<App> was created without expected data property 'hourPrice'");
		if (!('reloadOnUpdate' in this._state)) console.warn("<App> was created without expected data property 'reloadOnUpdate'");
		if (!('$absenceData' in this._state)) console.warn("<App> was created without expected data property '$absenceData'");
		if (!('$baseUrl' in this._state)) console.warn("<App> was created without expected data property '$baseUrl'");
		this._intro = true;

		this._handlers.destroy = [removeFromStore];

		this._fragment = create_main_fragment$1(this, this._state);

		this.root._oncreate.push(() => {
			oncreate$1.call(this);
			this.fire("update", { changed: assignTrue({}, this._state), current: this._state });
		});

		if (options.target) {
			if (options.hydrate) throw new Error("options.hydrate only works if the component was compiled with the `hydratable: true` option");
			this._fragment.c();
			this._mount(options.target, options.anchor);

			flush(this);
		}
	}

	assign(App$1.prototype, protoDev);
	assign(App$1.prototype, methods_1);

	App$1.prototype._checkReadOnly = function _checkReadOnly(newState) {
	};

	/*! *****************************************************************************
	Copyright (c) Microsoft Corporation. All rights reserved.
	Licensed under the Apache License, Version 2.0 (the "License"); you may not use
	this file except in compliance with the License. You may obtain a copy of the
	License at http://www.apache.org/licenses/LICENSE-2.0

	THIS CODE IS PROVIDED ON AN *AS IS* BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
	KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION ANY IMPLIED
	WARRANTIES OR CONDITIONS OF TITLE, FITNESS FOR A PARTICULAR PURPOSE,
	MERCHANTABLITY OR NON-INFRINGEMENT.

	See the Apache Version 2.0 License for specific language governing permissions
	and limitations under the License.
	***************************************************************************** */

	function __awaiter(thisArg, _arguments, P, generator) {
	    return new (P || (P = Promise))(function (resolve, reject) {
	        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
	        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
	        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
	        step((generator = generator.apply(thisArg, _arguments || [])).next());
	    });
	}

	// Request lib
	// Drupal apiBase for requests
	let apiBase = "";
	// Utilities
	function ok(response, isAsync = true) {
	    return __awaiter(this, void 0, void 0, function* () {
	        if (isAsync && !response.ok) {
	            throw new Error("HTTP status: " + response.status);
	        }
	        let data;
	        if (isAsync) {
	            data = jSh.parseJSON(yield response.text());
	        }
	        else {
	            data = jSh.parseJSON(response);
	        }
	        if (data.error) {
	            throw new Error(data.error);
	        }
	        return data.data;
	    });
	}
	function string(thing) {
	    return JSON.stringify(thing);
	}
	// Request directives
	const request = {
	    setBase(url, _langList) {
	        apiBase = url;
	    },
	    search(fname = "", lname = "", email = "", phone = "", role = Role.Both, studentsWithoutLessons = false, teachersWithoutStudents = true, page = 0) {
	        return __awaiter(this, void 0, void 0, function* () {
	            const response = yield fetch(apiBase + "/search?page=" + page, {
	                method: "POST",
	                body: string({
	                    fname,
	                    lname,
	                    email,
	                    phone,
	                    role: +role,
	                    studentsWithoutLessons,
	                    teachersWithoutStudents,
	                }),
	            });
	            return yield ok(response);
	        });
	    },
	    absences(name, startDay, startMonth, startYear, endDay, endMonth, endYear, page = 0) {
	        return __awaiter(this, void 0, void 0, function* () {
	            const response = yield fetch(apiBase + "/getAbsences?page=" + page, {
	                method: "POST",
	                body: string({
	                    name,
	                    startDay,
	                    startMonth,
	                    startYear,
	                    endDay,
	                    endMonth,
	                    endYear,
	                }),
	            });
	            return yield ok(response);
	        });
	    },
	};

	function main(env) {
	    const store = new Store({
	        baseUrl: env.baseUrl,
	        locale: env.locale,
	        absenceData: {
	            start: 0,
	            end: 0,
	            absences: [],
	        },
	    });
	    const app = new App$1({
	        target: env.root,
	        store,
	    });
	    request.setBase(env.apiBase);
	    store.on("absencequery", (query) => {
	        request
	            .absences(query.name, query.startDay, query.startMonth, query.startYear, query.endDay, query.endMonth, query.endYear)
	            .then((data) => {
	            store.set({
	                absenceData: data,
	            });
	            store.fire("absenceresults", data);
	        })
	            .catch((e) => {
	            console.error("Failed to load absence data", e);
	        });
	    });
	    // Do first search on load
	    setTimeout(() => {
	        app.search();
	    }, 350);
	}
	window.IhfathAbsence = main;
	// Side effects for Rollup
	_();

}());
//# sourceMappingURL=absenceadmin.js.map
