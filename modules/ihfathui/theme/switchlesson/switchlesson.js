// @ts-check
// Ihfath Professor Current Schedule Implementation with Cellidi
function IhfathSwitchLesson(args) {
  var options = {
    element: args.root,

    dimensions: args.dimensions,

    offsetX: 0,
    offsetY: 0,

    canSelect: false,
    history: true,

    headers: {
      top: scheduleOnNewDayHeaderCell,
      left: scheduleOnNewHourHeaderCell
    },

    newCell: scheduleOnNewCell,
    onCellStateChange: scheduleOnStateChange,

    emptyState: function(renew, old) {
      if (renew) {
        return {
          dur: old.dur,
          pid: old.pid,
          hour: old.hour
        };
      } else {
        return {
          dur: 0,
          pid: 0,
          hour: null
        };
      }
    },

    diffStates: function(a, b) {
      return a.dur !== b.dur || a.pid !== b.pid;
    }
  };

  // Clear wrapper
  args.root.innerHTML = "";

  // Create CellDisplay
  var CellDisplay = Cellidi.CellDisplay;
  var schedule    = Cellidi(options, CellDisplay.mapEachCell(args.data.table, function(data) {
    return {
      dur: 0,
      pid: 0,
      hour: data
    };
  }));

  // Cellidi Callbacks
  function scheduleOnNewCell(x, y, virtualX, virtualY, data, cellDisplay) {
    // Make cell time in 12hour format
    var yMap         = virtualY === 0 ? 12 : virtualY;
    var cellTime     = virtualY < 12 ? [yMap, "AM"] : [(virtualY === 12 ? 12 : yMap - 12), "PM"];
    var cellTimeFull = cellTime.join("");
    var cell;
    var checker = y % 2;

    if (data.hour !== 1 /* Vacant */) {
      var bookedMap = {
        2: ".ihfath-booked-cell",      // Booked
        4: ".ihfath-user-booked-cell", // Booked by the same user in the cart
      };
      var bookedHour = bookedMap[data.hour] || "";

      // It's a weekend cell, return dead cell
      cell = jSh.d(bookedHour + (checker ? ".ihfath-cell-checker" : ""), null, [
        jSh.c("span", ".ihfath-time", cellTime[0]),
        jSh.c("span", ".ihfath-time-meta", cellTime[1])
      ]);

      if (data.hour === 4) {
        cell.title = args.locale.bookedByUser;
      }

      return {
        dom: cell,
        deadCell: true
      };
    }

    var content;
    var extraClass = "";
    var cellLink   = "javascript:void(0)";
    var element    = "div";

    if (isNaN(data.hour)) {
      // It's data with lessons
      element = "a";

      content = [

      ];

      extraClass = ".ihfath-full-cell";
    } else {
      // Nothing on this day
      var lessonDetails = jSh.c("span", ".ihfath-lesson-details");
      content = [
        jSh.c("span", ".ihfath-time", cellTime[0]),
        jSh.c("span", ".ihfath-time-meta", cellTime[1]),
        lessonDetails,
        jSh.d("ihfath-fill-indicator"),
        jSh.d("ihfath-selection-indicator")
      ];

      extraClass = ".ihfath-empty-cell";
    }

    cell = jSh.c(element, {
      prop: {
        href: cellLink,
        title: args.locale.time + " " + cellTimeFull + ", " + args.dayMap[virtualX]
      },

      sel: extraClass + (checker ? ".ihfath-cell-checker" : ""),
      child: content
    });

    return {
      dom: cell,
      domLesson: lessonDetails
    };
  }

  function scheduleOnNewDayHeaderCell(x, virtualX, orientation, cellDisplay) {
    return jSh.d(null, args.dayMap[virtualX]);
  }

  function scheduleOnNewHourHeaderCell(y, virtualY, orientation, cellDisplay) {
    var yMap     = virtualY === 0 ? 12 : virtualY;
    var cellTime = virtualY < 12 ? [yMap, "AM"] : [(virtualY === 12 ? 12 : yMap - 12), "PM"];

    var checker = y % 2;

    return jSh.d(".main-color" + (checker ? ".ihfath-cell-checker" : ""), null, [
      jSh.t(cellTime[0]),
      jSh.c("span", null, cellTime[1])
    ]);
  }

  function scheduleOnStateChange(state, cellModels) {
    for (var i=0; i<state.length; i++) {
      var newState = state[i];
      var model    = cellModels[i];
      var dom      = model.dom;

      dom.classList.remove("ihfath-full");
      dom.classList.remove("ihfath-half-full");

      if (newState.pid !== 0) {
        switch (newState.dur) {
          case 0.5:
            dom.classList.add("ihfath-half-full");
            break;
          case 1:
          case 1.5:
          case 2:
            dom.classList.add("ihfath-full");
            break;
        }

        dom.classList.remove("ihfath-empty-cell");
        if (newState.pid === -1) {
          model.domLesson.innerHTML = "";
        } else {
          model.domLesson.innerHTML = "";
          model.domLesson.appendChild([
            jSh.t(args.programName),
            jSh.c("span", ".main-color", " " + newState.dur + "h")
          ]);
        }
      } else {
        dom.classList.add("ihfath-empty-cell");
      }
    }
  }

  // State

  // Current cell selected
  var curCell = null;

  // Cell state map pertaining cell's availability and whether they're selected
  var fullCells = schedule.mapEachCell(schedule._cellMap, function(cell) {
    if (cell.deadCell) {
      return null;
    } else {
      return false;
    }
  });

  // Update `fullCells` on rebuild
  schedule.on("rebuild", function() {
    fullCells = schedule.mapEachCell(schedule._cellMap, function(cell) {
      if (cell.deadCell) {
        return null;
      } else {
        return false;
      }
    });
  });

  // Create events
  schedule.on("activeinput", function(e) {
    var model;

    if (model = e.cell) {
      // Prevent default browser text selection behaviour
      e.event.preventDefault();

      // Update UI
      var dom      = model.dom;
      var coords   = model.coords;
      var state    = schedule.getCellState(model);
      var nextCell = false;

      // Select upper cell if this is a partially selected cell
      if (fullCells[coords[0]][coords[1]] === true) {
        // Get upper cell
        model    = schedule._cellMap[coords[0]][coords[1] - 1];
        dom      = model.dom;
        state    = schedule.getCellState(model);
        nextCell = true;
      }
      // This isn't a partial cell, indicate whether the bottom cell is available
      else if (fullCells[coords[0]][coords[1] + 1] !== null && fullCells[coords[0]][coords[1] + 1] !== undefined) {
        // This cell has a free cell beneath it
        nextCell = true;
      }

      // Only update modal if we've truly selected a different cell, and that
      // the hour fits into this cell and possibly the next
      if (curCell !== model && (nextCell || args.duration <= 1)) {
        // Clear older cell
        clearCell();

        // Update DOM for new cell
        curCell = model;
        updateCell();
      }
    }
  });

  function clearCell() {
    if (curCell) {
      var cellState = schedule.getCellState(curCell);
      var editCells = [[curCell.coords[0], curCell.coords[1]]];

      if (cellState.dur > 1) {
        editCells.push([curCell.coords[0], curCell.coords[1] + 1]);
        fullCells[curCell.coords[0]][curCell.coords[1] + 1] = false;
      }

      schedule.changeSubset(editCells, function(x, y, data) {
        return {
          pid: 0,
          dur: 0,
          hour: data.hour
        };
      });
    }
  };

  function updateCell() {
    var dur = args.duration;
    var pid = 1;

    var x = curCell.coords[0];
    var y = curCell.coords[1];

    var cellState = schedule.getCellState(curCell);
    var height    = schedule.options.dimensions[1];
    var editCells = [];

    // Set new cell states
    for (var i=0; i<height; i++) {
      editCells.push([x, i]);

      switch (i) {
        case y + 1:
          if (dur > 1) {
            fullCells[x][i] = true;
          } else {
            // Mark this cell as available if it's not a dead cell
            if (fullCells[x][i] !== null) {
              fullCells[x][i] = false;
            }
          }
          break;
        default:
          // Mark this cell as available if it's not a dead cell
          if (fullCells[x][i] !== null) {
            fullCells[x][i] = false;
          }
      }
    }

    schedule.changeSubset(editCells, function(cellX, cellY, data) {
      if (dur === 1) {
        if (y === cellY) {
          return {
            dur: dur,
            pid,
            hour: data.hour
          };
        } else {
          return {
            dur: 0,
            pid: 0,
            hour: data.hour
          };
        }
      } else {
        switch (cellY) {
          case y:
            return {
              dur: dur,
              pid,
              hour: data.hour
            };
            break;
          case y + 1:
            return {
              dur: dur - 1,
              pid: -1,
              hour: data.hour
            };
            break;
          default:
            return {
              dur: 0,
              pid: 0,
              hour: data.hour
            };
            break;
        }
      }
    });
  }

  // Data change updates
  schedule.on("newstate", function() {
    updateHiddenLessonsInput();
  });

  // Schedule informing outside world of updates
  schedule.addEvent("update");
  let newLesson = [];
  function updateHiddenLessonsInput() {
    var table  = schedule.state;
    var width  = schedule.options.dimensions[0];
    var height = schedule.options.dimensions[1];

    var lessons = [];

    for (var x=0; x<width; x++) {
      for (var y=0; y<height; y++) {
        var cell = table[x][y];

        if (cell.pid > 0) {
          var hour = y;

          if (hour !== 0) {
            hour *= 10;
          }

          // Add this lesson to the list
          lessons.push({
            weektime: ((x + 1) * 1000) + hour,
            dur: cell.dur * 60,
            pid: cell.pid
          });

          if (cell.dur > 1) {
            // The next cell is also occupied with this class, skip it
            y++;
          }
        }
      }
    }

    newLesson = lessons;
    schedule.triggerEvent("update", {
      lessons,
    });
  }

  schedule.on("update", function({ lessons }) {
    if (lessons.length) {
      args.switchLessonBtn.removeAttribute("disabled");
    } else {
      args.switchLessonBtn.disabled = true;
    }
  });

  args.switchLessonBtn.addEventListener("click", function() {
    if (newLesson.length) {
      const lesson = newLesson[0];

      fetch(args.switchApi, {
        method: "POST",
        body: JSON.stringify({
          lid: args.lid,
          weektime: lesson.weektime,
        }),
      }).then(response => {
        response.text().then(body => {
          const data = JSON.parse(body);

          if (data.error) {
            console.error("Failed switching lesson", data.error);
          } else {
            document.location = args.frontPage;
          }
        });
      });
    }
  });

  return {
    schedule,
  };
}
