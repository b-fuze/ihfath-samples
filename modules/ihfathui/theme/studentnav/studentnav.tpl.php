<?php
// Ihfath Professor Navigation
global $base_url;
global $user;

$unread_count = Ihfathmsg\Get::count(FALSE, NULL, NULL, $user->uid, FALSE);
?>
<div class="ihfath-double-small-section section main-bg">
  <div class="container">
    <div class="row">
      <a href="<?php echo $base_url; ?>/" class="col-md-3 ihfath-t-center ihfath-prof-nav-col">
        <div class="fa fa-home ihfath-prof-icon"></div>
        <span><?php echo t("Home"); ?></span>
      </a>

      <a href="<?php echo $base_url; ?>/koa/lesson/archive" class="col-md-3 ihfath-t-center ihfath-prof-nav-col">
        <div class="fa fa-calendar ihfath-prof-icon"></div>
        <span><?php echo t("Schedule"); ?></span>
      </a>

      <a href="<?php echo $base_url; ?>/koa/lesson/archive/list" class="col-md-3 ihfath-t-center ihfath-prof-nav-col">
        <div class="fa fa-list ihfath-prof-icon"></div>
        <span><?php echo t("Archive"); ?></span>
      </a>

      <a href="<?php echo $base_url; ?>/ihfath/messages" class="col-md-3 ihfath-t-center ihfath-prof-nav-col">
        <div class="fa fa-envelope ihfath-prof-icon">
          <div id="ihfathmsg-new-link" <?php echo $unread_count ? "data-msg-count=\"{$unread_count}\"" : ""; ?>>
            <!-- Icon here -->
          </div>
        </div>
        <span><?php echo t("Messages"); ?></span>
      </a>
    </div>
  </div>
</div>

<style media="screen">

  #ihfathmsg-new-link {
    position: absolute;
    z-index: 10;
    left: 10px;
    right: 0px;
    top: 1px;
    width: 60px;
    height: 100%;
    margin: 0px auto;

    pointer-events: none;
  }

  .ihfath-small-section {
    padding: 50px 0px;
  }

  .ihfath-double-small-section {
    padding: 25px 0px;
  }

  .ihfath-t-center {
    text-align: center;
  }

  .ihfath-prof-nav-col > span {
    font-weight: bold;
    font-size: 15px;
  }

  div.fa.ihfath-prof-icon {
    position: relative;
    display: block;
    margin-bottom: 10px;

    font-size: 60px;
    text-align: center;
  }
</style>
